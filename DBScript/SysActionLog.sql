USE [RM_DB]
GO

/****** Object:  Table [dbo].[Base_SysActionLog]    Script Date: 04/06/2016 23:35:36 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Base_SysActionLog](
	[PKID] [varchar](36) NOT NULL,
	[IP] [varchar](50) NULL,
	[ActionContents] [nvarchar](200) NULL,
	[CreateTime] [datetime] NULL,
	[CreateBy] [varchar](50) NULL,
	[HostName] [varchar](100) NULL,
	[PageName] [varchar](50) NULL,
 CONSTRAINT [PK_Base_SysActionLog] PRIMARY KEY CLUSTERED 
(
	[PKID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[Base_SysActionLog] ADD  CONSTRAINT [DF_Base_SysActionLog_PKID]  DEFAULT (newid()) FOR [PKID]
GO



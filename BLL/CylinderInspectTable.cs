﻿using System;
using System.Data;
using System.Collections.Generic;
using Maticsoft.Common;
using EGMNGS.Model;
namespace EGMNGS.BLL
{
    /// <summary>
    /// 登记钢瓶送检记录。
    /// </summary>
    public partial class CylinderInspectTable
    {
        private readonly EGMNGS.DAL.CylinderInspectTable dal = new EGMNGS.DAL.CylinderInspectTable();
        public CylinderInspectTable()
        { }
        #region  Method

        /// <summary>
        /// 得到最大ID
        /// </summary>
        public int GetMaxId()
        {
            return dal.GetMaxId();
        }

        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int OID)
        {
            return dal.Exists(OID);
        }

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(EGMNGS.Model.CylinderInspectTable model)
        {
            return dal.Add(model);
        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(EGMNGS.Model.CylinderInspectTable model)
        {
            return dal.Update(model);
        }
        public bool UpdateSJ(EGMNGS.Model.CylinderInspectTable model)
        {
            return dal.UpdateSJ(model);
        }
        public bool UpdateStatus(string cylinderCode, string status,string oid)
        {
            return dal.UpdateStatus(cylinderCode, status,oid);
        }
        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int OID)
        {

            return dal.Delete(OID);
        }
        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool DeleteList(string OIDlist)
        {
            return dal.DeleteList(OIDlist);
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public EGMNGS.Model.CylinderInspectTable GetModel(int OID)
        {

            return dal.GetModel(OID);
        }

        /// <summary>
        /// 得到一个对象实体，从缓存中
        /// </summary>
        public EGMNGS.Model.CylinderInspectTable GetModelByCache(int OID)
        {

            string CacheKey = "CylinderInspectTableModel-" + OID;
            object objModel = Maticsoft.Common.DataCache.GetCache(CacheKey);
            if (objModel == null)
            {
                try
                {
                    objModel = dal.GetModel(OID);
                    if (objModel != null)
                    {
                        int ModelCache = Maticsoft.Common.ConfigHelper.GetConfigInt("ModelCache");
                        Maticsoft.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
                    }
                }
                catch { }
            }
            return (EGMNGS.Model.CylinderInspectTable)objModel;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            return dal.GetList(strWhere);
        }
        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            return dal.GetList(Top, strWhere, filedOrder);
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<EGMNGS.Model.CylinderInspectTable> GetModelList(string strWhere)
        {
            DataSet ds = dal.GetList(strWhere);
            return DataTableToList(ds.Tables[0]);
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<EGMNGS.Model.CylinderInspectTable> DataTableToList(DataTable dt)
        {
            List<EGMNGS.Model.CylinderInspectTable> modelList = new List<EGMNGS.Model.CylinderInspectTable>();
            int rowsCount = dt.Rows.Count;
            if (rowsCount > 0)
            {
                EGMNGS.Model.CylinderInspectTable model;
                for (int n = 0; n < rowsCount; n++)
                {
                    model = new EGMNGS.Model.CylinderInspectTable();
                    if (dt.Rows[n]["OID"] != null && dt.Rows[n]["OID"].ToString() != "")
                    {
                        model.OID = int.Parse(dt.Rows[n]["OID"].ToString());
                    }
                    if (dt.Rows[n]["CylinderCode"] != null && dt.Rows[n]["CylinderCode"].ToString() != "")
                    {
                        model.CylinderCode = dt.Rows[n]["CylinderCode"].ToString();
                    }
                    if (dt.Rows[n]["OriginalEffectiveDate"] != null && dt.Rows[n]["OriginalEffectiveDate"].ToString() != "")
                    {
                        model.OriginalEffectiveDate = DateTime.Parse(dt.Rows[n]["OriginalEffectiveDate"].ToString());
                    }
                    if (dt.Rows[n]["NewEffectiveDate"] != null && dt.Rows[n]["NewEffectiveDate"].ToString() != "")
                    {
                        model.NewEffectiveDate = DateTime.Parse(dt.Rows[n]["NewEffectiveDate"].ToString());
                    }
                    if (dt.Rows[n]["IsPass"] != null && dt.Rows[n]["IsPass"].ToString() != "")
                    {
                        model.IsPass = dt.Rows[n]["IsPass"].ToString();
                    }
                    if (dt.Rows[n]["Remarks"] != null && dt.Rows[n]["Remarks"].ToString() != "")
                    {
                        model.Remarks = dt.Rows[n]["Remarks"].ToString();
                    }
                    if (dt.Rows[n]["Status"] != null && dt.Rows[n]["Status"].ToString() != "")
                    {
                        model.Status = dt.Rows[n]["Status"].ToString();
                    }
                    if (dt.Rows[n]["SendCheckOID"] != null && dt.Rows[n]["SendCheckOID"].ToString() != "")
                    {
                        model.SendCheckOID = dt.Rows[n]["SendCheckOID"].ToString();
                    }
                    if (dt.Rows[n]["SendCheckDate"] != null && dt.Rows[n]["SendCheckDate"].ToString() != "")
                    {
                        model.SendCheckDate = DateTime.Parse(dt.Rows[n]["SendCheckDate"].ToString());
                    }
                    if (dt.Rows[n]["RegistantsOID"] != null && dt.Rows[n]["RegistantsOID"].ToString() != "")
                    {
                        model.RegistantsOID = dt.Rows[n]["RegistantsOID"].ToString();
                    }
                    if (dt.Rows[n]["RegistantsDate"] != null && dt.Rows[n]["RegistantsDate"].ToString() != "")
                    {
                        model.RegistantsDate = DateTime.Parse(dt.Rows[n]["RegistantsDate"].ToString());
                    }
                    if (dt.Rows[n]["CREATED_BY"] != null && dt.Rows[n]["CREATED_BY"].ToString() != "")
                    {
                        model.CREATED_BY = dt.Rows[n]["CREATED_BY"].ToString();
                    }
                    if (dt.Rows[n]["CREATED_DATE"] != null && dt.Rows[n]["CREATED_DATE"].ToString() != "")
                    {
                        model.CREATED_DATE = DateTime.Parse(dt.Rows[n]["CREATED_DATE"].ToString());
                    }
                    if (dt.Rows[n]["LAST_UPD_BY"] != null && dt.Rows[n]["LAST_UPD_BY"].ToString() != "")
                    {
                        model.LAST_UPD_BY = dt.Rows[n]["LAST_UPD_BY"].ToString();
                    }
                    if (dt.Rows[n]["LAST_UPD_DATE"] != null && dt.Rows[n]["LAST_UPD_DATE"].ToString() != "")
                    {
                        model.LAST_UPD_DATE = DateTime.Parse(dt.Rows[n]["LAST_UPD_DATE"].ToString());
                    }
                    modelList.Add(model);
                }
            }
            return modelList;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetAllList()
        {
            return GetList("");
        }

        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            return dal.GetRecordCount(strWhere);
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            return dal.GetListByPage(strWhere, orderby, startIndex, endIndex);
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        //public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        //{
        //return dal.GetList(PageSize,PageIndex,strWhere);
        //}

        #endregion  Method
    }
}


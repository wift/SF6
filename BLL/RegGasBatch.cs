﻿using System;
using System.Data;
using System.Collections.Generic;
using Maticsoft.Common;
using EGMNGS.Model;
namespace EGMNGS.BLL
{
	/// <summary>
	/// 对气体净化处理后进行
	/// </summary>
	public partial class RegGasBatch
	{
		private readonly EGMNGS.DAL.RegGasBatch dal=new EGMNGS.DAL.RegGasBatch();
		public RegGasBatch()
		{}
		#region  Method

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
			return dal.GetMaxId();
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int OID)
		{
			return dal.Exists(OID);
		}

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int  Add(EGMNGS.Model.RegGasBatch model)
		{
			return dal.Add(model);
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(EGMNGS.Model.RegGasBatch model)
		{
			return dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int OID)
		{
			
			return dal.Delete(OID);
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool DeleteList(string OIDlist )
		{
			return dal.DeleteList(OIDlist );
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public EGMNGS.Model.RegGasBatch GetModel(int OID)
		{
			
			return dal.GetModel(OID);
		}

		/// <summary>
		/// 得到一个对象实体，从缓存中
		/// </summary>
		public EGMNGS.Model.RegGasBatch GetModelByCache(int OID)
		{
			
			string CacheKey = "RegGasBatchModel-" + OID;
			object objModel = Maticsoft.Common.DataCache.GetCache(CacheKey);
			if (objModel == null)
			{
				try
				{
					objModel = dal.GetModel(OID);
					if (objModel != null)
					{
						int ModelCache = Maticsoft.Common.ConfigHelper.GetConfigInt("ModelCache");
						Maticsoft.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
					}
				}
				catch{}
			}
			return (EGMNGS.Model.RegGasBatch)objModel;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			return dal.GetList(Top,strWhere,filedOrder);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<EGMNGS.Model.RegGasBatch> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<EGMNGS.Model.RegGasBatch> DataTableToList(DataTable dt)
		{
			List<EGMNGS.Model.RegGasBatch> modelList = new List<EGMNGS.Model.RegGasBatch>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				EGMNGS.Model.RegGasBatch model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = new EGMNGS.Model.RegGasBatch();
					if(dt.Rows[n]["OID"]!=null && dt.Rows[n]["OID"].ToString()!="")
					{
						model.OID=int.Parse(dt.Rows[n]["OID"].ToString());
					}
					if(dt.Rows[n]["BatchCode"]!=null && dt.Rows[n]["BatchCode"].ToString()!="")
					{
					model.BatchCode=dt.Rows[n]["BatchCode"].ToString();
					}
					if(dt.Rows[n]["Year"]!=null && dt.Rows[n]["Year"].ToString()!="")
					{
					model.Year=dt.Rows[n]["Year"].ToString();
					}
					if(dt.Rows[n]["Month"]!=null && dt.Rows[n]["Month"].ToString()!="")
					{
					model.Month=dt.Rows[n]["Month"].ToString();
					}
					if(dt.Rows[n]["ProcessEquipment"]!=null && dt.Rows[n]["ProcessEquipment"].ToString()!="")
					{
					model.ProcessEquipment=dt.Rows[n]["ProcessEquipment"].ToString();
					}
					if(dt.Rows[n]["AmountGas"]!=null && dt.Rows[n]["AmountGas"].ToString()!="")
					{
						model.AmountGas=decimal.Parse(dt.Rows[n]["AmountGas"].ToString());
					}
					if(dt.Rows[n]["SFContent"]!=null && dt.Rows[n]["SFContent"].ToString()!="")
					{
						model.SFContent=decimal.Parse(dt.Rows[n]["SFContent"].ToString());
					}
					if(dt.Rows[n]["AirContent"]!=null && dt.Rows[n]["AirContent"].ToString()!="")
					{
						model.AirContent=decimal.Parse(dt.Rows[n]["AirContent"].ToString());
					}
					if(dt.Rows[n]["SFHT"]!=null && dt.Rows[n]["SFHT"].ToString()!="")
					{
						model.SFHT=decimal.Parse(dt.Rows[n]["SFHT"].ToString());
					}
					if(dt.Rows[n]["Wet"]!=null && dt.Rows[n]["Wet"].ToString()!="")
					{
						model.Wet=decimal.Parse(dt.Rows[n]["Wet"].ToString());
					}
					if(dt.Rows[n]["Acidity"]!=null && dt.Rows[n]["Acidity"].ToString()!="")
					{
						model.Acidity=decimal.Parse(dt.Rows[n]["Acidity"].ToString());
					}
					if(dt.Rows[n]["KSJFHW"]!=null && dt.Rows[n]["KSJFHW"].ToString()!="")
					{
						model.KSJFHW=decimal.Parse(dt.Rows[n]["KSJFHW"].ToString());
					}
					if(dt.Rows[n]["KWY"]!=null && dt.Rows[n]["KWY"].ToString()!="")
					{
						model.KWY=decimal.Parse(dt.Rows[n]["KWY"].ToString());
					}
					if(dt.Rows[n]["Status"]!=null && dt.Rows[n]["Status"].ToString()!="")
					{
					model.Status=dt.Rows[n]["Status"].ToString();
					}
					if(dt.Rows[n]["RegistantsOID"]!=null && dt.Rows[n]["RegistantsOID"].ToString()!="")
					{
					model.RegistantsOID=dt.Rows[n]["RegistantsOID"].ToString();
					}
					if(dt.Rows[n]["RegistantsDate"]!=null && dt.Rows[n]["RegistantsDate"].ToString()!="")
					{
						model.RegistantsDate=DateTime.Parse(dt.Rows[n]["RegistantsDate"].ToString());
					}
					if(dt.Rows[n]["CREATED_BY"]!=null && dt.Rows[n]["CREATED_BY"].ToString()!="")
					{
					model.CREATED_BY=dt.Rows[n]["CREATED_BY"].ToString();
					}
					if(dt.Rows[n]["CREATED_DATE"]!=null && dt.Rows[n]["CREATED_DATE"].ToString()!="")
					{
						model.CREATED_DATE=DateTime.Parse(dt.Rows[n]["CREATED_DATE"].ToString());
					}
					if(dt.Rows[n]["LAST_UPD_BY"]!=null && dt.Rows[n]["LAST_UPD_BY"].ToString()!="")
					{
					model.LAST_UPD_BY=dt.Rows[n]["LAST_UPD_BY"].ToString();
					}
					if(dt.Rows[n]["LAST_UPD_DATE"]!=null && dt.Rows[n]["LAST_UPD_DATE"].ToString()!="")
					{
						model.LAST_UPD_DATE=DateTime.Parse(dt.Rows[n]["LAST_UPD_DATE"].ToString());
					}
                    if (dt.Rows[n]["IsPass"] != null && dt.Rows[n]["IsPass"].ToString() != "")
                    {
                        model.IsPass = dt.Rows[n]["IsPass"].ToString();
                    }
					modelList.Add(model);
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
            return GetList(" 1=1 ORDER BY OID DESC");
		}

		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			return dal.GetRecordCount(strWhere);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			return dal.GetListByPage( strWhere,  orderby,  startIndex,  endIndex);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		//public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		//{
			//return dal.GetList(PageSize,PageIndex,strWhere);
		//}

		#endregion  Method
	}
}


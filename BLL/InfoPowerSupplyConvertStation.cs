﻿using System;
using System.Data;
using System.Collections.Generic;
using Maticsoft.Common;
using EGMNGS.Model;
namespace EGMNGS.BLL
{
	/// <summary>
	/// 登记各供电局的变电站
	/// </summary>
	public partial class InfoPowerSupplyConvertStation
	{
		private readonly EGMNGS.DAL.InfoPowerSupplyConvertStation dal=new EGMNGS.DAL.InfoPowerSupplyConvertStation();
		public InfoPowerSupplyConvertStation()
		{}
		#region  Method

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
			return dal.GetMaxId();
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int OID)
		{
			return dal.Exists(OID);
		}

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int  Add(EGMNGS.Model.InfoPowerSupplyConvertStation model)
		{
			return dal.Add(model);
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(EGMNGS.Model.InfoPowerSupplyConvertStation model)
		{
			return dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int OID)
		{
			
			return dal.Delete(OID);
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool DeleteList(string OIDlist )
		{
			return dal.DeleteList(OIDlist );
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public EGMNGS.Model.InfoPowerSupplyConvertStation GetModel(int OID)
		{
			
			return dal.GetModel(OID);
		}

		/// <summary>
		/// 得到一个对象实体，从缓存中
		/// </summary>
		public EGMNGS.Model.InfoPowerSupplyConvertStation GetModelByCache(int OID)
		{
			
			string CacheKey = "InfoPowerSupplyConvertStationModel-" + OID;
			object objModel = Maticsoft.Common.DataCache.GetCache(CacheKey);
			if (objModel == null)
			{
				try
				{
					objModel = dal.GetModel(OID);
					if (objModel != null)
					{
						int ModelCache = Maticsoft.Common.ConfigHelper.GetConfigInt("ModelCache");
						Maticsoft.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
					}
				}
				catch{}
			}
			return (EGMNGS.Model.InfoPowerSupplyConvertStation)objModel;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			return dal.GetList(Top,strWhere,filedOrder);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<EGMNGS.Model.InfoPowerSupplyConvertStation> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<EGMNGS.Model.InfoPowerSupplyConvertStation> DataTableToList(DataTable dt)
		{
			List<EGMNGS.Model.InfoPowerSupplyConvertStation> modelList = new List<EGMNGS.Model.InfoPowerSupplyConvertStation>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				EGMNGS.Model.InfoPowerSupplyConvertStation model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = new EGMNGS.Model.InfoPowerSupplyConvertStation();
					if(dt.Rows[n]["OID"]!=null && dt.Rows[n]["OID"].ToString()!="")
					{
						model.OID=int.Parse(dt.Rows[n]["OID"].ToString());
					}
					if(dt.Rows[n]["PowerSupplyID"]!=null && dt.Rows[n]["PowerSupplyID"].ToString()!="")
					{
					model.PowerSupplyID=dt.Rows[n]["PowerSupplyID"].ToString();
					}
					if(dt.Rows[n]["PowerSupplyCode"]!=null && dt.Rows[n]["PowerSupplyCode"].ToString()!="")
					{
					model.PowerSupplyCode=dt.Rows[n]["PowerSupplyCode"].ToString();
					}
					if(dt.Rows[n]["PowerSupplyName"]!=null && dt.Rows[n]["PowerSupplyName"].ToString()!="")
					{
					model.PowerSupplyName=dt.Rows[n]["PowerSupplyName"].ToString();
					}
					if(dt.Rows[n]["CovnertStationCode"]!=null && dt.Rows[n]["CovnertStationCode"].ToString()!="")
					{
					model.CovnertStationCode=dt.Rows[n]["CovnertStationCode"].ToString();
					}
					if(dt.Rows[n]["ConvartStationName"]!=null && dt.Rows[n]["ConvartStationName"].ToString()!="")
					{
					model.ConvartStationName=dt.Rows[n]["ConvartStationName"].ToString();
					}
					if(dt.Rows[n]["Status"]!=null && dt.Rows[n]["Status"].ToString()!="")
					{
					model.Status=dt.Rows[n]["Status"].ToString();
					}
					if(dt.Rows[n]["RegistrantOID"]!=null && dt.Rows[n]["RegistrantOID"].ToString()!="")
					{
					model.RegistrantOID=dt.Rows[n]["RegistrantOID"].ToString();
					}
					if(dt.Rows[n]["RegistrantDate"]!=null && dt.Rows[n]["RegistrantDate"].ToString()!="")
					{
						model.RegistrantDate=DateTime.Parse(dt.Rows[n]["RegistrantDate"].ToString());
					}
					if(dt.Rows[n]["CREATED_BY"]!=null && dt.Rows[n]["CREATED_BY"].ToString()!="")
					{
					model.CREATED_BY=dt.Rows[n]["CREATED_BY"].ToString();
					}
					if(dt.Rows[n]["CREATED_DATE"]!=null && dt.Rows[n]["CREATED_DATE"].ToString()!="")
					{
						model.CREATED_DATE=DateTime.Parse(dt.Rows[n]["CREATED_DATE"].ToString());
					}
					if(dt.Rows[n]["LAST_UPD_BY"]!=null && dt.Rows[n]["LAST_UPD_BY"].ToString()!="")
					{
					model.LAST_UPD_BY=dt.Rows[n]["LAST_UPD_BY"].ToString();
					}
					if(dt.Rows[n]["LAST_UPD_DATE"]!=null && dt.Rows[n]["LAST_UPD_DATE"].ToString()!="")
					{
						model.LAST_UPD_DATE=DateTime.Parse(dt.Rows[n]["LAST_UPD_DATE"].ToString());
					}
					modelList.Add(model);
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			return dal.GetRecordCount(strWhere);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			return dal.GetListByPage( strWhere,  orderby,  startIndex,  endIndex);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		//public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		//{
			//return dal.GetList(PageSize,PageIndex,strWhere);
		//}

		#endregion  Method
	}
}


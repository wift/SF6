﻿using System;
using System.Data;
using System.Collections.Generic;
using Maticsoft.Common;
using EGMNGS.Model;
namespace EGMNGS.BLL
{
    /// <summary>
    /// 登记供电局下属变电站
    /// </summary>
    public partial class InfoDevChangeFill
    {
        private readonly EGMNGS.DAL.InfoDevChangeFill dal = new EGMNGS.DAL.InfoDevChangeFill();
        public InfoDevChangeFill()
        { }
        #region  Method

        /// <summary>
        /// 得到最大ID
        /// </summary>
        public int GetMaxId()
        {
            return dal.GetMaxId();
        }

        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int OID)
        {
            return dal.Exists(OID);
        }

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(EGMNGS.Model.InfoDevChangeFill model)
        {
            return dal.Add(model);
        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(EGMNGS.Model.InfoDevChangeFill model)
        {
            return dal.Update(model);
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int OID)
        {

            return dal.Delete(OID);
        }
        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool DeleteList(string OIDlist)
        {
            return dal.DeleteList(OIDlist);
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public EGMNGS.Model.InfoDevChangeFill GetModel(int OID)
        {

            return dal.GetModel(OID);
        }

        /// <summary>
        /// 得到一个对象实体，从缓存中
        /// </summary>
        public EGMNGS.Model.InfoDevChangeFill GetModelByCache(int OID)
        {

            string CacheKey = "InfoDevChangeFillModel-" + OID;
            object objModel = Maticsoft.Common.DataCache.GetCache(CacheKey);
            if (objModel == null)
            {
                try
                {
                    objModel = dal.GetModel(OID);
                    if (objModel != null)
                    {
                        int ModelCache = Maticsoft.Common.ConfigHelper.GetConfigInt("ModelCache");
                        Maticsoft.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
                    }
                }
                catch { }
            }
            return (EGMNGS.Model.InfoDevChangeFill)objModel;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            return dal.GetList(strWhere);
        }
        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            return dal.GetList(Top, strWhere, filedOrder);
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<EGMNGS.Model.InfoDevChangeFill> GetModelList(string strWhere)
        {
            DataSet ds = dal.GetList(strWhere);
            return DataTableToList(ds.Tables[0]);
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<EGMNGS.Model.InfoDevChangeFill> DataTableToList(DataTable dt)
        {
            List<EGMNGS.Model.InfoDevChangeFill> modelList = new List<EGMNGS.Model.InfoDevChangeFill>();
            int rowsCount = dt.Rows.Count;
            if (rowsCount > 0)
            {
                EGMNGS.Model.InfoDevChangeFill model;
                for (int n = 0; n < rowsCount; n++)
                {
                    model = new EGMNGS.Model.InfoDevChangeFill();
                    if (dt.Rows[n]["OID"] != null && dt.Rows[n]["OID"].ToString() != "")
                    {
                        model.OID = int.Parse(dt.Rows[n]["OID"].ToString());
                    }
                    if (dt.Rows[n]["DevCode"] != null && dt.Rows[n]["DevCode"].ToString() != "")
                    {
                        model.DevCode = dt.Rows[n]["DevCode"].ToString();
                    }
                    if (dt.Rows[n]["DevName"] != null && dt.Rows[n]["DevName"].ToString() != "")
                    {
                        model.DevName = dt.Rows[n]["DevName"].ToString();
                    }
                    if (dt.Rows[n]["DevType"] != null && dt.Rows[n]["DevType"].ToString() != "")
                    {
                        model.DevType = dt.Rows[n]["DevType"].ToString();
                    }
                    if (dt.Rows[n]["RatedQL"] != null && dt.Rows[n]["RatedQL"].ToString() != "")
                    {
                        model.RatedQL = decimal.Parse(dt.Rows[n]["RatedQL"].ToString());
                    }
                    if (dt.Rows[n]["RatedQY"] != null && dt.Rows[n]["RatedQY"].ToString() != "")
                    {
                        model.RatedQY = decimal.Parse(dt.Rows[n]["RatedQY"].ToString());
                    }
                    if (dt.Rows[n]["FillGasdate"] != null && dt.Rows[n]["FillGasdate"].ToString() != "")
                    {
                        model.FillGasdate = DateTime.Parse(dt.Rows[n]["FillGasdate"].ToString());
                    }
                    if (dt.Rows[n]["Operataff"] != null && dt.Rows[n]["Operataff"].ToString() != "")
                    {
                        model.Operataff = dt.Rows[n]["Operataff"].ToString();
                    }
                    if (dt.Rows[n]["GasUsing"] != null && dt.Rows[n]["GasUsing"].ToString() != "")
                    {
                        model.GasUsing = dt.Rows[n]["GasUsing"].ToString();
                    }
                    if (dt.Rows[n]["AmountFillGas"] != null && dt.Rows[n]["AmountFillGas"].ToString() != "")
                    {
                        model.AmountFillGas = decimal.Parse(dt.Rows[n]["AmountFillGas"].ToString());
                    }
                    if (dt.Rows[n]["AmountPressFillGassbefore"] != null && dt.Rows[n]["AmountPressFillGassbefore"].ToString() != "")
                    {
                        model.AmountPressFillGassbefore = decimal.Parse(dt.Rows[n]["AmountPressFillGassbefore"].ToString());
                    }
                    if (dt.Rows[n]["AmountPressFillGassafter"] != null && dt.Rows[n]["AmountPressFillGassafter"].ToString() != "")
                    {
                        model.AmountPressFillGassafter = decimal.Parse(dt.Rows[n]["AmountPressFillGassafter"].ToString());
                    }
                    if (dt.Rows[n]["RemarksDesc"] != null && dt.Rows[n]["RemarksDesc"].ToString() != "")
                    {
                        model.RemarksDesc = dt.Rows[n]["RemarksDesc"].ToString();
                    }
                    if (dt.Rows[n]["CylinderCode"] != null && dt.Rows[n]["CylinderCode"].ToString() != "")
                    {
                        model.CylinderCode = dt.Rows[n]["CylinderCode"].ToString();
                    }
                    if (dt.Rows[n]["GasCode"] != null && dt.Rows[n]["GasCode"].ToString() != "")
                    {
                        model.GasCode = dt.Rows[n]["GasCode"].ToString();
                    }
                    if (dt.Rows[n]["Status"] != null && dt.Rows[n]["Status"].ToString() != "")
                    {
                        model.Status = dt.Rows[n]["Status"].ToString();
                    }
                    if (dt.Rows[n]["RegistantsOID"] != null && dt.Rows[n]["RegistantsOID"].ToString() != "")
                    {
                        model.RegistantsOID = dt.Rows[n]["RegistantsOID"].ToString();
                    }
                    if (dt.Rows[n]["RegistantsDate"] != null && dt.Rows[n]["RegistantsDate"].ToString() != "")
                    {
                        model.RegistantsDate = DateTime.Parse(dt.Rows[n]["RegistantsDate"].ToString());
                    }
                    if (dt.Rows[n]["PowerSupplyCode"] != null && dt.Rows[n]["PowerSupplyCode"].ToString() != "")
                    {
                        model.PowerSupplyCode = dt.Rows[n]["PowerSupplyCode"].ToString();
                    }
                    if (dt.Rows[n]["PowerSupplyName"] != null && dt.Rows[n]["PowerSupplyName"].ToString() != "")
                    {
                        model.PowerSupplyName = dt.Rows[n]["PowerSupplyName"].ToString();
                    }
                    if (dt.Rows[n]["ConvertStationCode"] != null && dt.Rows[n]["ConvertStationCode"].ToString() != "")
                    {
                        model.ConvertStationCode = dt.Rows[n]["ConvertStationCode"].ToString();
                    }
                    if (dt.Rows[n]["ConvertStationName"] != null && dt.Rows[n]["ConvertStationName"].ToString() != "")
                    {
                        model.ConvertStationName = dt.Rows[n]["ConvertStationName"].ToString();
                    }
                    modelList.Add(model);
                }
            }
            return modelList;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetAllList()
        {
            return GetList("");
        }

        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            return dal.GetRecordCount(strWhere);
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            return dal.GetListByPage(strWhere, orderby, startIndex, endIndex);
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        //public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        //{
        //return dal.GetList(PageSize,PageIndex,strWhere);
        //}

        #endregion  Method
    }
}


﻿using System;
using System.Data;
using System.Collections.Generic;
using Maticsoft.Common;
using EGMNGS.Model;
namespace EGMNGS.BLL
{
	/// <summary>
	/// 查看3种气体的实时库存数量和历史库存数量等信息
	/// </summary>
	public partial class BookGasStorage
	{
		private readonly EGMNGS.DAL.BookGasStorage dal=new EGMNGS.DAL.BookGasStorage();
		public BookGasStorage()
		{}
		#region  Method

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
			return dal.GetMaxId();
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int OID)
		{
			return dal.Exists(OID);
		}

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int  Add(EGMNGS.Model.BookGasStorage model)
		{
			return dal.Add(model);
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(EGMNGS.Model.BookGasStorage model)
		{
			return dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int OID)
		{
			
			return dal.Delete(OID);
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool DeleteList(string OIDlist )
		{
			return dal.DeleteList(OIDlist );
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public EGMNGS.Model.BookGasStorage GetModel(int OID)
		{
			
			return dal.GetModel(OID);
		}

		/// <summary>
		/// 得到一个对象实体，从缓存中
		/// </summary>
		public EGMNGS.Model.BookGasStorage GetModelByCache(int OID)
		{
			
			string CacheKey = "BookGasStorageModel-" + OID;
			object objModel = Maticsoft.Common.DataCache.GetCache(CacheKey);
			if (objModel == null)
			{
				try
				{
					objModel = dal.GetModel(OID);
					if (objModel != null)
					{
						int ModelCache = Maticsoft.Common.ConfigHelper.GetConfigInt("ModelCache");
						Maticsoft.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
					}
				}
				catch{}
			}
			return (EGMNGS.Model.BookGasStorage)objModel;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			return dal.GetList(Top,strWhere,filedOrder);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<EGMNGS.Model.BookGasStorage> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<EGMNGS.Model.BookGasStorage> DataTableToList(DataTable dt)
		{
			List<EGMNGS.Model.BookGasStorage> modelList = new List<EGMNGS.Model.BookGasStorage>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				EGMNGS.Model.BookGasStorage model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = new EGMNGS.Model.BookGasStorage();
					if(dt.Rows[n]["OID"]!=null && dt.Rows[n]["OID"].ToString()!="")
					{
						model.OID=int.Parse(dt.Rows[n]["OID"].ToString());
					}
					if(dt.Rows[n]["Year"]!=null && dt.Rows[n]["Year"].ToString()!="")
					{
					model.Year=dt.Rows[n]["Year"].ToString();
					}
					if(dt.Rows[n]["Month"]!=null && dt.Rows[n]["Month"].ToString()!="")
					{
					model.Month=dt.Rows[n]["Month"].ToString();
					}
					if(dt.Rows[n]["CountRecycleGas"]!=null && dt.Rows[n]["CountRecycleGas"].ToString()!="")
					{
						model.CountRecycleGas=decimal.Parse(dt.Rows[n]["CountRecycleGas"].ToString());
					}
					if(dt.Rows[n]["CountWaitingGas"]!=null && dt.Rows[n]["CountWaitingGas"].ToString()!="")
					{
						model.CountWaitingGas=decimal.Parse(dt.Rows[n]["CountWaitingGas"].ToString());
					}
					if(dt.Rows[n]["CountPassGas"]!=null && dt.Rows[n]["CountPassGas"].ToString()!="")
					{
						model.CountPassGas=decimal.Parse(dt.Rows[n]["CountPassGas"].ToString());
					}
					if(dt.Rows[n]["CheckStatus"]!=null && dt.Rows[n]["CheckStatus"].ToString()!="")
					{
					model.CheckStatus=dt.Rows[n]["CheckStatus"].ToString();
					}
					if(dt.Rows[n]["CheckOID"]!=null && dt.Rows[n]["CheckOID"].ToString()!="")
					{
					model.CheckOID=dt.Rows[n]["CheckOID"].ToString();
					}
					if(dt.Rows[n]["CheckDate"]!=null && dt.Rows[n]["CheckDate"].ToString()!="")
					{
						model.CheckDate=DateTime.Parse(dt.Rows[n]["CheckDate"].ToString());
					}
					if(dt.Rows[n]["CREATED_BY"]!=null && dt.Rows[n]["CREATED_BY"].ToString()!="")
					{
					model.CREATED_BY=dt.Rows[n]["CREATED_BY"].ToString();
					}
					if(dt.Rows[n]["CREATED_DATE"]!=null && dt.Rows[n]["CREATED_DATE"].ToString()!="")
					{
						model.CREATED_DATE=DateTime.Parse(dt.Rows[n]["CREATED_DATE"].ToString());
					}
					if(dt.Rows[n]["LAST_UPD_BY"]!=null && dt.Rows[n]["LAST_UPD_BY"].ToString()!="")
					{
					model.LAST_UPD_BY=dt.Rows[n]["LAST_UPD_BY"].ToString();
					}
					if(dt.Rows[n]["LAST_UPD_DATE"]!=null && dt.Rows[n]["LAST_UPD_DATE"].ToString()!="")
					{
						model.LAST_UPD_DATE=DateTime.Parse(dt.Rows[n]["LAST_UPD_DATE"].ToString());
					}
					modelList.Add(model);
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			return dal.GetRecordCount(strWhere);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			return dal.GetListByPage( strWhere,  orderby,  startIndex,  endIndex);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		//public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		//{
			//return dal.GetList(PageSize,PageIndex,strWhere);
		//}

		#endregion  Method
	}
}


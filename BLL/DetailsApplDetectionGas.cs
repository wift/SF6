﻿using System;
using System.Data;
using System.Collections.Generic;
using Maticsoft.Common;
using EGMNGS.Model;
namespace EGMNGS.BLL
{
	/// <summary>
	/// 检测气体明细表
	/// </summary>
	public partial class DetailsApplDetectionGas
	{
		private readonly EGMNGS.DAL.DetailsApplDetectionGas dal=new EGMNGS.DAL.DetailsApplDetectionGas();
		public DetailsApplDetectionGas()
		{}
		#region  Method

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
			return dal.GetMaxId();
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int OID)
		{
			return dal.Exists(OID);
		}

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int  Add(EGMNGS.Model.DetailsApplDetectionGas model)
		{
			return dal.Add(model);
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(EGMNGS.Model.DetailsApplDetectionGas model)
		{
			return dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int OID)
		{
			
			return dal.Delete(OID);
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool DeleteList(string OIDlist )
		{
			return dal.DeleteList(OIDlist );
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public EGMNGS.Model.DetailsApplDetectionGas GetModel(int OID)
		{
			
			return dal.GetModel(OID);
		}

		/// <summary>
		/// 得到一个对象实体，从缓存中
		/// </summary>
		public EGMNGS.Model.DetailsApplDetectionGas GetModelByCache(int OID)
		{
			
			string CacheKey = "DetailsApplDetectionGasModel-" + OID;
			object objModel = Maticsoft.Common.DataCache.GetCache(CacheKey);
			if (objModel == null)
			{
				try
				{
					objModel = dal.GetModel(OID);
					if (objModel != null)
					{
						int ModelCache = Maticsoft.Common.ConfigHelper.GetConfigInt("ModelCache");
						Maticsoft.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
					}
				}
				catch{}
			}
			return (EGMNGS.Model.DetailsApplDetectionGas)objModel;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			return dal.GetList(Top,strWhere,filedOrder);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<EGMNGS.Model.DetailsApplDetectionGas> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<EGMNGS.Model.DetailsApplDetectionGas> DataTableToList(DataTable dt)
		{
			List<EGMNGS.Model.DetailsApplDetectionGas> modelList = new List<EGMNGS.Model.DetailsApplDetectionGas>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				EGMNGS.Model.DetailsApplDetectionGas model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = new EGMNGS.Model.DetailsApplDetectionGas();
					if(dt.Rows[n]["OID"]!=null && dt.Rows[n]["OID"].ToString()!="")
					{
						model.OID=int.Parse(dt.Rows[n]["OID"].ToString());
					}
					if(dt.Rows[n]["ApplCode"]!=null && dt.Rows[n]["ApplCode"].ToString()!="")
					{
					model.ApplCode=dt.Rows[n]["ApplCode"].ToString();
					}
					if(dt.Rows[n]["CylinderCode"]!=null && dt.Rows[n]["CylinderCode"].ToString()!="")
					{
					model.CylinderCode=dt.Rows[n]["CylinderCode"].ToString();
					}
					if(dt.Rows[n]["GasCode"]!=null && dt.Rows[n]["GasCode"].ToString()!="")
					{
					model.GasCode=dt.Rows[n]["GasCode"].ToString();
					}
					if(dt.Rows[n]["AmountGas"]!=null && dt.Rows[n]["AmountGas"].ToString()!="")
					{
						model.AmountGas=decimal.Parse(dt.Rows[n]["AmountGas"].ToString());
					}
					if(dt.Rows[n]["IsPass"]!=null && dt.Rows[n]["IsPass"].ToString()!="")
					{
					model.IsPass=dt.Rows[n]["IsPass"].ToString();
					}
					if(dt.Rows[n]["CheckDate"]!=null && dt.Rows[n]["CheckDate"].ToString()!="")
					{
						model.CheckDate=DateTime.Parse(dt.Rows[n]["CheckDate"].ToString());
					}
					if(dt.Rows[n]["Status"]!=null && dt.Rows[n]["Status"].ToString()!="")
					{
					model.Status=dt.Rows[n]["Status"].ToString();
					}

                    if (dt.Rows[n]["ProduceNum"] != null && dt.Rows[n]["ProduceNum"].ToString() != "")
                    {
                        model.ProduceNum = dt.Rows[n]["ProduceNum"].ToString();
                    }

                    if (dt.Rows[n]["Manufacturer"] != null && dt.Rows[n]["Manufacturer"].ToString() != "")
                    {
                        model.Manufacturer = dt.Rows[n]["Manufacturer"].ToString();
                    }

                 
					modelList.Add(model);
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			return dal.GetRecordCount(strWhere);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			return dal.GetListByPage( strWhere,  orderby,  startIndex,  endIndex);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		//public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		//{
			//return dal.GetList(PageSize,PageIndex,strWhere);
		//}

		#endregion  Method
	}
}


﻿using System;
using System.Data;
using System.Collections.Generic;
using Maticsoft.Common;
using EGMNGS.Model;
namespace EGMNGS.BLL
{
    /// <summary>
    /// 气体出库明细表
    /// </summary>
    public partial class DetailsGasOutStorage
    {
        private readonly EGMNGS.DAL.DetailsGasOutStorage dal = new EGMNGS.DAL.DetailsGasOutStorage();
        public DetailsGasOutStorage()
        { }
        #region  Method

        /// <summary>
        /// 得到最大ID
        /// </summary>
        public int GetMaxId()
        {
            return dal.GetMaxId();
        }

        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int OID)
        {
            return dal.Exists(OID);
        }

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(EGMNGS.Model.DetailsGasOutStorage model)
        {
            // if (GetRecordCount(string.Format("GasCode='{0}' and TableGasOutStorage_Code='{1}'", model.GasCode,model.TableGasOutStorage_Code)) >= 1)
            //            if (GetRecordCount(string.Format(@"GasCode='{0}' and exists(select * from TableGasOutStorage t 
            //where TableGasOutStorage_Code=t.Code and t.GasClass='3') ", model.GasCode)) >= 1)
            //            {
            //                return 0;
            //            }
            return dal.Add(model);
        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(EGMNGS.Model.DetailsGasOutStorage model)
        {
            return dal.Update(model);
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int OID)
        {

            return dal.Delete(OID);
        }
        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool DeleteList(string OIDlist)
        {
            return dal.DeleteList(OIDlist);
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public EGMNGS.Model.DetailsGasOutStorage GetModel(int OID)
        {

            return dal.GetModel(OID);
        }

        /// <summary>
        /// 得到一个对象实体，从缓存中
        /// </summary>
        public EGMNGS.Model.DetailsGasOutStorage GetModelByCache(int OID)
        {

            string CacheKey = "DetailsGasOutStorageModel-" + OID;
            object objModel = Maticsoft.Common.DataCache.GetCache(CacheKey);
            if (objModel == null)
            {
                try
                {
                    objModel = dal.GetModel(OID);
                    if (objModel != null)
                    {
                        int ModelCache = Maticsoft.Common.ConfigHelper.GetConfigInt("ModelCache");
                        Maticsoft.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
                    }
                }
                catch { }
            }
            return (EGMNGS.Model.DetailsGasOutStorage)objModel;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            return dal.GetList(strWhere);
        }
        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            return dal.GetList(Top, strWhere, filedOrder);
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<EGMNGS.Model.DetailsGasOutStorage> GetModelList(string strWhere)
        {
            DataSet ds = dal.GetList(strWhere);
            return DataTableToList(ds.Tables[0]);
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<EGMNGS.Model.DetailsGasOutStorage> DataTableToList(DataTable dt)
        {
            List<EGMNGS.Model.DetailsGasOutStorage> modelList = new List<EGMNGS.Model.DetailsGasOutStorage>();
            int rowsCount = dt.Rows.Count;
            if (rowsCount > 0)
            {
                EGMNGS.Model.DetailsGasOutStorage model;
                for (int n = 0; n < rowsCount; n++)
                {
                    model = new EGMNGS.Model.DetailsGasOutStorage();
                    if (dt.Rows[n]["OID"] != null && dt.Rows[n]["OID"].ToString() != "")
                    {
                        model.OID = int.Parse(dt.Rows[n]["OID"].ToString());
                    }
                    if (dt.Rows[n]["GasCode"] != null && dt.Rows[n]["GasCode"].ToString() != "")
                    {
                        model.GasCode = dt.Rows[n]["GasCode"].ToString();
                    }
                    if (dt.Rows[n]["CylinderCode"] != null && dt.Rows[n]["CylinderCode"].ToString() != "")
                    {
                        model.CylinderCode = dt.Rows[n]["CylinderCode"].ToString();
                    }
                    if (dt.Rows[n]["CylinderStatus"] != null && dt.Rows[n]["CylinderStatus"].ToString() != "")
                    {
                        model.CylinderStatus = dt.Rows[n]["CylinderStatus"].ToString();
                    }
                    if (dt.Rows[n]["IsPass"] != null && dt.Rows[n]["IsPass"].ToString() != "")
                    {
                        model.IsPass = dt.Rows[n]["IsPass"].ToString();
                    }
                    if (dt.Rows[n]["AmountGas"] != null && dt.Rows[n]["AmountGas"].ToString() != "")
                    {
                        model.AmountGas = decimal.Parse(dt.Rows[n]["AmountGas"].ToString());
                    }
                    if (dt.Rows[n]["CREATED_BY"] != null && dt.Rows[n]["CREATED_BY"].ToString() != "")
                    {
                        model.CREATED_BY = dt.Rows[n]["CREATED_BY"].ToString();
                    }
                    if (dt.Rows[n]["CREATED_DATE"] != null && dt.Rows[n]["CREATED_DATE"].ToString() != "")
                    {
                        model.CREATED_DATE = DateTime.Parse(dt.Rows[n]["CREATED_DATE"].ToString());
                    }
                    if (dt.Rows[n]["LAST_UPD_BY"] != null && dt.Rows[n]["LAST_UPD_BY"].ToString() != "")
                    {
                        model.LAST_UPD_BY = dt.Rows[n]["LAST_UPD_BY"].ToString();
                    }
                    if (dt.Rows[n]["LAST_UPD_DATE"] != null && dt.Rows[n]["LAST_UPD_DATE"].ToString() != "")
                    {
                        model.LAST_UPD_DATE = DateTime.Parse(dt.Rows[n]["LAST_UPD_DATE"].ToString());
                    }
                    modelList.Add(model);
                }
            }
            return modelList;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetAllList()
        {
            return GetList("");
        }

        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            return dal.GetRecordCount(strWhere);
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            return dal.GetListByPage(strWhere, orderby, startIndex, endIndex);
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        //public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        //{
        //return dal.GetList(PageSize,PageIndex,strWhere);
        //}

        #endregion  Method
    }
}


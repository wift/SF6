﻿using System;
using System.Data;
using System.Collections.Generic;
using Maticsoft.Common;
using EGMNGS.Model;
namespace EGMNGS.BLL
{
	/// <summary>
	/// 采购员登记新采购气体信息、钢瓶信息及气体编码
	/// </summary>
	public partial class SYS_USER_PWD
	{
		private readonly EGMNGS.DAL.SYS_USER_PWD dal=new EGMNGS.DAL.SYS_USER_PWD();
		public SYS_USER_PWD()
		{}
		#region  Method
		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(string USER_ID,DateTime EXPIRED_DATE)
		{
			return dal.Exists(USER_ID,EXPIRED_DATE);
		}

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(EGMNGS.Model.SYS_USER_PWD model)
		{
			return dal.Add(model);
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(EGMNGS.Model.SYS_USER_PWD model)
		{
			return dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(string USER_ID,DateTime EXPIRED_DATE)
		{
			
			return dal.Delete(USER_ID,EXPIRED_DATE);
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public EGMNGS.Model.SYS_USER_PWD GetModel(string USER_ID,DateTime EXPIRED_DATE)
		{
			
			return dal.GetModel(USER_ID,EXPIRED_DATE);
		}

		/// <summary>
		/// 得到一个对象实体，从缓存中
		/// </summary>
		public EGMNGS.Model.SYS_USER_PWD GetModelByCache(string USER_ID,DateTime EXPIRED_DATE)
		{
			
			string CacheKey = "SYS_USER_PWDModel-" + USER_ID+EXPIRED_DATE;
			object objModel = Maticsoft.Common.DataCache.GetCache(CacheKey);
			if (objModel == null)
			{
				try
				{
					objModel = dal.GetModel(USER_ID,EXPIRED_DATE);
					if (objModel != null)
					{
						int ModelCache = Maticsoft.Common.ConfigHelper.GetConfigInt("ModelCache");
						Maticsoft.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
					}
				}
				catch{}
			}
			return (EGMNGS.Model.SYS_USER_PWD)objModel;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			return dal.GetList(Top,strWhere,filedOrder);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<EGMNGS.Model.SYS_USER_PWD> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<EGMNGS.Model.SYS_USER_PWD> DataTableToList(DataTable dt)
		{
			List<EGMNGS.Model.SYS_USER_PWD> modelList = new List<EGMNGS.Model.SYS_USER_PWD>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				EGMNGS.Model.SYS_USER_PWD model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = new EGMNGS.Model.SYS_USER_PWD();
					if(dt.Rows[n]["USER_ID"]!=null && dt.Rows[n]["USER_ID"].ToString()!="")
					{
					model.USER_ID=dt.Rows[n]["USER_ID"].ToString();
					}
					if(dt.Rows[n]["EXPIRED_DATE"]!=null && dt.Rows[n]["EXPIRED_DATE"].ToString()!="")
					{
						model.EXPIRED_DATE=DateTime.Parse(dt.Rows[n]["EXPIRED_DATE"].ToString());
					}
					if(dt.Rows[n]["USER_PWD"]!=null && dt.Rows[n]["USER_PWD"].ToString()!="")
					{
					model.USER_PWD=dt.Rows[n]["USER_PWD"].ToString();
					}
					modelList.Add(model);
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			return dal.GetRecordCount(strWhere);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			return dal.GetListByPage( strWhere,  orderby,  startIndex,  endIndex);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		//public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		//{
			//return dal.GetList(PageSize,PageIndex,strWhere);
		//}

		#endregion  Method
	}
}


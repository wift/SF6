using System;
using System.Data;
using System.Collections.Generic;
using EGMNGS.Common;
using EGMNGS.Model;
namespace EGMNGS.BLL
{
	/// <summary>
	/// 业务逻辑类ApplRegGasQtyInspectionValue 的摘要说明。
	/// </summary>
	public class ApplRegGasQtyInspectionValue
	{
		private readonly EGMNGS.DAL.ApplRegGasQtyInspectionValue dal=new EGMNGS.DAL.ApplRegGasQtyInspectionValue();
		public ApplRegGasQtyInspectionValue()
		{}
		#region  成员方法

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
			return dal.GetMaxId();
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int OID)
		{
			return dal.Exists(OID);
		}

		/// <summary>
		/// 增加一条数据
		/// </summary>
        public int Add(EGMNGS.Model.ApplRegGasQtyInspectionValue model)
		{
			return dal.Add(model);
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public void Update(EGMNGS.Model.ApplRegGasQtyInspectionValue model)
		{
			dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public void Delete(int OID)
		{
			
			dal.Delete(OID);
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public EGMNGS.Model.ApplRegGasQtyInspectionValue GetModel(int OID)
		{
			
			return dal.GetModel(OID);
		}

		/// <summary>
		/// 得到一个对象实体，从缓存中。
		/// </summary>
		public EGMNGS.Model.ApplRegGasQtyInspectionValue GetModelByCache(int OID)
		{
			
			string CacheKey = "ApplRegGasQtyInspectionValueModel-" + OID;
            object objModel = Maticsoft.Common.DataCache.GetCache(CacheKey);
			if (objModel == null)
			{
				try
				{
					objModel = dal.GetModel(OID);
					if (objModel != null)
					{
                        int ModelCache = Maticsoft.Common.ConfigHelper.GetConfigInt("ModelCache");
                        Maticsoft.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
					}
				}
				catch{}
			}
			return (EGMNGS.Model.ApplRegGasQtyInspectionValue)objModel;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			return dal.GetList(Top,strWhere,filedOrder);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<EGMNGS.Model.ApplRegGasQtyInspectionValue> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<EGMNGS.Model.ApplRegGasQtyInspectionValue> DataTableToList(DataTable dt)
		{
			List<EGMNGS.Model.ApplRegGasQtyInspectionValue> modelList = new List<EGMNGS.Model.ApplRegGasQtyInspectionValue>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				EGMNGS.Model.ApplRegGasQtyInspectionValue model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = new EGMNGS.Model.ApplRegGasQtyInspectionValue();
					if(dt.Rows[n]["OID"].ToString()!="")
					{
						model.OID=int.Parse(dt.Rows[n]["OID"].ToString());
					}
					model.CheckCode=dt.Rows[n]["CheckCode"].ToString();
					if(dt.Rows[n]["EnvironmentTemperature"].ToString()!="")
					{
						model.EnvironmentTemperature=decimal.Parse(dt.Rows[n]["EnvironmentTemperature"].ToString());
					}
					if(dt.Rows[n]["EnvironmentHumidity"].ToString()!="")
					{
						model.EnvironmentHumidity=decimal.Parse(dt.Rows[n]["EnvironmentHumidity"].ToString());
					}
					if(dt.Rows[n]["BarometricPressure"].ToString()!="")
					{
						model.BarometricPressure=decimal.Parse(dt.Rows[n]["BarometricPressure"].ToString());
					}
					model.Pipe1=dt.Rows[n]["Pipe1"].ToString();
					model.Pipe2=dt.Rows[n]["Pipe2"].ToString();
					model.Pipe3=dt.Rows[n]["Pipe3"].ToString();
					model.Pipe4=dt.Rows[n]["Pipe4"].ToString();
					model.Pipe5=dt.Rows[n]["Pipe5"].ToString();
					model.Pipe6=dt.Rows[n]["Pipe6"].ToString();
					model.Pipe7=dt.Rows[n]["Pipe7"].ToString();
					model.Pipe8=dt.Rows[n]["Pipe8"].ToString();
					modelList.Add(model);
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		//public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		//{
			//return dal.GetList(PageSize,PageIndex,strWhere);
		//}

		#endregion  成员方法
	}
}


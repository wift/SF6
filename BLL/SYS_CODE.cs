﻿using System;
using System.Data;
using System.Collections.Generic;
using Maticsoft.Common;
using EGMNGS.Model;
namespace EGMNGS.BLL
{
    /// <summary>
    /// 采购员登记新采购气体信息、钢瓶信息及气体编码
    /// </summary>
    public partial class SYS_CODE
    {
        private readonly EGMNGS.DAL.SYS_CODE dal = new EGMNGS.DAL.SYS_CODE();
        public SYS_CODE()
        { }
        #region  Method
        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(string CODE_TYPE, string CODE, string SUBCODE)
        {
            return dal.Exists(CODE_TYPE, CODE, SUBCODE);
        }

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public bool Add(EGMNGS.Model.SYS_CODE model)
        {
            return dal.Add(model);
        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(EGMNGS.Model.SYS_CODE model)
        {
            return dal.Update(model);
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(string CODE_TYPE, string CODE, string SUBCODE)
        {

            return dal.Delete(CODE_TYPE, CODE, SUBCODE);
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public EGMNGS.Model.SYS_CODE GetModel(string CODE_TYPE, string CODE, string SUBCODE)
        {

            return dal.GetModel(CODE_TYPE, CODE, SUBCODE);
        }

        /// <summary>
        /// 得到一个对象实体，从缓存中
        /// </summary>
        public EGMNGS.Model.SYS_CODE GetModelByCache(string CODE_TYPE, string CODE, string SUBCODE)
        {

            string CacheKey = "SYS_CODEModel-" + CODE_TYPE + CODE + SUBCODE;
            object objModel = Maticsoft.Common.DataCache.GetCache(CacheKey);
            if (objModel == null)
            {
                try
                {
                    objModel = dal.GetModel(CODE_TYPE, CODE, SUBCODE);
                    if (objModel != null)
                    {
                        int ModelCache = Maticsoft.Common.ConfigHelper.GetConfigInt("ModelCache");
                        Maticsoft.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
                    }
                }
                catch { }
            }
            return (EGMNGS.Model.SYS_CODE)objModel;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            return dal.GetList(strWhere);
        }
        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            return dal.GetList(Top, strWhere, filedOrder);
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<EGMNGS.Model.SYS_CODE> GetModelList(string strWhere)
        {
            DataSet ds = dal.GetList(strWhere);
            return DataTableToList(ds.Tables[0]);
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<EGMNGS.Model.SYS_CODE> DataTableToList(DataTable dt)
        {
            List<EGMNGS.Model.SYS_CODE> modelList = new List<EGMNGS.Model.SYS_CODE>();
            int rowsCount = dt.Rows.Count;
            if (rowsCount > 0)
            {
                EGMNGS.Model.SYS_CODE model;
                for (int n = 0; n < rowsCount; n++)
                {
                    model = new EGMNGS.Model.SYS_CODE();
                    if (dt.Rows[n]["CODE_TYPE"] != null && dt.Rows[n]["CODE_TYPE"].ToString() != "")
                    {
                        model.CODE_TYPE = dt.Rows[n]["CODE_TYPE"].ToString();
                    }
                    if (dt.Rows[n]["CODE"] != null && dt.Rows[n]["CODE"].ToString() != "")
                    {
                        model.CODE = dt.Rows[n]["CODE"].ToString();
                    }
                    if (dt.Rows[n]["SUBCODE"] != null && dt.Rows[n]["SUBCODE"].ToString() != "")
                    {
                        model.SUBCODE = dt.Rows[n]["SUBCODE"].ToString();
                    }
                    if (dt.Rows[n]["CODE_ENG_DESC"] != null && dt.Rows[n]["CODE_ENG_DESC"].ToString() != "")
                    {
                        model.CODE_ENG_DESC = dt.Rows[n]["CODE_ENG_DESC"].ToString();
                    }
                    if (dt.Rows[n]["CODE_CHI_DESC"] != null && dt.Rows[n]["CODE_CHI_DESC"].ToString() != "")
                    {
                        model.CODE_CHI_DESC = dt.Rows[n]["CODE_CHI_DESC"].ToString();
                    }
                    if (dt.Rows[n]["SHORT_DESC"] != null && dt.Rows[n]["SHORT_DESC"].ToString() != "")
                    {
                        model.SHORT_DESC = dt.Rows[n]["SHORT_DESC"].ToString();
                    }
                    if (dt.Rows[n]["DISPLAY_ORDER"] != null && dt.Rows[n]["DISPLAY_ORDER"].ToString() != "")
                    {
                        model.DISPLAY_ORDER = decimal.Parse(dt.Rows[n]["DISPLAY_ORDER"].ToString());
                    }
                    if (dt.Rows[n]["MISC_IND"] != null && dt.Rows[n]["MISC_IND"].ToString() != "")
                    {
                        model.MISC_IND = dt.Rows[n]["MISC_IND"].ToString();
                    }
                    if (dt.Rows[n]["DEPART_CODE"] != null && dt.Rows[n]["DEPART_CODE"].ToString() != "")
                    {
                        model.DEPART_CODE = dt.Rows[n]["DEPART_CODE"].ToString();
                    }
                    if (dt.Rows[n]["CREATED_BY"] != null && dt.Rows[n]["CREATED_BY"].ToString() != "")
                    {
                        model.CREATED_BY = dt.Rows[n]["CREATED_BY"].ToString();
                    }
                    if (dt.Rows[n]["CREATED_DATE"] != null && dt.Rows[n]["CREATED_DATE"].ToString() != "")
                    {
                        model.CREATED_DATE = DateTime.Parse(dt.Rows[n]["CREATED_DATE"].ToString());
                    }
                    if (dt.Rows[n]["LAST_UPD_BY"] != null && dt.Rows[n]["LAST_UPD_BY"].ToString() != "")
                    {
                        model.LAST_UPD_BY = dt.Rows[n]["LAST_UPD_BY"].ToString();
                    }
                    if (dt.Rows[n]["LAST_UPD_DATE"] != null && dt.Rows[n]["LAST_UPD_DATE"].ToString() != "")
                    {
                        model.LAST_UPD_DATE = DateTime.Parse(dt.Rows[n]["LAST_UPD_DATE"].ToString());
                    }
                    modelList.Add(model);
                }
            }
            return modelList;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetAllList()
        {
            return GetList("");
        }

        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            return dal.GetRecordCount(strWhere);
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            return dal.GetListByPage(strWhere, orderby, startIndex, endIndex);
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize, int PageIndex, string strWhere)
        {
            return dal.GetList(PageSize, PageIndex, strWhere);
        }

        #endregion  Method
    }
}


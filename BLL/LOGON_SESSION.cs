﻿using System;
using System.Data;
using System.Collections.Generic;
using Maticsoft.Common;
using EGMNGS.Model;
namespace EGMNGS.BLL
{
	/// <summary>
	/// 登记各供电局的变电站
	/// </summary>
	public partial class LOGON_SESSION
	{
		private readonly EGMNGS.DAL.LOGON_SESSION dal=new EGMNGS.DAL.LOGON_SESSION();
		public LOGON_SESSION()
		{}
		#region  Method
		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(DateTime LOGON_DATE,string USER_ID)
		{
			return dal.Exists(LOGON_DATE,USER_ID);
		}

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(EGMNGS.Model.LOGON_SESSION model)
		{
			return dal.Add(model);
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(EGMNGS.Model.LOGON_SESSION model)
		{
			return dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(DateTime LOGON_DATE,string USER_ID)
		{
			
			return dal.Delete(LOGON_DATE,USER_ID);
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public EGMNGS.Model.LOGON_SESSION GetModel(DateTime LOGON_DATE,string USER_ID)
		{
			
			return dal.GetModel(LOGON_DATE,USER_ID);
		}

		/// <summary>
		/// 得到一个对象实体，从缓存中
		/// </summary>
		public EGMNGS.Model.LOGON_SESSION GetModelByCache(DateTime LOGON_DATE,string USER_ID)
		{
			
			string CacheKey = "LOGON_SESSIONModel-" + LOGON_DATE+USER_ID;
			object objModel = Maticsoft.Common.DataCache.GetCache(CacheKey);
			if (objModel == null)
			{
				try
				{
					objModel = dal.GetModel(LOGON_DATE,USER_ID);
					if (objModel != null)
					{
						int ModelCache = Maticsoft.Common.ConfigHelper.GetConfigInt("ModelCache");
						Maticsoft.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
					}
				}
				catch{}
			}
			return (EGMNGS.Model.LOGON_SESSION)objModel;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			return dal.GetList(Top,strWhere,filedOrder);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<EGMNGS.Model.LOGON_SESSION> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<EGMNGS.Model.LOGON_SESSION> DataTableToList(DataTable dt)
		{
			List<EGMNGS.Model.LOGON_SESSION> modelList = new List<EGMNGS.Model.LOGON_SESSION>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				EGMNGS.Model.LOGON_SESSION model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = new EGMNGS.Model.LOGON_SESSION();
					if(dt.Rows[n]["LOGON_DATE"]!=null && dt.Rows[n]["LOGON_DATE"].ToString()!="")
					{
						model.LOGON_DATE=DateTime.Parse(dt.Rows[n]["LOGON_DATE"].ToString());
					}
					if(dt.Rows[n]["USER_ID"]!=null && dt.Rows[n]["USER_ID"].ToString()!="")
					{
					model.USER_ID=dt.Rows[n]["USER_ID"].ToString();
					}
					if(dt.Rows[n]["LAST_ACTION_DATE"]!=null && dt.Rows[n]["LAST_ACTION_DATE"].ToString()!="")
					{
						model.LAST_ACTION_DATE=DateTime.Parse(dt.Rows[n]["LAST_ACTION_DATE"].ToString());
					}
					if(dt.Rows[n]["LOGOUT_DATE"]!=null && dt.Rows[n]["LOGOUT_DATE"].ToString()!="")
					{
						model.LOGOUT_DATE=DateTime.Parse(dt.Rows[n]["LOGOUT_DATE"].ToString());
					}
					if(dt.Rows[n]["FAIL_REASON"]!=null && dt.Rows[n]["FAIL_REASON"].ToString()!="")
					{
					model.FAIL_REASON=dt.Rows[n]["FAIL_REASON"].ToString();
					}
					if(dt.Rows[n]["SESSION_TIMEOUT_DATE"]!=null && dt.Rows[n]["SESSION_TIMEOUT_DATE"].ToString()!="")
					{
						model.SESSION_TIMEOUT_DATE=DateTime.Parse(dt.Rows[n]["SESSION_TIMEOUT_DATE"].ToString());
					}
					modelList.Add(model);
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			return dal.GetRecordCount(strWhere);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			return dal.GetListByPage( strWhere,  orderby,  startIndex,  endIndex);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		//public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		//{
			//return dal.GetList(PageSize,PageIndex,strWhere);
		//}

		#endregion  Method
	}
}


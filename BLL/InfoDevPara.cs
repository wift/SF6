﻿using System;
using System.Data;
using System.Collections.Generic;
using Maticsoft.Common;
using EGMNGS.Model;
namespace EGMNGS.BLL
{
	/// <summary>
	/// 登记各供电局下属变电
	/// </summary>
	public partial class InfoDevPara
	{
		private readonly EGMNGS.DAL.InfoDevPara dal=new EGMNGS.DAL.InfoDevPara();
		public InfoDevPara()
		{}
		#region  Method

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
			return dal.GetMaxId();
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int OID)
		{
			return dal.Exists(OID);
		}

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int  Add(EGMNGS.Model.InfoDevPara model)
		{
			return dal.Add(model);
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(EGMNGS.Model.InfoDevPara model)
		{
			return dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int OID)
		{
			
			return dal.Delete(OID);
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool DeleteList(string OIDlist )
		{
			return dal.DeleteList(OIDlist );
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public EGMNGS.Model.InfoDevPara GetModel(int OID)
		{
			
			return dal.GetModel(OID);
		}

		/// <summary>
		/// 得到一个对象实体，从缓存中
		/// </summary>
		public EGMNGS.Model.InfoDevPara GetModelByCache(int OID)
		{
			
			string CacheKey = "InfoDevParaModel-" + OID;
			object objModel = Maticsoft.Common.DataCache.GetCache(CacheKey);
			if (objModel == null)
			{
				try
				{
					objModel = dal.GetModel(OID);
					if (objModel != null)
					{
						int ModelCache = Maticsoft.Common.ConfigHelper.GetConfigInt("ModelCache");
						Maticsoft.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
					}
				}
				catch{}
			}
			return (EGMNGS.Model.InfoDevPara)objModel;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			return dal.GetList(Top,strWhere,filedOrder);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<EGMNGS.Model.InfoDevPara> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<EGMNGS.Model.InfoDevPara> DataTableToList(DataTable dt)
		{
			List<EGMNGS.Model.InfoDevPara> modelList = new List<EGMNGS.Model.InfoDevPara>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				EGMNGS.Model.InfoDevPara model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = new EGMNGS.Model.InfoDevPara();
					if(dt.Rows[n]["OID"]!=null && dt.Rows[n]["OID"].ToString()!="")
					{
						model.OID=int.Parse(dt.Rows[n]["OID"].ToString());
					}
					if(dt.Rows[n]["DevCode"]!=null && dt.Rows[n]["DevCode"].ToString()!="")
					{
					model.DevCode=dt.Rows[n]["DevCode"].ToString();
					}
					if(dt.Rows[n]["PowerSupplyCode"]!=null && dt.Rows[n]["PowerSupplyCode"].ToString()!="")
					{
					model.PowerSupplyCode=dt.Rows[n]["PowerSupplyCode"].ToString();
					}
					if(dt.Rows[n]["PowerSupplyName"]!=null && dt.Rows[n]["PowerSupplyName"].ToString()!="")
					{
					model.PowerSupplyName=dt.Rows[n]["PowerSupplyName"].ToString();
					}
					if(dt.Rows[n]["ConvertStationCode"]!=null && dt.Rows[n]["ConvertStationCode"].ToString()!="")
					{
					model.ConvertStationCode=dt.Rows[n]["ConvertStationCode"].ToString();
					}
					if(dt.Rows[n]["ConvertStationName"]!=null && dt.Rows[n]["ConvertStationName"].ToString()!="")
					{
					model.ConvertStationName=dt.Rows[n]["ConvertStationName"].ToString();
					}
					if(dt.Rows[n]["VoltageLevel"]!=null && dt.Rows[n]["VoltageLevel"].ToString()!="")
					{
					model.VoltageLevel=dt.Rows[n]["VoltageLevel"].ToString();
					}
					if(dt.Rows[n]["DevClass"]!=null && dt.Rows[n]["DevClass"].ToString()!="")
					{
					model.DevClass=dt.Rows[n]["DevClass"].ToString();
					}
					if(dt.Rows[n]["RunCode"]!=null && dt.Rows[n]["RunCode"].ToString()!="")
					{
					model.RunCode=dt.Rows[n]["RunCode"].ToString();
					}
					if(dt.Rows[n]["DevName"]!=null && dt.Rows[n]["DevName"].ToString()!="")
					{
					model.DevName=dt.Rows[n]["DevName"].ToString();
					}
					if(dt.Rows[n]["Manufacturer"]!=null && dt.Rows[n]["Manufacturer"].ToString()!="")
					{
					model.Manufacturer=dt.Rows[n]["Manufacturer"].ToString();
					}
					if(dt.Rows[n]["DevType"]!=null && dt.Rows[n]["DevType"].ToString()!="")
					{
					model.DevType=dt.Rows[n]["DevType"].ToString();
					}
					if(dt.Rows[n]["DevNum"]!=null && dt.Rows[n]["DevNum"].ToString()!="")
					{
					model.DevNum=dt.Rows[n]["DevNum"].ToString();
					}
					if(dt.Rows[n]["PurchaseDate"]!=null && dt.Rows[n]["PurchaseDate"].ToString()!="")
					{
						model.PurchaseDate=DateTime.Parse(dt.Rows[n]["PurchaseDate"].ToString());
					}
					if(dt.Rows[n]["PutDate"]!=null && dt.Rows[n]["PutDate"].ToString()!="")
					{
						model.PutDate=DateTime.Parse(dt.Rows[n]["PutDate"].ToString());
					}
					if(dt.Rows[n]["ReturnDate"]!=null && dt.Rows[n]["ReturnDate"].ToString()!="")
					{
						model.ReturnDate=DateTime.Parse(dt.Rows[n]["ReturnDate"].ToString());
					}
					if(dt.Rows[n]["RatedQL"]!=null && dt.Rows[n]["RatedQL"].ToString()!="")
					{
						model.RatedQL=decimal.Parse(dt.Rows[n]["RatedQL"].ToString());
					}
					if(dt.Rows[n]["RatedQY"]!=null && dt.Rows[n]["RatedQY"].ToString()!="")
					{
						model.RatedQY=decimal.Parse(dt.Rows[n]["RatedQY"].ToString());
					}
					if(dt.Rows[n]["Status"]!=null && dt.Rows[n]["Status"].ToString()!="")
					{
					model.Status=dt.Rows[n]["Status"].ToString();
					}
					if(dt.Rows[n]["RegistrantOID"]!=null && dt.Rows[n]["RegistrantOID"].ToString()!="")
					{
					model.RegistrantOID=dt.Rows[n]["RegistrantOID"].ToString();
					}
					if(dt.Rows[n]["RegistrantDate"]!=null && dt.Rows[n]["RegistrantDate"].ToString()!="")
					{
						model.RegistrantDate=DateTime.Parse(dt.Rows[n]["RegistrantDate"].ToString());
					}
					if(dt.Rows[n]["CREATED_BY"]!=null && dt.Rows[n]["CREATED_BY"].ToString()!="")
					{
					model.CREATED_BY=dt.Rows[n]["CREATED_BY"].ToString();
					}
					if(dt.Rows[n]["CREATED_DATE"]!=null && dt.Rows[n]["CREATED_DATE"].ToString()!="")
					{
						model.CREATED_DATE=DateTime.Parse(dt.Rows[n]["CREATED_DATE"].ToString());
					}
					if(dt.Rows[n]["LAST_UPD_BY"]!=null && dt.Rows[n]["LAST_UPD_BY"].ToString()!="")
					{
					model.LAST_UPD_BY=dt.Rows[n]["LAST_UPD_BY"].ToString();
					}
					if(dt.Rows[n]["LAST_UPD_DATE"]!=null && dt.Rows[n]["LAST_UPD_DATE"].ToString()!="")
					{
						model.LAST_UPD_DATE=DateTime.Parse(dt.Rows[n]["LAST_UPD_DATE"].ToString());
					}



                    if (dt.Rows[n]["KV"] != null && dt.Rows[n]["KV"].ToString() != "")
                    {
                        model.KV = int.Parse(dt.Rows[n]["KV"].ToString());
                    }
                    if (dt.Rows[n]["A"] != null && dt.Rows[n]["A"].ToString() != "")
                    {
                        model.A = int.Parse(dt.Rows[n]["A"].ToString());
                    }
                    if (dt.Rows[n]["KA"] != null && dt.Rows[n]["KA"].ToString() != "")
                    {
                        model.KA = int.Parse(dt.Rows[n]["KA"].ToString());
                    }
					modelList.Add(model);
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			return dal.GetRecordCount(strWhere);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			return dal.GetListByPage( strWhere,  orderby,  startIndex,  endIndex);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		//public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		//{
			//return dal.GetList(PageSize,PageIndex,strWhere);
		//}

		#endregion  Method
	}
}


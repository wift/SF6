﻿using System;
using System.Data;
using System.Collections.Generic;
using Maticsoft.Common;
using EGMNGS.Model;
namespace EGMNGS.BLL
{
	/// <summary>
	/// 登记新投产气体品质检
	/// </summary>
	public partial class ApplDetectionGas
	{
		private readonly EGMNGS.DAL.ApplDetectionGas dal=new EGMNGS.DAL.ApplDetectionGas();
		public ApplDetectionGas()
		{}
		#region  Method

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
			return dal.GetMaxId();
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int OID)
		{
			return dal.Exists(OID);
		}

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int  Add(EGMNGS.Model.ApplDetectionGas model)
		{
			return dal.Add(model);
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(EGMNGS.Model.ApplDetectionGas model)
		{
			return dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int OID)
		{
			
			return dal.Delete(OID);
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool DeleteList(string OIDlist )
		{
			return dal.DeleteList(OIDlist );
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public EGMNGS.Model.ApplDetectionGas GetModel(int OID)
		{
			
			return dal.GetModel(OID);
		}

		/// <summary>
		/// 得到一个对象实体，从缓存中
		/// </summary>
		public EGMNGS.Model.ApplDetectionGas GetModelByCache(int OID)
		{
			
			string CacheKey = "ApplDetectionGasModel-" + OID;
			object objModel = Maticsoft.Common.DataCache.GetCache(CacheKey);
			if (objModel == null)
			{
				try
				{
					objModel = dal.GetModel(OID);
					if (objModel != null)
					{
						int ModelCache = Maticsoft.Common.ConfigHelper.GetConfigInt("ModelCache");
						Maticsoft.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
					}
				}
				catch{}
			}
			return (EGMNGS.Model.ApplDetectionGas)objModel;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			return dal.GetList(Top,strWhere,filedOrder);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<EGMNGS.Model.ApplDetectionGas> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<EGMNGS.Model.ApplDetectionGas> DataTableToList(DataTable dt)
		{
			List<EGMNGS.Model.ApplDetectionGas> modelList = new List<EGMNGS.Model.ApplDetectionGas>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				EGMNGS.Model.ApplDetectionGas model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = new EGMNGS.Model.ApplDetectionGas();
					if(dt.Rows[n]["OID"]!=null && dt.Rows[n]["OID"].ToString()!="")
					{
						model.OID=int.Parse(dt.Rows[n]["OID"].ToString());
					}
					if(dt.Rows[n]["ApplCode"]!=null && dt.Rows[n]["ApplCode"].ToString()!="")
					{
					model.ApplCode=dt.Rows[n]["ApplCode"].ToString();
					}
					if(dt.Rows[n]["DevCode"]!=null && dt.Rows[n]["DevCode"].ToString()!="")
					{
					model.DevCode=dt.Rows[n]["DevCode"].ToString();
					}
					if(dt.Rows[n]["PowerSupplyCode"]!=null && dt.Rows[n]["PowerSupplyCode"].ToString()!="")
					{
					model.PowerSupplyCode=dt.Rows[n]["PowerSupplyCode"].ToString();
					}
					if(dt.Rows[n]["PowerSupplyName"]!=null && dt.Rows[n]["PowerSupplyName"].ToString()!="")
					{
					model.PowerSupplyName=dt.Rows[n]["PowerSupplyName"].ToString();
					}
					if(dt.Rows[n]["ConvertStationCode"]!=null && dt.Rows[n]["ConvertStationCode"].ToString()!="")
					{
					model.ConvertStationCode=dt.Rows[n]["ConvertStationCode"].ToString();
					}
					if(dt.Rows[n]["convertStationName"]!=null && dt.Rows[n]["convertStationName"].ToString()!="")
					{
					model.convertStationName=dt.Rows[n]["convertStationName"].ToString();
					}
					if(dt.Rows[n]["DevName"]!=null && dt.Rows[n]["DevName"].ToString()!="")
					{
					model.DevName=dt.Rows[n]["DevName"].ToString();
					}
					if(dt.Rows[n]["DevNo"]!=null && dt.Rows[n]["DevNo"].ToString()!="")
					{
					model.DevNo=dt.Rows[n]["DevNo"].ToString();
					}
					if(dt.Rows[n]["InspectionUnit"]!=null && dt.Rows[n]["InspectionUnit"].ToString()!="")
					{
					model.InspectionUnit=dt.Rows[n]["InspectionUnit"].ToString();
					}
					if(dt.Rows[n]["Contacts"]!=null && dt.Rows[n]["Contacts"].ToString()!="")
					{
					model.Contacts=dt.Rows[n]["Contacts"].ToString();
					}
					if(dt.Rows[n]["OfficeTel"]!=null && dt.Rows[n]["OfficeTel"].ToString()!="")
					{
					model.OfficeTel=dt.Rows[n]["OfficeTel"].ToString();
					}
					if(dt.Rows[n]["PhoneNum"]!=null && dt.Rows[n]["PhoneNum"].ToString()!="")
					{
					model.PhoneNum=dt.Rows[n]["PhoneNum"].ToString();
					}
					if(dt.Rows[n]["SampliInspituation"]!=null && dt.Rows[n]["SampliInspituation"].ToString()!="")
					{
					model.SampliInspituation=dt.Rows[n]["SampliInspituation"].ToString();
					}
					if(dt.Rows[n]["AmountDetection"]!=null && dt.Rows[n]["AmountDetection"].ToString()!="")
					{
						model.AmountDetection=decimal.Parse(dt.Rows[n]["AmountDetection"].ToString());
					}
					if(dt.Rows[n]["Status"]!=null && dt.Rows[n]["Status"].ToString()!="")
					{
					model.Status=dt.Rows[n]["Status"].ToString();
					}
					if(dt.Rows[n]["RegistantsOID"]!=null && dt.Rows[n]["RegistantsOID"].ToString()!="")
					{
					model.RegistantsOID=dt.Rows[n]["RegistantsOID"].ToString();
					}
					if(dt.Rows[n]["RegistantsDate"]!=null && dt.Rows[n]["RegistantsDate"].ToString()!="")
					{
						model.RegistantsDate=DateTime.Parse(dt.Rows[n]["RegistantsDate"].ToString());
					}
					if(dt.Rows[n]["CREATED_BY"]!=null && dt.Rows[n]["CREATED_BY"].ToString()!="")
					{
					model.CREATED_BY=dt.Rows[n]["CREATED_BY"].ToString();
					}
					if(dt.Rows[n]["CREATED_DATE"]!=null && dt.Rows[n]["CREATED_DATE"].ToString()!="")
					{
						model.CREATED_DATE=DateTime.Parse(dt.Rows[n]["CREATED_DATE"].ToString());
					}
					if(dt.Rows[n]["LAST_UPD_BY"]!=null && dt.Rows[n]["LAST_UPD_BY"].ToString()!="")
					{
					model.LAST_UPD_BY=dt.Rows[n]["LAST_UPD_BY"].ToString();
					}
					if(dt.Rows[n]["LAST_UPD_DATE"]!=null && dt.Rows[n]["LAST_UPD_DATE"].ToString()!="")
					{
						model.LAST_UPD_DATE=DateTime.Parse(dt.Rows[n]["LAST_UPD_DATE"].ToString());
					}
					modelList.Add(model);
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			return dal.GetRecordCount(strWhere);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			return dal.GetListByPage( strWhere,  orderby,  startIndex,  endIndex);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		//public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		//{
			//return dal.GetList(PageSize,PageIndex,strWhere);
		//}

		#endregion  Method
	}
}


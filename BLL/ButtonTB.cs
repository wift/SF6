﻿using System;
using System.Data;
using System.Collections.Generic;
using Maticsoft.Common;
using EGMNGS.Model;
namespace EGMNGS.BLL
{
	/// <summary>
	/// 查看3种气体的实时库存数量和历史库存数量等信息
	/// </summary>
	public partial class ButtonTB
	{
		private readonly EGMNGS.DAL.ButtonTB dal=new EGMNGS.DAL.ButtonTB();
		public ButtonTB()
		{}
		#region  Method

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int  Add(EGMNGS.Model.ButtonTB model)
		{
			return dal.Add(model);
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(EGMNGS.Model.ButtonTB model)
		{
			return dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int PKID)
		{
			
			return dal.Delete(PKID);
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool DeleteList(string PKIDlist )
		{
			return dal.DeleteList(PKIDlist );
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public EGMNGS.Model.ButtonTB GetModel(int PKID)
		{
			
			return dal.GetModel(PKID);
		}

		/// <summary>
		/// 得到一个对象实体，从缓存中
		/// </summary>
		public EGMNGS.Model.ButtonTB GetModelByCache(int PKID)
		{
			
			string CacheKey = "ButtonTBModel-" + PKID;
			object objModel = Maticsoft.Common.DataCache.GetCache(CacheKey);
			if (objModel == null)
			{
				try
				{
					objModel = dal.GetModel(PKID);
					if (objModel != null)
					{
						int ModelCache = Maticsoft.Common.ConfigHelper.GetConfigInt("ModelCache");
						Maticsoft.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
					}
				}
				catch{}
			}
			return (EGMNGS.Model.ButtonTB)objModel;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			return dal.GetList(Top,strWhere,filedOrder);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<EGMNGS.Model.ButtonTB> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<EGMNGS.Model.ButtonTB> DataTableToList(DataTable dt)
		{
			List<EGMNGS.Model.ButtonTB> modelList = new List<EGMNGS.Model.ButtonTB>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				EGMNGS.Model.ButtonTB model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = new EGMNGS.Model.ButtonTB();
					if(dt.Rows[n]["PKID"]!=null && dt.Rows[n]["PKID"].ToString()!="")
					{
						model.PKID=int.Parse(dt.Rows[n]["PKID"].ToString());
					}
					if(dt.Rows[n]["BtnValue"]!=null && dt.Rows[n]["BtnValue"].ToString()!="")
					{
					model.BtnValue=dt.Rows[n]["BtnValue"].ToString();
					}
					if(dt.Rows[n]["BtnName"]!=null && dt.Rows[n]["BtnName"].ToString()!="")
					{
					model.BtnName=dt.Rows[n]["BtnName"].ToString();
					}
					modelList.Add(model);
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			return dal.GetRecordCount(strWhere);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			return dal.GetListByPage( strWhere,  orderby,  startIndex,  endIndex);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		//public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		//{
			//return dal.GetList(PageSize,PageIndex,strWhere);
		//}

		#endregion  Method
	}
}


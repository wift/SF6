﻿using System;
using System.Data;
using System.Collections.Generic;
using Maticsoft.Common;
using EGMNGS.Model;
namespace EGMNGS.BLL
{
	/// <summary>
	/// 采购员登记新采购气体信息、钢瓶信息及气体编码
	/// </summary>
	public partial class SYS_CODE_TYPE
	{
		private readonly EGMNGS.DAL.SYS_CODE_TYPE dal=new EGMNGS.DAL.SYS_CODE_TYPE();
		public SYS_CODE_TYPE()
		{}
		#region  Method
		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(string CODE_TYPE)
		{
			return dal.Exists(CODE_TYPE);
		}

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(EGMNGS.Model.SYS_CODE_TYPE model)
		{
			return dal.Add(model);
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(EGMNGS.Model.SYS_CODE_TYPE model)
		{
			return dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(string CODE_TYPE)
		{
			
			return dal.Delete(CODE_TYPE);
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool DeleteList(string CODE_TYPElist )
		{
			return dal.DeleteList(CODE_TYPElist );
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public EGMNGS.Model.SYS_CODE_TYPE GetModel(string CODE_TYPE)
		{
			
			return dal.GetModel(CODE_TYPE);
		}

		/// <summary>
		/// 得到一个对象实体，从缓存中
		/// </summary>
		public EGMNGS.Model.SYS_CODE_TYPE GetModelByCache(string CODE_TYPE)
		{
			
			string CacheKey = "SYS_CODE_TYPEModel-" + CODE_TYPE;
			object objModel = Maticsoft.Common.DataCache.GetCache(CacheKey);
			if (objModel == null)
			{
				try
				{
					objModel = dal.GetModel(CODE_TYPE);
					if (objModel != null)
					{
						int ModelCache = Maticsoft.Common.ConfigHelper.GetConfigInt("ModelCache");
						Maticsoft.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
					}
				}
				catch{}
			}
			return (EGMNGS.Model.SYS_CODE_TYPE)objModel;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			return dal.GetList(Top,strWhere,filedOrder);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<EGMNGS.Model.SYS_CODE_TYPE> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<EGMNGS.Model.SYS_CODE_TYPE> DataTableToList(DataTable dt)
		{
			List<EGMNGS.Model.SYS_CODE_TYPE> modelList = new List<EGMNGS.Model.SYS_CODE_TYPE>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				EGMNGS.Model.SYS_CODE_TYPE model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = new EGMNGS.Model.SYS_CODE_TYPE();
					if(dt.Rows[n]["CODE_TYPE"]!=null && dt.Rows[n]["CODE_TYPE"].ToString()!="")
					{
					model.CODE_TYPE=dt.Rows[n]["CODE_TYPE"].ToString();
					}
					if(dt.Rows[n]["CODE_TYPE_DESC"]!=null && dt.Rows[n]["CODE_TYPE_DESC"].ToString()!="")
					{
					model.CODE_TYPE_DESC=dt.Rows[n]["CODE_TYPE_DESC"].ToString();
					}
					if(dt.Rows[n]["DISPLAY_ORDER"]!=null && dt.Rows[n]["DISPLAY_ORDER"].ToString()!="")
					{
						model.DISPLAY_ORDER=decimal.Parse(dt.Rows[n]["DISPLAY_ORDER"].ToString());
					}
					if(dt.Rows[n]["ALLOW_INSERT_IND"]!=null && dt.Rows[n]["ALLOW_INSERT_IND"].ToString()!="")
					{
					model.ALLOW_INSERT_IND=dt.Rows[n]["ALLOW_INSERT_IND"].ToString();
					}
					if(dt.Rows[n]["ALLOW_SUBCODE_IND"]!=null && dt.Rows[n]["ALLOW_SUBCODE_IND"].ToString()!="")
					{
					model.ALLOW_SUBCODE_IND=dt.Rows[n]["ALLOW_SUBCODE_IND"].ToString();
					}
					modelList.Add(model);
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			return dal.GetRecordCount(strWhere);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			return dal.GetListByPage( strWhere,  orderby,  startIndex,  endIndex);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		//public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		//{
			//return dal.GetList(PageSize,PageIndex,strWhere);
		//}

		#endregion  Method
	}
}


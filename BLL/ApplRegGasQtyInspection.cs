﻿using System;
using System.Data;
using System.Collections.Generic;
using Maticsoft.Common;
using EGMNGS.Model;
namespace EGMNGS.BLL
{
    /// <summary>
    /// 气体品质检测申请登记
    /// </summary>
    public partial class ApplRegGasQtyInspection
    {
        private readonly EGMNGS.DAL.ApplRegGasQtyInspection dal = new EGMNGS.DAL.ApplRegGasQtyInspection();
        public ApplRegGasQtyInspection()
        { }
        #region  Method

        /// <summary>
        /// 得到最大ID
        /// </summary>
        public int GetMaxId()
        {
            return dal.GetMaxId();
        }

        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int OID)
        {
            return dal.Exists(OID);
        }

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(EGMNGS.Model.ApplRegGasQtyInspection model)
        {
            return dal.Add(model);
        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(EGMNGS.Model.ApplRegGasQtyInspection model)
        {
            return dal.Update(model);
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int OID)
        {

            return dal.Delete(OID);
        }
        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool DeleteList(string OIDlist)
        {
            return dal.DeleteList(OIDlist);
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public EGMNGS.Model.ApplRegGasQtyInspection GetModel(int OID)
        {

            return dal.GetModel(OID);
        }

        /// <summary>
        /// 得到一个对象实体，从缓存中
        /// </summary>
        public EGMNGS.Model.ApplRegGasQtyInspection GetModelByCache(int OID)
        {

            string CacheKey = "ApplRegGasQtyInspectionModel-" + OID;
            object objModel = Maticsoft.Common.DataCache.GetCache(CacheKey);
            if (objModel == null)
            {
                try
                {
                    objModel = dal.GetModel(OID);
                    if (objModel != null)
                    {
                        int ModelCache = Maticsoft.Common.ConfigHelper.GetConfigInt("ModelCache");
                        Maticsoft.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
                    }
                }
                catch { }
            }
            return (EGMNGS.Model.ApplRegGasQtyInspection)objModel;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            return dal.GetList(strWhere);
        }
        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            return dal.GetList(Top, strWhere, filedOrder);
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<EGMNGS.Model.ApplRegGasQtyInspection> GetModelList(string strWhere)
        {
            DataSet ds = dal.GetList(strWhere);
            return DataTableToList(ds.Tables[0]);
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<EGMNGS.Model.ApplRegGasQtyInspection> DataTableToList(DataTable dt)
        {
            List<EGMNGS.Model.ApplRegGasQtyInspection> modelList = new List<EGMNGS.Model.ApplRegGasQtyInspection>();
            int rowsCount = dt.Rows.Count;
            if (rowsCount > 0)
            {
                EGMNGS.Model.ApplRegGasQtyInspection model;
                for (int n = 0; n < rowsCount; n++)
                {
                    model = new EGMNGS.Model.ApplRegGasQtyInspection();
                    if (dt.Rows[n]["OID"] != null && dt.Rows[n]["OID"].ToString() != "")
                    {
                        model.OID = int.Parse(dt.Rows[n]["OID"].ToString());
                    }
                    if (dt.Rows[n]["CheckCode"] != null && dt.Rows[n]["CheckCode"].ToString() != "")
                    {
                        model.CheckCode = dt.Rows[n]["CheckCode"].ToString();
                    }
                    if (dt.Rows[n]["ReportCode"] != null && dt.Rows[n]["ReportCode"].ToString() != "")
                    {
                        model.ReportCode = dt.Rows[n]["ReportCode"].ToString();
                    }
                    if (dt.Rows[n]["BusinessCode"] != null && dt.Rows[n]["BusinessCode"].ToString() != "")
                    {
                        model.BusinessCode = dt.Rows[n]["BusinessCode"].ToString();
                    }
                    if (dt.Rows[n]["GasCdoe"] != null && dt.Rows[n]["GasCdoe"].ToString() != "")
                    {
                        model.GasCdoe = dt.Rows[n]["GasCdoe"].ToString();
                    }
                    if (dt.Rows[n]["CylinderCode"] != null && dt.Rows[n]["CylinderCode"].ToString() != "")
                    {
                        model.CylinderCode = dt.Rows[n]["CylinderCode"].ToString();
                    }
                    if (dt.Rows[n]["GasSourse"] != null && dt.Rows[n]["GasSourse"].ToString() != "")
                    {
                        model.GasSourse = dt.Rows[n]["GasSourse"].ToString();
                    }
                    if (dt.Rows[n]["C"] != null && dt.Rows[n]["C"].ToString() != "")
                    {
                        model.C = decimal.Parse(dt.Rows[n]["C"].ToString());
                    }
                    if (dt.Rows[n]["RH"] != null && dt.Rows[n]["RH"].ToString() != "")
                    {
                        model.RH = decimal.Parse(dt.Rows[n]["RH"].ToString());
                    }
                    if (dt.Rows[n]["KPA"] != null && dt.Rows[n]["KPA"].ToString() != "")
                    {
                        model.KPA = decimal.Parse(dt.Rows[n]["KPA"].ToString());
                    }
                    if (dt.Rows[n]["SPYID"] != null && dt.Rows[n]["SPYID"].ToString() != "")
                    {
                        model.SPYID = dt.Rows[n]["SPYID"].ToString();
                    }
                    if (dt.Rows[n]["LDYID"] != null && dt.Rows[n]["LDYID"].ToString() != "")
                    {
                        model.LDYID = dt.Rows[n]["LDYID"].ToString();
                    }
                    if (dt.Rows[n]["WSJID"] != null && dt.Rows[n]["WSJID"].ToString() != "")
                    {
                        model.WSJID = dt.Rows[n]["WSJID"].ToString();
                    }
                    if (dt.Rows[n]["QYJID"] != null && dt.Rows[n]["QYJID"].ToString() != "")
                    {
                        model.QYJID = dt.Rows[n]["QYJID"].ToString();
                    }
                    if (dt.Rows[n]["SDJID"] != null && dt.Rows[n]["SDJID"].ToString() != "")
                    {
                        model.SDJID = dt.Rows[n]["SDJID"].ToString();
                    }
                    if (dt.Rows[n]["FGGDJID"] != null && dt.Rows[n]["FGGDJID"].ToString() != "")
                    {
                        model.FGGDJID = dt.Rows[n]["FGGDJID"].ToString();
                    }
                    if (dt.Rows[n]["HWHYFXYID"] != null && dt.Rows[n]["HWHYFXYID"].ToString() != "")
                    {
                        model.HWHYFXYID = dt.Rows[n]["HWHYFXYID"].ToString();
                    }
                    if (dt.Rows[n]["SF6Index"] != null && dt.Rows[n]["SF6Index"].ToString() != "")
                    {
                        model.SF6Index = dt.Rows[n]["SF6Index"].ToString();
                    }
                    if (dt.Rows[n]["SF6Result"] != null && dt.Rows[n]["SF6Result"].ToString() != "")
                    {
                        model.SF6Result = decimal.Parse(dt.Rows[n]["SF6Result"].ToString());
                    }
                    if (dt.Rows[n]["AirQualityScoreIndex"] != null && dt.Rows[n]["AirQualityScoreIndex"].ToString() != "")
                    {
                        model.AirQualityScoreIndex = dt.Rows[n]["AirQualityScoreIndex"].ToString();
                    }
                    if (dt.Rows[n]["AirQualityScoreResult"] != null && dt.Rows[n]["AirQualityScoreResult"].ToString() != "")
                    {
                        model.AirQualityScoreResult = decimal.Parse(dt.Rows[n]["AirQualityScoreResult"].ToString());
                    }
                    if (dt.Rows[n]["CF4Index"] != null && dt.Rows[n]["CF4Index"].ToString() != "")
                    {
                        model.CF4Index = dt.Rows[n]["CF4Index"].ToString();
                    }
                    if (dt.Rows[n]["CF4Result"] != null && dt.Rows[n]["CF4Result"].ToString() != "")
                    {
                        model.CF4Result = decimal.Parse(dt.Rows[n]["CF4Result"].ToString());
                    }
                    if (dt.Rows[n]["WaterQualityScoreIndex"] != null && dt.Rows[n]["WaterQualityScoreIndex"].ToString() != "")
                    {
                        model.WaterQualityScoreIndex = dt.Rows[n]["WaterQualityScoreIndex"].ToString();
                    }
                    if (dt.Rows[n]["WaterQualityScoreResult"] != null && dt.Rows[n]["WaterQualityScoreResult"].ToString() != "")
                    {
                        model.WaterQualityScoreResult = decimal.Parse(dt.Rows[n]["WaterQualityScoreResult"].ToString());
                    }
                    if (dt.Rows[n]["WaterDewIndex"] != null && dt.Rows[n]["WaterDewIndex"].ToString() != "")
                    {
                        model.WaterDewIndex = dt.Rows[n]["WaterDewIndex"].ToString();
                    }
                    if (dt.Rows[n]["WaterDewResult"] != null && dt.Rows[n]["WaterDewResult"].ToString() != "")
                    {
                        model.WaterDewResult = decimal.Parse(dt.Rows[n]["WaterDewResult"].ToString());
                    }
                    if (dt.Rows[n]["HFIndex"] != null && dt.Rows[n]["HFIndex"].ToString() != "")
                    {
                        model.HFIndex = dt.Rows[n]["HFIndex"].ToString();
                    }
                    if (dt.Rows[n]["HFResult"] != null && dt.Rows[n]["HFResult"].ToString() != "")
                    {
                        model.HFResult = decimal.Parse(dt.Rows[n]["HFResult"].ToString());
                    }
                    if (dt.Rows[n]["KSJHFIndex"] != null && dt.Rows[n]["KSJHFIndex"].ToString() != "")
                    {
                        model.KSJHFIndex = dt.Rows[n]["KSJHFIndex"].ToString();
                    }
                    if (dt.Rows[n]["KSJHFResult"] != null && dt.Rows[n]["KSJHFResult"].ToString() != "")
                    {
                        model.KSJHFResult = decimal.Parse(dt.Rows[n]["KSJHFResult"].ToString());
                    }
                    if (dt.Rows[n]["KWYSorceIndex"] != null && dt.Rows[n]["KWYSorceIndex"].ToString() != "")
                    {
                        model.KWYSorceIndex = dt.Rows[n]["KWYSorceIndex"].ToString();
                    }
                    if (dt.Rows[n]["KWYSorceResult"] != null && dt.Rows[n]["KWYSorceResult"].ToString() != "")
                    {
                        model.KWYSorceResult = decimal.Parse(dt.Rows[n]["KWYSorceResult"].ToString());
                    }
                    if (dt.Rows[n]["Conclusion"] != null && dt.Rows[n]["Conclusion"].ToString() != "")
                    {
                        model.Conclusion = dt.Rows[n]["Conclusion"].ToString();
                    }
                    if (dt.Rows[n]["IsPass"] != null && dt.Rows[n]["IsPass"].ToString() != "")
                    {
                        model.IsPass = dt.Rows[n]["IsPass"].ToString();
                    }
                    if (dt.Rows[n]["ApplOID"] != null && dt.Rows[n]["ApplOID"].ToString() != "")
                    {
                        model.ApplOID = dt.Rows[n]["ApplOID"].ToString();
                    }
                    if (dt.Rows[n]["AppLDate"] != null && dt.Rows[n]["AppLDate"].ToString() != "")
                    {
                        model.AppLDate = DateTime.Parse(dt.Rows[n]["AppLDate"].ToString());
                    }
                    if (dt.Rows[n]["CheckOID"] != null && dt.Rows[n]["CheckOID"].ToString() != "")
                    {
                        model.CheckOID = dt.Rows[n]["CheckOID"].ToString();
                    }
                    if (dt.Rows[n]["CheckDate"] != null && dt.Rows[n]["CheckDate"].ToString() != "")
                    {
                        model.CheckDate = DateTime.Parse(dt.Rows[n]["CheckDate"].ToString());
                    }
                    if (dt.Rows[n]["SignerOID"] != null && dt.Rows[n]["SignerOID"].ToString() != "")
                    {
                        model.SignerOID = dt.Rows[n]["SignerOID"].ToString();
                    }
                    if (dt.Rows[n]["SignerDate"] != null && dt.Rows[n]["SignerDate"].ToString() != "")
                    {
                        model.SignerDate = DateTime.Parse(dt.Rows[n]["SignerDate"].ToString());
                    }
                    if (dt.Rows[n]["ApproverOID"] != null && dt.Rows[n]["ApproverOID"].ToString() != "")
                    {
                        model.ApproverOID = dt.Rows[n]["ApproverOID"].ToString();
                    }
                    if (dt.Rows[n]["ApproverDate"] != null && dt.Rows[n]["ApproverDate"].ToString() != "")
                    {
                        model.ApproverDate = DateTime.Parse(dt.Rows[n]["ApproverDate"].ToString());
                    }
                    if (dt.Rows[n]["FlowStatus"] != null && dt.Rows[n]["FlowStatus"].ToString() != "")
                    {
                        model.FlowStatus = dt.Rows[n]["FlowStatus"].ToString();
                    }
                    if (dt.Rows[n]["IsPrint"] != null && dt.Rows[n]["IsPrint"].ToString() != "")
                    {
                        if ((dt.Rows[n]["IsPrint"].ToString() == "1") || (dt.Rows[n]["IsPrint"].ToString().ToLower() == "true"))
                        {
                            model.IsPrint = true;
                        }
                        else
                        {
                            model.IsPrint = false;
                        }
                    }
                    if (dt.Rows[n]["MTID"] != null && dt.Rows[n]["MTID"].ToString() != "")
                    {
                        model.MTID = dt.Rows[n]["MTID"].ToString();
                    }

                    if (dt.Rows[n]["CheckPJType"] != null && dt.Rows[n]["CheckPJType"].ToString() != "")
                    {
                        model.CheckPJType = dt.Rows[n]["CheckPJType"].ToString();
                    }

                    if (dt.Rows[n]["Monoxide"] != null && dt.Rows[n]["Monoxide"].ToString() != "")
                    {
                        model.Monoxide = decimal.Parse(dt.Rows[n]["Monoxide"].ToString());
                    }
                    if (dt.Rows[n]["hydrothion"] != null && dt.Rows[n]["hydrothion"].ToString() != "")
                    {
                        model.Hydrothion = decimal.Parse(dt.Rows[n]["hydrothion"].ToString());
                    }
                    if (dt.Rows[n]["SulfurDioxide"] != null && dt.Rows[n]["SulfurDioxide"].ToString() != "")
                    {
                        model.SulfurDioxide = decimal.Parse(dt.Rows[n]["SulfurDioxide"].ToString());
                    }

                    if (dt.Rows[0]["C3F8"] != null && dt.Rows[0]["C3F8"].ToString() != "")
                    {
                        model.C3F8 = decimal.Parse(dt.Rows[0]["C3F8"].ToString());
                    }
                    if (dt.Rows[0]["C2F6"] != null && dt.Rows[0]["C2F6"].ToString() != "")
                    {
                        model.C2F6 = decimal.Parse(dt.Rows[0]["C2F6"].ToString());
                    }
                    modelList.Add(model);
                }
            }
            return modelList;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetAllList()
        {
            return GetList("");
        }

        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            return dal.GetRecordCount(strWhere);
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            return dal.GetListByPage(strWhere, orderby, startIndex, endIndex);
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        //public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        //{
        //return dal.GetList(PageSize,PageIndex,strWhere);
        //}

        #endregion  Method
    }
}


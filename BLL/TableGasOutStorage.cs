﻿using System;
using System.Data;
using System.Collections.Generic;
using Maticsoft.Common;
using EGMNGS.Model;
namespace EGMNGS.BLL
{
	/// <summary>
	/// 对气体分别进行回收气
	/// </summary>
	public partial class TableGasOutStorage
	{
		private readonly EGMNGS.DAL.TableGasOutStorage dal=new EGMNGS.DAL.TableGasOutStorage();
		public TableGasOutStorage()
		{}
		#region  Method

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
			return dal.GetMaxId();
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int OID)
		{
			return dal.Exists(OID);
		}

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int  Add(EGMNGS.Model.TableGasOutStorage model)
		{
			return dal.Add(model);
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(EGMNGS.Model.TableGasOutStorage model)
		{
			return dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int OID)
		{
			
			return dal.Delete(OID);
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool DeleteList(string OIDlist )
		{
			return dal.DeleteList(OIDlist );
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public EGMNGS.Model.TableGasOutStorage GetModel(int OID)
		{
			
			return dal.GetModel(OID);
		}

		/// <summary>
		/// 得到一个对象实体，从缓存中
		/// </summary>
		public EGMNGS.Model.TableGasOutStorage GetModelByCache(int OID)
		{
			
			string CacheKey = "TableGasOutStorageModel-" + OID;
			object objModel = Maticsoft.Common.DataCache.GetCache(CacheKey);
			if (objModel == null)
			{
				try
				{
					objModel = dal.GetModel(OID);
					if (objModel != null)
					{
						int ModelCache = Maticsoft.Common.ConfigHelper.GetConfigInt("ModelCache");
						Maticsoft.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
					}
				}
				catch{}
			}
			return (EGMNGS.Model.TableGasOutStorage)objModel;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			return dal.GetList(Top,strWhere,filedOrder);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<EGMNGS.Model.TableGasOutStorage> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<EGMNGS.Model.TableGasOutStorage> DataTableToList(DataTable dt)
		{
			List<EGMNGS.Model.TableGasOutStorage> modelList = new List<EGMNGS.Model.TableGasOutStorage>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				EGMNGS.Model.TableGasOutStorage model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = new EGMNGS.Model.TableGasOutStorage();
					if(dt.Rows[n]["OID"]!=null && dt.Rows[n]["OID"].ToString()!="")
					{
						model.OID=int.Parse(dt.Rows[n]["OID"].ToString());
					}
					if(dt.Rows[n]["Code"]!=null && dt.Rows[n]["Code"].ToString()!="")
					{
					model.Code=dt.Rows[n]["Code"].ToString();
					}
					if(dt.Rows[n]["Year"]!=null && dt.Rows[n]["Year"].ToString()!="")
					{
					model.Year=dt.Rows[n]["Year"].ToString();
					}
					if(dt.Rows[n]["Month"]!=null && dt.Rows[n]["Month"].ToString()!="")
					{
					model.Month=dt.Rows[n]["Month"].ToString();
					}
					if(dt.Rows[n]["GasClass"]!=null && dt.Rows[n]["GasClass"].ToString()!="")
					{
					model.GasClass=dt.Rows[n]["GasClass"].ToString();
					}
					if(dt.Rows[n]["PowerSupplyCode"]!=null && dt.Rows[n]["PowerSupplyCode"].ToString()!="")
					{
					model.PowerSupplyCode=dt.Rows[n]["PowerSupplyCode"].ToString();
					}
					if(dt.Rows[n]["PowerSupplyName"]!=null && dt.Rows[n]["PowerSupplyName"].ToString()!="")
					{
					model.PowerSupplyName=dt.Rows[n]["PowerSupplyName"].ToString();
					}
					if(dt.Rows[n]["ConvertStationCode"]!=null && dt.Rows[n]["ConvertStationCode"].ToString()!="")
					{
					model.ConvertStationCode=dt.Rows[n]["ConvertStationCode"].ToString();
					}
					if(dt.Rows[n]["ConvertStationName"]!=null && dt.Rows[n]["ConvertStationName"].ToString()!="")
					{
					model.ConvertStationName=dt.Rows[n]["ConvertStationName"].ToString();
					}
					if(dt.Rows[n]["AmountOutStorage"]!=null && dt.Rows[n]["AmountOutStorage"].ToString()!="")
					{
						model.AmountOutStorage=decimal.Parse(dt.Rows[n]["AmountOutStorage"].ToString());
					}
					if(dt.Rows[n]["CountCyliner"]!=null && dt.Rows[n]["CountCyliner"].ToString()!="")
					{
						model.CountCyliner=int.Parse(dt.Rows[n]["CountCyliner"].ToString());
					}
					if(dt.Rows[n]["Address"]!=null && dt.Rows[n]["Address"].ToString()!="")
					{
					model.Address=dt.Rows[n]["Address"].ToString();
					}
					if(dt.Rows[n]["Contacts"]!=null && dt.Rows[n]["Contacts"].ToString()!="")
					{
					model.Contacts=dt.Rows[n]["Contacts"].ToString();
					}
					if(dt.Rows[n]["OfficeTel"]!=null && dt.Rows[n]["OfficeTel"].ToString()!="")
					{
					model.OfficeTel=dt.Rows[n]["OfficeTel"].ToString();
					}
					if(dt.Rows[n]["PhoneNum"]!=null && dt.Rows[n]["PhoneNum"].ToString()!="")
					{
					model.PhoneNum=dt.Rows[n]["PhoneNum"].ToString();
					}
					if(dt.Rows[n]["Status"]!=null && dt.Rows[n]["Status"].ToString()!="")
					{
					model.Status=dt.Rows[n]["Status"].ToString();
					}
					if(dt.Rows[n]["UsingOID"]!=null && dt.Rows[n]["UsingOID"].ToString()!="")
					{
					model.UsingOID=dt.Rows[n]["UsingOID"].ToString();
					}
					if(dt.Rows[n]["UsingDate"]!=null && dt.Rows[n]["UsingDate"].ToString()!="")
					{
						model.UsingDate=DateTime.Parse(dt.Rows[n]["UsingDate"].ToString());
					}
					if(dt.Rows[n]["RegistantOID"]!=null && dt.Rows[n]["RegistantOID"].ToString()!="")
					{
					model.RegistantOID=dt.Rows[n]["RegistantOID"].ToString();
					}
					if(dt.Rows[n]["RegistantDate"]!=null && dt.Rows[n]["RegistantDate"].ToString()!="")
					{
						model.RegistantDate=DateTime.Parse(dt.Rows[n]["RegistantDate"].ToString());
					}
					if(dt.Rows[n]["CREATED_BY"]!=null && dt.Rows[n]["CREATED_BY"].ToString()!="")
					{
					model.CREATED_BY=dt.Rows[n]["CREATED_BY"].ToString();
					}
					if(dt.Rows[n]["CREATED_DATE"]!=null && dt.Rows[n]["CREATED_DATE"].ToString()!="")
					{
						model.CREATED_DATE=DateTime.Parse(dt.Rows[n]["CREATED_DATE"].ToString());
					}
					if(dt.Rows[n]["LAST_UPD_BY"]!=null && dt.Rows[n]["LAST_UPD_BY"].ToString()!="")
					{
					model.LAST_UPD_BY=dt.Rows[n]["LAST_UPD_BY"].ToString();
					}
					if(dt.Rows[n]["LAST_UPD_DATE"]!=null && dt.Rows[n]["LAST_UPD_DATE"].ToString()!="")
					{
						model.LAST_UPD_DATE=DateTime.Parse(dt.Rows[n]["LAST_UPD_DATE"].ToString());
					}
					modelList.Add(model);
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			return dal.GetRecordCount(strWhere);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			return dal.GetListByPage( strWhere,  orderby,  startIndex,  endIndex);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		//public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		//{
			//return dal.GetList(PageSize,PageIndex,strWhere);
		//}

		#endregion  Method
	}
}


﻿using System;
using System.Data;
using System.Collections.Generic;
using Maticsoft.Common;
using EGMNGS.Model;
namespace EGMNGS.BLL
{
	/// <summary>
	/// 采购员登记新采购气体信息、钢瓶信息及气体编码
	/// </summary>
	public partial class SYS_USER_GRP_FUNC
	{
		private readonly EGMNGS.DAL.SYS_USER_GRP_FUNC dal=new EGMNGS.DAL.SYS_USER_GRP_FUNC();
		public SYS_USER_GRP_FUNC()
		{}
		#region  Method
		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(string USER_GRP_ID,string FUNC_ID)
		{
			return dal.Exists(USER_GRP_ID,FUNC_ID);
		}

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(EGMNGS.Model.SYS_USER_GRP_FUNC model)
		{
			return dal.Add(model);
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(EGMNGS.Model.SYS_USER_GRP_FUNC model)
		{
			return dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(string USER_GRP_ID,string FUNC_ID)
		{
			
			return dal.Delete(USER_GRP_ID,FUNC_ID);
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public EGMNGS.Model.SYS_USER_GRP_FUNC GetModel(string USER_GRP_ID,string FUNC_ID)
		{
			
			return dal.GetModel(USER_GRP_ID,FUNC_ID);
		}

		/// <summary>
		/// 得到一个对象实体，从缓存中
		/// </summary>
		public EGMNGS.Model.SYS_USER_GRP_FUNC GetModelByCache(string USER_GRP_ID,string FUNC_ID)
		{
			
			string CacheKey = "SYS_USER_GRP_FUNCModel-" + USER_GRP_ID+FUNC_ID;
			object objModel = Maticsoft.Common.DataCache.GetCache(CacheKey);
			if (objModel == null)
			{
				try
				{
					objModel = dal.GetModel(USER_GRP_ID,FUNC_ID);
					if (objModel != null)
					{
						int ModelCache = Maticsoft.Common.ConfigHelper.GetConfigInt("ModelCache");
						Maticsoft.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
					}
				}
				catch{}
			}
			return (EGMNGS.Model.SYS_USER_GRP_FUNC)objModel;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			return dal.GetList(Top,strWhere,filedOrder);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<EGMNGS.Model.SYS_USER_GRP_FUNC> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<EGMNGS.Model.SYS_USER_GRP_FUNC> DataTableToList(DataTable dt)
		{
			List<EGMNGS.Model.SYS_USER_GRP_FUNC> modelList = new List<EGMNGS.Model.SYS_USER_GRP_FUNC>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				EGMNGS.Model.SYS_USER_GRP_FUNC model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = new EGMNGS.Model.SYS_USER_GRP_FUNC();
					if(dt.Rows[n]["USER_GRP_ID"]!=null && dt.Rows[n]["USER_GRP_ID"].ToString()!="")
					{
					model.USER_GRP_ID=dt.Rows[n]["USER_GRP_ID"].ToString();
					}
					if(dt.Rows[n]["FUNC_ID"]!=null && dt.Rows[n]["FUNC_ID"].ToString()!="")
					{
					model.FUNC_ID=dt.Rows[n]["FUNC_ID"].ToString();
					}
					if(dt.Rows[n]["CREATED_BY"]!=null && dt.Rows[n]["CREATED_BY"].ToString()!="")
					{
					model.CREATED_BY=dt.Rows[n]["CREATED_BY"].ToString();
					}
					if(dt.Rows[n]["CREATED_DATE"]!=null && dt.Rows[n]["CREATED_DATE"].ToString()!="")
					{
						model.CREATED_DATE=DateTime.Parse(dt.Rows[n]["CREATED_DATE"].ToString());
					}
					if(dt.Rows[n]["LAST_UPD_BY"]!=null && dt.Rows[n]["LAST_UPD_BY"].ToString()!="")
					{
					model.LAST_UPD_BY=dt.Rows[n]["LAST_UPD_BY"].ToString();
					}
					if(dt.Rows[n]["LAST_UPD_DATE"]!=null && dt.Rows[n]["LAST_UPD_DATE"].ToString()!="")
					{
						model.LAST_UPD_DATE=DateTime.Parse(dt.Rows[n]["LAST_UPD_DATE"].ToString());
					}
					modelList.Add(model);
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			return dal.GetRecordCount(strWhere);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			return dal.GetListByPage( strWhere,  orderby,  startIndex,  endIndex);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		//public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		//{
			//return dal.GetList(PageSize,PageIndex,strWhere);
		//}

		#endregion  Method
	}
}


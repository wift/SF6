﻿using System;
using System.Data;
using System.Collections.Generic;
using Maticsoft.Common;
using EGMNGS.Model;
namespace EGMNGS.BLL
{
    /// <summary>
    /// 对回收气体和净化后气
    /// </summary>
    public partial class BookGasFill
    {
        private readonly EGMNGS.DAL.BookGasFill dal = new EGMNGS.DAL.BookGasFill();
        public BookGasFill()
        { }
        #region  Method

        /// <summary>
        /// 得到最大ID
        /// </summary>
        public int GetMaxId()
        {
            return dal.GetMaxId();
        }

        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int OID)
        {
            return dal.Exists(OID);
        }

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(EGMNGS.Model.BookGasFill model)
        {
            return dal.Add(model);
        }
        public Maticsoft.DBUtility.CommandInfo AddCommandInfo(Model.BookGasFill model)
        {
            return dal.AddCommandInfo(model);
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(EGMNGS.Model.BookGasFill model)
        {
            return dal.Update(model);
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int OID)
        {

            return dal.Delete(OID);
        }
        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool DeleteList(string OIDlist)
        {
            return dal.DeleteList(OIDlist);
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public EGMNGS.Model.BookGasFill GetModel(int OID)
        {

            return dal.GetModel(OID);
        }

        /// <summary>
        /// 得到一个对象实体，从缓存中
        /// </summary>
        public EGMNGS.Model.BookGasFill GetModelByCache(int OID)
        {

            string CacheKey = "BookGasFillModel-" + OID;
            object objModel = Maticsoft.Common.DataCache.GetCache(CacheKey);
            if (objModel == null)
            {
                try
                {
                    objModel = dal.GetModel(OID);
                    if (objModel != null)
                    {
                        int ModelCache = Maticsoft.Common.ConfigHelper.GetConfigInt("ModelCache");
                        Maticsoft.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
                    }
                }
                catch { }
            }
            return (EGMNGS.Model.BookGasFill)objModel;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            return dal.GetList(strWhere);
        }
        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            return dal.GetList(Top, strWhere, filedOrder);
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<EGMNGS.Model.BookGasFill> GetModelList(string strWhere)
        {
            DataSet ds = dal.GetList(strWhere);
            return DataTableToList(ds.Tables[0]);
        }
        public EGMNGS.Model.BookGasFill GetModeByWhere(string strWhere)
        {
            var listBookGasFill = GetModelList(strWhere);
            if (listBookGasFill != null && listBookGasFill.Count > 0)
            {
                listBookGasFill[0].BookCylinderInfoModel = new EGMNGS.BLL.BookCylinderInfo().GetModelByCylinderCode(listBookGasFill[0].CylinderCode);
                return listBookGasFill[0];
            }
            else
            {
                return null;
            }
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<EGMNGS.Model.BookGasFill> DataTableToList(DataTable dt)
        {
            List<EGMNGS.Model.BookGasFill> modelList = new List<EGMNGS.Model.BookGasFill>();
            int rowsCount = dt.Rows.Count;
            if (rowsCount > 0)
            {
                EGMNGS.Model.BookGasFill model;
                for (int n = 0; n < rowsCount; n++)
                {
                    model = new EGMNGS.Model.BookGasFill();
                    if (dt.Rows[n]["OID"] != null && dt.Rows[n]["OID"].ToString() != "")
                    {
                        model.OID = int.Parse(dt.Rows[n]["OID"].ToString());
                    }
                    if (dt.Rows[n]["FillGasCode"] != null && dt.Rows[n]["FillGasCode"].ToString() != "")
                    {
                        model.FillGasCode = dt.Rows[n]["FillGasCode"].ToString();
                    }
                    if (dt.Rows[n]["Year"] != null && dt.Rows[n]["Year"].ToString() != "")
                    {
                        model.Year = dt.Rows[n]["Year"].ToString();
                    }
                    if (dt.Rows[n]["Month"] != null && dt.Rows[n]["Month"].ToString() != "")
                    {
                        model.Month = dt.Rows[n]["Month"].ToString();
                    }
                    if (dt.Rows[n]["BusinessCode"] != null && dt.Rows[n]["BusinessCode"].ToString() != "")
                    {
                        model.BusinessCode = dt.Rows[n]["BusinessCode"].ToString();
                    }
                    if (dt.Rows[n]["CylinderCode"] != null && dt.Rows[n]["CylinderCode"].ToString() != "")
                    {
                        model.CylinderCode = dt.Rows[n]["CylinderCode"].ToString();
                    }
                    if (dt.Rows[n]["GasCode"] != null && dt.Rows[n]["GasCode"].ToString() != "")
                    {
                        model.GasCode = dt.Rows[n]["GasCode"].ToString();
                    }
                    if (dt.Rows[n]["GasType"] != null && dt.Rows[n]["GasType"].ToString() != "")
                    {
                        model.GasType = dt.Rows[n]["GasType"].ToString();
                    }
                    if (dt.Rows[n]["GasStatus"] != null && dt.Rows[n]["GasStatus"].ToString() != "")
                    {
                        model.GasStatus = dt.Rows[n]["GasStatus"].ToString();
                    }
                    if (dt.Rows[n]["CylinderStatus"] != null && dt.Rows[n]["CylinderStatus"].ToString() != "")
                    {
                        model.CylinderStatus = dt.Rows[n]["CylinderStatus"].ToString();
                    }
                    if (dt.Rows[n]["AmountGas"] != null && dt.Rows[n]["AmountGas"].ToString() != "")
                    {
                        model.AmountGas = decimal.Parse(dt.Rows[n]["AmountGas"].ToString());
                    }
                    if (dt.Rows[n]["RegistrantOID"] != null && dt.Rows[n]["RegistrantOID"].ToString() != "")
                    {
                        model.RegistrantOID = dt.Rows[n]["RegistrantOID"].ToString();
                    }
                    if (dt.Rows[n]["RegistrantDate"] != null && dt.Rows[n]["RegistrantDate"].ToString() != "")
                    {
                        model.RegistrantDate = DateTime.Parse(dt.Rows[n]["RegistrantDate"].ToString());
                    }
                    if (dt.Rows[n]["CREATED_BY"] != null && dt.Rows[n]["CREATED_BY"].ToString() != "")
                    {
                        model.CREATED_BY = dt.Rows[n]["CREATED_BY"].ToString();
                    }
                    if (dt.Rows[n]["CREATED_DATE"] != null && dt.Rows[n]["CREATED_DATE"].ToString() != "")
                    {
                        model.CREATED_DATE = DateTime.Parse(dt.Rows[n]["CREATED_DATE"].ToString());
                    }
                    if (dt.Rows[n]["LAST_UPD_BY"] != null && dt.Rows[n]["LAST_UPD_BY"].ToString() != "")
                    {
                        model.LAST_UPD_BY = dt.Rows[n]["LAST_UPD_BY"].ToString();
                    }
                    if (dt.Rows[n]["LAST_UPD_DATE"] != null && dt.Rows[n]["LAST_UPD_DATE"].ToString() != "")
                    {
                        model.LAST_UPD_DATE = DateTime.Parse(dt.Rows[n]["LAST_UPD_DATE"].ToString());
                    }
                    if (dt.Rows[n]["Status"] != null && dt.Rows[n]["Status"].ToString() != "")
                    {
                        model.Status = dt.Rows[n]["Status"].ToString();
                    }
                    modelList.Add(model);
                }
            }
            return modelList;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetAllList()
        {
            return GetList("");
        }

        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            return dal.GetRecordCount(strWhere);
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            return dal.GetListByPage(strWhere, orderby, startIndex, endIndex);
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize, int PageIndex, string strWhere)
        {
            return dal.GetList(PageSize, PageIndex, strWhere);
        }

        #endregion  Method

    
    }
}


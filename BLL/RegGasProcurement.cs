﻿using System;
using System.Data;
using System.Collections.Generic;
using Maticsoft.Common;
using EGMNGS.Model;
namespace EGMNGS.BLL
{
	/// <summary>
	/// 采购员登记新采购气体信息、钢瓶信息及气体编码
	/// </summary>
	public partial class RegGasProcurement
	{
		private readonly EGMNGS.DAL.RegGasProcurement dal=new EGMNGS.DAL.RegGasProcurement();
		public RegGasProcurement()
		{}
		#region  Method

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
			return dal.GetMaxId();
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int OID)
		{
			return dal.Exists(OID);
		}

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int  Add(EGMNGS.Model.RegGasProcurement model)
		{
			return dal.Add(model);
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(EGMNGS.Model.RegGasProcurement model)
		{
			return dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int OID)
		{
			
			return dal.Delete(OID);
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool DeleteList(string OIDlist )
		{
			return dal.DeleteList(OIDlist );
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public EGMNGS.Model.RegGasProcurement GetModel(int OID)
		{
			
			return dal.GetModel(OID);
		}

		/// <summary>
		/// 得到一个对象实体，从缓存中
		/// </summary>
		public EGMNGS.Model.RegGasProcurement GetModelByCache(int OID)
		{
			
			string CacheKey = "RegGasProcurementModel-" + OID;
			object objModel = Maticsoft.Common.DataCache.GetCache(CacheKey);
			if (objModel == null)
			{
				try
				{
					objModel = dal.GetModel(OID);
					if (objModel != null)
					{
						int ModelCache = Maticsoft.Common.ConfigHelper.GetConfigInt("ModelCache");
						Maticsoft.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
					}
				}
				catch{}
			}
			return (EGMNGS.Model.RegGasProcurement)objModel;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			return dal.GetList(Top,strWhere,filedOrder);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<EGMNGS.Model.RegGasProcurement> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<EGMNGS.Model.RegGasProcurement> DataTableToList(DataTable dt)
		{
			List<EGMNGS.Model.RegGasProcurement> modelList = new List<EGMNGS.Model.RegGasProcurement>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				EGMNGS.Model.RegGasProcurement model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = new EGMNGS.Model.RegGasProcurement();
					if(dt.Rows[n]["OID"]!=null && dt.Rows[n]["OID"].ToString()!="")
					{
						model.OID=int.Parse(dt.Rows[n]["OID"].ToString());
					}
					if(dt.Rows[n]["Code"]!=null && dt.Rows[n]["Code"].ToString()!="")
					{
					model.Code=dt.Rows[n]["Code"].ToString();
					}
					if(dt.Rows[n]["Year"]!=null && dt.Rows[n]["Year"].ToString()!="")
					{
					model.Year=dt.Rows[n]["Year"].ToString();
					}
					if(dt.Rows[n]["Month"]!=null && dt.Rows[n]["Month"].ToString()!="")
					{
					model.Month=dt.Rows[n]["Month"].ToString();
					}
					if(dt.Rows[n]["SupplierName"]!=null && dt.Rows[n]["SupplierName"].ToString()!="")
					{
					model.SupplierName=dt.Rows[n]["SupplierName"].ToString();
					}
					if(dt.Rows[n]["ManufacturerName"]!=null && dt.Rows[n]["ManufacturerName"].ToString()!="")
					{
					model.ManufacturerName=dt.Rows[n]["ManufacturerName"].ToString();
					}
					if(dt.Rows[n]["AmountPurchased"]!=null && dt.Rows[n]["AmountPurchased"].ToString()!="")
					{
						model.AmountPurchased=decimal.Parse(dt.Rows[n]["AmountPurchased"].ToString());
					}
					if(dt.Rows[n]["PurchasedDesc"]!=null && dt.Rows[n]["PurchasedDesc"].ToString()!="")
					{
					model.PurchasedDesc=dt.Rows[n]["PurchasedDesc"].ToString();
					}
					if(dt.Rows[n]["ContactsName"]!=null && dt.Rows[n]["ContactsName"].ToString()!="")
					{
					model.ContactsName=dt.Rows[n]["ContactsName"].ToString();
					}
					if(dt.Rows[n]["OfficeTel"]!=null && dt.Rows[n]["OfficeTel"].ToString()!="")
					{
					model.OfficeTel=dt.Rows[n]["OfficeTel"].ToString();
					}
					if(dt.Rows[n]["PhoneNum"]!=null && dt.Rows[n]["PhoneNum"].ToString()!="")
					{
					model.PhoneNum=dt.Rows[n]["PhoneNum"].ToString();
					}
					if(dt.Rows[n]["Status"]!=null && dt.Rows[n]["Status"].ToString()!="")
					{
					model.Status=dt.Rows[n]["Status"].ToString();
					}
					if(dt.Rows[n]["PurchasedID"]!=null && dt.Rows[n]["PurchasedID"].ToString()!="")
					{
					model.PurchasedID=dt.Rows[n]["PurchasedID"].ToString();
					}
					if(dt.Rows[n]["PurchasedDate"]!=null && dt.Rows[n]["PurchasedDate"].ToString()!="")
					{
						model.PurchasedDate=DateTime.Parse(dt.Rows[n]["PurchasedDate"].ToString());
					}
					if(dt.Rows[n]["RegistrantOID"]!=null && dt.Rows[n]["RegistrantOID"].ToString()!="")
					{
					model.RegistrantOID=dt.Rows[n]["RegistrantOID"].ToString();
					}
					if(dt.Rows[n]["RegistrantDate"]!=null && dt.Rows[n]["RegistrantDate"].ToString()!="")
					{
						model.RegistrantDate=DateTime.Parse(dt.Rows[n]["RegistrantDate"].ToString());
					}
					if(dt.Rows[n]["CREATED_BY"]!=null && dt.Rows[n]["CREATED_BY"].ToString()!="")
					{
					model.CREATED_BY=dt.Rows[n]["CREATED_BY"].ToString();
					}
					if(dt.Rows[n]["CREATED_DATE"]!=null && dt.Rows[n]["CREATED_DATE"].ToString()!="")
					{
						model.CREATED_DATE=DateTime.Parse(dt.Rows[n]["CREATED_DATE"].ToString());
					}
					if(dt.Rows[n]["LAST_UPD_BY"]!=null && dt.Rows[n]["LAST_UPD_BY"].ToString()!="")
					{
					model.LAST_UPD_BY=dt.Rows[n]["LAST_UPD_BY"].ToString();
					}
					if(dt.Rows[n]["LAST_UPD_DATE"]!=null && dt.Rows[n]["LAST_UPD_DATE"].ToString()!="")
					{
						model.LAST_UPD_DATE=DateTime.Parse(dt.Rows[n]["LAST_UPD_DATE"].ToString());
					}
					modelList.Add(model);
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
            return GetList(" 1=1 ORDER BY OID DESC");
		}

		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			return dal.GetRecordCount(strWhere);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			return dal.GetListByPage( strWhere,  orderby,  startIndex,  endIndex);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		//public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		//{
			//return dal.GetList(PageSize,PageIndex,strWhere);
		//}

		#endregion  Method
	}
}


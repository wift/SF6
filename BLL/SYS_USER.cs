﻿using System;
using System.Data;
using System.Collections.Generic;
using Maticsoft.Common;
using EGMNGS.Model;
namespace EGMNGS.BLL
{
	/// <summary>
	/// 采购员登记新采购气体信息、钢瓶信息及气体编码
	/// </summary>
	public partial class SYS_USER
	{
		private readonly EGMNGS.DAL.SYS_USER dal=new EGMNGS.DAL.SYS_USER();
		public SYS_USER()
		{}
		#region  Method
		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(string USER_ID)
		{
			return dal.Exists(USER_ID);
		}

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(EGMNGS.Model.SYS_USER model)
		{
			return dal.Add(model);
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(EGMNGS.Model.SYS_USER model)
		{
			return dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(string USER_ID)
		{
			
			return dal.Delete(USER_ID);
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool DeleteList(string USER_IDlist )
		{
			return dal.DeleteList(USER_IDlist );
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public EGMNGS.Model.SYS_USER GetModel(string USER_ID)
		{
			
			return dal.GetModel(USER_ID);
		}

		/// <summary>
		/// 得到一个对象实体，从缓存中
		/// </summary>
		public EGMNGS.Model.SYS_USER GetModelByCache(string USER_ID)
		{
			
			string CacheKey = "SYS_USERModel-" + USER_ID;
			object objModel = Maticsoft.Common.DataCache.GetCache(CacheKey);
			if (objModel == null)
			{
				try
				{
					objModel = dal.GetModel(USER_ID);
					if (objModel != null)
					{
						int ModelCache = Maticsoft.Common.ConfigHelper.GetConfigInt("ModelCache");
						Maticsoft.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
					}
				}
				catch{}
			}
			return (EGMNGS.Model.SYS_USER)objModel;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			return dal.GetList(Top,strWhere,filedOrder);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<EGMNGS.Model.SYS_USER> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<EGMNGS.Model.SYS_USER> DataTableToList(DataTable dt)
		{
			List<EGMNGS.Model.SYS_USER> modelList = new List<EGMNGS.Model.SYS_USER>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				EGMNGS.Model.SYS_USER model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = new EGMNGS.Model.SYS_USER();
					if(dt.Rows[n]["USER_ID"]!=null && dt.Rows[n]["USER_ID"].ToString()!="")
					{
					model.USER_ID=dt.Rows[n]["USER_ID"].ToString();
					}
					if(dt.Rows[n]["USER_PWD"]!=null && dt.Rows[n]["USER_PWD"].ToString()!="")
					{
					model.USER_PWD=dt.Rows[n]["USER_PWD"].ToString();
					}
					if(dt.Rows[n]["USER_NAME"]!=null && dt.Rows[n]["USER_NAME"].ToString()!="")
					{
					model.USER_NAME=dt.Rows[n]["USER_NAME"].ToString();
					}
					if(dt.Rows[n]["EMAIL"]!=null && dt.Rows[n]["EMAIL"].ToString()!="")
					{
					model.EMAIL=dt.Rows[n]["EMAIL"].ToString();
					}
					if(dt.Rows[n]["POST"]!=null && dt.Rows[n]["POST"].ToString()!="")
					{
					model.POST=dt.Rows[n]["POST"].ToString();
					}
					if(dt.Rows[n]["USER_STATUS"]!=null && dt.Rows[n]["USER_STATUS"].ToString()!="")
					{
					model.USER_STATUS=dt.Rows[n]["USER_STATUS"].ToString();
					}
					if(dt.Rows[n]["SUSPEND_REASON"]!=null && dt.Rows[n]["SUSPEND_REASON"].ToString()!="")
					{
					model.SUSPEND_REASON=dt.Rows[n]["SUSPEND_REASON"].ToString();
					}
					if(dt.Rows[n]["SUSPEND_DATE"]!=null && dt.Rows[n]["SUSPEND_DATE"].ToString()!="")
					{
						model.SUSPEND_DATE=DateTime.Parse(dt.Rows[n]["SUSPEND_DATE"].ToString());
					}
					if(dt.Rows[n]["LAST_SIGNON"]!=null && dt.Rows[n]["LAST_SIGNON"].ToString()!="")
					{
						model.LAST_SIGNON=DateTime.Parse(dt.Rows[n]["LAST_SIGNON"].ToString());
					}
					if(dt.Rows[n]["FAIL_COUNT"]!=null && dt.Rows[n]["FAIL_COUNT"].ToString()!="")
					{
						model.FAIL_COUNT=int.Parse(dt.Rows[n]["FAIL_COUNT"].ToString());
					}
					if(dt.Rows[n]["PWD_RENEWAL_DATE"]!=null && dt.Rows[n]["PWD_RENEWAL_DATE"].ToString()!="")
					{
						model.PWD_RENEWAL_DATE=DateTime.Parse(dt.Rows[n]["PWD_RENEWAL_DATE"].ToString());
					}
					if(dt.Rows[n]["Org_Code"]!=null && dt.Rows[n]["Org_Code"].ToString()!="")
					{
					model.Org_Code=dt.Rows[n]["Org_Code"].ToString();
					}
					if(dt.Rows[n]["CREATED_BY"]!=null && dt.Rows[n]["CREATED_BY"].ToString()!="")
					{
					model.CREATED_BY=dt.Rows[n]["CREATED_BY"].ToString();
					}
					if(dt.Rows[n]["CREATED_DATE"]!=null && dt.Rows[n]["CREATED_DATE"].ToString()!="")
					{
						model.CREATED_DATE=DateTime.Parse(dt.Rows[n]["CREATED_DATE"].ToString());
					}
					if(dt.Rows[n]["LAST_UPD_BY"]!=null && dt.Rows[n]["LAST_UPD_BY"].ToString()!="")
					{
					model.LAST_UPD_BY=dt.Rows[n]["LAST_UPD_BY"].ToString();
					}
					if(dt.Rows[n]["LAST_UPD_DATE"]!=null && dt.Rows[n]["LAST_UPD_DATE"].ToString()!="")
					{
						model.LAST_UPD_DATE=DateTime.Parse(dt.Rows[n]["LAST_UPD_DATE"].ToString());
					}
					modelList.Add(model);
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			return dal.GetRecordCount(strWhere);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			return dal.GetListByPage( strWhere,  orderby,  startIndex,  endIndex);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		//public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		//{
			//return dal.GetList(PageSize,PageIndex,strWhere);
		//}

		#endregion  Method
	}
}


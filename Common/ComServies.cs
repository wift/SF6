﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Maticsoft.DBUtility;
using System.Data;
using System.Data.SqlClient;

namespace EGMNGS.Common
{
    public static class ComServies
    {
        public static string[] GetPowerApplyByUserID(string userID)
        {
            string strSql = @"SELECT o.Organization_ID,o.Organization_Name
  FROM RM_DB..Base_UserInfo u 
  LEFT JOIN RM_DB..Base_StaffOrganize s ON s.User_ID='{0}' 
  LEFT JOIN RM_DB..Base_Organization o ON o.Organization_ID=s.Organization_ID 
  WHERE u.User_ID='{0}' 
  AND o.ParentId=(SELECT  Organization_ID FROM RM_DB..Base_Organization WHERE Organization_Name='供电局')
 ";
            DataSet ds = DbHelperSQL.Query(string.Format(strSql, userID));
            if (ds.Tables[0].Rows.Count > 0)
            {
                return new string[] { ds.Tables[0].Rows[0][0] as String, ds.Tables[0].Rows[0][1] as String };
            }

            return new string[] { "", "" };
        }

        public static DataTable GetAllUserInfo()
        {
            return DbHelperSQL.Query("SELECT User_ID,User_Name from RM_DB..Base_UserInfo").Tables[0];
        }

        public static string GetCode(string code, string DateString)
        {
            DataTable dt = DbHelperSQL.Query(string.Format("SELECT * from CodeTypeTb where CodeType='{0}'", code)).Tables[0];
            return code + DateString + (Convert.ToInt32(dt.Rows[0][1])).ToString("D4");
        }
        /// <summary>
        /// 没有日期的编码
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public static string GetCode(string code)
        {
            DataTable dt = DbHelperSQL.Query(string.Format("SELECT * from CodeTypeTb where CodeType='{0}'", code)).Tables[0];
            return code + (Convert.ToInt32(dt.Rows[0][1])).ToString("D6");
        }

        /// <summary>
        /// 没有日期的编码
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public static string GetCode4(string code)
        {
            DataTable dt = DbHelperSQL.Query(string.Format("SELECT * from CodeTypeTb where CodeType='{0}'", code)).Tables[0];
            return code + (Convert.ToInt32(dt.Rows[0][1])).ToString("D4");
        }
        /// <summary>
        /// 没有日期的编码
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public static string GetCode8(string code)
        {
            DataTable dt = DbHelperSQL.Query(string.Format("SELECT * from CodeTypeTb where CodeType='{0}'", code)).Tables[0];
            return code + (Convert.ToInt32(dt.Rows[0][1])).ToString("D8");
        }
        /// <summary>
        /// 更新编码最大数
        /// </summary>
        /// <param name="code"></param>
        public static void UpdCodeNum(string code)
        {
            DbHelperSQL.ExecuteSql(string.Format("UPDATE dbo.CodeTypeTb SET MaxNum=MaxNum+1 WHERE CodeType='{0}'", code.Substring(0, 2)));
        }
        /// <summary>
        /// 更新报告编号
        /// </summary>
        /// <param name="code"></param>
        public static void UpdCodeReportNum(string codeChar)
        {
            DbHelperSQL.ExecuteSql(string.Format("UPDATE dbo.CodeTypeTb SET MaxNum=MaxNum+1 WHERE CodeType='{0}'", codeChar));
        }
        public static DataTable GetAllPowerStation()
        {
            string sql = "SELECT * FROM RM_DB..Base_Organization WHERE ParentId=(SELECT Organization_ID FROM RM_DB..Base_Organization WHERE Organization_Name='供电局')";
            return DbHelperSQL.Query(sql).Tables[0];
        }

        public static void UpdateGPStatus(string GPCode, string statusCode)
        {
            DbHelperSQL.ExecuteSql(string.Format("UPDATE BookCylinderInfo SET Status='{0}' WHERE CylinderCode='{1}'", statusCode, GPCode));
        }

        public static bool UpdateWithStatusJHBookGasFillPage(string cylinderCode, string gasCode)
        {
            List<string> sqlList = new List<string>();
            sqlList.Add(string.Format("UPDATE BookGasFill SET Status='{0}',CylinderStatus={1} WHERE CylinderCode='{2}'", "1", "3", cylinderCode));
            sqlList.Add(string.Format("UPDATE BookCylinderInfo SET Status='{0}',CurGasCode='{1}' WHERE CylinderCode='{2}'", "3", gasCode, cylinderCode));
            if (DbHelperSQL.ExecuteSqlTran(sqlList) > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 1 钢瓶抽空清洗后，台帐信息同步更新“状态”。
        /// </summary>
        public static bool UpdateWithStatusCylinderCleanTable(string cylinderCode)
        {
            List<string> sqlList = new List<string>();
            sqlList.Add(string.Format("UPDATE CylinderCleanTable SET Status='{0}' WHERE CylinderCode='{1}'", "1", cylinderCode));
            sqlList.Add(string.Format("UPDATE BookCylinderInfo SET Status='{0}',CurGasCode=NULL WHERE CylinderCode='{1}'", "1", cylinderCode));
            if (DbHelperSQL.ExecuteSqlTran(sqlList) > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        public static void GetAmountRecovery(string recyCode, out int GPCount, out Decimal AmountRecovery)
        {
            DataSet ds = DbHelperSQL.Query(string.Format("SELECT COUNT(OID),SUM(AmountRecovery) FROM dbo.DetailsOfRecycleGas WHERE ApplRecyGasReg_RecyCode='{0}'", recyCode));
            if (ds != null && ds.Tables.Count > 0)
            {
                GPCount = (int)ds.Tables[0].Rows[0][0];

                AmountRecovery = Convert.ToDecimal(Convert.IsDBNull(ds.Tables[0].Rows[0][1]) ? 0 : ds.Tables[0].Rows[0][1]);
            }
            else
            {
                GPCount = 0;
                AmountRecovery = (Decimal)0;
            }
        }

        /// <summary>
        /// 待检气体入库更新状态和库存
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public static bool UpdateDJTableGasInStoragePageStatus(string code, string businessCode, string status)
        {
            List<string> sqlList = new List<string>();
            sqlList.Add(string.Format("UPDATE TableGasInStorage SET Status='{0}',AmountInput=(select sum(d.AmountGas) from DetailsGasStorage d WHERE d.DtGasInStorageCode='{1}') WHERE Code='{1}'", status, code));
            sqlList.Add(string.Format("UPDATE BookGasFill SET [Status]='2' WHERE BusinessCode='{0}' AND GasCode IN (SELECT  GasCode FROM dbo.DetailsGasStorage WHERE DtGasInStorageCode='{1}')", businessCode, code));
            sqlList.Add(string.Format("UPDATE DetailsGasStorage set FillStatus='2' WHERE DtGasInStorageCode='{0}'", code));

            if (DbHelperSQL.ExecuteSqlTran(sqlList) > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 合格气体入库更新状态和库存
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public static bool UpdateHGTableGasInStoragePageStatus(string code, string status)
        {
            List<string> sqlList = new List<string>();
            sqlList.Add(string.Format("UPDATE TableGasInStorage SET Status='{0}',AmountInput=(select isnull(sum(d.AmountGas),0) from DetailsGasStorage d WHERE d.DtGasInStorageCode='{1}') WHERE Code='{1}'", status, code));
            sqlList.Add(string.Format(@"UPDATE dbo.DetailsGasStorage
                                        SET fillStatus='2' where DtGasInStorageCode='{0}'", code));
            //            sqlList.Add(string.Format(@"UPDATE b
            //SET b.CountWaitingGas=(b.CountPassGas+(SELECT SUM(d.AmountGas) FROM DetailsGasStorage d WHERE d.DtGasInStorageCode='{0}'))
            //FROM TableGasInStorage a,dbo.BookGasStorage b
            //WHERE b.[Year]=a.Year AND b.[Month]=a.Month AND a.Code='{0}'", code));
            if (DbHelperSQL.ExecuteSqlTran(sqlList) > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 待检气体出库更新状态和库存
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public static bool UpdateDJTableGasOutStoragePageStatus(string code, string status)
        {
            List<string> sqlList = new List<string>();
            sqlList.Add(string.Format("UPDATE TableGasOutStorage SET Status='{0}',AmountOutStorage=(select sum(d.AmountGas) from DetailsGasOutStorage d WHERE d.TableGasOutStorage_Code='{1}') WHERE Code='{1}'", status, code));
            sqlList.Add(string.Format(@"update DetailsGasStorage
                                        set FillStatus='3'
                                        where GasCode in 
(select d.GasCode from DetailsGasOutStorage d where d.TableGasOutStorage_Code='{0}')", code));
            //            sqlList.Add(string.Format(@"UPDATE b
            //SET b.CountWaitingGas=(b.CountWaitingGas-(SELECT SUM(d.AmountGas) FROM DetailsGasOutStorage d WHERE d.TableGasOutStorage_Code='{0}'))
            //FROM TableGasOutStorage a,dbo.BookGasStorage b
            //WHERE b.[Year]=a.Year AND b.[Month]=a.Month AND a.Code='{0}'", code));
            if (DbHelperSQL.ExecuteSqlTran(sqlList) > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 回收气体出库更新状态和库存
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public static bool UpdateHSTableGasOutStoragePageStatus(string code)
        {
            List<string> sqlList = new List<string>();
            sqlList.Add(string.Format("UPDATE TableGasOutStorage SET Status='{0}',AmountOutStorage=(select sum(d.AmountGas) from DetailsGasOutStorage d WHERE d.TableGasOutStorage_Code='{1}') WHERE Code='{1}'", "1", code));
            sqlList.Add(string.Format(@"update DetailsGasStorage
                                        set FillStatus='3'
                                        where GasCode in 
(select d.GasCode from DetailsGasOutStorage d where d.TableGasOutStorage_Code='{0}')", code));

            if (DbHelperSQL.ExecuteSqlTran(sqlList) > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 合格气体出库更新状态和库存
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public static bool UpdateHGTableGasOutStoragePageStatus(string code, string status)
        {
            List<string> sqlList = new List<string>();
            sqlList.Add(string.Format("UPDATE TableGasOutStorage SET Status='{0}',AmountOutStorage=(select sum(d.AmountGas) from DetailsGasOutStorage d WHERE d.TableGasOutStorage_Code='{1}'),CountCyliner=(select count(1) from DetailsGasOutStorage d WHERE d.TableGasOutStorage_Code='{1}') WHERE Code='{1}'", status, code));
            sqlList.Add(string.Format(@"UPDATE DetailsGasStorage set FillStatus='3' from DetailsGasStorage dd 
WHERE EXISTS(SELECT 1 FROM dbo.DetailsGasOutStorage d 
WHERE d.TableGasOutStorage_Code='{0}' and d.GasCode=dd.GasCode)", code));//已出库

            //            sqlList.Add(string.Format(@"UPDATE b
            //SET b.CountWaitingGas=(b.CountPassGas-(SELECT SUM(d.AmountGas) FROM DetailsGasOutStorage d WHERE d.TableGasOutStorage_Code='{0}'))
            //FROM TableGasOutStorage a,dbo.BookGasStorage b
            //WHERE b.[Year]=a.Year AND b.[Month]=a.Month AND a.Code='{0}'", code));
            if (DbHelperSQL.ExecuteSqlTran(sqlList) > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 回收气体入库更新状态和库存
        /// </summary>
        /// <param name="code">rkcode</param>
        /// <returns></returns>
        public static bool UpdateHSTableGasInStoragePageStatus(string code, string businessCode)
        {
            List<string> sqlList = new List<string>();
            sqlList.Add(string.Format("UPDATE TableGasInStorage SET Status='{0}',AmountInput=(select sum(d.AmountGas) from DetailsGasStorage d WHERE d.DtGasInStorageCode='{1}') WHERE Code='{1}'", "1", code));
            sqlList.Add(string.Format("UPDATE BookGasFill SET [Status]='2' WHERE BusinessCode='{0}' AND GasCode IN (SELECT  GasCode FROM dbo.DetailsGasStorage WHERE DtGasInStorageCode='{1}')", businessCode, code));
            sqlList.Add(string.Format("UPDATE DetailsGasStorage set FillStatus='2' WHERE DtGasInStorageCode='{0}'", code));
            //            sqlList.Add(string.Format(@"UPDATE b
            //SET b.CountWaitingGas=(b.CountRecycleGas+(SELECT SUM(d.AmountGas) FROM DetailsGasStorage d WHERE d.DtGasInStorageCode='{0}'))
            //FROM TableGasInStorage a,dbo.BookGasStorage b
            //WHERE b.[Year]=a.Year AND b.[Month]=a.Month AND a.Code='{0}'", code));
            if (DbHelperSQL.ExecuteSqlTran(sqlList) > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 获取角色 
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public static List<string> GetRoleName(string userID)
        {
            string sql = @"SELECT b.Roles_Name 
                            FROM RM_DB..Base_UserRole a,RM_DB..Base_Roles b 
                            WHERE a.Roles_ID=b.Roles_ID 
                            AND a.User_ID='{0}'";
            List<string> listRole = new List<string>();
            DataTable dt = DbHelperSQL.Query(string.Format(sql, userID)).Tables[0];

            if (dt == null)
            {
                return listRole;
            }

            foreach (DataRow item in dt.Rows)
            {
                listRole.Add(item[0].ToString());
            }

            return listRole;

        }

        public static bool UpdateDetailsByCylinderCode(string gasCode, string isPass, string date, string gasSourse, string oid, string flowStatus, string userID)
        {
            bool flag = false;
            try
            {
                string sql = string.Empty;

                if (gasSourse == "2")//入网
                {
                    sql = @"UPDATE dbo.DetailsApplDetectionGas
                                SET IsPass='{0}', CheckDate='{1}'
                                WHERE GasCode='{2}'";
                }
                else if (gasSourse == "1")//新购气体管理
                {
                    sql = @"UPDATE dbo.DetailsGasProcurement
                                SET IsPass='{0}', CheckDate='{1}'
                                WHERE GasCode='{2}'";
                }


                //更新“钢瓶台账”的钢瓶状态，如果合格：由“已充待检气”改为“已充合格气”
                //，如果不合格：由“已充待检气”改为“充装回收气”
                string isPassSql = string.Empty;
                string sqlUpdate = string.Empty;
                if (isPass == "1")
                {
                    isPassSql = string.Format(@"update dbo.BookCylinderInfo
                                set Status='4'
                                where CurGasCode='{0}'", gasCode);

                }
                else if (isPass == "0")
                {
                    isPassSql = string.Format(@"update dbo.BookCylinderInfo
                                set Status='2'
                                where CurGasCode='{0}'", gasCode);
                }

                List<string> sqlList = new List<string>();
                string sqlInspectiont = @"update ApplRegGasQtyInspection SET
                                        FlowStatus='{0}',CheckDate='{1}',CheckOID='{2}' 
                                        WHERE OID='{3}'";

                sqlList.Add(string.Format(sqlInspectiont, flowStatus, date, userID, oid));
                sqlList.Add(string.Format(sql, isPass, date, gasCode));

                sqlList.Add(isPassSql);
                int i = DbHelperSQL.ExecuteSqlTran(sqlList);

                if (i > 0)
                {
                    flag = true;
                }
            }
            catch (Exception ex)
            {
                return false;
                throw ex;
            }

            return flag;
        }

        /// <summary>
        /// 更新气体
        /// </summary>
        /// <param name="gpCode"></param>
        /// <param name="gasCode"></param>
        public static void UpdateCylinderInfo(string gpCode, string gasCode)
        {
            string sql = @"UPDATE dbo.BookCylinderInfo
                            SET CurGasCode='{0}',Status='3'
                            WHERE CylinderCode='{1}'";

            DbHelperSQL.ExecuteSql(string.Format(sql, gasCode, gpCode));
        }

        /// <summary>
        /// 待检出库里的合格气体
        /// </summary>
        /// <returns></returns>
        public static DataSet GetHGISPASSDJData(string code)
        {
            string sql = @"select a.*,b.AmountGas from dbo.ApplRegGasQtyInspection a,BookGasFill b
where a.GasCdoe=b.GasCode and a.IsPass='1' and a.GasSourse in('1','3')
and not Exists(select 1 
from dbo.DetailsGasStorage d, dbo.TableGasInStorage t 
where d.GasCode=a.GasCdoe 
and t.Code=d.DtGasInStorageCode 
and t.GasType='3') and a.FlowStatus='2'
";//新购和回收净化气

            return DbHelperSQL.Query(string.Format(sql, code));
        }

        /// <summary>
        /// 合格气体出库,快速添加
        /// </summary>
        /// <returns></returns>
        public static DataSet GetCanOut()
        {
            return DbHelperSQL.Query(string.Format(@"select a.*,t1.BusinessCode,t1.GasSourse,b.EffectiveDate
 from DetailsGasStorage a
 left join TableGasInStorage t on a.DtGasInStorageCode=t.Code 
 left join BookCylinderInfo b on b.CurGasCode=a.GasCode 
 left join (select d1.* from DetailsGasOutStorage d1,TableGasOutStorage t1 
			where d1.TableGasOutStorage_Code=t1.Code and t1.BusinessCode like 'XQ%') d on d.GasCode=b.CurGasCode 
 left join ApplRegGasQtyInspection t1 on t1.GasCdoe=a.GasCode
 where a.FillStatus='2' and t.GasType='3'
 and d.GasCode is null order by t1.BusinessCode,b.EffectiveDate
                                        "));
        }

        /// <summary>
        /// 气体报告管理
        /// </summary>
        public static DataSet GetList(string strWhere = "DATEDIFF(MONTH, a.AppLDate, GETDATE()) <= 3")
        {


            StringBuilder strSql = new StringBuilder();


            strSql.AppendFormat("select * from ReportGasMngView where " + strWhere + " order by OID desc");

            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 业务单号，如果该业务单号的所有入库记录都已经出库，就过滤不显示
        /// </summary>
        /// <returns></returns>
        public static DataSet GetProcurementCode()
        {
            string strSql = @"select distinct BusinessCode as Code from TableGasInStorage t,DetailsGasStorage d
 where t.Code=d.DtGasInStorageCode and d.GasCode not in(select o.GasCode from DetailsGasOutStorage o)
 and t.Status='1' and BusinessCode like 'CG%'
 ";

            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 业务单号，如果该业务单号的所有入库记录都已经出库，就过滤不显示
        /// </summary>
        /// <returns></returns>
        public static DataSet GetRegBatchCode()
        {
            string strSql = @"select distinct BusinessCode as Code from TableGasInStorage t,DetailsGasStorage d
 where t.Code=d.DtGasInStorageCode and d.GasCode not in(select o.GasCode from DetailsGasOutStorage o)
 and t.Status='1' and BusinessCode like 'PC%'";

            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 当前气量<=回收单的气量-已充装明细总和
        /// </summary>
        /// <param name="recy"></param>
        /// <returns></returns>
        public static DataSet GetDiffWithRecyAndFill(string recy)
        {
            return DbHelperSQL.Query(string.Format(@"select isnull(AmountRecovery,0)
-(select isnull(sum(b.AmountGas),0) from BookGasFill b where BusinessCode='{0}')
from ApplRecyGasReg 
where RecyCode='{0}'", recy));
        }


        /// <summary>
        /// 当前气量<=批次的气量-已充装明细总和
        /// </summary>
        /// <param name="recy"></param>
        /// <returns></returns>
        public static DataSet GetDiffWithBatchAndFill(string batch)
        {
            return DbHelperSQL.Query(string.Format(@"select isnull(AmountGas,0)
-(select isnull(sum(b.AmountGas),0) from BookGasFill b where BusinessCode='{0}')
from RegGasBatch 
where BatchCode='{0}'", batch));
        }

        public static decimal? GetRecycleGasTotal(string year, string month)
        {
            DataTable dt = DbHelperSQL.Query(string.Format(@"select isnull(sum(t.AmountInput)-(select sum(o.AmountOutStorage) from TableGasOutStorage o where o.GasClass='1' and year(o.RegistantDate)={0} and MONTH(o.RegistantDate)={1}) ,0)
from TableGasInStorage t where t.GasType='1' and year(t.RegistrantDate)={0} and MONTH(t.RegistrantDate)={1}
", year, month)).Tables[0];
            if (dt.Rows.Count > 0)
            {
                return Convert.ToDecimal(dt.Rows[0][0]);
            }
            else
            {
                return 0;
            }

        }

        public static decimal? GetPassGassTotal(string year, string month)
        {
            DataTable dt = DbHelperSQL.Query(string.Format(@"select isnull(sum(t.AmountInput)-(select isnull(sum(o.AmountOutStorage),0) from TableGasOutStorage o where o.GasClass='3' and year(o.RegistantDate)={0} and MONTH(o.RegistantDate)={1}) ,0)
from TableGasInStorage t where t.GasType ='3' and year(t.RegistrantDate)={0} and MONTH(t.RegistrantDate)={1}", year, month)).Tables[0];

            if (dt.Rows.Count > 0)
            {
                return Convert.ToDecimal(dt.Rows[0][0]);
            }
            else
            {
                return 0;
            }
        }

        public static decimal? GetWaitingGasTotal(string year, string month)
        {
            DataTable dt = DbHelperSQL.Query(string.Format(@"select isnull(sum(t.AmountInput)-(select sum(o.AmountOutStorage) from TableGasOutStorage o where o.GasClass='2' and year(o.RegistantDate)={0} and MONTH(o.RegistantDate)={1}) ,0)
from TableGasInStorage t where t.GasType='2' and year(t.RegistrantDate)={0} and MONTH(t.RegistrantDate)={1}", year, month)).Tables[0];
            if (dt.Rows.Count > 0)
            {
                return Convert.ToDecimal(dt.Rows[0][0]);
            }
            else
            {
                return 0;
            }
        }

        /// <summary>
        /// 【计算库存】的时候，要先判断一下（回收、待检、合格气体出入库主表记录是否提交），确保这个月的记录要全部提交。如果没有提交的要提醒用户（提示回收、待检、合格气体出库还入库，出入库编码），并不可以计算库存。
        /// </summary>
        /// <param name="year"></param>
        /// <param name="month"></param>
        /// <returns></returns>
        public static DataTable GetNotSumitedRecord(int year, int month)
        {
            string sql = string.Format(@"select Code from TableGasInStorage where [MONTH]={0} and [YEAR]={1} and [Status]=0
union
select code from TableGasOutStorage where [MONTH]={0} and [YEAR]={1} and [Status]=0", month, year);

            return DbHelperSQL.Query(sql).Tables[0];
        }

        /// <summary>
        /// 钢瓶出入库记录
        /// </summary>
        /// <param name="sqlWhere"></param>
        /// <returns></returns>
        public static DataSet GetRecordOutInView(string sqlWhere)
        {
            string sql = "select * from RecordOutInView";

            sql += " where " + sqlWhere;

            return DbHelperSQL.Query(sql);
        }
        /// <summary>
        /// 钢瓶清洗记录
        /// </summary>
        /// <param name="sqlWhere"></param>
        /// <returns></returns>
        public static DataSet GetRecordClearView(string sqlWhere)
        {
            string sql = "select * from RecordClearView";

            sql += " where " + sqlWhere;

            return DbHelperSQL.Query(sql);
        }

        /// <summary>
        /// 钢瓶清洗记录
        /// </summary>
        /// <param name="sqlWhere"></param>
        /// <returns></returns>
        public static DataSet GetRecordGPSJView(string sqlWhere)
        {
            string sql = "select * from RecordGPSJView";

            sql += " where " + sqlWhere;

            return DbHelperSQL.Query(sql);
        }


        public static DataSet GeCylinderKCMng(string sqlWhere)
        {
            return DbHelperSQL.Query(string.Format(@"select t.CODE_CHI_DESC,COUNT(CASE WHEN b.CylinderCapacity='1' THEN 1 ELSE NULL END) AS '25kg',
 COUNT(CASE WHEN b.CylinderCapacity='2' THEN 1 ELSE NULL END) AS '50kg'
 , COUNT(b.CylinderCapacity) AS '合计'
from (
select CODE,CODE_CHI_DESC,DISPLAY_ORDER from SYS_CODE where CODE_TYPE ='GPStatus') t
left join BookCylinderInfo b on b.Status=t.CODE
where {0} group by t.CODE_CHI_DESC,t.DISPLAY_ORDER", sqlWhere));
        }

        public static bool GetCheckStatus(DateTime dateTime)
        {
            DataSet ds = DbHelperSQL.Query(string.Format(@"select CheckStatus from dbo.BookGasStorage where [YEAR]={0} and [Month]={1}", dateTime.Year, dateTime.Month));
            if (ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0].ToString() == "1")
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        /// <summary>
        /// 未入库的批次号都显示
        /// </summary>
        /// <returns></returns>
        public static DataSet GetIn()
        {
            return DbHelperSQL.Query(@"SELECT distinct b.BusinessCode FROM BookGasFill b
                                        where b.Status='1' and b.GasType='4' 
                                        and b.GasCode not in(select d.GasCode from DetailsGasStorage d)");
        }

        /// <summary>
        /// 获取新购气没入库的CG号
        /// </summary>
        /// <returns></returns>
        public static DataSet GetXG()
        {
            return DbHelperSQL.Query(@"SELECT distinct b.BusinessCode FROM BookGasFill b
                                        where b.Status='1' and b.GasType='1' 
                                        and b.GasCode not in(select d.GasCode from DetailsGasStorage d)");

        }

        /// <summary>
        /// 录入的气量≤5时，钢瓶容量5kg；
        /// 5＜气量≤10，钢瓶容量10kg；
        /// 10＜气量≤25，钢瓶容量25kg；
        /// 25＜气量≤50，钢瓶容量50kg；
        /// 其它，钢瓶容量100kg
        /// </summary>
        /// <param name="AmountGas"></param>
        /// <returns></returns>
        private static string GetCylinderCapacity(object AmountGas)
        {
            string returnValue = string.Empty;
            int amount = (AmountGas is DBNull) ? 0 : Convert.ToInt32(AmountGas);
            if (amount == 0)
            {
                returnValue = "";
            }
            else if (amount > 0 && amount <= 5)
            {
                returnValue = "4";
            }
            else if (amount > 5 && amount <= 10)
            {
                returnValue = "3";
            }
            else if (amount > 10 && amount <= 25)
            {
                returnValue = "2";
            }
            else if (amount > 25 && amount <= 50)
            {
                returnValue = "1";
            }
            else
            {
                returnValue = "5";
            }
            return returnValue;
        }
        /// <summary>
        ///        //同步更新《钢瓶台账》时没更新【钢瓶状态】改为“已充待检气”。
        /// </summary>
        /// <param name="p"></param>
        /// <param name="AmountDetection"></param>
        /// <param name="appCode"></param>
        /// <returns></returns>
        public static bool UpdateRWDJ(string p, decimal? AmountDetection, string appCode)
        {
            List<string> sqlList = new List<string>();
            //获取从表记录数量
            DataSet dsDetail = DbHelperSQL.Query(string.Format("select * from dbo.DetailsApplDetectionGas where ApplCode='{0}';", appCode));
            sqlList.Add(@"SET NOCOUNT ON;
	BEGIN TRAN   
	BEGIN TRY ");

            sqlList.Add(string.Format(@"update ApplDetectionGas
                                            set [Status]='1',AmountDetection='{1}'
                                            where ApplCode='{0}';", appCode, AmountDetection));
            sqlList.Add(string.Format(@" DECLARE @GPCode VARCHAR(8); DECLARE @MaxNum int; "));

            if (dsDetail.Tables[0].Rows.Count > 0)
            {
                sqlList.Add(@"SELECT @MaxNum=MaxNum-1 from CodeTypeTb where CodeType='RW'; ");
            }

            foreach (DataRow item in dsDetail.Tables[0].Rows)
            {
                sqlList.Add(@" set @MaxNum=@MaxNum+1;");
                sqlList.Add(string.Format(@"set @GPCode=RIGHT('000000'+CONVERT(VARCHAR(100),@MaxNum),6);"));
                sqlList.Add(string.Format(" update DetailsApplDetectionGas set CylinderCode='RW'+@GPCode where OID={0};", item["OID"]));
                sqlList.Add(string.Format(@"insert into BookCylinderInfo(CylinderCode,CylinderSourse,[Status],CylinderSealNo,CylinderCapacity,Manufacturer)
                                            VALUES('RW'+@GPCode,'GDJ'+@GPCode,'{0}','{1}','{2}','{3}');", 1, item["ProduceNum"], GetCylinderCapacity(item["AmountGas"]), '2'));

            }
            if (dsDetail.Tables[0].Rows.Count > 0)
            {
                sqlList.Add(string.Format(@"UPDATE dbo.CodeTypeTb SET MaxNum=@MaxNum+1 WHERE CodeType='RW';"));
            }

            sqlList.Add(@"COMMIT TRAN                              
	END TRY      
	BEGIN CATCH     
	 SELECT  ERROR_MESSAGE()  AS  ErrorMessage , 
    ERROR_SEVERITY()  AS  ErrorSeverity,  
    ERROR_STATE()  AS  ErrorState   
		ROLLBACK TRAN	
 UPDATE dbo.CodeTypeTb SET MaxNum=(select 
max(CAST(RIGHT(CylinderCode,6) as int))+1 
from BookCylinderInfo where CylinderCode like 'RW%') WHERE CodeType='RW';     
	END CATCH  
		 ");

            StringBuilder sb = new StringBuilder();

            foreach (string item in sqlList)
            {
                sb.Append(item);
            }

            DbHelperSQL.ExecuteSql(sb.ToString());

            LogHelper log = new LogHelper();
            log.WriteLog("UpdateRWDJ:" + sb.ToString());
            log.Dispose();

            return true;
        }

        /// <summary>
        /// 气体收发存表
        /// </summary>
        /// <param name="p"></param>
        /// <param name="p_2"></param>
        /// <returns></returns>
        public static DataSet GetRPTGasSFCTable(int p_year, int p_month)
        {
            string sql = @"select * from 
  (select count(d.AmountGas) AS RecyCount, isnull(SUM(d.AmountGas),0) as RecyTotal from DetailsGasStorage d,TableGasInStorage t
  where d.DtGasInStorageCode=t.Code
  and t.GasSourse='0' and t.Status='1' and d.AmountGas<=25 and d.FillStatus in('2','3') and t.Year={0} and t.Month={1}) h
,
  (select count(d.AmountGas) AS RecyInCount,isnull(SUM(d.AmountGas),0) as RecyIn from DetailsGasStorage d,TableGasInStorage t
  where d.DtGasInStorageCode=t.Code
  and t.GasSourse='0' and t.Status='1' and d.FillStatus='2' and d.AmountGas<=25 and t.Year={0} and t.Month={1}) h2
  ,
  (select count(d.AmountGas) AS RecyOutCount,isnull(SUM(d.AmountGas),0) as RecyOut from DetailsGasStorage d,TableGasInStorage t
  where d.DtGasInStorageCode=t.Code
  and t.GasSourse='0' and t.Status='1' and d.FillStatus='3' and d.AmountGas<=25 and t.Year={0} and t.Month={1}) h21,
  --净化
  (
 select count(d.AmountGas) AS JHCount,isnull(SUM(d.AmountGas),0) as JHTotal from DetailsGasStorage d,TableGasInStorage t
  where d.DtGasInStorageCode=t.Code
  and t.GasSourse='2' and t.Status='1' and d.AmountGas<=25 and d.FillStatus in('2','3') and t.Year={0} and t.Month={1}
  ) h3,
 (select count(d.AmountGas) AS JHInCount,isnull(SUM(d.AmountGas),0) as JHIn from DetailsGasStorage d,TableGasInStorage t
  where d.DtGasInStorageCode=t.Code
  and t.GasSourse='2' and t.Status='1' and d.FillStatus='2' and d.AmountGas<=25 and t.Year={0} and t.Month={1}) h4,

   (select count(d.AmountGas) AS JHOutCount,isnull(SUM(d.AmountGas),0) as JHOut from DetailsGasStorage d,TableGasInStorage t
  where d.DtGasInStorageCode=t.Code
  and t.GasSourse='2' and t.Status='1' and d.FillStatus='3' and d.AmountGas<=25 and t.Year={0} and t.Month={1}) h41,
  --新购
  ( select count(d.AmountGas) AS XGCount,isnull(SUM(d.AmountGas),0) as XGTotal from DetailsGasStorage d,TableGasInStorage t
  where d.DtGasInStorageCode=t.Code
  and t.GasSourse='1' and t.Status='1' and d.AmountGas<=25 and d.FillStatus in('2','3') and  t.Year={0} and t.Month={1}) h5,
  
  (   select count(d.AmountGas) AS XGInCount,isnull(SUM(d.AmountGas),0) as XGIn from DetailsGasStorage d,TableGasInStorage t
  where d.DtGasInStorageCode=t.Code
  and t.GasSourse='1' and t.Status='1' and d.FillStatus='2' and d.AmountGas<=25 and t.Year={0} and t.Month={1}) h6,
  
 (   select count(d.AmountGas) AS XGOutCount,isnull(SUM(d.AmountGas),0) as XGOut from DetailsGasStorage d,TableGasInStorage t
  where d.DtGasInStorageCode=t.Code
  and t.GasSourse='1' and t.Status='1' and d.FillStatus='3' and d.AmountGas<=25 and t.Year={0} and t.Month={1}) h61,
   --合格
   (select count(d.AmountGas) AS HGCount,isnull(SUM(d.AmountGas),0) as HGTotal from DetailsGasStorage d,TableGasInStorage t
  where d.DtGasInStorageCode=t.Code
  and t.GasType='3' and t.Status='1' and d.AmountGas<=25 and d.FillStatus in('2','3') and t.Year={0} and t.Month={1}) h7,

     (select count(d.AmountGas) AS HGInCount,isnull(SUM(d.AmountGas),0) as HGIn from DetailsGasStorage d,TableGasInStorage t
  where d.DtGasInStorageCode=t.Code
  and t.GasType='3' and t.Status='1' and d.FillStatus='2' and d.AmountGas<=25 and t.Year={0} and t.Month={1}) h8,

     (select count(d.AmountGas) AS HGOutCount,isnull(SUM(d.AmountGas),0) as HGOut from DetailsGasStorage d,TableGasInStorage t
  where d.DtGasInStorageCode=t.Code
  and t.GasType='3' and t.Status='1' and d.FillStatus='3' and d.AmountGas<=25 and t.Year={0} and t.Month={1}) h81
  
  union all
 
select * from 
  (select count(d.AmountGas) AS RecyCount,isnull(SUM(d.AmountGas),0) as RecyTotal from DetailsGasStorage d,TableGasInStorage t
  where d.DtGasInStorageCode=t.Code
  and t.GasSourse='0' and t.Status='1' and d.AmountGas>25 and d.FillStatus in('2','3') and t.Year={0} and t.Month={1}) h
,
  (select count(d.AmountGas) AS RecyInCount,isnull(SUM(d.AmountGas),0) as RecyIn from DetailsGasStorage d,TableGasInStorage t
  where d.DtGasInStorageCode=t.Code
  and t.GasSourse='0' and t.Status='1' and d.FillStatus='2' and d.AmountGas>25 and t.Year={0} and t.Month={1}) h2
  ,
  (select count(d.AmountGas) AS RecyOutCount,isnull(SUM(d.AmountGas),0) as RecyOut from DetailsGasStorage d,TableGasInStorage t
  where d.DtGasInStorageCode=t.Code
  and t.GasSourse='0' and t.Status='1' and d.FillStatus='3' and d.AmountGas>25 and t.Year={0} and t.Month={1}) h21,
  --净化
  (
 select count(d.AmountGas) AS JHCount,isnull(SUM(d.AmountGas),0) as JHTotal from DetailsGasStorage d,TableGasInStorage t
  where d.DtGasInStorageCode=t.Code
  and t.GasSourse='2' and t.Status='1' and d.AmountGas>25 and d.FillStatus in('2','3') and t.Year={0} and t.Month={1}
  ) h3,
 (select count(d.AmountGas) AS JHInCount,isnull(SUM(d.AmountGas),0) as JHIn from DetailsGasStorage d,TableGasInStorage t
  where d.DtGasInStorageCode=t.Code
  and t.GasSourse='2' and t.Status='1' and d.FillStatus='2' and d.AmountGas>25 and t.Year={0} and t.Month={1}) h4,

   (select count(d.AmountGas) AS JHOutCount,isnull(SUM(d.AmountGas),0) as JHOut from DetailsGasStorage d,TableGasInStorage t
  where d.DtGasInStorageCode=t.Code
  and t.GasSourse='2' and t.Status='1' and d.FillStatus='3' and d.AmountGas>25 and t.Year={0} and t.Month={1}) h41,
  --新购
  ( select count(d.AmountGas) AS XGCount,isnull(SUM(d.AmountGas),0) as XGTotal from DetailsGasStorage d,TableGasInStorage t
  where d.DtGasInStorageCode=t.Code
  and t.GasSourse='1' and t.Status='1' and d.AmountGas>25 and d.FillStatus in('2','3') and t.Year={0} and t.Month={1}) h5,
  
  (   select count(d.AmountGas) AS XGInCount,isnull(SUM(d.AmountGas),0) as XGIn from DetailsGasStorage d,TableGasInStorage t
  where d.DtGasInStorageCode=t.Code
  and t.GasSourse='1' and t.Status='1' and d.FillStatus='2' and d.AmountGas>25 and t.Year={0} and t.Month={1}) h6,
  
  (   select count(d.AmountGas) AS XGOutCount,isnull(SUM(d.AmountGas),0) as XGOut from DetailsGasStorage d,TableGasInStorage t
  where d.DtGasInStorageCode=t.Code
  and t.GasSourse='1' and t.Status='1' and d.FillStatus='3' and d.AmountGas>25 and t.Year={0} and t.Month={1}) h61,
  
   --合格
   (select count(d.AmountGas) AS HGCount,isnull(SUM(d.AmountGas),0) as HGTotal from DetailsGasStorage d,TableGasInStorage t
  where d.DtGasInStorageCode=t.Code
  and t.GasType='3' and t.Status='1' and d.AmountGas>25 and d.FillStatus in('2','3') and t.Year={0} and t.Month={1}) h7,

     (select count(d.AmountGas) AS HGInCount,isnull(SUM(d.AmountGas),0) as HGIn from DetailsGasStorage d,TableGasInStorage t
  where d.DtGasInStorageCode=t.Code
  and t.GasType='3' and t.Status='1' and d.FillStatus='2' and d.AmountGas>25 and t.Year={0} and t.Month={1}) h8,

  (select count(d.AmountGas) AS HGOutCount,isnull(SUM(d.AmountGas),0) as HGOut from DetailsGasStorage d,TableGasInStorage t
  where d.DtGasInStorageCode=t.Code
  and t.GasType='3' and t.Status='1' and d.FillStatus='3' and d.AmountGas>25 and t.Year={0} and t.Month={1}) h81

 union all
 
select * from 
  (select count(d.AmountGas) AS RecyCount,isnull(SUM(d.AmountGas),0) as RecyTotal from DetailsGasStorage d,TableGasInStorage t
  where d.DtGasInStorageCode=t.Code
  and t.GasSourse='0' and t.Status='1' and d.FillStatus in('2','3') and t.Year={0} and t.Month={1}) h
,
  (select count(d.AmountGas) AS RecyInCount,isnull(SUM(d.AmountGas),0) as RecyIn from DetailsGasStorage d,TableGasInStorage t
  where d.DtGasInStorageCode=t.Code
  and t.GasSourse='0' and t.Status='1' and d.FillStatus='2' and t.Year={0} and t.Month={1}) h2
  ,
  (select count(d.AmountGas) AS RecyOutCount,isnull(SUM(d.AmountGas),0) as RecyOut from DetailsGasStorage d,TableGasInStorage t
  where d.DtGasInStorageCode=t.Code
  and t.GasSourse='0' and t.Status='1' and d.FillStatus='3'  and t.Year={0} and t.Month={1}) h21,
  --净化
  (
 select count(d.AmountGas) AS JHCount,isnull(SUM(d.AmountGas),0) as JHTotal from DetailsGasStorage d,TableGasInStorage t
  where d.DtGasInStorageCode=t.Code
  and t.GasSourse='2' and t.Status='1' and d.FillStatus in('2','3') and t.Year={0} and t.Month={1}
  ) h3,
 (select count(d.AmountGas) AS JHInCount,isnull(SUM(d.AmountGas),0) as JHIn from DetailsGasStorage d,TableGasInStorage t
  where d.DtGasInStorageCode=t.Code
  and t.GasSourse='2' and t.Status='1' and d.FillStatus='2' and t.Year={0} and t.Month={1}) h4,

   (select count(d.AmountGas) AS JHOutCount,isnull(SUM(d.AmountGas),0) as JHOut from DetailsGasStorage d,TableGasInStorage t
  where d.DtGasInStorageCode=t.Code
  and t.GasSourse='2' and t.Status='1' and d.FillStatus='3' and t.Year={0} and t.Month={1}) h41,
  --新购
  ( select count(d.AmountGas) AS XGCount,isnull(SUM(d.AmountGas),0) as XGTotal from DetailsGasStorage d,TableGasInStorage t
  where d.DtGasInStorageCode=t.Code
  and t.GasSourse='1' and t.Status='1' and d.FillStatus in('2','3') and t.Year={0} and t.Month={1}) h5,
  
  (   select count(d.AmountGas) AS XGInCount,isnull(SUM(d.AmountGas),0) as XGIn from DetailsGasStorage d,TableGasInStorage t
  where d.DtGasInStorageCode=t.Code
  and t.GasSourse='1' and t.Status='1' and d.FillStatus='2' and t.Year={0} and t.Month={1}) h6,
  
  (   select count(d.AmountGas) AS XGOutCount,isnull(SUM(d.AmountGas),0) as XGOut from DetailsGasStorage d,TableGasInStorage t
  where d.DtGasInStorageCode=t.Code
  and t.GasSourse='1' and t.Status='1' and d.FillStatus='3' and t.Year={0} and t.Month={1}) h61,
  
   --合格
   (select count(d.AmountGas) AS HGCount,isnull(SUM(d.AmountGas),0) as HGTotal from DetailsGasStorage d,TableGasInStorage t
  where d.DtGasInStorageCode=t.Code
  and t.GasType='3' and t.Status='1' and d.FillStatus in('2','3') and t.Year={0} and t.Month={1}) h7,

     (select count(d.AmountGas) AS HGInCount,isnull(SUM(d.AmountGas),0) as HGIn from DetailsGasStorage d,TableGasInStorage t
  where d.DtGasInStorageCode=t.Code
  and t.GasType='3' and t.Status='1' and d.FillStatus='2' and t.Year={0} and t.Month={1}) h8,

  (select count(d.AmountGas) AS HGOutCount,isnull(SUM(d.AmountGas),0) as HGOut from DetailsGasStorage d,TableGasInStorage t
  where d.DtGasInStorageCode=t.Code
  and t.GasType='3' and t.Status='1' and d.FillStatus='3' and t.Year={0} and t.Month={1}) h81
";

            return DbHelperSQL.Query(string.Format(sql, p_year, p_month));
        }

        public static DataSet GetRPTJHProductMonthR(string p)
        {
            return DbHelperSQL.Query(string.Format(p));
        }

        public static int UpdateBookCylinderInfoStatusByWhere(string cylinderCode, string status)
        {
            string sql = "update BookCylinderInfo set status='{0}' WHERE CylinderCode='{1}'";
            return DbHelperSQL.ExecuteSql(string.Format(sql, status, cylinderCode));
        }

        public static DataTable getApplRegGasQtyInspectionIndex(string p)
        {
            string sql = string.Format("select lsnd,wxsp,sxsp,cxsp,cql,clds,nd,cytj,sfhl,KWYCQL from dbo.ApplRegGasQtyInspection where CheckCode='{0}'", p);
            return DbHelperSQL.Query(sql).Tables[0];
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="key">OID</param>
        /// <param name="lsnd">硫酸浓度</param>
        /// <param name="wxsp">未吸收瓶</param>
        /// <param name="sxsp">始吸收瓶</param>
        /// <param name="cxsp">次吸收瓶</param>
        /// <param name="cql">采气量</param>
        /// <param name="clds">测量读数</param>
        /// <param name="nd">浓度</param>
        /// <param name="cytj">采样体积</param>
        /// <param name="sfhl">水分含量</param>
        /// <returns></returns>
        public static int UpdateApplRegGasQtyInspectionIndex(string key, string lsnd, string wxsp, string sxsp, string cxsp, string cql, string clds, string nd, string cytj, string sfhl, string KWYCQL, decimal txtHFResult, decimal KSJHFResult, decimal KWYSorceResult, decimal WaterQualityScoreResult, string C, string RH, string KPA, string Monoxide, string hydrothion, string SulfurDioxide, decimal i_StandardVolume)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update ApplRegGasQtyInspection set ");
            strSql.Append("lsnd=@lsnd,");
            strSql.Append("wxsp=@wxsp,");
            strSql.Append("sxsp=@sxsp,");
            strSql.Append("cxsp=@cxsp,");
            strSql.Append("cql=@cql,");
            strSql.Append("clds=@clds,");
            strSql.Append("nd=@nd,");
            strSql.Append("cytj=@cytj,");
            strSql.Append("sfhl=@sfhl,");


            strSql.Append("HFResult=@HFResult,");
            strSql.Append("KSJHFResult=@KSJHFResult,");
            strSql.Append("KWYSorceResult=@KWYSorceResult,");
            strSql.Append("WaterQualityScoreResult=@WaterQualityScoreResult,");

            strSql.Append("C=@C,");
            strSql.Append("RH=@RH,");
            strSql.Append("KPA=@KPA,");

            strSql.Append("KWYCQL=@KWYCQL,");


            strSql.Append("Monoxide=@Monoxide,");
            strSql.Append("hydrothion=@hydrothion,");
            strSql.Append("SulfurDioxide=@SulfurDioxide,");
            strSql.Append("StandardVolume=@StandardVolume");

            strSql.Append(" where CheckCode=@CheckCode");
            SqlParameter[] parameters = {
					new SqlParameter("@lsnd", SqlDbType.Decimal,18),
					new SqlParameter("@wxsp", SqlDbType.Decimal,18),
					new SqlParameter("@sxsp", SqlDbType.Decimal,18),
					new SqlParameter("@cxsp", SqlDbType.Decimal,18),
					new SqlParameter("@cql", SqlDbType.Decimal,18),
					new SqlParameter("@clds", SqlDbType.Decimal,18),
					new SqlParameter("@nd", SqlDbType.Decimal,18),
					new SqlParameter("@cytj",SqlDbType.Decimal,18),
					new SqlParameter("@sfhl",SqlDbType.Decimal,18),	

                        new SqlParameter("@HFResult", SqlDbType.Decimal,18),
					new SqlParameter("@KSJHFResult", SqlDbType.Decimal,18),
					new SqlParameter("@KWYSorceResult",SqlDbType.Decimal,18),
					new SqlParameter("@WaterQualityScoreResult",SqlDbType.Decimal,18),	

                             new SqlParameter("@C", SqlDbType.Decimal,18),
					new SqlParameter("@RH", SqlDbType.Decimal,18),
					new SqlParameter("@KPA",SqlDbType.Decimal,18),
                    new SqlParameter("@KWYCQL",SqlDbType.Decimal,18),

                
					new SqlParameter("@CheckCode", SqlDbType.VarChar,20),
                     new SqlParameter("@Monoxide", SqlDbType.Decimal,18),
					new SqlParameter("@hydrothion",SqlDbType.Decimal,18),
                    new SqlParameter("@SulfurDioxide",SqlDbType.Decimal,18),
            new SqlParameter("@StandardVolume",SqlDbType.Decimal,18)};
            if (lsnd.Length > 0)
            {
                parameters[0].Value = Convert.ToDecimal(lsnd);
            }

            if (wxsp.Length > 0)
            {
                parameters[1].Value = Convert.ToDecimal(wxsp);
            }
            if (sxsp.Length > 0)
            {
                parameters[2].Value = Convert.ToDecimal(sxsp);

            }
            if (cxsp.Length > 0)
            {
                parameters[3].Value = Convert.ToDecimal(cxsp);
            }
            if (cql.Length > 0)
            {
                parameters[4].Value = Convert.ToDecimal(cql);
            }
            if (clds.Length > 0)
            {
                parameters[5].Value = Convert.ToDecimal(clds);
            }
            if (nd.Length > 0)
            {
                parameters[6].Value = Convert.ToDecimal(nd);
            }
            if (cytj.Length > 0)
            {
                parameters[7].Value = Convert.ToDecimal(cytj);
            }
            if (sfhl.Length > 0)
            {
                parameters[8].Value = Convert.ToDecimal(sfhl);

            }

            parameters[9].Value = txtHFResult;
            parameters[10].Value = KSJHFResult;
            parameters[11].Value = KWYSorceResult;
            parameters[12].Value = WaterQualityScoreResult;

            //parameters[13].Value = C;
            //parameters[14].Value = RH;
            //parameters[15].Value = KPA;

            if (C.Length > 0)
            {
                parameters[13].Value = Convert.ToDecimal(C);
            }
            if (RH.Length > 0)
            {
                parameters[14].Value = Convert.ToDecimal(RH);
            }
            if (KPA.Length > 0)
            {
                parameters[15].Value = Convert.ToDecimal(KPA);

            }

            if (KWYCQL.Length > 0)
            {
                parameters[16].Value = Convert.ToDecimal(KWYCQL);

            }
            if (key.Length > 0)
            {
                parameters[17].Value = key;

            }
            if (Monoxide.Length > 0)
            {
                parameters[18].Value = Monoxide;
            }
            if (hydrothion.Length > 0)
            {
                parameters[19].Value = hydrothion;
            }
            if (SulfurDioxide.Length > 0)
            {
                parameters[20].Value = SulfurDioxide;
            }

            parameters[21].Value = i_StandardVolume;




            return DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);


        }

        public static int UpdateApplRegGasQtyInspectionIndexPC(string key, string lsnd, string wxsp, string sxsp, string cxsp, string cql, string clds, string nd, string cytj, string KWYCQL, decimal txtHFResult, decimal KSJHFResult, decimal KWYSorceResult, decimal WaterQualityScoreResult, string C, string RH, string KPA, string Monoxide, string hydrothion, string SulfurDioxide, decimal i_StandardVolume)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update ApplRegGasQtyInspection set ");
            strSql.Append("lsnd=@lsnd,");
            strSql.Append("wxsp=@wxsp,");
            strSql.Append("sxsp=@sxsp,");
            strSql.Append("cxsp=@cxsp,");
            strSql.Append("cql=@cql,");
            strSql.Append("clds=@clds,");
            strSql.Append("nd=@nd,");
            strSql.Append("cytj=@cytj,");
            // strSql.Append("sfhl=@sfhl,");

            strSql.Append("HFResult=@HFResult,");
            strSql.Append("KSJHFResult=@KSJHFResult,");
            strSql.Append("KWYSorceResult=@KWYSorceResult,");
            // strSql.Append("WaterQualityScoreResult=@WaterQualityScoreResult,");

            strSql.Append("C=@C,");
            strSql.Append("RH=@RH,");
            strSql.Append("KPA=@KPA,");
            strSql.Append("Monoxide=@Monoxide,");
            strSql.Append("hydrothion=@hydrothion,");
            strSql.Append("SulfurDioxide=@SulfurDioxide,");
            strSql.Append("KWYCQL=@KWYCQL,");
            strSql.Append("StandardVolume=@StandardVolume");
            strSql.Append(" where BusinessCode=@BusinessCode ");
            SqlParameter[] parameters = {
					new SqlParameter("@lsnd", SqlDbType.Decimal,18),
					new SqlParameter("@wxsp", SqlDbType.Decimal,18),
					new SqlParameter("@sxsp", SqlDbType.Decimal,18),
					new SqlParameter("@cxsp", SqlDbType.Decimal,18),
					new SqlParameter("@cql", SqlDbType.Decimal,18),
					new SqlParameter("@clds", SqlDbType.Decimal,18),
					new SqlParameter("@nd", SqlDbType.Decimal,18),
					new SqlParameter("@cytj",SqlDbType.Decimal,18),
				//	new SqlParameter("@sfhl",SqlDbType.Decimal,18),	

                    new SqlParameter("@HFResult", SqlDbType.Decimal,18),
					new SqlParameter("@KSJHFResult", SqlDbType.Decimal,18),
					new SqlParameter("@KWYSorceResult",SqlDbType.Decimal,18),
				//	new SqlParameter("@WaterQualityScoreResult",SqlDbType.Decimal,18),	

                      new SqlParameter("@C", SqlDbType.Decimal,18),
					new SqlParameter("@RH", SqlDbType.Decimal,18),
					new SqlParameter("@KPA",SqlDbType.Decimal,18),
                    new SqlParameter("@KWYCQL",SqlDbType.Decimal,18),
					new SqlParameter("@BusinessCode", SqlDbType.VarChar,20),
                                         new SqlParameter("@Monoxide", SqlDbType.Decimal,18),
					new SqlParameter("@hydrothion",SqlDbType.Decimal,18),
                    new SqlParameter("@SulfurDioxide",SqlDbType.Decimal,18),
             new SqlParameter("@StandardVolume",SqlDbType.Decimal,18)};
            parameters[0].Value = Convert.ToDecimal(lsnd);
            if (wxsp.Length > 0)
            {
                parameters[1].Value = Convert.ToDecimal(wxsp);
            }
            if (sxsp.Length > 0)
            {
                parameters[2].Value = Convert.ToDecimal(sxsp);

            }
            if (cxsp.Length > 0)
            {
                parameters[3].Value = Convert.ToDecimal(cxsp);
            }
            if (cql.Length > 0)
            {
                parameters[4].Value = Convert.ToDecimal(cql);
            }
            if (clds.Length > 0)
            {
                parameters[5].Value = Convert.ToDecimal(clds);
            }
            if (nd.Length > 0)
            {
                parameters[6].Value = Convert.ToDecimal(nd);
            }
            if (cytj.Length > 0)
            {
                parameters[7].Value = Convert.ToDecimal(cytj);
            }

            parameters[8].Value = txtHFResult;
            parameters[9].Value = KSJHFResult;
            parameters[10].Value = KWYSorceResult;

            if (C.Length > 0)
            {
                parameters[11].Value = Convert.ToDecimal(C);
            }
            if (RH.Length > 0)
            {
                parameters[12].Value = Convert.ToDecimal(RH);
            }
            if (KPA.Length > 0)
            {
                parameters[13].Value = Convert.ToDecimal(KPA);

            }

            if (KWYCQL.Length > 0)
            {
                parameters[14].Value = Convert.ToDecimal(KWYCQL);

            }

            if (key.Length > 0)
            {
                parameters[15].Value = key;

            }
            if (Monoxide.Length > 0)
            {
                parameters[16].Value = Monoxide;
            }
            if (hydrothion.Length > 0)
            {
                parameters[17].Value = hydrothion;
            }
            if (SulfurDioxide.Length > 0)
            {
                parameters[18].Value = SulfurDioxide;
            }

            parameters[19].Value = i_StandardVolume;



            ///////////////////////////////////////////////////////////////////////////
            StringBuilder strSqlBatch = new StringBuilder();
            strSqlBatch.Append("update RegGasBatch set ");

            strSqlBatch.Append("Acidity=@Acidity,");
            strSqlBatch.Append("KSJFHW=@KSJFHW,");
            strSqlBatch.Append("KWY=@KWY,");

            strSqlBatch.Append("Wet=@Wet");

            strSqlBatch.Append(" where BatchCode=@BatchCode");
            SqlParameter[] bparameters = {
					new SqlParameter("@Wet", SqlDbType.Decimal,9),
					new SqlParameter("@Acidity", SqlDbType.Decimal,9),
					new SqlParameter("@KSJFHW", SqlDbType.Decimal,9),
					new SqlParameter("@KWY", SqlDbType.Decimal,9),
					new SqlParameter("@BatchCode", SqlDbType.VarChar,20)};

            bparameters[0].Value = WaterQualityScoreResult;
            bparameters[1].Value = txtHFResult;
            bparameters[2].Value = KSJHFResult;
            bparameters[3].Value = KWYSorceResult;

            bparameters[4].Value = key;

            List<CommandInfo> lc = new List<CommandInfo>();
            lc.Add(new CommandInfo(strSql.ToString(), parameters));
            lc.Add(new CommandInfo(strSqlBatch.ToString(), bparameters));

            return DbHelperSQL.ExecuteSqlTran(lc);


        }
        public static DataTable Query(string sqlStr)
        {
            return DbHelperSQL.Query(sqlStr).Tables[0];
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EGMNGS.Common
{
    public class GlobalConstants
    {
        // action mode
        public const string ActionModeAdd = "Add";
        public const string ActionModeEdit = "EDIT";
        public const string ActionModeDelete = "Delete";
    }
    public enum GPStatusEnum
    {
        未抽空清洗 = 0,
        已抽空清洗 = 1,
        充装回收气 = 2,
        已充待检气 = 3,
        已充合格气 = 4,
        待送检 = 5,
        已报废 = 6
    }
}

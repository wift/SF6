﻿using EGMNGS.Model;
using RM.Common.DotNetCode;
using RM.Common.DotNetConfig;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OnlineCheck_HandlerLib
{

    /// <summary>
    /// PutDataToSystem 的摘要说明
    /// </summary>
    public class PutDataToSystem : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            //00成功，01user_name不能为空，
            //02pwd不能为空，03用户或密码有误。
            //04datas不能为空，05数据格式有误,
            //06保存到数据库有异常。
            string msgCode = string.Empty;

            context.Response.ContentType = "text/plain";

            //获取请求数据
            string username = context.Request["user_name"];
            string pwd = context.Request["pwd"];
            string datas = context.Request["datas"];
           
            //记录请求日志
            LogInstanseHelper.Instance.WriteDataLog(String.Format("username:{0},pwd:{1},datas:{2}", username, pwd, datas), "request");

            //验证请求数据
            if (string.IsNullOrEmpty(username))
            {
                msgCode = "01";
            }
            else if (string.IsNullOrEmpty(pwd))
            {
                msgCode = "02";
            }
            else if (string.IsNullOrEmpty(datas))
            {
                msgCode = "04";
            }

            if (!string.IsNullOrEmpty(msgCode))
            {
                LogInstanseHelper.Instance.WriteDataLog(msgCode, "response");
                context.Response.Write(Newtonsoft.Json.JsonConvert.SerializeObject(msgCode));
                context.Response.End();

            }

            //todo验证用户

            bool success = validate(username, pwd);
            if (success == false)
            {
                msgCode = "03";
            }

            if (!string.IsNullOrEmpty(msgCode))
            {
                LogInstanseHelper.Instance.WriteDataLog(Newtonsoft.Json.JsonConvert.SerializeObject(msgCode), "response");

                context.Response.Write(msgCode);
                context.Response.End();
            }

            //todo记录日志
            try
            {
                List<OCInstrumentInfoOnline> list = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OCInstrumentInfoOnline>>(datas);
                if (list.Count() > 0)
                {
                    //todo保存到数据库

                    List<String> listSql = new List<string>();
                    foreach (OCInstrumentInfoOnline item in list)
                    {
                        listSql.Add(string.Format(@"INSERT INTO [OCInstrumentInfoOnline]
           ([CheckPointCode]
           ,[CheckPointName]
           ,[Pressure]
           ,[MicroWater],CollectionDatetime,CollectionType,CollectionBy)
select '{0}',(select [CheckPointName] from OCInstrumentInfo where CheckPointCode='{0}'),{1},{2},'{3}',1,'root';", item.CheckPointCode, item.Pressure, item.MicroWater, item.CollectionDatetime));
                    }

                    //保存到数据库
                    int executeCount = Maticsoft.DBUtility.DbHelperSQL.ExecuteSqlTran(listSql);

                    if (executeCount > 0)
                    {
                        msgCode = "00";
                    }

                }

            }
            catch (Exception ex)
            {
                LogInstanseHelper.Instance.WriteDataLog(string.Format("exception:{0}", ex.Message), "exception");
                msgCode = "06";
            }

            LogInstanseHelper.Instance.WriteDataLog(msgCode, "response");

            context.Response.Write(msgCode);
            context.Response.End();
        }
        /// <summary>
        /// 验证
        /// </summary>
        /// <param name="username"></param>
        /// <param name="pwd"></param>
        /// <returns>true存在，false不存在</returns>
        private bool validate(string username, string pwd)
        {
            
            if (ConfigHelper.GetAppSettings("user_name").Equals(username) 
                && ConfigHelper.GetAppSettings("pwd").Equals(pwd))
            {
                return true;
            }

            return false;
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}
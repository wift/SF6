USE [EGMNGS]
GO

/****** Object:  Table [dbo].[OCInstrumentInfoOnline]    Script Date: 07/23/2016 22:22:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO



CREATE TABLE [dbo].[OCInstrumentInfoOnline](
	[OID] [int] IDENTITY(1,1) NOT NULL,
	[CheckPointCode] [varchar](4) NULL,
	[CheckPointName] [nvarchar](50) NULL,
	[Pressure] [decimal](18, 2) NULL,
	[Temperture] [decimal](18, 2) NULL,
	[MicroWater] [decimal](18, 2) NULL,
	[SO2] [decimal](18, 2) NULL,
	[CO] [decimal](18, 2) NULL,
	[CF4] [decimal](18, 2) NULL,
	[SO2F2] [decimal](18, 2) NULL,
	[SOF2] [decimal](18, 2) NULL,
	[CS2] [decimal](18, 2) NULL,
	[HF] [decimal](18, 2) NULL,
	[CollectionType] [varchar](4) NULL,
	[CollectionDatetime] [datetime] NULL,
	[CollectionBy] [nvarchar](50) NULL,
 CONSTRAINT [PK_OCInstrumentInfoOnline] PRIMARY KEY CLUSTERED 
(
	[OID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'OCInstrumentInfoOnline', @level2type=N'COLUMN',@level2name=N'OID'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'测点编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'OCInstrumentInfoOnline', @level2type=N'COLUMN',@level2name=N'CheckPointCode'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'测点名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'OCInstrumentInfoOnline', @level2type=N'COLUMN',@level2name=N'CheckPointName'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'压力' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'OCInstrumentInfoOnline', @level2type=N'COLUMN',@level2name=N'Pressure'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'温度' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'OCInstrumentInfoOnline', @level2type=N'COLUMN',@level2name=N'Temperture'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'微水' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'OCInstrumentInfoOnline', @level2type=N'COLUMN',@level2name=N'MicroWater'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SO2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'OCInstrumentInfoOnline', @level2type=N'COLUMN',@level2name=N'SO2'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'CO' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'OCInstrumentInfoOnline', @level2type=N'COLUMN',@level2name=N'CO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'CF4' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'OCInstrumentInfoOnline', @level2type=N'COLUMN',@level2name=N'CF4'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SO2F2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'OCInstrumentInfoOnline', @level2type=N'COLUMN',@level2name=N'SO2F2'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SOF2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'OCInstrumentInfoOnline', @level2type=N'COLUMN',@level2name=N'SOF2'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'CS2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'OCInstrumentInfoOnline', @level2type=N'COLUMN',@level2name=N'CS2'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'HF' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'OCInstrumentInfoOnline', @level2type=N'COLUMN',@level2name=N'HF'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'采集类型' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'OCInstrumentInfoOnline', @level2type=N'COLUMN',@level2name=N'CollectionType'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'采集时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'OCInstrumentInfoOnline', @level2type=N'COLUMN',@level2name=N'CollectionDatetime'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'采集人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'OCInstrumentInfoOnline', @level2type=N'COLUMN',@level2name=N'CollectionBy'
GO



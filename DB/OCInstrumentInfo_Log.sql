USE [EGMNGS]
GO

/****** Object:  Table [dbo].[OCInstrumentInfo_Log]    Script Date: 07/17/2016 20:38:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[OCInstrumentInfo_Log](
	[OID] [int] IDENTITY(1,1) NOT NULL,
	[FiledName] [varchar](50) NULL,
	[NewValue] [nvarchar](50) NULL,
	[OldValue] [nvarchar](50) NULL,
	[Updateby] [nvarchar](50) NULL,
	[UpdateDate] [datetime] NULL,
	[OCInstrumentInfo_OID] [int] NULL,
 CONSTRAINT [PK_OCInstrumentInfo_Log] PRIMARY KEY CLUSTERED 
(
	[OID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'OCInstrumentInfo_Log', @level2type=N'COLUMN',@level2name=N'OID'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'字段' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'OCInstrumentInfo_Log', @level2type=N'COLUMN',@level2name=N'FiledName'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'当前值' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'OCInstrumentInfo_Log', @level2type=N'COLUMN',@level2name=N'NewValue'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'旧值' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'OCInstrumentInfo_Log', @level2type=N'COLUMN',@level2name=N'OldValue'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'OCInstrumentInfo_Log', @level2type=N'COLUMN',@level2name=N'Updateby'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'OCInstrumentInfo_Log', @level2type=N'COLUMN',@level2name=N'UpdateDate'
GO



USE [EGMNGS]
GO

/****** Object:  Table [dbo].[OCInstrumentInfo]    Script Date: 07/17/2016 20:38:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO


CREATE TABLE [dbo].[OCInstrumentInfo](
	[OID] [int] IDENTITY(1,1) NOT NULL,
	[CheckPointCode] [varchar](4) NULL,
	[CheckPointName] [nvarchar](50) NULL,
	[InstrumentCode] [varchar](4) NULL,
	[InstrumentName] [nvarchar](50) NULL,
	[RunStatus] [varchar](4) NULL,
	[IntallPostion] [varchar](50) NULL,
	[BelongEquipment] [varchar](50) NULL,
	[BelongPowerStation] [varchar](50) NULL,
	[VoltageLevel] [varchar](4) NULL,
	[Manufacturer] [varchar](50) NULL,
	[RunDate] [datetime] NULL,
	[CalibrationDate] [datetime] NULL,
	[ControlRunStatus] [varchar](4) NULL,
	[ControlRunFromDate] [datetime] NULL,
	[ControlRunToDate] [datetime] NULL,
	[Cycle] [int] NULL,
	[ControlPressureMin] [decimal](18, 2) NULL,
	[ControlPressureMax] [decimal](18, 2) NULL,
	[ControlTempertureMin] [decimal](18, 2) NULL,
	[ControlTempertureMax] [decimal](18, 2) NULL,
	[ControlMicroWater] [decimal](18, 2) NULL,
	[ControlSO2] [decimal](18, 2) NULL,
	[ControlCO] [decimal](18, 2) NULL,
	[ControlCF4] [decimal](18, 2) NULL,
	[ControlSO2F2] [decimal](18, 2) NULL,
	[ControlSOF2] [decimal](18, 2) NULL,
	[ControlCS2] [decimal](18, 2) NULL,
	[ControlHF] [decimal](18, 2) NULL,
	[CREATED_BY] [nvarchar](12) NULL,
	[CREATED_DATE] [date] NULL,
	[LAST_UPD_BY] [nvarchar](12) NULL,
	[LAST_UPD_DATE] [date] NULL,
 CONSTRAINT [PK_OCInstrumentInfo] PRIMARY KEY CLUSTERED 
(
	[OID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'测点编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'OCInstrumentInfo', @level2type=N'COLUMN',@level2name=N'CheckPointCode'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'测点名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'OCInstrumentInfo', @level2type=N'COLUMN',@level2name=N'CheckPointName'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'仪器编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'OCInstrumentInfo', @level2type=N'COLUMN',@level2name=N'InstrumentCode'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'仪器名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'OCInstrumentInfo', @level2type=N'COLUMN',@level2name=N'InstrumentName'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'运行状态' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'OCInstrumentInfo', @level2type=N'COLUMN',@level2name=N'RunStatus'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'安装位置' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'OCInstrumentInfo', @level2type=N'COLUMN',@level2name=N'IntallPostion'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'所属设备' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'OCInstrumentInfo', @level2type=N'COLUMN',@level2name=N'BelongEquipment'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'所属变电站' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'OCInstrumentInfo', @level2type=N'COLUMN',@level2name=N'BelongPowerStation'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'电压等级' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'OCInstrumentInfo', @level2type=N'COLUMN',@level2name=N'VoltageLevel'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'生产厂家' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'OCInstrumentInfo', @level2type=N'COLUMN',@level2name=N'Manufacturer'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'投运日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'OCInstrumentInfo', @level2type=N'COLUMN',@level2name=N'RunDate'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'校准日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'OCInstrumentInfo', @level2type=N'COLUMN',@level2name=N'CalibrationDate'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'运行状态' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'OCInstrumentInfo', @level2type=N'COLUMN',@level2name=N'ControlRunStatus'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'运行开始时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'OCInstrumentInfo', @level2type=N'COLUMN',@level2name=N'ControlRunFromDate'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'运行结束时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'OCInstrumentInfo', @level2type=N'COLUMN',@level2name=N'ControlRunToDate'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'周期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'OCInstrumentInfo', @level2type=N'COLUMN',@level2name=N'Cycle'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'压力最小值' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'OCInstrumentInfo', @level2type=N'COLUMN',@level2name=N'ControlPressureMin'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'压力最大值' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'OCInstrumentInfo', @level2type=N'COLUMN',@level2name=N'ControlPressureMax'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'温度最小值' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'OCInstrumentInfo', @level2type=N'COLUMN',@level2name=N'ControlTempertureMin'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'温度最大值' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'OCInstrumentInfo', @level2type=N'COLUMN',@level2name=N'ControlTempertureMax'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'微水' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'OCInstrumentInfo', @level2type=N'COLUMN',@level2name=N'ControlMicroWater'
GO

ALTER TABLE [dbo].[OCInstrumentInfo] ADD  CONSTRAINT [DF_OCInstrumentInfo_CREATED_DATE]  DEFAULT (getdate()) FOR [CREATED_DATE]
GO

ALTER TABLE [dbo].[OCInstrumentInfo] ADD  CONSTRAINT [DF_OCInstrumentInfo_LAST_UPD_DATE]  DEFAULT (getdate()) FOR [LAST_UPD_DATE]
GO



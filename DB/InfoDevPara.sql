alter table InfoDevPara
add OutDevRunNo nvarchar(50)  --设备运行编码

alter table InfoDevPara
add OutDevID varchar(50)  --设备ID

alter table InfoDevPara
add OutDevUniqueCode varchar(100)

alter table InfoDevPara
add OutDevStatus nvarchar(50)

alter table InfoDevPara
add OutManufacturerCurName nvarchar(100)

alter table InfoDevPara
add OutProvider nvarchar(100)

alter table InfoDevPara
add OutVoltageLevel varchar(50)

alter table InfoDevPara
add OutDevClass nvarchar(100)


alter table InfoDevPara
add OutFullPath nvarchar(200)

alter table InfoDevPara
add OutGoinCurStatus date	
	
alter table InfoDevPara
add OutOriFactoryWarranty nvarchar(50)
 	
alter table InfoDevPara
add OutUnits nvarchar(10)
 
alter table InfoDevPara
add OutDevCoutOrLength int
 	
alter table InfoDevPara
add OutDevCPISDept nvarchar(50)
 			
alter table InfoDevPara
add OutStorageTeam nvarchar(50)
 		
alter table InfoDevPara
add OutIsPropertyDev nvarchar(10)	
 			
alter table InfoDevPara
add OutPropertyCode varchar(100)
 		
alter table InfoDevPara
add OutIntervalName nvarchar(50)


alter table InfoDevPara
alter column DevClass nvarchar(100)

alter table InfoDevPara
alter column VoltageLevel varchar(50)
USE [EGMNGS]
GO
/****** Object:  Trigger [dbo].[OCInstrumentInfo_Update]    Script Date: 07/17/2016 20:39:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create trigger [dbo].[OCInstrumentInfo_Update] 
on [dbo].[OCInstrumentInfo] 
for update 
as 
     declare @updateby nVARCHAR(100)
	 declare @oid int
	 declare @old nvarchar(200)  --修改之前的
	 declare @new nVARCHAR(200)  --修改之后的

	 
if (update (ControlRunStatus) 
OR update (ControlRunFromDate)
OR update (ControlRunToDate)
OR update (Cycle)
OR update (ControlPressureMin)
OR update (ControlPressureMax)
OR update (ControlTempertureMin)
OR update (ControlTempertureMax)
OR update (ControlMicroWater)
OR update (ControlSO2)
OR update (ControlCO)
OR update (ControlCF4)
OR update (ControlSO2F2)
OR update (ControlSOF2)
OR update (ControlCS2)
OR update (ControlHF)
)  --当Type字段被更新时，才会触发此触发器 

begin
	  SELECT @new= [Cycle],
	 @oid=[OID],
	  @updateby=[LAST_UPD_BY] FROM INSERTED --从更新后的副本表(临时表)里面 获得要修改后的状态
      
      select @old=[Cycle]
      from deleted  --从之前删掉的临时表里面获取原来的值
 
	 if @new!=@old
		begin
			insert into OCInstrumentInfo_Log(FiledName,NewValue,oldValue,Updateby,UpdateDate,OCInstrumentInfo_OID) 
			select 'Cycle',@new,@old,@updateby,getdate(),@oid

		END
		
		
	  SELECT @new= ControlCO
	  FROM INSERTED 
 
      select @old=ControlCO
      from deleted  
 
	  if @new!=@old
		begin
			insert into OCInstrumentInfo_Log(FiledName,NewValue,oldValue,Updateby,UpdateDate,OCInstrumentInfo_OID) 
			select 'ControlCO',@new,@old,@updateby,getdate(),@oid 
		END   
		
		
		 SELECT @new= ControlRunStatus
	  FROM INSERTED 
 
      select @old=ControlRunStatus
      from deleted  
 
	  if @new!=@old
		begin
			insert into OCInstrumentInfo_Log(FiledName,NewValue,oldValue,Updateby,UpdateDate,OCInstrumentInfo_OID) 
			select 'ControlRunStatus',@new,@old,@updateby,getdate(),@oid 
		END  
		
		
	  SELECT @new= ControlRunFromDate
	  FROM INSERTED 
 
      select @old=ControlRunFromDate
      from deleted  
 
	  if @new!=@old
		begin
			insert into OCInstrumentInfo_Log(FiledName,NewValue,oldValue,Updateby,UpdateDate,OCInstrumentInfo_OID) 
			select 'ControlRunFromDate',@new,@old,@updateby,getdate(),@oid 
		END  
		
		
		  SELECT @new= ControlRunToDate
	  FROM INSERTED 
 
      select @old=ControlRunToDate
      from deleted  
 
	  if @new!=@old
		begin
			insert into OCInstrumentInfo_Log(FiledName,NewValue,oldValue,Updateby,UpdateDate,OCInstrumentInfo_OID) 
			select 'ControlRunToDate',@new,@old,@updateby,getdate(),@oid 
		END  
		
		
			  SELECT @new= ControlPressureMin
	  FROM INSERTED 
 
      select @old=ControlPressureMin
      from deleted  
 
	  if @new!=@old
		begin
			insert into OCInstrumentInfo_Log(FiledName,NewValue,oldValue,Updateby,UpdateDate,OCInstrumentInfo_OID) 
			select 'ControlPressureMin',@new,@old,@updateby,getdate(),@oid 
		END  
		
		
				  SELECT @new= ControlPressureMax
	  FROM INSERTED 
 
      select @old=ControlPressureMax
      from deleted  
 
	  if @new!=@old
		begin
			insert into OCInstrumentInfo_Log(FiledName,NewValue,oldValue,Updateby,UpdateDate,OCInstrumentInfo_OID) 
			select 'ControlPressureMax',@new,@old,@updateby,getdate(),@oid 
		END  
		
		
		  SELECT @new= ControlTempertureMin
	  FROM INSERTED 
 
      select @old=ControlTempertureMin
      from deleted  
 
	  if @new!=@old
		begin
			insert into OCInstrumentInfo_Log(FiledName,NewValue,oldValue,Updateby,UpdateDate,OCInstrumentInfo_OID) 
			select 'ControlTempertureMin',@new,@old,@updateby,getdate(),@oid 
		END  
		
		
		SELECT @new= ControlTempertureMax
	  FROM INSERTED 
 
      select @old=ControlTempertureMax
      from deleted  
 
	  if @new!=@old
		begin
			insert into OCInstrumentInfo_Log(FiledName,NewValue,oldValue,Updateby,UpdateDate,OCInstrumentInfo_OID) 
			select 'ControlTempertureMax',@new,@old,@updateby,getdate() ,@oid
		END  
		
		
		SELECT @new= ControlMicroWater
	  FROM INSERTED 
 
      select @old=ControlMicroWater
      from deleted  
 
	  if @new!=@old
		begin
			insert into OCInstrumentInfo_Log(FiledName,NewValue,oldValue,Updateby,UpdateDate,OCInstrumentInfo_OID) 
			select 'ControlMicroWater',@new,@old,@updateby,getdate() ,@oid
		END  
		
		
		SELECT @new= ControlSO2
	  FROM INSERTED 
 
      select @old=ControlSO2
      from deleted  
 
	  if @new!=@old
		begin
			insert into OCInstrumentInfo_Log(FiledName,NewValue,oldValue,Updateby,UpdateDate,OCInstrumentInfo_OID) 
			select 'ControlSO2',@new,@old,@updateby,getdate() ,@oid
		END 
		
		
		SELECT @new= ControlCF4
	  FROM INSERTED 
 
      select @old=ControlCF4
      from deleted  
 
	  if @new!=@old
		begin
			insert into OCInstrumentInfo_Log(FiledName,NewValue,oldValue,Updateby,UpdateDate,OCInstrumentInfo_OID) 
			select 'ControlCF4',@new,@old,@updateby,getdate() ,@oid
		END 
		
		SELECT @new= ControlSO2F2
	  FROM INSERTED 
 
      select @old=ControlSO2F2
      from deleted  
 
	  if @new!=@old
		begin
			insert into OCInstrumentInfo_Log(FiledName,NewValue,oldValue,Updateby,UpdateDate,OCInstrumentInfo_OID) 
			select 'ControlSO2F2',@new,@old,@updateby,getdate() ,@oid
		END 
		
		
		
		SELECT @new= ControlSOF2
	  FROM INSERTED 
 
      select @old=ControlSOF2
      from deleted  
 
	  if @new!=@old
		begin
			insert into OCInstrumentInfo_Log(FiledName,NewValue,oldValue,Updateby,UpdateDate,OCInstrumentInfo_OID) 
			select 'ControlSOF2',@new,@old,@updateby,getdate(),@oid 
		END
		
		
		
		SELECT @new= ControlCS2
	  FROM INSERTED 
 
      select @old=ControlCS2
      from deleted  
 
	  if @new!=@old
		begin
			insert into OCInstrumentInfo_Log(FiledName,NewValue,oldValue,Updateby,UpdateDate,OCInstrumentInfo_OID) 
			select 'ControlCS2',@new,@old,@updateby,getdate() ,@oid
		END
		
		
		
		SELECT @new= ControlHF
	  FROM INSERTED 
 
      select @old=ControlHF
      from deleted  
 
	  if @new!=@old
		begin
			insert into OCInstrumentInfo_Log(FiledName,NewValue,oldValue,Updateby,UpdateDate,OCInstrumentInfo_OID) 
			select 'ControlHF',@new,@old,@updateby,getdate(),@oid
		END
		
	
end

-----2016-08-16-------------
alter table dbo.InfoDevPara
alter column CREATED_BY varchar(50)

alter table dbo.InfoDevPara
alter column LAST_UPD_BY varchar(50)


-----2016-08-11-------------
ALTER TABLE InfoDevPara
ADD KV INT 

ALTER TABLE InfoDevPara
ADD A INT 

ALTER TABLE InfoDevPara
ADD KA INT 


ALTER TABLE OCInstrumentInfo
ADD BelongEquipmentName VARCHAR(50)

ALTER TABLE OCInstrumentInfo
ADD BelongPowerStationName VARCHAR(50)


ALTER TABLE OCInstrumentInfo
ADD ControlPressureMins DECIMAL(18,2)

ALTER TABLE OCInstrumentInfo
ADD ControlPressureMaxs DECIMAL(18,2)

ALTER TABLE OCInstrumentInfo
ADD ControlTempertureMins DECIMAL(18,2)

ALTER TABLE OCInstrumentInfo
ADD ControlTempertureMaxs DECIMAL(18,2)

ALTER TABLE OCInstrumentInfo
ADD ControlMicroWaters DECIMAL(18,2)

ALTER TABLE OCInstrumentInfo
ADD ControlSO2s DECIMAL(18,2)

ALTER TABLE OCInstrumentInfo
ADD ControlCOs DECIMAL(18,2)

ALTER TABLE OCInstrumentInfo
ADD ControlCF4s DECIMAL(18,2)

ALTER TABLE OCInstrumentInfo
ADD ControlSO2F2s DECIMAL(18,2)

ALTER TABLE OCInstrumentInfo
ADD ControlSOF2s DECIMAL(18,2)

ALTER TABLE OCInstrumentInfo
ADD ControlCS2s DECIMAL(18,2)

ALTER TABLE OCInstrumentInfo
ADD ControlHFs DECIMAL(18,2)



if exists (select 1
            from  sysobjects
           where  id = object_id('OCInstrumentInfo_Childs')
            and   type = 'U')
   drop table OCInstrumentInfo_Childs
go

/*==============================================================*/
/* Table: OCInstrumentInfo_Childs                               */
/*==============================================================*/
create table OCInstrumentInfo_Childs (
   OID                  int                  identity,
   CheckPointCode       varchar(50)          null,
   TopTarget            varchar(10)          null,
   BottomTarget         varchar(10)          null,
   MaxValue             float                null,
   constraint PK_OCINSTRUMENTINFO_CHILDS primary key (OID)
)
go

declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '预警比例',
   'user', @CurrentUser, 'table', 'OCInstrumentInfo_Childs'
go

declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '主键',
   'user', @CurrentUser, 'table', 'OCInstrumentInfo_Childs', 'column', 'OID'
go

declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '分子',
   'user', @CurrentUser, 'table', 'OCInstrumentInfo_Childs', 'column', 'TopTarget'
go

declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '分母',
   'user', @CurrentUser, 'table', 'OCInstrumentInfo_Childs', 'column', 'BottomTarget'
go

declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '预警值',
   'user', @CurrentUser, 'table', 'OCInstrumentInfo_Childs', 'column', 'MaxValue'
go




if exists (select 1
            from  sysobjects
           where  id = object_id('OCInstrumentElectricalData')
            and   type = 'U')
   drop table OCInstrumentElectricalData
go

/*==============================================================*/
/* Table: OCInstrumentElectricalData                            */
/*==============================================================*/
create table OCInstrumentElectricalData (
   OID                  int                  identity,
   CheckPointCode       varchar(4)           null,
   CheckPointName       varchar(50)          null,
   CollectionDatetime   datetime             null,
   Pressure             decimal(18,2)        null,
   Temperture           decimal(18,2)        null,
   OutElectricity       decimal(18,2)        null,
   CurrentVal           decimal(18,2)        null,
   Voltage              decimal(18,2)        null,
   PowerVal             decimal(18,2)        null,
   CollectionType       varchar(4)           null,
   CollectionBy         nvarchar(50)         null,
   constraint PK_OCINSTRUMENTELECTRICALDATA primary key (OID)
)
go

declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '电气常量数据',
   'user', @CurrentUser, 'table', 'OCInstrumentElectricalData'
go



ALTER TABLE OCInstrumentInfo
ADD AlterCount int


ALTER TABLE dbo.ApplRegGasQtyInspection
ADD C2F6 DECIMAL(18,2)

ALTER TABLE dbo.ApplRegGasQtyInspection
ADD C3F8 DECIMAL(18,2)


--20161004

ALTER TABLE OCInstrumentInfo
ADD PH2S DECIMAL(18,2)

ALTER TABLE OCInstrumentInfo
ADD MH2S DECIMAL(18,2)

ALTER TABLE OCInstrumentInfo
ADD PCOS DECIMAL(18,2)

ALTER TABLE OCInstrumentInfo
ADD MCOS DECIMAL(18,2)

ALTER TABLE OCInstrumentInfo
ADD PC2F6 DECIMAL(18,2)

ALTER TABLE OCInstrumentInfo
ADD MC2F6 DECIMAL(18,2)

ALTER TABLE OCInstrumentInfo
ADD PC3F8 DECIMAL(18,2)

ALTER TABLE OCInstrumentInfo
ADD MC3F8 DECIMAL(18,2)



ALTER TABLE OCInstrumentInfoOnline
ADD H2S DECIMAL(18,2)

ALTER TABLE OCInstrumentInfoOnline
ADD COSS DECIMAL(18,2)

ALTER TABLE OCInstrumentInfoOnline
ADD C2F6 DECIMAL(18,2)

ALTER TABLE OCInstrumentInfoOnline
ADD C3F8 DECIMAL(18,2)



   ALTER TABLE   Magnetic_CheckPointInfo
   ADD VoltageLevel VARCHAR(4)
   
   ALTER TABLE   Magnetic_CheckPointInfo
   ADD Electricity FLOAT
   
   ALTER TABLE   Magnetic_CheckPointInfo
   ADD Eload  FLOAT
   
   ALTER TABLE   Magnetic_CheckPointInfo
   ADD Remarks  NVARCHAR(300)
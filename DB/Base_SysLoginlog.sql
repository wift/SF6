USE [RM_DB]
GO

/****** Object:  Table [dbo].[Base_SysLoginlog]    Script Date: 07/25/2016 20:43:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Base_SysLoginlog](
	[SYS_LOGINLOG_ID] [dbo].[Name(50)] NULL,
	[SYS_LOGINLOG_IP] [dbo].[Name(50)] NULL,
	[SYS_LOGINLOG_TIME] [Date] NULL,
	[User_Account] [dbo].[Name(50)] NULL,
	[SYS_LOGINLOG_STATUS] [dbo].[ID] NULL,
	[OWNER_address] [dbo].[Name(200)] NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��¼��־����' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Base_SysLoginlog', @level2type=N'COLUMN',@level2name=N'SYS_LOGINLOG_ID'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��¼IP' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Base_SysLoginlog', @level2type=N'COLUMN',@level2name=N'SYS_LOGINLOG_IP'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��¼ʱ��' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Base_SysLoginlog', @level2type=N'COLUMN',@level2name=N'SYS_LOGINLOG_TIME'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��¼�˻�' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Base_SysLoginlog', @level2type=N'COLUMN',@level2name=N'User_Account'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��¼״̬' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Base_SysLoginlog', @level2type=N'COLUMN',@level2name=N'SYS_LOGINLOG_STATUS'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'IP���ڵ�ַ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Base_SysLoginlog', @level2type=N'COLUMN',@level2name=N'OWNER_address'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'��¼��־' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Base_SysLoginlog'
GO

ALTER TABLE [dbo].[Base_SysLoginlog] ADD  CONSTRAINT [DF_Base_SysLoginlog_SYS_LOGINLOG_TIME]  DEFAULT (getdate()) FOR [SYS_LOGINLOG_TIME]
GO



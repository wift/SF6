﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;

namespace RM.Web.ExControl
{
    public class ExTextBox : System.Web.UI.WebControls.TextBox
    {
        protected override void OnTextChanged(EventArgs e)
        {

            object pageBase = this.Page;
            object model = pageBase.GetType().GetProperty(bindName).GetValue(pageBase, null);

            if (model != null && this.Enabled == true)
            {
                Type t = model.GetType();

                t.GetProperty(fieldName).SetValue(model, ChangeType(this.Text, t.GetProperty(fieldName).PropertyType), null);
            }
            base.OnTextChanged(e);
        }

        private Object ChangeType(object value, Type targetType)
        {
            Type convertType = targetType;
            if (targetType.IsGenericType && targetType.GetGenericTypeDefinition().Equals(typeof(Nullable<>)))
            {
                NullableConverter nullableConverter = new NullableConverter(targetType);
                convertType = nullableConverter.UnderlyingType;

            }
            return Convert.ChangeType(value, convertType);
        }
        protected override void OnPreRender(EventArgs e)
        {

            object pageBase = this.Page;
            object model = pageBase.GetType().GetProperty(bindName).GetValue(pageBase, null);

            if (model != null)
            {
                if (!string.IsNullOrEmpty(strFormat))
                {
                    this.Text = string.Format("{0:" + strFormat + "}", model.GetType().GetProperty(fieldName).GetValue(model, null));
                }
                else
                {
                    this.Text = Convert.ToString(model.GetType().GetProperty(fieldName).GetValue(model, null));
                }

            }

            base.OnPreRender(e);


        }

        private string strFormat;

        public string StrFormat
        {
            get { return strFormat; }
            set { strFormat = value; }
        }
        private string bindName;

        public string BindName
        {
            get { return bindName; }
            set { bindName = value; }
        }

        private string fieldName;

        public string FieldName
        {
            get { return fieldName; }
            set { fieldName = value; }
        }

    }
}
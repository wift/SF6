﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using System.ComponentModel;

namespace RM.Web.ExControl
{

    public class ExDropDownList : DropDownList
    {

        private Object ChangeType(object value, Type targetType)
        {
            Type convertType = targetType;
            if (targetType.IsGenericType && targetType.GetGenericTypeDefinition().Equals(typeof(Nullable<>)))
            {
                NullableConverter nullableConverter = new NullableConverter(targetType);
                convertType = nullableConverter.UnderlyingType;

            }
            return Convert.ChangeType(value, convertType);
        }

        protected override void OnTextChanged(EventArgs e)
        {

            object pageBase = this.Page;
            object model = pageBase.GetType().GetProperty(bindName).GetValue(pageBase, null);

            if (model != null && this.Enabled == true)
            {
                Type t = model.GetType();

                t.GetProperty(fieldName).SetValue(model, this.Items[this.SelectedIndex].Value, null);
            }
            base.OnTextChanged(e);
        }

        protected override void OnPreRender(EventArgs e)
        {
            object pageBase = this.Page;
            object model = pageBase.GetType().GetProperty(bindName).GetValue(pageBase, null);

            if (model != null)
            {
                var value = this.Items.FindByValue(Convert.ToString(model.GetType().GetProperty(fieldName).GetValue(model, null)));
                if (value != null)
                {
                    this.SelectedValue = Convert.ToString(model.GetType().GetProperty(fieldName).GetValue(model, null));

                }
            }
            base.OnPreRender(e);

        }

        private string bindName;

        public string BindName
        {
            get { return bindName; }
            set { bindName = value; }
        }

        private string fieldName;

        public string FieldName
        {
            get
            {
                if (this.DataValueField.Length > 0)
                {
                    fieldName = this.DataValueField;
                }

                return fieldName;
            }
            set { fieldName = value; }
        }

  
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;
using RM.Common.DotNetUI;
using RM.Web.App_Code;
using Microsoft.Office.Interop.Excel;
using RM.Common.DotNetFile;
using EGMNGS.Common;

namespace RM.Web.Report
{
    public partial class RPTDevCheckUseGas : PageBase
    {
        EGMNGS.BLL.InfoDevChangeFill bll = new EGMNGS.BLL.InfoDevChangeFill();
        private string sqlStr
        {
            get
            {
                if (ViewState["sqlStr"] == null)
                {
                    ViewState["sqlStr"] = "1=0";
                }
                return ViewState["sqlStr"] as string;
            }
            set { ViewState["sqlStr"] = value; }
        }
        private System.Data.DataTable ResultTB
        {
            get
            {
                if (ViewState["ResultTB"] == null)
                {
                    return new System.Data.DataTable();
                }
                return ViewState["ResultTB"] as System.Data.DataTable;
            }
            set { ViewState["ResultTB"] = value; }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            ScriptManager.GetCurrent(this.Page).RegisterPostBackControl(this.btnExport);
            if (!IsPostBack)
            {
                DropDownListBinder();
            }
        }

        private void DropDownListBinder()
        {
            System.Data.DataTable dtPowerStation = ComServies.GetAllPowerStation();
            this.ddlPowerSupplyName.DataSource = dtPowerStation;
            this.ddlPowerSupplyName.DataTextField = "Organization_Name";
            this.ddlPowerSupplyName.DataValueField = "Organization_ID";
            this.ddlPowerSupplyName.DataBind();
            this.ddlPowerSupplyName.Items.Insert(0, string.Empty);
        }


        private void DataBindGrid()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("1=1");
            if (this.txtBeginDate.Text.Length > 0)
            {
                sb.AppendFormat("and FillGasdate>cast('{0}' as datetime) ", this.txtBeginDate.Text.Trim());
            }
            if (this.txtEndDate.Text.Length > 0)
            {
                sb.AppendFormat("and FillGasdate<cast('{0}' as datetime) ", this.txtEndDate.Text.Trim());
            }
            if (ddlPowerSupplyName.SelectedIndex > 0)
            {
                sb.AppendFormat(" and PowerSupplyName='{0}'", ddlPowerSupplyName.SelectedItem.Text);
                if (ddlConvertStationName.SelectedIndex > 0)
                {
                    sb.AppendFormat(" and ConvertStationName='{0}'", ddlConvertStationName.SelectedItem.Text);
                }

            }

            //sqlStr = sb.ToString();
            //if (sqlStr.Length == 0)
            //{
            //    sqlStr = "1=1";
            //}
            DataSet ds = bll.GetList(sb.ToString());
            ControlBindHelper.BindRepeaterList(ds.Tables[0], rp_Item);
            ResultTB = ds.Tables[0];
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            DataBindGrid();
        }

        protected void ddlPowerSupplyName_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadPowerSupplyName();

        }
        private void LoadPowerSupplyName()
        {
            this.ddlConvertStationName.Items.Clear();
            EGMNGS.BLL.InfoPowerSupplyConvertStation bll = new EGMNGS.BLL.InfoPowerSupplyConvertStation();
            DataSet dsPowerSupplyCode = bll.GetList(string.Format("PowerSupplyCode='{0}'", this.ddlPowerSupplyName.SelectedValue));
            this.ddlConvertStationName.DataSource = dsPowerSupplyCode.Tables[0];
            this.ddlConvertStationName.DataTextField = "ConvartStationName";
            this.ddlConvertStationName.DataValueField = "CovnertStationCode";
            this.ddlConvertStationName.DataBind();
            this.ddlConvertStationName.Items.Insert(0, string.Empty);
        }
        /// <summary>
        /// 导出
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnExport_Click(object sender, EventArgs e)
        {

            MouldExportExcel mee = new MouldExportExcel();
            try
            {
                string physicalPath = Request.PhysicalApplicationPath + "Xls\\";
                Workbook sh = mee.OpenExcel(physicalPath + "设备检修用气.xls");
                Worksheet ws = sh.Worksheets[1] as Worksheet;

                #region 设置值

                for (int i = 0; i < ResultTB.Rows.Count; i++)
                {
                    DataRow item = ResultTB.Rows[i];
                    mee.SetCellValue(ws, 3 + i, 1, i + 1);
                    mee.SetCellValue(ws, 3 + i, 2, item["PowerSupplyName"].ToString());
                    mee.SetCellValue(ws, 3 + i, 3, item["ConvertStationName"].ToString());
                    mee.SetCellValue(ws, 3 + i, 4, item["DevCode"].ToString());
                    mee.SetCellValue(ws, 3 + i, 5, item["DevName"].ToString());
                    mee.SetCellValue(ws, 3 + i, 6, item["RatedQL"].ToString());
                    mee.SetCellValue(ws, 3 + i, 7, item["RatedQY"].ToString());
                    mee.SetCellValue(ws, 3 + i, 8, Convert.ToDateTime(item["FillGasdate"]).ToShortDateString());
                    mee.SetCellValue(ws, 3 + i, 9, item["Operataff"].ToString());
                    mee.SetCellValue(ws, 3 + i, 10, item["AmountFillGas"].ToString());
                    mee.SetCellValue(ws, 3 + i, 11, item["AmountPressFillGassbefore"].ToString());
                    mee.SetCellValue(ws, 3 + i, 12, item["AmountPressFillGassafter"].ToString());

                }

                mee.SetCellValue(ws, 3 + ResultTB.Rows.Count, 10, string.Format("=SUM(J3:J{0})", 3 + ResultTB.Rows.Count - 1));

                mee.SetCellValue(ws, 3 + ResultTB.Rows.Count, 1, "小计");
                #endregion





                string fileName = "设备检修用气temp.xls";
                string pathFile = physicalPath + fileName;
                // FileHelper.DeleteFile(pathFile);
                mee.SaveExcel(pathFile);
                //  ShowMsgHelper.Alert("导出成功!");

                FileDownHelper.DownLoadold(@"~/Xls/" + fileName);

            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                mee.Dispose();

            }
        }

    }
}
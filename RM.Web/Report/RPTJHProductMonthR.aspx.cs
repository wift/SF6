﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;
using RM.Web.App_Code;
using RM.Common.DotNetFile;
using Microsoft.Office.Interop.Excel;

namespace RM.Web.Report
{
    public partial class RPTJHProductMonthR : System.Web.UI.Page
    {
        public System.Data.DataTable ResultTB
        {
            get
            {
                if (ViewState["ResultTB"] == null)
                {
                    return new System.Data.DataTable();
                }
                return ViewState["ResultTB"] as System.Data.DataTable;
            }
            set { ViewState["ResultTB"] = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                DropDownListBinder();
            }
            ScriptManager.GetCurrent(this.Page).RegisterPostBackControl(this.btnExport);
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            StringBuilder sb = new StringBuilder();

            string sql = @"select 
(select ISNULL(sum(AmountRecovery),0.00)
from dbo.ApplRecyGasReg where (Status=1 or Status=2) and Year={0} and Month={1})  '回收气体申请量' ,

(select ISNULL(sum(AmountRecovery),0.00)
from dbo.ApplRecyGasReg where Status=2 and Year={0} and Month={1}) '已回收气体量',

(select ISNULL(SUM(AmountPurchased),0.00) from dbo.RegGasProcurement
where Status='1' and Year={0} and Month={1}) '新购气体量',

(select ISNULL(SUM(AmountGas),0.00) from dbo.RegGasBatch
where Status='1' and Year={0} and Month={1}) '处理总量',

(select ISNULL(SUM(b.AmountGas),0.00) from dbo.ApplRegGasQtyInspection a,BookGasFill b
where a.GasSourse in ('1','3') and b.GasCode=a.GasCdoe
and a.FlowStatus in ('0','1') and b.Year={0} and b.Month={1}) '未检测',

(select ISNULL(SUM(b.AmountGas),0.00) from dbo.ApplRegGasQtyInspection a,BookGasFill b
where a.GasSourse in ('1','3')  and b.GasCode=a.GasCdoe and b.Year={0} and b.Month={1}
and a.IsPass='1') '已检合格',


(select ISNULL(SUM(b.AmountGas),0.0) from dbo.ApplRegGasQtyInspection a,BookGasFill b
where a.GasSourse in ('1','3') and b.GasCode=a.GasCdoe and b.Year={0} and b.Month={1}
and a.IsPass='0') '已检不合格',

(select ISNULL(SUM(AirDemand),0.00) from dbo.ApplNeedGasReg where Status='2' and Year={0} and Month={1}) '需气申请量',

(select ISNULL(SUM(AmountOutStorage),0.00) from TableGasOutStorage
where Status='1' 
and GasClass='3' and Year={0} and Month={1}) '需气发送量',

(select ISNULL(SUM(AmountDetection),0.00) from dbo.ApplDetectionGas a
where a.Status='2' and YEAR(a.RegistantsDate)={0} and Month(a.RegistantsDate)={1}) '入网检测申请量',

(select ISNULL(SUM(AmountDetection),0.00) from dbo.ApplDetectionGas a
where a.Status='2' and YEAR(a.RegistantsDate)={0} and Month(a.RegistantsDate)={1}) '已入网检测量'
";
            //(select  ISNULL(SUM(b.AmountGas),0.00) from dbo.ApplRegGasQtyInspection a,BookGasFill b
//where a.GasSourse='2' and b.GasCode=a.GasCdoe and b.Year={0} and b.Month={1}
//and a.FlowStatus not in ('0','1'))
            //
            string mergeSql = @"select 
(select ISNULL(sum(AmountRecovery),0.00)
from dbo.ApplRecyGasReg where (Status=1 or Status=2) and Year={0})  '回收气体申请量' ,

(select ISNULL(sum(AmountRecovery),0.00)
from dbo.ApplRecyGasReg where Status=2 and Year={0}) '已回收气体量',

(select ISNULL(SUM(AmountPurchased),0.00) from dbo.RegGasProcurement
where Status='1' and Year={0}) '新购气体量',

(select ISNULL(SUM(AmountGas),0.00) from dbo.RegGasBatch
where Status='1' and Year={0}) '处理总量',

(select ISNULL(SUM(b.AmountGas),0.00) from dbo.ApplRegGasQtyInspection a,BookGasFill b
where a.GasSourse in ('1','3') and b.GasCode=a.GasCdoe
and a.FlowStatus in ('0','1') and b.Year={0}) '未检测',

(select ISNULL(SUM(b.AmountGas),0.00) from dbo.ApplRegGasQtyInspection a,BookGasFill b
where a.GasSourse in ('1','3')  and b.GasCode=a.GasCdoe and b.Year={0}
and a.IsPass='1') '已检合格',


(select ISNULL(SUM(b.AmountGas),0.00) from dbo.ApplRegGasQtyInspection a,BookGasFill b
where a.GasSourse in ('1','3') and b.GasCode=a.GasCdoe and b.Year={0}
and a.IsPass='0') '已检不合格',

(select ISNULL(SUM(AirDemand),0.00) from dbo.ApplNeedGasReg where Status='2' and Year={0}) '需气申请量',

(select ISNULL(SUM(AmountOutStorage),0.00) from TableGasOutStorage
where Status='1' 
and GasClass='3' and Year={0}) '需气发送量',

(select ISNULL(SUM(AmountDetection),0.00) from dbo.ApplDetectionGas a
where a.Status='2' and YEAR(a.RegistantsDate)={0}) '入网检测申请量',

(select ISNULL(SUM(AmountDetection),0.00) from dbo.ApplDetectionGas a
where a.Status='2' and YEAR(a.RegistantsDate)={0}) '已入网检测量'
";

//(select  ISNULL(SUM(b.AmountGas),0.00) from dbo.ApplRegGasQtyInspection a,BookGasFill b
//where a.GasSourse='2' and b.GasCode=a.GasCdoe and b.Year={0}
//and a.FlowStatus not in ('0','1'))
            for (int i = 1; i <= 12; i++)
            {
                sb.AppendLine();
                sb.AppendFormat(sql, this.ddlYear.SelectedValue, i);
                sb.AppendLine();

                sb.Append("union all");
            }
            sb.AppendLine();
            sb.AppendFormat(mergeSql, this.ddlYear.SelectedValue);
            DataSet ds = EGMNGS.Common.ComServies.GetRPTJHProductMonthR(sb.ToString());
            ResultTB = ds.Tables[0];
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            MouldExportExcel mee = new MouldExportExcel();
            try
            {
                string physicalPath = Request.PhysicalApplicationPath + "Xls\\";
                Workbook sh = mee.OpenExcel(physicalPath + "净化生产月报.xls");
                Worksheet ws = sh.Worksheets[1] as Worksheet;

                #region 设置值



                for (int i = 0; i < 12; i++)
                {
                    DataRow item = ResultTB.Rows[i];
                    mee.SetCellValue(ws, 4 + i, 1, string.Format("{0}年{1}月", this.ddlYear.SelectedValue, (i + 1).ToString("D2")));

                    mee.SetCellValue(ws, 4 + i, 2, item[0].ToString());
                    mee.SetCellValue(ws, 4 + i, 3, item[1].ToString());
                    mee.SetCellValue(ws, 4 + i, 4, item[2].ToString());
                    mee.SetCellValue(ws, 4 + i, 5, item[3].ToString());
                    mee.SetCellValue(ws, 4 + i, 6, item[4].ToString());
                    mee.SetCellValue(ws, 4 + i, 7, item[5].ToString());
                    mee.SetCellValue(ws, 4 + i, 8, item[6].ToString());
                    mee.SetCellValue(ws, 4 + i, 9, item[7].ToString());
                    mee.SetCellValue(ws, 4 + i, 10, item[8].ToString());
                    mee.SetCellValue(ws, 4 + i, 11, item[9].ToString());
                    mee.SetCellValue(ws, 4 + i, 12, item[10].ToString());
                }
                #endregion





                string fileName = "净化生产月报temp.xls";
                string pathFile = physicalPath + fileName;
                // FileHelper.DeleteFile(pathFile);
                mee.SaveExcel(pathFile);
                //  ShowMsgHelper.Alert("导出成功!");

                FileDownHelper.DownLoadold(@"~/Xls/" + fileName);

            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                mee.Dispose();

            }
        }


        private void DropDownListBinder()
        {


            EGMNGS.BLL.SYS_CODE sysCodeBll = new EGMNGS.BLL.SYS_CODE();
            DataSet dsYear = sysCodeBll.GetList("CODE_TYPE='Year' ORDER BY DISPLAY_ORDER");

            this.ddlYear.DataSource = dsYear.Tables[0];
            this.ddlYear.DataTextField = "CODE_CHI_DESC";
            this.ddlYear.DataValueField = "CODE";
            this.ddlYear.DataBind();

            this.ddlYear.SelectedValue = DateTime.Now.Year.ToString();
        }
    }
}
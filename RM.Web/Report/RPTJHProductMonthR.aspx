﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RPTJHProductMonthR.aspx.cs"
    Inherits="RM.Web.Report.RPTJHProductMonthR" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>净化生产月报</title>
    <style type="text/css">
        .style1
        {
            height: 32.25pt;
            width: 797pt;
            color: windowtext;
            font-size: 20.0pt;
            font-weight: 700;
            font-style: normal;
            text-decoration: none;
            font-family: 宋体;
            text-align: center;
            vertical-align: middle;
            white-space: nowrap;
            border: .5pt solid windowtext;
            padding-left: 1px;
            padding-right: 1px;
            padding-top: 1px;
        }
        .style2
        {
            height: 27.0pt;
            width: 65pt;
            color: windowtext;
            font-size: 10.0pt;
            font-weight: 700;
            font-style: normal;
            text-decoration: none;
            font-family: 宋体;
            text-align: center;
            vertical-align: middle;
            white-space: normal;
            border: .5pt solid windowtext;
            padding-left: 1px;
            padding-right: 1px;
            padding-top: 1px;
        }
        .style3
        {
            width: 64pt;
            color: windowtext;
            font-size: 10.0pt;
            font-weight: 700;
            font-style: normal;
            text-decoration: none;
            font-family: 宋体;
            text-align: center;
            vertical-align: middle;
            white-space: normal;
            border-left: .5pt solid windowtext;
            border-right: .5pt solid windowtext;
            border-top: .5pt solid windowtext;
            border-bottom-style: none;
            border-bottom-color: inherit;
            border-bottom-width: medium;
            padding-left: 1px;
            padding-right: 1px;
            padding-top: 1px;
        }
        .style4
        {
            width: 58pt;
            color: windowtext;
            font-size: 10.0pt;
            font-weight: 700;
            font-style: normal;
            text-decoration: none;
            font-family: 宋体;
            text-align: center;
            vertical-align: middle;
            white-space: normal;
            border-left: .5pt solid windowtext;
            border-right: .5pt solid windowtext;
            border-top: .5pt solid windowtext;
            border-bottom-style: none;
            border-bottom-color: inherit;
            border-bottom-width: medium;
            padding-left: 1px;
            padding-right: 1px;
            padding-top: 1px;
        }
        .style5
        {
            width: 65pt;
            color: windowtext;
            font-size: 10.0pt;
            font-weight: 700;
            font-style: normal;
            text-decoration: none;
            font-family: 宋体;
            text-align: center;
            vertical-align: middle;
            white-space: normal;
            border-left: .5pt solid windowtext;
            border-right: .5pt solid windowtext;
            border-top: .5pt solid windowtext;
            border-bottom-style: none;
            border-bottom-color: inherit;
            border-bottom-width: medium;
            padding-left: 1px;
            padding-right: 1px;
            padding-top: 1px;
        }
        .style6
        {
            width: 86pt;
            color: windowtext;
            font-size: 10.0pt;
            font-weight: 700;
            font-style: normal;
            text-decoration: none;
            font-family: 宋体;
            text-align: center;
            vertical-align: middle;
            white-space: normal;
            border: .5pt solid windowtext;
            padding-left: 1px;
            padding-right: 1px;
            padding-top: 1px;
        }
        .style7
        {
            width: 189pt;
            color: windowtext;
            font-size: 10.0pt;
            font-weight: 700;
            font-style: normal;
            text-decoration: none;
            font-family: 宋体;
            text-align: center;
            vertical-align: middle;
            white-space: normal;
            border: .5pt solid windowtext;
            padding-left: 1px;
            padding-right: 1px;
            padding-top: 1px;
        }
        .style8
        {
            width: 55pt;
            color: windowtext;
            font-size: 10.0pt;
            font-weight: 700;
            font-style: normal;
            text-decoration: none;
            font-family: 宋体;
            text-align: center;
            vertical-align: middle;
            white-space: normal;
            border: .5pt solid windowtext;
            padding-left: 1px;
            padding-right: 1px;
            padding-top: 1px;
        }
        .style9
        {
            width: 53pt;
            color: windowtext;
            font-size: 10.0pt;
            font-weight: 700;
            font-style: normal;
            text-decoration: none;
            font-family: 宋体;
            text-align: center;
            vertical-align: middle;
            white-space: normal;
            border: .5pt solid windowtext;
            padding-left: 1px;
            padding-right: 1px;
            padding-top: 1px;
        }
        .style10
        {
            width: 54pt;
            color: windowtext;
            font-size: 10.0pt;
            font-weight: 700;
            font-style: normal;
            text-decoration: none;
            font-family: 宋体;
            text-align: center;
            vertical-align: middle;
            white-space: normal;
            border: .5pt solid windowtext;
            padding-left: 1px;
            padding-right: 1px;
            padding-top: 1px;
        }
        .style11
        {
            height: 13.5pt;
            width: 70pt;
            color: windowtext;
            font-size: 10.0pt;
            font-weight: 700;
            font-style: normal;
            text-decoration: none;
            font-family: 宋体;
            text-align: center;
            vertical-align: middle;
            white-space: normal;
            border: .5pt solid windowtext;
            padding-left: 1px;
            padding-right: 1px;
            padding-top: 1px;
        }
        .style12
        {
            width: 62pt;
            color: windowtext;
            font-size: 10.0pt;
            font-weight: 700;
            font-style: normal;
            text-decoration: none;
            font-family: 宋体;
            text-align: center;
            vertical-align: middle;
            white-space: normal;
            border: .5pt solid windowtext;
            padding-left: 1px;
            padding-right: 1px;
            padding-top: 1px;
        }
        .style13
        {
            width: 57pt;
            color: windowtext;
            font-size: 10.0pt;
            font-weight: 700;
            font-style: normal;
            text-decoration: none;
            font-family: 宋体;
            text-align: center;
            vertical-align: middle;
            white-space: normal;
            border: .5pt solid windowtext;
            padding-left: 1px;
            padding-right: 1px;
            padding-top: 1px;
        }
        .style14
        {
            height: 13.5pt;
            color: windowtext;
            font-size: 10.0pt;
            font-weight: 700;
            font-style: normal;
            text-decoration: none;
            font-family: 宋体;
            text-align: center;
            vertical-align: middle;
            white-space: nowrap;
            border: .5pt solid windowtext;
            padding-left: 1px;
            padding-right: 1px;
            padding-top: 1px;
        }
        .style15
        {
            color: windowtext;
            font-size: 10.0pt;
            font-weight: 700;
            font-style: normal;
            text-decoration: none;
            font-family: 宋体;
            text-align: center;
            vertical-align: middle;
            white-space: nowrap;
            border: .5pt solid windowtext;
            padding-left: 1px;
            padding-right: 1px;
            padding-top: 1px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional">
        <ContentTemplate>
            <table width="100%">
                <colgroup>
                    <col width="5%" />
                    <col width="6%" />
                    <col width="70%" />
                </colgroup>
                <tr>
                    <td class="inner_cell_right">
                        年度:
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlYear" runat="server">
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:Button ID="btnSearch" runat="server" OnClick="btnSearch_Click" Text="查询" />
                        <asp:Button ID="btnExport" runat="server" Text="导出" OnClick="btnExport_Click" />
                    </td>
                </tr>
            </table>
            <div class="div-body">
                <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse;
                    width: 797pt" width="1060">
                    <colgroup>
                        <col width="87" />
                        <col width="85" />
                        <col width="77" />
                        <col width="86" />
                        <col width="114" />
                        <col width="93" />
                        <col width="83" />
                        <col width="76" />
                        <col width="73" />
                        <col width="70" />
                        <col span="3" width="72" />
                    </colgroup>
                    <tr height="43">
                        <td class="style1" colspan="13" height="43" width="1060">
                            回收净化气体生产情况统计报表
                        </td>
                    </tr>
                    <tr height="18">
                        <td class="style2" height="36" rowspan="2" width="87">
                            月度
                        </td>
                        <td class="style3" rowspan="2" width="85">
                            回收气体申请量
                        </td>
                        <td class="style4" rowspan="2" width="77">
                            已回收气体量
                        </td>
                        <td class="style5" rowspan="2" width="86">
                            新购气体量
                        </td>
                        <td class="style6" rowspan="2" width="114">
                            处理总量
                        </td>
                        <td class="style7" colspan="3" width="252">
                            新购气或净化气体检测情况
                        </td>
                        <td class="style8" rowspan="2" width="73">
                            需气申请量
                        </td>
                        <td class="style9" rowspan="2" width="70">
                            需气发送量
                        </td>
                        <td class="style10" rowspan="2" width="72">
                            入网检测申请量
                        </td>
                        <td class="style10" rowspan="2" width="72">
                            已入网检测量
                        </td>
                        <td class="style10" rowspan="2" width="72">
                            备注
                        </td>
                    </tr>
                    <tr height="18">
                        <td class="style11" height="18" width="93">
                            未检测
                        </td>
                        <td class="style12" width="83">
                            已检合格
                        </td>
                        <td class="style13" width="76">
                            已检不合格
                        </td>
                    </tr>
                    <tr height="18">
                        <td class="style14" height="18">
                            <%= this.ddlYear.SelectedValue.ToString()%>年1月
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[0][0].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[0][1].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[0][2].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[0][3].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[0][4].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[0][5].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[0][6].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[0][7].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[0][8].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[0][9].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[0][10].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                        </td>
                    </tr>
                    <tr height="18">
                        <td class="style14" height="18">
                            <%= this.ddlYear.SelectedValue.ToString()%>年2月
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[1][0].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[1][1].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[1][2].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[1][3].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[1][4].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[1][5].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[1][6].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[1][7].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[1][8].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[1][9].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[1][10].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                        </td>
                    </tr>
                    <tr height="18">
                        <td class="style14" height="18">
                            <%= this.ddlYear.SelectedValue.ToString()%>年3月
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[2][0].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[2][1].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[2][2].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[2][3].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[2][4].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[2][5].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[2][6].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[2][7].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[2][8].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[2][9].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[2][10].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                        </td>
                    </tr>
                    <tr height="18">
                        <td class="style14" height="18">
                            <%= this.ddlYear.SelectedValue.ToString()%>年4月
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[3][0].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[3][1].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[3][2].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[3][3].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[3][4].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[3][5].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[3][6].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[3][7].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[3][8].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[3][9].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[3][10].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                        </td>
                    </tr>
                    <tr height="18">
                        <td class="style14" height="18">
                            <%= this.ddlYear.SelectedValue.ToString()%>年5月
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[4][0].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[4][1].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[4][2].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[4][3].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[4][4].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[4][5].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[4][6].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[4][7].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[4][8].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[4][9].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[4][10].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                        </td>
                    </tr>
                    <tr height="18">
                        <td class="style14" height="18">
                            <%= this.ddlYear.SelectedValue.ToString()%>年6月
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[5][0].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[5][1].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[5][2].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[5][3].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[5][4].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[5][5].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[5][6].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[5][7].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[5][8].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[5][9].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[5][10].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                        </td>
                    </tr>
                    <tr height="18">
                        <td class="style14" height="18">
                            <%= this.ddlYear.SelectedValue.ToString()%>年7月
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[6][0].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[6][1].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[6][2].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[6][3].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[6][4].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[6][5].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[6][6].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[6][7].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[6][8].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[6][9].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[6][10].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                        </td>
                    </tr>
                    <tr height="18">
                        <td class="style14" height="18">
                            <%= this.ddlYear.SelectedValue.ToString()%>年8月
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[7][0].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[7][1].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[7][2].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[7][3].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[7][4].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[7][5].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[7][6].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[7][7].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[7][8].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[7][9].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[7][10].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                        </td>
                    </tr>
                    <tr height="18">
                        <td class="style14" height="18">
                            <%= this.ddlYear.SelectedValue.ToString()%>年9月
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[8][0].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[8][1].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[8][2].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[8][3].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[8][4].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[8][5].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[8][6].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[8][7].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[8][8].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[8][9].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[8][10].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                        </td>
                    </tr>
                    <tr height="18">
                        <td class="style14" height="18">
                            <%= this.ddlYear.SelectedValue.ToString()%>年10月
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[9][0].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[9][1].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[9][2].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[9][3].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[9][4].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[9][5].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[9][6].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[9][7].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[9][8].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[9][9].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[9][10].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                        </td>
                    </tr>
                    <tr height="18">
                        <td class="style14" height="18">
                            <%= this.ddlYear.SelectedValue.ToString()%>年11月
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[10][0].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[10][1].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[10][2].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[10][3].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[10][4].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[10][5].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[10][6].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[10][7].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[10][8].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[10][9].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[10][10].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                        </td>
                    </tr>
                    <tr height="18">
                        <td class="style14" height="18">
                            <%= this.ddlYear.SelectedValue.ToString()%>年12月
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[11][0].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[11][1].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[11][2].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[11][3].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[11][4].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[11][5].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[11][6].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[11][7].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[11][8].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[11][9].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[11][10].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                        </td>
                    </tr>
                    <tr height="18">
                        <td class="style14" height="18">
                            合计
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[12][0].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[12][1].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[12][2].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[12][3].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[12][4].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[12][5].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[12][6].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[12][7].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[12][8].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[12][9].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                            <%= ResultTB.Rows.Count>0? ResultTB.Rows[12][10].ToString():string.Empty%>
                        </td>
                        <td class="style15">
                        </td>
                    </tr>
                </table>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnSearch" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>
    </form>
</body>
</html>

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using RM.Web.App_Code;
using System.Text;
using System.Data;
using RM.Common.DotNetUI;
using Microsoft.Office.Interop.Excel;
using RM.Common.DotNetFile;
using EGMNGS.Common;

namespace RM.Web.Report
{
    public partial class RPTGasRecStatistics : PageBase
    {
        EGMNGS.BLL.ApplRecyGasReg bll = new EGMNGS.BLL.ApplRecyGasReg();

        private System.Data.DataTable ResultTB
        {
            get
            {
                if (ViewState["ResultTB"] == null)
                {
                    return new System.Data.DataTable();
                }
                return ViewState["ResultTB"] as System.Data.DataTable;
            }
            set { ViewState["ResultTB"] = value; }
        }

        private string sqlStr
        {
            get
            {
                if (ViewState["sqlStr"] == null)
                {
                    ViewState["sqlStr"] = "1=0";
                }
                return ViewState["sqlStr"] as string;
            }
            set { ViewState["sqlStr"] = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                DropDownListBinder();
            }

            ScriptManager.GetCurrent(this.Page).RegisterPostBackControl(this.btnExport);
        }

        private void DataBindGrid()
        {
            StringBuilder sb = new StringBuilder();

            if (this.ddlMonth.SelectedValue.Length > 0)
            {
                sb.AppendFormat("and Month='{0}' ", this.ddlMonth.SelectedValue.Trim());
            }
            if (this.ddlYear.SelectedValue.Length > 0)
            {
                sb.AppendFormat("and Year='{0}' ", this.ddlYear.SelectedValue.Trim());
            }
            if (ddlPowerSupplyName.SelectedIndex > 0)
            {
                sb.AppendFormat(" and PowerSupplyName='{0}'", ddlPowerSupplyName.SelectedItem.Text);
                if (ddlConvertStationName.SelectedIndex > 0)
                {
                    sb.AppendFormat(" and ConvertStationName='{0}'", ddlConvertStationName.SelectedItem.Text);
                }

            }

            sqlStr = sb.ToString().TrimStart(new char[3] { 'a', 'n', 'd' });
            if (sqlStr.Length == 0)
            {
                sqlStr = "1=0";
            }
            DataSet ds = bll.GetList(sqlStr);
            ControlBindHelper.BindRepeaterList(ds.Tables[0], rp_Item);
            ResultTB = ds.Tables[0];


        }
        private void DropDownListBinder()
        {
            EGMNGS.BLL.SYS_CODE sysCodeBll = new EGMNGS.BLL.SYS_CODE();
            DataSet dsYear = sysCodeBll.GetList("CODE_TYPE='Year' ORDER BY DISPLAY_ORDER");

            this.ddlYear.DataSource = dsYear.Tables[0];
            this.ddlYear.DataTextField = "CODE_CHI_DESC";
            this.ddlYear.DataValueField = "CODE";
            this.ddlYear.DataBind();

            this.ddlYear.SelectedValue = DateTime.Now.Year.ToString();

            DataSet dsMonth = sysCodeBll.GetList("CODE_TYPE='Month' ORDER BY DISPLAY_ORDER");

            this.ddlMonth.DataSource = dsMonth.Tables[0];
            this.ddlMonth.DataTextField = "CODE_CHI_DESC";
            this.ddlMonth.DataValueField = "CODE";
            this.ddlMonth.DataBind();

            this.ddlMonth.Items.Insert(0, new ListItem("全年", ""));
            this.ddlMonth.SelectedValue = "";


            System.Data.DataTable dtPowerStation = ComServies.GetAllPowerStation();
            this.ddlPowerSupplyName.DataSource = dtPowerStation;
            this.ddlPowerSupplyName.DataTextField = "Organization_Name";
            this.ddlPowerSupplyName.DataValueField = "Organization_ID";
            this.ddlPowerSupplyName.DataBind();
            this.ddlPowerSupplyName.Items.Insert(0, string.Empty);
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            DataBindGrid();
        }

        /// <summary>
        /// 导出
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnExport_Click(object sender, EventArgs e)
        {

            MouldExportExcel mee = new MouldExportExcel();
            try
            {
                string physicalPath = Request.PhysicalApplicationPath + "Xls\\";
                Workbook sh = mee.OpenExcel(physicalPath + "气体回收统计.xls");
                Worksheet ws = sh.Worksheets[1] as Worksheet;

                #region 设置值


                string tempTitle = string.Format("{0}年{1}供电局回收气体统计表", this.ddlYear.SelectedValue, (this.ddlMonth.SelectedValue.Length > 0 ? this.ddlMonth.SelectedValue + "月" : this.ddlMonth.SelectedValue));
                mee.SetCellValue(ws, 1, 1, tempTitle);

                for (int i = 0; i < ResultTB.Rows.Count; i++)
                {
                    DataRow item = ResultTB.Rows[i];
                    mee.SetCellValue(ws, 3 + i, 1, i + 1);
                    mee.SetCellValue(ws, 3 + i, 2, item["RecyCode"].ToString());
                    mee.SetCellValue(ws, 3 + i, 3, item["PowerSupplyName"].ToString());
                    mee.SetCellValue(ws, 3 + i, 4, item["ConvertStationName"].ToString());
                    mee.SetCellValue(ws, 3 + i, 5, item["AmountRecovery"].ToString());
                    mee.SetCellValue(ws, 3 + i, 6, item["CylinderCount"].ToString());
                    mee.SetCellValue(ws, 3 + i, 7, item["RecoveryDesc"].ToString());
                    mee.SetCellValue(ws, 3 + i, 8, item["ContactsName"].ToString());
                    mee.SetCellValue(ws, 3 + i, 9, item["PhoneNum"].ToString());

                }

                mee.SetCellValue(ws, 3 + ResultTB.Rows.Count, 5, string.Format("=SUM(E3:E{0})", 3 + ResultTB.Rows.Count - 1));
                //mee.SetCellValue(ws, 3 + ResultTB.Rows.Count, 6, string.Format("=SUM(F3:F{0})", 3 + ResultTB.Rows.Count - 1));

                mee.SetCellValue(ws, 3 + ResultTB.Rows.Count, 1, "小计");

                #endregion





                string fileName = "气体回收统计temp.xls";
                string pathFile = physicalPath + fileName;
                // FileHelper.DeleteFile(pathFile);
                mee.SaveExcel(pathFile);
                //  ShowMsgHelper.Alert("导出成功!");

                FileDownHelper.DownLoadold(@"~/Xls/" + fileName);

            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                mee.Dispose();

            }
        }


        protected void ddlPowerSupplyName_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadPowerSupplyName();

        }
        private void LoadPowerSupplyName()
        {
            this.ddlConvertStationName.Items.Clear();
            EGMNGS.BLL.InfoPowerSupplyConvertStation bll = new EGMNGS.BLL.InfoPowerSupplyConvertStation();
            DataSet dsPowerSupplyCode = bll.GetList(string.Format("PowerSupplyCode='{0}'", this.ddlPowerSupplyName.SelectedValue));
            this.ddlConvertStationName.DataSource = dsPowerSupplyCode.Tables[0];
            this.ddlConvertStationName.DataTextField = "ConvartStationName";
            this.ddlConvertStationName.DataValueField = "CovnertStationCode";
            this.ddlConvertStationName.DataBind();
            this.ddlConvertStationName.Items.Insert(0, string.Empty);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EGMNGS.Common;
using System.Data;
using Microsoft.Office.Interop.Excel;
using RM.Common.DotNetFile;

namespace RM.Web.Report
{
    public partial class RPTGasSFCTable : System.Web.UI.Page
    {
        public System.Data.DataTable dtRPTGasSFCTable
        {
            get
            {
                if (ViewState["RPTGasSFCTable"] == null)
                {
                    return ComServies.GetRPTGasSFCTable(0, 0).Tables[0];
                }
                return ViewState["RPTGasSFCTable"] as System.Data.DataTable;
            }
            set { ViewState["RPTGasSFCTable"] = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                DropDownListBinder();
            }

            ScriptManager.GetCurrent(this.Page).RegisterPostBackControl(this.btnExport);
        }
        private void DropDownListBinder()
        {
            EGMNGS.BLL.SYS_CODE sysCodeBll = new EGMNGS.BLL.SYS_CODE();
            DataSet dsYear = sysCodeBll.GetList("CODE_TYPE='Year' ORDER BY DISPLAY_ORDER");

            this.ddlYear.DataSource = dsYear.Tables[0];
            this.ddlYear.DataTextField = "CODE_CHI_DESC";
            this.ddlYear.DataValueField = "CODE";
            this.ddlYear.DataBind();
            this.ddlYear.SelectedValue = DateTime.Now.Year.ToString();


            DataSet dsMonth = sysCodeBll.GetList("CODE_TYPE='Month' ORDER BY DISPLAY_ORDER");

            this.ddlMonth.DataSource = dsMonth.Tables[0];
            this.ddlMonth.DataTextField = "CODE_CHI_DESC";
            this.ddlMonth.DataValueField = "CODE";
            this.ddlMonth.DataBind();
            this.ddlYear.SelectedValue = DateTime.Now.Month.ToString();
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            DataBindGrid();
        }
        private void DataBindGrid()
        {
            dtRPTGasSFCTable = ComServies.GetRPTGasSFCTable(int.Parse(this.ddlYear.SelectedValue), int.Parse(this.ddlMonth.SelectedValue)).Tables[0];
        }

        /// <summary>
        /// 导出
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnExport_Click(object sender, EventArgs e)
        {

            MouldExportExcel mee = new MouldExportExcel();
            try
            {
                string physicalPath = Request.PhysicalApplicationPath + "Xls\\";
                Workbook sh = mee.OpenExcel(physicalPath + "气体收发存表.xls");
                Worksheet ws = sh.Worksheets[1] as Worksheet;

                #region 设置值

                mee.SetCellValue(ws, 1, 1, this.ddlYear.SelectedValue + "年" + this.ddlMonth.SelectedValue + "月六氟化硫气体收发存表");



                for (int i = 0; i < dtRPTGasSFCTable.Rows.Count; i++)
                {
                    DataRow item = dtRPTGasSFCTable.Rows[i];
                    int rowIndex = 5 + i;
                    mee.SetCellValue(ws, rowIndex, 2, item["RecyCount"].ToString());
                    mee.SetCellValue(ws, rowIndex, 3, item["RecyTotal"].ToString());
                    mee.SetCellValue(ws, rowIndex, 4, item["RecyOutCount"].ToString());
                    mee.SetCellValue(ws, rowIndex, 5, item["RecyOut"].ToString());
                    mee.SetCellValue(ws, rowIndex, 6, item["RecyInCount"].ToString());
                    mee.SetCellValue(ws, rowIndex, 7, item["RecyIn"].ToString());
                    mee.SetCellValue(ws, rowIndex, 8, item["JHCount"].ToString());
                    mee.SetCellValue(ws, rowIndex, 9, item["JHTotal"].ToString());
                    mee.SetCellValue(ws, rowIndex, 10, item["JHOutCount"].ToString());
                    mee.SetCellValue(ws, rowIndex, 11, item["JHOut"].ToString());
                    mee.SetCellValue(ws, rowIndex, 12, item["JHInCount"].ToString());
                    mee.SetCellValue(ws, rowIndex, 13, item["JHIn"].ToString());
                    mee.SetCellValue(ws, rowIndex, 14, item["XGCount"].ToString());
                    mee.SetCellValue(ws, rowIndex, 15, item["XGTotal"].ToString());
                    mee.SetCellValue(ws, rowIndex, 16, item["XGOutCount"].ToString());
                    mee.SetCellValue(ws, rowIndex, 17, item["XGOut"].ToString());
                    mee.SetCellValue(ws, rowIndex, 18, item["XGInCount"].ToString());
                    mee.SetCellValue(ws, rowIndex, 19, item["XGIn"].ToString());
                    mee.SetCellValue(ws, rowIndex, 20, item["HGCount"].ToString());
                    mee.SetCellValue(ws, rowIndex, 21, item["HGTotal"].ToString());
                    mee.SetCellValue(ws, rowIndex, 22, item["HGOutCount"].ToString());
                    mee.SetCellValue(ws, rowIndex, 23, item["HGOut"].ToString());
                    mee.SetCellValue(ws, rowIndex, 24, item["HGInCount"].ToString());
                    mee.SetCellValue(ws, rowIndex, 25, item["HGIn"].ToString());
                }


                #endregion

                string fileName = "气体收发存表temp.xls";
                string pathFile = physicalPath + fileName;

                mee.SaveExcel(pathFile);

                FileDownHelper.DownLoadold(@"~/Xls/" + fileName);

            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                mee.Dispose();

            }
        }

    }
}
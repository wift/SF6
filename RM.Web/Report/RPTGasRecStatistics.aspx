﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RPTGasRecStatistics.aspx.cs"
    Inherits="RM.Web.Report.RPTGasRecStatistics" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>气体回收统计</title>
    <link href="~/Themes/Styles/Site.css" rel="stylesheet" type="text/css" />
    <script src="/Themes/Scripts/Validator/JValidator.js" type="text/javascript"></script>
    <script src="/Themes/Scripts/jquery-1.8.2.min.js" type="text/javascript"></script>
    <script src="/Themes/Scripts/jquery.pullbox.js" type="text/javascript"></script>
    <script src="/Themes/Scripts/FunctionJS.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {
            InitControl();
        })
        function InitControl() {
            $(".div-body").PullBox({ dv: $(".div-body"), obj: $("#table1").find("tr") });
            FixedTableHeader("#table1", $(window).height() - 91);
            divresize(60);
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional">
        <ContentTemplate>
            <table width="100%">
                <colgroup>
                    <col width="5%" />
                    <col width="6%" />
                    <col width="5%" />
                    <col width="6%" />
                    <col width="10%" />
                    <col width="10%" />
                    <col width="10%" />
                    <col width="10%" />
                    <col width="30%" />
                </colgroup>
                <tr>
                    <td class="inner_cell_right">
                        年度:
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlYear" runat="server">
                        </asp:DropDownList>
                    </td>
                    <td class="inner_cell_right">
                        月度:
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlMonth" runat="server">
                        </asp:DropDownList>
                    </td>
                    <td class="inner_cell_right">
                        供电局名称：
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlPowerSupplyName" runat="server" Width="100%" AutoPostBack="true"
                            OnSelectedIndexChanged="ddlPowerSupplyName_SelectedIndexChanged" CssClass="select">
                        </asp:DropDownList>
                    </td>
                    <td class="inner_cell_right">
                        变电站名称：
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlConvertStationName" runat="server" Width="100% " CssClass="select"
                            checkexpession="NotNull" datacol="yes" err="此项">
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:Button ID="btnSearch" runat="server" OnClick="btnSearch_Click" Text="查询" />
                        <asp:Button ID="btnExport" runat="server" Text="导出" OnClick="btnExport_Click" />
                    </td>
                </tr>
            </table>
            <div class="div-body" style="overflow-y: hidden;">
                <table id="table1" class="grid" singleselect="true">
                    <colgroup>
                        <col width="10%" />
                        <col width="10%" />
                        <col width="10%" />
                        <col width="10%" />
                        <%-- <col width="10%" />--%>
                        <col width="30%" />
                        <col width="10%" />
                        <col width="20%" />
                    </colgroup>
                    <thead>
                        <tr>
                            <td style="text-align: center;">
                                回收编号
                            </td>
                            <td style="text-align: center;">
                                供电局名称
                            </td>
                            <td style="text-align: center;">
                                变电站名称
                            </td>
                            <td style="text-align: center;">
                                回收量（kg）
                            </td>
                            <%--<td style="text-align: center;">
                                钢瓶数
                            </td>--%>
                            <td style="text-align: center;">
                                回收说明
                            </td>
                            <td style="text-align: center;">
                                联系人
                            </td>
                            <td style="text-align: center;">
                                手机
                            </td>
                        </tr>
                    </thead>
                    <tbody>
                        <asp:Repeater ID="rp_Item" runat="server">
                            <ItemTemplate>
                                <tr>
                                    <td style="width: 100px; text-align: center;">
                                        <%#Eval("RecyCode")%>
                                    </td>
                                    <td style="text-align: center;">
                                        <%#Eval("PowerSupplyName")%>
                                    </td>
                                    <td style="text-align: center;">
                                        <%#Eval("ConvertStationName")%>
                                    </td>
                                    <td style="text-align: center;">
                                        <%#Eval("AmountRecovery")%>
                                    </td>
                                    <%--    <td style="text-align: center;">
                                        <%#Eval("CylinderCount")%>
                                    </td>--%>
                                    <td style="text-align: center;">
                                        <%#Eval("RecoveryDesc")%>
                                    </td>
                                    <td style="text-align: center;">
                                        <%#Eval("ContactsName")%>
                                    </td>
                                    <td style="text-align: center;">
                                        <%#Eval("PhoneNum")%>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </tbody>
                </table>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnSearch" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>
    </form>
</body>
</html>

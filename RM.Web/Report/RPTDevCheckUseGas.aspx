﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RPTDevCheckUseGas.aspx.cs"
    Inherits="RM.Web.Report.RPTDevCheckUseGas" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>设备检修用气</title>
    <link href="~/Themes/Styles/Site.css" rel="stylesheet" type="text/css" />
    <script src="/Themes/Scripts/Validator/JValidator.js" type="text/javascript"></script>
    <script src="/Themes/Scripts/jquery-1.8.2.min.js" type="text/javascript"></script>
    <script src="/Themes/Scripts/jquery.pullbox.js" type="text/javascript"></script>
    <script src="/Themes/Scripts/FunctionJS.js" type="text/javascript"></script>
    <script src="/Themes/Scripts/DatePicker/WdatePicker.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {
            InitControl();
        })
        function InitControl() {
            $(".div-body").PullBox({ dv: $(".div-body"), obj: $("#table1").find("tr") });
            FixedTableHeader("#table1", $(window).height() - 91);
            divresize(60);
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional">
        <ContentTemplate>
            <table width="100%">
                <colgroup>
                    <col width="10%" />
                    <col width="10%" />
                    <col width="10%" />
                    <col width="10%" />
                    <col width="10%" />
                    <col width="10%" />
                    <col width="10%" />
                    <col width="10%" />
                    <col width="20%" />
                </colgroup>
                <tr>
                    <td class="inner_cell_right">
                        供电局名称：
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlPowerSupplyName" runat="server" Width="100%" AutoPostBack="true"
                            OnSelectedIndexChanged="ddlPowerSupplyName_SelectedIndexChanged" CssClass="select">
                        </asp:DropDownList>
                    </td>
                    <td class="inner_cell_right">
                        变电站名称：
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlConvertStationName" runat="server" Width="100% " CssClass="select"
                            checkexpession="NotNull" datacol="yes" err="此项">
                        </asp:DropDownList>
                    </td>
                    <td class="inner_cell_right">
                        补换气日期起:
                    </td>
                    <td>
                        <asp:TextBox ID="txtBeginDate" runat="server" onfocus="WdatePicker()"></asp:TextBox>
                    </td>
                    <td class="inner_cell_right">
                        补换气日期止:
                    </td>
                    <td>
                        <asp:TextBox ID="txtEndDate" runat="server" onfocus="WdatePicker()"></asp:TextBox>
                    </td>
                    <td>
                        <asp:Button ID="btnSearch" runat="server" OnClick="btnSearch_Click" Text="查询" />
                        <asp:Button ID="btnExport" runat="server" Text="导出" OnClick="btnExport_Click" />
                    </td>
                </tr>
            </table>
            <div class="div-body" style="overflow-y: hidden;">
                <table id="table1" class="grid" singleselect="true">
                    <colgroup>
                        <col width="6%" />
                        <col width="10%" />
                        <col width="6%" />
                        <col width="6%" />
                        <col width="10%" />
                        <col width="10%" />
                        <col width="10%" />
                        <col width="5%" />
                        <col width="10%" />
                        <col width="10%" />
                        <col width="10%" />
                    </colgroup>
                    <thead>
                        <tr>
                            <td style="text-align: center;">
                                供电局名称
                            </td>
                            <td style="text-align: center;">
                                变电站名称
                            </td>
                            <td style="text-align: center;">
                                设备编码
                            </td>
                            <td style="text-align: center;">
                                设备名称
                            </td>
                            <td style="text-align: center;">
                                额定气量(kg)
                            </td>
                            <td style="text-align: center;">
                                额定气压（Mpa）
                            </td>
                            <td style="text-align: center;">
                                用/补气日期
                            </td>
                            <td style="text-align: center;">
                                操作人员
                            </td>
                            <td style="text-align: center;">
                                用/补气量（kg）
                            </td>
                            <td style="text-align: center;">
                                用/补气前压力(Mpa)
                            </td>
                            <td style="text-align: center;">
                                用/补气后压力(Mpa)
                            </td>
                        </tr>
                    </thead>
                    <tbody>
                        <asp:Repeater ID="rp_Item" runat="server">
                            <ItemTemplate>
                                <tr>
                                    <td style="text-align: center;">
                                        <%#Eval("PowerSupplyName")%>
                                    </td>
                                    <td style="text-align: center;">
                                        <%#Eval("ConvertStationName")%>
                                    </td>
                                    <td style="text-align: center;">
                                        <%#Eval("DevCode")%>
                                    </td>
                                    <td style="text-align: center;">
                                        <%#Eval("DevName")%>
                                    </td>
                                    <td style="text-align: center;">
                                        <%#Eval("RatedQL")%>
                                    </td>
                                    <td style="text-align: center;">
                                        <%#Eval("RatedQY")%>
                                    </td>
                                    <td style="text-align: center;">
                                        <%#Eval("FillGasdate", "{0:yyyy-MM-dd}")%>
                                    </td>
                                    <td style="text-align: center;">
                                        <%#Eval("Operataff")%>
                                    </td>
                                    <td style="text-align: center;">
                                        <%#Eval("AmountFillGas")%>
                                    </td>
                                    <td style="text-align: center;">
                                        <%#Eval("AmountPressFillGassbefore")%>
                                    </td>
                                    <td style="text-align: center;">
                                        <%#Eval("AmountPressFillGassafter")%>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </tbody>
                </table>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnSearch" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>
    </form>
</body>
</html>

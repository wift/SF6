﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;
using RM.Web.App_Code;
using RM.Common.DotNetFile;
using Microsoft.Office.Interop.Excel;
using Aspose.Cells;

namespace RM.Web.Report
{
    public partial class RPTGasCheckMonthR : System.Web.UI.Page
    {
        public System.Data.DataTable ResultTB
        {
            get
            {
                if (ViewState["ResultTB"] == null)
                {
                    return new System.Data.DataTable();
                }
                return ViewState["ResultTB"] as System.Data.DataTable;
            }
            set { ViewState["ResultTB"] = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                DropDownListBinder();
            }
            ScriptManager.GetCurrent(this.Page).RegisterPostBackControl(this.btnExport);
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            StringBuilder sb = new StringBuilder();

            string sql = @"  select '{0}年{1}月' as 'Month', (select ISNULL(SUM(b.AmountGas),0.00) from dbo.ApplRegGasQtyInspection a,BookGasFill b
where a.GasSourse='1' and b.GasCode=a.GasCdoe
and a.CheckDate is null and YEAR(a.CheckDate)={0} and MONTH(a.CheckDate)={1}) '未检测x',
(select ISNULL(SUM(b.AmountGas),0.00) from dbo.ApplRegGasQtyInspection a,BookGasFill b
where a.GasSourse='1' and b.GasCode=a.GasCdoe
and a.CheckDate is not null and YEAR(a.CheckDate)={0} and MONTH(a.CheckDate)={1}
and a.IsPass='1') '已检合格x',
(select ISNULL(SUM(b.AmountGas),0.00) from dbo.ApplRegGasQtyInspection a,BookGasFill b
where a.GasSourse='1' and b.GasCode=a.GasCdoe
and a.CheckDate is not null and YEAR(a.CheckDate)={0} and MONTH(a.CheckDate)={1}
and a.IsPass='0') '已检不合格x',

(select ISNULL(SUM(b.AmountGas),0.00) from dbo.ApplRegGasQtyInspection a,BookGasFill b
where a.GasSourse='2' and b.GasCode=a.GasCdoe
and a.CheckDate is null and YEAR(a.CheckDate)={0} and MONTH(a.CheckDate)={1}) '未检测r',
(select ISNULL(SUM(b.AmountGas),0.00) from dbo.ApplRegGasQtyInspection a,BookGasFill b
where a.GasSourse='2' and b.GasCode=a.GasCdoe
and a.CheckDate is not null and YEAR(a.CheckDate)={0} and MONTH(a.CheckDate)={1}
and a.IsPass='1') '已检合格r',
(select ISNULL(SUM(b.AmountGas),0.00) from dbo.ApplRegGasQtyInspection a,BookGasFill b
where a.GasSourse='2' and b.GasCode=a.GasCdoe
and a.CheckDate is not null and YEAR(a.CheckDate)={0} and MONTH(a.CheckDate)={1}
and a.IsPass='0') '已检不合格r',


(select ISNULL(SUM(b.AmountGas),0.00) from dbo.ApplRegGasQtyInspection a,BookGasFill b
where a.GasSourse='3' and b.GasCode=a.GasCdoe
and a.CheckDate is null and YEAR(a.CheckDate)={0} and MONTH(a.CheckDate)={1}) '未检测h',
(select ISNULL(SUM(b.AmountGas),0.00) from dbo.ApplRegGasQtyInspection a,BookGasFill b
where a.GasSourse='3' and b.GasCode=a.GasCdoe
and a.CheckDate is not null and YEAR(a.CheckDate)={0} and MONTH(a.CheckDate)={1}
and a.IsPass='1') '已检合格h',
(select ISNULL(SUM(b.AmountGas),0.00) from dbo.ApplRegGasQtyInspection a,BookGasFill b
where a.GasSourse ='3' and b.GasCode=a.GasCdoe
and a.CheckDate is not null and YEAR(a.CheckDate)={0} and MONTH(a.CheckDate)={1}
and a.IsPass='0') '已检不合格h'
";



            for (int i = 1; i <= 12; i++)
            {
                sb.AppendLine();
                sb.AppendFormat(sql, this.ddlYear.SelectedValue, i);
                sb.AppendLine();
                
                if (i<12)
                {
                    sb.Append("union all");
                }
                
            }

            sb.AppendLine();
       
            DataSet ds = EGMNGS.Common.ComServies.GetRPTJHProductMonthR(sb.ToString());
            ResultTB = ds.Tables[0];
            ResultTB.TableName = "ResultTB";
            DataRow dr=ResultTB.NewRow();
            dr["Month"]="合计";
            foreach (DataRow item in ResultTB.Rows)
            {
                dr["未检测x"] = dr["未检测x"].AsTargetType<decimal>(0) + item["未检测x"].AsTargetType<decimal>(0);
                dr["已检合格x"] = dr["已检合格x"].AsTargetType<decimal>(0) + item["已检合格x"].AsTargetType<decimal>(0);
                dr["已检不合格x"] = dr["已检不合格x"].AsTargetType<decimal>(0) + item["已检不合格x"].AsTargetType<decimal>(0);
                dr["未检测r"] = dr["未检测r"].AsTargetType<decimal>(0) + item["未检测r"].AsTargetType<decimal>(0);
                dr["已检合格r"] = dr["已检合格r"].AsTargetType<decimal>(0) + item["已检合格r"].AsTargetType<decimal>(0);
                dr["已检不合格r"] = dr["已检不合格r"].AsTargetType<decimal>(0) + item["已检不合格r"].AsTargetType<decimal>(0);
                dr["未检测h"] = dr["未检测h"].AsTargetType<decimal>(0) + item["未检测h"].AsTargetType<decimal>(0);
                dr["已检合格h"] = dr["已检合格h"].AsTargetType<decimal>(0) + item["已检合格h"].AsTargetType<decimal>(0);
                dr["已检不合格h"] = dr["已检不合格h"].AsTargetType<decimal>(0) + item["已检不合格h"].AsTargetType<decimal>(0);
            }
            ResultTB.Rows.Add(dr);

            rp_Item.DataSource = ResultTB;
            rp_Item.DataBind();

        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            WorkbookDesigner wd = new WorkbookDesigner();
            wd.Open(HttpContext.Current.Server.MapPath("~/TemplateFiles/气体检测年报-导出模版.xlsx"));
            
         
            wd.SetDataSource(ResultTB);

            wd.Process();
            wd.Save(string.Format(string.Format("气体检测年报{0}.xls",DateTime.Now.ToString())), SaveType.OpenInExcel, FileFormatType.Excel2003, this.Response);
            this.Response.Flush();
            this.Response.Close();
            this.Response.End();
        }


        private void DropDownListBinder()
        {


            EGMNGS.BLL.SYS_CODE sysCodeBll = new EGMNGS.BLL.SYS_CODE();
            DataSet dsYear = sysCodeBll.GetList("CODE_TYPE='Year' ORDER BY DISPLAY_ORDER");

            this.ddlYear.DataSource = dsYear.Tables[0];
            this.ddlYear.DataTextField = "CODE_CHI_DESC";
            this.ddlYear.DataValueField = "CODE";
            this.ddlYear.DataBind();

            this.ddlYear.SelectedValue = DateTime.Now.Year.ToString();
        }
    }
}
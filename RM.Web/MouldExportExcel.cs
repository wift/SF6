﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using Excel = Microsoft.Office.Interop.Excel;
using System.Reflection;
using Microsoft.Office.Interop.Excel;
namespace RM.Web
{
    public class MouldExportExcel
    {

        protected Excel.Application app;
        protected Excel.Workbooks workbooks;
        protected Excel.Workbook workbook;
        protected Excel.Worksheet worksheet;

        string ftpPath = string.Empty, ftpUserID = string.Empty, ftpPassword = string.Empty;
        FtpWebRequest req;
        List<String> fileList = new List<String>();

        public MouldExportExcel()
        {
            app = new Excel.Application();
            if (app == null)
            {
                // MessageBox.Show("无法生成Excel表，可能是由于您的计算机上未安装Excel软件!");
                return;
            }
        }


        /// <summary>
        /// 加载Excel模板
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public virtual Excel.Workbook OpenExcel(string path)
        {
            workbook = app.Workbooks.Open(path, Missing.Value, Missing.Value, Missing.Value, Missing.Value,
                                            Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value,
                                            Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value);
            if (workbook != null) return workbook; else return null;
        }

        /// <summary>
        /// 加载Excel模板中某分页
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public virtual Excel.Worksheet OpenExcel(string path, int Index)
        {
            workbook = app.Workbooks.Open(path, Missing.Value, Missing.Value, Missing.Value, Missing.Value,
                                            Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value,
                                            Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value);
            worksheet = (Excel.Worksheet)workbook.Worksheets[Index];
            if (worksheet != null) return worksheet; else return null;
        }

        /// <summary>
        /// 为当前加载sheet的单元格赋值
        /// </summary>
        /// <param name="wrkbk"></param>
        /// <param name="Row"></param>
        /// <param name="Column"></param>
        /// <param name="Value"></param>
        public virtual void SetCellValue(Excel.Worksheet worksheet, int Row, int Column, string Value)
        {
            if (worksheet != null)
            {
                worksheet.Cells[Row, Column] = Value;

            }
        }
        public virtual void SetHorizontalAlignment(Excel.Worksheet worksheet, int Row, int Column)
        {
            ((Microsoft.Office.Interop.Excel.Range)worksheet.Cells[Row, Column]).HorizontalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;

        }
        public virtual void SetMerge(Excel.Worksheet worksheet, int Row, int Column, int toRow, int toColumn)
        {
            worksheet.get_Range(worksheet.Cells[Row, Column], worksheet.Cells[toRow, toColumn]).MergeCells = true;
        }

        public virtual void SetBorderLine(Excel.Worksheet worksheet, int Row, int Column, int toRow, int toColumn)
        {
            Excel.Range excelRange = worksheet.get_Range(worksheet.Cells[Row, Column], worksheet.Cells[toRow, toColumn]);
            //单元格边框线类型(线型,虚线型)
            excelRange.Borders.LineStyle = 1;
            excelRange.Borders.get_Item(XlBordersIndex.xlEdgeTop).LineStyle = Excel.XlLineStyle.xlContinuous;
        }
        public virtual void SetCellValue(Excel.Worksheet worksheet, int Row, int Column, int Value)
        {
            if (worksheet != null)
            {
                worksheet.Cells[Row, Column] = Value;
            }
        }
        /// <summary>
        /// 设置sheet名称
        /// </summary>
        /// <param name="strName"></param>
        public virtual void SetSheetName(Excel.Worksheet worksheet, string strName)
        {
            if (worksheet != null)
            {
                worksheet.Name = strName;
            }
        }

        /// <summary>
        /// 另存为
        /// </summary>
        /// <param name="path"></param>
        public virtual bool SaveExcel(string path)
        {
            try
            {
                workbook.SaveCopyAs(path);
            }
            catch (System.Exception ex)
            {
                return false;
            }
            return true;
        }

        #region IDisposable 成员

        public void Dispose()//释放资源
        {
            if (worksheet != null)
            {
                ReleaseExcelObject(worksheet);
            }
            if (workbook != null)
            {
                workbook.Close(false, System.Reflection.Missing.Value, System.Reflection.Missing.Value);
                ReleaseExcelObject(workbook);
            }
            if (workbooks != null)
            {
                workbooks.Close();
                ReleaseExcelObject(workbooks);
            }
            if (app != null)
            {
                app.Quit();
                ReleaseExcelObject(app);
            }
        }

        private void ReleaseExcelObject(object o)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(o);
            }
            catch (System.Exception e)
            {
                throw e;
            }
            finally
            {
                o = null;
                GC.Collect();
            }
        }

        #endregion
    }
}

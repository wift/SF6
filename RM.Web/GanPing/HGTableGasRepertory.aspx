﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="HGTableGasRepertory.aspx.cs" Inherits="RM.Web.GanPing.HGTableGasRepertory" %>

<%@ Register Src="~/UserControl/PageControl.ascx" TagName="PageControl" TagPrefix="uc1" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>合格气体库存</title>
    <link href="/Themes/Styles/Site.css" rel="stylesheet" type="text/css" />
    <script src="/Themes/Scripts/Validator/JValidator.js" type="text/javascript"></script>
    <script src="/Themes/Scripts/jquery-1.8.2.min.js" type="text/javascript"></script>
    <script src="/Themes/Scripts/jquery.pullbox.js" type="text/javascript"></script>
    <script src="/Themes/Scripts/FunctionJS.js" type="text/javascript"></script>
    <script type="text/javascript">


        $(function () {
            function InitControl() {
                //  $(".div-body").PullBox({ dv: $(".div-body"), obj: $("#table1").find("tr") });
                // divresize(115);

                FixedTableHeader("#table1", 100);
                // checkboxdetail();

            }
        });

    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div>

            <div class="div-body">
                <table id="table1" class="grid" singleselect="true">
                    <colgroup>
                        <col width="5%" />
                        <col width="7%" />
                        <col width="7%" />
                        <col width="7%" />
                        <col width="7%" />
                        <col width="7%" />
                        <col width="7%" />
                        <col width="7%" />
                        <col width="7%" />
                        <col width="7%" />
                        <col width="7%" />
                        <col width="7%" />
                    </colgroup>
                    <thead>
                        <tr>
                            <td style="text-align: center;">序号
                            </td>
                            <td style="text-align: center;">钢瓶编码
                            </td>
                            <td style="text-align: center;">气体编码
                            </td>
                            <td style="text-align: center;">报告编码
                            </td>
                            <td style="text-align: center;">气量(kg)
                            </td>
                            <td style="text-align: center;">已用气量
                            </td>
                            <td style="text-align: center;">剩余气量
                            </td>
                            <td style="text-align: center;">出库编号
                            </td>
                            <td style="text-align: center;">业务编号
                            </td>
                            <td style="text-align: center;">供电局名称
                            </td>
                            <td style="text-align: center;">变电站名称
                            </td>
                            <td style="text-align: center;">出库日期
                            </td>


                        </tr>
                    </thead>
                    <tbody>
                        <asp:Repeater ID="rp_Item" runat="server">
                            <ItemTemplate>
                                <tr>
                                    <td style="text-align: center;">
                                        <%# Container.ItemIndex + 1%>
                                    </td>
                                    <td style="text-align: center;">
                                        <%#Eval("CylinderCode")%>
                                    </td>
                                    <td style="text-align: center;">
                                        <%#Eval("GasCode")%>
                                    </td>
                                    <td style="text-align: center;">
                                        <%#Eval("ReportCode")%>
                                    </td>
                                    <td style="text-align: center;">
                                        <%#Eval("AmountGas")%>
                                    </td>
                                    <td style="text-align: center;">
                                        <%#Eval("AmountfillGas")%>
                                    </td>
                                    <td style="text-align: center;">
                                        <%#Eval("resultsAmount")%>
                                    </td>
                                    <td style="text-align: center;">
                                        <%#Eval("Code")%>
                                    </td>
                                    <td style="text-align: center;">
                                        <%#Eval("BusinessCode")%>
                                    </td>
                                    <td style="text-align: center;">
                                        <%#Eval("PowerSupplyName")%>
                                    </td>
                                    <td style="text-align: center;">
                                        <%#Eval("ConvertStationName")%>
                                    </td>
                                    <td style="text-align: center;">
                                        <%#Eval("RegistantDate", "{0:yyyy-MM-dd}")%>
                                    </td>


                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </tbody>
                </table>
            </div>
            <uc1:PageControl ID="PageControl1" runat="server" />

        </div>
    </form>
</body>
</html>

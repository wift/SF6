﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using RM.Common.DotNetCode;
using System.Data;
using RM.Common.DotNetUI;
using RM.Web.App_Code;
using EGMNGS.Common;
using EGMNGS.Model;
using RM.Busines.DAL;
using RM.Busines.IDAO;
namespace RM.Web.GanPing
{
    public partial class ApplRecyGasRegDJPage : PageBase
    {
        #region 属性字段

        EGMNGS.BLL.ApplRecyGasReg bll = new EGMNGS.BLL.ApplRecyGasReg();
        EGMNGS.BLL.DetailsOfRecycleGas DetailsBLL = new EGMNGS.BLL.DetailsOfRecycleGas();
        private DataTable UserList
        {
            get
            {
                return ViewState["UserList"] as DataTable;
            }
            set { ViewState["UserList"] = value; }
        }
        private EGMNGS.Model.ApplRecyGasReg Model
        {
            get
            {
                if (ViewState["ApplRecyGasReg"] == null)
                {
                    return new EGMNGS.Model.ApplRecyGasReg();
                }
                return ViewState["ApplRecyGasReg"] as EGMNGS.Model.ApplRecyGasReg;
            }
            set { ViewState["ApplRecyGasReg"] = value; }
        }
        private string actionMode
        {
            get { return ViewState["ActionMode"] as String; }
            set { ViewState["ActionMode"] = value; }
        }
        public string PowerSupplyCode
        {
            get { return ViewState["PowerSupplyCode"] as String; }
            set { ViewState["PowerSupplyCode"] = value; }
        }
        public string PowerSupplyName
        {
            get { return ViewState["PowerSupplyName"] as String; }
            set { ViewState["PowerSupplyName"] = value; }
        }
        public DataTable DetailsTable
        {
            get
            {
                if (ViewState["DetailsTable"] == null)
                {
                    return new DataTable();
                }
                return ViewState["DetailsTable"] as DataTable;
            }
            set { ViewState["DetailsTable"] = value; }
        }
        /// <summary>
        /// 需要更新的数据
        /// </summary>
        private List<DetailsOfRecycleGas> UpdateList
        {
            get
            {
                if (ViewState["UpdateList"] == null)
                {
                    return new List<DetailsOfRecycleGas>();
                }
                return ViewState["UpdateList"] as List<DetailsOfRecycleGas>;
            }
            set { ViewState["UpdateList"] = value; }
        }
        /// <summary>
        /// 需要添加的数据
        /// </summary>
        private List<DetailsOfRecycleGas> AddList
        {
            get
            {
                if (ViewState["AddList"] == null)
                {
                    return new List<DetailsOfRecycleGas>();
                }
                return ViewState["AddList"] as List<DetailsOfRecycleGas>;
            }
            set { ViewState["AddList"] = value; }
        }

        private string actionModeDetails
        {
            get
            {
                if (ViewState["actionModeDetails"] == null)
                {
                    return string.Empty;
                }
                return ViewState["actionModeDetails"] as String;
            }
            set { ViewState["actionModeDetails"] = value; }
        }

        private string HSID
        {
            get
            {
                if (ViewState["actionModeDetails"] == null)
                {
                    return string.Empty;
                }
                return ViewState["actionModeDetails"] as String;
            }
            set { ViewState["actionModeDetails"] = value; }
        }
        #endregion

        #region 事件方法

        protected void Page_Load(object sender, EventArgs e)
        {
            this.PageControl1.pageHandler += new EventHandler(pager_PageChanged);

            if (!IsPostBack)
            {

                UserList = ComServies.GetAllUserInfo();
                string[] orgIdOrgName = ComServies.GetPowerApplyByUserID(User.UserId.ToString());
                PowerSupplyCode = orgIdOrgName[0];
                PowerSupplyName = orgIdOrgName[1];
                DropDownListBinder();
            }
        }

        /// <summary>
        /// 绑定下拉列
        /// </summary>
        private void DropDownListBinder()
        {
            EGMNGS.BLL.InfoPowerSupplyConvertStation bll = new EGMNGS.BLL.InfoPowerSupplyConvertStation();
            DataSet dsPowerSupplyCode = bll.GetList(string.Format("PowerSupplyCode='{0}'", PowerSupplyCode));
            this.ddlConvertStationName.DataSource = dsPowerSupplyCode.Tables[0];
            this.ddlConvertStationName.DataTextField = "ConvartStationName";
            this.ddlConvertStationName.DataValueField = "CovnertStationCode";
            this.ddlConvertStationName.DataBind();
            this.ddlConvertStationName.Items.Insert(0, string.Empty);

            EGMNGS.BLL.SYS_CODE sysCodeBll = new EGMNGS.BLL.SYS_CODE();
            DataSet dsYear = sysCodeBll.GetList("CODE_TYPE='Year' ORDER BY DISPLAY_ORDER");

            this.ddlYear.DataSource = dsYear.Tables[0];
            this.ddlYear.DataTextField = "CODE_CHI_DESC";
            this.ddlYear.DataValueField = "CODE";
            this.ddlYear.DataBind();

            DataSet dsMonth = sysCodeBll.GetList("CODE_TYPE='Month' ORDER BY DISPLAY_ORDER");

            this.ddlMonth.DataSource = dsMonth.Tables[0];
            this.ddlMonth.DataTextField = "CODE_CHI_DESC";
            this.ddlMonth.DataValueField = "CODE";
            this.ddlMonth.DataBind();


            DataSet dtStatus = sysCodeBll.GetList("CODE_TYPE='TaskStatus' ORDER BY DISPLAY_ORDER");

            this.ddlStatus.DataSource = dtStatus.Tables[0];
            this.ddlStatus.DataTextField = "CODE_CHI_DESC";
            this.ddlStatus.DataValueField = "CODE";
            this.ddlStatus.DataBind();

            this.ddlStatus.Items.Insert(0, string.Empty);


            DataTable dtPowerStation = ComServies.GetAllPowerStation();
            this.ddlPowerSupplyName.DataSource = dtPowerStation;
            this.ddlPowerSupplyName.DataTextField = "Organization_Name";
            this.ddlPowerSupplyName.DataValueField = "Organization_ID";
            this.ddlPowerSupplyName.DataBind();
            this.ddlPowerSupplyName.Items.Insert(0, string.Empty);


            RM_UserInfo_Dal userinfo = new RM_UserInfo_Dal();

            DataTable usertb = userinfo.GetUserInfoAll();
            this.ddlRecyclerOID.DataSource = usertb;
            this.ddlRecyclerOID.DataTextField = "User_Name";
            this.ddlRecyclerOID.DataValueField = "User_ID";
            this.ddlRecyclerOID.DataBind();
            this.ddlRecyclerOID.Items.Insert(0, string.Empty);

        }


        /// <summary>
        /// 绑定后激发事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void rp_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Label lblUserID = e.Item.FindControl("lblUserID") as Label;
                Label lblRecycleroid = e.Item.FindControl("lblRecycleroid") as Label;
                if (lblUserID != null)
                {
                    lblUserID.Text = GetUserName(lblUserID.Text);
                }
                if (lblRecycleroid != null)
                {
                    lblRecycleroid.Text = GetUserName(lblRecycleroid.Text);
                }
            }
        }

        /// <summary>
        /// 绑定数据，分页
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void pager_PageChanged(object sender, EventArgs e)
        {
            DataBindGrid();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void pager_PageChangedDetails(object sender, EventArgs e)
        {
            GetDetailsOfRecycleGas();
        }
        /// <summary>
        /// 绑定数据源
        /// </summary>
        private void DataBindGrid()
        {
            string sql = @"PowerSupplyCode IN(
                                                SELECT Organization_ID FROM RM_DB..Base_Organization WHERE ParentId='a0184ca3-9a65-444b-920f-88f1f4be7d5b'
                                                )";
            int count = bll.GetRecordCount(sql);
            DataSet ds = bll.GetListByPage(sql, "RecyCode desc", PageControl1.PageIndex, PageControl1.PageSize);
            ControlBindHelper.BindRepeaterList(ds.Tables[0], rp_Item);
            this.PageControl1.RecordCount = Convert.ToInt32(count);
        }

        /// <summary>
        /// 绑定数据到对象
        /// </summary>
        private void DataBinder()
        {
            Model.RecyCode = this.txtRecyCode.Text;
            Model.Year = this.ddlYear.SelectedValue;
            Model.Month = this.ddlMonth.SelectedValue;
            Model.ConvertStationCode = this.ddlConvertStationName.SelectedValue;
            Model.ConvertStationName = this.ddlConvertStationName.SelectedItem.Text;
            Model.PowerSupplyCode = this.ddlPowerSupplyName.SelectedValue;
            Model.PowerSupplyName = this.ddlPowerSupplyName.SelectedItem.Text;
            if (this.txtAmountRecovery.Text.Trim().Length > 0)
            {
                Model.AmountRecovery = Decimal.Parse(this.txtAmountRecovery.Text);
            }
            if (this.txtCylinderCount.Text.Trim().Length > 0)
            {
                Model.CylinderCount = Int32.Parse(this.txtCylinderCount.Text);
            }

            Model.RecoveryDesc = this.txtRecoveryDesc.Text;
            Model.ContactsName = this.txtContacts.Text;
            Model.OfficeTel = this.txtOfficeTel.Text;
            Model.PhoneNum = this.txtPhoneNum.Text;
            Model.RecyclerOID = this.ddlRecyclerOID.SelectedValue;
            if (this.txtRecyDate.Text.Trim().Length > 0)
            {
                Model.RecyDate = Convert.ToDateTime(this.txtRecyDate.Text);
            }
        }

        /// <summary>
        /// 初始化控件
        /// </summary>
        private void InitControl()
        {
            this.txtRecyCode.Text = Model.RecyCode;
            this.ddlYear.SelectedValue = Model.Year;
            this.ddlMonth.SelectedValue = Model.Month;
            this.ddlPowerSupplyName.SelectedValue = Model.PowerSupplyCode;

            this.ddlConvertStationName.Items.Clear();
            this.ddlConvertStationName.Items.Add(new ListItem(Model.ConvertStationName, Model.ConvertStationCode));
            //this.ddlConvertStationName.SelectedValue = Model.ConvertStationCode;
            this.txtAmountRecovery.Text = Model.AmountRecovery.ToString();
            this.txtCylinderCount.Text = Model.CylinderCount.ToString();
            this.txtRecoveryDesc.Text = Model.RecoveryDesc;
            this.txtContacts.Text = Model.ContactsName;
            this.txtOfficeTel.Text = Model.OfficeTel;
            this.txtPhoneNum.Text = Model.PhoneNum;
            this.ddlStatus.SelectedValue = Model.Status;
            this.txtRegistrantOID.Text = GetUserName(Model.RegistrantOID);
            if (Model.RegistrantDate != null)
            {
                this.txtRegistrantDate.Text = Model.RegistrantDate.Value.ToShortDateString(); ;
            }

            this.txtAuditorOID.Text = GetUserName(Model.AuditorOID);

            if (Model.AuditsDate != null)
            {
                this.txtAuditsDate.Text = Model.AuditsDate.Value.ToShortDateString();
            }

            if (this.ddlRecyclerOID.Items.FindByValue(Model.RecyclerOID) != null)
            {
                this.ddlRecyclerOID.SelectedValue = Model.RecyclerOID;
            }


            if (Model.RecyDate != null)
            {
                this.txtRecyDate.Text = Model.RecyDate.Value.ToShortDateString();
            }

            //this.txtRecyDate.Text = Model.RecyDate.Value.ToShortDateString();
        }

        /// <summary>
        /// 获取用户名
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        private string GetUserName(string userId)
        {
            DataRow[] dr = UserList.Select(string.Format("User_ID='{0}'", userId));
            if (dr.Length > 0)
            {
                return dr[0][1].ToString();
            }
            return string.Empty;
        }

        private void ClearControl()
        {
            Model = new ApplRecyGasReg();
            InitControl();
        }
        #endregion

        #region 按钮事件
        /// <summary>
        /// 确认
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnConfirm_Click(object sender, EventArgs e)
        {

            if (Model.Status == "1" || Model.Status == "3")
            {
                int GPCount;
                decimal AmountRecovery;
                ComServies.GetAmountRecovery(Model.RecyCode, out GPCount, out AmountRecovery);
                //  Model.RecyclerOID = User.UserId.ToString();
                //  Model.RecyDate = DateTime.Now;
                Model.Status = "2";
                Model.AmountRecovery = AmountRecovery;
                Model.CylinderCount = GPCount;
                bll.Update(Model);
                for (int i = 0; i < this.rptDetails.Items.Count; i++)
                {
                    TextBox txtCylinderCode = this.rptDetails.Items[i].FindControl("txtCylinderCode") as TextBox;
                    ComServies.UpdateBookCylinderInfoStatusByWhere(txtCylinderCode.Text, Convert.ToInt32(GPStatusEnum.充装回收气).ToString());
                }
                ShowMsgHelper.Alert("确认成功!");
                InitControl();
                DataBindGrid();
            }
        }

        private bool CheckCylinderCode()
        {
            bool flag = false;
            for (int i = 0; i < this.rptDetails.Items.Count; i++)
            {
                System.Web.UI.WebControls.CheckBox checkbox = (System.Web.UI.WebControls.CheckBox)rptDetails.Items[i].FindControl("checkbox");


                TextBox txtCC = this.rptDetails.Items[i].FindControl("txtCylinderCode") as TextBox;

                if (txtCC.Text.Length == 0)
                {
                    flag = true;
                }
            }

            return flag;
        }
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            if (Model.OID == 0 || Model.Status != "0")
            {
                return;
            }

            int GPCount;
            decimal AmountRecovery;
            ComServies.GetAmountRecovery(Model.RecyCode, out GPCount, out AmountRecovery);
            //气体回收申请：提交检查：回收气量与从表的总量是否相等,钢瓶数与钢瓶数。
            //if (Model.CylinderCount != GPCount)
            //{
            //    ShowMsgHelper.showWarningMsg("钢瓶数不对!");
            //    return;
            //}
            //else if (AmountRecovery != Model.AmountRecovery)
            //{
            //    ShowMsgHelper.showWarningMsg("回收气量不对!");
            //    return;
            //}

            Model.Status = "1";
            Model.AmountRecovery = AmountRecovery;
            Model.CylinderCount = GPCount;
            if (bll.Update(Model))
            {
                ShowMsgHelper.Alert("提交成功！");
                InitControl();
                DataBindGrid();
            }

        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            DataBindGrid();
        }

        /// <summary>
        /// 获取当前对象
        /// </summary>
        private void ShowMaxMode()
        {
            Model = bll.GetModel(bll.GetMaxId() - 1);
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {


                if (actionMode == "Add")
                {
                    DataBinder();
                    Model.RegistrantOID = User.UserId.ToString();

                    int count = bll.GetRecordCount(string.Format("RecyCode='{0}'", Model.RecyCode));
                    if (count > 0)
                    {
                        ShowMsgHelper.showWarningMsg("回收编号已经存在!");
                        return;
                    }

                    if (bll.Add(Model) > 0)
                    {

                        actionMode = string.Empty;
                    }

                    DataBindGrid();
                    ShowMaxMode();
                }
                else if (actionMode == "Edit")
                {
                    if (Convert.ToInt32(Model.Status) == 2)
                    {
                        ShowMsgHelper.Alert_Wern("“已确认”状态无法修改数据！");
                        InitControl();
                        return;
                    }

                    DataBinder();
                    if (bll.Update(Model))
                    {
                        EGMNSShowMsg.ShowEditMsgSuccess();
                        actionMode = string.Empty;
                    }
                    DataBindGrid();
                }
            }

            catch (ArgumentOutOfRangeException ex)
            {
                string[] errs = CommonHelper.GetValue(ex.Message, "@").Split(':');
                ShowMsgHelper.ExecuteScript(string.Format("ChangeCss($('#txt{0}'),'长度不能超{1}字')", errs[0], errs[1]));
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {

            actionMode = "Add";
            Model = new EGMNGS.Model.ApplRecyGasReg(actionMode);
            Model.PowerSupplyCode = PowerSupplyCode;
            Model.PowerSupplyName = PowerSupplyName;
            Model.RegistrantDate = DateTime.Now;
            Model.RegistrantOID = User.UserId.ToString();
            Model.Status = "0";
            InitControl();
            GetDetailsOfRecycleGas();

            this.txtAuditsDate.Text = "";
            this.txtRecyDate.Text = "";
        }


        protected void btnDel_Click(object sender, EventArgs e)
        {
            if (Convert.ToInt32(Model.Status) == 2)
            {
                ShowMsgHelper.Alert_Wern("“已确认”状态不可以删除");
            }
            else
            {
                bll.Delete(Model.OID);
                Model = new EGMNGS.Model.ApplRecyGasReg();
                InitControl();
                DataBindGrid();
                ShowMsgHelper.Alert("删除成功！");
            }
        }

        protected void rp_Item_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            int oid = Convert.ToInt32(e.CommandArgument);

            Model = bll.GetModel(oid);
            InitControl();
            actionMode = "Edit";


            //绑定明细表
            GetDetailsOfRecycleGas();
        }
        #endregion

        #region Details

        private void GetDetailsOfRecycleGas()
        {
            DetailsTable = DetailsBLL.GetList(string.Format("ApplRecyGasReg_RecyCode='{0}'", Model.RecyCode)).Tables[0];
            int count = DetailsBLL.GetRecordCount(string.Format("ApplRecyGasReg_RecyCode='{0}'", Model.RecyCode));
            ControlBindHelper.BindRepeaterList(DetailsTable, rptDetails);
        }
        protected void btnAddDetails_Click(object sender, EventArgs e)
        {
            if (Convert.ToInt32(Model.Status) == 2)
            {
                ShowMsgHelper.Alert_Wern("“已确认”状态无法修改数据！");
                GetDetailsOfRecycleGas();
                return;
            }
            //首先，恢复数据源
            RetriRepeter();

            //首先，恢复数据源
            DataTable dt = DetailsTable;
            // LoadTestData(dt);
            DataRow row = dt.NewRow();
            row[2] = string.Empty;
            row[3] = string.Empty;
            row[4] = 0.0;
            row[5] = string.Empty;
            row[6] = string.Empty;
            dt.Rows.Add(row);

            rptDetails.DataSource = dt;
            rptDetails.DataBind();


            actionModeDetails = "Add";
        }
        private void RetriRepeter()
        {
            for (int i = 0; i < this.rptDetails.Items.Count; i++)
            {
                TextBox txtAmountRecovery = this.rptDetails.Items[i].FindControl("txtAmountRecovery") as TextBox;
                DetailsTable.Rows[i]["AmountRecovery"] = Convert.ToDecimal(txtAmountRecovery.Text);

                TextBox txtCylinderCode = this.rptDetails.Items[i].FindControl("txtCylinderCode") as TextBox;
                DetailsTable.Rows[i]["CylinderCode"] = txtCylinderCode.Text;

                TextBox txtRecoveryDesc = this.rptDetails.Items[i].FindControl("txtRecoveryDesc") as TextBox;
                DetailsTable.Rows[i]["RecoveryDesc"] = txtRecoveryDesc.Text;

                TextBox txtDevName = this.rptDetails.Items[i].FindControl("txtDevName") as TextBox;
                DetailsTable.Rows[i]["InfoDevPara_DevName"] = txtDevName.Text;

                DropDownList ddlGasType = this.rptDetails.Items[i].FindControl("ddlGasType") as DropDownList;
                DetailsTable.Rows[i]["GasType"] = ddlGasType.SelectedValue;

            }
        }
        protected void btnDelDetails_Click(object sender, EventArgs e)
        {
            try
            {

                if (Convert.ToInt32(Model.Status) == 2)
                {
                    ShowMsgHelper.Alert_Wern("“已确认”状态无法修改数据！");
                    return;
                }

                string strID = HiddenField1.Value;
                if (strID.Trim().Length == 0)
                {
                    return;
                }

                if (DetailsBLL.DeleteList(strID))
                {
                    ShowMsgHelper.Alert("删除成功！");
                    GetDetailsOfRecycleGas();
                }
                else
                {
                    ShowMsgHelper.Alert("删除失败！");
                }
            }
            catch (Exception ex)
            {
                ShowMsgHelper.Alert("删除失败！" + ex.Message);
            }
        }

        protected void btnSaveDetails_Click(object sender, EventArgs e)
        {

            if (Convert.ToInt32(Model.Status) == 2)
            {
                ShowMsgHelper.Alert_Wern("“已确认”状态无法修改数据！");
                GetDetailsOfRecycleGas();
                return;
            }

            GetCheckBox();

            if (actionModeDetails.Equals("Add"))
            {
                if (AddList.Count > 0)
                {
                    //批量添加
                    foreach (var item in AddList)
                    {
                        BookCylinderInfo obj = new EGMNGS.BLL.BookCylinderInfo().GetModelByCylinderCode(item.CylinderCode);
                        if (obj == null && item.CylinderCode.Trim().Length > 0)
                        {
                            ShowMsgHelper.Alert_Wern(string.Format("{0}此钢瓶不存在！", item.CylinderCode));
                            continue;
                        }
                        else if (obj != null)
                        {
                            if (Convert.ToInt32(EGMNGS.Common.GPStatusEnum.已充待检气) == Convert.ToInt32(obj.Status) ||
                                Convert.ToInt32(EGMNGS.Common.GPStatusEnum.已充合格气) == Convert.ToInt32(obj.Status) ||
                                Convert.ToInt32(EGMNGS.Common.GPStatusEnum.待送检) == Convert.ToInt32(obj.Status) ||
                                Convert.ToInt32(EGMNGS.Common.GPStatusEnum.已报废) == Convert.ToInt32(obj.Status)
                                )//已充待检气、已充合格气、待送检、已报废状态的钢瓶
                            {
                                ShowMsgHelper.Alert_Wern(string.Format("{0}此钢瓶无法登记！", item.CylinderCode));
                                continue;
                            }

                        }

                        DetailsBLL.Add(item);

                    }
                    AddList.Clear();
                }

                ShowMsgHelper.Alert("添加成功！");
                actionModeDetails = string.Empty;
            }

            if (UpdateList.Count > 0)
            {
                //批量修改
                foreach (var item in UpdateList)
                {
                    BookCylinderInfo obj = new EGMNGS.BLL.BookCylinderInfo().GetModelByCylinderCode(item.CylinderCode);
                    if (obj == null && item.CylinderCode.Trim().Length > 0)
                    {
                        ShowMsgHelper.Alert_Wern(string.Format("{0}此钢瓶不存在！", item.CylinderCode));
                        item.CylinderCode = string.Empty;
                        item.InfoDevPara_DevName = string.Empty;
                        item.AmountRecovery = null;
                        item.RecoveryDesc = string.Empty;
                        continue;
                    }

                    if (obj != null)
                    {

                        if (Convert.ToInt32(EGMNGS.Common.GPStatusEnum.已充待检气) == Convert.ToInt32(obj.Status) ||
                            Convert.ToInt32(EGMNGS.Common.GPStatusEnum.已充合格气) == Convert.ToInt32(obj.Status) ||
                            Convert.ToInt32(EGMNGS.Common.GPStatusEnum.待送检) == Convert.ToInt32(obj.Status) ||
                            Convert.ToInt32(EGMNGS.Common.GPStatusEnum.已报废) == Convert.ToInt32(obj.Status)
                            )//已充待检气、已充合格气、待送检、已报废状态的钢瓶
                        {
                            ShowMsgHelper.Alert_Wern(string.Format("{0}此钢瓶无法登记！", item.CylinderCode));
                            continue;
                        }

                    }

                    DetailsBLL.Update(item);
                    //  ComServies.UpdateBookCylinderInfoStatusByWhere(item.CylinderCode, Convert.ToInt32(GPStatusEnum.充装回收气).ToString());
                }
                UpdateList.Clear();
            }

            GetDetailsOfRecycleGas();

        }

        protected void rptDetails_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                CheckBox cb = e.Item.FindControl("checkbox") as CheckBox;
                if (cb.ToolTip.Length == 0)
                {
                    cb.Checked = true;
                }

                DropDownList ddlGasType = e.Item.FindControl("ddlGasType") as DropDownList;

                EGMNGS.BLL.SYS_CODE sysCodeBll = new EGMNGS.BLL.SYS_CODE();
                DataSet dsYear = sysCodeBll.GetList("CODE_TYPE='RecyStatus' ORDER BY DISPLAY_ORDER");

                ddlGasType.DataSource = dsYear.Tables[0];
                ddlGasType.DataTextField = "CODE_CHI_DESC";
                ddlGasType.DataValueField = "CODE";
                ddlGasType.DataBind();

                DataRowView dr = (DataRowView)e.Item.DataItem;

                var gasTypeItem = ddlGasType.Items.FindByValue(dr["GasType"].ToString());
                if (gasTypeItem != null)
                {
                    gasTypeItem.Selected = true;
                }
            }
        }


        /// <summary>
        /// 
        /// </summary>
        private void GetCheckBox()
        {
            UpdateList = new List<DetailsOfRecycleGas>();
            AddList = new List<DetailsOfRecycleGas>();
            for (int i = 0; i < this.rptDetails.Items.Count; i++)
            {
                System.Web.UI.WebControls.CheckBox checkbox = (System.Web.UI.WebControls.CheckBox)rptDetails.Items[i].FindControl("checkbox");

                TextBox txtCylinderCode = this.rptDetails.Items[i].FindControl("txtCylinderCode") as TextBox;
                //if (txtCylinderCode.Text.Trim().Length == 0)
                //{
                //    continue;
                //}

                if (checkbox.Checked == true && checkbox.ToolTip.Length > 0)
                {
                    //  TextBox txtCylinderCode = this.rptDetails.Items[i].FindControl("txtCylinderCode") as TextBox;
                    TextBox txtDevName = this.rptDetails.Items[i].FindControl("txtDevName") as TextBox;
                    DropDownList ddlGasType = this.rptDetails.Items[i].FindControl("ddlGasType") as DropDownList;
                    TextBox txtAmountRecovery = this.rptDetails.Items[i].FindControl("txtAmountRecovery") as TextBox;
                    TextBox txtRecoveryDesc = this.rptDetails.Items[i].FindControl("txtRecoveryDesc") as TextBox;

                    DetailsOfRecycleGas DetailsOfRecycleGasObj = new EGMNGS.BLL.DetailsOfRecycleGas().GetModel(int.Parse(checkbox.ToolTip));
                    //DetailsOfRecycleGasObj.OID = int.Parse(checkbox.ToolTip);
                    DetailsOfRecycleGasObj.CylinderCode = txtCylinderCode.Text;
                    //DetailsOfRecycleGasObj.ApplRecyGasReg_RecyCode = Model.RecyCode;
                    // DetailsOfRecycleGasObj.InfoDevPara_DevCode = ddlDevlist.SelectedValue;
                    DetailsOfRecycleGasObj.InfoDevPara_DevName = txtDevName.Text;

                    DetailsOfRecycleGasObj.GasType = ddlGasType.SelectedValue;
                    if (txtAmountRecovery.Text.Trim().Length > 0)
                    {
                        DetailsOfRecycleGasObj.AmountRecovery = decimal.Parse(txtAmountRecovery.Text);
                    }

                    DetailsOfRecycleGasObj.RecoveryDesc = txtRecoveryDesc.Text;

                    UpdateList.Add(DetailsOfRecycleGasObj);
                }
                else if (checkbox.Checked == true && checkbox.ToolTip.Length == 0)
                {
                    // TextBox txtCylinderCode = this.rptDetails.Items[i].FindControl("txtCylinderCode") as TextBox;
                    TextBox txtDevName = this.rptDetails.Items[i].FindControl("txtDevName") as TextBox;
                    DropDownList ddlGasType = this.rptDetails.Items[i].FindControl("ddlGasType") as DropDownList;
                    TextBox txtAmountRecovery = this.rptDetails.Items[i].FindControl("txtAmountRecovery") as TextBox;
                    TextBox txtRecoveryDesc = this.rptDetails.Items[i].FindControl("txtRecoveryDesc") as TextBox;

                    DetailsOfRecycleGas DetailsOfRecycleGasObj = new EGMNGS.Model.DetailsOfRecycleGas();
                    // DetailsOfRecycleGasObj.OID = int.Parse(checkbox.ToolTip);
                    DetailsOfRecycleGasObj.ApplRecyGasReg_RecyCode = Model.RecyCode;
                    DetailsOfRecycleGasObj.CylinderCode = txtCylinderCode.Text;
                    //DetailsOfRecycleGasObj.InfoDevPara_DevCode = ddlDevlist.SelectedValue;
                    //DetailsOfRecycleGasObj.InfoDevPara_DevName = ddlDevlist.SelectedItem.Text;
                    DetailsOfRecycleGasObj.InfoDevPara_DevName = txtDevName.Text;
                    DetailsOfRecycleGasObj.GasType = ddlGasType.SelectedValue;
                    if (txtAmountRecovery.Text.Trim().Length > 0)
                    {
                        DetailsOfRecycleGasObj.AmountRecovery = decimal.Parse(txtAmountRecovery.Text);
                    }

                    DetailsOfRecycleGasObj.RecoveryDesc = txtRecoveryDesc.Text;

                    AddList.Add(DetailsOfRecycleGasObj);
                }
            }
        }

        protected void ddlPowerSupplyName_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadPowerSupplyName();

        }

        private void LoadPowerSupplyName()
        {
            this.ddlConvertStationName.Items.Clear();
            EGMNGS.BLL.InfoPowerSupplyConvertStation bll = new EGMNGS.BLL.InfoPowerSupplyConvertStation();
            DataSet dsPowerSupplyCode = bll.GetList(string.Format("PowerSupplyCode='{0}'", this.ddlPowerSupplyName.SelectedValue));
            this.ddlConvertStationName.DataSource = dsPowerSupplyCode.Tables[0];
            this.ddlConvertStationName.DataTextField = "ConvartStationName";
            this.ddlConvertStationName.DataValueField = "CovnertStationCode";
            this.ddlConvertStationName.DataBind();
            this.ddlConvertStationName.Items.Insert(0, string.Empty);
        }

        #endregion
    }
}
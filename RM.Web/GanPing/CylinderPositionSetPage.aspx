﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CylinderPositionSetPage.aspx.cs" Inherits="RM.Web.GanPing.CylinderPositionSetPage" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Src="~/UserControl/PageControl.ascx" TagName="PageControl" TagPrefix="uc1" %>
<%@ Register Src="~/UserControl/LoadButton.ascx" TagName="LoadButton" TagPrefix="uc2" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>库区设置维护</title>
    <link href="/Themes/Styles/Site.css" rel="stylesheet" type="text/css" />
    <script src="/Themes/Scripts/Validator/JValidator.js" type="text/javascript"></script>
    <script src="/Themes/Scripts/jquery-1.8.2.min.js" type="text/javascript"></script>
    <script src="/Themes/Scripts/jquery.pullbox.js" type="text/javascript"></script>
    <script src="/Themes/Scripts/FunctionJS.js" type="text/javascript"></script>
    <script src="/Themes/Scripts/DatePicker/WdatePicker.js" type="text/javascript"></script>
    <script type="text/javascript">
        //添加
        function add() {
            document.getElementById('<%= this.btnAdd.ClientID %>').click();
        }
        //查询
        function search() {

            document.getElementById('<%= this.btnSearch.ClientID %>').click();
        }
        //删除
        function Delete() {
            showConfirmMsg("此操作不可恢复，您确定要删除吗？", function (r) {
                if (r) {
                    document.getElementById('<%= this.btnDel.ClientID %>').click();
                }
            });


        }
        //保存
        function SaveForm() {
            document.getElementById('<%= this.btnSave.ClientID %>').click();
        }


        $(function () {
            InitControl();
        })

        function InitControl() {
            $(".div-body").PullBox({ dv: $(".div-body"), obj: $("#table1").find("tr") });
            divresize(110);

        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional">
        <ContentTemplate>
            <table width="100%">
                <colgroup>
                    <col width="11%" />
                    <col width="18%" />
                    <col width="14%" />
                    <col width="17%" />
                    <col width="15%" />
                    <col width="25%" />
                </colgroup>
                <tr>
                    <td class="inner_cell_right">
                        库区编码：
                    </td>
                    <td>
                       <cc1:ExTextBox ID="txtPositionCode" runat="server" checkexpession="LenStr" datacol="yes" length="50"
                        err="此项" FieldName="PositionCode" BindName="CylinderPositionSetObj" />
                    </td>
                    <td class="inner_cell_right">
                        库区名称：
                    </td>
                    <td>
                        <cc1:ExTextBox ID="txtPositionName" runat="server" checkexpession="LenStr" datacol="yes" length="50"
                        err="此项" FieldName="PositionName" BindName="CylinderPositionSetObj" />
                    </td>
                    <td class="inner_cell_right">
                        存放说明：
                    </td>
                    <td>
                     <cc1:ExTextBox ID="txtpositionRemarks" runat="server" checkexpession="LenStr" datacol="yes" length="50"
                        err="此项" FieldName="positionRemarks" BindName="CylinderPositionSetObj" />
                    </td>
                </tr>
            
            </table>
            <div style="text-align: left;">
                <uc2:LoadButton ID="LoadButton1" runat="server" />
            </div>
            <div style="text-align: right; display: none;">
    
                <asp:Button ID="btnSearch" runat="server" OnClick="btnSearch_Click" Text="btnSearch_Click" />
                <asp:Button ID="btnSave" runat="server" OnClick="btnSave_Click" Text="btnSave_Click"
                    OnClientClick="return CheckDataValid('#form1');" />
                <asp:Button ID="btnAdd" runat="server" OnClick="btnAdd_Click" Text="btnAdd_Click" />
                <asp:Button ID="btnDel" runat="server" OnClick="btnDel_Click" Text="btnDel_Click" />
            </div>
            <div class="div-body">
                <table id="table1" class="grid" singleselect="true">
                    <colgroup>
                        <col width="10%" />
                        <col width="20%" />
                        <col width="70%" />
                      
                    </colgroup>
                    <thead>
                        <tr>
                   
                            <td style="text-align: center;">
                                库区编码
                            </td>
                            <td style="text-align: center;">
                                库区名称
                            </td>
                            <td style="text-align: center;">
                               存放说明
                            </td>
                           
                        </tr>
                    </thead>
                    <tbody>
                        <asp:Repeater ID="rp_Item" runat="server"  OnItemCommand="rp_Item_ItemCommand">
                            <ItemTemplate>
                                <tr>
                                    <td style="text-align: center;">
                                        <asp:LinkButton ID="LBtnDel" runat="server" OnClientClick="selectrow(this)" CommandArgument='<%#Eval("OID") %>' CommandName="del"><%#Eval("PositionCode")%></asp:LinkButton>
                                    </td>
                                    <td style="text-align: center;">
                                        <%#Eval("PositionName")%>
                                    </td>
                                    <td style="text-align: center;">
                                        <%#Eval("positionRemarks")%>
                                    </td>
                             
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </tbody>
                </table>
            </div>
            <uc1:PageControl ID="PageControl1" runat="server" />
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnAdd" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>
    </form>
</body>
</html>

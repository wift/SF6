﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BatchShowCylinderInspectDetails.aspx.cs"
    Inherits="RM.Web.GanPing.BatchShowCylinderInspectDetails" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>送检清单</title>
    <link href="/Themes/Styles/Site.css" rel="stylesheet" type="text/css" />
    <script src="/Themes/Scripts/Validator/JValidator.js" type="text/javascript"></script>
    <script src="/Themes/Scripts/jquery-1.8.2.min.js" type="text/javascript"></script>
    <script src="/Themes/Scripts/jquery.pullbox.js" type="text/javascript"></script>
    <script src="/Themes/Scripts/FunctionJS.js" type="text/javascript"></script>
    <script src="/Themes/Scripts/DatePicker/WdatePicker.js" type="text/javascript"></script>
    <script type="text/javascript">
        function BatchAdd() {
            var key = CheckboxValue();
            showConfirmMsg("您确定要批量添加吗？", function (r) {
                if (r) {
                    document.getElementById('<%= this.HiddenField1.ClientID %>').value = key.toString();
                    document.getElementById('<%= this.btnBatchAdd.ClientID %>').click();
                }
            });

        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:HiddenField ID="HiddenField1" runat="server" />
    <div class="div-body1">
        <table id="tableDetails" class="grid1">
            <colgroup>
                <col width="11%" />
                <col width="11%" />
                <col width="11%" />
                <col width="11%" />
                <col width="11%" />
                <col width="11%" />
                <col width="11%" />
                <col width="11%" />
                <col width="12%" />
            </colgroup>
            <thead>
                <tr>
                    <td style="width: 20px; text-align: left;">
                        <label id="checkAllOff" onclick="CheckAllLine()" title="全选">
                            &nbsp;</label>
                    </td>
                    <td style="text-align: center;">
                        钢瓶编码
                    </td>
                    <td style="text-align: center;">
                        钢瓶钢印号
                    </td>
                    <td style="text-align: center;">
                        钢瓶容量
                    </td>
                    <td style="text-align: center;">
                        新有效日期
                    </td>
                    <td style="text-align: center;">
                        是否合格
                    </td>
                    <td style="text-align: center;">
                        送检人
                    </td>
                    <td style="text-align: center;">
                        送检日期
                    </td>
                    <td style="text-align: center;">
                        备注说明
                    </td>
                </tr>
            </thead>
            <tbody>
                <asp:Repeater ID="rp_Item" runat="server" OnItemDataBound="rp_ItemDataBound">
                    <ItemTemplate>
                        <tr>
                            <td style="text-align: center;">
                                <%#Eval("CylinderCode")%>
                            </td>
                            <td style="text-align: center;">
                                <%#Eval("CylinderSealNo")%>
                            </td>
                            <td style="text-align: center;">
                                <asp:Label ID="lblCylinderCapacity" runat="server" Text='<%#Eval("CylinderCapacity")%>'></asp:Label>
                            </td>
                            <td style="text-align: center;">
                                <%#Eval("NewEffectiveDate", "{0:yyyy-MM-dd}")%>
                            </td>
                            <td style="text-align: center;">
                                <asp:Label ID="lblIsPass" runat="server" Text='<%#Eval("IsPass")%>'></asp:Label>
                            </td>
                            <td style="text-align: center;">
                                <asp:Label ID="lblSendCheckOID" runat="server" Text='<%#Eval("SendCheckOID")%>'></asp:Label>
                            </td>
                            <td style="text-align: center;">
                                <%#Eval("SendCheckDate", "{0:yyyy-MM-dd}")%>
                            </td>
                            <td style="text-align: center;">
                                <%#Eval("Remarks")%>
                            </td>
                        </tr>
                    </ItemTemplate>
              
                </asp:Repeater>
            </tbody>
        </table>
        <div style="display: none;">
            <asp:Button ID="btnBatchAdd" runat="server" Text="" OnClick="btnBatchAdd_Click" />
        </div>
    </div>
    </form>
</body>
</html>

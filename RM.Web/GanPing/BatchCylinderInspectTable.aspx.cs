﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using RM.Common.DotNetUI;
using EGMNGS.Model;
using RM.Web.App_Code;
using RM.Common.DotNetBean;

namespace RM.Web.GanPing
{
    public partial class BatchCylinderInspectTable : Page
    {
        EGMNGS.BLL.BookCylinderInfo bll = new EGMNGS.BLL.BookCylinderInfo();
        EGMNGS.BLL.CylinderInspectTable bllInspect = new EGMNGS.BLL.CylinderInspectTable();
        private Dictionary<string, string> lsCFStatus
        {
            get
            {
                if (Session["GPStatus"] != null)
                {
                    return Session["GPStatus"] as Dictionary<string, string>;
                }
                return new Dictionary<string, string>();
            }

            set
            {
                Session["GPStatus"] = value;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                EGMNGS.BLL.SYS_CODE sysCodeBll = new EGMNGS.BLL.SYS_CODE();
                lsCFStatus = sysCodeBll.GetModelList("CODE_TYPE='GPStatus' ORDER BY DISPLAY_ORDER").ToDictionary(o => o.CODE, o => o.CODE_CHI_DESC);

                DataBindGrid();

            }
        }
        /// <summary>
        /// 绑定数据源
        /// </summary>
        private void DataBindGrid()
        {
            DataSet ds = bll.GetList(" Status not in('5','6') ORDER BY EffectiveDate,Status,CylinderCode");
            ControlBindHelper.BindRepeaterList(ds.Tables[0], rp_Item);
        }
        protected void btnBatchAdd_Click(object sender, EventArgs e)
        {
            if (HiddenField1.Value.Length > 0)
            {
                List<BookCylinderInfo> lb = bll.GetModelList(string.Format("oid in ({0})", HiddenField1.Value));

                foreach (var item in lb)
                {
                    CylinderInspectTable tempModel = new CylinderInspectTable();
                    tempModel.CylinderCode = item.CylinderCode;
                    tempModel.OriginalEffectiveDate = item.EffectiveDate;
                    tempModel.Status = item.Status;
                    tempModel.RegistantsOID = RequestSession.GetSessionUser().UserId.ToString();
                    tempModel.RegistantsDate = DateTime.Now;
                    bllInspect.Add(tempModel);
                }

                ShowMsgHelper.AlertMsg("批量添加成功！");
            }
        }

        /// <summary>
        /// 绑定后激发事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void rp_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {

                Label lblStatus = e.Item.FindControl("lblStatus") as Label;
                if (lblStatus != null)
                {
                    if (lblStatus.Text.Length > 0)
                    {
                        lblStatus.Text = lsCFStatus[lblStatus.Text];

                    }
                }
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using RM.Common.DotNetUI;
using System.Data;
using EGMNGS.Common;

namespace RM.Web.GanPing
{
    public partial class RecordGPFIll : System.Web.UI.Page
    {
        public Dictionary<string, string> GasType
        {
            get
            {
                if (ViewState["GasType"] == null)
                {
                    Dictionary<string, string> GasType = new Dictionary<string, string>();
                    EGMNGS.BLL.SYS_CODE sysCodeBll = new EGMNGS.BLL.SYS_CODE();
                    DataSet dsGasType = sysCodeBll.GetList("CODE_TYPE='GasType' ORDER BY DISPLAY_ORDER");
                    foreach (DataRow item in dsGasType.Tables[0].Rows)
                    {
                        GasType.Add(item["CODE"].ToString(), item["CODE_CHI_DESC"].ToString());
                    }
                    ViewState["GasType"] = GasType;
                }

                return ViewState["GasType"] as Dictionary<string, string>;
            }
            set { ViewState["GasType"] = value; }
        }

        public Dictionary<string, string> GPStatus
        {
            get
            {
                if (ViewState["GPStatus"] == null)
                {
                    Dictionary<string, string> GPStatus = new Dictionary<string, string>();
                    EGMNGS.BLL.SYS_CODE sysCodeBll = new EGMNGS.BLL.SYS_CODE();
                    DataSet dsGPStatus = sysCodeBll.GetList("CODE_TYPE='GPStatus' ORDER BY DISPLAY_ORDER");
                    foreach (DataRow item in dsGPStatus.Tables[0].Rows)
                    {
                        GPStatus.Add(item["CODE"].ToString(), item["CODE_CHI_DESC"].ToString());
                    }
                    GPStatus.Add(string.Empty,string.Empty);
                    ViewState["GPStatus"] = GPStatus;
                }

                return ViewState["GPStatus"] as Dictionary<string, string>;
            }
            set { ViewState["GPStatus"] = value; }
        }
        private DataTable UserList
        {
            get
            {
                return ViewState["UserList"] as DataTable;
            }
            set { ViewState["UserList"] = value; }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            this.PageControl1.pageHandler += new EventHandler(pager_PageChanged);
            UserList = ComServies.GetAllUserInfo();
            DataBindGrid();
        }

        /// <summary>
        /// 绑定数据源
        /// </summary>
        private void DataBindGrid()
        {
            string cylinderCode = Convert.ToString(Session["CylinderCode"]);
            string sqlWhere = string.Format("CylinderCode='{0}' order by RegistrantDate desc", cylinderCode);
            DataSet ds = new EGMNGS.BLL.BookGasFill().GetList(sqlWhere);// ComServies.GetListByPage(sqlStr, string.Empty, PageControl1.PageIndex, PageControl1.PageSize);
            ControlBindHelper.BindRepeaterList(ds.Tables[0], rp_Item);
            this.PageControl1.RecordCount = Convert.ToInt32(ds.Tables[0].Rows.Count);
        }

        /// <summary>
        /// 绑定数据，分页
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void pager_PageChanged(object sender, EventArgs e)
        {
            DataBindGrid();
        }

        /// <summary>
        /// 获取用户名
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public string GetUserName(string userId)
        {
            DataRow[] dr = UserList.Select(string.Format("User_ID='{0}'", userId));
            if (dr.Length > 0)
            {
                return dr[0][1].ToString();
            }
            return string.Empty;
        }
    }
}
﻿using RM.Common.DotNetUI;
using RM.Web.App_Code;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace RM.Web.GanPing
{
    public partial class SelectSBDialog : PageBase
    {
        EGMNGS.BLL.InfoDevPara bll = new EGMNGS.BLL.InfoDevPara();
        public EGMNGS.Model.InfoDevPara Model
        {
            get
            {
                if (ViewState["InfoDevPara"] == null)
                {
                    return new EGMNGS.Model.InfoDevPara();
                }
                return ViewState["InfoDevPara"] as EGMNGS.Model.InfoDevPara;
            }
            set { ViewState["InfoDevPara"] = value; }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            this.PageControl1.pageHandler += new EventHandler(pager_PageChanged);
            if (!IsPostBack)
            {

            }
        }
        protected override void OnLoad(EventArgs e)
        {
            CheckURLPermission = true;
            base.OnLoad(e);
        }
        /// <summary>
        /// 绑定数据，分页
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void pager_PageChanged(object sender, EventArgs e)
        {
            DataBindGrid();
        }

        /// <summary>
        /// 绑定数据源
        /// </summary>
        private void DataBindGrid()
        {
            string sqlStrwhere = "1=1";

            GenFindWhere(ref sqlStrwhere);
            string m_ConvertStationCode = Request["ConvertStationCode"] ?? "";

            if (m_ConvertStationCode!="")
            {
                sqlStrwhere += string.Format(" and ConvertStationCode='{0}'", m_ConvertStationCode);
            }

            int count = bll.GetRecordCount(sqlStrwhere);
            DataSet ds = bll.GetListByPage(sqlStrwhere, string.Empty, PageControl1.PageIndex, PageControl1.PageSize);
            ControlBindHelper.BindRepeaterList(ds.Tables[0], rp_Item);
            this.PageControl1.RecordCount = Convert.ToInt32(count);
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            ActionMode = eActionMode.Search;
            DataBindGrid();
        }

    
    }
}
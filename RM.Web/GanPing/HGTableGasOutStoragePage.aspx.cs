﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using RM.Common.DotNetCode;
using System.Data;
using RM.Common.DotNetUI;
using RM.Web.App_Code;
using EGMNGS.Common;
using EGMNGS.Model;
using RM.Common.DotNetFile;

namespace RM.Web.GanPing
{
    public partial class HGTableGasOutStoragePage : PageBase
    {
        #region 属性字段

        EGMNGS.BLL.TableGasOutStorage bll = new EGMNGS.BLL.TableGasOutStorage();
        EGMNGS.BLL.DetailsGasOutStorage DetailsBLL = new EGMNGS.BLL.DetailsGasOutStorage();


        public Dictionary<string, string> lsCFStatus
        {
            get
            {
                if (Session["GPStatus"] != null)
                {
                    return Session["GPStatus"] as Dictionary<string, string>;
                }
                return new Dictionary<string, string>();
            }

            set
            {
                Session["GPStatus"] = value;
            }
        }

        private DataTable UserList
        {
            get
            {
                return ViewState["UserList"] as DataTable;
            }
            set { ViewState["UserList"] = value; }
        }
        private EGMNGS.Model.TableGasOutStorage Model
        {
            get
            {
                if (Session["TableGasOutStorage"] == null)
                {
                    return new EGMNGS.Model.TableGasOutStorage();
                }
                return Session["TableGasOutStorage"] as EGMNGS.Model.TableGasOutStorage;
            }
            set { Session["TableGasOutStorage"] = value; }
        }
        private string actionMode
        {
            get { return ViewState["ActionMode"] as String; }
            set { ViewState["ActionMode"] = value; }
        }
        public string PowerSupplyCode
        {
            get { return ViewState["PowerSupplyCode"] as String; }
            set { ViewState["PowerSupplyCode"] = value; }
        }
        public string PowerSupplyName
        {
            get { return ViewState["PowerSupplyName"] as String; }
            set { ViewState["PowerSupplyName"] = value; }
        }

        public DataTable DetailsTable
        {
            get
            {
                if (ViewState["DetailsTable"] == null)
                {
                    return new DataTable();
                }
                return ViewState["DetailsTable"] as DataTable;
            }
            set { ViewState["DetailsTable"] = value; }
        }
        private string actionModeDetails
        {
            get
            {
                if (ViewState["actionModeDetails"] == null)
                {
                    return string.Empty;
                }
                return ViewState["actionModeDetails"] as String;
            }
            set { ViewState["actionModeDetails"] = value; }
        }

        private List<DetailsGasOutStorage> UpdateList
        {
            get
            {
                if (ViewState["UpdateList"] == null)
                {
                    return new List<DetailsGasOutStorage>();
                }
                return ViewState["UpdateList"] as List<DetailsGasOutStorage>;
            }
            set { ViewState["UpdateList"] = value; }
        }
        /// <summary>
        /// 需要添加的数据
        /// </summary>
        private List<DetailsGasOutStorage> AddList
        {
            get
            {
                if (ViewState["AddList"] == null)
                {
                    return new List<DetailsGasOutStorage>();
                }
                return ViewState["AddList"] as List<DetailsGasOutStorage>;
            }
            set { ViewState["AddList"] = value; }
        }
        #endregion

        #region 事件方法

        protected void Page_Load(object sender, EventArgs e)
        {
            this.PageControl1.pageHandler += new EventHandler(pager_PageChanged);

            if (!IsPostBack)
            {
                EGMNGS.BLL.SYS_CODE sysCodeBll = new EGMNGS.BLL.SYS_CODE();
                lsCFStatus = sysCodeBll.GetModelList("CODE_TYPE='GPStatus' ORDER BY DISPLAY_ORDER").ToDictionary(o => o.CODE, o => o.CODE_CHI_DESC);

                UserList = ComServies.GetAllUserInfo();
                string[] orgIdOrgName = ComServies.GetPowerApplyByUserID(User.UserId.ToString());
                PowerSupplyCode = orgIdOrgName[0];
                PowerSupplyName = orgIdOrgName[1];
                DropDownListBinder();

                GetDetailsOfRecycleGas();

            }
            //actionMode = "Edit";
            //InitControl();

            ScriptManager.GetCurrent(this.Page).RegisterPostBackControl(this.btnExport);
        }

        /// <summary>
        /// 绑定下拉列
        /// </summary>
        private void DropDownListBinder()
        {

            EGMNGS.BLL.SYS_CODE sysCodeBll = new EGMNGS.BLL.SYS_CODE();
            DataSet dsYear = sysCodeBll.GetList("CODE_TYPE='Year' ORDER BY DISPLAY_ORDER");

            this.ddlYear.DataSource = dsYear.Tables[0];
            this.ddlYear.DataTextField = "CODE_CHI_DESC";
            this.ddlYear.DataValueField = "CODE";
            this.ddlYear.DataBind();

            DataSet dsMonth = sysCodeBll.GetList("CODE_TYPE='Month' ORDER BY DISPLAY_ORDER");

            this.ddlMonth.DataSource = dsMonth.Tables[0];
            this.ddlMonth.DataTextField = "CODE_CHI_DESC";
            this.ddlMonth.DataValueField = "CODE";
            this.ddlMonth.DataBind();


            sysCodeBll = new EGMNGS.BLL.SYS_CODE();
            //DataSet dsGasType = sysCodeBll.GetList("CODE_TYPE='GasClass' ORDER BY DISPLAY_ORDER");

            //ddlGasType.DataSource = dsGasType.Tables[0];
            //ddlGasType.DataTextField = "CODE_CHI_DESC";
            //ddlGasType.DataValueField = "CODE";
            //ddlGasType.DataBind();
            //ddlGasType.SelectedValue = "3";


            EGMNGS.BLL.InfoPowerSupplyConvertStation bll = new EGMNGS.BLL.InfoPowerSupplyConvertStation();
            DataSet dsPowerSupplyCode = bll.GetList(string.Format("PowerSupplyCode='{0}'", PowerSupplyCode));
            this.ddlConvertStationName.DataSource = dsPowerSupplyCode.Tables[0];
            this.ddlConvertStationName.DataTextField = "ConvartStationName";
            this.ddlConvertStationName.DataValueField = "CovnertStationCode";
            this.ddlConvertStationName.DataBind();
            this.ddlConvertStationName.Items.Insert(0, string.Empty);

            DataSet dtCFStatus = sysCodeBll.GetList("CODE_TYPE='CFStatus' ORDER BY DISPLAY_ORDER");

            this.ddlStatus.DataSource = dtCFStatus.Tables[0];
            this.ddlStatus.DataTextField = "CODE_CHI_DESC";
            this.ddlStatus.DataValueField = "CODE";
            this.ddlStatus.DataBind();

            this.ddlStatus.Items.Insert(0, string.Empty);


        }

        private void LoadBusinessCode()
        {
            this.ddlBusinessCode.Items.Clear();
            EGMNGS.BLL.ApplNeedGasReg needBLL = new EGMNGS.BLL.ApplNeedGasReg();
            DataSet dsApplNeedGasReg = needBLL.GetList("Status='2' and AirDemand>(select isnull(SUM(t.AmountOutStorage),0) from TableGasOutStorage t where t.BusinessCode=GasCode)");
            //this.ddlBusinessCode.DataSource = dsApplNeedGasReg.Tables[0];
            //this.ddlBusinessCode.DataTextField = "GasCode";
            //this.ddlBusinessCode.DataValueField = "GasCode";
            //this.ddlBusinessCode.DataBind();

            foreach (DataRow item in dsApplNeedGasReg.Tables[0].Rows)
            {
                this.ddlBusinessCode.Items.Add(new ListItem(item["GasCode"].ToString(), item["GasCode"].ToString()));
            }
            this.ddlBusinessCode.Items.Insert(0, string.Empty);
        }

        /// <summary>
        /// 绑定后激发事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void rp_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
        }

        /// <summary>
        /// 绑定数据，分页
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void pager_PageChanged(object sender, EventArgs e)
        {
            DataBindGrid();
        }

        /// <summary>
        /// 绑定数据源
        /// </summary>
        private void DataBindGrid()
        {
            int count = bll.GetRecordCount("GasClass='3'");
            DataSet ds = bll.GetListByPage("GasClass='3'", "Code desc", PageControl1.PageIndex, PageControl1.PageSize);
            ControlBindHelper.BindRepeaterList(ds.Tables[0], rp_Item);
            this.PageControl1.RecordCount = Convert.ToInt32(count);
        }

        /// <summary>
        /// 绑定数据到对象
        /// </summary>
        private void DataBinder()
        {
            Model.Code = this.txtCode.Text;
            Model.Year = this.ddlYear.SelectedValue;
            Model.Month = this.ddlMonth.SelectedValue;
            Model.GasClass = "3";// this.ddlGasType.SelectedValue;
            if (this.txtAmountOutStorage.Text.Trim().Length > 0)
            {
                Model.AmountOutStorage = decimal.Parse(this.txtAmountOutStorage.Text);
            }

            Model.CountCyliner = Convert.ToInt32(this.txtCountCyliner.Text);
            Model.Address = this.txtAddress.Text;
            Model.Contacts = this.txtContacts.Text; ;
            Model.OfficeTel = this.txtOfficeTel.Text;
            Model.PhoneNum = this.txtPhoneNum.Text;
            Model.BusinessCode = this.ddlBusinessCode.SelectedValue;
            Model.ConvertStationCode = this.ddlConvertStationName.SelectedValue;
            Model.ConvertStationName = this.ddlConvertStationName.SelectedItem.Text;
            Model.CountDesc = this.txtCountDesc.Text;
        }

        /// <summary>
        /// 初始化控件
        /// </summary>
        private void InitControl()
        {
            this.txtCode.Text = Model.Code;
            this.ddlMonth.SelectedValue = Model.Month;
            this.ddlYear.SelectedValue = Model.Year;
            //  this.ddlGasType.SelectedValue = Model.GasClass;
            this.txtPowerSupplyName.Text = Model.PowerSupplyName;

            if (actionMode == "Edit")
            {

                this.ddlConvertStationName.Items.Clear();
                this.ddlConvertStationName.Items.Add(new ListItem(Model.ConvertStationName, Model.ConvertStationCode));


                this.ddlBusinessCode.Items.Clear();
                this.ddlBusinessCode.Items.Add(new ListItem(Model.BusinessCode, Model.BusinessCode));
            }

            this.txtAmountOutStorage.Text = Model.AmountOutStorage.ToString();
            this.txtCountCyliner.Text = Model.CountCyliner.ToString();
            this.txtAddress.Text = Model.Address;
            this.txtContacts.Text = Model.Contacts;
            this.txtOfficeTel.Text = Model.OfficeTel;
            this.txtPhoneNum.Text = Model.PhoneNum;
            this.ddlStatus.SelectedValue = Model.Status;

            this.txtRegistantOID.Text = GetUserName(Model.RegistantOID);
            this.txtRegistantDate.Text = Model.RegistantDate.ToShortDateString();


            var bc = this.ddlBusinessCode.Items.FindByValue(Model.BusinessCode);
            if (bc != null)
            {
                this.ddlBusinessCode.SelectedValue = Model.BusinessCode;
            }
            this.txtCountDesc.Text = Model.CountDesc;
        }

        /// <summary>
        /// 获取用户名
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        private string GetUserName(string userId)
        {
            DataRow[] dr = UserList.Select(string.Format("User_ID='{0}'", userId));
            if (dr.Length > 0)
            {
                return dr[0][1].ToString();
            }
            return string.Empty;
        }
        #endregion

        #region 按钮事件

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            if (Model.Status == "1")
            {
                ShowMsgHelper.Alert_Wern("无法重新提交！");
                return;
            }

            Model.Status = "1";
            if (ComServies.UpdateHGTableGasOutStoragePageStatus(Model.Code, Model.Status))
            {
                ShowMsgHelper.Alert("提交成功！");
                InitControl();
                DataBindGrid();
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            DataBindGrid();
        }

        private void ShowMaxMode()
        {
            Model = bll.GetModel(bll.GetMaxId() - 1);
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                if (actionMode == "Add")
                {
                    DataBinder();
                    Model.RegistantOID = User.UserId.ToString();

                    int count = bll.GetRecordCount(string.Format("Code='{0}'", Model.Code));
                    if (count > 0)
                    {
                        ShowMsgHelper.showWarningMsg("出库编号已经存在!");
                        return;
                    }

                    if (bll.Add(Model) > 0)
                    {
                        actionMode = string.Empty;
                    }

                    DataBindGrid();
                    ShowMaxMode();
                    GetDetailsOfRecycleGas();
                }
                else if (actionMode == "Edit")
                {
                    if (bll.GetModel(Model.OID).Status != "0")
                    {
                        ShowMsgHelper.Alert_Wern("“已确认”状态无法修改数据！");
                        return;
                    }

                    DataBinder();
                    if (bll.Update(Model))
                    {
                        EGMNSShowMsg.ShowEditMsgSuccess();
                        actionMode = string.Empty;
                    }
                    DataBindGrid();
                }
            }
            catch (ArgumentOutOfRangeException ex)
            {
                string[] errs = CommonHelper.GetValue(ex.Message, "@").Split(':');
                ShowMsgHelper.ExecuteScript(string.Format("ChangeCss($('#txt{0}'),'长度不能超{1}字')", errs[0], errs[1]));
            }
        }
        private void ClearControl()
        {
            Model = new TableGasOutStorage();
            InitControl();
        }
        protected void btnAdd_Click(object sender, EventArgs e)
        {
            if (ComServies.GetCheckStatus(DateTime.Now))
            {
                ShowMsgHelper.Alert_Wern("已对账");
                return;
            }

            actionMode = "Add";
            Model = new EGMNGS.Model.TableGasOutStorage(actionMode);
            Model.PowerSupplyCode = PowerSupplyCode;
            Model.PowerSupplyName = PowerSupplyName;
            Model.RegistantDate = DateTime.Now;
            Model.RegistantOID = User.UserId.ToString();
            Model.Status = "0";
            Model.GasClass = "3";
            DropDownListBinder();
            InitControl();
            LoadBusinessCode();
            GetDetailsOfRecycleGas();
        }


        protected void btnDel_Click(object sender, EventArgs e)
        {

            if (Model.OID == 0)
            {
                ShowMsgHelper.Alert_Wern("请选择！");
                return;
            }
            if (Model.Status != null && Model.Status != "0")
            {
                ShowMsgHelper.Alert_Wern("“已确认”状态不可以删除");
            }
            else
            {
                bll.Delete(Model.OID);
                Model = new EGMNGS.Model.TableGasOutStorage();
                InitControl();
                DataBindGrid();
                ShowMsgHelper.Alert("删除成功！");
            }
        }

        protected void rp_Item_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            int oid = Convert.ToInt32(e.CommandArgument);

            Model = bll.GetModel(oid);

            actionMode = "Edit";
            InitControl();
            //绑定明细表
            GetDetailsOfRecycleGas();
        }
        #endregion

        #region Details
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void pager_PageChangedDetails(object sender, EventArgs e)
        {
            GetDetailsOfRecycleGas();
        }

        private void GetDetailsOfRecycleGas()
        {
            DetailsTable = DetailsBLL.GetList(string.Format("TableGasOutStorage_Code='{0}' order by oid desc", Model.Code)).Tables[0];

            int count = DetailsBLL.GetRecordCount(string.Format("TableGasOutStorage_Code='{0}'", Model.Code));
            ControlBindHelper.BindRepeaterList(DetailsTable, rptDetails);
        }
        protected void btnAddDetails_Click(object sender, EventArgs e)
        {
            if (Model.OID == 0)
            {
                ShowMsgHelper.Alert_Wern("请选择出库单!");
                return;
            }
            //首先，恢复数据源
            RetriRepeter();
            DataTable dt = DetailsTable;
            if (LoadTestData()) return;

            DataRow row = dt.NewRow();
            row[2] = string.Empty;
            row[3] = string.Empty;
            row[4] = string.Empty;
            row[5] = 0.0;
            row["TableGasOutStorage_Code"] = Model.Code;
            dt.Rows.Add(row);

            rptDetails.DataSource = dt;
            rptDetails.DataBind();

            actionModeDetails = "Add";
        }
        /// <summary> 
        /// 从库表获取数据
        /// </summary>
        private bool LoadTestData()
        {

            if (DetailsTable.Rows.Count > 0)
            {
                string gpCode = DetailsTable.Rows[DetailsTable.Rows.Count - 1]["CylinderCode"].ToString();

                var obj = new EGMNGS.BLL.DetailsGasStorage().GetModelList(string.Format(@"CylinderCode='{0}'
	                                                                      AND FillStatus='2'", gpCode));
                if (obj.Count == 0)
                {
                    ShowMsgHelper.Alert_Error(string.Format("{0}此钢瓶不在库中", DetailsTable.Rows[DetailsTable.Rows.Count - 1]["CylinderCode"].ToString()));
                    return true;
                }

                if (DetailsBLL.GetRecordCount(string.Format(@"GasCode='{0}' and exists(select * from TableGasOutStorage t 
where TableGasOutStorage_Code=t.Code and t.GasClass='3') ", obj[0].GasCode)) >= 1 && Convert.IsDBNull(DetailsTable.Rows[DetailsTable.Rows.Count - 1]["OID"]))
                {
                    ShowMsgHelper.Alert_Error(string.Format("{0}此钢瓶已经出库!", DetailsTable.Rows[DetailsTable.Rows.Count - 1]["CylinderCode"].ToString()));
                    return true;
                }

                DetailsTable.Rows[DetailsTable.Rows.Count - 1]["AmountGas"] = Convert.ToDecimal(obj[0].AmountGas);
                DetailsTable.Rows[DetailsTable.Rows.Count - 1]["CylinderStatus"] = obj[0].CylinderStatus;
                DetailsTable.Rows[DetailsTable.Rows.Count - 1]["GasCode"] = obj[0].GasCode;
                DetailsTable.Rows[DetailsTable.Rows.Count - 1]["IsPass"] = obj[0].IsPass;
            }

            return false;
        }
        private void RetriRepeter()
        {
            for (int i = 0; i < this.rptDetails.Items.Count; i++)
            {
                Label txtAmountGas = this.rptDetails.Items[i].FindControl("lblAmountGas") as Label;
                DetailsTable.Rows[i]["AmountGas"] = Convert.ToDecimal(txtAmountGas.Text);
                TextBox txtCylinderCode = this.rptDetails.Items[i].FindControl("txtCylinderCode") as TextBox;
                DetailsTable.Rows[i]["CylinderCode"] = txtCylinderCode.Text;
            }
        }

        protected void btnDelDetails_Click(object sender, EventArgs e)
        {
            try
            {
                if (Model.Status.Length > 0 && Model.Status != "0")
                {
                    ShowMsgHelper.Alert("“已确认”状态不可以删除");
                }

                string strID = HiddenField1.Value;
                if (strID.Trim().Length == 0)
                {
                    return;
                }
                DetailsBLL.DeleteList(strID);
                ShowMsgHelper.Alert("删除成功！");
                GetDetailsOfRecycleGas();
            }
            catch (Exception)
            {
                ShowMsgHelper.Alert("删除失败！");
                throw;
            }
        }

        protected void btnSaveDetails_Click(object sender, EventArgs e)
        {
            try
            {
                if (Model.Status.Length > 0 && Model.Status != "0")
                {
                    ShowMsgHelper.Alert_Wern("“已确认”状态不可以删除");
                }

                if (IsCheckOverAmountGas() == false)
                {
                    ShowMsgHelper.Alert_Wern("出库量已经超出！");
                    return;
                }
                GetCheckBox();

                if (actionModeDetails.Equals("Add"))
                {
                    if (AddList.Count > 0)
                    {
                        //批量添加
                        foreach (var item in AddList)
                        {
                            if (DetailsBLL.GetRecordCount(string.Format(@"GasCode='{0}' and exists(select * from TableGasOutStorage t 
where TableGasOutStorage_Code=t.Code and t.GasClass='3') ", item.GasCode)) >= 1)
                            {
                                continue;
                            }

                            DetailsBLL.Add(item);
                        }

                        AddList.Clear();
                    }
                    ShowMsgHelper.Alert("添加成功！");
                    actionModeDetails = string.Empty;

                }

                if (UpdateList.Count > 0)
                {
                    //批量修改
                    foreach (var item in UpdateList)
                    {
                        DetailsBLL.Update(item);
                    }
                    UpdateList.Clear();
                }

                GetDetailsOfRecycleGas();
            }
            catch (Exception ex)
            {
                ShowMsgHelper.AlertMsg("添加失败！" + ex.Message);
                throw;
            }
        }

        protected void rptDetails_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Label lblIsPass = e.Item.FindControl("lblIsPass") as Label;
                if (lblIsPass != null)
                {
                    if (lblIsPass.Text.Length > 0)
                    {
                        if (lblIsPass.Text == "0")
                        {
                            lblIsPass.Text = "不合格";
                        }
                        else if (lblIsPass.Text == "1")
                        {
                            lblIsPass.Text = "合格";
                        }

                    }
                }

                //Label lblCylinderStatus = e.Item.FindControl("lblCylinderStatus") as Label;
                //if (lblCylinderStatus != null)
                //{
                //    if (lblCylinderStatus.Text.Length > 0)
                //    {
                //        lblCylinderStatus.Text = lsCFStatus[lblCylinderStatus.Text];
                //    }
                //}

                CheckBox cb = e.Item.FindControl("checkbox") as CheckBox;
                if (cb.ToolTip.Length == 0)
                {
                    cb.Checked = true;
                }
            }
        }


        /// <summary>
        /// 
        /// </summary>
        private void GetCheckBox()
        {
            UpdateList = new List<DetailsGasOutStorage>();
            AddList = new List<DetailsGasOutStorage>();

            for (int i = 0; i < this.rptDetails.Items.Count; i++)
            {
                System.Web.UI.WebControls.CheckBox checkbox = (System.Web.UI.WebControls.CheckBox)rptDetails.Items[i].FindControl("checkbox");
                TextBox txtCylinderCode = this.rptDetails.Items[i].FindControl("txtCylinderCode") as TextBox;
                if (txtCylinderCode.Text.Trim().Length == 0)
                {
                    continue;
                }

                if (checkbox.Checked == true && checkbox.ToolTip.Length > 0)
                {

                    Label txtGasCode = this.rptDetails.Items[i].FindControl("lblGasCode") as Label;
                    Label txtAmountGas = this.rptDetails.Items[i].FindControl("lblAmountGas") as Label;

                    DetailsGasOutStorage DetailsOfRecycleGasObj = new EGMNGS.BLL.DetailsGasOutStorage().GetModel(int.Parse(checkbox.ToolTip));
                    // DetailsOfRecycleGasObj.TableGasOutStorage_Code = Model.Code;
                    // DetailsOfRecycleGasObj.OID = int.Parse(checkbox.ToolTip);
                    DetailsOfRecycleGasObj.CylinderCode = txtCylinderCode.Text;
                    if (txtAmountGas.Text.Trim().Length > 0)
                    {
                        DetailsOfRecycleGasObj.AmountGas = decimal.Parse(txtAmountGas.Text);
                    }
                    if (txtGasCode.Text.Trim().Length > 0)
                    {
                        DetailsOfRecycleGasObj.GasCode = txtGasCode.Text;
                    }
                    UpdateList.Add(DetailsOfRecycleGasObj);
                }
                else if (checkbox.Checked == true && checkbox.ToolTip.Length == 0)
                {

                    Label txtGasCode = this.rptDetails.Items[i].FindControl("lblGasCode") as Label;
                    Label txtAmountGas = this.rptDetails.Items[i].FindControl("lblAmountGas") as Label;

                    DetailsGasOutStorage DetailsOfRecycleGasObj = new EGMNGS.Model.DetailsGasOutStorage();
                    DetailsOfRecycleGasObj.TableGasOutStorage_Code = Model.Code;

                    DetailsOfRecycleGasObj.CylinderCode = txtCylinderCode.Text;
                    if (txtAmountGas.Text.Trim().Length > 0)
                    {
                        DetailsOfRecycleGasObj.AmountGas = decimal.Parse(txtAmountGas.Text);
                    }
                    if (txtGasCode.Text.Trim().Length > 0)
                    {
                        DetailsOfRecycleGasObj.GasCode = txtGasCode.Text;
                    }

                    AddList.Add(DetailsOfRecycleGasObj);
                }

            }
        }

        private bool IsCheckOverAmountGas()
        {
            decimal sumAmountGas = 0;

            for (int i = 0; i < this.rptDetails.Items.Count; i++)
            {
                Label txtAmountGas = this.rptDetails.Items[i].FindControl("lblAmountGas") as Label;
                sumAmountGas += decimal.Parse(txtAmountGas.Text);
            }

            if (sumAmountGas > Model.AmountOutStorage)
            {
                return false;
            }
            else
            {
                return true;

            }
        }
        #endregion

        protected void ddlBusinessCode_SelectedIndexChanged(object sender, EventArgs e)
        {
            var objAppNeed = new EGMNGS.BLL.ApplNeedGasReg().GetModelList(string.Format(@"GasCode='{0}'", this.ddlBusinessCode.SelectedValue));
            if (objAppNeed.Count > 0)
            {
                this.txtPowerSupplyName.Text = objAppNeed[0].PowerSupplyName;
                Model.PowerSupplyCode = objAppNeed[0].PowerSupplyCode;
                Model.PowerSupplyName = objAppNeed[0].PowerSupplyName;
                this.ddlConvertStationName.Items.Clear();
                this.ddlConvertStationName.Items.Add(new ListItem(objAppNeed[0].ConvertStationName, objAppNeed[0].ConvertStationCode));
                this.txtAmountOutStorage.Text = Convert.ToString(objAppNeed[0].AirDemand);
                this.txtContacts.Text = objAppNeed[0].Contacts;
                this.txtPhoneNum.Text = objAppNeed[0].PhoneNum;
                this.txtOfficeTel.Text = objAppNeed[0].OfficeTel;
                this.txtAddress.Text = objAppNeed[0].ReceiptAddress;
                this.txtCountDesc.Text = objAppNeed[0].QtyDesc;
            }
        }


        /// <summary>
        /// 导出
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnExport_Click(object sender, EventArgs e)
        {
            if (Model == null)
            {
                ShowMsgHelper.Alert_Error("请选择一个检测任务！");
                return;
            }
            MouldExportExcel mee = new MouldExportExcel();
            try
            {
                string physicalPath = Request.PhysicalApplicationPath + "Xls\\";
                Microsoft.Office.Interop.Excel.Workbook sh = mee.OpenExcel(physicalPath + "合格气体出库.xls");
                Microsoft.Office.Interop.Excel.Worksheet ws = sh.Worksheets[1] as Microsoft.Office.Interop.Excel.Worksheet;
                // mee.SetCellValue(ws, 4, 3, "1111");
                #region 设置值
                mee.SetCellValue(ws, 2, 2, Model.Code);
                mee.SetCellValue(ws, 3, 2, Model.BusinessCode);

                mee.SetCellValue(ws, 4, 2, Model.Contacts);


                mee.SetCellValue(ws, 5, 2, Model.Address);
                mee.SetCellValue(ws, 6, 2, Convert.ToString(Model.AmountOutStorage));
                mee.SetCellValue(ws, 6, 4, Convert.ToString(Model.CountCyliner));

                mee.SetCellValue(ws, 2, 4, GetUserName(Model.RegistantOID));
                mee.SetCellValue(ws, 3, 4, Model.PowerSupplyName);
                mee.SetCellValue(ws, 4, 4, Model.PhoneNum);

                mee.SetCellValue(ws, 2, 6, Model.RegistantDate.ToShortDateString());
                mee.SetCellValue(ws, 3, 6, Model.ConvertStationName);

                mee.SetCellValue(ws, 4, 6, Model.OfficeTel);
                mee.SetCellValue(ws, 5, 6, GetUserName(Model.UsingOID));

                // Model.CountCyliner

                for (int i = 0; i < DetailsTable.Rows.Count; i++)
                {
                    DataRow dr = DetailsTable.Rows[i];
                    mee.SetCellValue(ws, i + 9, 1, i + 1);
                    mee.SetCellValue(ws, i + 9, 2, Convert.ToString(dr["CylinderCode"]));
                    mee.SetCellValue(ws, i + 9, 3, Convert.ToString(dr["GasCode"]));
                    mee.SetCellValue(ws, i + 9, 4, Convert.ToDecimal(dr["AmountGas"]).ToString());
                    mee.SetCellValue(ws, i + 9, 5, "电科院");// Convert.ToString(dr["Manufacturer"]));
                    mee.SetCellValue(ws, i + 9, 6, Convert.ToString(dr["ReportCode"]));
                }


                #endregion


                string fileName = "合格气体出库temp.xls";
                string pathFile = physicalPath + fileName;
                mee.SaveExcel(pathFile);
                ShowMsgHelper.Alert("导出成功!");

                FileDownHelper.DownLoadold(@"~/Xls/" + fileName);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                mee.Dispose();

            }

        }

    }
}
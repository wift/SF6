﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using RM.Common.DotNetCode;
using System.Data;
using RM.Common.DotNetUI;
using RM.Web.App_Code;
using EGMNGS.Common;

using EGMNGS.Model;
using System.Globalization;
using RM.Common.DotNetFile;
using System.Configuration;

namespace RM.Web.GanPing
{

    public partial class ApplRegGasQtyInspectionPage : PageBase
    {
        public class checkObject
        {
            public string GPCode { set; get; }
            public int GPStatus { set; get; }
        }

        EGMNGS.BLL.ApplRegGasQtyInspectionValue bllApplRegGasQtyInspectionValue = new EGMNGS.BLL.ApplRegGasQtyInspectionValue();

        #region 属性字段

        public List<checkObject> CheckObjectList
        {
            set { Application["CheckObjectList"] = value; }
            get
            {
                if (Application["CheckObjectList"] == null)
                {
                    Application["CheckObjectList"] = new List<checkObject>();
                }

                return Application["CheckObjectList"] as List<checkObject>;
            }
        }
        EGMNGS.BLL.ApplRegGasQtyInspection bll = new EGMNGS.BLL.ApplRegGasQtyInspection();
        private DataTable UserList
        {
            get
            {
                return ViewState["UserList"] as DataTable;
            }
            set { ViewState["UserList"] = value; }
        }
        private EGMNGS.Model.ApplRegGasQtyInspection Model
        {
            get
            {
                if (Session["ApplRegGasQtyInspection"] == null)
                {
                    return new EGMNGS.Model.ApplRegGasQtyInspection();
                }
                return Session["ApplRegGasQtyInspection"] as EGMNGS.Model.ApplRegGasQtyInspection;
            }
            set { Session["ApplRegGasQtyInspection"] = value; }
        }
        private string actionMode
        {
            get { return ViewState["ActionMode"] as String; }
            set { ViewState["ActionMode"] = value; }
        }
        public string PowerSupplyCode
        {
            get { return ViewState["PowerSupplyCode"] as String; }
            set { ViewState["PowerSupplyCode"] = value; }
        }
        public string PowerSupplyName
        {
            get { return ViewState["PowerSupplyName"] as String; }
            set { ViewState["PowerSupplyName"] = value; }
        }

        public List<string> listRole
        {
            get { return Session["listRole"] as List<string>; }
            set { Session["listRole"] = value; }
        }

        public Dictionary<string, string> FlowStatus
        {
            get
            {
                if (ViewState["FlowStatus"] == null)
                {
                    Dictionary<string, string> flowStatus = new Dictionary<string, string>();
                    EGMNGS.BLL.SYS_CODE sysCodeBll = new EGMNGS.BLL.SYS_CODE();
                    DataSet dsFlowStatus = sysCodeBll.GetList("CODE_TYPE='FlowStatus' ORDER BY DISPLAY_ORDER");
                    foreach (DataRow item in dsFlowStatus.Tables[0].Rows)
                    {
                        flowStatus.Add(item["CODE"].ToString(), item["CODE_CHI_DESC"].ToString());
                    }
                    ViewState["FlowStatus"] = flowStatus;
                }

                return ViewState["FlowStatus"] as Dictionary<string, string>;
            }
            set { ViewState["FlowStatus"] = value; }
        }
        #endregion

        #region 事件方法

        protected void Page_Load(object sender, EventArgs e)
        {
            this.PageControl1.pageHandler += new EventHandler(pager_PageChanged);

            if (!IsPostBack)
            {
                UserList = ComServies.GetAllUserInfo();
                DropDownListBinder();
                listRole = ComServies.GetRoleName(User.UserId.ToString());


                IList<ApplRegGasQtyInspectionValue> lt = bllApplRegGasQtyInspectionValue.GetModelList("1=1");
                if (lt.Count > 0)
                {
                    //绑定管道数据到页面
                    ShowDataToControl(lt[0]);
                }

            }

        }
        private void ShowDataToControl(ApplRegGasQtyInspectionValue model)
        {

            this.txtPipe1.Value = model.Pipe1;
            this.txtPipe2.Value = model.Pipe2;
            this.txtPipe3.Value = model.Pipe3;
            this.txtPipe4.Value = model.Pipe4;
            this.txtPipe5.Value = model.Pipe5;
            this.txtPipe6.Value = model.Pipe6;
            this.txtPipe7.Value = model.Pipe7;
            this.txtPipe8.Value = model.Pipe8;


            if (CheckObjectList.Exists(k => k.GPCode == txtPipe1.Value && k.GPStatus == 1))//不合格
            {
                txtPipe1.Style.Add("color", "red");
            }
            else if (CheckObjectList.Exists(k => k.GPCode == txtPipe1.Value && k.GPStatus == 2))//合格
            {
                txtPipe1.Style.Add("color", "blue");
            }

            if (CheckObjectList.Exists(k => k.GPCode == txtPipe2.Value && k.GPStatus == 1))//不合格
            {
                txtPipe2.Style.Add("color", "red");
            }
            else if (CheckObjectList.Exists(k => k.GPCode == txtPipe2.Value && k.GPStatus == 2))//合格
            {
                txtPipe2.Style.Add("color", "blue");
            }

            if (CheckObjectList.Exists(k => k.GPCode == txtPipe3.Value && k.GPStatus == 1))//不合格
            {
                txtPipe3.Style.Add("color", "red");
            }
            else if (CheckObjectList.Exists(k => k.GPCode == txtPipe3.Value && k.GPStatus == 2))//合格
            {
                txtPipe3.Style.Add("color", "blue");
            }

            if (CheckObjectList.Exists(k => k.GPCode == txtPipe4.Value && k.GPStatus == 1))//不合格
            {
                txtPipe4.Style.Add("color", "red");
            }
            else if (CheckObjectList.Exists(k => k.GPCode == txtPipe4.Value && k.GPStatus == 2))//合格
            {
                txtPipe4.Style.Add("color", "blue");
            }


            if (CheckObjectList.Exists(k => k.GPCode == txtPipe5.Value && k.GPStatus == 1))//不合格
            {
                txtPipe5.Style.Add("color", "red");
            }
            else if (CheckObjectList.Exists(k => k.GPCode == txtPipe5.Value && k.GPStatus == 2))//合格
            {
                txtPipe5.Style.Add("color", "blue");
            }


            if (CheckObjectList.Exists(k => k.GPCode == txtPipe6.Value && k.GPStatus == 1))//不合格
            {
                txtPipe6.Style.Add("color", "red");
            }
            else if (CheckObjectList.Exists(k => k.GPCode == txtPipe6.Value && k.GPStatus == 2))//合格
            {
                txtPipe6.Style.Add("color", "blue");
            }

            if (CheckObjectList.Exists(k => k.GPCode == txtPipe7.Value && k.GPStatus == 1))//不合格
            {
                txtPipe7.Style.Add("color", "red");
            }
            else if (CheckObjectList.Exists(k => k.GPCode == txtPipe7.Value && k.GPStatus == 2))//合格
            {
                txtPipe7.Style.Add("color", "blue");
            }

            if (CheckObjectList.Exists(k => k.GPCode == txtPipe8.Value && k.GPStatus == 1))//不合格
            {
                txtPipe8.Style.Add("color", "red");
            }
            else if (CheckObjectList.Exists(k => k.GPCode == txtPipe8.Value && k.GPStatus == 2))//合格
            {
                txtPipe8.Style.Add("color", "blue");
            }

        }


        /// <summary>
        /// 绑定下拉列
        /// </summary>
        private void DropDownListBinder()
        {



        }


        /// <summary>
        /// 绑定后激发事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void rp_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Label lblClearManoid = e.Item.FindControl("lblApplOID") as Label;
                if (lblClearManoid != null)
                {
                    lblClearManoid.Text = GetUserName(lblClearManoid.Text);
                }
                Label lblGasSourse = e.Item.FindControl("lblGasSourse") as Label;
                if (lblGasSourse != null && lblGasSourse.Text.Length > 0)
                {
                    lblGasSourse.Text = DictEGMNS["GasSourse"][lblGasSourse.Text ?? string.Empty];

                }

                Label lblIsPass = e.Item.FindControl("lblIsPass") as Label;
                if (lblIsPass != null)
                {
                    if (lblIsPass.Text.Trim().Length > 0)
                    {

                    }
                    lblIsPass.Text = DictEGMNS["IsPass"][lblIsPass.Text];

                }

            }
        }

        /// <summary>
        /// 绑定数据，分页
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void pager_PageChanged(object sender, EventArgs e)
        {
            DataBindGrid();
        }

        /// <summary>
        /// 绑定数据源
        /// </summary>
        private void DataBindGrid()
        {
            EGMNGS.BLL.ApplRegGasQtyInspectionValue blll = new EGMNGS.BLL.ApplRegGasQtyInspectionValue();
            DataSet dss = blll.GetAllList();
            List<string> ls = new List<string>();
            foreach (DataColumn item in dss.Tables[0].Columns)
            {

                if (item.ColumnName.Contains("Pipe"))
                {
                    var tempV = dss.Tables[0].Rows[0][item.ColumnName];
                    if (!Convert.IsDBNull(tempV) && tempV.ToString().Length > 0)
                    {
                        ls.Add(string.Format("'{0}'", tempV.ToString()));
                    }
                }
            }

            string pupleStringorderby = string.Empty;
            if (ls.Count > 0)
            {

                // pupleStringorderby = " order by case when CylinderCode in({0}) then '' end desc ";
                for (int i = 0; i < ls.Count; i++)
                {
                    if (i == ls.Count - 1)
                    {
                        pupleStringorderby += string.Format(" then '{0}'", ls[i]);
                    }
                    else
                    {
                        pupleStringorderby += string.Format(" when '{0}'", ls[i]);
                    }
                }

                // pupleStringorderby = string.Format(" order by case when CylinderCode in({0}) then '' end desc ", string.Join(",", ls.ToArray()));
                pupleStringorderby = string.Format("CylinderCode in({0}) then '' end desc ", string.Join(",", ls.ToArray()));

            }

            bool b = listRole.Contains("检测用户");
            string sql = string.Empty;
            if (Session["Select"] != null)
            {
                sql = Session["Select"].ToString();
            }
            else
            {
                sql = "1=1";
            }

            if (b)
            {

                sql += " and " + "FlowStatus='1'"; //+ pupleStringorderby;
                int count = bll.GetRecordCount(sql);
                DataSet ds = bll.GetListByPage(sql, pupleStringorderby, PageControl1.PageIndex, PageControl1.PageSize);
                ControlBindHelper.BindRepeaterList(ds.Tables[0], rp_Item);
                this.PageControl1.RecordCount = count;

                return;
            }

            b = listRole.Contains("主管用户");

            if (b)
            {
                sql += " and " + "FlowStatus='2'";// +pupleStringorderby;
                int count = bll.GetRecordCount(sql);
                DataSet ds = bll.GetListByPage(sql, pupleStringorderby, PageControl1.PageIndex, PageControl1.PageSize);
                ControlBindHelper.BindRepeaterList(ds.Tables[0], rp_Item);
                this.PageControl1.RecordCount = count;
                return;
            }


            b = listRole.Contains("领导用户");

            if (b)
            {
                sql += " and " + "FlowStatus='3'";// +pupleStringorderby;
                int count = bll.GetRecordCount(sql);
                DataSet ds = bll.GetListByPage(sql, pupleStringorderby, PageControl1.PageIndex, PageControl1.PageSize);
                ControlBindHelper.BindRepeaterList(ds.Tables[0], rp_Item);
                this.PageControl1.RecordCount = count;

            }
        }
        /// <summary>
        /// 绑定数据到对象
        /// </summary>
        private void DataBinder()
        {

        }

        /// <summary>
        /// 初始化控件
        /// </summary>
        private void InitControl()
        {

            this.txtBusinessCode.Value = Model.BusinessCode;
            //this.txtGasCdoe.Value = Model.GasCdoe;
            this.txtCylinderCode.Value = Model.CylinderCode;

        }

        /// <summary>
        /// 获取用户名
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        private string GetUserName(string userId)
        {
            DataRow[] dr = UserList.Select(string.Format("User_ID='{0}'", userId));
            if (dr.Length > 0)
            {
                return dr[0][1].ToString();
            }
            return string.Empty;
        }
        private void ClearControl()
        {
            Model = new EGMNGS.Model.ApplRegGasQtyInspection();
            InitControl();
        }

        #endregion

        #region 按钮事件

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                if (listRole.Contains("检测用户"))
                {
                    if (Model.FlowStatus == "2")
                    {
                        ShowMsgHelper.Alert("已检测！");
                        return;
                    }
                    DataBinder();
                    Model.FlowStatus = "2";//已检测


                    bool b = ComServies.UpdateDetailsByCylinderCode(Model.GasCdoe, Model.IsPass, Model.CheckDate.Value.ToShortDateString(), Model.GasSourse, Model.OID.ToString(), Model.FlowStatus, Model.CheckOID);
                    if (b)
                    {
                        ShowMsgHelper.Alert("提交成功！");
                    }
                    else
                    {
                        ShowMsgHelper.Alert("修改从表失败！");
                    }
                }
                else if (listRole.Contains("主管用户"))
                {
                    if (Model.FlowStatus == "3")
                    {
                        ShowMsgHelper.Alert("已签发！");
                        return;
                    }
                    Model.FlowStatus = "3";//已签发
                    Model.SignerDate = DateTime.Now;
                    Model.SignerOID = User.UserId.ToString();


                }
                else if (listRole.Contains("领导用户"))
                {
                    if (Model.FlowStatus == "4")
                    {
                        ShowMsgHelper.Alert("已批准！");
                        return;
                    }

                    Model.FlowStatus = "4";//已批准
                    Model.ApproverDate = DateTime.Now;
                    Model.ApproverOID = User.UserId.ToString();

                }

                if (bll.Update(Model))
                {
                    ShowMsgHelper.Alert("提交成功！");
                }
                else
                {
                    ShowMsgHelper.Alert("修改从表失败！");
                }

            }
            catch (Exception ex)
            {
                ShowMsgHelper.Alert("提交失败！");
            }

            DataBindGrid();
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            DataBindGrid();
        }

        protected void btnReject_Click(object sender, EventArgs e)
        {
            Model.FlowStatus = "1";
            if (bll.Update(Model))
            {
                ShowMsgHelper.Alert("成功打回！");
                DataBindGrid();
            }
            else
            {
                ShowMsgHelper.Alert("打回失败！");
            }
        }
        /// <summary>
        /// 获取当前对象
        /// </summary>
        private void ShowMaxMode()
        {
            Model = bll.GetModel(bll.GetMaxId() - 1);
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {


                if (actionMode == "Add")
                {
                    DataBinder();
                    int count = bll.GetRecordCount(string.Format("CheckCode='{0}'", Model.CheckCode));
                    if (count > 0)
                    {
                        ShowMsgHelper.showWarningMsg("检测编号已经存在!");
                        return;
                    }

                    if (bll.Add(Model) > 0)
                    {
                        actionMode = string.Empty;
                    }

                    DataBindGrid();
                    ShowMaxMode();
                }
                else if (actionMode == "Edit")
                {
                    DataBinder();

                    var b = listRole.Find(o => o.Contains("检测"));

                    if (b != null && b.Length > 0)
                    {
                        Model.CheckDate = DateTime.Now;
                        Model.CheckOID = User.UserId.ToString();
                    }

                    if (bll.Update(Model))
                    {
                        EGMNSShowMsg.ShowEditMsgSuccess();
                        actionMode = string.Empty;
                    }
                    DataBindGrid();
                }
            }
            catch (ArgumentOutOfRangeException ex)
            {
                string[] errs = CommonHelper.GetValue(ex.Message, "@").Split(':');
                ShowMsgHelper.ExecuteScript(string.Format("ChangeCss($('#txt{0}'),'长度不能超{1}字')", errs[0], errs[1]));
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            actionMode = "Add";
            Model = new EGMNGS.Model.ApplRegGasQtyInspection(actionMode);
            Model.AppLDate = DateTime.Now;
            Model.ApplOID = User.UserId.ToString();
            InitControl();
        }


        protected void btnDel_Click(object sender, EventArgs e)
        {
            bll.Delete(Model.OID);
            Model = new EGMNGS.Model.ApplRegGasQtyInspection();
            InitControl();
            DataBindGrid();
        }

        protected void rp_Item_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if ("del".Equals(e.CommandName))
            {
                int oid = Convert.ToInt32(e.CommandArgument);

                Model = bll.GetModel(oid);
                InitControl();
                actionMode = "Edit";
            }
        }

        /// <summary>
        /// 导出
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnExport_Click(object sender, EventArgs e)
        {
            string strID = HiddenField1.Value;


            if (strID.Trim().Length == 0)
            {
                return;

            }

            ReportExport.ComUsingByThreeExportByID(strID);


        }

        #endregion

        protected void btnInNew_Click(object sender, EventArgs e)
        {
            Session["Select"] = "[GasSourse]='2'";
            DataBindGrid();

        }

        protected void btnReJHGas_Click(object sender, EventArgs e)
        {
            Session["Select"] = "[GasSourse]='3'";
            DataBindGrid();
        }

        protected void btnNewBuy_Click(object sender, EventArgs e)
        {
            Session["Select"] = "[GasSourse]='1'";
            DataBindGrid();
        }

        /// <summary>
        /// 合格证打印
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnPassPrint_Click(object sender, EventArgs e)
        {

            string strID = HiddenField1.Value;
            if (strID.Trim().Length == 0)
            {
                return;
            }

            var modelList = bll.GetModelList(string.Format(" OID in({0})", strID));



            string barCodePath = ConfigurationManager.AppSettings["PassBarCodePath"];
            string passPrinterName = ConfigurationManager.AppSettings["PassPrinterName"];
            //Declare a BarTender application variable 

            BarTender.Application btApp;

            //Declare a BarTender format variable 

            BarTender.Format btFormat;

            //Instantiate a BarTender application variable 

            btApp = new BarTender.Application();

            //Set the BarTender application visible 

            btApp.Visible = true;

            //Open a BarTender label format 

            btFormat = btApp.Formats.Open(barCodePath, false, passPrinterName);

            foreach (var item in modelList)
            {

                BookCylinderInfo bookCylinderInfoOBJ = new EGMNGS.BLL.BookCylinderInfo().GetModelByCylinderCode(item.CylinderCode);

                btFormat.SetNamedSubStringValue("钢印号", bookCylinderInfoOBJ.CylinderSealNo);
                btFormat.SetNamedSubStringValue("出厂编号", bookCylinderInfoOBJ.CurGasCode);

                string GPRL = string.Format("{0}KG", DictEGMNS["GPRL"][bookCylinderInfoOBJ.CylinderCapacity]);

                string suttle = new EGMNGS.BLL.BookGasFill().GetModeByWhere(string.Format("GasCode='{0}'", bookCylinderInfoOBJ.CurGasCode)).AmountGas.ToString();
                btFormat.SetNamedSubStringValue("净重", suttle);
                btFormat.SetNamedSubStringValue("瓶重", GPRL);


                //小数第4位为0时(＜0.0001)，否则保留4位[if N<0.0001,'＜0.0001',保留4位小数]
                if (Convert.ToDouble(item.CF4Result) < 0.0001)
                {
                    btFormat.SetNamedSubStringValue("四氟化碳", "＜1");
                }
                else
                {
                    btFormat.SetNamedSubStringValue("四氟化碳", Convert.ToDecimal(item.CF4Result * 10000).ToString("F2"));
                }


                if (item.SF6Result >= (decimal)99.99)
                {

                    btFormat.SetNamedSubStringValue("六氟化硫", ">99.99");
                }
                else
                {
                    btFormat.SetNamedSubStringValue("六氟化硫", Convert.ToDecimal(item.SF6Result).ToString("F2"));
                }

                if (Convert.ToDouble(item.WaterQualityScoreResult) < 0.0001)
                {

                    btFormat.SetNamedSubStringValue("水分", "＜1");
                }
                else
                {
                    btFormat.SetNamedSubStringValue("水分", Convert.ToDecimal(item.WaterQualityScoreResult * 10000).ToString("F5"));
                }

                btFormat.SetNamedSubStringValue("空气", Convert.ToDecimal(item.AirQualityScoreResult * 10000).ToString("F2"));

                if (Convert.ToDouble(item.KSJHFResult) < 0.000002)
                {
                    btFormat.SetNamedSubStringValue("可水解氟化物", "＜0.02");

                }
                else if (Convert.ToDouble(item.KSJHFResult) < 0.00001)
                {
                    btFormat.SetNamedSubStringValue("可水解氟化物", Convert.ToDecimal(item.KSJHFResult * 10000).ToString("F2"));

                }
                else
                {
                    btFormat.SetNamedSubStringValue("可水解氟化物", Convert.ToDecimal(item.KSJHFResult * 10000).ToString("F2"));

                }


                if (Convert.ToDouble(item.HFResult) < 0.000002)
                {

                    btFormat.SetNamedSubStringValue("酸度", "＜0.02");
                }
                else if (Convert.ToDouble(item.HFResult) < 0.00001)
                {

                    btFormat.SetNamedSubStringValue("酸度", Convert.ToDecimal(item.HFResult * 10000).ToString("F2"));
                }
                else
                {
                    btFormat.SetNamedSubStringValue("酸度", Convert.ToDecimal(item.HFResult * 10000).ToString("F2"));
                }


                if (item.GasSourse == "3")
                {
                    var listRGB = new EGMNGS.BLL.RegGasBatch().GetModelList(string.Format("BatchCode='{0}'", item.BusinessCode));
                    if (listRGB.Count > 0)
                    {
                        btFormat.SetNamedSubStringValue("生产日期", listRGB[0].RegistantsDate.Value.ToString("yyyy-MM-dd", DateTimeFormatInfo.InvariantInfo));
                    }
                }
                else
                {
                    btFormat.SetNamedSubStringValue("生产日期", item.CheckDate == null ? "" : item.CheckDate.Value.ToString("yyyy-MM-dd", DateTimeFormatInfo.InvariantInfo));
                }

                if (Convert.ToDouble(item.KWYSorceResult) < 0.000002)
                {
                    btFormat.SetNamedSubStringValue("矿物油", "＜0.02");
                }
                else if (Convert.ToDouble(item.KWYSorceResult) < 0.00001)
                {
                    btFormat.SetNamedSubStringValue("矿物油", Convert.ToDecimal(item.KWYSorceResult * 10000).ToString("F2"));
                }
                else
                {
                    btFormat.SetNamedSubStringValue("矿物油", Convert.ToDecimal(item.KWYSorceResult * 10000).ToString("F2"));
                }

                btFormat.SetNamedSubStringValue("报告编号", item.ReportCode);
                btFormat.SetNamedSubStringValue("钢瓶编码", item.CylinderCode);
                btFormat.PrintOut(false, false);
            }

            btApp.Quit(BarTender.BtSaveOptions.btDoNotSaveChanges);

            System.Runtime.InteropServices.Marshal.ReleaseComObject(btApp);

            GC.Collect();     // 强制对零代到指定代进行垃圾回收。
        }

        #region PipeSave
        protected void btnPipe1Save_Click(object sender, EventArgs e)
        {

            IList<ApplRegGasQtyInspectionValue> lt = bllApplRegGasQtyInspectionValue.GetModelList("1=1");
            if (lt.Count > 0)
            {
                lt[0].Pipe1 = this.txtPipe1.Value;
                bllApplRegGasQtyInspectionValue.Update(lt[0]);
            }

        }

        protected void btnPipe2Save_Click(object sender, EventArgs e)
        {
            IList<ApplRegGasQtyInspectionValue> lt = bllApplRegGasQtyInspectionValue.GetModelList("1=1");
            if (lt.Count > 0)
            {
                lt[0].Pipe2 = this.txtPipe2.Value;
                bllApplRegGasQtyInspectionValue.Update(lt[0]);
            }
        }

        protected void btnPipe3Save_Click(object sender, EventArgs e)
        {
            IList<ApplRegGasQtyInspectionValue> lt = bllApplRegGasQtyInspectionValue.GetModelList("1=1");
            if (lt.Count > 0)
            {
                lt[0].Pipe3 = this.txtPipe3.Value;
                bllApplRegGasQtyInspectionValue.Update(lt[0]);
            }
        }

        protected void btnPipe4Save_Click(object sender, EventArgs e)
        {
            IList<ApplRegGasQtyInspectionValue> lt = bllApplRegGasQtyInspectionValue.GetModelList("1=1");
            if (lt.Count > 0)
            {
                lt[0].Pipe4 = this.txtPipe4.Value;
                bllApplRegGasQtyInspectionValue.Update(lt[0]);
            }
        }

        protected void btnPipe5Save_Click(object sender, EventArgs e)
        {

            IList<ApplRegGasQtyInspectionValue> lt = bllApplRegGasQtyInspectionValue.GetModelList("1=1");
            if (lt.Count > 0)
            {
                lt[0].Pipe5 = this.txtPipe5.Value;
                bllApplRegGasQtyInspectionValue.Update(lt[0]);
            }
        }

        protected void btnPipe6Save_Click(object sender, EventArgs e)
        {
            IList<ApplRegGasQtyInspectionValue> lt = bllApplRegGasQtyInspectionValue.GetModelList("1=1");
            if (lt.Count > 0)
            {
                lt[0].Pipe6 = this.txtPipe6.Value;
                bllApplRegGasQtyInspectionValue.Update(lt[0]);
            }
        }

        protected void btnPipe7Save_Click(object sender, EventArgs e)
        {

            IList<ApplRegGasQtyInspectionValue> lt = bllApplRegGasQtyInspectionValue.GetModelList("1=1");
            if (lt.Count > 0)
            {
                lt[0].Pipe7 = this.txtPipe7.Value;
                bllApplRegGasQtyInspectionValue.Update(lt[0]);
            }
        }

        protected void btnPipe8Pipe_Click(object sender, EventArgs e)
        {
            IList<ApplRegGasQtyInspectionValue> lt = bllApplRegGasQtyInspectionValue.GetModelList("1=1");
            if (lt.Count > 0)
            {
                lt[0].Pipe8 = this.txtPipe8.Value;
                bllApplRegGasQtyInspectionValue.Update(lt[0]);
            }
        }
        #endregion

        #region PipeClear
        protected void btnPipe1Clear_Click(object sender, EventArgs e)
        {
            IList<ApplRegGasQtyInspectionValue> lt = bllApplRegGasQtyInspectionValue.GetModelList("1=1");
            if (lt.Count > 0)
            {
                CheckObjectList.Remove(CheckObjectList.FirstOrDefault(o => o.GPCode == txtPipe1.Value));
                lt[0].Pipe1 = this.txtPipe1.Value = string.Empty;
                bllApplRegGasQtyInspectionValue.Update(lt[0]);
            }
        }

        protected void btnPipe2Clear_Click(object sender, EventArgs e)
        {
            IList<ApplRegGasQtyInspectionValue> lt = bllApplRegGasQtyInspectionValue.GetModelList("1=1");
            if (lt.Count > 0)
            {
                CheckObjectList.Remove(CheckObjectList.FirstOrDefault(o => o.GPCode == txtPipe2.Value));
                lt[0].Pipe2 = this.txtPipe2.Value = string.Empty;
                bllApplRegGasQtyInspectionValue.Update(lt[0]);
            }
        }

        protected void btnPipe3Clear_Click(object sender, EventArgs e)
        {
            IList<ApplRegGasQtyInspectionValue> lt = bllApplRegGasQtyInspectionValue.GetModelList("1=1");
            if (lt.Count > 0)
            {
                CheckObjectList.Remove(CheckObjectList.FirstOrDefault(o => o.GPCode == txtPipe3.Value));
                lt[0].Pipe3 = this.txtPipe3.Value = string.Empty;
                bllApplRegGasQtyInspectionValue.Update(lt[0]);
            }
        }

        protected void btnPipe4Clear_Click(object sender, EventArgs e)
        {
            IList<ApplRegGasQtyInspectionValue> lt = bllApplRegGasQtyInspectionValue.GetModelList("1=1");
            if (lt.Count > 0)
            {
                CheckObjectList.Remove(CheckObjectList.FirstOrDefault(o => o.GPCode == txtPipe4.Value));
                lt[0].Pipe4 = this.txtPipe4.Value = string.Empty;
                bllApplRegGasQtyInspectionValue.Update(lt[0]);
            }
        }

        protected void btnPipe5Clear_Click(object sender, EventArgs e)
        {
            IList<ApplRegGasQtyInspectionValue> lt = bllApplRegGasQtyInspectionValue.GetModelList("1=1");
            if (lt.Count > 0)
            {
                CheckObjectList.Remove(CheckObjectList.FirstOrDefault(o => o.GPCode == txtPipe5.Value));
                lt[0].Pipe5 = this.txtPipe5.Value = string.Empty;
                bllApplRegGasQtyInspectionValue.Update(lt[0]);
            }
        }

        protected void btnPipe6Clear_Click(object sender, EventArgs e)
        {
            IList<ApplRegGasQtyInspectionValue> lt = bllApplRegGasQtyInspectionValue.GetModelList("1=1");
            if (lt.Count > 0)
            {
                CheckObjectList.Remove(CheckObjectList.FirstOrDefault(o => o.GPCode == txtPipe6.Value));

                lt[0].Pipe6 = this.txtPipe6.Value = string.Empty;
                bllApplRegGasQtyInspectionValue.Update(lt[0]);

            }
        }

        protected void btnPipe7Clear_Click(object sender, EventArgs e)
        {
            IList<ApplRegGasQtyInspectionValue> lt = bllApplRegGasQtyInspectionValue.GetModelList("1=1");
            if (lt.Count > 0)
            {
                CheckObjectList.Remove(CheckObjectList.FirstOrDefault(o => o.GPCode == txtPipe7.Value));

                lt[0].Pipe7 = this.txtPipe7.Value = string.Empty;
                bllApplRegGasQtyInspectionValue.Update(lt[0]);
            }
        }

        protected void btnPipe8Clear_Click(object sender, EventArgs e)
        {
            IList<ApplRegGasQtyInspectionValue> lt = bllApplRegGasQtyInspectionValue.GetModelList("1=1");
            if (lt.Count > 0)
            {
               
                CheckObjectList.Remove(CheckObjectList.FirstOrDefault(o=>o.GPCode== txtPipe8.Value));

                lt[0].Pipe8 = this.txtPipe8.Value = string.Empty;
                bllApplRegGasQtyInspectionValue.Update(lt[0]);
            }
        }
        #endregion

        private void CheckPassByPipe(string cylinderCode)
        {
           // if (!CheckObjectList.Exists(o => o.GPCode == this.txtPipe1.Value))
           // {
                int m_GPStatus = 0;
                var ml = bll.GetModelList(string.Format("CylinderCode='{0}' and FlowStatus='1'", cylinderCode));
                if (ml.Count > 0 && ml[0].IsPass == "1")
                {
                    m_GPStatus = 2;
                }
                else if (ml.Count > 0 && ml[0].IsPass == "0")
                {
                    m_GPStatus = 1;
                }
            CheckObjectList.Remove(CheckObjectList.FirstOrDefault(o => o.GPCode == cylinderCode));
            CheckObjectList.Add(new checkObject { GPCode = cylinderCode, GPStatus = m_GPStatus });
            //  }
            ShowMsgHelper.AlertMsg("检测成功");
        }

        protected void btnPipe1Checked_Click(object sender, EventArgs e)
        {

            CheckPassByPipe(txtPipe1.Value);

        }

        protected void btnPipe2Checked_Click(object sender, EventArgs e)
        {
            CheckPassByPipe(txtPipe2.Value);
        }

        protected void btnPipe3Checked_Click(object sender, EventArgs e)
        {
            CheckPassByPipe(txtPipe3.Value);
        }

        protected void btnPipe4Checked_Click(object sender, EventArgs e)
        {
            CheckPassByPipe(txtPipe4.Value);
        }

        protected void btnPipe5Checked_Click(object sender, EventArgs e)
        {
            CheckPassByPipe(txtPipe5.Value);
        }

        protected void btnPipe6Checked_Click(object sender, EventArgs e)
        {
            CheckPassByPipe(txtPipe6.Value);

        }

        protected void btnPipe7Checked_Click(object sender, EventArgs e)
        {
            CheckPassByPipe(txtPipe7.Value);

        }

        protected void btnPipe8Checked_Click(object sender, EventArgs e)
        {
            CheckPassByPipe(txtPipe8.Value);
            //string updateSql = string.Format("update ApplRegGasQtyInspection set MTID='1' where Flowstatus='1' and CylinderCode='{0}'", this.txtPipe8.Value);
            //Maticsoft.DBUtility.DbHelperSQL.ExecuteSql(updateSql);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using RM.Web.App_Code;
using EGMNGS.Common;
using RM.Common.DotNetUI;
using RM.Common.DotNetCode;

namespace RM.Web.GanPing
{
    public partial class CylinderCarcaseCodePage : PageBase
    {
        #region 属性字段

        EGMNGS.BLL.CylinderCarcaseCode bll = new EGMNGS.BLL.CylinderCarcaseCode();


        public EGMNGS.Model.CylinderCarcaseCode CylinderCarcaseCodeObj
        {
            get
            {
                if (ViewState["CylinderCarcaseCode"] == null)
                {
                    return new EGMNGS.Model.CylinderCarcaseCode();
                }
                return ViewState["CylinderCarcaseCode"] as EGMNGS.Model.CylinderCarcaseCode;
            }
            set { ViewState["CylinderCarcaseCode"] = value; }
        }

        private string actionMode
        {
            get { return ViewState["ActionMode"] as String; }
            set { ViewState["ActionMode"] = value; }
        }
        public string PowerSupplyCode
        {
            get { return ViewState["PowerSupplyCode"] as String; }
            set { ViewState["PowerSupplyCode"] = value; }
        }
        public string PowerSupplyName
        {
            get { return ViewState["PowerSupplyName"] as String; }
            set { ViewState["PowerSupplyName"] = value; }
        }

        #endregion

        #region 事件方法

        protected void Page_Load(object sender, EventArgs e)
        {
            this.PageControl1.pageHandler += new EventHandler(pager_PageChanged);
            if (!IsPostBack)
            {
                //  UserList = ComServies.GetAllUserInfo();
                CylinderCarcaseCodeObj = new EGMNGS.Model.CylinderCarcaseCode();
                DropDownListBinder();

            }
        }

        /// <summary>
        /// 绑定下拉列
        /// </summary>
        private void DropDownListBinder()
        {
            var sysCodeBll = new EGMNGS.BLL.SYS_CODE();
            DataSet ds = sysCodeBll.GetList("CODE_TYPE='GasType' ORDER BY DISPLAY_ORDER");

            ddlGasType.DataSource = ds.Tables[0];
            ddlGasType.DataTextField = "CODE_CHI_DESC";
            ddlGasType.DataValueField = "CODE";
            ddlGasType.DataBind();
            ddlGasType.Items.Insert(0, new ListItem("", ""));


            ddlDepositPosition.Items.Add(new ListItem() { Value = "", Text = "" });
            ddlDepositPosition.Items.Add(new ListItem() { Value = "01", Text = "左边" });
            ddlDepositPosition.Items.Add(new ListItem() { Value = "02", Text = "中间" });
            ddlDepositPosition.Items.Add(new ListItem() { Value = "03", Text = "右边" });

            // ds = sysCodeBll.GetList("CODE_TYPE='CylinderStatus' ORDER BY DISPLAY_ORDER");
            dllCylinderStatus.DataSource = DictEGMNS["GPStatus"];
            dllCylinderStatus.DataTextField = "value";
            dllCylinderStatus.DataValueField = "key";
            dllCylinderStatus.DataBind();

            ds = new EGMNGS.BLL.CylinderPositionSet().GetAllList();
            ddlPositionCode.DataSource = ds;

            ddlPositionCode.DataSource = ds.Tables[0];
            ddlPositionCode.DataTextField = "PositionName";
            ddlPositionCode.DataValueField = "PositionCode";
            ddlPositionCode.DataBind();

            ddlPositionCode.Items.Insert(0, new ListItem("", ""));
        }




        /// <summary>
        /// 绑定数据，分页
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void pager_PageChanged(object sender, EventArgs e)
        {
            DataBindGrid();
        }

        /// <summary>
        /// 绑定数据源
        /// </summary>
        private void DataBindGrid()
        {

            string sql = "1=1 ";

            //if (!string.IsNullOrWhiteSpace(CylinderCarcaseCodeObj))
            //{
            //    sql += string.Format(" and PositionCode='{0}'", CylinderCarcaseCodeObj.PositionCode);
            //}
            //else if (!string.IsNullOrWhiteSpace(CylinderCarcaseCodeObj.PositionName))
            //{
            //    sql += string.Format(" and PositionName='{0}'", CylinderCarcaseCodeObj.PositionName);
            //}
            //else if (!string.IsNullOrWhiteSpace(CylinderCarcaseCodeObj.positionRemarks))
            //{
            //    sql += string.Format(" and positionRemarks='{0}'", CylinderCarcaseCodeObj.positionRemarks);
            //}


            int count = bll.GetRecordCount(sql);
            DataSet ds = bll.GetListByPage(sql, "OID", PageControl1.PageIndex, PageControl1.PageSize);
            ControlBindHelper.BindRepeaterList(ds.Tables[0], rp_Item);
            this.PageControl1.RecordCount = Convert.ToInt32(count);
        }
        /// <summary>
        /// 绑定数据到对象
        /// </summary>
        private void DataBinder()
        {
            //  Model.CylinderCode = this.txtCylinderCode.Text;
            //   Model.RemarksDesc = this.txtRemarksDesc.Text;
        }

        /// <summary>
        /// 初始化控件
        /// </summary>
        private void InitControl()
        {
            //  txtCylinderCode.Focus();
            //  this.txtCylinderCode.Text = Model.CylinderCode;
            //  this.txtRemarksDesc.Text = Model.RemarksDesc;
            //   this.ddlCFStatus.SelectedValue = Model.Status;
            //  this.txtClearManOID.Text = GetUserName(Model.ClearManOID);
            //  this.txtClearDate.Text = Model.ClearDate == null ? string.Empty : Model.ClearDate.Value.ToShortDateString();
        }

        /// <summary>
        /// 获取用户名
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        //private string GetUserName(string userId)
        //{
        //    DataRow[] dr = UserList.Select(string.Format("User_ID='{0}'", userId));
        //    if (dr.Length > 0)
        //    {
        //        return dr[0][1].ToString();
        //    }
        //    return string.Empty;
        //}

        private void ClearControl()
        {

        }

        #endregion

        #region 按钮事件

        protected void btnSubmit_Click(object sender, EventArgs e)
        {

        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            DataBindGrid();
        }

        /// <summary>
        /// 获取当前对象
        /// </summary>
        private void ShowMaxMode()
        {

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                CylinderCarcaseCodeObj.CylinderStatusName = dllCylinderStatus.SelectedItem.Text;
                CylinderCarcaseCodeObj.GasTypeName = ddlGasType.SelectedItem.Text;
                CylinderCarcaseCodeObj.DepositPositionName = ddlDepositPosition.SelectedItem.Text;
                CylinderCarcaseCodeObj.PositionName = this.ddlPositionCode.SelectedItem.Text;

                if (CylinderCarcaseCodeObj.OID > 0)//编辑
                {
                    CylinderCarcaseCodeObj.LAST_UPD_BY = User.UserName.ToString();
                    CylinderCarcaseCodeObj.LAST_UPD_DATE = DateTime.Now;

                    //不能编辑
                    // bll.Update(CylinderCarcaseCodeObj);
                }
                else//添加
                {
                    //XGQT010203，XGQT是库区编码，左边是01，右边是02；列和层都是两位小数。
                    CylinderCarcaseCodeObj.CarcaseCode = string.Format("{0}{1}{2:00}{3:00}", CylinderCarcaseCodeObj.PositionCode, CylinderCarcaseCodeObj.DepositPosition, CylinderCarcaseCodeObj.DepositColumnCount, CylinderCarcaseCodeObj.DepositRowCount);
                    CylinderCarcaseCodeObj.CREATED_BY = User.UserName.ToString();
                    CylinderCarcaseCodeObj.CREATED_DATE = DateTime.Now;
                    bll.Add(CylinderCarcaseCodeObj);
                }

                ShowMsgHelper.AlertMsg("保存成功");

            }
            catch (ArgumentOutOfRangeException ex)
            {
                string[] errs = CommonHelper.GetValue(ex.Message, "@").Split(':');
                ShowMsgHelper.ExecuteScript(string.Format("ChangeCss($('#txt{0}'),'长度不能超{1}字')", errs[0], errs[1]));
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            CylinderCarcaseCodeObj = new EGMNGS.Model.CylinderCarcaseCode();
            CylinderCarcaseCodeObj.CarcaseCode = "新建编码";
            CylinderCarcaseCodeObj.CREATED_BY = User.UserName.ToString();

            this.txtDepositColumnCount.ReadOnly = false;
            this.txtDepositDesc.ReadOnly = false;
            this.txtDepositRowCount.ReadOnly = false;
            this.ddlDepositPosition.Enabled = true;
            this.ddlGasType.Enabled = true;
            this.ddlPositionCode.Enabled = true;
        }


        protected void btnDel_Click(object sender, EventArgs e)
        {
            if (bll.Delete(CylinderCarcaseCodeObj.OID))
            {
                ShowMsgHelper.AlertMsg("删除成功！");
            }
            else
            {
                ShowMsgHelper.AlertMsg("删除失败！");
            }
        }

        protected void rp_Item_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            int oid = Convert.ToInt32(e.CommandArgument);

            CylinderCarcaseCodeObj = bll.GetModel(oid);


      
            this.txtDepositColumnCount.ReadOnly = true;
            this.txtDepositDesc.ReadOnly = true;
            this.txtDepositRowCount.ReadOnly = true;
            this.ddlDepositPosition.Enabled = false;
            this.ddlGasType.Enabled = false;
            this.ddlPositionCode.Enabled = false;
        }
        #endregion
    }
}
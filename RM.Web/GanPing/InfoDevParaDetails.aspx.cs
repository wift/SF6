﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DapperData;
using EGMNGS.Model;

namespace RM.Web.GanPing
{
    public partial class InfoDevParaDetails : System.Web.UI.Page
    {
        public EGMNGS.Model.InfoDevPara Model
        {
            get
            {
                if (ViewState["InfoDevPara"] == null)
                {
                    return new EGMNGS.Model.InfoDevPara();
                }
                return ViewState["InfoDevPara"] as EGMNGS.Model.InfoDevPara;
            }
            set { ViewState["InfoDevPara"] = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                int oid = Request["oid"].AsTargetType<int>(0);
                Repository _Repositor = new Repository();

                Model = _Repositor.GetById<InfoDevPara>(oid);
            }
        }
    }
}
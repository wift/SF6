﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using RM.Common.DotNetCode;
using System.Data;
using RM.Common.DotNetUI;
using RM.Web.App_Code;
using EGMNGS.Common;
using EGMNGS.Model;

namespace RM.Web.GanPing
{
    public partial class HSTableGasInStoragePage : PageBase
    {
        #region 属性字段

        EGMNGS.BLL.TableGasInStorage bll = new EGMNGS.BLL.TableGasInStorage();
        EGMNGS.BLL.DetailsGasStorage DetailsBLL = new EGMNGS.BLL.DetailsGasStorage();

        public Dictionary<string, string> DictInGasSourse
        {
            get
            {
                if (ViewState["InGasSourse"] == null)
                {
                    Dictionary<string, string> inGasSourse = new Dictionary<string, string>();
                    EGMNGS.BLL.SYS_CODE sysCodeBll = new EGMNGS.BLL.SYS_CODE();
                    DataSet dsFlowStatus = sysCodeBll.GetList("CODE_TYPE='InGasSourse' ORDER BY DISPLAY_ORDER");
                    foreach (DataRow item in dsFlowStatus.Tables[0].Rows)
                    {
                        inGasSourse.Add(item["CODE"].ToString(), item["CODE_CHI_DESC"].ToString());
                    }
                    ViewState["InGasSourse"] = inGasSourse;
                }

                return ViewState["InGasSourse"] as Dictionary<string, string>;
            }
            set { ViewState["InGasSourse"] = value; }
        }

        private Dictionary<string, string> lsCFStatus
        {
            get
            {
                if (Session["GPStatus"] != null)
                {
                    return Session["GPStatus"] as Dictionary<string, string>;
                }
                return new Dictionary<string, string>();
            }

            set
            {
                Session["GPStatus"] = value;
            }
        }

        private DataTable UserList
        {
            get
            {
                return ViewState["UserList"] as DataTable;
            }
            set { ViewState["UserList"] = value; }
        }
        private EGMNGS.Model.TableGasInStorage Model
        {
            get
            {
                if (ViewState["TableGasInStorage"] == null)
                {
                    return new EGMNGS.Model.TableGasInStorage();
                }
                return ViewState["TableGasInStorage"] as EGMNGS.Model.TableGasInStorage;
            }
            set { ViewState["TableGasInStorage"] = value; }
        }
        private string actionMode
        {
            get { return ViewState["ActionMode"] as String; }
            set { ViewState["ActionMode"] = value; }
        }
        public string PowerSupplyCode
        {
            get { return ViewState["PowerSupplyCode"] as String; }
            set { ViewState["PowerSupplyCode"] = value; }
        }
        public string PowerSupplyName
        {
            get { return ViewState["PowerSupplyName"] as String; }
            set { ViewState["PowerSupplyName"] = value; }
        }

        public DataTable DetailsTable
        {
            get
            {
                if (ViewState["DetailsTable"] == null)
                {
                    return new DataTable();
                }
                return ViewState["DetailsTable"] as DataTable;
            }
            set { ViewState["DetailsTable"] = value; }
        }
        private string actionModeDetails
        {
            get
            {
                if (ViewState["actionModeDetails"] == null)
                {
                    return string.Empty;
                }
                return ViewState["actionModeDetails"] as String;
            }
            set { ViewState["actionModeDetails"] = value; }
        }

        private List<DetailsGasStorage> UpdateList
        {
            get
            {
                if (ViewState["UpdateList"] == null)
                {
                    return new List<DetailsGasStorage>();
                }
                return ViewState["UpdateList"] as List<DetailsGasStorage>;
            }
            set { ViewState["UpdateList"] = value; }
        }
        /// <summary>
        /// 需要添加的数据
        /// </summary>
        private List<DetailsGasStorage> AddList
        {
            get
            {
                if (ViewState["AddList"] == null)
                {
                    return new List<DetailsGasStorage>();
                }
                return ViewState["AddList"] as List<DetailsGasStorage>;
            }
            set { ViewState["AddList"] = value; }
        }

        public Dictionary<string, string> FillStatus
        {
            get
            {
                if (ViewState["FillStatus"] == null)
                {
                    Dictionary<string, string> fillStatus = new Dictionary<string, string>();
                    fillStatus.Add(string.Empty, string.Empty);
                    EGMNGS.BLL.SYS_CODE sysCodeBll = new EGMNGS.BLL.SYS_CODE();
                    DataSet dsFlowStatus = sysCodeBll.GetList("CODE_TYPE='FillStatus' ORDER BY DISPLAY_ORDER");
                    foreach (DataRow item in dsFlowStatus.Tables[0].Rows)
                    {
                        fillStatus.Add(item["CODE"].ToString(), item["CODE_CHI_DESC"].ToString());
                    }
                    ViewState["FillStatus"] = fillStatus;
                }

                return ViewState["FillStatus"] as Dictionary<string, string>;
            }
            set { ViewState["FillStatus"] = value; }
        }
        #endregion

        #region 事件方法

        protected void Page_Load(object sender, EventArgs e)
        {
            this.PageControl1.pageHandler += new EventHandler(pager_PageChanged);

            if (!IsPostBack)
            {
                EGMNGS.BLL.SYS_CODE sysCodeBll = new EGMNGS.BLL.SYS_CODE();
                lsCFStatus = sysCodeBll.GetModelList("CODE_TYPE='GPStatus' ORDER BY DISPLAY_ORDER").ToDictionary(o => o.CODE, o => o.CODE_CHI_DESC);


                UserList = ComServies.GetAllUserInfo();
                string[] orgIdOrgName = ComServies.GetPowerApplyByUserID(User.UserId.ToString());
                PowerSupplyCode = orgIdOrgName[0];
                PowerSupplyName = orgIdOrgName[1];

                DropDownListBinder();
            }

            if (actionMode != "Add")
            {
                intiBusinessCode();
            }



        }
        /// <summary>
        ///  //业务编号
        /// </summary>
        private void intiBusinessCode()
        {

            DataSet dt = new EGMNGS.BLL.ApplRecyGasReg().GetList(@"EXISTS (SELECT 1 FROM BookGasFill WHERE BusinessCode=RecyCode)
                                                                    and 

                                                                    (SELECT COUNT(1) FROM BookGasFill b WHERE b.BusinessCode=RecyCode)>
                                                                    (select COUNT(1) from TableGasInStorage t,DetailsGasStorage d 
                                                                    where t.BusinessCode=RecyCode and t.Code=d.DtGasInStorageCode)");
            this.ddlBusinessCode.DataSource = dt.Tables[0];
            this.ddlBusinessCode.DataTextField = "RecyCode";
            this.ddlBusinessCode.DataValueField = "RecyCode";
            this.ddlBusinessCode.DataBind();
        }
        /// <summary>
        /// 绑定下拉列
        /// </summary>
        private void DropDownListBinder()
        {

            EGMNGS.BLL.SYS_CODE sysCodeBll = new EGMNGS.BLL.SYS_CODE();
            DataSet dsYear = sysCodeBll.GetList("CODE_TYPE='Year' ORDER BY DISPLAY_ORDER");

            this.ddlYear.DataSource = dsYear.Tables[0];
            this.ddlYear.DataTextField = "CODE_CHI_DESC";
            this.ddlYear.DataValueField = "CODE";
            this.ddlYear.DataBind();

            DataSet dsMonth = sysCodeBll.GetList("CODE_TYPE='Month' ORDER BY DISPLAY_ORDER");

            this.ddlMonth.DataSource = dsMonth.Tables[0];
            this.ddlMonth.DataTextField = "CODE_CHI_DESC";
            this.ddlMonth.DataValueField = "CODE";
            this.ddlMonth.DataBind();


            sysCodeBll = new EGMNGS.BLL.SYS_CODE();
            DataSet dsGasType = sysCodeBll.GetList("CODE_TYPE='GasClass' ORDER BY DISPLAY_ORDER");

            ddlGasType.DataSource = dsGasType.Tables[0];
            ddlGasType.DataTextField = "CODE_CHI_DESC";
            ddlGasType.DataValueField = "CODE";
            ddlGasType.DataBind();


            DataSet dsGasSourse = sysCodeBll.GetList("CODE_TYPE='InGasSourse' ORDER BY DISPLAY_ORDER");

            ddlGasSourse.DataSource = dsGasSourse.Tables[0];
            ddlGasSourse.DataTextField = "CODE_CHI_DESC";
            ddlGasSourse.DataValueField = "CODE";
            ddlGasSourse.DataBind();

            ddlGasSourse.SelectedValue = "3";

            DataSet dtCFStatus = sysCodeBll.GetList("CODE_TYPE='CFStatus' ORDER BY DISPLAY_ORDER");

            this.ddlStatus.DataSource = dtCFStatus.Tables[0];
            this.ddlStatus.DataTextField = "CODE_CHI_DESC";
            this.ddlStatus.DataValueField = "CODE";
            this.ddlStatus.DataBind();

            this.ddlStatus.Items.Insert(0, string.Empty);

            this.ddlGasType.SelectedValue = "1";


            //业务编号
            //DataSet dt = new EGMNGS.BLL.ApplRecyGasReg().GetList("EXISTS (SELECT 1 FROM BookGasFill WHERE BusinessCode=RecyCode)");
            //this.ddlBusinessCode.DataSource = dt.Tables[0];
            //this.ddlBusinessCode.DataTextField = "RecyCode";
            //this.ddlBusinessCode.DataValueField = "RecyCode";
            //this.ddlBusinessCode.DataBind();
        }


        /// <summary>
        /// 绑定后激发事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void rp_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Label lblUserID = e.Item.FindControl("lblUserID") as Label;
                if (lblUserID != null)
                {
                    lblUserID.Text = GetUserName(lblUserID.Text);
                }
            }
        }

        /// <summary>
        /// 绑定数据，分页
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void pager_PageChanged(object sender, EventArgs e)
        {
            DataBindGrid();
        }

        /// <summary>
        /// 绑定数据源
        /// </summary>
        private void DataBindGrid()
        {
            int count = bll.GetRecordCount("GasType='1'");
            DataSet ds = bll.GetListByPage("GasType='1'", "Code desc", PageControl1.PageIndex, PageControl1.PageSize);
            ControlBindHelper.BindRepeaterList(ds.Tables[0], rp_Item);
            this.PageControl1.RecordCount = Convert.ToInt32(count);
        }

        /// <summary>
        /// 绑定数据到对象
        /// </summary>
        private void DataBinder()
        {
            if (this.txtAmountInput.Text.Trim().Length > 0)
            {
                Model.AmountInput = decimal.Parse(this.txtAmountInput.Text);
            }

            Model.Year = this.ddlYear.SelectedValue;
            Model.Month = this.ddlMonth.SelectedValue;

            Model.Code = this.txtCode.Text;

            Model.BusinessCode = this.ddlBusinessCode.SelectedValue;
            Model.GasSourse = this.ddlGasSourse.SelectedValue;
            Model.GasType = this.ddlGasType.SelectedValue;
        }

        /// <summary>
        /// 初始化控件
        /// </summary>
        private void InitControl()
        {
            this.txtAmountInput.Text = Model.AmountInput.ToString();
            this.ddlMonth.SelectedValue = Model.Month;
            this.ddlYear.SelectedValue = Model.Year;
            if (actionMode == "Edit")
            {
                this.ddlBusinessCode.Items.Clear();
                this.ddlBusinessCode.Items.Add(Model.BusinessCode);
            }

            //this.ddlBusinessCode.SelectedValue = Model.BusinessCode;
            this.ddlGasSourse.SelectedValue = Model.GasSourse;
            this.ddlGasType.SelectedValue = Model.GasType;
            this.txtRegistrantOID.Text = GetUserName(Model.registrantOID);
            this.txtRegistrantDate.Text = Model.RegistrantDate == null ? string.Empty : Model.RegistrantDate.Value.ToShortDateString();
            this.ddlStatus.SelectedValue = Model.Status;
            this.txtCode.Text = Model.Code;
        }

        /// <summary>
        /// 获取用户名
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        private string GetUserName(string userId)
        {
            DataRow[] dr = UserList.Select(string.Format("User_ID='{0}'", userId));
            if (dr.Length > 0)
            {
                return dr[0][1].ToString();
            }
            return string.Empty;
        }
        #endregion

        #region 按钮事件

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            if (Model.Status == "1")
            {
                ShowMsgHelper.Alert_Wern("无法重新提交！");
                return;
            }

            if (ComServies.UpdateHSTableGasInStoragePageStatus(Model.Code, Model.BusinessCode))
            {
                Model = bll.GetModel(Model.OID);
                ShowMsgHelper.Alert("提交成功！");
                InitControl();
                DataBindGrid();
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            DataBindGrid();
        }

        private void ClearControl()
        {
            Model = new TableGasInStorage();
            InitControl();
        }
        private void ShowMaxMode()
        {
            Model = bll.GetModel(bll.GetMaxId() - 1);
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                if (actionMode == "Add")
                {
                    DataBinder();
                    Model.registrantOID = User.UserId.ToString();

                    int count = bll.GetRecordCount(string.Format("Code='{0}'", Model.Code));
                    if (count > 0)
                    {
                        ShowMsgHelper.showWarningMsg("入库编号已经存在!");
                        return;
                    }

                    if (bll.Add(Model) > 0)
                    {
                        actionMode = string.Empty;
                    }

                    DataBindGrid();
                    ShowMaxMode();
                    GetDetailsOfRecycleGas();
                }
                else if (actionMode == "Edit")
                {
                    if (bll.GetModel(Model.OID).Status != "0")
                    {
                        ShowMsgHelper.Alert_Wern("“已确认”状态无法修改数据！");
                        return;
                    }

                    DataBinder();
                    if (bll.Update(Model))
                    {
                        EGMNSShowMsg.ShowEditMsgSuccess();
                        actionMode = string.Empty;
                    }
                    DataBindGrid();
                }
            }
            catch (ArgumentOutOfRangeException ex)
            {
                string[] errs = CommonHelper.GetValue(ex.Message, "@").Split(':');
                ShowMsgHelper.ExecuteScript(string.Format("ChangeCss($('#txt{0}'),'长度不能超{1}字')", errs[0], errs[1]));
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            if (ComServies.GetCheckStatus(DateTime.Now))
            {
                ShowMsgHelper.Alert_Wern("已对账");
                return;
            }

            actionMode = "Add";
            Model = new EGMNGS.Model.TableGasInStorage(actionMode);
            Model.RegistrantDate = DateTime.Now;
            Model.registrantOID = User.UserId.ToString();
            Model.Status = "0";
            Model.GasType = "1";
            InitControl();

            GetDetailsOfRecycleGas();
        }


        protected void btnDel_Click(object sender, EventArgs e)
        {
            if (bll.GetModel(Model.OID).Status != "0")
            {
                ShowMsgHelper.Alert_Wern("“已确认”状态不可以删除");
            }
            else
            {
                bll.Delete(Model.OID);
                Model = new EGMNGS.Model.TableGasInStorage();
                InitControl();
                DataBindGrid();
                ShowMsgHelper.Alert("删除成功！");
            }
        }

        protected void rp_Item_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            int oid = Convert.ToInt32(e.CommandArgument);

            Model = bll.GetModel(oid);

            actionMode = "Edit";
            InitControl();
            //绑定明细表
            GetDetailsOfRecycleGas();
        }
        #endregion

        #region Details
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void pager_PageChangedDetails(object sender, EventArgs e)
        {
            GetDetailsOfRecycleGas();
        }

        private void GetDetailsOfRecycleGas()
        {
            DetailsTable = DetailsBLL.GetList(string.Format("DtGasInStorageCode='{0}'", Model.Code)).Tables[0];

            int count = DetailsBLL.GetRecordCount(string.Format("DtGasInStorageCode='{0}'", Model.Code));
            ControlBindHelper.BindRepeaterList(DetailsTable, rptDetails);
        }

        protected void btnAddDetails_Click(object sender, EventArgs e)
        {
            if (Convert.ToString(Model.Status) == "1")
            {
                ShowMsgHelper.Alert_Wern("该记录已提交！");
                return;
            }

            if (this.txtCode.Text.Trim().Length == 0)
            {
                ShowMsgHelper.Alert("请选择入库编号");
                return;
            }


            //首先，恢复数据源
            RetriRepeter();
            DataTable dt = DetailsTable;
            if (LoadTestData()) return;
            DataRow row = dt.NewRow();
            row[2] = string.Empty;
            row[3] = string.Empty;
            row[4] = string.Empty;
            row[5] = 0.0;
            row[6] = string.Empty;
            row[10] = Model.Code;
            dt.Rows.Add(row);

            rptDetails.DataSource = dt;
            rptDetails.DataBind();


            actionModeDetails = "Add";
        }

        private void RetriRepeter()
        {
            for (int i = 0; i < this.rptDetails.Items.Count; i++)
            {
                Label txtAmountGas = this.rptDetails.Items[i].FindControl("lblAmountGas") as Label;
                DetailsTable.Rows[i]["AmountGas"] = Convert.ToDecimal(txtAmountGas.Text);
                TextBox txtCylinderCode = this.rptDetails.Items[i].FindControl("txtCylinderCode") as TextBox;
                DetailsTable.Rows[i]["CylinderCode"] = txtCylinderCode.Text;
            }
        }

        /// <summary> 
        /// 从库表获取数据
        /// </summary>
        private bool LoadTestData()
        {

            if (DetailsTable.Rows.Count > 0)
            {
                string gpCode = DetailsTable.Rows[DetailsTable.Rows.Count - 1]["CylinderCode"].ToString();
                var oid = DetailsTable.Rows[DetailsTable.Rows.Count - 1][0];


                var obj = new EGMNGS.BLL.BookGasFill().GetModelList(string.Format(@"CylinderCode='{0}'
	                                                                  AND BusinessCode='{1}'", gpCode, Model.BusinessCode));

                if (obj.Count == 0)
                {
                    ShowMsgHelper.Alert_Error(string.Format("{0}此钢瓶无法入库", DetailsTable.Rows[DetailsTable.Rows.Count - 1]["CylinderCode"].ToString()));
                    return true;

                }

                int count = new EGMNGS.BLL.DetailsGasStorage().GetRecordCount(string.Format("GasCode='{0}'", obj[0].GasCode));
                if ((count > 0 && Convert.IsDBNull(oid)) || Convert.ToInt16(obj[0].Status) >= 2)
                {
                    ShowMsgHelper.Alert_Error(string.Format("{0}此钢瓶已入库", DetailsTable.Rows[DetailsTable.Rows.Count - 1]["CylinderCode"].ToString()));
                    return true;
                }

                DetailsTable.Rows[DetailsTable.Rows.Count - 1]["AmountGas"] = Convert.ToDecimal(obj[0].AmountGas);


            }



            return false;
        }

        protected void btnDelDetails_Click(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToString(Model.Status) == "1")
                {
                    ShowMsgHelper.Alert_Wern("该记录已提交！");
                    return;
                }

                string strID = HiddenField1.Value;
                if (strID.Length == 0)
                {
                    return;
                }
                DetailsBLL.DeleteList(strID);
                ShowMsgHelper.Alert("删除成功！");
                GetDetailsOfRecycleGas();
            }
            catch (Exception)
            {
                ShowMsgHelper.Alert("删除失败！");
                throw;
            }
        }

        protected void btnSaveDetails_Click(object sender, EventArgs e)
        {
            try
            {

                if (Convert.ToString(Model.Status) == "1")
                {
                    ShowMsgHelper.Alert_Wern("该记录已提交！");
                    return;
                }
                if (LoadTestData()) return;
                GetCheckBox();
                if (actionModeDetails.Equals("Add"))
                {
                    if (AddList.Count > 0)
                    {
                        //批量添加
                        foreach (var item in AddList)
                        {
                            List<BookGasFill> listBookGasFill = new EGMNGS.BLL.BookGasFill().GetModelList(string.Format("BusinessCode='{0}' and CylinderCode='{1}'", Model.BusinessCode, item.CylinderCode));

                            if (listBookGasFill != null && listBookGasFill.Count == 0)
                            {
                                ShowMsgHelper.Alert_Error(string.Format("此钢瓶编码{0}不属于此业务单号", item.CylinderCode));
                                continue;
                            }
                            var itemFill = listBookGasFill[0];
                            //DetailsBLL.Add(item);
                            DetailsBLL.Add(new DetailsGasStorage() { CylinderCode = itemFill.CylinderCode, GasCode = itemFill.GasCode, AmountGas = itemFill.AmountGas, CylinderStatus = itemFill.CylinderStatus, DtGasInStorageCode = Model.Code });
                        }
                        AddList.Clear();
                    }

                    ShowMsgHelper.Alert("添加成功！");
                    actionModeDetails = string.Empty;

                }

                if (UpdateList.Count > 0)
                {
                    //批量修改
                    foreach (var item in UpdateList)
                    {
                        DetailsBLL.Update(item);
                    }
                    UpdateList.Clear();
                }

                GetDetailsOfRecycleGas();
            }
            catch (Exception ex)
            {
                ShowMsgHelper.AlertMsg("添加失败！" + ex.Message);
            }
        }

        protected void rptDetails_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                CheckBox cb = e.Item.FindControl("checkbox") as CheckBox;
                if (cb.ToolTip.Length == 0)
                {
                    cb.Checked = true;
                }

            }
        }


        /// <summary>
        /// 
        /// </summary>
        private void GetCheckBox()
        {
            UpdateList = new List<DetailsGasStorage>();
            AddList = new List<DetailsGasStorage>();
            for (int i = 0; i < this.rptDetails.Items.Count; i++)
            {
                System.Web.UI.WebControls.CheckBox checkbox = (System.Web.UI.WebControls.CheckBox)rptDetails.Items[i].FindControl("checkbox");

                TextBox txtCylinderCode = this.rptDetails.Items[i].FindControl("txtCylinderCode") as TextBox;
                if (txtCylinderCode.Text.Trim().Length == 0)
                {
                    continue;
                }
                if (checkbox.Checked == true && checkbox.ToolTip.Length > 0)
                {
                    // TextBox txtCylinderCode = this.rptDetails.Items[i].FindControl("txtCylinderCode") as TextBox;

                    Label txtAmountGas = this.rptDetails.Items[i].FindControl("lblAmountGas") as Label;

                    DetailsGasStorage DetailsOfRecycleGasObj = new EGMNGS.BLL.DetailsGasStorage().GetModel(int.Parse(checkbox.ToolTip));

                    //  DetailsOfRecycleGasObj.OID = int.Parse(checkbox.ToolTip);
                    DetailsOfRecycleGasObj.CylinderCode = txtCylinderCode.Text;
                    if (txtAmountGas.Text.Trim().Length > 0)
                    {
                        DetailsOfRecycleGasObj.AmountGas = decimal.Parse(txtAmountGas.Text);
                    }


                    UpdateList.Add(DetailsOfRecycleGasObj);
                }
                else if (checkbox.Checked == true && checkbox.ToolTip.Length == 0)
                {
                    // TextBox txtCylinderCode = this.rptDetails.Items[i].FindControl("txtCylinderCode") as TextBox;

                    Label txtAmountGas = this.rptDetails.Items[i].FindControl("lblAmountGas") as Label;

                    DetailsGasStorage DetailsOfRecycleGasObj = new EGMNGS.Model.DetailsGasStorage();

                    DetailsOfRecycleGasObj.CylinderCode = txtCylinderCode.Text;
                    if (txtAmountGas.Text.Trim().Length > 0)
                    {
                        DetailsOfRecycleGasObj.AmountGas = decimal.Parse(txtAmountGas.Text);
                    }


                    AddList.Add(DetailsOfRecycleGasObj);
                }
            }
        }



        /// <summary>
        /// 快速添加
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnAddFast_Click(object sender, EventArgs e)
        {

            List<BookGasFill> listBookGasFill = new EGMNGS.BLL.BookGasFill().GetModelList(string.Format("BusinessCode='{0}'", Model.BusinessCode));

            foreach (BookGasFill item in listBookGasFill)
            {
                if (Convert.ToString(item.Status) == "1")
                {
                    if (DetailsBLL.GetRecordCount(string.Format("GasCode='{0}'", item.GasCode)) > 0)
                    {
                        continue;
                    }
                    DetailsBLL.Add(new DetailsGasStorage() { CylinderCode = item.CylinderCode, GasCode = item.GasCode, AmountGas = item.AmountGas, CylinderStatus = item.CylinderStatus, DtGasInStorageCode = Model.Code });

                }

            }
            GetDetailsOfRecycleGas();

        }

        #endregion
    }
}
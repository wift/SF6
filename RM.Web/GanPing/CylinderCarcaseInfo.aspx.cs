﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using RM.Common.DotNetUI;

namespace RM.Web.GanPing
{

    public partial class CylinderCarcaseInfo : System.Web.UI.Page
    {
        public string MiddleTitle { set; get; }
        public string LocationCarcaseCode { set; get; }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                var ds = new EGMNGS.BLL.CylinderPositionSet().GetAllList();
                ddlPositionCode.DataSource = ds;

                ddlPositionCode.DataSource = ds.Tables[0];
                ddlPositionCode.DataTextField = "PositionName";
                ddlPositionCode.DataValueField = "PositionCode";
                ddlPositionCode.DataBind();

                ddlPositionCode.Items.Insert(0, new ListItem("", ""));

            }

        }


        /// <summary>
        /// 绑定数据源
        /// </summary>
        private void DataBindGrid()
        {
            string carcaseCode = this.ddlPositionCode.SelectedValue;
            LoadDataByCarcaseCode(carcaseCode);
        }

        private void LoadDataByCarcaseCode(string p_carcaseCode)
        {
            string carcaseCode = p_carcaseCode;
             DataTable dt = GetCarcaseInfoByPositionCode(carcaseCode);
            int allcount = dt.Rows.Count * 6;
            string sql = string.Format(@"select count(c.CylinderCode) AS codeCount,
  (    SELECT CODE_CHI_DESC
    FROM dbo.SYS_CODE 
    WHERE CODE_TYPE='GPRL' AND CODE=b.CylinderCapacity) AS CylinderCapacity
   from Cyl_Car_Code_Relations c
   LEFT JOIN [EGMNGS].[dbo].[BookCylinderInfo] b ON b.CylinderCode=c.CylinderCode
   where CarcaseCode LIKE '{0}%' GROUP BY b.CylinderCapacity", carcaseCode);

            DataTable dtcount = EGMNGS.Common.ComServies.Query(sql);
            string tempCount = string.Empty;
            int usecount = 0;
            foreach (DataRow item in dtcount.Rows)
            {
                int codeCount = item["codeCount"].AsTargetType<int>(0);
                usecount += codeCount;

                tempCount += string.Format("{0}个{1}KG,", codeCount, item["CylinderCapacity"]);
            }
            tempCount = string.Format("已存放：{0}个", usecount) + "(" + tempCount.TrimEnd(',') + ")";

            MiddleTitle = string.Format("{0}({1}),钢瓶糟位共：{3}个。 {2}", this.ddlPositionCode.SelectedItem.Text, carcaseCode, tempCount, allcount);
            this.rpttopList.DataSource = GetDataSource(dt, "03");
            this.rpttopList.DataBind();

            this.rptMiddleList.DataSource = GetDataSource(dt, "02");
            this.rptMiddleList.DataBind();

            this.rptbottomList.DataSource = GetDataSource(dt, "01");
            this.rptbottomList.DataBind();
        }
        /// <summary>
        /// 获取右边
        /// </summary>
        /// <returns></returns>
        private Dictionary<string, string> GetDataSource(DataTable dt, string depositPosition)
        {
            //将列保存到dict中一列一个键值
            Dictionary<string, List<string>> dict = new Dictionary<string, List<string>>();
            Dictionary<string, string> resultsdict = new Dictionary<string, string>();

            var dr = dt.Select(string.Format("DepositPosition='{0}'", depositPosition), "DepositRowCount desc");
            foreach (var item in dr)
            {
                if (!dict.Keys.Contains(item["DepositColumnCount"].AsTargetType<string>("")))
                {
                    dict[item["DepositColumnCount"].ToString()] = new List<string>();
                }

                dict[item["DepositColumnCount"].ToString()].Add(item["CarcaseCode"].ToString());

            }

            return GenStringColumn(depositPosition, dict);
        }
        /// <summary>
        /// 根据列生成html
        /// </summary>
        /// <param name="dict"></param>
        /// <returns></returns>
        private Dictionary<string, string> GenStringColumn(string depositPosition, Dictionary<string, List<string>> dict)
        {
            Dictionary<string, string> resultsdict = new Dictionary<string, string>();

            foreach (var item in dict)
            {
                List<string> list = item.Value;
                resultsdict[item.Key] = "";
                foreach (var item1 in list)
                {

                    resultsdict[item.Key] += GenRowByCode(item1);//rowhtml;

                }
                if (item.Value.Count > 0)
                {
                    if (depositPosition == "01")
                    {
                        depositPosition = "左边";
                    }
                    else if (depositPosition == "02")
                    {
                        depositPosition = "中间";
                    }
                    else if (depositPosition == "03")
                    {
                        depositPosition = "右边";
                    }

                    int usecout = resultsdict[item.Key].Replace("circleyellow", "*").Count(o => o == '*');
                    resultsdict[item.Key] += string.Format("<table><tr><td>{0}{1}第{2}列（{3}）   已存：{4},空位：{5}</td></tr></table>", this.ddlPositionCode.SelectedItem.Text, depositPosition, item.Key, item.Value.FirstOrDefault().Substring(0, 8), usecout, item.Value.Count * 6 - usecout);
                }
            }

            return resultsdict;
        }
        ///// <summary>
        ///// 获取左边
        ///// </summary>
        ///// <returns></returns>
        //private Dictionary<string, string> GetBottomDataSource(DataTable dt)
        //{
        //    //将列保存到dict中一列一个键值
        //    Dictionary<string, List<string>> dict = new Dictionary<string, List<string>>();
        //    Dictionary<string, string> resultsdict = new Dictionary<string, string>();

        //    var dr = dt.Select("DepositPosition='01'");
        //    foreach (var item in dr)
        //    {
        //        if (!dict.Keys.Contains(item["DepositColumnCount"].AsTargetType<string>("")))
        //        {
        //            dict[item["DepositColumnCount"].ToString()] = new List<string>();
        //        }

        //        dict[item["DepositColumnCount"].ToString()].Add(item["CarcaseCode"].ToString());

        //    }

        //    return GenStringColumn(dict);
        //}

        /// <summary>
        /// 生成行html
        /// </summary>
        /// <param name="carcaseCode"></param>
        /// <returns></returns>
        private string GenRowByCode(string carcaseCode)
        {

            string rowhtml = string.Format("<table class=\"jzlayer {1}\"><tr><td>{0}</td>#td#</tr></table>", carcaseCode, LocationCarcaseCode == carcaseCode?"HighText":"");
            System.Text.StringBuilder sb = new System.Text.StringBuilder();

            try
            {


                string sql = string.Format(@"select c.CylinderCode,IsPass,
  (    SELECT CODE_CHI_DESC
    FROM dbo.SYS_CODE 
    WHERE CODE_TYPE='GPRL' AND CODE=b.CylinderCapacity) AS CylinderCapacity,b.[Status]
   from Cyl_Car_Code_Relations c
   LEFT JOIN [EGMNGS].[dbo].[BookCylinderInfo] b ON b.CylinderCode=c.CylinderCode
left join ApplRegGasQtyInspection a on a.GasCdoe=b.CurGasCode
   where CarcaseCode='{0}' order by PKID desc", carcaseCode);
                DataTable dt = EGMNGS.Common.ComServies.Query(sql);




                for (int i = 0; i <= 5; i++)
                {
                    string cylinderCapacity = string.Empty;
                    if (dt.Rows.Count > i)
                    {
                        cylinderCapacity = dt.Rows[i]["CylinderCapacity"].AsTargetType<string>("");
                    }

                    if (!string.IsNullOrWhiteSpace(cylinderCapacity))//有钢瓶
                    {
                        string status = dt.Rows[i]["Status"].AsTargetType<string>("");

                        string classByStatus = string.Empty; ;
                        //0	1	NULL	未抽空清洗
                        //1	1	NULL	已抽空清洗
                        //2	1	NULL	充装回收气
                        //3	1	NULL	已充待检气
                        //4	1	NULL	已充合格气
                        //5	1	NULL	待送检
                        //6	1	NULL	已报废

                        if (status.Equals("3"))
                        {
                            classByStatus = "circleyellow";
                            string isPass = dt.Rows[i]["IsPass"].AsTargetType<string>("");
                            if (isPass == "0")//已充待检气（而且不合格）
                            {
                                classByStatus = "circleColor" + 9;
                            }
                            else
                            {
                                classByStatus = "circleColor" + status;
                            }
                        }
                        else
                        {
                            classByStatus = "circleColor" + status;
                        }



                        sb.AppendFormat("<td><div class=\"circleyellow {2}\" id=\"c{0}\">{1}</div></td>", i + 1, cylinderCapacity + "KG", classByStatus);
                    }
                    else //空的
                    {
                        sb.AppendFormat("<td><div class=\"circlewhite\" id=\"c{0}\"></div></td>", i + 1);

                    }

                }
            }
            catch (Exception ex)
            {
                RM.Common.DotNetCode.LogInstanseHelper.Instance.WriteLog(ex.Message);
            }

            return rowhtml.Replace("#td#", sb.ToString());
        }
        private DataTable GetCarcaseInfoByPositionCode(string positionCode)
        {
            string sql = string.Format("SELECT * FROM dbo.CylinderCarcaseCode WHERE PositionCode='{0}'", positionCode);
            DataTable dt = EGMNGS.Common.ComServies.Query(sql);
            return dt;
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            if (this.txtCPCode.Text.Trim().Length > 0)
            {
                string sqlCarcaseCode = string.Format(@"SELECT 
                                                          [CarcaseCode]
                                                      FROM [EGMNGS].[dbo].[Cyl_Car_Code_Relations]
                                                       where CylinderCode='{0}'", this.txtCPCode.Text);

                LocationCarcaseCode = Maticsoft.DBUtility.DbHelperSQL.GetSingle(sqlCarcaseCode).AsTargetType<string>("");

                if (string.IsNullOrWhiteSpace(LocationCarcaseCode))
                {
                    ShowMsgHelper.Alert_Wern("钢瓶不存在");
                    return;
                }
                else
                {
                    this.ddlPositionCode.SelectedValue = LocationCarcaseCode.Substring(0, 4);
                }
               

            }

            DataBindGrid();
        }
    }


}
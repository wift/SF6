﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using RM.Common.DotNetUI;
using EGMNGS.Common;

namespace RM.Web.GanPing
{
    public partial class RecordGPClear : System.Web.UI.Page
    {
        public Dictionary<string, string> GPStatus
        {
            get
            {
                if (ViewState["GPStatus"] == null)
                {
                    Dictionary<string, string> GPStatus = new Dictionary<string, string>();
                    EGMNGS.BLL.SYS_CODE sysCodeBll = new EGMNGS.BLL.SYS_CODE();
                    DataSet dsGPStatus = sysCodeBll.GetList("CODE_TYPE='GPStatus' ORDER BY DISPLAY_ORDER");
                    foreach (DataRow item in dsGPStatus.Tables[0].Rows)
                    {
                        GPStatus.Add(item["CODE"].ToString(), item["CODE_CHI_DESC"].ToString());
                    }
                    ViewState["GPStatus"] = GPStatus;
                }

                return ViewState["GPStatus"] as Dictionary<string, string>;
            }
            set { ViewState["GPStatus"] = value; }
        }

        private DataTable UserList
        {
            get
            {
                return ViewState["UserList"] as DataTable;
            }
            set { ViewState["UserList"] = value; }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            this.PageControl1.pageHandler += new EventHandler(pager_PageChanged);
            UserList = ComServies.GetAllUserInfo();
            DataBindGrid();
        }
        /// <summary>
        /// 绑定数据源
        /// </summary>
        private void DataBindGrid()
        {
            string cylinderCode = Convert.ToString(Session["CylinderCode"]);
            string sqlWhere = string.Format("CylinderCode='{0}' order by ClearDate desc", cylinderCode);
            DataSet ds = ComServies.GetRecordClearView(sqlWhere);// new EGMNGS.BLL.BookGasFill().GetList(sqlWhere);// ComServies.GetListByPage(sqlStr, string.Empty, PageControl1.PageIndex, PageControl1.PageSize);
            ControlBindHelper.BindRepeaterList(ds.Tables[0], rp_Item);
            this.PageControl1.RecordCount = Convert.ToInt32(ds.Tables[0].Rows.Count);
        }

        /// <summary>
        /// 绑定数据，分页
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void pager_PageChanged(object sender, EventArgs e)
        {
            DataBindGrid();
        }

        /// <summary>
        /// 获取用户名
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public string GetUserName(string userId)
        {
            DataRow[] dr = UserList.Select(string.Format("User_ID='{0}'", userId));
            if (dr.Length > 0)
            {
                return dr[0][1].ToString();
            }
            return string.Empty;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using EGMNGS.Common;
using System.Text;
using RM.Common.DotNetUI;
using Microsoft.Office.Interop.Excel;
using RM.Common.DotNetFile;

namespace RM.Web.GanPing
{
    public partial class RtpYear : System.Web.UI.Page
    {
        public System.Data.DataTable dtRtpYear
        {
            get
            {
                if (ViewState["RtpYear"] == null)
                {
                    ViewState["RtpYear"] = new System.Data.DataTable();
                }

                return ViewState["RtpYear"] as System.Data.DataTable;
            }
            set { ViewState["RtpYear"] = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                DropDownListBinder();
            }
        }


        private void DropDownListBinder()
        {


            EGMNGS.BLL.SYS_CODE sysCodeBll = new EGMNGS.BLL.SYS_CODE();
            DataSet dsYear = sysCodeBll.GetList("CODE_TYPE='Year' ORDER BY DISPLAY_ORDER");

            this.ddlBeginYear.DataSource = dsYear.Tables[0];
            this.ddlBeginYear.DataTextField = "CODE_CHI_DESC";
            this.ddlBeginYear.DataValueField = "CODE";
            this.ddlBeginYear.DataBind();

            this.ddlBeginYear.SelectedValue = DateTime.Now.Year.ToString();

            this.ddlEndYear.DataSource = dsYear.Tables[0];
            this.ddlEndYear.DataTextField = "CODE_CHI_DESC";
            this.ddlEndYear.DataValueField = "CODE";
            this.ddlEndYear.DataBind();

            this.ddlEndYear.SelectedValue = DateTime.Now.Year.ToString();


            System.Data.DataTable dtPowerStation = ComServies.GetAllPowerStation();
            this.ddlPowerSupplyName.DataSource = dtPowerStation;
            this.ddlPowerSupplyName.DataTextField = "Organization_Name";
            this.ddlPowerSupplyName.DataValueField = "Organization_ID";
            this.ddlPowerSupplyName.DataBind();
            this.ddlPowerSupplyName.Items.Insert(0, string.Empty);
        }
        protected void ddlPowerSupplyName_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadPowerSupplyName();

        }
        private void LoadPowerSupplyName()
        {
            this.ddlConvertStationName.Items.Clear();
            EGMNGS.BLL.InfoPowerSupplyConvertStation bll = new EGMNGS.BLL.InfoPowerSupplyConvertStation();
            DataSet dsPowerSupplyCode = bll.GetList(string.Format("PowerSupplyCode='{0}'", this.ddlPowerSupplyName.SelectedValue));
            this.ddlConvertStationName.DataSource = dsPowerSupplyCode.Tables[0];
            this.ddlConvertStationName.DataTextField = "ConvartStationName";
            this.ddlConvertStationName.DataValueField = "CovnertStationCode";
            this.ddlConvertStationName.DataBind();
            this.ddlConvertStationName.Items.Insert(0, string.Empty);
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            string sql = @"SELECT '{0}' 年份,
 (SELECT ISNULL(SUM([AmountRecovery]),0)
    FROM [ApplRecyGasReg] WHERE [Year]='{0}' {1}) a,
    (SELECT ISNULL(sum(AirDemand),0.00) FROM ApplNeedGasReg WHERE [Year]='{0}' {1}) b,
    (SELECT ISNULL(sum(AmountOutStorage),0) FROM TableGasOutStorage WHERE GasClass=3 and [Year]='{0}' {1}) c,
    (SELECT ISNULL(sum(AmountFillGas),0) FROM InfoDevChangeFill WHERE YEAR(FillGasdate)='{0}' {1}) d";

            string sqlCount = @"SELECT '合计' 年份,
 (SELECT ISNULL(SUM([AmountRecovery]),0)
    FROM [ApplRecyGasReg] WHERE [Year] in ({0}) {1}) a,
    (SELECT ISNULL(sum(AirDemand),0.00) FROM ApplNeedGasReg WHERE [Year] in ({0}) {1}) b,
    (SELECT ISNULL(sum(AmountOutStorage),0) FROM TableGasOutStorage WHERE GasClass=3 and [Year] in ({0}) {1}) c,
    (SELECT ISNULL(sum(AmountFillGas),0) FROM InfoDevChangeFill WHERE YEAR(FillGasdate) in({0}) {1}) d";
            string where = string.Empty;

            if (ddlPowerSupplyName.SelectedIndex > 0)
            {
                where = string.Format(" and PowerSupplyName='{0}'", ddlPowerSupplyName.SelectedItem.Text);
                if (ddlConvertStationName.SelectedIndex > 0)
                {
                    where += string.Format(" and ConvertStationName='{0}'", ddlConvertStationName.SelectedItem.Text);
                }

            }

            StringBuilder sb = new StringBuilder();
            string yearStr = string.Empty;

            for (int i = Convert.ToInt32(this.ddlBeginYear.SelectedValue); i <= Convert.ToInt32(this.ddlEndYear.SelectedValue); i++)
            {
                sb.AppendFormat(sql, i, where);
                sb.AppendLine();
                yearStr += "'" + i.ToString() + "'";
                if (i < Convert.ToInt32(this.ddlEndYear.SelectedValue))
                {
                    yearStr += ",";
                }
                sb.Append(" UNION ");
            }

            sb.AppendFormat(sqlCount, yearStr, where);

            dtRtpYear = ComServies.Query(sb.ToString());
            ControlBindHelper.BindRepeaterList(dtRtpYear, rp_Item);
        }


        /// <summary>
        /// 导出
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnExport_Click(object sender, EventArgs e)
        {

            MouldExportExcel mee = new MouldExportExcel();
            try
            {
                string physicalPath = Request.PhysicalApplicationPath + "Xls\\";
                Workbook sh = mee.OpenExcel(physicalPath + "年度统计表.xls");
                Worksheet ws = sh.Worksheets[1] as Worksheet;

                #region 设置值

                mee.SetCellValue(ws, 2, 2, this.ddlBeginYear.SelectedValue + "年至" + this.ddlEndYear.SelectedValue + "年");
                mee.SetCellValue(ws, 2, 4, this.ddlPowerSupplyName.SelectedItem.Text);
                mee.SetCellValue(ws, 2, 6, this.ddlConvertStationName.SelectedItem.Text);



                for (int i = 0; i < dtRtpYear.Rows.Count; i++)
                {
                    DataRow item = dtRtpYear.Rows[i];
                    int rowIndex = 4 + i;
                    mee.SetCellValue(ws, rowIndex, 1, item["年份"].ToString());
                    mee.SetCellValue(ws, rowIndex, 2, item["a"].ToString());
                    mee.SetCellValue(ws, rowIndex, 3, item["b"].ToString());
                    mee.SetCellValue(ws, rowIndex, 4, item["c"].ToString());
                    mee.SetCellValue(ws, rowIndex, 5, item["d"].ToString());
                    mee.SetMerge(ws, rowIndex, 5, rowIndex, 6);
                    mee.SetBorderLine(ws, rowIndex, 1, rowIndex, 6);

                }

                #endregion

                string fileName = "年度统计表temp.xls";
                string pathFile = physicalPath + fileName;

                mee.SaveExcel(pathFile);

                FileDownHelper.DownLoadold(@"~/Xls/" + fileName);

            }
            catch (Exception ex)
            {
                mee.Dispose();
                throw ex;
            }
            finally
            {
                mee.Dispose();

            }
        }

    }
}
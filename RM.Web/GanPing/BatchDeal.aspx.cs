﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using RM.Common.DotNetUI;
using RM.Web.App_Code;
using Maticsoft.DBUtility;
using EGMNGS.Model;

namespace RM.Web.GanPing
{
    public partial class BatchDeal : PageBase
    {
        private EGMNGS.Model.RegGasBatch RegGasBatchObj
        {
            get
            {
                if (Session["RegGasBatch"] == null)
                {
                    return new EGMNGS.Model.RegGasBatch();
                }
                return Session["RegGasBatch"] as EGMNGS.Model.RegGasBatch;
            }
            set { Session["RegGasBatch"] = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnBatchDeal_Click(object sender, EventArgs e)
        {
            string batchCode = Request["BatchCode"] ?? string.Empty;

            if (batchCode == string.Empty)
            {
                return;
            }


            string noexistsCode = "";

            List<CommandInfo> lc = new List<CommandInfo>();
            int collback = 0;
            int sum = 0;
            for (int i = 1; i < 9; i++)
            {
                TextBox txtCylinderCode = this.form1.FindControl("txtCylinderCode" + i.ToString()) as TextBox;
                TextBox txtGasAmount = this.form1.FindControl("txtGasAmount" + i.ToString()) as TextBox;

                if (txtCylinderCode.Text.Length == 0)
                {
                    continue;
                }
                else
                {
                    var mode = new EGMNGS.BLL.BookCylinderInfo().GetModelByCylinderCode(txtCylinderCode.Text);
                    if (mode == null)
                    {
                        noexistsCode += txtCylinderCode.Text + ",";
                        continue;
                    }
                }
                if (txtCylinderCode.Text.Length > 0 && txtGasAmount.Text.Length == 0)
                {
                    ShowMsgHelper.showWarningMsg(string.Format("气量{0}不能为空!", i.ToString()));
                    return;
                }

                int tempCC;
                if (VerifyRL(Convert.ToInt32(txtGasAmount.Text), txtCylinderCode.Text, out tempCC) == false)
                {
                    ShowMsgHelper.showWarningMsg(string.Format("气量{0}大于钢瓶{0}的容量{1}", i.ToString(), tempCC));
                    return;
                }

                sum += Convert.ToInt32(txtGasAmount.Text);
                if (RegGasBatchObj.AmountGas < sum)
                {
                    ShowMsgHelper.showWarningMsg("气量已经超过批次气量！");
                    return;
                }
                collback++;
                EGMNGS.Model.CylinderCleanTable cylinderCleanTb = new EGMNGS.Model.CylinderCleanTable();
                cylinderCleanTb.CylinderCode = txtCylinderCode.Text;
                cylinderCleanTb.Status = "1";
                cylinderCleanTb.ClearDate = DateTime.Now;
                cylinderCleanTb.ClearManOID = User.UserId.ToString();
                EGMNGS.BLL.CylinderCleanTable bllCylinderCleanTable = new EGMNGS.BLL.CylinderCleanTable();

                CommandInfo commandInfoCylinderCleanTable = bllCylinderCleanTable.AddCommandInfo(cylinderCleanTb);
                lc.Add(commandInfoCylinderCleanTable);

                string actionMode = "Add";
                BookGasFill Model = new EGMNGS.Model.BookGasFill(actionMode);
                Model.RegistrantDate = DateTime.Now;
                Model.RegistrantOID = User.UserId.ToString();
                Model.Status = "1";
                Model.GasType = "4";
                Model.GasStatus = "0";
                Model.CylinderCode = txtCylinderCode.Text;
                Model.AmountGas = Convert.ToDecimal(txtGasAmount.Text);
                Model.CylinderStatus = "3";
                Model.BusinessCode = batchCode;

                EGMNGS.BLL.BookGasFill bllBookGasFill = new EGMNGS.BLL.BookGasFill();
                CommandInfo bookGasFillCommandInfo = bllBookGasFill.AddCommandInfo(Model);
                lc.Add(bookGasFillCommandInfo);
                //  lc.Add(new CommandInfo(string.Format("UPDATE dbo.CodeTypeTb SET MaxNum=MaxNum+1 WHERE CodeType='{0}';", Model.FillGasCode.Substring(0, 2)), new System.Data.SqlClient.SqlParameter[0]));
                //  lc.Add(new CommandInfo(string.Format("UPDATE dbo.CodeTypeTb SET MaxNum=MaxNum+1 WHERE CodeType='{0}';", Model.GasCode.Substring(0, 2)), new System.Data.SqlClient.SqlParameter[0]));
                DbHelperSQL.ExecuteSql(string.Format("UPDATE dbo.CodeTypeTb SET MaxNum=MaxNum+1 WHERE CodeType='CQ';"));
                DbHelperSQL.ExecuteSql(string.Format("UPDATE dbo.CodeTypeTb SET MaxNum=MaxNum+1 WHERE CodeType='QT';"));
                // lc.Add(new CommandInfo(string.Format("UPDATE BookGasFill SET Status='{0}',CylinderStatus={1} WHERE CylinderCode='{2}';", "1", "3", Model.CylinderCode), new System.Data.SqlClient.SqlParameter[0]));
                lc.Add(new CommandInfo(string.Format("UPDATE BookCylinderInfo SET Status='{0}',CurGasCode='{1}' WHERE CylinderCode='{2}';", "3", Model.GasCode, Model.CylinderCode), new System.Data.SqlClient.SqlParameter[0]));

            }
            try
            {
                string msgstr = "";
                
                if (!string.IsNullOrWhiteSpace(noexistsCode))
                {
                    msgstr = string.Format("钢瓶：{0}不存在!", noexistsCode);
                }

                DbHelperSQL.ExecuteSqlTranWithIndentity(lc);

                ShowMsgHelper.Alert("批量处理成功！" + msgstr);
             
                ShowMsgHelper.ShowScript("artDialog.close();");

            }
            catch (Exception ex)
            {
                DbHelperSQL.ExecuteSql(string.Format("UPDATE dbo.CodeTypeTb SET MaxNum=MaxNum-{0} WHERE CodeType='CQ';", collback));
                DbHelperSQL.ExecuteSql(string.Format("UPDATE dbo.CodeTypeTb SET MaxNum=MaxNum-{0} WHERE CodeType='QT';", collback));
                ShowMsgHelper.Alert("批量处理失败！");

                throw ex;
            }
        }

        private bool VerifyRL(int txtGasAmount, string txtCylinderCode, out int cy)
        {
            bool isYes = true;
            var mode = new EGMNGS.BLL.BookCylinderInfo().GetModelByCylinderCode(txtCylinderCode);
            int cylinderCapacity = Convert.ToInt32(DictEGMNS["GPRL"][mode.CylinderCapacity]);

            if (txtGasAmount > cylinderCapacity)
            {
                isYes = false;
            }
            cy = cylinderCapacity;
            return isYes;
        }


    }
}
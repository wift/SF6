﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using RM.Common.DotNetCode;
using System.Data;
using RM.Common.DotNetUI;
using RM.Web.App_Code;
using EGMNGS.Common;

namespace RM.Web.GanPing
{
    public partial class BookGasStoragePage : PageBase
    {
        #region 属性字段

        EGMNGS.BLL.BookGasStorage bll = new EGMNGS.BLL.BookGasStorage();
        private DataTable UserList
        {
            get
            {
                return ViewState["UserList"] as DataTable;
            }
            set { ViewState["UserList"] = value; }
        }
        private EGMNGS.Model.BookGasStorage Model
        {
            get
            {
                return ViewState["BookGasStorage"] as EGMNGS.Model.BookGasStorage;
            }
            set { ViewState["BookGasStorage"] = value; }
        }
        private string actionMode
        {
            get { return ViewState["ActionMode"] as String; }
            set { ViewState["ActionMode"] = value; }
        }
        public string PowerSupplyCode
        {
            get { return ViewState["PowerSupplyCode"] as String; }
            set { ViewState["PowerSupplyCode"] = value; }
        }
        public string PowerSupplyName
        {
            get { return ViewState["PowerSupplyName"] as String; }
            set { ViewState["PowerSupplyName"] = value; }
        }

        #endregion

        #region 事件方法

        protected void Page_Load(object sender, EventArgs e)
        {
            this.PageControl1.pageHandler += new EventHandler(pager_PageChanged);
            if (!IsPostBack)
            {
                UserList = ComServies.GetAllUserInfo();
                //string[] orgIdOrgName = ComServies.GetPowerApplyByUserID(User.UserId.ToString());
                //PowerSupplyCode = orgIdOrgName[0];
                //PowerSupplyName = orgIdOrgName[1];
                DropDownListBinder();

                this.ddlMonth.SelectedValue = DateTime.Now.Month.ToString();
                this.ddlYear.SelectedValue = DateTime.Now.Year.ToString();
            }
        }

        /// <summary>
        /// 绑定下拉列
        /// </summary>
        private void DropDownListBinder()
        {

            EGMNGS.BLL.SYS_CODE sysCodeBll = new EGMNGS.BLL.SYS_CODE();
            DataSet dsYear = sysCodeBll.GetList("CODE_TYPE='Year' ORDER BY DISPLAY_ORDER");

            this.ddlYear.DataSource = dsYear.Tables[0];
            this.ddlYear.DataTextField = "CODE_CHI_DESC";
            this.ddlYear.DataValueField = "CODE";
            this.ddlYear.DataBind();

            DataSet dsMonth = sysCodeBll.GetList("CODE_TYPE='Month' ORDER BY DISPLAY_ORDER");

            this.ddlMonth.DataSource = dsMonth.Tables[0];
            this.ddlMonth.DataTextField = "CODE_CHI_DESC";
            this.ddlMonth.DataValueField = "CODE";
            this.ddlMonth.DataBind();
        }


        /// <summary>
        /// 绑定后激发事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void rp_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Label lblUserID = e.Item.FindControl("lblUserID") as Label;
                //    Label lblAuditorOID = e.Item.FindControl("lblAuditorOID") as Label;

                if (lblUserID != null)
                {
                    lblUserID.Text = GetUserName(lblUserID.Text);
                }


            }
        }

        /// <summary>
        /// 绑定数据，分页
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void pager_PageChanged(object sender, EventArgs e)
        {
            DataBindGrid();
        }

        /// <summary>
        /// 绑定数据源
        /// </summary>
        private void DataBindGrid()
        {
            int count = bll.GetRecordCount("1=1");
            DataSet ds = bll.GetListByPage("1=1", string.Empty, PageControl1.PageIndex, PageControl1.PageSize);
            ControlBindHelper.BindRepeaterList(ds.Tables[0], rp_Item);
            this.PageControl1.RecordCount = Convert.ToInt32(count);
        }

        /// <summary>
        /// 绑定数据到对象
        /// </summary>
        private void DataBinder()
        {
            Model.Year = this.ddlYear.SelectedValue;
            Model.Month = this.ddlMonth.SelectedValue;
            if (this.txtCountPassGas.Text.Trim().Length > 0)
            {
                Model.CountPassGas = Convert.ToDecimal(this.txtCountPassGas.Text);
            }
            if (this.txtCountRecycleGas.Text.Trim().Length > 0)
            {
                Model.CountRecycleGas = Convert.ToDecimal(this.txtCountRecycleGas.Text);
            }
            if (this.txtCountWaitingGas.Text.Trim().Length > 0)
            {
                Model.CountWaitingGas = Convert.ToDecimal(this.txtCountWaitingGas.Text);
            }

        }

        /// <summary>
        /// 初始化控件
        /// </summary>
        private void InitControl()
        {
            this.ddlMonth.SelectedValue = Model.Month;
            this.ddlYear.SelectedValue = Model.Year;
            this.txtCountWaitingGas.Text = Convert.ToString(Model.CountWaitingGas);
            this.txtCountRecycleGas.Text = Convert.ToString(Model.CountRecycleGas);
            this.txtCountPassGas.Text = Convert.ToString(Model.CountPassGas);
        }

        /// <summary>
        /// 获取用户名
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        private string GetUserName(string userId)
        {
            DataRow[] dr = UserList.Select(string.Format("User_ID='{0}'", userId));
            if (dr.Length > 0)
            {
                return dr[0][1].ToString();
            }
            return string.Empty;
        }
        #endregion

        #region 按钮事件

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            Model = new EGMNGS.Model.BookGasStorage();

            DataBinder();

            //Model.CountPassGas = decimal.Parse("97.99");
            //Model.CountRecycleGas = decimal.Parse("98.99");
            //Model.CountWaitingGas = decimal.Parse("99.99");
            Model.CheckOID = User.UserId.ToString();
            Model.CheckDate = DateTime.Now;

            if (bll.Add(Model) > 0)
            {
                actionMode = string.Empty;
            }

            DataBindGrid();
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            DataBindGrid();
        }

        /// <summary>
        /// 获取当前对象
        /// </summary>
        private void ShowMaxMode()
        {
            Model = bll.GetModel(bll.GetMaxId() - 1);
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (actionMode == "Add")
                {
                    DataBinder();
                    //  Model.Month = (Convert.ToInt32(Model.Month) - 1).ToString();
                    int count = bll.GetRecordCount(string.Format(@"[YEAR]={0} and [Month]={1}", Model.Year, (Convert.ToInt32(Model.Month)).ToString()));
                    if (count > 0)
                    {
                        ShowMsgHelper.Alert_Wern("已经存在对账库存！");
                        return;
                    }

                    if (bll.Add(Model) > 0)
                    {
                        actionMode = string.Empty;
                    }

                    DataBindGrid();
                    ShowMaxMode();
                }
                else if (actionMode == "Edit")
                {
                    DataBinder();
                    bll.Update(Model);
                    DataBindGrid();

                }
            }
            catch (ArgumentOutOfRangeException ex)
            {
                string[] errs = CommonHelper.GetValue(ex.Message, "@").Split(':');
                ShowMsgHelper.ExecuteScript(string.Format("ChangeCss($('#txt{0}'),'长度不能超{1}字')", errs[0], errs[1]));
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            actionMode = "Add";
            Model = new EGMNGS.Model.BookGasStorage();
            DataBinder();
            Model.CheckStatus = "0";

            Model.CountRecycleGas = ComServies.GetRecycleGasTotal(Model.Year, (Convert.ToInt32(Model.Month)).ToString());
            Model.CountPassGas = ComServies.GetPassGassTotal(Model.Year, (Convert.ToInt32(Model.Month)).ToString());
            Model.CountWaitingGas = ComServies.GetWaitingGasTotal(Model.Year, (Convert.ToInt32(Model.Month)).ToString());

            InitControl();
            this.ddlMonth.Enabled = true;
            this.ddlYear.Enabled = true;

        }


        //protected void btnDel_Click(object sender, EventArgs e)
        //{
        //    bll.Delete(Model.OID);
        //    Model = new EGMNGS.Model.BookGasStorage();
        //    InitControl();
        //    DataBindGrid();
        //}

        protected void rp_Item_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            int oid = Convert.ToInt32(e.CommandArgument);

            Model = bll.GetModel(oid);

            actionMode = "Edit";
            InitControl();
            this.ddlMonth.Enabled = false;
            this.ddlYear.Enabled = false;
        }
        #endregion

        /// <summary>
        /// 对账复核
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnReCheck_Click(object sender, EventArgs e)
        {
            Model.CheckStatus = "1";
            Model.CheckOID = User.UserId.ToString();
            if (bll.Update(Model))
            {
                ShowMsgHelper.Alert("对账复核成功！");
                DataBindGrid();
            }
        }
        /// <summary>
        /// 计算库存
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCalculate_Click(object sender, EventArgs e)
        {
            int year = Convert.ToInt32(Model.Year);
            int month = int.Parse(Model.Month) - 1;
            if (Convert.ToInt32(Model.Month) == 1)
            {
                year = int.Parse(Model.Year) - 1;
                month = 12;
                //Model.CountRecycleGas = ComServies.GetRecycleGasTotal(Model.Year, Model.Month) + bll.GetModelList(string.Format(@"Year='{0}' and Month='{1}'", year, month))[0].CountRecycleGas;
                //Model.CountPassGas = ComServies.GetPassGassTotal(Model.Year, Model.Month) + bll.GetModelList(string.Format(@"Year='{0}' and Month='{1}'", year, month))[0].CountPassGas;
                //Model.CountWaitingGas = ComServies.GetWaitingGasTotal(Model.Year, Model.Month) + bll.GetModelList(string.Format(@"Year='{0}' and Month='{1}'", year, month))[0].CountWaitingGas;
            }
            //else
            //{
            //    Model.CountRecycleGas = ComServies.GetRecycleGasTotal(Model.Year, Model.Month) + bll.GetModelList(string.Format(@"Year='{0}' and Month='{1}'", year, month))[0].CountRecycleGas;
            //    Model.CountPassGas = ComServies.GetPassGassTotal(Model.Year, Model.Month) + bll.GetModelList(string.Format(@"Year='{0}' and Month='{1}'", year, month))[0].CountPassGas;
            //    Model.CountWaitingGas = ComServies.GetWaitingGasTotal(Model.Year, Model.Month) + bll.GetModelList(string.Format(@"Year='{0}' and Month='{1}'", year, month))[0].CountWaitingGas;
            //}

            DataTable dt = ComServies.GetNotSumitedRecord(year, Convert.ToInt32(Model.Month));
            if (dt.Rows.Count > 0)
            {
                List<string> lss = new List<string>();
                foreach (DataRow item in dt.Rows)
                {
                    lss.Add(item[0].ToString());
                }

                ShowMsgHelper.Alert(string.Format("{0}还没提交", string.Join(",", lss.ToArray())));
                return;
            }
            Model.CountRecycleGas = ComServies.GetRecycleGasTotal(Model.Year, Model.Month) + bll.GetModelList(string.Format(@"Year='{0}' and Month='{1}'", year, month))[0].CountRecycleGas;
            Model.CountPassGas = ComServies.GetPassGassTotal(Model.Year, Model.Month) + bll.GetModelList(string.Format(@"Year='{0}' and Month='{1}'", year, month))[0].CountPassGas;
            Model.CountWaitingGas = ComServies.GetWaitingGasTotal(Model.Year, Model.Month) + bll.GetModelList(string.Format(@"Year='{0}' and Month='{1}'", year, month))[0].CountWaitingGas;
            bll.Update(Model);
            InitControl();
        }
        /// <summary>
        /// 取消对账复核
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCancelRecheck_Click(object sender, EventArgs e)
        {
            Model.CheckStatus = "0";
            if (bll.Update(Model))
            {
                ShowMsgHelper.Alert("取消对账复核成功！");
                DataBindGrid();
            }
        }
    }
}
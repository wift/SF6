﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EGMNGS.Model;
using System.Data;
using RM.Common.DotNetUI;
using RM.Web.App_Code;
using System.Globalization;
using RM.Common.DotNetCode;

namespace RM.Web.GanPing
{
    public partial class CalculateIndexPopuPage : PageBase
    {
        EGMNGS.BLL.ApplRegGasQtyInspectionValue bll = new EGMNGS.BLL.ApplRegGasQtyInspectionValue();

        private EGMNGS.Model.ApplRegGasQtyInspection Model
        {
            get
            {
                if (Session["ApplRegGasQtyInspection"] == null)
                {
                    return new EGMNGS.Model.ApplRegGasQtyInspection();
                }
                return Session["ApplRegGasQtyInspection"] as EGMNGS.Model.ApplRegGasQtyInspection;
            }
            set { Session["ApplRegGasQtyInspection"] = value; }
        }
        public List<string> listRole
        {
            get { return Session["listRole"] as List<string>; }
            set { Session["listRole"] = value; }
        }
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                DropDownListBinder();
                IList<ApplRegGasQtyInspectionValue> lt = bll.GetModelList("1=1");
                if (lt.Count > 0)
                {

                    ShowDataToControl(lt[0]);
                }
                InitControl();
                // object dd = Model;
                string key = Model.CheckCode;
                if (!string.IsNullOrEmpty(key))
                {
                    DataTable dt = EGMNGS.Common.ComServies.getApplRegGasQtyInspectionIndex(key);

                    foreach (DataRow item in dt.Rows)
                    {
                        this.txtsfhl.Value = Convert.ToString(item["sfhl"]);
                        this.txtsxsp.Value = Convert.ToString(item["sxsp"]);
                        this.txtcytj.Value = Convert.ToString(item["cytj"]);
                        this.txtclds.Value = Convert.ToString(item["clds"]);
                        this.txtcql.Value = Convert.ToString(item["cql"]);

                        this.txtcxsp.Value = Convert.ToString(item["cxsp"]);
                        this.txtlsnd.Value = Convert.ToString(item["lsnd"]);
                        this.txtnd.Value = Convert.ToString(item["nd"]);
                        this.txtwxsp.Value = Convert.ToString(item["wxsp"]);
                        this.txtKWYCQL.Value = Convert.ToString(item["KWYCQL"]);


                    }
                }

            }
        }

        /// <summary>
        /// 绑定下拉列
        /// </summary>
        private void DropDownListBinder()
        {

            EGMNGS.BLL.SYS_CODE sysCodeBll = new EGMNGS.BLL.SYS_CODE();
            DataSet dsGasSourse = sysCodeBll.GetList("CODE_TYPE='GasSourse' ORDER BY DISPLAY_ORDER");

            this.ddlGasSourse.DataSource = dsGasSourse.Tables[0];
            this.ddlGasSourse.DataTextField = "CODE_CHI_DESC";
            this.ddlGasSourse.DataValueField = "CODE";
            this.ddlGasSourse.DataBind();

            DataSet dsIsPass = sysCodeBll.GetList("CODE_TYPE='IsPass' ORDER BY DISPLAY_ORDER");

            this.ddlIsPass.DataSource = dsIsPass.Tables[0];
            this.ddlIsPass.DataTextField = "CODE_CHI_DESC";
            this.ddlIsPass.DataValueField = "CODE";
            this.ddlIsPass.DataBind();
            //  this.ddlIsPass.Items.Insert(0, string.Empty);

            DataSet dsSPYID = new EGMNGS.BLL.BookDetectionTools().GetList("ToolType=1  and EffectiveDate>=GETDATE() order by LAST_UPD_BY");

            this.ddlSPYID.DataSource = dsSPYID.Tables[0];
            this.ddlSPYID.DataTextField = "sfe";
            this.ddlSPYID.DataValueField = "ToolCode";
            this.ddlSPYID.DataBind();

            this.ddlSPYID.SelectedIndex = -1;

            DataSet dsLDYID = new EGMNGS.BLL.BookDetectionTools().GetList("ToolType=2 and EffectiveDate>=GETDATE() order by LAST_UPD_BY");

            this.ddlLDYID.DataSource = dsLDYID.Tables[0];
            this.ddlLDYID.DataTextField = "sfe";
            this.ddlLDYID.DataValueField = "ToolCode";
            this.ddlLDYID.DataBind();
            // this.ddlLDYID.Items.Insert(0, string.Empty);



            DataSet dsWSJID = new EGMNGS.BLL.BookDetectionTools().GetList("ToolType=3 and EffectiveDate>=GETDATE() order by LAST_UPD_BY ");

            this.ddlWSJID.DataSource = dsWSJID.Tables[0];
            this.ddlWSJID.DataTextField = "sfe";
            this.ddlWSJID.DataValueField = "ToolCode";
            this.ddlWSJID.DataBind();


            //  this.ddlWSJID.Items.Insert(0, string.Empty);

            DataSet dsQYJID = new EGMNGS.BLL.BookDetectionTools().GetList("ToolType=4 and EffectiveDate>=GETDATE() order by LAST_UPD_BY ");

            this.ddlQYJID.DataSource = dsQYJID.Tables[0];
            this.ddlQYJID.DataTextField = "sfe";
            this.ddlQYJID.DataValueField = "ToolCode";
            this.ddlQYJID.DataBind();
            // this.ddlQYJID.Items.Insert(0, string.Empty);


            DataSet dsSDJID = new EGMNGS.BLL.BookDetectionTools().GetList("ToolType=5 and EffectiveDate>=GETDATE() order by LAST_UPD_BY ");

            this.ddlSDJID.DataSource = dsSDJID.Tables[0];
            this.ddlSDJID.DataTextField = "sfe";
            this.ddlSDJID.DataValueField = "ToolCode";
            this.ddlSDJID.DataBind();
            // this.ddlSDJID.Items.Insert(0, string.Empty);

            DataSet dsFGGDJID = new EGMNGS.BLL.BookDetectionTools().GetList("ToolType=6 and EffectiveDate>=GETDATE() order by LAST_UPD_BY ");

            this.ddlFGGDJID.DataSource = dsFGGDJID.Tables[0];
            this.ddlFGGDJID.DataTextField = "sfe";
            this.ddlFGGDJID.DataValueField = "ToolCode";
            this.ddlFGGDJID.DataBind();
            // this.ddlFGGDJID.Items.Insert(0, string.Empty);


            DataSet dsHWHYFXYID = new EGMNGS.BLL.BookDetectionTools().GetList("ToolType=7 and EffectiveDate>=GETDATE() order by LAST_UPD_BY ");

            this.ddlHWHYFXYID.DataSource = dsHWHYFXYID.Tables[0];
            this.ddlHWHYFXYID.DataTextField = "sfe";
            this.ddlHWHYFXYID.DataValueField = "ToolCode";
            this.ddlHWHYFXYID.DataBind();
            //this.ddlHWHYFXYID.Items.Insert(0, string.Empty);

            DataSet dtCheckPJType = sysCodeBll.GetList("CODE_TYPE='CheckPJTypeStatus' ORDER BY DISPLAY_ORDER");

            this.ddlCheckPJType.DataSource = dtCheckPJType.Tables[0];
            this.ddlCheckPJType.DataTextField = "CODE_CHI_DESC";
            this.ddlCheckPJType.DataValueField = "CODE";
            this.ddlCheckPJType.DataBind();

        }

        /// <summary>
        /// 初始化控件
        /// </summary>
        private void InitControl()
        {

            // this.txtCheckCode.Value = Model.CheckCode;
            // this.txtReportCode.Value = Model.ReportCode;
            this.txtBusinessCode.Value = Model.BusinessCode;
            // this.txtGasCdoe.Value = Model.GasCdoe;
            this.txtCylinderCode.Value = Model.CylinderCode;
            this.ddlGasSourse.SelectedValue = Model.GasSourse;
            this.txtCylinderSeaNo.Value = new EGMNGS.BLL.BookCylinderInfo().GetModelByCylinderCode(Model.CylinderCode).CylinderSealNo;
            //  this.txtC.Value = Model.C.ToString();
            //  this.txtRH.Value = Model.RH.ToString();
            //  this.txtKPA.Value = Model.KPA.ToString();
            this.txtCheckDate.Text = Model.CheckDate.HasValue ? Model.CheckDate.Value.ToString("yyyy-MM-dd") : DateTime.Now.ToString("yyyy-MM-dd");

           

            if (!string.IsNullOrEmpty(Model.SPYID) && this.ddlSPYID.Items.FindByValue(Model.SPYID) != null)
            {
                this.ddlSPYID.SelectedValue = Model.SPYID;
            }
            else if (Model.GasSourse == "2")
            {
                this.ddlSPYID.SelectedIndex = this.ddlSPYID.Items.Count >= 2 ? 1 : -1;
            }
            else
            {
                this.ddlSPYID.SelectedIndex = -1;
            }

            if (!string.IsNullOrEmpty(Model.LDYID) && this.ddlLDYID.Items.FindByValue(Model.LDYID) != null)
            {
                this.ddlLDYID.SelectedValue = Model.LDYID;
            }
            else if (Model.GasSourse == "2")
            {
                this.ddlLDYID.SelectedIndex = this.ddlLDYID.Items.Count >= 2 ? 1 : -1;
            }
            else
            {
                this.ddlLDYID.SelectedIndex = -1;
            }

            if (!string.IsNullOrEmpty(Model.WSJID) && this.ddlWSJID.Items.FindByValue(Model.WSJID) != null)
            {
                this.ddlWSJID.SelectedValue = Model.WSJID;
            }
            else if (Model.GasSourse == "2")
            {
                this.ddlWSJID.SelectedIndex = this.ddlWSJID.Items.Count >= 2 ? 1 : -1;
            }
            else
            {
                this.ddlWSJID.SelectedIndex = -1;
            }

            if (!string.IsNullOrEmpty(Model.QYJID) && this.ddlQYJID.Items.FindByValue(Model.QYJID) != null)
            {
                this.ddlQYJID.SelectedValue = Model.QYJID;
            }
            else if (Model.GasSourse == "2")
            {
                this.ddlQYJID.SelectedIndex = this.ddlQYJID.Items.Count >= 2 ? 1 : -1;
            }
            else
            {
                this.ddlQYJID.SelectedIndex = -1;
            }

            if (!string.IsNullOrEmpty(Model.SDJID) && this.ddlSDJID.Items.FindByValue(Model.SDJID) != null)
            {
                this.ddlSDJID.SelectedValue = Model.SDJID;
            }
            else if (Model.GasSourse == "2")
            {
                this.ddlSDJID.SelectedIndex = this.ddlSDJID.Items.Count >= 2 ? 1 : -1;
            }
            else
            {
                this.ddlSDJID.SelectedIndex = -1;
            }

            if (!string.IsNullOrEmpty(Model.FGGDJID) && this.ddlFGGDJID.Items.FindByValue(Model.FGGDJID) != null)
            {
                this.ddlFGGDJID.SelectedValue = Model.FGGDJID;
            }
            else if (Model.GasSourse == "2")
            {
                this.ddlFGGDJID.SelectedIndex = this.ddlFGGDJID.Items.Count >= 2 ? 1 : -1;
            }
            else
            {
                this.ddlFGGDJID.SelectedIndex = -1;
            }

            if (!string.IsNullOrEmpty(Model.HWHYFXYID) && this.ddlHWHYFXYID.Items.FindByValue(Model.HWHYFXYID) != null)
            {
                this.ddlHWHYFXYID.SelectedValue = Model.HWHYFXYID;
            }
            else if (Model.GasSourse == "2")
            {
                this.ddlHWHYFXYID.SelectedIndex = this.ddlHWHYFXYID.Items.Count >= 2 ? 1 : -1;
            }
            else
            {
                this.ddlHWHYFXYID.SelectedIndex = -1;
            }

            this.txtSF6Result.Value = Convert.ToString(Model.SF6Result);
            this.txtAirQualityScoreResult.Value = Convert.ToString(Model.AirQualityScoreResult);
            this.txtCF4Result.Value = Convert.ToString(Model.CF4Result);
            this.txtWaterQualityScoreResult.Value = Convert.ToString(Model.WaterQualityScoreResult);
            this.txtWaterDewResult.Value = Convert.ToString(Model.WaterDewResult);
            this.txtHFResult.Value = Convert.ToString(Model.HFResult);
            this.txtKSJHFResult.Value = Convert.ToString(Model.KSJHFResult);
            this.txtKWYSorceResult.Value = Convert.ToString(Model.KWYSorceResult);

            if (!string.IsNullOrEmpty(Model.Conclusion))
            {
                this.txtConclusion.Value = Model.Conclusion;
            }

            this.ddlIsPass.SelectedValue = Model.IsPass;


            this.ddlCheckPJType.SelectedValue = Model.CheckPJType;
            this.txtMonoxide.Value = Model.Monoxide.ToString();
            this.txtSulfurDioxide.Value = Model.SulfurDioxide.ToString();
            this.txthydrothion.Value = Model.Hydrothion.ToString();

            txtC2F6.Value = Model.C2F6.ToString();
            txtC3F8.Value = Model.C3F8.ToString();

            txtReportCode.Value = Model.ReportCode;
            txtStandardVolume.Value = Model.StandardVolume.AsTargetType<string>("");
        }

        private void ShowDataToControl(ApplRegGasQtyInspectionValue model)
        {
            if (Model.C == null)
            {
                this.txtEnvironmentTemperature.Value = model.EnvironmentTemperature.ToString();
            }
            else
            {
                this.txtEnvironmentTemperature.Value = Model.C.ToString();
            }

            if (Model.RH == null)
            {
                this.txtEnvironmentHumidity.Value = model.EnvironmentHumidity.ToString();
            }
            else
            {
                this.txtEnvironmentHumidity.Value = Model.RH.ToString();
            }

            if (Model.KPA == null)
            {
                this.txtBarometricPressure.Value = model.BarometricPressure.ToString();
            }
            else
            {
                this.txtBarometricPressure.Value = Model.KPA.ToString();
            }



        }


        protected void btnSave3_Click(object sender, EventArgs e)
        {
            IList<ApplRegGasQtyInspectionValue> lt = bll.GetModelList("1=1");
            if (lt.Count > 0)
            {
                lt[0].BarometricPressure = Convert.ToDecimal(this.txtBarometricPressure.Value);
                lt[0].EnvironmentHumidity = Convert.ToDecimal(this.txtEnvironmentHumidity.Value);
                lt[0].EnvironmentTemperature = Convert.ToDecimal(this.txtEnvironmentTemperature.Value);
                bll.Update(lt[0]);
            }
        }

        /// <summary>
        /// 计算保存
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {


                string key = Model.CheckCode;
                if (!string.IsNullOrEmpty(key))
                {
                    //decimal txtHFResult, KSJHFResult, KWYSorceResult, WaterQualityScoreResult;

                    decimal txtHFResult = 0.0M;
                    decimal KSJHFResult = 0.0M;
                    decimal KWYSorceResult = 0.0M;
                    decimal WaterQualityScoreResult = 0.0M;
                    decimal p_StandardVolume = 0.0M;

                    CalcuFunction(out txtHFResult, out KSJHFResult, out KWYSorceResult, out WaterQualityScoreResult);
                    CalcuFunctionStandardVolume(out p_StandardVolume);

                    int returnValue = EGMNGS.Common.ComServies.UpdateApplRegGasQtyInspectionIndex(key, this.txtlsnd.Value, this.txtwxsp.Value, this.txtsxsp.Value, this.txtcxsp.Value, this.txtcql.Value, this.txtclds.Value, this.txtnd.Value, this.txtcytj.Value, this.txtsfhl.Value, txtKWYCQL.Value, txtHFResult, KSJHFResult, KWYSorceResult, WaterQualityScoreResult, txtEnvironmentTemperature.Value, txtEnvironmentHumidity.Value, txtBarometricPressure.Value, txtMonoxide.Value, txthydrothion.Value, txtSulfurDioxide.Value, p_StandardVolume);
                    if (returnValue > 0)
                    {
                        EGMNGS.BLL.ApplRegGasQtyInspectionValue bllApplRegGasQtyInspectionValue = new EGMNGS.BLL.ApplRegGasQtyInspectionValue();
                        EGMNGS.Model.ApplRegGasQtyInspectionValue tempInspectionValueObj = bllApplRegGasQtyInspectionValue.GetModelList("1=1")[0];

                        tempInspectionValueObj.EnvironmentTemperature = decimal.Parse(this.txtEnvironmentTemperature.Value);
                        tempInspectionValueObj.EnvironmentHumidity = decimal.Parse(this.txtEnvironmentHumidity.Value);
                        tempInspectionValueObj.BarometricPressure = decimal.Parse(this.txtBarometricPressure.Value);
                       
                        bllApplRegGasQtyInspectionValue.Update(tempInspectionValueObj);
                        ShowMsgHelper.AlertMsg("保存成功！");
                    }
                    else
                    {
                        ShowMsgHelper.AlertMsg("保存失败！");
                    }

                    InitControl();
                }
            }
            catch (ArgumentOutOfRangeException ex)
            {
                string[] errs = CommonHelper.GetValue(ex.Message, "@").Split(':');
                ShowMsgHelper.ExecuteScript(string.Format("ChangeCss($('#txt{0}'),'长度不能超{1}字')", errs[0], errs[1]));
            }

            
        }

        protected void btnBatchSave_Click(object sender, EventArgs e)
        {


            string bcode = Model.BusinessCode;
            if (!string.IsNullOrEmpty(bcode))
            {
                decimal txtHFResult = 0.0M;
                decimal KSJHFResult = 0.0M;
                decimal KWYSorceResult = 0.0M;
                decimal WaterQualityScoreResult = 0.0M;
                decimal p_StandardVolume = 0.0M;

                CalcuFunction(out txtHFResult, out KSJHFResult, out KWYSorceResult, out WaterQualityScoreResult);
                CalcuFunctionStandardVolume(out p_StandardVolume);

                int returnValue = EGMNGS.Common.ComServies.UpdateApplRegGasQtyInspectionIndexPC(bcode, this.txtlsnd.Value, this.txtwxsp.Value, this.txtsxsp.Value, this.txtcxsp.Value, this.txtcql.Value, this.txtclds.Value, this.txtnd.Value, this.txtcytj.Value, this.txtKWYCQL.Value, txtHFResult, KSJHFResult, KWYSorceResult, WaterQualityScoreResult, txtEnvironmentTemperature.Value, txtEnvironmentHumidity.Value, txtBarometricPressure.Value, txtMonoxide.Value, txthydrothion.Value, txtSulfurDioxide.Value, p_StandardVolume);
                if (returnValue > 0)
                {
                   
                    ShowMsgHelper.AlertMsg("保存成功！");
                }
                else
                {
                    ShowMsgHelper.Alert("保存失败！");
                }

                InitControl();
            }
        }

        private void CalcuFunction(out decimal txtHFResult, out decimal KSJHFResult, out decimal KWYSorceResult, out decimal WaterQualityScoreResult)
        {
            try
            {
                if (ddlCheckPJType.SelectedValue == "1")//四项
                {
                    txtHFResult = 0.0M;
                    KWYSorceResult = 0.0M;
                    KSJHFResult = 0.0M;

                    //水的质量分数
                    double WaterQualityScoreResulttemp = Convert.ToDouble(this.txtsfhl.Value) * 0.123;
                    WaterQualityScoreResult = Convert.ToDecimal(WaterQualityScoreResulttemp * 0.0001);
                }
                else
                {
                    //酸度
                    //HF=20*c*（2*B-X-Y）*1000/6.16/V
                    double txtHFResulttemp = 20 * Convert.ToDouble(this.txtlsnd.Value) * (2 * Convert.ToDouble(this.txtwxsp.Value) - Convert.ToDouble(this.txtsxsp.Value) - Convert.ToDouble(this.txtcxsp.Value)) * 1000.0 / 6.16 / Convert.ToDouble(this.txtcql.Value);

                    txtHFResult = Convert.ToDecimal(txtHFResulttemp * 0.0001);

                    //矿物油
                    double KWYSorceResulttemp = 100 * Convert.ToDouble(this.txtclds.Value) / 6.16 / Convert.ToDouble(this.txtKWYCQL.Value);
                    KWYSorceResult = Convert.ToDecimal(KWYSorceResulttemp * 0.0001);

                    //可水解氟化物
                    double txtKSJHFResulttemp = 20 * Convert.ToDouble(this.txtnd.Value) / (19 * 6.16 * Convert.ToDouble(this.txtcytj.Value) * Convert.ToDouble(this.txtBarometricPressure.Value) / 101325 * 293 / (273 + Convert.ToDouble(this.txtEnvironmentTemperature.Value)));
                    KSJHFResult = Convert.ToDecimal(txtKSJHFResulttemp * 0.0000001);

                    //水的质量分数
                    double WaterQualityScoreResulttemp = Convert.ToDouble(this.txtsfhl.Value) * 0.123;
                    WaterQualityScoreResult = Convert.ToDecimal(WaterQualityScoreResulttemp * 0.0001);
                }





            }
            catch (Exception ex)
            {

                ShowMsgHelper.Alert("计算指标失败:" + ex.Message);
                txtHFResult = 0;
                KWYSorceResult = 0;
                KSJHFResult = 0;
                WaterQualityScoreResult = 0;
                throw ex;
            }

        }
        /// <summary>
        /// 标准体积= 采样体积*293/(273+温度)*气压/101.325
        /// </summary>
        /// <param name=""></param>
        private void CalcuFunctionStandardVolume(out decimal i_StandardVolume)
        {
            decimal cytj = this.txtcytj.Value.AsTargetType<decimal>(0);
            decimal tempC = txtEnvironmentTemperature.Value.AsTargetType<decimal>(0);
            decimal BarometricPressure = txtBarometricPressure.Value.AsTargetType<decimal>(0);

            i_StandardVolume = cytj * 293 / (273 + tempC) * BarometricPressure/101.325M;
            Model.StandardVolume = Math.Round(i_StandardVolume,6);
        }
        /// <summary>
        /// 保存指标
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSaveIndex_Click(object sender, EventArgs e)
        {
            if (IsExistReportCode(this.txtReportCode.Value))
            {
                ShowMsgHelper.Alert_Wern("此报告编号已经存在!");
                return;
            }

            EGMNGS.BLL.ApplRegGasQtyInspection inspectionBLL = new EGMNGS.BLL.ApplRegGasQtyInspection();
            List<ApplRegGasQtyInspectionValue> listValue = bll.GetModelList("1=1");
            ApplRegGasQtyInspectionValue value = listValue[0];

            Model = inspectionBLL.GetModel(Model.OID);

            Model.SPYID = this.ddlSPYID.SelectedValue;
            Model.LDYID = this.ddlLDYID.SelectedValue;
            Model.WSJID = this.ddlWSJID.SelectedValue;
            Model.QYJID = this.ddlQYJID.SelectedValue;
            Model.SDJID = this.ddlSDJID.SelectedValue;
            Model.FGGDJID = this.ddlFGGDJID.SelectedValue;
            Model.HWHYFXYID = this.ddlHWHYFXYID.SelectedValue;
            Model.ReportCode = this.txtReportCode.Value;
            //if (this.txtEnvironmentTemperature.Value.Trim().Length > 0)
            //{
            //    value.EnvironmentTemperature = Model.C = decimal.Parse(this.txtEnvironmentTemperature.Value);
            //}
            //if (this.txtEnvironmentHumidity.Value.Trim().Length > 0)
            //{
            //    value.EnvironmentHumidity = Model.RH = decimal.Parse(this.txtEnvironmentHumidity.Value);
            //}
            //if (this.txtBarometricPressure.Value.Trim().Length > 0)
            //{
            //    value.BarometricPressure = Model.KPA = decimal.Parse(this.txtBarometricPressure.Value);
            //}
            //if (this.txtSF6Result.Value.Trim().Length > 0)
            //{
            //    Model.SF6Result = decimal.Parse(this.txtSF6Result.Value);
            //}
            //if (this.txtAirQualityScoreResult.Value.Trim().Length > 0)
            //{
            //    Model.AirQualityScoreResult = decimal.Parse(this.txtAirQualityScoreResult.Value);
            //}
            //if (this.txtCF4Result.Value.Trim().Length > 0)
            //{
            //    Model.CF4Result = decimal.Parse(this.txtCF4Result.Value);
            //}
            if (this.txtWaterQualityScoreResult.Value.Trim().Length > 0)
            {
                Model.WaterQualityScoreResult = decimal.Parse(this.txtWaterQualityScoreResult.Value);
            }

            //if (this.txtWaterDewResult.Value.Trim().Length > 0)
            //{
            //    Model.WaterDewResult = decimal.Parse(this.txtWaterDewResult.Value);
            //}
            if (this.txtHFResult.Value.Trim().Length > 0)
            {
                Model.HFResult = decimal.Parse(this.txtHFResult.Value);
            }
            if (this.txtKSJHFResult.Value.Trim().Length > 0)
            {
                Model.KSJHFResult = decimal.Parse(this.txtKSJHFResult.Value);
            }
            if (this.txtKWYSorceResult.Value.Trim().Length > 0)
            {
                Model.KWYSorceResult = decimal.Parse(this.txtKWYSorceResult.Value);
            }

            if (this.ddlIsPass.SelectedItem.Text == "合格")
            {
                if (this.ddlCheckPJType.SelectedValue == "1")
                {
                    Model.Conclusion = "本次测验气样中六氟化硫水分及六氟化硫、空气、四氟化碳的质量分数符合国家标准GB/T12022-2014《工业六氟化硫》的要求。";

                }
                else
                {
                    Model.Conclusion = "本次测验气样中六氟化硫水分及六氟化硫、空气、四氟化碳、六氟乙烷、八氟丙烷、酸度、矿物油、可水解氟化物的质量分数符合国家标准GB/T 12022-2014《工业六氟化硫》的要求。";

                }
            }
            else
            {
                Model.Conclusion = this.txtConclusion.Value;
            }
            Model.IsPass = this.ddlIsPass.SelectedValue;

            Model.CheckPJType = this.ddlCheckPJType.SelectedValue;
            Model.CheckDate = DateTime.Parse(this.txtCheckDate.Text);

            var b = listRole.Find(o => o.Contains("检测"));

            if (b != null && b.Length > 0)
            {

                Model.CheckOID = User.UserId.ToString();
            }

            inspectionBLL.Update(Model);
            bll.Update(value);
            ShowMsgHelper.AlertMsg("保存成功");
            InitControl();
        }

        /// <summary>
        /// 导出
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnExport_Click(object sender, EventArgs e)
        {
            EGMNGS.Model.ApplRegGasQtyInspection item = Model;
            if (item == null)
            {
                return;

            }

            MouldExportExcel mee = new MouldExportExcel();
            try
            {
                string physicalPath = Request.PhysicalApplicationPath + "Xls\\";
                Microsoft.Office.Interop.Excel.Workbook sh = mee.OpenExcel(physicalPath + "气体报告管理.xls");
                bool ispass = false;


                if (item.IsPass == null)
                {
                    ShowMsgHelper.Alert(string.Format("{0}此检测单号还未检测不能导出！", item.CheckCode));
                }
                ispass = true;
                Microsoft.Office.Interop.Excel.Worksheet ws = sh.Worksheets[1] as Microsoft.Office.Interop.Excel.Worksheet;

                #region 设置值


                if (item.GasSourse == "3")//回收净化气取“广东电网公司电力科学研究院”。
                {
                    mee.SetCellValue(ws, 1, 1, "六氟化硫再生气体检测报告");
                    mee.SetCellValue(ws, 4, 2, "气体编码：");
                    mee.SetCellValue(ws, 4, 3, item.GasCdoe);
                    //mee.SetCellValue(ws, 4, 6, "检测单号：");
                    //mee.SetCellValue(ws, 4, 7, item.CheckCode);
                    mee.SetCellValue(ws, 4, 6, "批 次 号：");
                    mee.SetCellValue(ws, 4, 7, item.BusinessCode);
                }
                else if (item.GasSourse == "2")//入网新气取“入网检测登记”功能的“送检单位”。
                {
                    ApplDetectionGas tempObj = new EGMNGS.BLL.ApplDetectionGas().GetModelList(string.Format(@"ApplCode='{0}'", item.BusinessCode))[0];
                    DetailsApplDetectionGas tempDetailsApplDetectionGa = new EGMNGS.BLL.DetailsApplDetectionGas().GetModelList(string.Format("GasCode='{0}'", item.GasCdoe))[0];

                    mee.SetCellValue(ws, 1, 1, "六氟化硫气体检测报告");
                    mee.SetCellValue(ws, 4, 2, "委托单位：");
                    mee.SetCellValue(ws, 4, 3, tempObj.InspectionUnit);
                    mee.SetCellValue(ws, 4, 6, "生产厂家：");
                    mee.SetCellValue(ws, 4, 7, tempDetailsApplDetectionGa.Manufacturer);
                    mee.SetCellValue(ws, 5, 6, "安装地点：");
                    mee.SetCellValue(ws, 5, 7, tempObj.SampliInspituation);
                }
                else if (item.GasSourse == "1")//新采购气取“新购气体管理”功能的“生产厂家”。
                {
                    //  RegGasProcurement tempObj = new EGMNGS.BLL.RegGasProcurement().GetModelList(string.Format(@"Code='{0}'", item.BusinessCode))[0];

                    // mee.SetCellValue(ws, 4, 7, tempObj.ManufacturerName);

                    mee.SetCellValue(ws, 1, 1, "六氟化硫气体检测报告");
                    mee.SetCellValue(ws, 4, 2, "气体编码：");
                    mee.SetCellValue(ws, 4, 3, item.GasCdoe);
                    //mee.SetCellValue(ws, 4, 6, "检测单号：");
                    //mee.SetCellValue(ws, 4, 7, item.CheckCode);
                    mee.SetCellValue(ws, 4, 6, "批 次 号：");
                    mee.SetCellValue(ws, 4, 7, item.BusinessCode);

                }


                mee.SetCellValue(ws, 3, 4, item.ReportCode);
                if (item.CheckDate != null)
                {
                    mee.SetCellValue(ws, 3, 7, item.CheckDate.Value.ToShortDateString());

                }
                string cylinderSealNo = new EGMNGS.BLL.BookCylinderInfo().GetModelByCylinderCode(item.CylinderCode).CylinderSealNo;
                mee.SetCellValue(ws, 5, 3, string.Format("{0}", item.CylinderCode));
                //  mee.SetCellValue(ws, 5, 7, item.IsPass == "1" ? "合格" : "不合格");
                var MyRng = ws.get_Range("A42:I42", Type.Missing);
                MyRng.WrapText = true;
                MyRng.Orientation = 0;
                MyRng.AddIndent = false;
                MyRng.IndentLevel = 0;
                MyRng.ShrinkToFit = false;
                MyRng.MergeCells = true;
                //  End With

                mee.SetCellValue(ws, 6, 3, cylinderSealNo);
                //  mee.SetCellValue(ws, 6, 7, item.BusinessCode);
                mee.SetCellValue(ws, 18, 3, item.C.ToString());
                mee.SetCellValue(ws, 19, 3, item.KPA.ToString());
                mee.SetCellValue(ws, 18, 7, item.RH.ToString());

                EGMNGS.BLL.BookDetectionTools bllBookDetectionTools = new EGMNGS.BLL.BookDetectionTools();

                var list = bllBookDetectionTools.GetModelList(string.Format("ToolCode='{0}'", item.SPYID));
                if (list.Count > 0)
                {

                    mee.SetCellValue(ws, 23, 3, list[0].SpecType);
                    mee.SetCellValue(ws, 23, 6, list[0].FactoryCode);
                    mee.SetCellValue(ws, 23, 9, list[0].EffectiveDate.Value.ToString("yyyy-MM-dd", DateTimeFormatInfo.InvariantInfo));

                }
                list = bllBookDetectionTools.GetModelList(string.Format("ToolCode='{0}'", item.LDYID));
                if (list.Count > 0)
                {
                    mee.SetCellValue(ws, 24, 3, list[0].SpecType);
                    mee.SetCellValue(ws, 24, 6, list[0].FactoryCode);
                    mee.SetCellValue(ws, 24, 9, list[0].EffectiveDate.Value.ToString("yyyy-MM-dd", DateTimeFormatInfo.InvariantInfo));
                }
                list = bllBookDetectionTools.GetModelList(string.Format("ToolCode='{0}'", item.WSJID));
                if (list.Count > 0)
                {
                    mee.SetCellValue(ws, 25, 3, list[0].SpecType);
                    mee.SetCellValue(ws, 25, 6, list[0].FactoryCode);
                    mee.SetCellValue(ws, 25, 9, list[0].EffectiveDate.Value.ToString("yyyy-MM-dd", DateTimeFormatInfo.InvariantInfo));
                }
                list = bllBookDetectionTools.GetModelList(string.Format("ToolCode='{0}'", item.QYJID));
                if (list.Count > 0)
                {
                    mee.SetCellValue(ws, 26, 3, list[0].SpecType);
                    mee.SetCellValue(ws, 26, 6, list[0].FactoryCode);
                    mee.SetCellValue(ws, 26, 9, list[0].EffectiveDate.Value.ToString("yyyy-MM-dd", DateTimeFormatInfo.InvariantInfo));
                }
                list = bllBookDetectionTools.GetModelList(string.Format("ToolCode='{0}'", item.SDJID));
                if (list.Count > 0)
                {
                    mee.SetCellValue(ws, 27, 3, list[0].SpecType);
                    mee.SetCellValue(ws, 27, 6, list[0].FactoryCode);
                    mee.SetCellValue(ws, 27, 9, list[0].EffectiveDate.Value.ToString("yyyy-MM-dd", DateTimeFormatInfo.InvariantInfo));
                }
                list = bllBookDetectionTools.GetModelList(string.Format("ToolCode='{0}'", item.FGGDJID));
                if (list.Count > 0)
                {
                    mee.SetCellValue(ws, 28, 3, list[0].SpecType);
                    mee.SetCellValue(ws, 28, 6, list[0].FactoryCode);
                    mee.SetCellValue(ws, 28, 9, list[0].EffectiveDate.Value.ToString("yyyy-MM-dd", DateTimeFormatInfo.InvariantInfo));
                }
                list = bllBookDetectionTools.GetModelList(string.Format("ToolCode='{0}'", item.HWHYFXYID));
                if (list.Count > 0)
                {
                    mee.SetCellValue(ws, 29, 3, list[0].SpecType);
                    mee.SetCellValue(ws, 29, 6, list[0].FactoryCode);
                    mee.SetCellValue(ws, 29, 9, list[0].EffectiveDate.Value.ToString("yyyy-MM-dd", DateTimeFormatInfo.InvariantInfo));

                }


                if (item.SF6Result >= (decimal)99.99)
                {
                    mee.SetCellValue(ws, 34, 2, ">99.99");
                }
                else
                {
                    mee.SetCellValue(ws, 34, 2, Convert.ToDecimal(item.SF6Result).ToString("F4"));
                }

                //  mee.SetCellValue(ws, 36, 2, item.SF6Index);

                if ((item.AirQualityScoreResult * 10000) < 1)
                {
                    mee.SetCellValue(ws, 34, 4, "＜1");
                }
                else
                {
                    mee.SetCellValue(ws, 34, 4, Convert.ToDecimal(item.AirQualityScoreResult * 10000).ToString("F4"));

                }
                //  mee.SetCellValue(ws, 36, 3, item.AirQualityScoreIndex.ToString());

                //小数第4位为0时(＜0.0001)，否则保留4位[if N<0.0001,'＜0.0001',保留4位小数]
                if (Convert.ToDouble(item.CF4Result * 10000) < 1)
                {
                    mee.SetCellValue(ws, 34, 6, "＜1");
                }
                else
                {
                    mee.SetCellValue(ws, 34, 6, Convert.ToDecimal(item.CF4Result * 10000).ToString("F4"));
                }

                if (Convert.ToDouble(item.C2F6) < 1)
                {
                    mee.SetCellValue(ws, 34, 8, "＜1");
                }
                else
                {
                    mee.SetCellValue(ws, 34, 8, item.C2F6.AsTargetType<int>(0));
                }

                if (Convert.ToDouble(item.C3F8) < 1)
                {
                    mee.SetCellValue(ws, 34, 9, "＜1");
                }
                else
                {
                    mee.SetCellValue(ws, 34, 9, item.C3F8.AsTargetType<int>(0));
                }


                // mee.SetCellValue(ws, 36, 4, item.CF4Index);

                //小数第4位为0时(＜0.0001)，否则保留4位[if N<0.0001,'＜0.0001',保留4位小数]
                if (Convert.ToDouble(item.WaterQualityScoreResult) * 10000 < 1)
                {
                    mee.SetCellValue(ws, 39, 2, "＜1");
                }
                else
                {
                    mee.SetCellValue(ws, 39, 2, Convert.ToDecimal(item.WaterQualityScoreResult * 10000).ToString("F5"));
                }

                //  mee.SetCellValue(ws, 36, 5, item.WaterQualityScoreIndex);

                //气体检测报告：露点，如果录入数据是小于-61.0就导出＜-61.0，其它值就按实际数据导出。

                mee.SetCellValue(ws, 39, 4, item.WaterDewResult < -61 ? "<-61.0" : Convert.ToString(item.WaterDewResult));

                //  mee.SetCellValue(ws, 36, 6, item.WaterDewIndex);


                if (Convert.ToDouble(item.HFResult) * 10000 < 0)
                {

                    mee.SetCellValue(ws, 39, 6, "0");
                }
                else if (Convert.ToDouble(item.HFResult) * 10000 < 0.1)
                {
                    mee.SetCellValue(ws, 39, 6, Convert.ToDecimal(item.HFResult * 10000).ToString("F7"));
                }
                else
                {
                    mee.SetCellValue(ws, 39, 6, Convert.ToDecimal(item.HFResult * 10000).ToString("F6"));
                }




                // mee.SetCellValue(ws, 36, 7, item.HFIndex);

                //  mee.SetCellValue(ws, 35, 8, item.KSJHFResult.Value.ToString());

                if (Convert.ToDouble(item.KSJHFResult * 10000) < 0.02)
                {

                    mee.SetCellValue(ws, 39, 8, "＜0.02");
                }
                else if (Convert.ToDouble(item.KSJHFResult * 10000) < 0.00001)
                {
                    mee.SetCellValue(ws, 39, 8, Convert.ToDecimal(item.KSJHFResult * 10000).ToString("F7"));
                }
                else
                {
                    mee.SetCellValue(ws, 39, 8, Convert.ToDecimal(item.KSJHFResult * 10000).ToString("F6"));
                }

                //  mee.SetCellValue(ws, 36, 8, item.KSJHFIndex);

                //  mee.SetCellValue(ws, 39, 9, item.KWYSorceResult.AsTargetType<string>(""));

                if (Convert.ToDouble(item.KWYSorceResult * 10000) < 0.02)
                {

                    mee.SetCellValue(ws, 39, 9, "＜0.02");
                }
                else if (Convert.ToDouble(item.KWYSorceResult * 10000) < 0.1)
                {
                    mee.SetCellValue(ws, 39, 9, Convert.ToDecimal(item.KWYSorceResult * 10000).ToString("F7"));
                }
                else
                {
                    mee.SetCellValue(ws, 39, 9, Convert.ToDecimal(item.KWYSorceResult * 10000).ToString("F6"));
                }

                //  mee.SetCellValue(ws, 36, 9, item.KWYSorceIndex);
                mee.SetCellValue(ws, 43, 1, item.Conclusion);


                if (item.GasSourse == "2")//入网新气取
                {
                    mee.SetCellValue(ws, 5, 2, "出厂编号：");
                    mee.SetCellValue(ws, 5, 3, cylinderSealNo);
                    mee.SetCellValue(ws, 6, 2, "");
                    mee.SetCellValue(ws, 6, 3, "");
                }

                if (item.CheckPJType == "1")
                {
                    mee.SetCellValue(ws, 12, 2, "");
                    mee.SetCellValue(ws, 13, 2, "");
                    mee.SetCellValue(ws, 14, 2, "");

                    mee.SetCellValue(ws, 27, 2, "");
                    mee.SetCellValue(ws, 27, 3, "");

                    mee.SetCellValue(ws, 27, 5, "");
                    mee.SetCellValue(ws, 27, 6, "");

                    mee.SetCellValue(ws, 27, 8, "");
                    mee.SetCellValue(ws, 27, 9, "");

                    mee.SetCellValue(ws, 28, 1, "");
                    mee.SetCellValue(ws, 28, 3, "");

                    mee.SetCellValue(ws, 28, 5, "");
                    mee.SetCellValue(ws, 28, 6, "");

                    mee.SetCellValue(ws, 28, 8, "");
                    mee.SetCellValue(ws, 28, 9, "");

                    mee.SetCellValue(ws, 29, 1, "");
                    mee.SetCellValue(ws, 29, 3, "");

                    mee.SetCellValue(ws, 29, 5, "");
                    mee.SetCellValue(ws, 29, 6, "");

                    mee.SetCellValue(ws, 29, 8, "");
                    mee.SetCellValue(ws, 29, 9, "");


                    //mee.SetCellValue(ws, 35, 7, "—");
                    //mee.SetCellValue(ws, 35, 8, "—");
                    //mee.SetCellValue(ws, 35, 9, "—");


                }

                #endregion

                item.IsPrint = true;
                new EGMNGS.BLL.ApplRegGasQtyInspection().Update(item);

                if (!ispass)// 不合格
                {
                    return;
                }

                string fileName = "气体报告管理temp.xls";
                string pathFile = physicalPath + fileName;
                mee.SaveExcel(pathFile);
                //  ShowMsgHelper.Alert("导出成功!");
                RM.Common.DotNetFile.FileDownHelper.DownLoadold(@"~/Xls/" + fileName);

            }
            finally
            {
                mee.Dispose();

            }

        }
       /// <summary>
       /// 只保存录入
       /// </summary>
       /// <param name="sender"></param>
       /// <param name="e"></param>
        protected void btnOnlySave_Click(object sender, EventArgs e)
        {
            EGMNGS.BLL.ApplRegGasQtyInspection inspectionBLL = new EGMNGS.BLL.ApplRegGasQtyInspection();
            Model = inspectionBLL.GetModel(Model.OID);

            if (this.txtEnvironmentTemperature.Value.Trim().Length > 0)
            {
                 Model.C = decimal.Parse(this.txtEnvironmentTemperature.Value);
            }
            if (this.txtEnvironmentHumidity.Value.Trim().Length > 0)
            {
                 Model.RH = decimal.Parse(this.txtEnvironmentHumidity.Value);
            }
            if (this.txtBarometricPressure.Value.Trim().Length > 0)
            {
                Model.KPA = decimal.Parse(this.txtBarometricPressure.Value);
            }
            if (this.txtSF6Result.Value.Trim().Length > 0)
            {
                Model.SF6Result = decimal.Parse(this.txtSF6Result.Value);
            }
            if (this.txtAirQualityScoreResult.Value.Trim().Length > 0)
            {
                Model.AirQualityScoreResult = decimal.Parse(this.txtAirQualityScoreResult.Value);
            }
            if (this.txtCF4Result.Value.Trim().Length > 0)
            {
                Model.CF4Result = decimal.Parse(this.txtCF4Result.Value);
            }

            if (this.txtWaterDewResult.Value.Trim().Length > 0)
            {
                Model.WaterDewResult = decimal.Parse(this.txtWaterDewResult.Value);
            }

            if (this.txtC3F8.Value.Trim().Length > 0)
            {
                Model.C3F8 = decimal.Parse(this.txtC3F8.Value);
            }
            if (this.txtC2F6.Value.Trim().Length > 0)
            {
                Model.C2F6 = decimal.Parse(this.txtC2F6.Value);
            }
        
            if (inspectionBLL.Update(Model))
            {
                ShowMsgHelper.AlertMsg("保存成功！");
            }
            InitControl();
            // txtEnvironmentTemperature
            //txtEnvironmentHumidity
            //txtBarometricPressure
            //txtSF6Result
            //txtAirQualityScoreResult
            //txtCF4Result
            //txtWaterDewResult
            //txtC2F6
            //txtC3F8


        }

        /// <summary>
        /// 验证报告编号是否存在
        /// </summary>
        /// <returns></returns>
        private bool IsExistReportCode(string reportCode)
        {
            bool isExist = false;

            EGMNGS.BLL.ApplRegGasQtyInspection bllInspection = new EGMNGS.BLL.ApplRegGasQtyInspection();
         DataSet ds=  bllInspection.GetList(string.Format("ReportCode='{0}'", reportCode)) ;

         if (ds.Tables.Count>1&&ds.Tables[0].Rows.Count>0)
         {
             isExist = true;
             
         }
            return isExist;
        }
    }
}
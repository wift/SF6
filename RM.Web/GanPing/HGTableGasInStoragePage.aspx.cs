﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using RM.Common.DotNetCode;
using System.Data;
using RM.Common.DotNetUI;
using RM.Web.App_Code;
using EGMNGS.Common;
using EGMNGS.Model;

namespace RM.Web.GanPing
{
    public partial class HGTableGasInStoragePage : PageBase
    {
        #region 属性字段

        EGMNGS.BLL.TableGasInStorage bll = new EGMNGS.BLL.TableGasInStorage();
        EGMNGS.BLL.DetailsGasStorage DetailsBLL = new EGMNGS.BLL.DetailsGasStorage();

        private Dictionary<string, string> lsCFStatus
        {
            get
            {
                if (Session["GPStatus"] != null)
                {
                    return Session["GPStatus"] as Dictionary<string, string>;
                }
                return new Dictionary<string, string>();
            }

            set
            {
                Session["GPStatus"] = value;
            }
        }

        private DataTable UserList
        {
            get
            {
                return ViewState["UserList"] as DataTable;
            }
            set { ViewState["UserList"] = value; }
        }
        private EGMNGS.Model.TableGasInStorage Model
        {
            get
            {
                if (Session["TableGasInStorage"] == null)
                {
                    return new EGMNGS.Model.TableGasInStorage();
                }
                return Session["TableGasInStorage"] as EGMNGS.Model.TableGasInStorage;
            }
            set { Session["TableGasInStorage"] = value; }
        }
        private string actionMode
        {
            get { return ViewState["ActionMode"] as String; }
            set { ViewState["ActionMode"] = value; }
        }
        public string PowerSupplyCode
        {
            get { return ViewState["PowerSupplyCode"] as String; }
            set { ViewState["PowerSupplyCode"] = value; }
        }
        public string PowerSupplyName
        {
            get { return ViewState["PowerSupplyName"] as String; }
            set { ViewState["PowerSupplyName"] = value; }
        }

        public DataTable DetailsTable
        {
            get
            {
                if (ViewState["DetailsTable"] == null)
                {
                    return new DataTable();
                }
                return ViewState["DetailsTable"] as DataTable;
            }
            set { ViewState["DetailsTable"] = value; }
        }
        private string actionModeDetails
        {
            get
            {
                if (ViewState["actionModeDetails"] == null)
                {
                    return string.Empty;
                }
                return ViewState["actionModeDetails"] as String;
            }
            set { ViewState["actionModeDetails"] = value; }
        }

        private List<DetailsGasStorage> UpdateList
        {
            get
            {
                if (ViewState["UpdateList"] == null)
                {
                    return new List<DetailsGasStorage>();
                }
                return ViewState["UpdateList"] as List<DetailsGasStorage>;
            }
            set { ViewState["UpdateList"] = value; }
        }
        /// <summary>
        /// 需要添加的数据
        /// </summary>
        private List<DetailsGasStorage> AddList
        {
            get
            {
                if (ViewState["AddList"] == null)
                {
                    return new List<DetailsGasStorage>();
                }
                return ViewState["AddList"] as List<DetailsGasStorage>;
            }
            set { ViewState["AddList"] = value; }
        }

        public Dictionary<string, string> FillStatus
        {
            get
            {
                if (ViewState["FillStatus"] == null)
                {
                    Dictionary<string, string> fillStatus = new Dictionary<string, string>();
                    fillStatus.Add(string.Empty, string.Empty);
                    EGMNGS.BLL.SYS_CODE sysCodeBll = new EGMNGS.BLL.SYS_CODE();
                    DataSet dsFlowStatus = sysCodeBll.GetList("CODE_TYPE='FillStatus' ORDER BY DISPLAY_ORDER");
                    foreach (DataRow item in dsFlowStatus.Tables[0].Rows)
                    {
                        fillStatus.Add(item["CODE"].ToString(), item["CODE_CHI_DESC"].ToString());
                    }
                    ViewState["FillStatus"] = fillStatus;
                }

                return ViewState["FillStatus"] as Dictionary<string, string>;
            }
            set { ViewState["FillStatus"] = value; }
        }
        #endregion

        #region 事件方法

        protected void Page_Load(object sender, EventArgs e)
        {
            this.PageControl1.pageHandler += new EventHandler(pager_PageChanged);

            if (!IsPostBack)
            {
                EGMNGS.BLL.SYS_CODE sysCodeBll = new EGMNGS.BLL.SYS_CODE();
                lsCFStatus = sysCodeBll.GetModelList("CODE_TYPE='GPStatus' ORDER BY DISPLAY_ORDER").ToDictionary(o => o.CODE, o => o.CODE_CHI_DESC);

                UserList = ComServies.GetAllUserInfo();
                string[] orgIdOrgName = ComServies.GetPowerApplyByUserID(User.UserId.ToString());
                PowerSupplyCode = orgIdOrgName[0];
                PowerSupplyName = orgIdOrgName[1];
                DropDownListBinder();

                GetDetailsOfRecycleGas();
            }
            if (string.IsNullOrEmpty(actionMode))
            {
                InitControl();
            }

        }

        /// <summary>
        /// 绑定下拉列
        /// </summary>
        private void DropDownListBinder()
        {

            EGMNGS.BLL.SYS_CODE sysCodeBll = new EGMNGS.BLL.SYS_CODE();
            DataSet dsYear = sysCodeBll.GetList("CODE_TYPE='Year' ORDER BY DISPLAY_ORDER");

            this.ddlYear.DataSource = dsYear.Tables[0];
            this.ddlYear.DataTextField = "CODE_CHI_DESC";
            this.ddlYear.DataValueField = "CODE";
            this.ddlYear.DataBind();

            DataSet dsMonth = sysCodeBll.GetList("CODE_TYPE='Month' ORDER BY DISPLAY_ORDER");

            this.ddlMonth.DataSource = dsMonth.Tables[0];
            this.ddlMonth.DataTextField = "CODE_CHI_DESC";
            this.ddlMonth.DataValueField = "CODE";
            this.ddlMonth.DataBind();


            sysCodeBll = new EGMNGS.BLL.SYS_CODE();
            DataSet dsGasType = sysCodeBll.GetList("CODE_TYPE='GasClass' ORDER BY DISPLAY_ORDER");

            ddlGasType.DataSource = dsGasType.Tables[0];
            ddlGasType.DataTextField = "CODE_CHI_DESC";
            ddlGasType.DataValueField = "CODE";
            ddlGasType.DataBind();


            DataSet dtCFStatus = sysCodeBll.GetList("CODE_TYPE='CFStatus' ORDER BY DISPLAY_ORDER");

            this.ddlStatus.DataSource = dtCFStatus.Tables[0];
            this.ddlStatus.DataTextField = "CODE_CHI_DESC";
            this.ddlStatus.DataValueField = "CODE";
            this.ddlStatus.DataBind();

            this.ddlStatus.Items.Insert(0, string.Empty);

            this.ddlGasType.SelectedValue = "3";
            //  this.ddlGasSourse.SelectedValue = "0";
        }


        /// <summary>
        /// 绑定后激发事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void rp_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

        }

        /// <summary>
        /// 绑定数据，分页
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void pager_PageChanged(object sender, EventArgs e)
        {
            DataBindGrid();
        }

        /// <summary>
        /// 绑定数据源
        /// </summary>
        private void DataBindGrid()
        {
            int count = bll.GetRecordCount("GasType='3'");
            DataSet ds = bll.GetListByPage("GasType='3'", "Code desc", PageControl1.PageIndex, PageControl1.PageSize);
            ControlBindHelper.BindRepeaterList(ds.Tables[0], rp_Item);
            this.PageControl1.RecordCount = Convert.ToInt32(count);
        }

        /// <summary>
        /// 绑定数据到对象
        /// </summary>
        private void DataBinder()
        {
            if (this.txtAmountInput.Text.Trim().Length > 0)
            {
                Model.AmountInput = decimal.Parse(this.txtAmountInput.Text);
            }

            Model.Year = this.ddlYear.SelectedValue;
            Model.Month = this.ddlMonth.SelectedValue;

            Model.Code = this.txtCode.Text;

            //Model.BusinessCode = this.txtBusinessCode.Text;
            //Model.GasSourse = this.ddlGasSourse.SelectedValue;
            Model.GasType = this.ddlGasType.SelectedValue;
        }

        /// <summary>
        /// 初始化控件
        /// </summary>
        private void InitControl()
        {
            this.txtAmountInput.Text = Model.AmountInput.ToString();
            this.ddlMonth.SelectedValue = Model.Month;
            this.ddlYear.SelectedValue = Model.Year;
            //this.txtBusinessCode.Text = Model.BusinessCode;
            //this.ddlGasSourse.SelectedValue = Model.GasSourse;
            this.ddlGasType.SelectedValue = Model.GasType;
            this.txtRegistrantOID.Text = GetUserName(Model.registrantOID);
            this.txtRegistrantDate.Text = Model.RegistrantDate == null ? string.Empty : Model.RegistrantDate.Value.ToShortDateString();
            this.ddlStatus.SelectedValue = Model.Status;
            this.txtCode.Text = Model.Code;
        }

        /// <summary>
        /// 获取用户名
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        private string GetUserName(string userId)
        {
            DataRow[] dr = UserList.Select(string.Format("User_ID='{0}'", userId));
            if (dr.Length > 0)
            {
                return dr[0][1].ToString();
            }
            return string.Empty;
        }
        #endregion

        #region 按钮事件

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            if (Model.OID == 0)
            {
                ShowMsgHelper.Alert_Wern("请选择！");
                return;
            }

            if (Model.Status == "1")
            {
                ShowMsgHelper.Alert("无法重复提交！");
                return;
            }

            Model.Status = "1";
            if (ComServies.UpdateHGTableGasInStoragePageStatus(Model.Code, Model.Status))
            {
                ShowMsgHelper.Alert("提交成功！");
                Model = bll.GetModel(Model.OID);
                InitControl();
                DataBindGrid();
                GetDetailsOfRecycleGas();
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            DataBindGrid();
        }

        private void ClearControl()
        {
            Model = new TableGasInStorage();
            InitControl();
        }
        private void ShowMaxMode()
        {
            Model = bll.GetModel(bll.GetMaxId() - 1);
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                if (Model.Status == "1")
                {
                    ShowMsgHelper.Alert_Wern("该记录已提交！");
                    return;
                }

                if (actionMode == "Add")
                {
                    DataBinder();

                    Model.registrantOID = User.UserId.ToString();

                    int count = bll.GetRecordCount(string.Format("Code='{0}'", Model.Code));
                    if (count > 0)
                    {
                        ShowMsgHelper.showWarningMsg("入库编号已经存在!");
                        return;
                    }

                    if (bll.Add(Model) > 0)
                    {
                        actionMode = string.Empty;
                    }

                    DataBindGrid();
                    ShowMaxMode();
                    GetDetailsOfRecycleGas();
                }
                else if (actionMode == "Edit")
                {
                    if (bll.GetModel(Model.OID).Status != "0")
                    {
                        ShowMsgHelper.Alert_Wern("“已确认”状态无法修改数据！");
                        return;
                    }

                    DataBinder();
                    if (bll.Update(Model))
                    {
                        EGMNSShowMsg.ShowEditMsgSuccess();
                        actionMode = string.Empty;
                    }
                    DataBindGrid();
                }
            }
            catch (ArgumentOutOfRangeException ex)
            {
                string[] errs = CommonHelper.GetValue(ex.Message, "@").Split(':');
                ShowMsgHelper.ExecuteScript(string.Format("ChangeCss($('#txt{0}'),'长度不能超{1}字')", errs[0], errs[1]));
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            //if (Model.Status == "1")
            //{
            //    ShowMsgHelper.Alert_Wern("该记录已提交！");
            //    return;
            //}

            if (ComServies.GetCheckStatus(DateTime.Now))
            {
                ShowMsgHelper.Alert_Wern("已对账");
                return;
            }

            actionMode = "Add";
            Model = new EGMNGS.Model.TableGasInStorage(actionMode);
            Model.RegistrantDate = DateTime.Now;
            Model.registrantOID = User.UserId.ToString();
            Model.Status = "0";
            Model.GasType = "3";
            InitControl();

            GetDetailsOfRecycleGas();
        }


        protected void btnDel_Click(object sender, EventArgs e)
        {
            if (Model.OID == 0)
            {
                ShowMsgHelper.Alert_Wern("请选择！");
                return;
            }

            if (Model.Status == "1")
            {
                ShowMsgHelper.Alert_Wern("该记录已提交！");
                return;
            }

            if (Model.Status != "0")
            {
                ShowMsgHelper.Alert_Wern("“已确认”状态不可以删除");
            }
            else
            {
                bll.Delete(Model.OID);
                Model = new EGMNGS.Model.TableGasInStorage();
                InitControl();
                DataBindGrid();
                ShowMsgHelper.Alert("删除成功！");
            }
        }

        protected void rp_Item_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            int oid = Convert.ToInt32(e.CommandArgument);

            Model = bll.GetModel(oid);
            InitControl();
            actionMode = "Edit";

            //绑定明细表
            GetDetailsOfRecycleGas();
        }
        #endregion

        #region Details
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void pager_PageChangedDetails(object sender, EventArgs e)
        {
            GetDetailsOfRecycleGas();
        }

        private void GetDetailsOfRecycleGas()
        {
            DetailsTable = DetailsBLL.GetList(string.Format("DtGasInStorageCode='{0}' order by oid desc", Model.Code)).Tables[0];

            int count = DetailsBLL.GetRecordCount(string.Format("DtGasInStorageCode='{0}'", Model.Code));
            ControlBindHelper.BindRepeaterList(DetailsTable, rptDetails);
        }

        protected void btnAddDetails_Click(object sender, EventArgs e)
        {
            if (Model.Status == "1")
            {
                ShowMsgHelper.Alert_Wern("该记录已提交！");
                return;
            }

            if (Model.OID == 0)
            {
                ShowMsgHelper.Alert_Wern("请选择！");
                return;
            }

            if (this.txtCode.Text.Trim().Length == 0)
            {
                ShowMsgHelper.Alert("请选择入库编号");
                return;
            }

            //首先，恢复数据源
            RetriRepeter();

            DataTable dt = DetailsTable;
            if (LoadTestData() == false) return;

            DataRow row = dt.NewRow();
            row[2] = string.Empty;
            row[3] = string.Empty;
            row[4] = string.Empty;
            row[5] = 0.0;
            row[6] = string.Empty;
            row[10] = Model.Code;
            dt.Rows.Add(row);

            rptDetails.DataSource = dt;
            rptDetails.DataBind();


            actionModeDetails = "Add";
        }
        private void RetriRepeter()
        {
            for (int i = 0; i < this.rptDetails.Items.Count; i++)
            {
                Label txtAmountGas = this.rptDetails.Items[i].FindControl("lblAmountGas") as Label;
                DetailsTable.Rows[i]["AmountGas"] = Convert.ToDecimal(txtAmountGas.Text);
                TextBox txtCylinderCode = this.rptDetails.Items[i].FindControl("txtCylinderCode") as TextBox;
                DetailsTable.Rows[i]["CylinderCode"] = txtCylinderCode.Text;
            }
        }
        /// <summary> 
        /// 从库表获取数据
        /// </summary>
        private bool LoadTestData()
        {
            if (DetailsTable.Rows.Count > 0)
            {
                var tempObj = new EGMNGS.BLL.BookCylinderInfo().GetModelByCylinderCode(Convert.ToString(DetailsTable.Rows[DetailsTable.Rows.Count - 1]["CylinderCode"]));

                if (tempObj == null)
                {
                    ShowMsgHelper.Alert_Error(string.Format("此钢瓶不在台账中！"));
                    return false;
                }

                string gasCode = tempObj.CurGasCode;

                var obj = new EGMNGS.BLL.DetailsGasOutStorage().GetModelList(string.Format(@"TableGasOutStorage_Code 
                                                                                                IN( SELECT Code FROM dbo.TableGasOutStorage WHERE GasClass=2)
                                                                                                and GasCode='{0}'", gasCode));
                if (obj.Count == 0 || tempObj == null)
                {
                    ShowMsgHelper.Alert_Error(string.Format("{0}待检气体出库不存在", DetailsTable.Rows[DetailsTable.Rows.Count - 1]["GasCode"].ToString()));
                    return false;
                }
                else if (new EGMNGS.BLL.ApplRegGasQtyInspection().GetRecordCount(string.Format(@"GasCdoe='{0}' and IsPass='1'", gasCode)) == 0)
                {
                    ShowMsgHelper.Alert_Error(string.Format("{0}气体不合格！", DetailsTable.Rows[DetailsTable.Rows.Count - 1]["GasCode"].ToString()));
                    return false;
                }

                if (DetailsBLL.GetRecordCount(string.Format(@"GasCode='{0}' and exists(select 1 from dbo.TableGasInStorage t 
  where t.Code=DtGasInStorageCode and t.GasType='3')", gasCode)) > 0 && Convert.IsDBNull(DetailsTable.Rows[DetailsTable.Rows.Count - 1]["OID"]))
                {
                    ShowMsgHelper.Alert_Error(string.Format("{0}已经入库！", DetailsTable.Rows[DetailsTable.Rows.Count - 1]["GasCode"].ToString()));
                    return false;
                }
                DetailsTable.Rows[DetailsTable.Rows.Count - 1]["AmountGas"] = Convert.ToDecimal(obj[0].AmountGas);
                DetailsTable.Rows[DetailsTable.Rows.Count - 1]["CylinderStatus"] = obj[0].CylinderStatus;
                DetailsTable.Rows[DetailsTable.Rows.Count - 1]["GasCode"] = obj[0].GasCode;
                DetailsTable.Rows[DetailsTable.Rows.Count - 1]["IsPass"] = obj[0].IsPass;


            }
            return true;
        }
        protected void btnDelDetails_Click(object sender, EventArgs e)
        {
            if (Model.OID == 0)
            {
                ShowMsgHelper.Alert("请选择！");
                return;
            }
            if (Model.Status == "1")
            {
                ShowMsgHelper.Alert_Wern("该记录已提交！");
                return;
            }

            try
            {
                string strID = HiddenField1.Value;

                DetailsBLL.DeleteList(strID.TrimEnd(new char[] { ',' }));
                ShowMsgHelper.Alert("删除成功！");
                GetDetailsOfRecycleGas();
            }
            catch (Exception)
            {
                ShowMsgHelper.Alert("删除失败！");
                throw;
            }
        }

        protected void btnSaveDetails_Click(object sender, EventArgs e)
        {
            try
            {
                if (Model.Status == "1")
                {
                    ShowMsgHelper.Alert_Wern("该记录已提交！");
                    return;
                }

                if (Model.OID == 0)
                {
                    ShowMsgHelper.Alert_Wern("请选择！");
                    return;
                }


                GetCheckBox();
                if (actionModeDetails.Equals("Add"))
                {

                    if (AddList.Count > 0)
                    {
                        //批量添加
                        foreach (var item in AddList)
                        {
                            if (DetailsBLL.GetRecordCount(string.Format(@"GasCode='{0}' and exists(select 1 from dbo.TableGasInStorage t 
  where t.Code=DtGasInStorageCode and t.GasType='3')", item.GasCode)) > 0)
                            {
                                ShowMsgHelper.Alert_Error(string.Format("{0}已经入库！", item.GasCode));
                                continue;
                            }
                            DetailsBLL.Add(item);
                        }
                        AddList.Clear();
                    }

                    ShowMsgHelper.Alert("添加成功！");
                    actionModeDetails = string.Empty;

                }

                if (UpdateList.Count > 0)
                {

                    //批量修改
                    foreach (var item in UpdateList)
                    {
                        DetailsBLL.Update(item);
                    }
                    UpdateList.Clear();
                }

                GetDetailsOfRecycleGas();
            }
            catch (Exception ex)
            {
                ShowMsgHelper.AlertMsg("添加失败！" + ex.Message);
            }
        }

        protected void rptDetails_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {

                CheckBox cb = e.Item.FindControl("checkbox") as CheckBox;
                if (cb.ToolTip.Length == 0)
                {
                    cb.Checked = true;
                }
            }
        }


        /// <summary>
        /// 
        /// </summary>
        private void GetCheckBox()
        {
            UpdateList = new List<DetailsGasStorage>();
            AddList = new List<DetailsGasStorage>();
            for (int i = 0; i < this.rptDetails.Items.Count; i++)
            {
                System.Web.UI.WebControls.CheckBox checkbox = (System.Web.UI.WebControls.CheckBox)rptDetails.Items[i].FindControl("checkbox");
                TextBox txtCylinderCode = this.rptDetails.Items[i].FindControl("txtCylinderCode") as TextBox;
                if (txtCylinderCode.Text.Trim().Length == 0)
                {
                    continue;
                }
                if (checkbox.Checked == true && checkbox.ToolTip.Length > 0)
                {


                    Label txtGasCode = this.rptDetails.Items[i].FindControl("lblGasCode") as Label;
                    Label txtAmountGas = this.rptDetails.Items[i].FindControl("lblAmountGas") as Label;

                    DetailsGasStorage DetailsOfRecycleGasObj = new EGMNGS.BLL.DetailsGasStorage().GetModel(int.Parse(checkbox.ToolTip));
                    // DetailsOfRecycleGasObj.DtGasInStorageCode = Model.Code;
                    //DetailsOfRecycleGasObj.OID = int.Parse(checkbox.ToolTip);
                    DetailsOfRecycleGasObj.CylinderCode = txtCylinderCode.Text;
                    if (txtAmountGas.Text.Trim().Length > 0)
                    {
                        DetailsOfRecycleGasObj.AmountGas = decimal.Parse(txtAmountGas.Text);
                    }
                    if (txtGasCode.Text.Trim().Length > 0)
                    {
                        DetailsOfRecycleGasObj.GasCode = txtGasCode.Text;
                    }

                    UpdateList.Add(DetailsOfRecycleGasObj);
                }
                else if (checkbox.Checked == true && checkbox.ToolTip.Length == 0)
                {

                    Label txtGasCode = this.rptDetails.Items[i].FindControl("lblGasCode") as Label;
                    Label txtAmountGas = this.rptDetails.Items[i].FindControl("lblAmountGas") as Label;

                    DetailsGasStorage DetailsOfRecycleGasObj = new EGMNGS.Model.DetailsGasStorage();
                    DetailsOfRecycleGasObj.DtGasInStorageCode = Model.Code;
                    DetailsOfRecycleGasObj.CylinderCode = txtCylinderCode.Text;
                    if (txtAmountGas.Text.Trim().Length > 0)
                    {
                        DetailsOfRecycleGasObj.AmountGas = decimal.Parse(txtAmountGas.Text);
                    }
                    if (txtGasCode.Text.Trim().Length > 0)
                    {
                        DetailsOfRecycleGasObj.GasCode = txtGasCode.Text;
                    }

                    AddList.Add(DetailsOfRecycleGasObj);
                }
            }
        }

        /// <summary>
        /// 快速添加
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnAddFast_Click(object sender, EventArgs e)
        {
            try
            {
                //    List<BookGasFill> listBookGasFill = new EGMNGS.BLL.BookGasFill().GetModelList(string.Format("BusinessCode='{0}'", Model.BusinessCode));

                //    foreach (BookGasFill item in listBookGasFill)
                //    {
                //        DetailsBLL.Add(new DetailsGasStorage() { CylinderCode = item.CylinderCode, GasCode = item.GasCode, AmountGas = item.AmountGas, CylinderStatus = item.CylinderStatus, DtGasInStorageCode = Model.Code });
                //    }

                //    GetDetailsOfRecycleGas();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }



        #endregion
    }
}
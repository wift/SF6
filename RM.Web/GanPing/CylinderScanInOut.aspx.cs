﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using RM.Web.App_Code;
using RM.Common.DotNetBean;

namespace RM.Web.GanPing
{
    public partial class CylinderScanInOut : PageBase
    {
        public string UserName { set; get; }
        protected void Page_Load(object sender, EventArgs e)
        {
            UserName = RequestSession.GetSessionUser().UserName.ToString(); ;
        }
    }
}
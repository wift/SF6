﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using RM.Common.DotNetCode;
using System.Data;
using RM.Common.DotNetUI;
using RM.Web.App_Code;
using EGMNGS.Common;
using EGMNGS.Model;
namespace RM.Web.GanPing
{
    public partial class HSBookGasFillPage : PageBase
    {
        #region 属性字段

        EGMNGS.BLL.BookGasFill bll = new EGMNGS.BLL.BookGasFill();
        private DataTable UserList
        {
            get
            {
                return ViewState["UserList"] as DataTable;
            }
            set { ViewState["UserList"] = value; }
        }
        private EGMNGS.Model.BookGasFill Model
        {
            get
            {
                if (ViewState["BookGasFill"] == null)
                {
                    return new EGMNGS.Model.BookGasFill();
                }
                return ViewState["BookGasFill"] as EGMNGS.Model.BookGasFill;
            }
            set { ViewState["BookGasFill"] = value; }
        }
        private string actionMode
        {
            get { return ViewState["ActionMode"] as String; }
            set { ViewState["ActionMode"] = value; }
        }
        public string PowerSupplyCode
        {
            get { return ViewState["PowerSupplyCode"] as String; }
            set { ViewState["PowerSupplyCode"] = value; }
        }
        public string PowerSupplyName
        {
            get { return ViewState["PowerSupplyName"] as String; }
            set { ViewState["PowerSupplyName"] = value; }
        }

        #endregion

        #region 事件方法

        protected void Page_Load(object sender, EventArgs e)
        {
            this.PageControl1.pageHandler += new EventHandler(pager_PageChanged);
            if (!IsPostBack)
            {
                UserList = ComServies.GetAllUserInfo();
                string[] orgIdOrgName = ComServies.GetPowerApplyByUserID(User.UserId.ToString());
                PowerSupplyCode = orgIdOrgName[0];
                PowerSupplyName = orgIdOrgName[1];
                DropDownListBinder();
                this.txtCylinderCode.Focus();
            }
        }

        /// <summary>
        /// 绑定下拉列
        /// </summary>
        private void DropDownListBinder()
        {

            EGMNGS.BLL.SYS_CODE sysCodeBll = new EGMNGS.BLL.SYS_CODE();
            DataSet dsYear = sysCodeBll.GetList("CODE_TYPE='Year' ORDER BY DISPLAY_ORDER");

            this.ddlYear.DataSource = dsYear.Tables[0];
            this.ddlYear.DataTextField = "CODE_CHI_DESC";
            this.ddlYear.DataValueField = "CODE";
            this.ddlYear.DataBind();

            DataSet dsMonth = sysCodeBll.GetList("CODE_TYPE='Month' ORDER BY DISPLAY_ORDER");

            this.ddlMonth.DataSource = dsMonth.Tables[0];
            this.ddlMonth.DataTextField = "CODE_CHI_DESC";
            this.ddlMonth.DataValueField = "CODE";
            this.ddlMonth.DataBind();


            sysCodeBll = new EGMNGS.BLL.SYS_CODE();
            DataSet dsGasType = sysCodeBll.GetList("CODE_TYPE='GasType' ORDER BY DISPLAY_ORDER");

            ddlGasType.DataSource = dsGasType.Tables[0];
            ddlGasType.DataTextField = "CODE_CHI_DESC";
            ddlGasType.DataValueField = "CODE";
            ddlGasType.DataBind();

            this.ddlGasType.SelectedIndex = 2;

            DataSet dtFillStatus = sysCodeBll.GetList("CODE_TYPE='FillStatus' ORDER BY DISPLAY_ORDER");

            this.ddlStatus.DataSource = dtFillStatus.Tables[0];
            this.ddlStatus.DataTextField = "CODE_CHI_DESC";
            this.ddlStatus.DataValueField = "CODE";
            this.ddlStatus.DataBind();

            //业务单号要过滤，气体回收申请单状态=“已确认”，回收气体总量还没灌装完的记录，灌装完的就不要出来了。
            this.ddlBusinessCode.DataSource = new EGMNGS.BLL.ApplRecyGasReg().GetList(@"[Status]='2' AND 
AmountRecovery>(SELECT isnull(SUM(AmountGas),0) FROM dbo.BookGasFill WHERE BusinessCode=RecyCode)
").Tables[0];
            this.ddlBusinessCode.DataTextField = "RecyCode";
            this.ddlBusinessCode.DataValueField = "RecyCode";
            this.ddlBusinessCode.DataBind();

            this.ddlBusinessCode.Items.Insert(0, string.Empty);


            DataSet dtGPStatus = sysCodeBll.GetList("CODE_TYPE='GPStatus' ORDER BY DISPLAY_ORDER");

            this.ddlCylinderStatus.DataSource = dtGPStatus.Tables[0];
            this.ddlCylinderStatus.DataTextField = "CODE_CHI_DESC";
            this.ddlCylinderStatus.DataValueField = "CODE";
            this.ddlCylinderStatus.DataBind();
        }


        /// <summary>
        /// 绑定后激发事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void rp_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Label lblStatus = e.Item.FindControl("lblStatus") as Label;

                if (lblStatus != null)
                {
                    if (lblStatus.Text.Length > 0)
                    {
                        lblStatus.Text = this.ddlStatus.Items.FindByValue(lblStatus.Text).Text;
                    }
                }

                Label lblCylinderStatus = e.Item.FindControl("lblCylinderStatus") as Label;

                if (lblCylinderStatus != null)
                {
                    if (lblCylinderStatus.Text.Length > 0)
                    {
                        lblCylinderStatus.Text = this.ddlCylinderStatus.Items.FindByValue(lblCylinderStatus.Text).Text;
                    }
                }
            }
        }

        /// <summary>
        /// 绑定数据，分页
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void pager_PageChanged(object sender, EventArgs e)
        {
            DataBindGrid();
        }

        /// <summary>
        /// 绑定数据源
        /// </summary>
        private void DataBindGrid()
        {
            int count = bll.GetRecordCount("GasType='3'");
            DataSet ds = bll.GetListByPage("GasType='3'", string.Empty, PageControl1.PageIndex, PageControl1.PageSize);
            ControlBindHelper.BindRepeaterList(ds.Tables[0], rp_Item);
            this.PageControl1.RecordCount = Convert.ToInt32(count);
        }

        /// <summary>
        /// 绑定数据到对象
        /// </summary>
        private void DataBinder()
        {
            //Model.GasCode = this.txtGasCode.Text;
            Model.Year = this.ddlYear.SelectedValue;
            Model.Month = this.ddlMonth.SelectedValue;
            if (this.txtAmountGas.Text.Trim().Length > 0)
            {
                Model.AmountGas = decimal.Parse(this.txtAmountGas.Text);
            }
            Model.BusinessCode = this.ddlBusinessCode.SelectedValue;
            Model.CylinderCode = this.txtCylinderCode.Text;
            Model.FillGasCode = this.txtFillGasCode.Text;
            Model.GasType = this.ddlGasType.SelectedValue;

        }

        /// <summary>
        /// 初始化控件
        /// </summary>
        private void InitControl()
        {
            //this.txtGasCode.Text = Model.GasCode;
            this.ddlMonth.SelectedValue = Model.Month;
            this.ddlYear.SelectedValue = Model.Year;
            this.txtAmountGas.Text = Model.AmountGas.ToString();
            //this.ddlBusinessCode.SelectedValue = Model.BusinessCode;
            var vl = this.ddlBusinessCode.Items.FindByValue(Model.BusinessCode);


            if (actionMode == "Edit")
            {

                if (vl == null && Model.BusinessCode != null)
                {
                    this.ddlBusinessCode.Items.Clear();
                    this.ddlBusinessCode.Items.Add(Model.BusinessCode);
                }
                else
                {
                    this.ddlBusinessCode.SelectedValue = Model.BusinessCode;
                }

            }
            this.txtCylinderCode.Text = Model.CylinderCode;
            this.ddlCylinderStatus.SelectedValue = Model.CylinderStatus;
            this.txtFillGasCode.Text = Model.FillGasCode;
            //this.ddlGasStatus.Text = Model.GasStatus;
            this.ddlGasType.SelectedValue = Model.GasType;
            this.txtRegistrantOID.Text = GetUserName(Model.RegistrantOID);
            this.txtRegistrantDate.Text = Model.RegistrantDate == null ? string.Empty : Model.RegistrantDate.Value.ToShortDateString();
            this.ddlStatus.SelectedValue = Model.Status;
        }

        /// <summary>
        /// 获取用户名
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        private string GetUserName(string userId)
        {
            DataRow[] dr = UserList.Select(string.Format("User_ID='{0}'", userId));
            if (dr.Length > 0)
            {
                return dr[0][1].ToString();
            }
            return string.Empty;
        }
        #endregion

        #region 按钮事件

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            if (Model.Status == "1")
            {
                ShowMsgHelper.Alert("无法重复提交！");
                return;
            }

            Model.Status = "1";
            Model.CylinderStatus = "2";
            if (bll.Update(Model))
            {
                ComServies.UpdateGPStatus(Model.CylinderCode, "2");//已充装回收气
                ShowMsgHelper.Alert("提交成功！");
                InitControl();
                DataBindGrid();
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            DataBindGrid();
        }

        private void ShowMaxMode()
        {
            Model = bll.GetModel(bll.GetMaxId() - 1);
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                if (actionMode == "Add")
                {
                    DataBinder();

                    BookCylinderInfo tempBookCylinderInfo = new EGMNGS.BLL.BookCylinderInfo().GetModelByCylinderCode(Model.CylinderCode);

                    //是否存在
                    if (tempBookCylinderInfo == null)
                    {
                        ShowMsgHelper.showWarningMsg("此钢瓶不存在！");
                        return;
                    }

                    //状态=“未抽空清洗”或“已抽空清洗”
                    if (Convert.ToInt32(tempBookCylinderInfo.Status) > 1)
                    {
                        ShowMsgHelper.showWarningMsg("钢瓶必须为“未抽空清洗”或“已抽空清洗”");
                        return;
                    }

                    if (Model.AmountGas > Convert.ToDecimal(DictEGMNS["GPRL"][tempBookCylinderInfo.CylinderCapacity]))
                    {
                        ShowMsgHelper.showWarningMsg("气量不能超过钢瓶的容量!");
                        return;
                    }
                    DataSet ds = ComServies.GetDiffWithRecyAndFill(Model.BusinessCode);

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        int diffNum = Convert.ToInt32(ds.Tables[0].Rows[0][0]);
                        if (diffNum < Model.AmountGas)
                        {
                            ShowMsgHelper.showWarningMsg("气量已经超出!请输入小于" + diffNum.ToString() + "kg");
                            return;
                        }

                    }


                    //是否已经存在
                    //int count = bll.GetRecordCount(string.Format("FillGasCode='{0}'", Model.FillGasCode));
                    //if (count > 0)
                    //{
                    //    ShowMsgHelper.showWarningMsg("充气编号已经存在!");
                    //    return;
                    //}

                    if (bll.Add(Model) > 0)
                    {


                        actionMode = string.Empty;
                        ShowMsgHelper.Alert("添加成功！");
                    }

                    DataBindGrid();
                    ShowMaxMode();
                }
                else if (actionMode == "Edit")
                {
                    if (bll.GetModel(Model.OID).Status != "0")
                    {
                        ShowMsgHelper.Alert_Wern("“已确认”状态无法修改数据！");
                        return;
                    }

                    DataBinder();
                    if (bll.Update(Model))
                    {
                        EGMNSShowMsg.ShowEditMsgSuccess();
                        actionMode = string.Empty;
                    }
                    DataBindGrid();
                }
            }
            catch (ArgumentOutOfRangeException ex)
            {
                string[] errs = CommonHelper.GetValue(ex.Message, "@").Split(':');
                ShowMsgHelper.ExecuteScript(string.Format("ChangeCss($('#txt{0}'),'长度不能超{1}字')", errs[0], errs[1]));
            }
        }
        private void ClearControl()
        {
            Model = new EGMNGS.Model.BookGasFill();
            InitControl();
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            actionMode = "Add";
            Model = new EGMNGS.Model.BookGasFill(actionMode);
            Model.RegistrantDate = DateTime.Now;
            Model.RegistrantOID = User.UserId.ToString();
            Model.GasType = "3";
            Model.Status = "0";
            InitControl();
            this.txtCylinderCode.Focus();
            DropDownListBinder();
        }


        protected void btnDel_Click(object sender, EventArgs e)
        {
            if (bll.GetModel(Model.OID).Status != "0")
            {
                ShowMsgHelper.Alert_Wern("“已确认”状态不可以删除");
            }
            else
            {
                bll.Delete(Model.OID);
                Model = new EGMNGS.Model.BookGasFill();
                InitControl();
                DataBindGrid();
                ShowMsgHelper.Alert("删除成功！");
            }
        }

        protected void rp_Item_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            int oid = Convert.ToInt32(e.CommandArgument);

            Model = bll.GetModel(oid);

            actionMode = "Edit";
            InitControl();
        }


        #endregion

        protected void txtCylinderCode_TextChanged(object sender, EventArgs e)
        {
            var obj = new EGMNGS.BLL.BookCylinderInfo().GetModelByCylinderCode(this.txtCylinderCode.Text);

            this.txtAmountGas.Text = DictEGMNS["GPRL"][obj.CylinderCapacity];
        }
    }
}
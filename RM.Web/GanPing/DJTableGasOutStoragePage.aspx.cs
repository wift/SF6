﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using RM.Common.DotNetCode;
using System.Data;
using RM.Common.DotNetUI;
using RM.Web.App_Code;
using EGMNGS.Common;
using EGMNGS.Model;

namespace RM.Web.GanPing
{
    public partial class DJTableGasOutStoragePage : PageBase
    {
        #region 属性字段

        EGMNGS.BLL.TableGasOutStorage bll = new EGMNGS.BLL.TableGasOutStorage();
        EGMNGS.BLL.DetailsGasOutStorage DetailsBLL = new EGMNGS.BLL.DetailsGasOutStorage();

        private Dictionary<string, string> lsCFStatus
        {
            get
            {
                if (Session["GPStatus"] != null)
                {
                    return Session["GPStatus"] as Dictionary<string, string>;
                }
                return new Dictionary<string, string>();
            }

            set
            {
                Session["GPStatus"] = value;
            }
        }

        private DataTable UserList
        {
            get
            {
                return ViewState["UserList"] as DataTable;
            }
            set { ViewState["UserList"] = value; }
        }
        private EGMNGS.Model.TableGasOutStorage Model
        {
            get
            {
                if (ViewState["TableGasOutStorage"] == null)
                {
                    return new EGMNGS.Model.TableGasOutStorage();
                }
                return ViewState["TableGasOutStorage"] as EGMNGS.Model.TableGasOutStorage;
            }
            set { ViewState["TableGasOutStorage"] = value; }
        }
        private string actionMode
        {
            get { return ViewState["ActionMode"] as String; }
            set { ViewState["ActionMode"] = value; }
        }
        public string PowerSupplyCode
        {
            get { return ViewState["PowerSupplyCode"] as String; }
            set { ViewState["PowerSupplyCode"] = value; }
        }
        public string PowerSupplyName
        {
            get { return ViewState["PowerSupplyName"] as String; }
            set { ViewState["PowerSupplyName"] = value; }
        }

        public DataTable DetailsTable
        {
            get
            {
                if (ViewState["DetailsTable"] == null)
                {
                    return new DataTable();
                }
                return ViewState["DetailsTable"] as DataTable;
            }
            set { ViewState["DetailsTable"] = value; }
        }
        private string actionModeDetails
        {
            get
            {
                if (ViewState["actionModeDetails"] == null)
                {
                    return string.Empty;
                }
                return ViewState["actionModeDetails"] as String;
            }
            set { ViewState["actionModeDetails"] = value; }
        }

        private List<DetailsGasOutStorage> UpdateList
        {
            get
            {
                if (ViewState["UpdateList"] == null)
                {
                    return new List<DetailsGasOutStorage>();
                }
                return ViewState["UpdateList"] as List<DetailsGasOutStorage>;
            }
            set { ViewState["UpdateList"] = value; }
        }
        /// <summary>
        /// 需要添加的数据
        /// </summary>
        private List<DetailsGasOutStorage> AddList
        {
            get
            {

                if (ViewState["AddList"] == null)
                {
                    return new List<DetailsGasOutStorage>();
                }
                return ViewState["AddList"] as List<DetailsGasOutStorage>;
            }
            set { ViewState["AddList"] = value; }
        }
        #endregion

        #region 事件方法

        protected void Page_Load(object sender, EventArgs e)
        {
            this.PageControl1.pageHandler += new EventHandler(pager_PageChanged);

            if (!IsPostBack)
            {
                EGMNGS.BLL.SYS_CODE sysCodeBll = new EGMNGS.BLL.SYS_CODE();
                lsCFStatus = sysCodeBll.GetModelList("CODE_TYPE='GPStatus' ORDER BY DISPLAY_ORDER").ToDictionary(o => o.CODE, o => o.CODE_CHI_DESC);

                UserList = ComServies.GetAllUserInfo();
                string[] orgIdOrgName = ComServies.GetPowerApplyByUserID(User.UserId.ToString());
                PowerSupplyCode = orgIdOrgName[0];
                PowerSupplyName = orgIdOrgName[1];
                DropDownListBinder();
            }
        }

        /// <summary>
        /// 绑定下拉列
        /// </summary>
        private void DropDownListBinder()
        {

            EGMNGS.BLL.SYS_CODE sysCodeBll = new EGMNGS.BLL.SYS_CODE();
            DataSet dsYear = sysCodeBll.GetList("CODE_TYPE='Year' ORDER BY DISPLAY_ORDER");

            this.ddlYear.DataSource = dsYear.Tables[0];
            this.ddlYear.DataTextField = "CODE_CHI_DESC";
            this.ddlYear.DataValueField = "CODE";
            this.ddlYear.DataBind();

            DataSet dsMonth = sysCodeBll.GetList("CODE_TYPE='Month' ORDER BY DISPLAY_ORDER");

            this.ddlMonth.DataSource = dsMonth.Tables[0];
            this.ddlMonth.DataTextField = "CODE_CHI_DESC";
            this.ddlMonth.DataValueField = "CODE";
            this.ddlMonth.DataBind();


            sysCodeBll = new EGMNGS.BLL.SYS_CODE();
            DataSet dsGasType = sysCodeBll.GetList("CODE_TYPE='GasClass' ORDER BY DISPLAY_ORDER");

            ddlGasType.DataSource = dsGasType.Tables[0];
            ddlGasType.DataTextField = "CODE_CHI_DESC";
            ddlGasType.DataValueField = "CODE";
            ddlGasType.DataBind();
            ddlGasType.SelectedValue = "2";


            DataSet dtCFStatus = sysCodeBll.GetList("CODE_TYPE='CFStatus' ORDER BY DISPLAY_ORDER");

            this.ddlStatus.DataSource = dtCFStatus.Tables[0];
            this.ddlStatus.DataTextField = "CODE_CHI_DESC";
            this.ddlStatus.DataValueField = "CODE";
            this.ddlStatus.DataBind();

            this.ddlStatus.Items.Insert(0, string.Empty);

            DataSet dsGasSourse = sysCodeBll.GetList("CODE_TYPE='InGasSourse' ORDER BY DISPLAY_ORDER");

            ddlGasSourse.DataSource = dsGasSourse.Tables[0];
            ddlGasSourse.DataTextField = "CODE_CHI_DESC";
            ddlGasSourse.DataValueField = "CODE";
            ddlGasSourse.DataBind();
            ddlGasSourse.Items.RemoveAt(0);
            ddlGasSourse.Items.Insert(0, string.Empty);

        }


        /// <summary>
        /// 绑定后激发事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void rp_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Label lblUserID = e.Item.FindControl("lblUserID") as Label;
                Label lblGasClass = e.Item.FindControl("lblGasClass") as Label;

                if (lblUserID != null)
                {
                    lblUserID.Text = GetUserName(lblUserID.Text);
                }

                if (lblGasClass != null)
                {
                    if (lblGasClass.Text.Length > 0)
                    {
                        lblGasClass.Text = this.ddlGasType.Items.FindByValue(lblGasClass.Text).Text;
                    }
                }
            }
        }

        /// <summary>
        /// 绑定数据，分页
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void pager_PageChanged(object sender, EventArgs e)
        {
            DataBindGrid();
        }

        /// <summary>
        /// 绑定数据源
        /// </summary>
        private void DataBindGrid()
        {
            int count = bll.GetRecordCount("GasClass='2'");
            DataSet ds = bll.GetListByPage("GasClass='2'", "Code desc", PageControl1.PageIndex, PageControl1.PageSize);
            ControlBindHelper.BindRepeaterList(ds.Tables[0], rp_Item);
            this.PageControl1.RecordCount = Convert.ToInt32(count);
        }

        /// <summary>
        /// 绑定数据到对象
        /// </summary>
        private void DataBinder()
        {
            Model.Code = this.txtCode.Text;
            Model.Year = this.ddlYear.SelectedValue;
            Model.Month = this.ddlMonth.SelectedValue;
            Model.GasClass = this.ddlGasType.SelectedValue;
            if (this.txtAmountOutStorage.Text.Trim().Length > 0)
            {
                Model.AmountOutStorage = decimal.Parse(this.txtAmountOutStorage.Text);
            }

            Model.BusinessCode = this.ddlBusinessCode.SelectedValue;

        }

        /// <summary>
        /// 初始化控件
        /// </summary>
        private void InitControl()
        {
            this.txtCode.Text = Model.Code;
            this.ddlMonth.SelectedValue = Model.Month;
            this.ddlYear.SelectedValue = Model.Year;
            this.ddlGasType.SelectedValue = Model.GasClass;

            this.txtAmountOutStorage.Text = Model.AmountOutStorage.ToString();

            this.ddlStatus.SelectedValue = Model.Status;

            this.txtRegistantOID.Text = GetUserName(Model.RegistantOID);
            this.txtRegistantDate.Text = Model.RegistantDate.ToShortDateString();

            this.ddlBusinessCode.Items.Clear();
            this.ddlBusinessCode.Items.Add(new ListItem(Model.BusinessCode));
            if (Model.BusinessCode != null)
            {
                if (Model.BusinessCode.StartsWith("CG"))
                {
                    ddlGasSourse.SelectedValue = "1";
                }
                else if (Model.BusinessCode.StartsWith("PC"))
                {
                    ddlGasSourse.SelectedValue = "2";
                }
            }

        }

        /// <summary>
        /// 获取用户名
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        private string GetUserName(string userId)
        {
            DataRow[] dr = UserList.Select(string.Format("User_ID='{0}'", userId));
            if (dr.Length > 0)
            {
                return dr[0][1].ToString();
            }
            return string.Empty;
        }
        #endregion

        #region 按钮事件

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            if (Model.OID == 0)
            {
                ShowMsgHelper.Alert_Wern("请选择！");
                return;
            }

            if (Model.Status == "1")
            {
                ShowMsgHelper.Alert_Wern("无法重新提交！");
                return;
            }
            Model.Status = "1";
            //同步更新待检气体库存数量，并批量生成气体检测任务
            if (ComServies.UpdateDJTableGasOutStoragePageStatus(Model.Code, Model.Status))
            {
                EGMNGS.BLL.DetailsGasOutStorage DetailsOutBLL = new EGMNGS.BLL.DetailsGasOutStorage();

                //GetCheckBox();
                // List<DetailsApplDetectionGas> listDetials = DetailsBLL.GetModelList(string.Format("ApplCode='{0}'", Model.Code));
                var ApplRegGasQtyInspectionDLL = new EGMNGS.BLL.ApplRegGasQtyInspection();
                var regGasBatchObjlist = new EGMNGS.BLL.RegGasBatch().GetModelList(string.Format(@"BatchCode='{0}'", Model.BusinessCode));
                foreach (DetailsGasOutStorage item in GetSumbitDetailsGasOutStorage())
                {
                    EGMNGS.Model.ApplRegGasQtyInspection tempApplRegGasQtyInspection = new ApplRegGasQtyInspection(GlobalConstants.ActionModeAdd);
                    tempApplRegGasQtyInspection.CylinderCode = item.CylinderCode;
                    tempApplRegGasQtyInspection.BusinessCode = Model.BusinessCode;
                    tempApplRegGasQtyInspection.GasCdoe = item.GasCode;

                    string gasS = Model.BusinessCode.Contains("CG") ? "1" : "2"; //new EGMNGS.BLL.TableGasInStorage().GetModelList(string.Format(@"Code in
                    //  (SELECT d.DtGasInStorageCode FROM dbo.DetailsGasStorage d WHERE d.GasCode='{0}')", item.GasCode))[0].GasSourse;
                    if (gasS == "2")
                    {
                        tempApplRegGasQtyInspection.GasSourse = "3";
                        tempApplRegGasQtyInspection.KSJHFResult = regGasBatchObjlist[0].KSJFHW;
                        tempApplRegGasQtyInspection.KWYSorceResult = regGasBatchObjlist[0].KWY;
                        tempApplRegGasQtyInspection.HFResult = regGasBatchObjlist[0].Acidity;
                    }
                    else if (gasS == "1")
                    {
                        tempApplRegGasQtyInspection.GasSourse = "1";
                    }

                    if (Model.BusinessCode.Contains("SQ"))
                    {
                        tempApplRegGasQtyInspection.GasSourse = "2";
                    }

                    tempApplRegGasQtyInspection.FlowStatus = "1";
                    tempApplRegGasQtyInspection.ApplOID = User.UserId.ToString();
                    tempApplRegGasQtyInspection.AppLDate = DateTime.Now;
                    string codeReport = string.Empty; ;
                    if (tempApplRegGasQtyInspection.GasSourse == "1")
                    {
                        codeReport = "X";
                    }
                    else if (tempApplRegGasQtyInspection.GasSourse == "2")
                    {
                        codeReport = "R";
                    }
                    else if (tempApplRegGasQtyInspection.GasSourse == "3")
                    {
                        codeReport = "H";
                    }

                    tempApplRegGasQtyInspection.ReportCode = DateTime.Now.Year + ComServies.GetCode4(codeReport);
                    tempApplRegGasQtyInspection.CheckPJType = "2";//其他两种气体，检测项目类型默认=“八项检测”。
                    ApplRegGasQtyInspectionDLL.Add(tempApplRegGasQtyInspection);
                }

                ShowMsgHelper.Alert("提交成功！");
                Model = bll.GetModel(Model.OID);
                InitControl();
                DataBindGrid();
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            DataBindGrid();
        }

        private void ShowMaxMode()
        {
            Model = bll.GetModel(bll.GetMaxId() - 1);
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                if (actionMode == "Add")
                {
                    DataBinder();
                    Model.RegistantOID = User.UserId.ToString();

                    int count = bll.GetRecordCount(string.Format("Code='{0}'", Model.Code));
                    if (count > 0)
                    {
                        ShowMsgHelper.showWarningMsg("出库编号已经存在!");
                        return;
                    }

                    if (bll.Add(Model) > 0)
                    {
                        actionMode = string.Empty;
                    }

                    DataBindGrid();
                    ShowMaxMode();
                    GetDetailsOfRecycleGas();
                }
                else if (actionMode == "Edit")
                {
                    if (bll.GetModel(Model.OID).Status != "0")
                    {
                        ShowMsgHelper.Alert_Wern("“已确认”状态无法修改数据！");
                        return;
                    }

                    DataBinder();
                    if (bll.Update(Model))
                    {
                        EGMNSShowMsg.ShowEditMsgSuccess();
                        actionMode = string.Empty;
                    }
                    DataBindGrid();
                }
            }
            catch (ArgumentOutOfRangeException ex)
            {
                string[] errs = CommonHelper.GetValue(ex.Message, "@").Split(':');
                ShowMsgHelper.ExecuteScript(string.Format("ChangeCss($('#txt{0}'),'长度不能超{1}字')", errs[0], errs[1]));
            }
        }
        private void ClearControl()
        {
            Model = new TableGasOutStorage();
            InitControl();
        }
        protected void btnAdd_Click(object sender, EventArgs e)
        {
            if (ComServies.GetCheckStatus(DateTime.Now))
            {
                ShowMsgHelper.Alert_Wern("已对账");
                return;
            }
            actionMode = "Add";
            Model = new EGMNGS.Model.TableGasOutStorage(actionMode);
            Model.PowerSupplyCode = PowerSupplyCode;
            Model.PowerSupplyName = PowerSupplyName;
            Model.RegistantDate = DateTime.Now;
            Model.RegistantOID = User.UserId.ToString();
            Model.Status = "0";
            Model.GasClass = "2";
            GetDetailsOfRecycleGas();
            InitControl();
            ddlGasSourse_SelectedIndexChanged(null, null);
        }


        protected void btnDel_Click(object sender, EventArgs e)
        {
            if (Model.OID == 0)
            {
                ShowMsgHelper.Alert_Wern("请选择！");
                return;
            }

            if (Model.Status != "0")
            {
                ShowMsgHelper.Alert_Wern("“已确认”状态不可以删除");
            }
            else
            {
                bll.Delete(Model.OID);
                Model = new EGMNGS.Model.TableGasOutStorage();
                InitControl();
                DataBindGrid();
                ShowMsgHelper.Alert("删除成功！");
            }
        }

        protected void rp_Item_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            int oid = Convert.ToInt32(e.CommandArgument);

            Model = bll.GetModel(oid);

            actionMode = "Edit";
            InitControl();
            //绑定明细表
            GetDetailsOfRecycleGas();
        }
        #endregion

        #region Details
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void pager_PageChangedDetails(object sender, EventArgs e)
        {
            GetDetailsOfRecycleGas();
        }

        private void GetDetailsOfRecycleGas()
        {
            DetailsTable = DetailsBLL.GetList(string.Format("TableGasOutStorage_Code='{0}' order by oid desc", Model.Code)).Tables[0];

            // int count = DetailsBLL.GetRecordCount(string.Format("TableGasOutStorage_Code='{0}'", Model.Code));
            ControlBindHelper.BindRepeaterList(DetailsTable, rptDetails);
        }
        protected void btnAddDetails_Click(object sender, EventArgs e)
        {
            if (Model.OID == 0)
            {
                ShowMsgHelper.Alert_Wern("请选择！");
                return;
            }

            if (Model.Status == "1")
            {
                ShowMsgHelper.Alert_Wern("该记录已提交！");
                return;
            }

            //首先，恢复数据源
            RetriRepeter();
            DataTable dt = DetailsTable;
            if (LoadTestData()) return;
            DataRow row = dt.NewRow();
            row[2] = string.Empty;
            row[3] = string.Empty;
            row[4] = string.Empty;
            row[5] = 0.0;
            row["TableGasOutStorage_Code"] = Model.Code;
            dt.Rows.Add(row);

            rptDetails.DataSource = dt;
            rptDetails.DataBind();


            actionModeDetails = "Add";
        }

        /// <summary> 
        /// 从库表获取数据
        /// </summary>
        private bool LoadTestData()
        {

            if (DetailsTable.Rows.Count > 0)
            {
                string gpCode = DetailsTable.Rows[DetailsTable.Rows.Count - 1]["CylinderCode"].ToString();

                var obj = new EGMNGS.BLL.DetailsGasStorage().GetModelList(string.Format(@"CylinderCode='{0}'
	                                                                      AND FillStatus='2'", gpCode));
                if (obj.Count == 0)
                {
                    ShowMsgHelper.Alert_Error(string.Format("{0}此钢瓶不在库中", DetailsTable.Rows[DetailsTable.Rows.Count - 1]["CylinderCode"].ToString()));
                    return true;
                }

                DetailsTable.Rows[DetailsTable.Rows.Count - 1]["AmountGas"] = Convert.ToDecimal(obj[0].AmountGas);
                DetailsTable.Rows[DetailsTable.Rows.Count - 1]["CylinderStatus"] = obj[0].CylinderStatus;
                DetailsTable.Rows[DetailsTable.Rows.Count - 1]["GasCode"] = obj[0].GasCode;
                DetailsTable.Rows[DetailsTable.Rows.Count - 1]["IsPass"] = obj[0].IsPass;
            }

            return false;
        }
        private void RetriRepeter()
        {
            for (int i = 0; i < this.rptDetails.Items.Count; i++)
            {
                Label txtAmountGas = this.rptDetails.Items[i].FindControl("lblAmountGas") as Label;

                TextBox txtCylinderCode = this.rptDetails.Items[i].FindControl("txtCylinderCode") as TextBox;
                DetailsTable.Rows[i][2] = txtCylinderCode.Text;
                DetailsTable.Rows[i][4] = Convert.ToDecimal(txtAmountGas.Text);
            }
        }
        protected void btnDelDetails_Click(object sender, EventArgs e)
        {
            try
            {
                if (Model.OID == 0)
                {
                    ShowMsgHelper.Alert("请选择！");
                    return;
                }

                if (Model.Status == "1")
                {
                    ShowMsgHelper.Alert("该记录已提交！");
                    return;
                }

                string strID = HiddenField1.Value;
                if (strID.Trim().Length == 0)
                {
                    return;

                }
                DetailsBLL.DeleteList(strID);
                ShowMsgHelper.Alert("删除成功！");
                GetDetailsOfRecycleGas();
            }
            catch (Exception)
            {
                ShowMsgHelper.Alert("删除失败！");
                throw;
            }
        }

        protected void btnSaveDetails_Click(object sender, EventArgs e)
        {
            try
            {
                if (Model.OID == 0)
                {
                    ShowMsgHelper.Alert_Wern("请选择！");
                    return;
                }

                if (Model.Status == "1")
                {
                    ShowMsgHelper.Alert_Wern("该记录已提交！");
                    return;
                }

                if (!GetCheckBox())
                {
                    return;
                }
                // if (LoadTestData()) return;
                if (actionModeDetails.Equals("Add"))
                {

                    if (AddList.Count > 0)
                    {
                        //批量添加
                        foreach (var item in AddList)
                        {
                            var lst = new EGMNGS.BLL.DetailsGasProcurement().GetModelList(string.Format("RegGasProcurement_Code='{0}' and CylinderCode='{1}'", this.ddlBusinessCode.SelectedValue, item.CylinderCode));
                            if (lst.Count == 0)
                            {
                                ShowMsgHelper.Alert_Wern(item.GasCode + "此钢瓶不能出库!");
                            }
                            else
                            {
                                item.GasCode = lst[0].GasCode;
                            }

                            if (DetailsBLL.GetRecordCount(string.Format(@"GasCode='{0}' and exists(select * from TableGasOutStorage t 
where TableGasOutStorage_Code=t.Code and t.GasClass='2') ", item.GasCode)) >= 1)
                            {
                                continue;
                            }
                            DetailsBLL.Add(item);
                        }
                        AddList.Clear();
                    }
                    ShowMsgHelper.Alert("添加成功！");
                    actionModeDetails = string.Empty;

                }

                if (UpdateList.Count > 0)
                {
                    //批量修改
                    foreach (var item in UpdateList)
                    {
                        DetailsBLL.Update(item);
                    }
                    UpdateList.Clear();
                }

                GetDetailsOfRecycleGas();
            }
            catch (Exception ex)
            {
                ShowMsgHelper.AlertMsg("添加失败！");
                throw;
            }
        }

        protected void rptDetails_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {

                CheckBox cb = e.Item.FindControl("checkbox") as CheckBox;
                if (cb.ToolTip.Length == 0)
                {
                    cb.Checked = true;
                    TextBox tb = e.Item.FindControl("txtCylinderCode") as TextBox;
                    tb.Focus();
                }
            }
        }


        /// <summary>
        /// 
        /// </summary>
        private bool GetCheckBox()
        {
            UpdateList = new List<DetailsGasOutStorage>();
            AddList = new List<DetailsGasOutStorage>();
            for (int i = 0; i < this.rptDetails.Items.Count; i++)
            {
                System.Web.UI.WebControls.CheckBox checkbox = (System.Web.UI.WebControls.CheckBox)rptDetails.Items[i].FindControl("checkbox");

                TextBox txtCylinderCode = this.rptDetails.Items[i].FindControl("txtCylinderCode") as TextBox;



                if (txtCylinderCode.Text.Trim().Length == 0)
                {
                    continue;
                }

                string gpCode = txtCylinderCode.Text;

                var obj = new EGMNGS.BLL.DetailsGasStorage().GetModelList(string.Format(@"CylinderCode='{0}'
	                                                                      AND FillStatus='2'", gpCode));
                if (obj.Count == 0)
                {
                    ShowMsgHelper.Alert_Error(string.Format("{0}此钢瓶不在库中", DetailsTable.Rows[DetailsTable.Rows.Count - 1]["CylinderCode"].ToString()));
                    AddList.Clear();
                    UpdateList.Clear();
                    return false;
                }

                if (checkbox.Checked == true && checkbox.ToolTip.Length > 0)
                {
                    //TextBox txtCylinderCode = this.rptDetails.Items[i].FindControl("txtCylinderCode") as TextBox;
                    //TextBox txtDtGasInStorageCode = this.rptDetails.Items[i].FindControl("txtDtGasInStorageCode") as TextBox;
                    Label txtGasCode = this.rptDetails.Items[i].FindControl("lblGasCode") as Label;
                    Label txtAmountGas = this.rptDetails.Items[i].FindControl("lblAmountGas") as Label;

                    DetailsGasOutStorage DetailsOfRecycleGasObj = new EGMNGS.BLL.DetailsGasOutStorage().GetModel(int.Parse(checkbox.ToolTip));
                    // DetailsOfRecycleGasObj.TableGasOutStorage_Code = Model.Code;
                    // DetailsOfRecycleGasObj.OID = int.Parse(checkbox.ToolTip);
                    DetailsOfRecycleGasObj.CylinderCode = txtCylinderCode.Text;
                    if (txtAmountGas.Text.Trim().Length > 0)
                    {
                        DetailsOfRecycleGasObj.AmountGas = decimal.Parse(txtAmountGas.Text);
                    }
                    if (txtGasCode.Text.Trim().Length > 0)
                    {
                        DetailsOfRecycleGasObj.GasCode = txtGasCode.Text;
                    }
                    UpdateList.Add(DetailsOfRecycleGasObj);
                }
                else if (checkbox.Checked == true && checkbox.ToolTip.Length == 0)
                {
                    //  TextBox txtCylinderCode = this.rptDetails.Items[i].FindControl("txtCylinderCode") as TextBox;
                    //TextBox txtDtGasInStorageCode = this.rptDetails.Items[i].FindControl("txtDtGasInStorageCode") as TextBox;
                    Label txtGasCode = this.rptDetails.Items[i].FindControl("lblGasCode") as Label;
                    Label txtAmountGas = this.rptDetails.Items[i].FindControl("lblAmountGas") as Label;

                    DetailsGasOutStorage DetailsOfRecycleGasObj = new EGMNGS.Model.DetailsGasOutStorage();
                    DetailsOfRecycleGasObj.TableGasOutStorage_Code = Model.Code;

                    DetailsOfRecycleGasObj.CylinderCode = txtCylinderCode.Text;
                    if (txtAmountGas.Text.Trim().Length > 0)
                    {
                        DetailsOfRecycleGasObj.AmountGas = decimal.Parse(txtAmountGas.Text);
                    }
                    if (txtGasCode.Text.Trim().Length > 0)
                    {
                        DetailsOfRecycleGasObj.GasCode = txtGasCode.Text;
                    }

                    AddList.Add(DetailsOfRecycleGasObj);
                }

            }

            return true;
        }

        private List<DetailsGasOutStorage> GetSumbitDetailsGasOutStorage()
        {
            List<DetailsGasOutStorage> listDetailsGasOutStorage = new List<DetailsGasOutStorage>();
            foreach (DataRow item in DetailsTable.Rows)
            {
                string txtCylinderCode = item["CylinderCode"].ToString();
                string txtAmountGas = item["AmountGas"].ToString();
                string txtGasCode = item["GasCode"].ToString();
                DetailsGasOutStorage DetailsOfRecycleGasObj = new EGMNGS.Model.DetailsGasOutStorage();
                DetailsOfRecycleGasObj.TableGasOutStorage_Code = Model.Code;

                DetailsOfRecycleGasObj.CylinderCode = txtCylinderCode;
                if (txtAmountGas.Trim().Length > 0)
                {
                    DetailsOfRecycleGasObj.AmountGas = decimal.Parse(txtAmountGas);
                }
                if (txtGasCode.Trim().Length > 0)
                {
                    DetailsOfRecycleGasObj.GasCode = txtGasCode;
                }
                listDetailsGasOutStorage.Add(DetailsOfRecycleGasObj);
            }

            return listDetailsGasOutStorage;
        }
        protected void ddlGasSourse_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.ddlBusinessCode.Items.Clear();
            DataSet ds = new DataSet();

            if (this.ddlGasSourse.SelectedItem.Text == "新购气体")
            {
                ds = ComServies.GetProcurementCode();
                //ds = new EGMNGS.BLL.RegGasProcurement().GetAllList();
                this.ddlBusinessCode.DataSource = ds.Tables[0];
                this.ddlBusinessCode.DataTextField = "Code";
                this.ddlBusinessCode.DataValueField = "Code";
                this.ddlBusinessCode.DataBind();
            }
            else if (this.ddlGasSourse.SelectedItem.Text == "净化气体")
            {
                ds = ComServies.GetRegBatchCode(); ;
                this.ddlBusinessCode.DataSource = ds.Tables[0];
                this.ddlBusinessCode.DataTextField = "Code";
                this.ddlBusinessCode.DataValueField = "Code";
                this.ddlBusinessCode.DataBind();
            }
        }


        /// <summary>
        /// 快速添加
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnAddFast_Click(object sender, EventArgs e)
        {
            try
            {
                if (Model.OID == 0)
                {
                    ShowMsgHelper.Alert_Wern("请选择！");
                    return;
                }

                if (Model.Status == "1")
                {
                    ShowMsgHelper.Alert_Wern("该记录已提交！");
                    return;
                }

                if (this.ddlGasSourse.SelectedItem.Text == "新购气体")
                {
                    //var lst = new EGMNGS.BLL.DetailsGasProcurement().GetModelList(string.Format("RegGasProcurement_Code='{0}'", this.ddlBusinessCode.SelectedValue));
                    var lst = new EGMNGS.BLL.DetailsGasStorage().GetModelList(string.Format(@"DtGasInStorageCode in (select Code from dbo.TableGasInStorage where BusinessCode='{0}') and FillStatus='2' and not exists(select 1 from DetailsGasOutStorage d where d.GasCode=DetailsGasStorage.GasCode)", this.ddlBusinessCode.SelectedValue));

                    foreach (var item in lst)
                    {
                        if (DetailsBLL.GetRecordCount(string.Format(@"GasCode='{0}' and exists(select * from TableGasOutStorage t 
where TableGasOutStorage_Code=t.Code and t.GasClass='2') ", item.GasCode)) >= 1)
                        {
                            continue;
                        }

                        DetailsGasOutStorage DetailsOfRecycleGasObj = new EGMNGS.Model.DetailsGasOutStorage();
                        DetailsOfRecycleGasObj.TableGasOutStorage_Code = Model.Code;
                        DetailsOfRecycleGasObj.CylinderCode = item.CylinderCode;
                        DetailsOfRecycleGasObj.AmountGas = item.AmountGas;
                        DetailsOfRecycleGasObj.GasCode = item.GasCode;

                        DetailsBLL.Add(DetailsOfRecycleGasObj);
                    }
                }
                else if (this.ddlGasSourse.SelectedItem.Text == "净化气体")
                {
                    var lst = new EGMNGS.BLL.DetailsGasStorage().GetModelList(string.Format(@"DtGasInStorageCode in (select Code from dbo.TableGasInStorage where BusinessCode='{0}') and FillStatus='2' and not exists(select 1 from DetailsGasOutStorage d where d.GasCode=DetailsGasStorage.GasCode)", this.ddlBusinessCode.SelectedValue));
                    foreach (var item in lst)
                    {
                        if (DetailsBLL.GetRecordCount(string.Format(@"GasCode='{0}' and exists(select * from TableGasOutStorage t 
where TableGasOutStorage_Code=t.Code and t.GasClass='2') ", item.GasCode)) >= 1)
                        {
                            continue;
                        }

                        DetailsGasOutStorage DetailsOfRecycleGasObj = new EGMNGS.Model.DetailsGasOutStorage();
                        DetailsOfRecycleGasObj.TableGasOutStorage_Code = Model.Code;
                        DetailsOfRecycleGasObj.CylinderCode = item.CylinderCode;
                        DetailsOfRecycleGasObj.AmountGas = item.AmountGas;
                        DetailsOfRecycleGasObj.GasCode = item.GasCode;

                        DetailsBLL.Add(DetailsOfRecycleGasObj);
                    }
                }


                GetDetailsOfRecycleGas();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }



        #endregion
    }
}
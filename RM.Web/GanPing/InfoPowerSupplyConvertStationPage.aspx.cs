﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using RM.Common.DotNetUI;
using System.Data;
using EGMNGS.Common;
using RM.Web.App_Code;
using System.Text;
using RM.Common.DotNetCode;

namespace RM.Web.GanPing
{
    public partial class InfoPowerSupplyConvertStationPage : PageBase
    {
        #region 属性字段

        public StringBuilder treeItem_Table = new StringBuilder();

        EGMNGS.BLL.InfoPowerSupplyConvertStation bll = new EGMNGS.BLL.InfoPowerSupplyConvertStation();

        private DataTable UserList
        {
            get
            {
                return ViewState["UserList"] as DataTable;
            }
            set { ViewState["UserList"] = value; }
        }

        private EGMNGS.Model.InfoPowerSupplyConvertStation Model
        {
            get
            {
                if (ViewState["InfoPowerSupplyConvertStation"] == null)
                {
                    return new EGMNGS.Model.InfoPowerSupplyConvertStation();
                }

                return ViewState["InfoPowerSupplyConvertStation"] as EGMNGS.Model.InfoPowerSupplyConvertStation;
            }
            set { ViewState["InfoPowerSupplyConvertStation"] = value; }
        }
        private string actionMode
        {
            get { return ViewState["ActionMode"] as String; }
            set { ViewState["ActionMode"] = value; }
        }

        #endregion

        #region 事件方法
        // <summary>
        /// 所以变电站设置信息
        /// </summary>
        public void GetTreeTable()
        {
            DataSet dtroot = Maticsoft.DBUtility.DbHelperSQL.Query("select DISTINCT PowerSupplyCode,PowerSupplyName from [EGMNGS].[dbo].[InfoPowerSupplyConvertStation]");

            foreach (DataRow drv in dtroot.Tables[0].Rows)
            {
               
                treeItem_Table.Append("<li>");
                treeItem_Table.Append("<div onclick=\"GetTable('" + drv["PowerSupplyCode"] + "')\">" + drv["PowerSupplyName"] + "</div>");

                DataSet dssecond = null;//Maticsoft.DBUtility.DbHelperSQL.Query(string.Format(@"select DISTINCT CovnertStationCode as ConvertStationCode,ConvartStationName as ConvertStationName from [EGMNGS].[dbo].[InfoPowerSupplyConvertStation] where PowerSupplyCode='{0}'", drv["PowerSupplyCode"]));

                if (dssecond!=null&&dssecond.Tables.Count > 0 && dssecond.Tables[0].Rows.Count > 0 )
                {
                    treeItem_Table.Append("<ul>");

                    foreach (DataRow item in dssecond.Tables[0].Rows)
                    {
                        treeItem_Table.Append("<li>");
                        treeItem_Table.Append("<div>" + item["ConvertStationName"] + "</div>");

                        //DataSet dsThree = Maticsoft.DBUtility.DbHelperSQL.Query(string.Format("select DISTINCT  [DevCode],[DevName] from [EGMNGS].[dbo].[InfoDevPara] where ConvertStationCode='{0}'", item["ConvertStationCode"]));

                        //if (dsThree.Tables.Count > 0 && dsThree.Tables[0].Rows.Count > 0)
                        //{
                        //    treeItem_Table.Append("<ul>");
                        //    foreach (DataRow itemThree in dsThree.Tables[0].Rows)
                        //    {
                        //        treeItem_Table.Append("<li><div onclick=\"GetTable('" + itemThree["DevCode"] + "')\"><img src=\"/Themes/Images/20130502112716785_easyicon_net_16.png\" width=\"16\" height=\"16\" />" + itemThree["DevName"] + "</div></li>");

                        //    }
                        //    treeItem_Table.Append("</ul>");
                        //}
                        treeItem_Table.Append("</li>");
                    }


                    treeItem_Table.Append("</ul>");
                }

                treeItem_Table.Append("</li>");
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            this.PageControl1.pageHandler += new EventHandler(pager_PageChanged);
            if (!IsPostBack)
            {
                UserList = ComServies.GetAllUserInfo();
                DropDownListBinder();
                GetTreeTable();
            }
        }
        /// <summary>
        /// 绑定下拉列
        /// </summary>
        private void DropDownListBinder()
        {
            DataTable dtPowerStation = ComServies.GetAllPowerStation();
            this.ddlPowerSupplyName.DataSource = dtPowerStation;
            this.ddlPowerSupplyName.DataTextField = "Organization_Name";
            this.ddlPowerSupplyName.DataValueField = "Organization_ID";
            this.ddlPowerSupplyName.DataBind();

            EGMNGS.BLL.SYS_CODE sysCodeBll = new EGMNGS.BLL.SYS_CODE();
            DataSet dtCFStatus = sysCodeBll.GetList("CODE_TYPE='CFStatus' ORDER BY DISPLAY_ORDER");

            this.ddlCFStatus.DataSource = dtCFStatus.Tables[0];
            this.ddlCFStatus.DataTextField = "CODE_CHI_DESC";
            this.ddlCFStatus.DataValueField = "CODE";
            this.ddlCFStatus.DataBind();
        }
        /// <summary>
        /// 绑定后激发事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void rp_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Label lblUser_Sex = e.Item.FindControl("lblUserID") as Label;
                if (lblUser_Sex != null)
                {
                    DataRow[] dr = UserList.Select(string.Format("User_ID='{0}'", lblUser_Sex.Text));
                    if (dr.Length > 0)
                    {
                        lblUser_Sex.Text = UserList.Select(string.Format("User_ID='{0}'", lblUser_Sex.Text))[0][1].ToString();
                    }
                }

                Label lblStatus = e.Item.FindControl("lblStatus") as Label;
                if (lblStatus != null)
                {
                    if (lblStatus.Text.Length > 0)
                    {
                        lblStatus.Text = this.ddlCFStatus.Items.FindByValue(lblStatus.Text).Text;

                    }
                }
            }
        }
        /// <summary>
        /// 绑定数据，分页
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void pager_PageChanged(object sender, EventArgs e)
        {
            DataBindGrid();
        }
        /// <summary>
        /// 绑定数据源
        /// </summary>
        private void DataBindGrid()
        {
            PageControl1.PageSize = 17;
            string whereStr = string.Format("PowerSupplyCode='{0}'", this.ddlPowerSupplyName.SelectedValue);
            int count = bll.GetRecordCount(whereStr);
            DataSet ds = bll.GetListByPage(whereStr, string.Empty, PageControl1.PageIndex, PageControl1.PageSize);
            ControlBindHelper.BindRepeaterList(ds.Tables[0], rp_Item);
            this.PageControl1.RecordCount = Convert.ToInt32(count);
        }

        /// <summary>
        /// 绑定数据到对象
        /// </summary>
        private void DataBinder()
        {
            Model.CovnertStationCode = this.txtCovnertStationCode.Text;
            Model.ConvartStationName = this.txtConvartStationName.Text;
            Model.RegistrantDate = this.txtRegistrantDate.Text == "" ? DateTime.Now : Convert.ToDateTime(this.txtRegistrantDate.Text);
            Model.PowerSupplyCode = this.ddlPowerSupplyName.SelectedValue;
            Model.PowerSupplyName = this.ddlPowerSupplyName.SelectedItem.Text;
            Model.Longitude = this.txtLongitude.Text.AsTargetType<decimal>(0);
            Model.Latitude=this.txtLatitude.Text.AsTargetType<decimal>(0);
            Model.IsMagnetic = this.ddlIsMagnetic.SelectedValue;
            Model.IsOnlineCheck = this.ddlIsOnlineCheck.SelectedValue;
        }

        /// <summary>
        /// 绑定数据到控件
        /// </summary>
        private void InitControl()
        {
            if (Model.PowerSupplyCode!=null)
            {
                this.ddlPowerSupplyName.SelectedValue = Model.PowerSupplyCode;
            }
            
            this.ddlCFStatus.SelectedValue = Model.Status;
            this.txtCovnertStationCode.Text = Model.CovnertStationCode;
            this.txtConvartStationName.Text = Model.ConvartStationName;
            this.txtRegistrantOID.Text = GetUserName(Model.RegistrantOID);
            this.txtRegistrantDate.Text = Model.RegistrantDate == null ? string.Empty : Model.RegistrantDate.Value.ToShortDateString();
            this.ddlIsOnlineCheck.SelectedValue = Model.IsOnlineCheck;
            this.ddlIsMagnetic.SelectedValue = Model.IsMagnetic;
            this.txtLatitude.Text = Model.Latitude.AsTargetType<string>("");
            this.txtLongitude.Text = Model.Longitude.AsTargetType<string>("");

        }
        /// <summary>
        /// 获取用户名
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        private string GetUserName(string userId)
        {
            DataRow[] dr = UserList.Select(string.Format("User_ID='{0}'", userId));
            if (dr.Length > 0)
            {
                return dr[0][1].ToString();
            }
            return string.Empty;
        }
        #endregion

        #region 按钮事件

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            DataBindGrid();
        }

        private void ShowMaxMode()
        {
            Model = bll.GetModel(bll.GetMaxId() - 1);
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                if (actionMode == "Add")
                {
                    DataBinder();

                    int count = bll.GetRecordCount(string.Format("CovnertStationCode='{0}'", Model.CovnertStationCode));

                    if (count > 0)
                    {
                        ShowMsgHelper.showWarningMsg("变电站编号已经存在!");
                        return;
                    }
                    count = bll.GetRecordCount(string.Format("PowerSupplyCode='{0}' and ConvartStationName='{1}'", Model.PowerSupplyCode, Model.ConvartStationName));

                    if (count > 0)
                    {
                        ShowMsgHelper.showWarningMsg("此变电站名称已存在！");
                        return;
                    }
                    if (bll.Add(Model) > 0)
                    {
                        actionMode = string.Empty;
                    }

                    DataBindGrid();
                    ShowMaxMode();
                }
                else if (actionMode == "Edit")
                {
                    if (bll.GetModel(Model.OID).Status == "1")
                    {
                        ShowMsgHelper.Alert_Wern("“已确认”状态无法修改数据！");
                        return;
                    }

                    DataBinder();
                    if (bll.Update(Model))
                    {
                        EGMNSShowMsg.ShowEditMsgSuccess();
                        actionMode = string.Empty;
                    }
                    DataBindGrid();
                }
            }
            catch (ArgumentOutOfRangeException ex)
            {
                string[] errs = CommonHelper.GetValue(ex.Message, "@").Split(':');
                ShowMsgHelper.ExecuteScript(string.Format("ChangeCss($('#txt{0}'),'长度不能超{1}字')", errs[0], errs[1]));
            }
        }
        private void ClearControl()
        {
            Model = new EGMNGS.Model.InfoPowerSupplyConvertStation();
            InitControl();
        }
        protected void btnAdd_Click(object sender, EventArgs e)
        {
            actionMode = "Add";
            Model = new EGMNGS.Model.InfoPowerSupplyConvertStation(actionMode);
            //string[] orgIdOrgName = ComServies.GetPowerApplyByUserID(User.UserId.ToString());
            //Model.PowerSupplyCode = orgIdOrgName[0];
            //Model.PowerSupplyName = orgIdOrgName[1];
            Model.RegistrantDate = DateTime.Now;
            Model.RegistrantOID = User.UserId.ToString();

            InitControl();


        }

        protected void btnDel_Click(object sender, EventArgs e)
        {
            if (bll.GetModel(Model.OID).Status == "1")
            {
                ShowMsgHelper.Alert_Wern("“已确认”状态不可以删除");
            }
            else
            {
                bll.Delete(Model.OID);
                Model = new EGMNGS.Model.InfoPowerSupplyConvertStation();
                InitControl();
                DataBindGrid();
                ShowMsgHelper.Alert("删除成功！");
            }
        }

        protected void btnConfirm_Click(object sender, EventArgs e)
        {
            Model.Status = "1";
            if (bll.Update(Model))
            {
                ShowMsgHelper.Alert("提交成功！");
                DataBindGrid();
            }
        }
        protected void rp_Item_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            int oid = Convert.ToInt32(e.CommandArgument);

            Model = bll.GetModel(oid);

            InitControl();
            actionMode = "Edit";
        }
        #endregion

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using RM.Common.DotNetUI;
using EGMNGS.Common;
using EGMNGS.Model;

namespace RM.Web.GanPing
{
    public partial class BatchShowCylinderInspectDetails : System.Web.UI.Page
    {
        EGMNGS.BLL.CylinderInspectTable bll = new EGMNGS.BLL.CylinderInspectTable();
        private DataTable UserList
        {
            get
            {
                return ViewState["UserList"] as DataTable;
            }
            set { ViewState["UserList"] = value; }
        }

        private Dictionary<string, string> ddlIsPass
        {
            get
            {
                return ViewState["ddlIsPass"] as Dictionary<string, string>;
            }
            set { ViewState["ddlIsPass"] = value; }
        }

        private Dictionary<string, string> ddlCylinderCapacity
        {
            get
            {
                return ViewState["ddlCylinderCapacity"] as Dictionary<string, string>;
            }
            set { ViewState["ddlCylinderCapacity"] = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                UserList = ComServies.GetAllUserInfo();
                InitData();
                DataBindGrid();
            }
        }

        private void InitData()
        {
            ddlIsPass = new Dictionary<string, string>();
            ddlCylinderCapacity = new Dictionary<string, string>();

            EGMNGS.BLL.SYS_CODE sysCodeBll = new EGMNGS.BLL.SYS_CODE();
            List<SYS_CODE> dtIsPass = sysCodeBll.GetModelList("CODE_TYPE='IsPass' ORDER BY DISPLAY_ORDER");
            foreach (SYS_CODE item in dtIsPass)
            {
                ddlIsPass.Add(item.CODE, item.CODE_CHI_DESC);
            }

            List<SYS_CODE> dtCylinderCapacity = sysCodeBll.GetModelList("CODE_TYPE='GPRL' ORDER BY DISPLAY_ORDER");
            foreach (SYS_CODE item in dtCylinderCapacity)
            {
                ddlCylinderCapacity.Add(item.CODE, item.CODE_CHI_DESC);
            }
        }



        /// <summary>
        /// 绑定数据源
        /// </summary>
        private void DataBindGrid()
        {
            DataSet ds = bll.GetList("c.Status='5' ORDER BY c.RegistantsDate DESC");
            ControlBindHelper.BindRepeaterList(ds.Tables[0], rp_Item);
        }

        /// <summary>
        /// 绑定后激发事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void rp_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {

                Label lblSendCheckOID = e.Item.FindControl("lblSendCheckOID") as Label;
                if (lblSendCheckOID != null)
                {
                    if (lblSendCheckOID.Text.Length > 0)
                    {
                        lblSendCheckOID.Text = GetUserName(lblSendCheckOID.Text);

                    }
                }


                Label lblCylinderCapacity = e.Item.FindControl("lblCylinderCapacity") as Label;
                if (lblCylinderCapacity != null)
                {
                    if (lblCylinderCapacity.Text.Length > 0)
                    {
                        lblCylinderCapacity.Text = ddlCylinderCapacity[lblCylinderCapacity.Text];

                    }
                }


                Label lblIsPass = e.Item.FindControl("lblIsPass") as Label;
                if (lblIsPass != null)
                {
                    if (lblIsPass.Text.Length > 0)
                    {
                        lblIsPass.Text = ddlIsPass[lblIsPass.Text];
                    }
                }
            }
        }

        /// <summary>
        /// 获取用户名
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        private string GetUserName(string userId)
        {
            DataRow[] dr = UserList.Select(string.Format("User_ID='{0}'", userId));
            if (dr.Length > 0)
            {
                return dr[0][1].ToString();
            }
            return string.Empty;
        }


        protected void btnBatchAdd_Click(object sender, EventArgs e)
        {
            if (HiddenField1.Value.Length > 0)
            {


                ShowMsgHelper.AlertMsg("快速添加成功！");
            }
        }

    }
}
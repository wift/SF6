﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RegGasBatchPage.aspx.cs"
    Inherits="RM.Web.GanPing.RegGasBatchPage" EnableEventValidation="false" ClientIDMode="AutoID" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Src="~/UserControl/PageControl.ascx" TagName="PageControl" TagPrefix="uc1" %>
<%@ Register Src="~/UserControl/LoadButton.ascx" TagName="LoadButton" TagPrefix="uc2" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>净化气体批次管理</title>
    <link href="/Themes/Styles/Site.css" rel="stylesheet" type="text/css" />
    <script src="/Themes/Scripts/Validator/JValidator.js" type="text/javascript"></script>
    <script src="/Themes/Scripts/jquery-1.8.2.min.js" type="text/javascript"></script>
    <script src="/Themes/Scripts/jquery.pullbox.js" type="text/javascript"></script>
    <script src="/Themes/Scripts/FunctionJS.js" type="text/javascript"></script>
    <script type="text/javascript">
        //添加
        function add() {
            document.getElementById('<%= this.btnAdd.ClientID %>').click();
        }
        //查询
        function search() {

            document.getElementById('<%= this.btnSearch.ClientID %>').click();
        }
        //删除
        function Delete() {
            showConfirmMsg("此操作不可恢复，您确定要删除吗？", function (r) {
                if (r) {
                    document.getElementById('<%= this.btnDel.ClientID %>').click();
                }
            });
        }
        //保存
        function SaveForm() {
            document.getElementById('<%= this.btnSave.ClientID %>').click();
        }
        //提交
        function Submit() {
            document.getElementById('<%= this.btnSubmit.ClientID %>').click();
        }

        $(function () {
            InitControl();
        })
        function InitControl() {
            $(".div-body").PullBox({ dv: $(".div-body"), obj: $("#table1").find("tr") });
            divresize(155);
        }

        function BatchDeal() {
            var url = "/GanPing/BatchDeal.aspx?BatchCode=" + $("#txtBatchCode").val();
            top.openDialog(url, 'BatchDeal', '批量处理', 1000, 400, 50, 50);
            
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional">
        <ContentTemplate>
            <table width="100%">
                <colgroup>
                    <col width="11%" />
                    <col width="11%" />
                    <col width="11%" />
                    <col width="11%" />
                    <col width="11%" />
                    <col width="11%" />
                    <col width="11%" />
                    <col width="11%" />
                </colgroup>
                <tr>
                    <td class="inner_cell_right">
                        批次编号：
                    </td>
                    <td>
                        <asp:TextBox ID="txtBatchCode" runat="server" CssClass="text" datacol="yes" err="此项"
                            checkexpession="NotNull" />
                    </td>
                    <td class="inner_cell_right">
                        气量（kg）：
                    </td>
                    <td>
                        <asp:TextBox ID="txtAmountGas" runat="server" CssClass="text" datacol="yes" err="此项"
                            checkexpession="NotNull" />
                    </td>
                    <td class="inner_cell_right">
                        可水解氟化物：
                    </td>
                    <td>
                        <asp:TextBox ID="txtKSJFHW" runat="server" CssClass="text" Enabled="false" />
                    </td>
                    <td class="inner_cell_right">
                        年度：
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlYear" runat="server" datacol="yes" err="此项" checkexpession="NotNull">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td class="inner_cell_right">
                        处理设备：
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlProcessEquipment" runat="server" Width="100%" datacol="yes"
                            err="此项" checkexpession="NotNull">
                        </asp:DropDownList>
                    </td>
                    <td class="inner_cell_right">
                        酸度：
                    </td>
                    <td>
                        <asp:TextBox ID="txtAcidity" runat="server" CssClass="text" Enabled="false" />
                    </td>
                    <td class="inner_cell_right">
                        六氟化硫含量：
                    </td>
                    <td>
                        <asp:TextBox ID="txtSFContent" runat="server" CssClass="text" />
                    </td>
                    <td class="inner_cell_right">
                        月度：
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlMonth" runat="server" datacol="yes" err="此项" checkexpession="NotNull">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td class="inner_cell_right">
                        状态：
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlStatus" runat="server" Width="100%" Enabled="false">
                        </asp:DropDownList>
                    </td>
                    <td class="inner_cell_right">
                        水分：
                    </td>
                    <td>
                        <asp:TextBox ID="txtWet" runat="server" CssClass="text" Enabled="false"></asp:TextBox>
                    </td>
                    <td class="inner_cell_right">
                        空气含量：
                    </td>
                    <td>
                        <asp:TextBox ID="txtAirContent" runat="server" />
                    </td>
                    <td class="inner_cell_right">
                        登记人：
                    </td>
                    <td>
                        <asp:TextBox ID="txtRegistrantOID" runat="server" CssClass="text" Enabled="false" />
                    </td>
                </tr>
                <tr>
                    <td class="inner_cell_right">
                        初检是否合格:
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlIsPass" runat="server" Width="100%" datacol="yes" err="此项"
                            checkexpession="NotNull">
                        </asp:DropDownList>
                    </td>
                    <td class="inner_cell_right">
                        矿物油：
                    </td>
                    <td>
                        <asp:TextBox ID="txtKWY" runat="server" CssClass="text" Enabled="false" />
                    </td>
                    <td class="inner_cell_right">
                        四氟化碳：
                    </td>
                    <td>
                        <asp:TextBox ID="txtSFHT" runat="server" CssClass="text" />
                    </td>
                    <td class="inner_cell_right">
                        登记日期：
                    </td>
                    <td>
                        <asp:TextBox ID="txtRegistrantDate" runat="server" CssClass="text" datacol="yes"
                            err="此项" checkexpession="NotNull" />
                    </td>
                </tr>
            </table>
            <div style="text-align: left;">
                <uc2:LoadButton ID="LoadButton1" runat="server" />
            </div>
            <div style="text-align: right; display: none;">
                <asp:Button ID="btnSubmit" runat="server" OnClick="btnSubmit_Click" Text="btnSubmit_Click" />
                <asp:Button ID="btnSearch" runat="server" OnClick="btnSearch_Click" Text="btnSearch_Click" />
                <asp:Button ID="btnSave" runat="server" OnClick="btnSave_Click" Text="btnSave_Click"
                    OnClientClick="return CheckDataValid('#form1');" />
                <asp:Button ID="btnAdd" runat="server" OnClick="btnAdd_Click" Text="btnAdd_Click" />
                <asp:Button ID="btnDel" runat="server" OnClick="btnDel_Click" Text="btnDel_Click" />
            </div>
            <div class="div-body">
                <table id="table1" class="grid" singleselect="true">
                    <colgroup>
                        <col width="10%" />
                        <col width="5%" />
                        <col width="10%" />
                        <col width="10%" />
                        <col width="10%" />
                        <col width="10%" />
                        <col width="10%" />
                        <col width="10%" />
                        <col width="10%" />
                        <col width="10%" />
                        <col width="5%" />
                    </colgroup>
                    <thead>
                        <tr>
                            <%-- <td style="width: 20px; text-align: left;">
                        <label id="checkAllOff" onclick="CheckAllLine()" title="全选">
                            &nbsp;</label>
                    </td>--%>
                            <td style="text-align: center;">
                                批次编号
                            </td>
                            <td style="text-align: center;">
                                气量
                            </td>
                            <td style="text-align: center;">
                                酸度
                            </td>
                            <td style="text-align: center;">
                                水分
                            </td>
                            <td style="text-align: center;">
                                矿物油
                            </td>
                            <td style="text-align: center;">
                                可水解氟化物
                            </td>
                            <td style="text-align: center;">
                                六氟化硫含量
                            </td>
                            <td style="text-align: center;">
                                空气含量
                            </td>
                            <td style="text-align: center;">
                                四氟化碳
                            </td>
                            <td style="text-align: center;">
                                登记日期
                            </td>
                            <td style="text-align: center;">
                                状态
                            </td>
                        </tr>
                    </thead>
                    <tbody>
                        <asp:Repeater ID="rp_Item" runat="server" OnItemDataBound="rp_ItemDataBound" OnItemCommand="rp_Item_ItemCommand">
                            <ItemTemplate>
                                <tr>
                                    <%--<td style="width: 20px; text-align: left;">
                                <input type="checkbox" value="<%#Eval("OID")%>" name="checkbox" />
                            </td>--%>
                                    <td style="text-align: center;">
                                        <asp:LinkButton ID="LBtnDel" runat="server" OnClientClick="selectrow(this)" CommandArgument='<%#Eval("OID") %>'><%#Eval("BatchCode")%></asp:LinkButton>
                                    </td>
                                    <td style="text-align: center;">
                                        <%#Eval("AmountGas")%>
                                    </td>
                                    <td style="text-align: center;">
                                        <%#Eval("Acidity")%>
                                    </td>
                                    <td style="text-align: center;">
                                        <%#Eval("Wet")%>
                                    </td>
                                    <td style="text-align: center;">
                                        <%#Eval("KWY")%>
                                    </td>
                                    <td style="text-align: center;">
                                        <%#Eval("KSJFHW")%>
                                    </td>
                                    <td style="text-align: center;">
                                        <%#Eval("SFContent")%>
                                    </td>
                                    <td style="text-align: center;">
                                        <%#Eval("AirContent")%>
                                    </td>
                                    <td style="text-align: center;">
                                        <%#Eval("SFHT")%>
                                    </td>
                                    <td style="text-align: center;">
                                        <%#Eval("RegistantsDate", "{0:yyyy-MM-dd}")%>
                                    </td>
                                    <td style="text-align: center;">
                                        <%# DictEGMNS["CFStatus"][Eval("Status").ToString()]%>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </tbody>
                </table>
            </div>
            <uc1:PageControl ID="PageControl1" runat="server" />
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnAdd" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>
    </form>
</body>
</html>

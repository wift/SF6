﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using RM.Web.App_Code;
using EGMNGS.Common;
using RM.Common.DotNetUI;
using RM.Common.DotNetCode;
using System.Data;

namespace RM.Web.GanPing
{
    public partial class CylinderPositionSetPage : PageBase
    {
        #region 属性字段

        EGMNGS.BLL.CylinderPositionSet bll = new EGMNGS.BLL.CylinderPositionSet();
        //private DataTable UserList
        //{
        //    get
        //    {
        //        return Session["UserList"] as DataTable;
        //    }
        //    set { Session["UserList"] = value; }
        //}
        public EGMNGS.Model.CylinderPositionSet CylinderPositionSetObj
        {
            get
            {
                if (ViewState["CylinderPositionSet"] == null)
                {
                    return new EGMNGS.Model.CylinderPositionSet();
                }
                return ViewState["CylinderPositionSet"] as EGMNGS.Model.CylinderPositionSet;
            }
            set { ViewState["CylinderPositionSet"] = value; }
        }

        private string actionMode
        {
            get { return ViewState["ActionMode"] as String; }
            set { ViewState["ActionMode"] = value; }
        }


        #endregion

        #region 事件方法

        protected void Page_Load(object sender, EventArgs e)
        {
            this.PageControl1.pageHandler += new EventHandler(pager_PageChanged);
            if (!IsPostBack)
            {
                //  UserList = ComServies.GetAllUserInfo();
                CylinderPositionSetObj = new EGMNGS.Model.CylinderPositionSet();
                DropDownListBinder();

            }
        }

        /// <summary>
        /// 绑定下拉列
        /// </summary>
        private void DropDownListBinder()
        {
            //EGMNGS.BLL.SYS_CODE sysCodeBll = new EGMNGS.BLL.SYS_CODE();
            //DataSet dtCFStatus = sysCodeBll.GetList("CODE_TYPE='GPStatus' ORDER BY DISPLAY_ORDER");

            //this.ddlCFStatus.DataSource = dtCFStatus.Tables[0];
            //this.ddlCFStatus.DataTextField = "CODE_CHI_DESC";
            //this.ddlCFStatus.DataValueField = "CODE";
            //this.ddlCFStatus.DataBind();

            //this.ddlCFStatus.Items.Insert(0, string.Empty);
        }




        /// <summary>
        /// 绑定数据，分页
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void pager_PageChanged(object sender, EventArgs e)
        {
            DataBindGrid();
        }

        /// <summary>
        /// 绑定数据源
        /// </summary>
        private void DataBindGrid()
        {

            string sql = "1=1 ";

            if (!string.IsNullOrWhiteSpace(CylinderPositionSetObj.PositionCode))
            {
                sql += string.Format(" and PositionCode='{0}'", CylinderPositionSetObj.PositionCode);
            }
            else if (!string.IsNullOrWhiteSpace(CylinderPositionSetObj.PositionName))
            {
                sql += string.Format(" and PositionName='{0}'", CylinderPositionSetObj.PositionName);
            }
            else if (!string.IsNullOrWhiteSpace(CylinderPositionSetObj.positionRemarks))
            {
                sql += string.Format(" and positionRemarks='{0}'", CylinderPositionSetObj.positionRemarks);
            }


            int count = bll.GetRecordCount(sql);
            DataSet ds = bll.GetListByPage(sql, "CREATED_DATE", PageControl1.PageIndex, PageControl1.PageSize);
            ControlBindHelper.BindRepeaterList(ds.Tables[0], rp_Item);
            this.PageControl1.RecordCount = Convert.ToInt32(count);
        }
        /// <summary>
        /// 绑定数据到对象
        /// </summary>
        private void DataBinder()
        {
            //  Model.CylinderCode = this.txtCylinderCode.Text;
            //   Model.RemarksDesc = this.txtRemarksDesc.Text;
        }

        /// <summary>
        /// 初始化控件
        /// </summary>
        private void InitControl()
        {
            //  txtCylinderCode.Focus();
            //  this.txtCylinderCode.Text = Model.CylinderCode;
            //  this.txtRemarksDesc.Text = Model.RemarksDesc;
            //   this.ddlCFStatus.SelectedValue = Model.Status;
            //  this.txtClearManOID.Text = GetUserName(Model.ClearManOID);
            //  this.txtClearDate.Text = Model.ClearDate == null ? string.Empty : Model.ClearDate.Value.ToShortDateString();
        }

        /// <summary>
        /// 获取用户名
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        //private string GetUserName(string userId)
        //{
        //    DataRow[] dr = UserList.Select(string.Format("User_ID='{0}'", userId));
        //    if (dr.Length > 0)
        //    {
        //        return dr[0][1].ToString();
        //    }
        //    return string.Empty;
        //}

        private void ClearControl()
        {

        }

        #endregion

        #region 按钮事件

        protected void btnSubmit_Click(object sender, EventArgs e)
        {

        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            DataBindGrid();
        }

        /// <summary>
        /// 获取当前对象
        /// </summary>
        private void ShowMaxMode()
        {

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (CylinderPositionSetObj.OID > 0)//编辑
                {
                    CylinderPositionSetObj.LAST_UPD_BY = User.UserName.ToString();
                    CylinderPositionSetObj.LAST_UPD_DATE = DateTime.Now;
                    bll.Update(CylinderPositionSetObj);
                }
                else//添加
                {
                    CylinderPositionSetObj.CREATED_BY = User.UserName.ToString();
                    CylinderPositionSetObj.LAST_UPD_DATE = DateTime.Now;
                    bll.Add(CylinderPositionSetObj);
                }

                ShowMsgHelper.AlertMsg("保存成功");

            }
            catch (ArgumentOutOfRangeException ex)
            {
                string[] errs = CommonHelper.GetValue(ex.Message, "@").Split(':');
                ShowMsgHelper.ExecuteScript(string.Format("ChangeCss($('#txt{0}'),'长度不能超{1}字')", errs[0], errs[1]));
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            CylinderPositionSetObj = new EGMNGS.Model.CylinderPositionSet();
            CylinderPositionSetObj.PositionName = "新建库区名称";
            CylinderPositionSetObj.CREATED_BY = User.UserName.ToString();
        }


        protected void btnDel_Click(object sender, EventArgs e)
        {
            if (bll.Delete(CylinderPositionSetObj.OID))
            {
                ShowMsgHelper.AlertMsg("删除成功！");
            }
            else
            {
                ShowMsgHelper.AlertMsg("删除失败！");
            }
        }

        protected void rp_Item_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            int oid = Convert.ToInt32(e.CommandArgument);

            CylinderPositionSetObj = bll.GetModel(oid);
        }
        #endregion
    }
}
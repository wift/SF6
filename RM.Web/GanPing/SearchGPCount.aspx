﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SearchGPCount.aspx.cs"
    Inherits="RM.Web.GanPing.SearchGPCount" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>钢瓶数据查询</title>
    <link href="/Themes/Styles/Site.css" rel="stylesheet" type="text/css" />
    <script src="/Themes/Scripts/jquery-1.8.2.min.js" type="text/javascript"></script>
    <script src="/Themes/Scripts/FunctionJS.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {
            $("html").css("overflow", "hidden");
            $("body").css("overflow", "hidden");
            iframeresize();
            Loading(true);

        })

        /**自应高度**/
        function iframeresize() {
            resizeU();
            $(window).resize(resizeU);
            function resizeU() {
                var iframeMain = $(window).height();
                $("#iframeMainContent").height(iframeMain - 59);
            }
        }

        function LoadTab(url) {
            $("#target_right").attr("src", url);
        }
        //        onbeforeunload = function myfunction() {
        //            Session["CylinderCode"] = "";
        //        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table width="100%">
            <colgroup>
                <col width="11%" />
                <col width="11%" />
                <col width="11%" />
                <col width="11%" />
                <col width="11%" />
                <col width="11%" />
                <col width="11%" />
                <col width="11%" />
            </colgroup>
            <tr>
                <td class="inner_cell_right">
                    钢瓶编码：
                </td>
                <td>
                    <asp:TextBox ID="txtCylinderCode" runat="server" Width="100%" />
                </td>
                <td class="inner_cell_right">
                    钢瓶编码：
                </td>
                <td>
                    <%= Model.CylinderCode %>
                </td>
                <td class="inner_cell_right">
                    钢瓶钢印号：
                </td>
                <td>
                    <%=Model.CylinderSealNo %>
                </td>
                <td class="inner_cell_right">
                    有效日期：
                </td>
                <td>
                    <%= Model.EffectiveDateString
                    %>
                </td>
            </tr>
            <tr>
                <td class="inner_cell_right">
                    钢瓶钢印号：
                </td>
                <td>
                    <asp:TextBox ID="txtCylinderSealNo" runat="server" Width="100%"></asp:TextBox>
                </td>
                <td class="inner_cell_right">
                    自编编号：
                </td>
                <td>
                    <%=Model.CylinderSourse %>
                </td>
                <td class="inner_cell_right">
                    钢瓶容量（kg）：
                </td>
                <td>
                    <%=DictCylinderCapacity[Model.CylinderCapacity??string.Empty]%>
                </td>
                <td class="inner_cell_right">
                    检定日期:
                </td>
                <td>
                    <%=Model.ManufactureDateString%>
            </tr>
            <tr>
                <td style="text-align: center;" colspan="2">
                    <asp:Button ID="btnSearch" Width="50%" runat="server" Text="查询" OnClick="btnSearch_Click" />
                </td>
                <td class="inner_cell_right">
                    所属单位 :
                </td>
                <td>
                    <%= 
                        Manufacturer
                    %>
                </td>
                <td class="inner_cell_right">
                    当前气体编码:
                </td>
                <td>
                    <%=Model.CurGasCode%>
                </td>
                <td class="inner_cell_right">
                    钢瓶状态:
                </td>
                <td>
                    <%=GPStatus[Model.Status??string.Empty]%>
                </td>
            </tr>
        </table>
    </div>
    <div class="btnbarcontetn" style="margin-bottom: 1px;">
        <div style="float: left;">
            <table style="padding: 0px; margin: 0px; height: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td id="menutab" style="vertical-align: bottom;">
                        <div id="tab0" class="Tabsel1" onclick="GetTabClick1(this);LoadTab('RecordGPFIll.aspx');">
                            钢瓶充气记录</div>
                        <div id="tab1" class="Tabremovesel1" onclick="GetTabClick1(this);LoadTab('RecordGPOUTIN.aspx');">
                            钢瓶出入库记录</div>
                        <div id="tab2" class="Tabremovesel1" onclick="GetTabClick1(this);LoadTab('RecordGPClear.aspx');">
                            钢瓶清洗记录</div>
                        <div id="tab3" class="Tabremovesel1" onclick="GetTabClick1(this);LoadTab('RecordGPSJ.aspx');">
                            钢瓶送检记录</div>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div id="iframeMainContent">
        <div class="iframeContent">
            <iframe id="target_right" name="target_right" scrolling="auto" frameborder="0" scrolling="yes"
                width="100%" height="100%" src="RecordGPFIll.aspx"></iframe>
        </div>
    </div>
    </form>
</body>
</html>

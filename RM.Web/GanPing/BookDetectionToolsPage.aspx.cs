﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using RM.Common.DotNetCode;
using System.Data;
using RM.Common.DotNetUI;
using RM.Web.App_Code;
using EGMNGS.Common;
using System.Web.UI.HtmlControls;

namespace RM.Web.GanPing
{
    public partial class BookDetectionToolsPage : PageBase
    {
        #region 属性字段

        EGMNGS.BLL.BookDetectionTools bll = new EGMNGS.BLL.BookDetectionTools();
        private DataTable UserList
        {
            get
            {
                return ViewState["UserList"] as DataTable;
            }
            set { ViewState["UserList"] = value; }
        }
        private EGMNGS.Model.BookDetectionTools Model
        {
            get
            {
                if (ViewState["BookDetectionTools"] == null)
                {
                    return new EGMNGS.Model.BookDetectionTools();
                }
                return ViewState["BookDetectionTools"] as EGMNGS.Model.BookDetectionTools;
            }
            set { ViewState["BookDetectionTools"] = value; }
        }
        private string actionMode
        {
            get { return ViewState["ActionMode"] as String; }
            set { ViewState["ActionMode"] = value; }
        }
        public string PowerSupplyCode
        {
            get { return ViewState["PowerSupplyCode"] as String; }
            set { ViewState["PowerSupplyCode"] = value; }
        }
        public string PowerSupplyName
        {
            get { return ViewState["PowerSupplyName"] as String; }
            set { ViewState["PowerSupplyName"] = value; }
        }
        private string EffectDatetime
        {
            get
            {
                if (ViewState["EffectDatetime"] == null)
                {
                    ViewState["EffectDatetime"] = string.Empty;
                }

                return ViewState["EffectDatetime"] as String;
            }
            set { ViewState["EffectDatetime"] = value; }
        }
        #endregion

        #region 事件方法

        protected void Page_Load(object sender, EventArgs e)
        {
            this.PageControl1.pageHandler += new EventHandler(pager_PageChanged);
            if (!IsPostBack)
            {
                UserList = ComServies.GetAllUserInfo();
                // string[] orgIdOrgName = ComServies.GetPowerApplyByUserID(User.UserId.ToString());
                //PowerSupplyCode = orgIdOrgName[0];
                //PowerSupplyName = orgIdOrgName[1];
                DropDownListBinder();
            }
        }

        /// <summary>
        /// 绑定下拉列
        /// </summary>
        private void DropDownListBinder()
        {
            EGMNGS.BLL.SYS_CODE sysCodeBll = new EGMNGS.BLL.SYS_CODE();
            DataSet dsToolType = sysCodeBll.GetList("CODE_TYPE='ToolsType' ORDER BY DISPLAY_ORDER");

            this.ddlToolType.DataSource = dsToolType.Tables[0];
            this.ddlToolType.DataTextField = "CODE_CHI_DESC";
            this.ddlToolType.DataValueField = "CODE";
            this.ddlToolType.DataBind();
            this.ddlToolType.Items.Insert(0, string.Empty);

            DataSet dtCFStatus = sysCodeBll.GetList("CODE_TYPE='CFStatus' ORDER BY DISPLAY_ORDER");

            this.ddlCFStatus.DataSource = dtCFStatus.Tables[0];
            this.ddlCFStatus.DataTextField = "CODE_CHI_DESC";
            this.ddlCFStatus.DataValueField = "CODE";
            this.ddlCFStatus.DataBind();
        }


        /// <summary>
        /// 绑定后激发事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void rp_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Label lblStatus = e.Item.FindControl("lblStatus") as Label;
                if (lblStatus != null)
                {
                    if (lblStatus.Text.Length > 0)
                    {
                        lblStatus.Text = this.ddlCFStatus.Items.FindByValue(lblStatus.Text).Text;

                    }
                }

                Label lblToolType = e.Item.FindControl("lblToolType") as Label;
                if (lblToolType != null)
                {
                    if (lblToolType.Text.Length > 0)
                    {
                        lblToolType.Text = this.ddlToolType.Items.FindByValue(lblToolType.Text).Text;

                    }
                }
                Label lblEffectiveDate = e.Item.FindControl("lblEffectiveDate") as Label;
                if (lblEffectiveDate != null)
                {
                    DateTime dtime = Convert.ToDateTime(lblEffectiveDate.Text);

                    int diffDays = DateTimeHelper.DiffDays(dtime, DateTime.Now);
                    if (diffDays <= 30)
                    {
                        Literal Literal1 = e.Item.FindControl("Literal1") as Literal;
                        Literal1.Text = "style=\"background-color:Red;\"";
                    }
                }



            }
        }

        /// <summary>
        /// 绑定数据，分页
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void pager_PageChanged(object sender, EventArgs e)
        {
            DataBindGrid();
        }

        /// <summary>
        /// 绑定数据源
        /// </summary>
        private void DataBindGrid()
        {
            int count = bll.GetRecordCount("1=1");
            DataSet ds = bll.GetListByPage("1=1", EffectDatetime, PageControl1.PageIndex, PageControl1.PageSize);
            ControlBindHelper.BindRepeaterList(ds.Tables[0], rp_Item);
            this.PageControl1.RecordCount = Convert.ToInt32(count);
        }
        /// <summary>
        /// 绑定数据到对象
        /// </summary>
        private void DataBinder()
        {
            Model.ToolCode = this.txtToolCode.Text;
            Model.ToolType = this.ddlToolType.SelectedValue;
            Model.ToolName = this.txtToolName.Text;
            Model.SpecType = this.txtSpecType.Text;
            Model.FactoryCode = this.txtFactoryCode.Text;
            if (this.txtEffectiveDate.Text.Trim().Length > 0)
            {
                Model.EffectiveDate = DateTime.Parse(this.txtEffectiveDate.Text);
            }

            Model.LAST_UPD_BY = this.txtLAST_UPD_BY.Text;

        }

        /// <summary>
        /// 初始化控件
        /// </summary>
        private void InitControl()
        {
            this.txtToolCode.Text = Model.ToolCode;
            this.ddlToolType.SelectedValue = Model.ToolType;
            this.txtToolName.Text = Model.ToolName;
            this.txtSpecType.Text = Model.SpecType;
            this.txtFactoryCode.Text = Model.FactoryCode;
            if (Model.EffectiveDate != null)
            {
                this.txtEffectiveDate.Text = Model.EffectiveDate.Value.ToShortDateString();
            }

            this.ddlCFStatus.SelectedValue = Model.Status;
            this.txtRegistrantoid.Text = GetUserName(Model.RegistantsOID);
            if (Model.RegistantsDate != null)
            {
                this.txtRegistrantDate.Text = Model.RegistantsDate.Value.ToShortDateString();
            }
            this.txtLAST_UPD_BY.Text = Model.LAST_UPD_BY;

        }

        /// <summary>
        /// 获取用户名
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        private string GetUserName(string userId)
        {
            DataRow[] dr = UserList.Select(string.Format("User_ID='{0}'", userId));
            if (dr.Length > 0)
            {
                return dr[0][1].ToString();
            }
            return string.Empty;
        }

        private void ClearControl()
        {
            Model = new EGMNGS.Model.BookDetectionTools();
            InitControl();
        }
        #endregion

        #region 按钮事件

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            Model.Status = "1";
            if (bll.Update(Model))
            {
                ShowMsgHelper.Alert("提交成功！");
                DataBindGrid();
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            DataBindGrid();
        }

        /// <summary>
        /// 获取当前对象
        /// </summary>
        private void ShowMaxMode()
        {
            Model = bll.GetModel(bll.GetMaxId() - 1);
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {


                if (actionMode == "Add")
                {
                    DataBinder();
                    Model.RegistantsOID = User.UserId.ToString();

                    int count = bll.GetRecordCount(string.Format("ToolCode='{0}'", Model.ToolCode));
                    if (count > 0)
                    {
                        ShowMsgHelper.showWarningMsg("工具编号已经存在!");
                        return;
                    }

                    if (bll.Add(Model) > 0)
                    {
                        actionMode = string.Empty;
                    }

                    DataBindGrid();
                    ShowMaxMode();
                }
                else if (actionMode == "Edit")
                {
                    //if (bll.GetModel(Model.OID).Status == "1")
                    //{
                    //    ShowMsgHelper.Alert_Wern("“已确认”状态无法修改数据！");
                    //    return;
                    //}

                    DataBinder();
                    if (bll.Update(Model))
                    {
                        EGMNSShowMsg.ShowEditMsgSuccess();
                        actionMode = string.Empty;
                    }
                    DataBindGrid();
                }
            }
            catch (ArgumentOutOfRangeException ex)
            {
                string[] errs = CommonHelper.GetValue(ex.Message, "@").Split(':');
                ShowMsgHelper.ExecuteScript(string.Format("ChangeCss($('#txt{0}'),'长度不能超{1}字')", errs[0], errs[1]));
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            actionMode = "Add";
            Model = new EGMNGS.Model.BookDetectionTools(actionMode);
            Model.RegistantsDate = DateTime.Now;
            Model.RegistantsOID = User.UserId.ToString();
            InitControl();
        }


        protected void btnDel_Click(object sender, EventArgs e)
        {
            if (bll.GetModel(Model.OID).Status == "1")
            {
                ShowMsgHelper.Alert_Wern("“已确认”状态不可以删除");
            }
            else
            {
                bll.Delete(Model.OID);
                Model = new EGMNGS.Model.BookDetectionTools();
                InitControl();
                DataBindGrid();
                ShowMsgHelper.Alert("删除成功！");
            }


        }

        protected void rp_Item_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            int oid = Convert.ToInt32(e.CommandArgument);

            Model = bll.GetModel(oid);
            InitControl();
            actionMode = "Edit";
        }
        #endregion

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            if (EffectDatetime == "EffectiveDate desc")
            {
                EffectDatetime = "EffectiveDate asc";
            }
            else
            {
                EffectDatetime = "EffectiveDate desc";
            }
            DataBindGrid();
        }
    }
}
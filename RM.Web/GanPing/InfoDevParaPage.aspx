﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="InfoDevParaPage.aspx.cs"
    Inherits="RM.Web.GanPing.InfoDevParaPage" EnableEventValidation="false" ClientIDMode="AutoID" %>

<%@ Register Src="~/UserControl/PageControl.ascx" TagName="PageControl" TagPrefix="uc1" %>
<%@ Register Src="~/UserControl/LoadButton.ascx" TagName="LoadButton" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>设备台账信息</title>
    <link href="/Themes/Styles/Site.css" rel="stylesheet" type="text/css" />
    <script src="/Themes/Scripts/Validator/JValidator.js" type="text/javascript"></script>
    
    <script src="../Scripts/jquery-1.4.1.min.js"></script>
    <script src="/Themes/Scripts/jquery.pullbox.js" type="text/javascript"></script>
    <script src="/Themes/Scripts/FunctionJS.js" type="text/javascript"></script>
    <script src="/Themes/Scripts/DatePicker/WdatePicker.js" type="text/javascript"></script>
     <script src="../Scripts/uploadify/jquery.uploadify.min.js" type="text/javascript"></script>
    <script type="text/javascript">



        //添加
        function add() {
            document.getElementById('<%= this.btnAdd.ClientID %>').click();
        }
        //查询
        function search() {

            document.getElementById('<%= this.btnSearch.ClientID %>').click();
        }
        //删除
        function Delete() {
            showConfirmMsg("此操作不可恢复，您确定要删除吗？", function (r) {
                if (r) {
                    document.getElementById('<%= this.btnDel.ClientID %>').click();

                }
            });
        }
        //保存
        function SaveForm() {
            document.getElementById('<%= this.btnSave.ClientID %>').click();
        }
        //提交
        function Submit() {
            document.getElementById('<%= this.btnSubmit.ClientID %>').click();
        }


        $(function () {
            InitControl();
        })
        function InitControl() {
            $(".div-body").PullBox({ dv: $(".div-body"), obj: $("#table1").find("tr") });
            divresize(190);

            $('#uploadify2').uploadify({
                'formData': {
                    'action': 'ImportInfoDevParaPage'
                },
                'swf': '../Scripts/uploadify/uploadify.swf',
                'uploader': 'UploadHandler.ashx',
                'buttonText': '导入',
                'width': 40,
                'height': 25,
                'queueID': true,
                'fileTypeExts': '*.xls;*.xlsx',
                onUploadComplete: function myfunction(file) {

                    alert('导入成功');
                    window.location.reload();
                }
            });


          
        }


        function detail()
        {
          var  oid = $("#OID").val();
            var url = '../GanPing/InfoDevParaDetails.aspx';
            top.openDialog(url + "?oid=" + oid, 'InfoDevParaDetails', '详细', 1250, 320, 50, 50);
        }

        function ShowDialogSelectFunc()
        {
            var url = '../GanPing/InfoDevParaDetails.aspx';
            top.openDialog(url + "?oid=" + oid, 'InfoDevParaDetails', '详细', 1250, 320, 50, 50);
           
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional">
        <ContentTemplate>
            <table width="100%">
                <colgroup>
                    <col width="11%" />
                    <col width="11%" />
                    <col width="11%" />
                    <col width="11%" />
                    <col width="11%" />
                    <col width="11%" />
                    <col width="11%" />
                    <col width="11%" />
                </colgroup>
                <tr>
                    <td class="inner_cell_right">
                        设备编码：
                    </td>
                    <td>
                        <asp:TextBox ID="txtsDevCode" runat="server" checkexpession="NotNull" datacol="yes"
                            err="此项" />
                       <asp:HiddenField runat="server" ID="OID"/>
                    </td>
                    <td class="inner_cell_right">
                        设备名称：
                    </td>
                    <td>
                        <asp:TextBox ID="txtsDevName" runat="server" checkexpession="NotNull" datacol="yes"
                            err="此项" />
                    </td>
                    <td class="inner_cell_right">
                        出厂日期：
                    </td>
                    <td>
                        <asp:TextBox ID="txtPurchaseDate" runat="server" onfocus="WdatePicker()" checkexpession="NotNull"
                            datacol="yes" err="此项" />
                    </td>
                    <td class="inner_cell_right">
                        供电局名称：
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlsPowerSupplyCode" runat="server" Width="100%" AutoPostBack="true"
                            OnSelectedIndexChanged="ddlPowerSupplyName_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td class="inner_cell_right">
                        运行编号：
                    </td>
                    <td>
                        <asp:TextBox ID="txtsRunCode" runat="server" checkexpession="NotNull" datacol="yes"
                            err="此项" />
                    </td>
                    <td class="inner_cell_right">
                        设备型号：
                    </td>
                    <td>
                        <asp:TextBox ID="txtsDevType" runat="server" checkexpession="NotNull" datacol="yes"
                            err="此项" />
                    </td>
                    <td class="inner_cell_right">
                        投运日期：
                    </td>
                    <td>
                        <asp:TextBox ID="txtsPutDate" runat="server" onfocus="WdatePicker()" checkexpession="NotNull"
                            datacol="yes" err="此项" />
                    </td>
                    <td class="inner_cell_right">
                        变电站名称：
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlsConvertStationCode" runat="server" Width="100% " CssClass="select"
                            checkexpession="NotNull" datacol="yes" err="此项">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td class="inner_cell_right">
                        设备分类：
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlsDevClass" runat="server" CssClass="select" Width="100%"
                            checkexpession="NotNull" datacol="yes" err="此项">
                        </asp:DropDownList>
                    </td>
                    <td class="inner_cell_right">
                        额定气量(kg)：
                    </td>
                    <td>
                        <asp:TextBox ID="txtRatedQL" runat="server" checkexpession="NotNull" datacol="yes"
                            err=" 此项" />
                    </td>
                    <td class="inner_cell_right">
                        退运日期：
                    </td>
                    <td>
                        <asp:TextBox ID="txtReturnDate" runat="server" onfocus="WdatePicker()" />
                    </td>
                    <td class="inner_cell_right">
                        登记人：
                    </td>
                    <td>
                        <asp:TextBox ID="txtRegistrantoid" runat="server" Enabled="false" />
                    </td>
                </tr>
                <tr>
                    <td height="28" class="inner_cell_right">
                        电压等级：
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlsVoltageLevel" runat="server" CssClass="select" Width="100%"
                            checkexpession="NotNull" datacol="yes" err="此项">
                        </asp:DropDownList>
                    </td>
                    <td class="inner_cell_right">
                        额定气压（Mpa）：
                    </td>
                    <td>
                        <asp:TextBox ID="txtRatedQY" runat="server" checkexpession="NotNull" datacol="yes"
                            err="此项" />
                    </td>
                    <td class="inner_cell_right">
                        生产厂家：
                    </td>
                    <td>
                        <asp:TextBox ID="txtsManufacturer" runat="server" checkexpession="NotNull" datacol="yes"
                            err="此项" />
                    </td>
                    <td class="inner_cell_right">
                        登记日期：
                    </td>
                    <td>
                        <asp:TextBox ID="txtRegistrantDate" runat="server" Enabled="false" />
                    </td>
                </tr>
                <tr>
                    <td class="inner_cell_right">
                        设备编号：
                    </td>
                    <td>
                        <asp:TextBox ID="txtsDevNum" runat="server" checkexpession="NotNull" datacol="yes"
                            err=" 此项" />
                    </td>
                    <td class="inner_cell_right">
                        状态：
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlCFStatus" runat="server" Width="100%" Enabled="false">
                        </asp:DropDownList>
                    </td>
                    <td class="inner_cell_right">
                        额定电压（kV）
                    </td>
                    <td>
                        <asp:TextBox ID="txtkv" runat="server" checkexpession="NotNull" datacol="yes" err=" 此项" />
                    </td>
                    <td class="inner_cell_right">
                        额定电流（A）
                    </td>
                    <td>
                        <asp:TextBox ID="txtA" runat="server" checkexpession="NotNull" datacol="yes" err=" 此项" />
                    </td>
                </tr>
                <tr>
                    <td class="inner_cell_right">
                        全路径：
                    </td>
                   
                    <td>
                        <asp:TextBox ID="txtOutFullPath" runat="server" datacol="yes" />
                    </td>
                    <td class="inner_cell_left">
                        <input id="ShowDialogSelect" value="选择路径" type="button" onclick="ShowDialogSelectFunc()" />
                    </td>
                    <td>
                    </td>
                    <td class="inner_cell_right">
                        是否在线检测：
                    </td>
                    <td>
                         
                        <asp:DropDownList ID="ddlIsOnlineCheck" runat="server" Width="100%" >
                              <asp:ListItem Text="否" Value="1" Selected="True"></asp:ListItem>
                            <asp:ListItem Text="是" Value="0"></asp:ListItem>
                              
                        </asp:DropDownList>
                    </td>
                    <td class="inner_cell_right">
                        开断电流（kA）
                    <td>
                        <asp:TextBox ID="txtkA" runat="server" checkexpession="NotNull" datacol="yes" err=" 此项" />
                    </td>
                </tr>
            </table>
            <div style="text-align: left;">
                <uc2:LoadButton ID="LoadButton1" runat="server" /><a class="button green"><span id="uploadify2"/></a>
            </div>
            <div style="text-align: right; display: none;">
                <asp:Button ID="btnSubmit" runat="server" OnClick="btnSubmit_Click" Text="btnSubmit_Click" />
                <asp:Button ID="btnSearch" runat="server" OnClick="btnSearch_Click" Text="btnSearch_Click" />
                <asp:Button ID="btnSave" runat="server" OnClick="btnSave_Click" Text="btnSave_Click"
                    OnClientClick="return CheckDataValid('#form1');" />
                <asp:Button ID="btnAdd" runat="server" OnClick="btnAdd_Click" Text="btnAdd_Click" />
                <asp:Button ID="btnDel" runat="server" OnClick="btnDel_Click" Text="btnDel_Click" />
            </div>
            <div class="div-body">
                <table id="table1" class="grid" singleselect="true">
                    <colgroup>
                        <col width="15%" />
                        <col width="15%" />
                        <col width="15%" />
                        <col width="15%" />
                        <col width="10%" />
                        <col width="10%" />
                        <col width="10%" />
                        <col width="10%" />
                    </colgroup>
                    <thead>
                        <tr>
                            <td style="text-align: center;">
                                供电局名称
                            </td>
                            <td style="text-align: center;">
                                变电站名称
                            </td>
                            <td style="text-align: center;">
                                设备名称
                            </td>
                            <td style="text-align: center;">
                                设备型号
                            </td>
                            <td style="text-align: center;">
                                运行编号
                            </td>
                            <td style="text-align: center;">
                                额定气量
                            </td>
                            <td style="text-align: center;">
                                额定气压
                            </td>
                            <td style="text-align: center;">
                                状态
                            </td>
                        </tr>
                    </thead>
                    <tbody>
                        <asp:Repeater ID="rp_Item" runat="server" OnItemDataBound="rp_ItemDataBound" OnItemCommand="rp_Item_ItemCommand">
                            <ItemTemplate>
                                <tr>
                                    <td style="text-align: center;">
                                        <asp:LinkButton ID="LBtnDel" runat="server" OnClientClick="selectrow(this)" CommandArgument='<%#Eval("OID") %>'
                                            CommandName="del"><%#Eval("PowerSupplyName")%></asp:LinkButton>
                                    </td>
                                    <td style="text-align: center;">
                                        <%#Eval("ConvertStationName")%></a>
                                    </td>
                                    <td style="text-align: center;">
                                        <%#Eval("DevName")%>
                                    </td>
                                    <td style="text-align: center;">
                                        <%#Eval("DevType")%>
                                    </td>
                                    <td style="text-align: center;">
                                        <%#Eval("RunCode")%>
                                    </td>
                                    <td style="text-align: center;">
                                        <%#Eval("RatedQL")%>
                                    </td>
                                    <td style="text-align: center;">
                                        <%#Eval("RatedQY")%>
                                    </td>
                                    <td style="text-align: center;">
                                        <%# DictEGMNS["CFStatus"][Eval("Status").ToString()]%>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </tbody>
                </table>
            </div>
            <uc1:PageControl ID="PageControl1" runat="server" />
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="ddlsPowerSupplyCode" EventName="SelectedIndexChanged" />
            <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnAdd" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnDel" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>
    </form>
</body>
</html>

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using RM.Common.DotNetCode;
using System.Data;
using RM.Common.DotNetUI;
using RM.Web.App_Code;
using EGMNGS.Common;
using EGMNGS.Model;

namespace RM.Web.GanPing
{

    public partial class ApplDetectionGasPage : PageBase
    {
        #region 属性字段

        EGMNGS.BLL.ApplDetectionGas bll = new EGMNGS.BLL.ApplDetectionGas();
        EGMNGS.BLL.DetailsApplDetectionGas DetailsBLL = new EGMNGS.BLL.DetailsApplDetectionGas();
        /// <summary>
        /// 需要添加的数据
        /// </summary>
        private Decimal? TotalAmountGas
        {
            get
            {
                if (ViewState["Decimal"] == null)
                {
                    return new Decimal(); ;
                }
                return ViewState["Decimal"] as Decimal?;
            }
            set { ViewState["Decimal"] = value; }
        }

        /// <summary>
        /// 需要添加的数据
        /// </summary>
        private List<DetailsApplDetectionGas> AddList
        {
            get
            {
                if (ViewState["AddList"] == null)
                {
                    return new List<DetailsApplDetectionGas>();
                }
                return ViewState["AddList"] as List<DetailsApplDetectionGas>;
            }
            set { ViewState["AddList"] = value; }
        }

        private DataTable UserList
        {
            get
            {
                return ViewState["UserList"] as DataTable;
            }
            set { ViewState["UserList"] = value; }
        }
        private EGMNGS.Model.ApplDetectionGas Model
        {
            get
            {
                if (ViewState["ApplDetectionGas"] == null)
                {
                    return new EGMNGS.Model.ApplDetectionGas();
                }
                return ViewState["ApplDetectionGas"] as EGMNGS.Model.ApplDetectionGas;
            }
            set { ViewState["ApplDetectionGas"] = value; }
        }
        private string actionMode
        {
            get { return ViewState["ActionMode"] as String; }
            set { ViewState["ActionMode"] = value; }
        }
        public string PowerSupplyCode
        {
            get { return ViewState["PowerSupplyCode"] as String; }
            set { ViewState["PowerSupplyCode"] = value; }
        }
        public string PowerSupplyName
        {
            get { return ViewState["PowerSupplyName"] as String; }
            set { ViewState["PowerSupplyName"] = value; }
        }
        public DataTable DetailsTable
        {
            get
            {
                if (ViewState["DetailsTable"] == null)
                {
                    return new DataTable();
                }
                return ViewState["DetailsTable"] as DataTable;
            }
            set { ViewState["DetailsTable"] = value; }
        }
        private string actionModeDetails
        {
            get
            {
                if (ViewState["actionModeDetails"] == null)
                {
                    return string.Empty;
                }
                return ViewState["actionModeDetails"] as String;
            }
            set { ViewState["actionModeDetails"] = value; }
        }

        private List<DetailsApplDetectionGas> UpdateList
        {
            get
            {
                if (ViewState["UpdateList"] == null)
                {
                    return new List<DetailsApplDetectionGas>();
                }
                return ViewState["UpdateList"] as List<DetailsApplDetectionGas>;
            }
            set { ViewState["UpdateList"] = value; }
        }


        #endregion

        #region 事件方法

        protected void Page_Load(object sender, EventArgs e)
        {
            this.PageControl1.pageHandler += new EventHandler(pager_PageChanged);
            if (!IsPostBack)
            {
                UserList = ComServies.GetAllUserInfo();
                string[] orgIdOrgName = ComServies.GetPowerApplyByUserID(User.UserId.ToString());
                PowerSupplyCode = orgIdOrgName[0];
                PowerSupplyName = orgIdOrgName[1];
                DropDownListBinder();

            }
        }

        /// <summary>
        /// 绑定下拉列
        /// </summary>
        private void DropDownListBinder()
        {
            EGMNGS.BLL.InfoPowerSupplyConvertStation bll = new EGMNGS.BLL.InfoPowerSupplyConvertStation();
            DataSet dsPowerSupplyCode = bll.GetList(string.Format("PowerSupplyCode='{0}'", PowerSupplyCode));
            this.ddlConvertStationName.DataSource = dsPowerSupplyCode.Tables[0];
            this.ddlConvertStationName.DataTextField = "ConvartStationName";
            this.ddlConvertStationName.DataValueField = "CovnertStationCode";
            this.ddlConvertStationName.DataBind();
            this.ddlConvertStationName.Items.Insert(0, string.Empty);


            EGMNGS.BLL.SYS_CODE sysCodeBll = new EGMNGS.BLL.SYS_CODE();
            DataSet dtCFStatus = sysCodeBll.GetList("CODE_TYPE='TaskStatus' ORDER BY DISPLAY_ORDER");

            this.ddlStatus.DataSource = dtCFStatus.Tables[0];
            this.ddlStatus.DataTextField = "CODE_CHI_DESC";
            this.ddlStatus.DataValueField = "CODE";
            this.ddlStatus.DataBind();

            this.ddlStatus.Items.Insert(0, string.Empty);


            DataSet dtCheckPJType = sysCodeBll.GetList("CODE_TYPE='CheckPJTypeStatus' ORDER BY DISPLAY_ORDER");

            this.ddlCheckPJType.DataSource = dtCheckPJType.Tables[0];
            this.ddlCheckPJType.DataTextField = "CODE_CHI_DESC";
            this.ddlCheckPJType.DataValueField = "CODE";
            this.ddlCheckPJType.DataBind();
        }


        /// <summary>
        /// 绑定后激发事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void rp_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Label lblStatus = e.Item.FindControl("lblStatus") as Label;
                if (lblStatus != null)
                {
                    if (lblStatus.Text.Length > 0)
                    {
                        lblStatus.Text = this.ddlStatus.Items.FindByValue(lblStatus.Text).Text;
                    }
                }

            }
        }

        /// <summary>
        /// 绑定数据，分页
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void pager_PageChanged(object sender, EventArgs e)
        {
            DataBindGrid();
        }

        /// <summary>
        /// 绑定数据源
        /// </summary>
        private void DataBindGrid()
        {
            int count = bll.GetRecordCount(string.Format("PowerSupplyCode='{0}'", PowerSupplyCode));
            DataSet ds = bll.GetListByPage(string.Format("PowerSupplyCode='{0}'", PowerSupplyCode), string.Empty, PageControl1.PageIndex, PageControl1.PageSize);
            ControlBindHelper.BindRepeaterList(ds.Tables[0], rp_Item);
            this.PageControl1.RecordCount = Convert.ToInt32(count);
        }
        /// <summary>
        /// 绑定数据到对象
        /// </summary>
        private void DataBinder()
        {

            Model.ApplCode = this.tbAppCode.Text;
            Model.DevCode = this.txtDevCode.Text;

            Model.ConvertStationCode = this.ddlConvertStationName.SelectedValue;
            Model.convertStationName = this.ddlConvertStationName.SelectedItem.Text;
            Model.DevName = this.txtDevName.Text;
            Model.DevNo = this.tbDevNo.Text;
            Model.InspectionUnit = this.tbInspectionUnit.Text;
            Model.Contacts = this.tbContacts.Text;
            Model.OfficeTel = this.tbOfficeTel.Text;
            Model.PhoneNum = this.tbPhoneNum.Text;
            Model.SampliInspituation = this.tbSampliInspituation.Text;
            if (this.tbAmountDetection.Text.Trim().Length > 0)
            {
                Model.AmountDetection = Convert.ToDecimal(this.tbAmountDetection.Text);
            }
            Model.CheckPJType = this.ddlCheckPJType.SelectedValue;
        }

        /// <summary>
        /// 初始化控件
        /// </summary>
        private void InitControl()
        {

            this.tbAppCode.Text = Model.ApplCode;

            this.tbPowerSupplyName.Text = Model.PowerSupplyName;
            this.ddlConvertStationName.SelectedValue = Model.ConvertStationCode;
            this.txtDevName.Text = Model.DevName;
            this.tbDevNo.Text = Model.DevNo;
            this.tbInspectionUnit.Text = Model.InspectionUnit;
            this.tbContacts.Text = Model.Contacts;
            this.tbOfficeTel.Text = Model.OfficeTel;
            this.tbPhoneNum.Text = Model.PhoneNum;
            this.tbSampliInspituation.Text = Model.SampliInspituation;
            this.tbAmountDetection.Text = Model.AmountDetection.ToString();
            this.ddlStatus.SelectedValue = Model.Status;
            this.txtRegistantsoid.Text = GetUserName(Model.RegistantsOID);
            if (Model.RegistantsDate != null)
            {
                this.txtRegistantsDate.Text = Model.RegistantsDate.Value.ToShortDateString();
            }

            BindddlDevCode();
            this.txtDevCode.Text = Model.DevCode;
            this.ddlCheckPJType.SelectedValue = Model.CheckPJType;
        }

        /// <summary>
        /// 获取用户名
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        private string GetUserName(string userId)
        {
            DataRow[] dr = UserList.Select(string.Format("User_ID='{0}'", userId));
            if (dr.Length > 0)
            {
                return dr[0][1].ToString();
            }
            return string.Empty;
        }

        private void ClearControl()
        {
            Model = new EGMNGS.Model.ApplDetectionGas();
            InitControl();
        }
        #endregion

        #region 按钮事件

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            if (Convert.ToInt32(Model.Status) >= 1)
            {
                ShowMsgHelper.Alert("已经提交！");
                return;
            }

            GetCheckBox();

            Model.Status = "1";
            Model.AmountDetection = TotalAmountGas;
            if (ComServies.UpdateRWDJ(Model.Status, Model.AmountDetection, Model.ApplCode))
            {
                //同步更新《钢瓶台账》时没更新【钢瓶状态】改为“已充待检气”。
                ShowMsgHelper.Alert("提交成功！");
                InitControl();
                DataBindGrid();
                GetDetailsOfRecycleGas();

            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            DataBindGrid();
        }

        /// <summary>
        /// 获取当前对象
        /// </summary>
        private void ShowMaxMode()
        {
            Model = bll.GetModel(bll.GetMaxId() - 1);
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (actionMode == "Add")
                {
                    DataBinder();

                    int count = bll.GetRecordCount(string.Format("ApplCode='{0}'", Model.ApplCode));
                    if (count > 0)
                    {
                        ShowMsgHelper.showWarningMsg("申请编号已经存在!");
                        return;
                    }

                    if (bll.Add(Model) > 0)
                    {
                        actionMode = string.Empty;
                    }

                    DataBindGrid();
                    ShowMaxMode();
                }
                else if (actionMode == "Edit")
                {
                    if (bll.GetModel(Model.OID).Status == "1")
                    {
                        ShowMsgHelper.Alert_Wern("“已确认”状态无法修改数据！");
                        return;

                    }
                    DataBinder();
                    if (bll.Update(Model))
                    {
                        EGMNSShowMsg.ShowEditMsgSuccess();
                    }
                    DataBindGrid();
                }
            }
            catch (ArgumentOutOfRangeException ex)
            {
                string[] errs = CommonHelper.GetValue(ex.Message, "@").Split(':');
                ShowMsgHelper.ExecuteScript(string.Format("ChangeCss($('#txt{0}'),'长度不能超{1}字')", errs[0], errs[1]));
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            actionMode = "Add";
            Model = new EGMNGS.Model.ApplDetectionGas(actionMode);
            Model.PowerSupplyCode = PowerSupplyCode;
            Model.PowerSupplyName = PowerSupplyName;
            Model.RegistantsDate = DateTime.Now;
            Model.RegistantsOID = User.UserId.ToString();
            Model.Status = "0";
            InitControl();

            GetDetailsOfRecycleGas();
        }


        protected void btnDel_Click(object sender, EventArgs e)
        {
            if (Model.Status != "0")
            {
                ShowMsgHelper.Alert_Wern("“已提交”状态不可以删除");
            }
            else
            {
                bll.Delete(Model.OID);
                Model = new EGMNGS.Model.ApplDetectionGas();
                InitControl();
                DataBindGrid();
                ShowMsgHelper.Alert("删除成功！");
            }
        }

        protected void rp_Item_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            int oid = Convert.ToInt32(e.CommandArgument);

            Model = bll.GetModel(oid);
            InitControl();
            actionMode = "Edit";

            //绑定明细表
            GetDetailsOfRecycleGas();
        }
        #endregion

        #region Details

        protected void pager_PageChangedDetails(object sender, EventArgs e)
        {
            GetDetailsOfRecycleGas();
        }

        private void GetDetailsOfRecycleGas()
        {
            DetailsTable = DetailsBLL.GetList(string.Format("ApplCode='{0}'", Model.ApplCode)).Tables[0];
            int count = DetailsBLL.GetRecordCount(string.Format("ApplCode='{0}'", Model.ApplCode));
            ControlBindHelper.BindRepeaterList(DetailsTable, rptDetails);
        }

        protected void btnAddDetails_Click(object sender, EventArgs e)
        {
            if (Model.Status == "1")
            {
                ShowMsgHelper.Alert_Wern(Model.ApplCode + "已经提交!");
                return;
            }
            //首先，恢复数据源
            RetriRepeter();
            DataTable dt = DetailsTable;
            // LoadTestData(dt);
            DataRow row = dt.NewRow();
            row[2] = string.Empty;
            row[3] = string.Empty;
            row[4] = 0.0;
            dt.Rows.Add(row);

            rptDetails.DataSource = dt;
            rptDetails.DataBind();


            actionModeDetails = "Add";
        }
        protected void btnDelDetails_Click(object sender, EventArgs e)
        {
            try
            {
                if (Model.Status == "1")
                {
                    ShowMsgHelper.Alert_Wern(Model.ApplCode + "已经提交!无法删除！");
                    return;
                }
                else
                {
                    string strID = HiddenField1.Value;
                    DetailsBLL.DeleteList(strID);
                    ShowMsgHelper.Alert("删除成功！");
                    GetDetailsOfRecycleGas();
                }
            }
            catch (Exception ex)
            {
                ShowMsgHelper.AlertMsg("删除失败！" + ex.Message);
            }
        }

        protected void btnSaveDetails_Click(object sender, EventArgs e)
        {
            try
            {
                if (Model.Status == "1")
                {
                    ShowMsgHelper.Alert_Wern(Model.ApplCode + "已经提交!无法修改或添加！");
                    return;
                }


                GetCheckBox();
                if (actionModeDetails.Equals("Add"))
                {
                    if (AddList.Count > 0)
                    {
                        //批量添加
                        foreach (var item in AddList)
                        {
                            DetailsBLL.Add(item);
                        }
                        AddList.Clear();
                    }

                    ShowMsgHelper.Alert("添加成功！");
                    actionModeDetails = string.Empty;
                }

                if (UpdateList.Count > 0)
                {
                    //批量修改
                    foreach (var item in UpdateList)
                    {
                        DetailsBLL.Update(item);
                    }

                    UpdateList.Clear();
                }
                GetDetailsOfRecycleGas();
            }
            catch (Exception ex)
            {
                ShowMsgHelper.AlertMsg("添加失败！");
                throw;
            }
        }

        protected void rptDetails_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                CheckBox cb = e.Item.FindControl("checkbox") as CheckBox;
                if (cb.ToolTip.Length == 0)
                {
                    cb.Checked = true;
                }
            }
        }


        /// <summary>
        /// 
        /// </summary>
        private void GetCheckBox()
        {
            UpdateList = new List<DetailsApplDetectionGas>();
            AddList = new List<DetailsApplDetectionGas>();
            TotalAmountGas = new Decimal();

            for (int i = 0; i < this.rptDetails.Items.Count; i++)
            {
                TextBox txtAmountGastemp = this.rptDetails.Items[i].FindControl("txtAmountGas") as TextBox;
                TotalAmountGas += Convert.ToDecimal(txtAmountGastemp.Text);

                System.Web.UI.WebControls.CheckBox checkbox = (System.Web.UI.WebControls.CheckBox)rptDetails.Items[i].FindControl("checkbox");

                TextBox txtAmountGas = this.rptDetails.Items[i].FindControl("txtAmountGas") as TextBox;
                TextBox txtProduceNum = this.rptDetails.Items[i].FindControl("txtProduceNum") as TextBox;
                TextBox txtManufacturer = this.rptDetails.Items[i].FindControl("txtManufacturer") as TextBox;

                if (checkbox.Checked == true && checkbox.ToolTip.Length > 0)//更新
                {
                    //TextBox txtCylinderCode = this.rptDetails.Items[i].FindControl("txtCylinderCode") as TextBox;
                    //TextBox txtGasCode = this.rptDetails.Items[i].FindControl("txtGasCode") as TextBox;


                    DetailsApplDetectionGas Obj = new EGMNGS.BLL.DetailsApplDetectionGas().GetModel(int.Parse(checkbox.ToolTip));
                    // Obj.OID = int.Parse(checkbox.ToolTip);
                    // Obj.ApplCode = Model.ApplCode;
                    // Obj.CylinderCode = txtCylinderCode.Text;
                    if (txtAmountGas.Text.Trim().Length > 0)
                    {
                        Obj.AmountGas = decimal.Parse(txtAmountGas.Text);
                    }
                    Obj.ProduceNum = txtProduceNum.Text;
                    Obj.Manufacturer = txtManufacturer.Text;

                    //if (txtGasCode.Text.Trim().Length > 0)
                    //{
                    //    Obj.GasCode = txtGasCode.Text;
                    //}

                    UpdateList.Add(Obj);
                }
                else if (checkbox.Checked == true && checkbox.ToolTip.Length == 0)//添加
                {
                    //TextBox txtCylinderCode = this.rptDetails.Items[i].FindControl("txtCylinderCode") as TextBox;
                    //TextBox txtGasCode = this.rptDetails.Items[i].FindControl("txtGasCode") as TextBox;
                    //  TextBox txtAmountGas = this.rptDetails.Items[i].FindControl("txtAmountGas") as TextBox;

                    DetailsApplDetectionGas Obj = new EGMNGS.Model.DetailsApplDetectionGas();
                    Obj.ApplCode = Model.ApplCode;
                    // Obj.CylinderCode = txtCylinderCode.Text;
                    if (txtAmountGas.Text.Trim().Length > 0)
                    {
                        Obj.AmountGas = decimal.Parse(txtAmountGas.Text);
                    }
                    //if (txtGasCode.Text.Trim().Length > 0)
                    //{
                    //    Obj.GasCode = txtGasCode.Text;
                    //}
                    Obj.ProduceNum = txtProduceNum.Text;
                    Obj.Manufacturer = txtManufacturer.Text;

                    AddList.Add(Obj);
                }
            }
        }

        #endregion

        EGMNGS.BLL.InfoDevPara bllInfoDevPara = new EGMNGS.BLL.InfoDevPara();
        protected void ddlConvertStationName_SelectedIndexChanged(object sender, EventArgs e)
        {
          //  BindddlDevCode();

        }

        private void BindddlDevCode()
        {
            //DataSet ds = bllInfoDevPara.GetList(string.Format("ConvertStationCode='{0}'", this.ddlConvertStationName.SelectedValue));
            //this.ddlDevCode.Items.Clear();
            //this.ddlDevCode.DataSource = ds.Tables[0];
            //this.ddlDevCode.DataTextField = "DevCode";
            //this.ddlDevCode.DataValueField = "OID";
            //this.ddlDevCode.DataBind();
            //this.ddlDevCode.Items.Insert(0, string.Empty);
        }
        protected void ddlDevCode_SelectedIndexChanged(object sender, EventArgs e)
        {
            //string oid = this.ddlDevCode.SelectedValue;
            //InfoDevPara infoDevP = bllInfoDevPara.GetModel(int.Parse(oid));
            //this.txtDevName.Text = infoDevP.DevName;
            //this.tbDevNo.Text = infoDevP.DevType;
        }

        private void RetriRepeter()
        {
            for (int i = 0; i < this.rptDetails.Items.Count; i++)
            {
                TextBox txtAmountGas = this.rptDetails.Items[i].FindControl("txtAmountGas") as TextBox;
                TextBox txtProduceNum = this.rptDetails.Items[i].FindControl("txtProduceNum") as TextBox;
                TextBox txtManufacturer = this.rptDetails.Items[i].FindControl("txtManufacturer") as TextBox;

                DetailsTable.Rows[i][4] = Convert.ToDecimal(txtAmountGas.Text);

                DetailsTable.Rows[i]["ProduceNum"] = txtProduceNum.Text;
                DetailsTable.Rows[i]["Manufacturer"] = txtManufacturer.Text;
            }
        }
    }
}
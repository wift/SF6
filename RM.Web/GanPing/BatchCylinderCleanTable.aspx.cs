﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using RM.Common.DotNetUI;
using EGMNGS.Model;
using RM.Web.App_Code;
using RM.Common.DotNetBean;

namespace RM.Web.GanPing
{
    public partial class BatchCylinderCleanTable : Page
    {
        EGMNGS.BLL.BookCylinderInfo bll = new EGMNGS.BLL.BookCylinderInfo();
        EGMNGS.BLL.CylinderCleanTable bllClean = new EGMNGS.BLL.CylinderCleanTable();

        /// <summary>
        /// 获取平台所有字典
        /// </summary>
        public Dictionary<string, Dictionary<string, string>> DictEGMNS
        {
            get
            {
                if (Session["DictEGMNS"] == null)
                {
                    Dictionary<string, Dictionary<string, string>> tempDictEGMNS = new Dictionary<string, Dictionary<string, string>>();


                    EGMNGS.BLL.SYS_CODE sysCodeBll = new EGMNGS.BLL.SYS_CODE();
                    EGMNGS.BLL.SYS_CODE_TYPE sysCodeTypeBll = new EGMNGS.BLL.SYS_CODE_TYPE();
                    List<EGMNGS.Model.SYS_CODE_TYPE> listSYS_CODE_TYPE = sysCodeTypeBll.GetModelList("1=1");
                    List<EGMNGS.Model.SYS_CODE> listSYS_CODE = sysCodeBll.GetModelList("1=1");
                    foreach (var item in listSYS_CODE_TYPE)
                    {
                        Dictionary<string, string> tempCode = new Dictionary<string, string>();
                        foreach (var itemCode in listSYS_CODE.Where(o => o.CODE_TYPE == item.CODE_TYPE))
                        {
                            tempCode.Add(itemCode.CODE, itemCode.CODE_CHI_DESC);
                        }
                        tempCode.Add("", "");
                        tempDictEGMNS.Add(item.CODE_TYPE, tempCode);
                    }


                    return tempDictEGMNS;
                }
                return Session["DictEGMNS"] as Dictionary<string, Dictionary<string, string>>;
            }
            set { Session["DictEGMNS"] = value; }
        }
        private Dictionary<string, string> lsCFStatus
        {
            get
            {
                if (Session["GPStatus"] != null)
                {
                    return Session["GPStatus"] as Dictionary<string, string>;
                }
                return new Dictionary<string, string>();
            }

            set
            {
                Session["GPStatus"] = value;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                EGMNGS.BLL.SYS_CODE sysCodeBll = new EGMNGS.BLL.SYS_CODE();
                lsCFStatus = sysCodeBll.GetModelList("CODE_TYPE='GPStatus' ORDER BY DISPLAY_ORDER").ToDictionary(o => o.CODE, o => o.CODE_CHI_DESC);

                DataBindGrid();

            }
        }
        /// <summary>
        /// 绑定数据源
        /// </summary>
        private void DataBindGrid()
        {
            //int count = bll.GetRecordCount("1=1");
            DataSet ds = bll.GetList("Status='0' ORDER BY CylinderCode");
            ControlBindHelper.BindRepeaterList(ds.Tables[0], rp_Item);
        }
        protected void btnBatchAdd_Click(object sender, EventArgs e)
        {
            if (HiddenField1.Value.Length > 0)
            {
                List<BookCylinderInfo> lb = bll.GetModelList(string.Format("Status='0' and oid in ({0})", HiddenField1.Value));

                foreach (var item in lb)
                {
                    CylinderCleanTable tempCylinderCleanTable = new CylinderCleanTable();
                    tempCylinderCleanTable.CylinderCode = item.CylinderCode;
                    tempCylinderCleanTable.ClearDate = DateTime.Now;
                    tempCylinderCleanTable.Status = "0";
                    tempCylinderCleanTable.ClearManOID = RequestSession.GetSessionUser().UserId.ToString();

                    bllClean.Add(tempCylinderCleanTable);
                }

                ShowMsgHelper.AlertMsg("批量添加成功！");
            }
        }

        /// <summary>
        /// 绑定后激发事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void rp_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {

                Label lblStatus = e.Item.FindControl("lblStatus") as Label;
                if (lblStatus != null)
                {
                    if (lblStatus.Text.Length > 0)
                    {
                        lblStatus.Text = lsCFStatus[lblStatus.Text];

                    }
                }
            }
        }
    }
}
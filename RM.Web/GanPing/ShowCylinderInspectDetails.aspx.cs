﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using RM.Common.DotNetUI;
using EGMNGS.Common;
using EGMNGS.Model;
using Microsoft.Office.Interop.Excel;
using RM.Common.DotNetFile;
using RM.Web.App_Code;

namespace RM.Web.GanPing
{
    public partial class ShowCylinderInspectDetails : System.Web.UI.Page
    {
        EGMNGS.BLL.CylinderInspectTable bll = new EGMNGS.BLL.CylinderInspectTable();
        /// <summary>
        /// 获取平台所有字典
        /// </summary>
        public Dictionary<string, Dictionary<string, string>> DictEGMNS
        {
            get
            {
                if (Session["DictEGMNS"] == null)
                {
                    Dictionary<string, Dictionary<string, string>> tempDictEGMNS = new Dictionary<string, Dictionary<string, string>>();


                    EGMNGS.BLL.SYS_CODE sysCodeBll = new EGMNGS.BLL.SYS_CODE();
                    EGMNGS.BLL.SYS_CODE_TYPE sysCodeTypeBll = new EGMNGS.BLL.SYS_CODE_TYPE();
                    List<EGMNGS.Model.SYS_CODE_TYPE> listSYS_CODE_TYPE = sysCodeTypeBll.GetModelList("1=1");
                    List<EGMNGS.Model.SYS_CODE> listSYS_CODE = sysCodeBll.GetModelList("1=1");
                    foreach (var item in listSYS_CODE_TYPE)
                    {
                        Dictionary<string, string> tempCode = new Dictionary<string, string>();
                        foreach (var itemCode in listSYS_CODE.Where(o => o.CODE_TYPE == item.CODE_TYPE))
                        {
                            tempCode.Add(itemCode.CODE, itemCode.CODE_CHI_DESC);
                        }
                        tempCode.Add("", "");
                        tempDictEGMNS.Add(item.CODE_TYPE, tempCode);
                    }


                    return tempDictEGMNS;
                }
                return Session["DictEGMNS"] as Dictionary<string, Dictionary<string, string>>;
            }
            set { Session["DictEGMNS"] = value; }
        }

        private System.Data.DataTable UserList
        {
            get
            {
                return ViewState["UserList"] as System.Data.DataTable;
            }
            set { ViewState["UserList"] = value; }
        }

        private Dictionary<string, string> ddlIsPass
        {
            get
            {
                return ViewState["ddlIsPass"] as Dictionary<string, string>;
            }
            set { ViewState["ddlIsPass"] = value; }
        }

        private Dictionary<string, string> ddlCylinderCapacity
        {
            get
            {
                return ViewState["ddlCylinderCapacity"] as Dictionary<string, string>;
            }
            set { ViewState["ddlCylinderCapacity"] = value; }
        }

        public System.Data.DataTable ResultTB
        {
            get
            {
                if (ViewState["ResultTB"] == null)
                {
                    return new System.Data.DataTable();
                }
                return ViewState["ResultTB"] as System.Data.DataTable;
            }
            set { ViewState["ResultTB"] = value; }

        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                UserList = ComServies.GetAllUserInfo();
                InitData();
                DataBindGrid();
            }
        }

        private void InitData()
        {
            ddlIsPass = new Dictionary<string, string>();
            ddlCylinderCapacity = new Dictionary<string, string>();

            EGMNGS.BLL.SYS_CODE sysCodeBll = new EGMNGS.BLL.SYS_CODE();
            List<SYS_CODE> dtIsPass = sysCodeBll.GetModelList("CODE_TYPE='IsPass' ORDER BY DISPLAY_ORDER");
            foreach (SYS_CODE item in dtIsPass)
            {
                ddlIsPass.Add(item.CODE, item.CODE_CHI_DESC);
            }

            List<SYS_CODE> dtCylinderCapacity = sysCodeBll.GetModelList("CODE_TYPE='GPRL' ORDER BY DISPLAY_ORDER");
            foreach (SYS_CODE item in dtCylinderCapacity)
            {
                ddlCylinderCapacity.Add(item.CODE, item.CODE_CHI_DESC);
            }
        }



        /// <summary>
        /// 绑定数据源
        /// </summary>
        private void DataBindGrid()
        {
            DataSet ds = bll.GetList("c.Status='5' ORDER BY c.RegistantsDate DESC");
            ControlBindHelper.BindRepeaterList(ds.Tables[0], rp_Item);
            ResultTB = ds.Tables[0];
        }

        /// <summary>
        /// 绑定后激发事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void rp_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {

                System.Web.UI.WebControls.Label lblSendCheckOID = e.Item.FindControl("lblSendCheckOID") as System.Web.UI.WebControls.Label;
                if (lblSendCheckOID != null)
                {
                    if (lblSendCheckOID.Text.Length > 0)
                    {
                        lblSendCheckOID.Text = GetUserName(lblSendCheckOID.Text);

                    }
                }


                System.Web.UI.WebControls.Label lblCylinderCapacity = e.Item.FindControl("lblCylinderCapacity") as System.Web.UI.WebControls.Label;
                if (lblCylinderCapacity != null)
                {
                    if (lblCylinderCapacity.Text.Length > 0)
                    {
                        lblCylinderCapacity.Text = ddlCylinderCapacity[lblCylinderCapacity.Text];

                    }
                }


                System.Web.UI.WebControls.Label lblIsPass = e.Item.FindControl("lblIsPass") as System.Web.UI.WebControls.Label;
                if (lblIsPass != null)
                {
                    if (lblIsPass.Text.Length > 0)
                    {
                        lblIsPass.Text = ddlIsPass[lblIsPass.Text];
                    }
                }
            }
        }

        /// <summary>
        /// 获取用户名
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        private string GetUserName(string userId)
        {
            DataRow[] dr = UserList.Select(string.Format("User_ID='{0}'", userId));
            if (dr.Length > 0)
            {
                return dr[0][1].ToString();
            }
            return string.Empty;
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            MouldExportExcel mee = new MouldExportExcel();
            try
            {
                string physicalPath = Request.PhysicalApplicationPath + "Xls\\";
                Workbook sh = mee.OpenExcel(physicalPath + "钢瓶送检清单.xls");
                Worksheet ws = sh.Worksheets[1] as Worksheet;

                #region 设置值

                for (int i = 0; i < ResultTB.Rows.Count; i++)
                {
                    DataRow item = ResultTB.Rows[i];
                    mee.SetCellValue(ws, 3 + i, 1, i + 1);
                    mee.SetCellValue(ws, 3 + i, 2, item["CylinderCode"].ToString());
                    mee.SetCellValue(ws, 3 + i, 3, item["CylinderSealNo"].ToString());
                    mee.SetCellValue(ws, 3 + i, 4, DictEGMNS["GPRL"][item["CylinderCapacity"].ToString()]);
                    if (!Convert.IsDBNull(item["OriginalEffectiveDate"]))
                    {
                        mee.SetCellValue(ws, 3 + i, 5, Convert.ToDateTime(item["OriginalEffectiveDate"]).ToShortDateString());

                    }

                    if (!Convert.IsDBNull(item["NewEffectiveDate"]))
                    {
                        mee.SetCellValue(ws, 3 + i, 6, Convert.ToDateTime(item["NewEffectiveDate"]).ToShortDateString());

                    }

                    //  mee.SetCellValue(ws, 3 + i, 6, Convert.ToDateTime(item["NewEffectiveDate"]).ToShortDateString());
                    mee.SetCellValue(ws, 3 + i, 7, DictEGMNS["IsPass"][item["IsPass"].ToString()]);
                    mee.SetCellValue(ws, 3 + i, 8, GetUserName(item["SendCheckOID"].ToString()));

                    if (!Convert.IsDBNull(item["SendCheckDate"]))
                    {
                        mee.SetCellValue(ws, 3 + i, 9, Convert.ToDateTime(item["SendCheckDate"]).ToShortDateString());

                    }

                    //mee.SetCellValue(ws, 3 + i, 9, Convert.ToDateTime(item["SendCheckDate"]).ToShortDateString());
                    mee.SetCellValue(ws, 3 + i, 10, item["Remarks"].ToString());

                    string range = string.Format("A{0}:I{0}", 3 + i);
                    var MyRng = ws.get_Range(range, Type.Missing);
                    MyRng.Font.Size = 10;
                    MyRng.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
                    MyRng.VerticalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
                    MyRng.Borders.get_Item(XlBordersIndex.xlEdgeLeft).Weight = Microsoft.Office.Interop.Excel.XlBorderWeight.xlThin;
                    MyRng.Borders.get_Item(XlBordersIndex.xlEdgeTop).Weight = Microsoft.Office.Interop.Excel.XlBorderWeight.xlThin;
                    MyRng.Borders.get_Item(XlBordersIndex.xlEdgeRight).Weight = Microsoft.Office.Interop.Excel.XlBorderWeight.xlThin;
                    MyRng.Borders.get_Item(XlBordersIndex.xlEdgeBottom).Weight = Microsoft.Office.Interop.Excel.XlBorderWeight.xlThin;
                    MyRng.Borders.get_Item(XlBordersIndex.xlInsideVertical).Weight = Microsoft.Office.Interop.Excel.XlBorderWeight.xlThin;
                    MyRng.Borders.get_Item(XlBordersIndex.xlInsideHorizontal).Weight = Microsoft.Office.Interop.Excel.XlBorderWeight.xlThin;


                }
                #endregion





                string fileName = "钢瓶送检清单temp.xls";
                string pathFile = physicalPath + fileName;
                mee.SaveExcel(pathFile);
                FileDownHelper.DownLoadold(@"~/Xls/" + fileName);

            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                mee.Dispose();

            }
        }

    }
}
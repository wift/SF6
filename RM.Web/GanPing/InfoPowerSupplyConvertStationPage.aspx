﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="InfoPowerSupplyConvertStationPage.aspx.cs"
    Inherits="RM.Web.GanPing.InfoPowerSupplyConvertStationPage" EnableEventValidation="false" ClientIDMode="AutoID" %>

<%@ Register Src="~/UserControl/PageControl.ascx" TagName="PageControl" TagPrefix="uc1" %>
<%@ Register Src="~/UserControl/LoadButton.ascx" TagName="LoadButton" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>设备场地管理</title>
    <link href="/Themes/Styles/Site.css" rel="stylesheet" type="text/css" />
    <script src="/Themes/Scripts/Validator/JValidator.js" type="text/javascript"></script>
    <script src="/Themes/Scripts/jquery-1.8.2.min.js" type="text/javascript"></script>
    <script src="/Themes/Scripts/jquery.pullbox.js" type="text/javascript"></script>
    <link href="/Themes/Scripts/TreeView/treeview.css" rel="stylesheet" type="text/css" />
    <script src="/Themes/Scripts/TreeView/treeview.pack.js" type="text/javascript"></script>
    <script src="/Themes/Scripts/FunctionJS.js" type="text/javascript"></script>
    <script src="/Themes/Scripts/DatePicker/WdatePicker.js" type="text/javascript"></script>
    <script type="text/javascript">
        //添加
        function add() {
            document.getElementById('<%= this.btnAdd.ClientID %>').click();
        }
        //查询
        function search() {

            document.getElementById('<%= this.btnSearch.ClientID %>').click();
            setTimeout(InitControl, 500);

        }
        //删除
        function Delete() {
            showConfirmMsg("此操作不可恢复，您确定要删除吗？", function (r) {
                if (r) {
                    document.getElementById('<%= this.btnDel.ClientID %>').click();
                }
            });
        }
        //保存
        function SaveForm() {
            document.getElementById('<%= this.btnSave.ClientID %>').click();
        }

        //确认
        function Submit() {
            document.getElementById('<%= this.btnConfirm.ClientID %>').click();
        }

        $(function () {
            InitControl();

            treeAttrCss();
            $.lightTreeview.open('.strTree ul', 100);
        })

        function InitControl() {
            $(".div-body").PullBox({ dv: $(".div-body"), obj: $("#table1").find("tr") });
            divresize(100);
            FixedTableHeader("#table1", $(window).height() - 190);
        }

        function GetTable(code) {
            $("#ddlPowerSupplyName").val(code);
            search();
            //  LoadMc_CheckPointTemp_ChildsList(code, ConvertStationName)
            Loading(true);
        }
        $(document).ready(function myfunction() {

        });
    </script>

    <style type="text/css">
        html, body {
            margin: 0;
            height: 100%;
        }

        form {
            height: 100%;
            width: 100%;
        }

        #containt {
            float: left;
            height: -moz-calc(100% - 0px);
            height: -webkit-calc(100% - 00px);
            height: calc(100% - 0px);
            width: -moz-calc(100% - 0px);
            width: -webkit-calc(100% - 00px);
            width: calc(100% - 0px);
        }

        .Panel {
            float: left;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">

        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <div id="leftPanel" class="Panel" style="width: 15%; height: 100%; border-right-width: 1px; border-right-style: solid;">
            <ul class="strTree">
                <li>
                    <div title="广东电网公司">
                        广东电网公司
                    </div>
                    <ul>
                        <%=treeItem_Table.ToString() %>
                    </ul>
                </li>
            </ul>
        </div>
        <div id="rightPanel" class="Panel" style="width: 84%; height: 100%;">
            <asp:UpdatePanel ID="UpdatePanel" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional">
                <ContentTemplate>
                    <table width="100%">
                        <colgroup>
                            <col />
                            <col width="18%" />
                            <col width="14%" />
                            <col width="17%" />
                            <col width="15%" />
                            <col width="25%" />
                        </colgroup>
                        <tr>
                            <td class="inner_cell_right">变电站名称：
                            </td>
                            <td>
                                <asp:TextBox ID="txtConvartStationName" runat="server" checkexpession="NotNull" datacol="yes"
                                    err="此项"></asp:TextBox>
                            </td>
                            <td class="inner_cell_right">供电局名称：
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlPowerSupplyName" CssClass="select" runat="server" Width="100%"
                                    checkexpession="NotNull" datacol="yes" err="此项">
                                </asp:DropDownList>
                            </td>
                            <td class="inner_cell_right">登记人ID：
                            </td>
                            <td>
                                <asp:TextBox ID="txtRegistrantOID" runat="server" Enabled="false" />
                            </td>
                        </tr>
                        <tr>
                            <td class="inner_cell_right">变电站编码：
                            </td>
                            <td>
                                <asp:TextBox ID="txtCovnertStationCode" runat="server" checkexpession="NotNull" datacol="yes"
                                    err="此项"></asp:TextBox>
                            </td>
                            <td class="inner_cell_right">状态：
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlCFStatus" runat="server" Width="100%" Enabled="false">
                                </asp:DropDownList>
                            </td>
                            <td class="inner_cell_right">登记日期：
                            </td>
                            <td>
                                <asp:TextBox ID="txtRegistrantDate" runat="server" Enabled="false" />
                            </td>
                        </tr>
                        <tr>
                            <td class="inner_cell_right">在线检测系统：
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlIsOnlineCheck" runat="server" Width="100%">
                                       <asp:ListItem Text="否" Value="1" Selected="True"></asp:ListItem>
                                    <asp:ListItem Text="是" Value="0"></asp:ListItem>
                                 
                                </asp:DropDownList>
                            </td>

                            <td class="inner_cell_right">经度：
                            </td>
                            <td>
                                <asp:TextBox ID="txtLongitude" runat="server" />
                            </td>
                            <td class="inner_cell_right">纬度:
                            </td>
                            <td>
                                <asp:TextBox ID="txtLatitude" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="inner_cell_right">电磁环境系统：
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlIsMagnetic" runat="server" Width="100%">
                                     <asp:ListItem Text="否" Value="1" Selected="True"></asp:ListItem>
                                    <asp:ListItem Text="是" Value="0"></asp:ListItem>
                                   
                                </asp:DropDownList>
                            </td>

                            <td ></td>
                            <td></td>
                            <td ></td>
                            <td></td>
                        </tr>
                    </table>
                    <div style="text-align: left;">
                        <uc2:LoadButton ID="LoadButton1" runat="server" />
                    </div>
                    <div style="text-align: right; display: none;">
                        <asp:Button ID="btnSearch" runat="server" Text="btnSearch" OnClick="btnSearch_Click" />
                        <asp:Button ID="btnSave" runat="server" Text="btnSave" OnClick="btnSave_Click" OnClientClick="return CheckDataValid('#form1');" />
                        <asp:Button ID="btnAdd" runat="server" Text="btnAdd" OnClick="btnAdd_Click" />
                        <asp:Button ID="btnDel" runat="server" Text="btnDel" OnClick="btnDel_Click" />
                        <asp:Button ID="btnConfirm" runat="server" Text="btnConfirm" OnClick="btnConfirm_Click" />
                    </div>

                    <div class="div-body">


                        <table id="table1" class="grid" singleselect="true">
                            <colgroup>
                                <col width="20%" />
                                <col width="10%" />
                                <col width="10%" />
                                <col width="10%" />
                                <col width="10%" />
                                <col width="10%" />
                                <col width="10%" />
                                <col width="10%" />
                                <col width="10%" />
                            </colgroup>
                            <thead>
                                <tr>

                                    <td style="text-align: center;">变电站名称
                                    </td>
                                    <td style="text-align: center;">供电局名称
                                    </td>
                                     <td style="text-align: center;">在线检测系统
                                    </td>
                                     <td style="text-align: center;">在电磁环境系统
                                    </td>
                                     <td style="text-align: center;">经度
                                    </td>
                                     <td style="text-align: center;">纬度
                                    </td>
                                    <td style="text-align: center;">状态
                                    </td>
                                    <td style="text-align: center;">登记人
                                    </td>
                                    <td style="text-align: center;">登记日期
                                    </td>
                                </tr>
                            </thead>
                            <tbody>
                                <asp:Repeater ID="rp_Item" runat="server" OnItemCommand="rp_Item_ItemCommand" OnItemDataBound="rp_ItemDataBound">
                                    <ItemTemplate>
                                        <tr>

                                            <td style="text-align: center;">
                                                <asp:LinkButton ID="LBtnDel" runat="server" OnClientClick="selectrow(this)" CommandArgument='<%#Eval("OID") %>' CommandName="del"><%#Eval("ConvartStationName")%></asp:LinkButton>
                                            </td>
                                            <td style="text-align: center;">
                                                <%#Eval("PowerSupplyName")%>
                                            </td>
                                            <td style="text-align: center;">
                                                <%#Eval("IsOnlineCheck").ToString()=="0"?"是":"否"%>
                                            </td>
                                            <td style="text-align: center;">
                                                <%#Eval("IsMagnetic").ToString()=="0"?"是":"否"%>
                                            </td>
                                            <td style="text-align: center;">
                                                <%#Eval("Longitude")%>
                                            </td>
                                             <td style="text-align: center;">
                                                <%#Eval("Latitude")%>
                                            </td>
                                            <td style="text-align: center;">
                                                <asp:Label ID="lblStatus" runat="server" Text='<%#Eval("Status")%>'></asp:Label>
                                            </td>
                                            <td style="text-align: center;">
                                                <asp:Label ID="lblUserID" runat="server" Text='<%#Eval("Registrantoid")%>'></asp:Label>
                                            </td>
                                            <td style="text-align: center;">
                                                <%#Eval("RegistrantDate","{0:yyyy-MM-dd}")%>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </tbody>
                        </table>

                    </div>
                    <uc1:PageControl ID="PageControl1" runat="server" />
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
                    <asp:AsyncPostBackTrigger ControlID="btnAdd" EventName="Click" />
                    <asp:AsyncPostBackTrigger ControlID="btnDel" EventName="Click" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </form>
</body>
</html>

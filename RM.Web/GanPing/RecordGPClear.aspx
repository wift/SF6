﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RecordGPClear.aspx.cs"
    Inherits="RM.Web.GanPing.RecordGPClear" %>

<%@ Register Src="~/UserControl/PageControl.ascx" TagName="PageControl" TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>钢瓶清洗记录 </title>
    <link href="/Themes/Styles/Site.css" rel="stylesheet" type="text/css" />
    <script src="/Themes/Scripts/Validator/JValidator.js" type="text/javascript"></script>
    <script src="/Themes/Scripts/jquery-1.8.2.min.js" type="text/javascript"></script>
    <script src="/Themes/Scripts/jquery.pullbox.js" type="text/javascript"></script>
    <script src="/Themes/Scripts/FunctionJS.js" type="text/javascript"></script>
    <script src="/Themes/Scripts/DatePicker/WdatePicker.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function InitControl() {
            $(".div-body1").PullBox({ dv: $(".div-body1"), obj: $("#tableDetails").find("tr") });
            divresize(155);
        })
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div class="div-body1">
        <table id="tableDetails" class="grid1">
            <colgroup>
                <col width="25%" />
                <col width="25%" />
                <col width="25%" />
                <col width="25%" />
            </colgroup>
            <thead>
                <tr>
                    <td style="text-align: center;">
                        钢瓶编码
                    </td>
                    <td style="text-align: center;">
                        钢瓶状态
                    </td>
                    <td style="text-align: center;">
                        清洗人
                    </td>
                    <td style="text-align: center;">
                        清洗日期
                    </td>
                </tr>
            </thead>
            <tbody>
                <asp:Repeater ID="rp_Item" runat="server">
                    <ItemTemplate>
                        <tr>
                            <td style="text-align: center;">
                                <%#Eval("CylinderCode")%>
                            </td>
                            <td style="text-align: center;">
                                <%# GPStatus[Eval("Status").ToString()]%>
                            </td>
                            <td style="text-align: center;">
                                <%# GetUserName(Eval("ClearManOID").ToString())%>
                            </td>
                            <td style="text-align: center;">
                                <%#Eval("cleardate", "{0:yyyy-MM-dd}")%>
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
            </tbody>
        </table>
    </div>
    <uc1:PageControl ID="PageControl1" runat="server" />
    </form>
</body>
</html>

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using EGMNGS.Common;

namespace RM.Web.GanPing
{
    public partial class ShowDetailsInspectionBox : System.Web.UI.Page
    {
        private DataTable UserList
        {
            get
            {
                return ViewState["UserList"] as DataTable;
            }
            set { ViewState["UserList"] = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            UserList = ComServies.GetAllUserInfo();
            string key = Request["key"].ToString();

            if (!string.IsNullOrEmpty(key))
            {
                EGMNGS.Model.ApplRegGasQtyInspection model = new EGMNGS.BLL.ApplRegGasQtyInspection().GetModel(int.Parse(key));

                if (model != null)
                {

                    if (model.SF6Index != null)
                        lblSF6Index.Text = model.SF6Index.ToString();

                    if (model.AirQualityScoreIndex != null)
                        lblAirQualityScoreIndex.Text = model.AirQualityScoreIndex.ToString();
                    if (model.CF4Index != null)
                        lblCF4Index.Text = model.CF4Index.ToString();

                    if (model.WaterQualityScoreIndex != null)
                        lblWaterQualityScoreIndex.Text = model.WaterQualityScoreIndex.ToString();

                    if (model.WaterDewIndex != null)
                        lblWaterDewIndex.Text = model.WaterDewIndex.ToString();
                    if (model.HFIndex != null)
                        lblHFIndex.Text = model.HFIndex.ToString();

                    if (model.KSJHFIndex != null)
                        lblKSJHFIndex.Text = model.KSJHFIndex.ToString();

                    if (model.KWYSorceIndex != null)
                        lblKWYSorceIndex.Text = model.KWYSorceIndex.ToString();


                    if (model.SignerOID != null)
                        lblSignerOID.Text = GetUserName(model.SignerOID);
                    if (model.SignerDate != null)
                        lblSignerDate.Text = model.SignerDate.Value.ToShortDateString();
                    if (model.ApproverOID != null)
                        lblApproverOID.Text = GetUserName(model.ApproverOID);
                    if (model.ApproverDate != null)
                        lblApproverDate.Text = model.ApproverDate.Value.ToShortDateString();

                }
            }
        }

        /// <summary>
        /// 获取用户名
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        private string GetUserName(string userId)
        {
            DataRow[] dr = UserList.Select(string.Format("User_ID='{0}'", userId));
            if (dr.Length > 0)
            {
                return dr[0][1].ToString();
            }
            return string.Empty;
        }
    }
}
﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RecordGPSJ.aspx.cs" Inherits="RM.Web.GanPing.RecordGPSJ" %>

<%@ Register Src="~/UserControl/PageControl.ascx" TagName="PageControl" TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>钢瓶送检记录 </title>
    <link href="/Themes/Styles/Site.css" rel="stylesheet" type="text/css" />
    <script src="/Themes/Scripts/Validator/JValidator.js" type="text/javascript"></script>
    <script src="/Themes/Scripts/jquery-1.8.2.min.js" type="text/javascript"></script>
    <script src="/Themes/Scripts/jquery.pullbox.js" type="text/javascript"></script>
    <script src="/Themes/Scripts/FunctionJS.js" type="text/javascript"></script>
    <script src="/Themes/Scripts/DatePicker/WdatePicker.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function InitControl() {
            $(".div-body1").PullBox({ dv: $(".div-body1"), obj: $("#tableDetails").find("tr") });
            divresize(155);
        })
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div class="div-body1">
        <table id="tableDetails" class="grid1">
            <colgroup>
                <col width="11%" />
                <col width="11%" />
                <col width="11%" />
                <col width="11%" />
                <col width="11%" />
                <col width="11%" />
                <col width="11%" />
                <col width="11%" />
                <col width="12%" />
            </colgroup>
            <thead>
                <tr>
                    <td style="text-align: center;">
                        钢瓶编码
                    </td>
                    <td style="text-align: center;">
                        原有效日期
                    </td>
                    <td style="text-align: center;">
                        新有效日期
                    </td>
                    <td style="text-align: center;">
                        是否合格
                    </td>
                    <td style="text-align: center;">
                        送检人
                    </td>
                    <td style="text-align: center;">
                        送检日期
                    </td>
                    <td style="text-align: center;">
                        钢瓶状态
                    </td>
                    <td style="text-align: center;">
                        登记人
                    </td>
                    <td style="text-align: center;">
                        登记日期
                    </td>
                </tr>
            </thead>
            <tbody>
                <asp:Repeater ID="rp_Item" runat="server">
                    <ItemTemplate>
                        <tr>
                            <td style="text-align: center;">
                                <%#Eval("CylinderCode")%>
                            </td>
                            <td style="text-align: center;">
                                <%#Eval("OriginalEffectiveDate", "{0:yyyy-MM-dd}")%>
                            </td>
                            <td style="text-align: center;">
                                <%#Eval("NewEffectiveDate", "{0:yyyy-MM-dd}")%>
                            </td>
                            <td style="text-align: center;">
                                <%# DictIsPass[Eval("IsPass").ToString()]%>
                            </td>
                            <td style="text-align: center;">
                                <%#  GetUserName(Eval("SendCheckOID").ToString())%>
                            </td>
                            <td style="text-align: center;">
                                <%#Eval("SendCheckDate", "{0:yyyy-MM-dd}")%>
                            </td>
                            <td style="text-align: center;">
                                <%# GPStatus[Eval("Status").ToString()]%>
                            </td>
                            <td style="text-align: center;">
                                <%# GetUserName(Eval("RegistantsOID").ToString())%>
                            </td>
                            <td style="text-align: center;">
                                <%#Eval("RegistantsDate", "{0:yyyy-MM-dd}")%>
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
            </tbody>
        </table>
    </div>
    <uc1:PageControl ID="PageControl1" runat="server" />
    </form>
</body>
</html>

﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="HSBookGasFillPage.aspx.cs"
    Inherits="RM.Web.GanPing.HSBookGasFillPage" EnableEventValidation="false" ClientIDMode="AutoID" %>

<%@ Register Src="~/UserControl/PageControl.ascx" TagName="PageControl" TagPrefix="uc1" %>
<%@ Register Src="~/UserControl/LoadButton.ascx" TagName="LoadButton" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>回收气体充瓶管理</title>
    <link href="/Themes/Styles/Site.css" rel="stylesheet" type="text/css" />
    <script src="/Themes/Scripts/Validator/JValidator.js" type="text/javascript"></script>
    <script src="/Themes/Scripts/jquery-1.8.2.min.js" type="text/javascript"></script>
    <script src="/Themes/Scripts/jquery.pullbox.js" type="text/javascript"></script>
    <script src="/Themes/Scripts/FunctionJS.js" type="text/javascript"></script>
    <script type="text/javascript">
        //添加
        function add() {
            document.getElementById('<%= this.btnAdd.ClientID %>').click();
        }
        //查询
        function search() {

            document.getElementById('<%= this.btnSearch.ClientID %>').click();
        }
        //删除
        function Delete() {
            showConfirmMsg("此操作不可恢复，您确定要删除吗？", function (r) {
                if (r) {
                    document.getElementById('<%= this.btnDel.ClientID %>').click();
                }
            });
        }
        //保存
        function SaveForm() {
            document.getElementById('<%= this.btnSave.ClientID %>').click();
        }
        //提交
        function Submit() {
            document.getElementById('<%= this.btnSubmit.ClientID %>').click();
        }

        $(function () {
            InitControl();
        })

        function InitControl() {
            $(".div-body").PullBox({ dv: $(".div-body"), obj: $("#table1").find("tr") });
            divresize(150);

            FixedTableHeader("#table1", $(window).height() - 200);
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:HiddenField ID="HiddenField1" runat="server" />
            <table width="100%">
                <colgroup>
                    <col width="11%" />
                    <col width="11%" />
                    <col width="11%" />
                    <col width="11%" />
                    <col width="11%" />
                    <col width="11%" />
                    <col width="11%" />
                    <col width="11%" />
                </colgroup>
                <tr>
                    <td class="inner_cell_right">
                        充气编号：
                    </td>
                    <td>
                        <asp:TextBox ID="txtFillGasCode" runat="server" />
                    </td>
                    <td class="inner_cell_right">
                        气体类型：
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlGasType" runat="server" Width="100%" Enabled="false">
                        </asp:DropDownList>
                    </td>
                    <td class="inner_cell_right">
                        业务单号：
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlBusinessCode" runat="server" Width="100%" checkexpession="NotNull"
                            datacol="yes" err="此项" CssClass="select">
                        </asp:DropDownList>
                    </td>
                    <td class="inner_cell_right">
                        年度：
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlYear" runat="server">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td class="inner_cell_right">
                        钢瓶编码：
                    </td>
                    <td>
                        <asp:TextBox ID="txtCylinderCode" runat="server" AutoPostBack="true" OnTextChanged="txtCylinderCode_TextChanged" />
                    </td>
                    <td class="inner_cell_right">
                        气量（kg）：
                    </td>
                    <td>
                        <asp:TextBox ID="txtAmountGas" runat="server" checkexpession="NotNull" datacol="yes"
                            err="此项" />
                    </td>
                    <td class="inner_cell_right">
                        钢瓶状态：
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlCylinderStatus" runat="server" Width="100%" Enabled="false">
                        </asp:DropDownList>
                    </td>
                    <td class="inner_cell_right">
                        月度：
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlMonth" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="inner_cell_right">
                        状态：
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlStatus" runat="server" Width="100%" Enabled="false">
                        </asp:DropDownList>
                    </td>
                    <td class="inner_cell_right">
                    </td>
                    <td>
                    </td>
                    <td class="inner_cell_right">
                        登记人：
                    </td>
                    <td>
                        <asp:TextBox ID="txtRegistrantOID" runat="server" Enabled="false" />
                    </td>
                    <td class="inner_cell_right">
                        登记日期：
                    </td>
                    <td>
                        <asp:TextBox ID="txtRegistrantDate" runat="server" />
                    </td>
                </tr>
            </table>
            <div style="text-align: left;">
                <uc2:LoadButton ID="LoadButton1" runat="server" />
            </div>
            <div style="text-align: right; display: none;">
                <asp:Button ID="btnSubmit" runat="server" OnClick="btnSubmit_Click" Text="btnSubmit_Click" />
                <asp:Button ID="btnSearch" runat="server" OnClick="btnSearch_Click" Text="btnSearch_Click" />
                <asp:Button ID="btnSave" runat="server" OnClick="btnSave_Click" Text="btnSave_Click"
                    OnClientClick="return CheckDataValid('#form1');" />
                <asp:Button ID="btnAdd" runat="server" OnClick="btnAdd_Click" Text="btnAdd_Click" />
                <asp:Button ID="btnDel" runat="server" OnClick="btnDel_Click" Text="btnDel_Click" />
            </div>
            <div class="div-body">
                <table id="table1" class="grid">
                    <colgroup>
                        <col width="15%" />
                        <col width="15%" />
                        <col width="15%" />
                        <col width="15%" />
                        <col width="15%" />
                        <col width="10%" />
                        <col width="15%" />
                    </colgroup>
                    <thead>
                        <tr>
                            <%-- <td style="width: 20px; text-align: left;">
                        <label id="checkAllOff" onclick="CheckAllLine()" title="全选">
                            &nbsp;</label>
                    </td>--%>
                            <td style="text-align: center;">
                                充气编号
                            </td>
                            <td style="text-align: center;">
                                业务单号
                            </td>
                            <td style="text-align: center;">
                                钢瓶编码
                            </td>
                            <td style="text-align: center;">
                                气量
                            </td>
                            <td style="text-align: center;">
                                钢瓶状态
                            </td>
                            <td style="text-align: center;">
                                状态
                            </td>
                            <td style="text-align: center;">
                                登记日期
                            </td>
                        </tr>
                    </thead>
                    <tbody>
                        <asp:Repeater ID="rp_Item" runat="server" OnItemDataBound="rp_ItemDataBound" OnItemCommand="rp_Item_ItemCommand">
                            <ItemTemplate>
                                <tr>
                                    <td style="text-align: center;">
                                        <asp:LinkButton ID="LBtnDel" runat="server" OnClientClick="selectrow(this)" CommandArgument='<%#Eval("OID") %>'
                                            CommandName="del"><%#Eval("FillGasCode")%></asp:LinkButton>
                                    </td>
                                    <td style="text-align: center;">
                                        <%#Eval("BusinessCode")%>
                                    </td>
                                    <td style="text-align: center;">
                                        <%#Eval("CylinderCode")%>
                                    </td>
                                    <td style="text-align: center;">
                                        <%#Eval("AmountGas")%>
                                    </td>
                                    <td style="text-align: center;">
                                        <asp:Label ID="lblCylinderStatus" runat="server" Text='<%#Eval("CylinderStatus")%>'></asp:Label>
                                    </td>
                                    <td style="text-align: center;">
                                        <asp:Label ID="lblStatus" runat="server" Text='<%#Eval("Status")%>'></asp:Label>
                                    </td>
                                    <td style="text-align: center;">
                                        <%#Eval("RegistrantDate","{0:yyyy-MM-dd}")%>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </tbody>
                </table>
            </div>
            <uc1:PageControl ID="PageControl1" runat="server" />
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnAdd" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>
    </form>
</body>
</html>

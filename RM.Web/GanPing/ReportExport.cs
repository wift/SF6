﻿using EGMNGS.Model;
using RM.Common.DotNetFile;
using RM.Common.DotNetUI;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace RM.Web.GanPing
{
    public class ReportExport
    {

        public static void ComUsingByThreeExportByID(string strID)
        {
            EGMNGS.BLL.ApplRegGasQtyInspection bll = new EGMNGS.BLL.ApplRegGasQtyInspection();

            var modelList = bll.GetModelList(string.Format(" OID in({0})", strID));



            MouldExportExcel mee = new MouldExportExcel();
            try
            {
              
                string physicalPath = HttpContext.Current.Request.PhysicalApplicationPath + "Xls\\";
                Microsoft.Office.Interop.Excel.Workbook sh = mee.OpenExcel(physicalPath + "气体报告管理.xls");
                bool ispass = false;
                foreach (var item in modelList)
                {
                    if (item.IsPass == null)
                    {
                        ShowMsgHelper.Alert(string.Format("{0}此检测单号还未检测不能导出！", item.CheckCode));
                        continue;
                    }
                    ispass = true;
                    Microsoft.Office.Interop.Excel.Worksheet ws = sh.Worksheets[modelList.IndexOf(item) + 1] as Microsoft.Office.Interop.Excel.Worksheet;
                    if (modelList.IndexOf(item) < modelList.Count - 1)
                    {
                        ws.Copy(Type.Missing, ws);
                    }
                    #region 设置值


                    if (item.GasSourse == "3")//回收净化气取“广东电网公司电力科学研究院”。
                    {
                        mee.SetCellValue(ws, 1, 1, "六氟化硫再生气体检测报告");
                        mee.SetCellValue(ws, 4, 2, "气体编码：");
                        mee.SetCellValue(ws, 4, 3, item.GasCdoe);
                        //mee.SetCellValue(ws, 4, 6, "检测单号：");
                        //mee.SetCellValue(ws, 4, 7, item.CheckCode);
                        mee.SetCellValue(ws, 4, 6, "批 次 号：");
                        mee.SetCellValue(ws, 4, 7, item.BusinessCode);
                    }
                    else if (item.GasSourse == "2")//入网新气取“入网检测登记”功能的“送检单位”。
                    {
                        ApplDetectionGas tempObj = new EGMNGS.BLL.ApplDetectionGas().GetModelList(string.Format(@"ApplCode='{0}'", item.BusinessCode))[0];
                        DetailsApplDetectionGas tempDetailsApplDetectionGa = new EGMNGS.BLL.DetailsApplDetectionGas().GetModelList(string.Format("GasCode='{0}'", item.GasCdoe))[0];

                        mee.SetCellValue(ws, 1, 1, "六氟化硫气体检测报告");
                        mee.SetCellValue(ws, 4, 2, "委托单位：");
                        mee.SetCellValue(ws, 4, 3, tempObj.InspectionUnit);
                        mee.SetCellValue(ws, 4, 6, "生产厂家：");
                        mee.SetCellValue(ws, 4, 7, tempDetailsApplDetectionGa.Manufacturer);
                        mee.SetCellValue(ws, 5, 6, "安装地点：");
                        mee.SetCellValue(ws, 5, 7, tempObj.SampliInspituation);
                    }
                    else if (item.GasSourse == "1")//新采购气取“新购气体管理”功能的“生产厂家”。
                    {
                        //  RegGasProcurement tempObj = new EGMNGS.BLL.RegGasProcurement().GetModelList(string.Format(@"Code='{0}'", item.BusinessCode))[0];

                        // mee.SetCellValue(ws, 4, 7, tempObj.ManufacturerName);

                        mee.SetCellValue(ws, 1, 1, "六氟化硫气体检测报告");
                        mee.SetCellValue(ws, 4, 2, "气体编码：");
                        mee.SetCellValue(ws, 4, 3, item.GasCdoe);
                        //mee.SetCellValue(ws, 4, 6, "检测单号：");
                        //mee.SetCellValue(ws, 4, 7, item.CheckCode);
                        mee.SetCellValue(ws, 4, 6, "批 次 号：");
                        mee.SetCellValue(ws, 4, 7, item.BusinessCode);

                    }


                    mee.SetCellValue(ws, 3, 4, item.ReportCode);
                    if (item.CheckDate != null)
                    {
                        mee.SetCellValue(ws, 3, 7, item.CheckDate.Value.ToShortDateString());

                    }
                    string cylinderSealNo = new EGMNGS.BLL.BookCylinderInfo().GetModelByCylinderCode(item.CylinderCode).CylinderSealNo;
                    mee.SetCellValue(ws, 5, 3, string.Format("{0}", item.CylinderCode));
                    //  mee.SetCellValue(ws, 5, 7, item.IsPass == "1" ? "合格" : "不合格");
                    var MyRng = ws.get_Range("A42:I42", Type.Missing);
                    MyRng.WrapText = true;
                    MyRng.Orientation = 0;
                    MyRng.AddIndent = false;
                    MyRng.IndentLevel = 0;
                    MyRng.ShrinkToFit = false;
                    MyRng.MergeCells = true;
                    //  End With

                    mee.SetCellValue(ws, 6, 3, cylinderSealNo);
                    //  mee.SetCellValue(ws, 6, 7, item.BusinessCode);
                    mee.SetCellValue(ws, 18, 3, item.C.ToString());
                    mee.SetCellValue(ws, 19, 3, item.KPA.ToString());
                    mee.SetCellValue(ws, 18, 7, item.RH.ToString());

                    EGMNGS.BLL.BookDetectionTools bllBookDetectionTools = new EGMNGS.BLL.BookDetectionTools();

                    var list = bllBookDetectionTools.GetModelList(string.Format("ToolCode='{0}'", item.SPYID));
                    if (list.Count > 0)
                    {

                        mee.SetCellValue(ws, 23, 3, list[0].SpecType);
                        mee.SetCellValue(ws, 23, 6, list[0].FactoryCode);
                        mee.SetCellValue(ws, 23, 9, list[0].EffectiveDate.Value.ToString("yyyy-MM-dd", DateTimeFormatInfo.InvariantInfo));

                    }
                    list = bllBookDetectionTools.GetModelList(string.Format("ToolCode='{0}'", item.LDYID));
                    if (list.Count > 0)
                    {
                        mee.SetCellValue(ws, 24, 3, list[0].SpecType);
                        mee.SetCellValue(ws, 24, 6, list[0].FactoryCode);
                        mee.SetCellValue(ws, 24, 9, list[0].EffectiveDate.Value.ToString("yyyy-MM-dd", DateTimeFormatInfo.InvariantInfo));
                    }
                    list = bllBookDetectionTools.GetModelList(string.Format("ToolCode='{0}'", item.WSJID));
                    if (list.Count > 0)
                    {
                        mee.SetCellValue(ws, 25, 3, list[0].SpecType);
                        mee.SetCellValue(ws, 25, 6, list[0].FactoryCode);
                        mee.SetCellValue(ws, 25, 9, list[0].EffectiveDate.Value.ToString("yyyy-MM-dd", DateTimeFormatInfo.InvariantInfo));
                    }
                    list = bllBookDetectionTools.GetModelList(string.Format("ToolCode='{0}'", item.QYJID));
                    if (list.Count > 0)
                    {
                        mee.SetCellValue(ws, 26, 3, list[0].SpecType);
                        mee.SetCellValue(ws, 26, 6, list[0].FactoryCode);
                        mee.SetCellValue(ws, 26, 9, list[0].EffectiveDate.Value.ToString("yyyy-MM-dd", DateTimeFormatInfo.InvariantInfo));
                    }
                    list = bllBookDetectionTools.GetModelList(string.Format("ToolCode='{0}'", item.SDJID));
                    if (list.Count > 0)
                    {
                        mee.SetCellValue(ws, 27, 3, list[0].SpecType);
                        mee.SetCellValue(ws, 27, 6, list[0].FactoryCode);
                        mee.SetCellValue(ws, 27, 9, list[0].EffectiveDate.Value.ToString("yyyy-MM-dd", DateTimeFormatInfo.InvariantInfo));
                    }
                    list = bllBookDetectionTools.GetModelList(string.Format("ToolCode='{0}'", item.FGGDJID));
                    if (list.Count > 0)
                    {
                        mee.SetCellValue(ws, 28, 3, list[0].SpecType);
                        mee.SetCellValue(ws, 28, 6, list[0].FactoryCode);
                        mee.SetCellValue(ws, 28, 9, list[0].EffectiveDate.Value.ToString("yyyy-MM-dd", DateTimeFormatInfo.InvariantInfo));
                    }
                    list = bllBookDetectionTools.GetModelList(string.Format("ToolCode='{0}'", item.HWHYFXYID));
                    if (list.Count > 0)
                    {
                        mee.SetCellValue(ws, 29, 3, list[0].SpecType);
                        mee.SetCellValue(ws, 29, 6, list[0].FactoryCode);
                        mee.SetCellValue(ws, 29, 9, list[0].EffectiveDate.Value.ToString("yyyy-MM-dd", DateTimeFormatInfo.InvariantInfo));

                    }


                    if (item.SF6Result >= (decimal)99.99)
                    {
                        mee.SetCellValue(ws, 34, 2, ">99.99");
                    }
                    else
                    {
                        mee.SetCellValue(ws, 34, 2, Convert.ToDecimal(item.SF6Result).ToString("F4"));
                    }

                    //  mee.SetCellValue(ws, 36, 2, item.SF6Index);

                    if ((item.AirQualityScoreResult*10000)<1)
                    {
                        mee.SetCellValue(ws, 34, 4, "＜1");
                    }
                    else
                    {
                        mee.SetCellValue(ws, 34, 4, Convert.ToDecimal(item.AirQualityScoreResult * 10000).ToString("F4"));

                    }
                    //  mee.SetCellValue(ws, 36, 3, item.AirQualityScoreIndex.ToString());

                    //小数第4位为0时(＜0.0001)，否则保留4位[if N<0.0001,'＜0.0001',保留4位小数]
                    if (Convert.ToDouble(item.CF4Result*10000) < 1)
                    {
                        mee.SetCellValue(ws, 34, 6, "＜1");
                    }
                    else
                    {
                        mee.SetCellValue(ws, 34, 6, Convert.ToDecimal(item.CF4Result * 10000).ToString("F4"));
                    }

                    if (Convert.ToDouble(item.C2F6) < 1)
                    {
                        mee.SetCellValue(ws, 34, 8, "＜1");
                    }
                    else
                    {
                        mee.SetCellValue(ws, 34, 8, item.C2F6.AsTargetType<int>(0));
                    }

                    if (Convert.ToDouble(item.C3F8) < 1)
                    {
                        mee.SetCellValue(ws, 34, 9, "＜1");
                    }
                    else
                    {
                        mee.SetCellValue(ws, 34, 9, item.C3F8.AsTargetType<int>(0));
                    }
                   

                    // mee.SetCellValue(ws, 36, 4, item.CF4Index);

                    //小数第4位为0时(＜0.0001)，否则保留4位[if N<0.0001,'＜0.0001',保留4位小数]
                    if (Convert.ToDouble(item.WaterQualityScoreResult)*10000 < 1)
                    {
                        mee.SetCellValue(ws, 39, 2, "＜1");
                    }
                    else
                    {
                        mee.SetCellValue(ws, 39, 2, Convert.ToDecimal(item.WaterQualityScoreResult * 10000).ToString("F5"));
                    }

                    //  mee.SetCellValue(ws, 36, 5, item.WaterQualityScoreIndex);

                    //气体检测报告：露点，如果录入数据是小于-61.0就导出＜-61.0，其它值就按实际数据导出。

                    mee.SetCellValue(ws, 39, 4, item.WaterDewResult < -61 ? "<-61.0" : Convert.ToString(item.WaterDewResult));

                    //  mee.SetCellValue(ws, 36, 6, item.WaterDewIndex);


                    if (Convert.ToDouble(item.HFResult)*10000 < 0)
                    {

                        mee.SetCellValue(ws, 39, 6, "0");
                    }
                    else if (Convert.ToDouble(item.HFResult) * 10000 < 0.1)
                    {
                        mee.SetCellValue(ws, 39, 6, Convert.ToDecimal(item.HFResult * 10000).ToString("F7"));
                    }
                    else
                    {
                        mee.SetCellValue(ws, 39, 6, Convert.ToDecimal(item.HFResult * 10000).ToString("F6"));
                    }




                    // mee.SetCellValue(ws, 36, 7, item.HFIndex);

                    //  mee.SetCellValue(ws, 35, 8, item.KSJHFResult.Value.ToString());

                    if (Convert.ToDouble(item.KSJHFResult * 10000) < 0.02)
                    {

                        mee.SetCellValue(ws, 39, 8, "＜0.02");
                    }
                    else if (Convert.ToDouble(item.KSJHFResult * 10000) < 0.00001)
                    {
                        mee.SetCellValue(ws, 39, 8, Convert.ToDecimal(item.KSJHFResult * 10000).ToString("F7"));
                    }
                    else
                    {
                        mee.SetCellValue(ws, 39, 8, Convert.ToDecimal(item.KSJHFResult * 10000).ToString("F6"));
                    }

                    //  mee.SetCellValue(ws, 36, 8, item.KSJHFIndex);

                  //  mee.SetCellValue(ws, 39, 9, item.KWYSorceResult.AsTargetType<string>(""));

                    if (Convert.ToDouble(item.KWYSorceResult * 10000) < 0.02)
                    {

                        mee.SetCellValue(ws, 39, 9, "＜0.02");
                    }
                    else if (Convert.ToDouble(item.KWYSorceResult * 10000) < 0.1)
                    {
                        mee.SetCellValue(ws, 39, 9, Convert.ToDecimal(item.KWYSorceResult * 10000).ToString("F7"));
                    }
                    else
                    {
                        mee.SetCellValue(ws, 39, 9, Convert.ToDecimal(item.KWYSorceResult * 10000).ToString("F6"));
                    }

                    //  mee.SetCellValue(ws, 36, 9, item.KWYSorceIndex);
                    mee.SetCellValue(ws, 43, 1, item.Conclusion);


                    if (item.GasSourse == "2")//入网新气取
                    {
                        mee.SetCellValue(ws, 5, 2, "出厂编号：");
                        mee.SetCellValue(ws, 5, 3, cylinderSealNo);
                        mee.SetCellValue(ws, 6, 2, "");
                        mee.SetCellValue(ws, 6, 3, "");
                    }

                    if (item.CheckPJType == "1")
                    {
                        mee.SetCellValue(ws, 12, 2, "");
                        mee.SetCellValue(ws, 13, 2, "");
                        mee.SetCellValue(ws, 14, 2, "");

                        mee.SetCellValue(ws, 27, 2, "");
                        mee.SetCellValue(ws, 27, 3, "");

                        mee.SetCellValue(ws, 27, 5, "");
                        mee.SetCellValue(ws, 27, 6, "");

                        mee.SetCellValue(ws, 27, 8, "");
                        mee.SetCellValue(ws, 27, 9, "");

                        mee.SetCellValue(ws, 28, 1, "");
                        mee.SetCellValue(ws, 28, 3, "");

                        mee.SetCellValue(ws, 28, 5, "");
                        mee.SetCellValue(ws, 28, 6, "");

                        mee.SetCellValue(ws, 28, 8, "");
                        mee.SetCellValue(ws, 28, 9, "");

                        mee.SetCellValue(ws, 29, 1, "");
                        mee.SetCellValue(ws, 29, 3, "");

                        mee.SetCellValue(ws, 29, 5, "");
                        mee.SetCellValue(ws, 29, 6, "");

                        mee.SetCellValue(ws, 29, 8, "");
                        mee.SetCellValue(ws, 29, 9, "");


                        //mee.SetCellValue(ws, 35, 7, "—");
                        //mee.SetCellValue(ws, 35, 8, "—");
                        //mee.SetCellValue(ws, 35, 9, "—");


                    }

                    #endregion

                    item.IsPrint = true;
                    new EGMNGS.BLL.ApplRegGasQtyInspection().Update(item);
                }
                if (!ispass)// 不合格
                {
                    return;
                }

                string fileName = "气体报告管理temp.xls";
                string pathFile = physicalPath + fileName;
                mee.SaveExcel(pathFile);
                //  ShowMsgHelper.Alert("导出成功!");
                FileDownHelper.DownLoadold(@"~/Xls/" + fileName);

            }
            finally
            {
                mee.Dispose();

            }
        }
    }
}
﻿using EGMNGS.Common;
using RM.Web.App_Code;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DapperData;
using RM.Common.DotNetUI;

namespace RM.Web.GanPing
{
    public class HGGR
    {
        public string CylinderCode { set; get; }
        public string GasCode { set; get; }
        public string ReportCode { set; get; }
        public string AmountGas { set; get; }
        public string AmountfillGas { set; get; }
        public string resultsAmount { set; get; }
        public string Code { set; get; }
        public string BusinessCode { set; get; }
        public string PowerSupplyName { set; get; }
        public string ConvertStationName { set; get; }
        public DateTime RegistantDate { set; get; }
    }

    public partial class HGTableGasRepertory : PageBase
    {
        public string PowerSupplyCode
        {
            get { return ViewState["PowerSupplyCode"] as String; }
            set { ViewState["PowerSupplyCode"] = value; }
        }
        public string PowerSupplyName
        {
            get { return ViewState["PowerSupplyName"] as String; }
            set { ViewState["PowerSupplyName"] = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.PageControl1.pageHandler += new EventHandler(pager_PageChanged);
            if (!IsPostBack)
            {
                string[] orgIdOrgName = ComServies.GetPowerApplyByUserID(User.UserId.ToString());
                PowerSupplyCode = orgIdOrgName[0];
                PowerSupplyName = orgIdOrgName[1];
            }
        }
    
        protected void pager_PageChanged(object sender, EventArgs e)
        {
            string sql = string.Format(@"select d.CylinderCode,d.GasCode,
(select a.ReportCode  from ApplRegGasQtyInspection a where a.GasCdoe=d.GasCode) as ReportCode,d.AmountGas,
isnull(a.AmountfillGas,0) as AmountfillGas,(d.AmountGas-isnull(a.AmountfillGas,0)) as resultsAmount,t.Code,t.BusinessCode,t.PowerSupplyName,
t.ConvertStationName,t.RegistantDate,t.PowerSupplyCode
from DetailsGasOutStorage d
left join tablegasoutstorage t on t.Code=d.TableGasOutStorage_Code
left join (select sum(amountfillgas) as AmountfillGas,gascode 
from InfoDevChangeFill group by gascode) a on a.GasCode=d.GasCode
");
            
            Pager p = new Pager();
            p.TableName = "("+ sql + ") temptable";
            p.FieldName = "*";
            p.strOrderFld = "RegistantDate desc";
            p.PageIndex = PageControl1.PageIndex;
            p.PageSize = PageControl1.PageSize = 20;
            p.StrWhere = string.Format("PowerSupplyCode='{0}'", PowerSupplyCode);
            Repository _db = new Repository();
            var listBugData = _db.GetPaged<HGGR>(SessionFactory.CreateConnection(), p);
            this.PageControl1.RecordCount = p.TotalRowsCount;
            ControlBindHelper.BindRepeaterList(listBugData, rp_Item);
           
        }



    }



}
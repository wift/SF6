﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="HGTableGasOutStoragePage.aspx.cs"
    Inherits="RM.Web.GanPing.HGTableGasOutStoragePage" EnableEventValidation="false"
    ClientIDMode="AutoID" %>

<%@ Register Src="~/UserControl/PageControl.ascx" TagName="PageControl" TagPrefix="uc1" %>
<%@ Register Src="~/UserControl/LoadButton.ascx" TagName="LoadButton" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>合格气体出库</title>
    <link href="/Themes/Styles/Site.css" rel="stylesheet" type="text/css" />
    <script src="/Themes/Scripts/Validator/JValidator.js" type="text/javascript"></script>
    <script src="/Themes/Scripts/jquery-1.8.2.min.js" type="text/javascript"></script>
    <script src="/Themes/Scripts/jquery.pullbox.js" type="text/javascript"></script>
    <script src="/Themes/Scripts/FunctionJS.js" type="text/javascript"></script>
    <script type="text/javascript">
        //添加
        function add() {
            document.getElementById('<%= this.btnAdd.ClientID %>').click();
        }
        //查询
        function search() {

            document.getElementById('<%= this.btnSearch.ClientID %>').click();
        }
        //删除
        function Delete() {
            showConfirmMsg("此操作不可恢复，您确定要删除吗？", function (r) {
                if (r) {
                    document.getElementById('<%= this.btnDel.ClientID %>').click();
                }
            });
        }
        //保存
        function SaveForm() {
            document.getElementById('<%= this.btnSave.ClientID %>').click();
        }
        //提交
        function Submit() {
            document.getElementById('<%= this.btnSubmit.ClientID %>').click();
        }



        //明细表

        function DelDetails() {
            var key = CheckboxValue1();
            showConfirmMsg("此操作不可恢复，您确定要删除吗？", function (r) {
                if (r) {
                    document.getElementById('<%= this.HiddenField1.ClientID %>').value = key.toString();
                    document.getElementById('<%= this.btnDelDetails.ClientID %>').click();
                }
            });

        }

        $(function () {
            InitControl();
        })
        function InitControl() {
            $(".div-body").PullBox({ dv: $(".div-body"), obj: $("#table1").find("tr") });
            divresize(160);
            FixedTableHeader("#tableDetails", $(window).height() - 90);
            checkboxdetail();
        }


        //快速添加
        function AddFast() {
            var url = "/GanPing/HGTableGasOutStoragePageFastAdd.aspx?key=" + $("#" + '<%= this.txtCode.ClientID %>').val();
            top.openDialog(url, 'HGTableGasOutStoragePageFastAdd', '详细信息 - 查看', 1000, 400, 50, 50);

        }

        function ExportXls() {
            document.getElementById('<%= this.btnExport.ClientID %>').click();
        }

        function ExcAddButton() {
            document.getElementById('<%= this.Add.ClientID %>').click();
            Foc();
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:HiddenField ID="HiddenField1" runat="server" />
            <table width="100%">
                <colgroup>
                    <col width="11%" />
                    <col width="11%" />
                    <col width="11%" />
                    <col width="11%" />
                    <col width="11%" />
                    <col width="11%" />
                    <col width="11%" />
                    <col width="11%" />
                </colgroup>
                <tr>
                    <td class="inner_cell_right">
                        出库编号：
                    </td>
                    <td>
                        <asp:TextBox ID="txtCode" runat="server" checkexpession="NotNull" datacol="yes" err="此项" />
                    </td>
                    <td class="inner_cell_right">
                        业务编号:
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlBusinessCode" runat="server" Width="100% " CssClass="select"
                            checkexpession="NotNull" datacol="yes" err="此项" AutoPostBack="true" OnSelectedIndexChanged="ddlBusinessCode_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                    <td class="inner_cell_right">
                        供电局名称：
                    </td>
                    <td>
                        <asp:TextBox ID="txtPowerSupplyName" runat="server" Enabled="false" />
                    </td>
                    <td class="inner_cell_right">
                        变电站名称：
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlConvertStationName" runat="server" Width="100% " CssClass="select">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td class="inner_cell_right">
                        数据说明：
                    </td>
                    <td>
                        <asp:TextBox ID="txtCountDesc" runat="server" />
                    </td>
                    <td class="inner_cell_right">
                        出库量（kg）：
                    </td>
                    <td>
                        <asp:TextBox ID="txtAmountOutStorage" runat="server" Enabled="false" />
                    </td>
                    <td class="inner_cell_right">
                        钢瓶数：
                    </td>
                    <td>
                        <asp:TextBox ID="txtCountCyliner" runat="server" Enabled="false" />
                    </td>
                    <td class="inner_cell_right">
                        联系人：
                    </td>
                    <td>
                        <asp:TextBox ID="txtContacts" runat="server" checkexpession="NotNull" datacol="yes"
                            err="此项" />
                    </td>
                </tr>
                <tr>
                    <td class="inner_cell_right">
                        手机：
                    </td>
                    <td>
                        <asp:TextBox ID="txtPhoneNum" runat="server" checkexpession="NotNull" datacol="yes"
                            err="此项" />
                    </td>
                    <td class="inner_cell_right">
                        办公电话：
                    </td>
                    <td>
                        <asp:TextBox ID="txtOfficeTel" runat="server" />
                    </td>
                    <td class="inner_cell_right">
                        年度：
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlYear" runat="server">
                        </asp:DropDownList>
                    </td>
                    <td class="inner_cell_right">
                        登记人：
                    </td>
                    <td>
                        <asp:TextBox ID="txtRegistantOID" runat="server" checkexpession="NotNull" datacol="yes"
                            err="此项" />
                    </td>
                </tr>
                <tr>
                    <td class="inner_cell_right">
                        联系地址：
                    </td>
                    <td>
                        <asp:TextBox ID="txtAddress" runat="server" checkexpession="NotNull" datacol="yes"
                            err="此项" />
                    </td>
                    <td class="inner_cell_right">
                        状态：
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlStatus" runat="server" Width="100%" Enabled="false">
                        </asp:DropDownList>
                    </td>
                    <td class="inner_cell_right">
                        月度：
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlMonth" runat="server" CssClass="select" checkexpession="NotNull"
                            datacol="yes" err="此项" />
                    </td>
                    <td class="inner_cell_right">
                        登记日期：
                    </td>
                    <td>
                        <asp:TextBox ID="txtRegistantDate" runat="server" checkexpession="NotNull" datacol="yes"
                            err="此项" />
                    </td>
                </tr>
            </table>
            <div style="text-align: left;">
                <uc2:LoadButton ID="LoadButton1" runat="server" />
                <div style="clear: left; float: right;">
                    <asp:LinkButton ID="Add" runat="server" class="l-btn" OnClick="btnAddDetails_Click"><span class="l-btn-left">
            <img src="/Themes/Images/13.png" alt="" />添 加</span></asp:LinkButton>
                    <a class="l-btn" onclick="DelDetails()"><span class="l-btn-left">
                        <img src="/Themes/Images/delete.png" alt="" />删 除</span></a>
                    <asp:LinkButton ID="Save" runat="server" class="l-btn" OnClick="btnSaveDetails_Click"><span class="l-btn-left">
            <img src="/Themes/Images/disk.png" alt="" />保 存</span></asp:LinkButton>
                </div>
            </div>
            <div style="text-align: right; display: none;">
                <asp:Button ID="btnSubmit" runat="server" OnClick="btnSubmit_Click" Text="btnSubmit_Click" />
                <asp:Button ID="btnSearch" runat="server" OnClick="btnSearch_Click" Text="btnSearch_Click" />
                <asp:Button ID="btnSave" runat="server" OnClick="btnSave_Click" Text="btnSave_Click"
                    OnClientClick="return CheckDataValid('#form1');" />
                <asp:Button ID="btnAdd" runat="server" OnClick="btnAdd_Click" Text="btnAdd_Click" />
                <asp:Button ID="btnDel" runat="server" OnClick="btnDel_Click" Text="btnDel_Click" />
                <asp:Button ID="btnDelDetails" runat="server" OnClick="btnDelDetails_Click" Text="btnDel_Click" />
                <asp:Button ID="btnExport" runat="server" OnClick="btnExport_Click" Text="btnExport" />
            </div>
            <div>
                <table width="100%">
                    <tr>
                        <td width="50%" style="vertical-align: top;">
                            <div class="div-body">
                                <table id="table1" class="grid" singleselect="true">
                                    <colgroup>
                                        <col width="18%" />
                                        <col width="18%" />
                                        <col width="15%" />
                                        <col width="7%" />
                                        <col width="20%" />
                                        <col width="12%" />
                                        <col width="10%" />
                                    </colgroup>
                                    <thead>
                                        <tr>
                                            <td style="text-align: center;">
                                                出库编号
                                            </td>
                                            <td style="text-align: center;">
                                                业务编号
                                            </td>
                                            <td style="text-align: center;">
                                                出库量(kg)
                                            </td>
                                            <td style="text-align: center;">
                                                钢瓶数
                                            </td>
                                            <td style="text-align: center;">
                                                供电局名称
                                            </td>
                                            <td style="text-align: center;">
                                                登记日期
                                            </td>
                                            <td style="text-align: center;">
                                                状态
                                            </td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <asp:Repeater ID="rp_Item" runat="server" OnItemDataBound="rp_ItemDataBound" OnItemCommand="rp_Item_ItemCommand">
                                            <ItemTemplate>
                                                <tr>
                                                    <td style="text-align: center;">
                                                        <asp:LinkButton ID="LBtnDel" runat="server" OnClientClick="selectrow(this)" CommandArgument='<%#Eval("OID") %>'
                                                            CommandName="del"><%#Eval("Code")%></asp:LinkButton>
                                                    </td>
                                                    <td style="text-align: center;">
                                                        <%#Eval("BusinessCode")%></a>
                                                    </td>
                                                    <td style="text-align: center;">
                                                        <%#Eval("AmountOutStorage")%>
                                                    </td>
                                                    <td style="text-align: center;">
                                                        <%#Eval("CountCyliner")%>
                                                    </td>
                                                    <td style="text-align: center;">
                                                        <%#Eval("PowerSupplyName")%>
                                                    </td>
                                                    <td style="text-align: center;">
                                                        <%#Eval("RegistantDate", "{0:yyyy-MM-dd}")%>
                                                    </td>
                                                    <td style="text-align: center;">
                                                        <%# DictEGMNS["CFStatus"][Eval("Status").ToString()]%>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </tbody>
                                </table>
                            </div>
                            <uc1:PageControl ID="PageControl1" runat="server" />
                        </td>
                        <td width="50%" style="vertical-align: top;">
                            <div class="div-body1">
                                <table id="tableDetails" class="grid1" singleselect="true">
                                    <colgroup>
                                        <col width="5%" />
                                        <col width="5%" />
                                        <col width="25%" />
                                        <col width="25%" />
                                        <col width="20%" />
                                        <col width="20%" />
                                    </colgroup>
                                    <thead>
                                        <tr>
                                            <td style="text-align: center;">
                                                序号
                                            </td>
                                            <td style="text-align: left;">
                                                <label id="checkAllOff" onclick="CheckAllLine1()" title="全选">
                                                    &nbsp;</label>
                                            </td>
                                            <td style="text-align: center;">
                                                钢瓶编码
                                            </td>
                                            <td style="text-align: center;">
                                                气体编码
                                            </td>
                                            <td style="text-align: center;">
                                                报告编号
                                            </td>
                                            <td style="text-align: center;">
                                                气量（kg）
                                            </td>
                                            <td style="text-align: center;">
                                                是否合格
                                            </td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <asp:Repeater ID="rptDetails" runat="server" OnItemDataBound="rptDetails_ItemDataBound">
                                            <ItemTemplate>
                                                <tr>
                                                    <td style="text-align: center;">
                                                        <%# Container.ItemIndex + 1%>
                                                    </td>
                                                    <td style="text-align: left;">
                                                        <asp:CheckBox ID="checkbox" runat="server" ToolTip='<%#Eval("OID")%>' />
                                                    </td>
                                                    <td style="text-align: center;">
                                                        <asp:TextBox ID="txtCylinderCode" runat="server" Text='<%#Eval("CylinderCode")%>'
                                                            Width="100%" onkeypress="return(clickButton(this));" CssClass="select"></asp:TextBox>
                                                    </td>
                                                    <td style="text-align: center;">
                                                        <asp:Label ID="lblGasCode" runat="server" Text='<%#Eval("GasCode")%>'></asp:Label>
                                                    </td>
                                                    <td style="text-align: center;">
                                                        <%#Eval("ReportCode")%>
                                                    </td>
                                                    <td style="text-align: center;">
                                                        <asp:Label ID="lblAmountGas" runat="server" Text='<%#Eval("AmountGas")%>'></asp:Label>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </tbody>
                                </table>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnAdd" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>
    </form>
</body>
</html>

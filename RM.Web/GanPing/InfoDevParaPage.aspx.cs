﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using RM.Common.DotNetCode;
using System.Data;
using RM.Common.DotNetUI;
using RM.Web.App_Code;
using EGMNGS.Common;

namespace RM.Web.GanPing
{
    public partial class InfoDevParaPage : PageBase
    {
        #region 属性字段

        EGMNGS.BLL.InfoDevPara bll = new EGMNGS.BLL.InfoDevPara();
        private DataTable UserList
        {
            get
            {
                return ViewState["UserList"] as DataTable;
            }
            set { ViewState["UserList"] = value; }
        }
        public EGMNGS.Model.InfoDevPara Model
        {
            get
            {
                if (ViewState["InfoDevPara"] == null)
                {
                    return new EGMNGS.Model.InfoDevPara();
                }
                return ViewState["InfoDevPara"] as EGMNGS.Model.InfoDevPara;
            }
            set { ViewState["InfoDevPara"] = value; }
        }
        private string actionMode
        {
            get { return ViewState["ActionMode"] as String; }
            set { ViewState["ActionMode"] = value; }
        }
        public string PowerSupplyCode
        {
            get { return ViewState["PowerSupplyCode"] as String; }
            set { ViewState["PowerSupplyCode"] = value; }
        }
        public string PowerSupplyName
        {
            get { return ViewState["PowerSupplyName"] as String; }
            set { ViewState["PowerSupplyName"] = value; }
        }

        #endregion

        #region 事件方法

        protected void Page_Load(object sender, EventArgs e)
        {
            this.PageControl1.pageHandler += new EventHandler(pager_PageChanged);
            if (!IsPostBack)
            {
                UserList = ComServies.GetAllUserInfo();
                string[] orgIdOrgName = ComServies.GetPowerApplyByUserID(User.UserId.ToString());
                PowerSupplyCode = orgIdOrgName[0];
                PowerSupplyName = orgIdOrgName[1];
                DropDownListBinder();
                var tempdict = DictEGMNS;
            }
        }

        /// <summary>
        /// 绑定下拉列
        /// </summary>
        private void DropDownListBinder()
        {

            EGMNGS.BLL.SYS_CODE sysCodeBll = new EGMNGS.BLL.SYS_CODE();
            DataSet dsYear = sysCodeBll.GetList("CODE_TYPE='DEVClass' ORDER BY DISPLAY_ORDER");

            this.ddlsDevClass.DataSource = dsYear.Tables[0];
            this.ddlsDevClass.DataTextField = "CODE_CHI_DESC";
            this.ddlsDevClass.DataValueField = "CODE";
            this.ddlsDevClass.DataBind();
            this.ddlsDevClass.Items.Insert(0, string.Empty);

            DataSet dsMonth = sysCodeBll.GetList("CODE_TYPE='DYLev' ORDER BY DISPLAY_ORDER");

            this.ddlsVoltageLevel.DataSource = dsMonth.Tables[0];
            this.ddlsVoltageLevel.DataTextField = "CODE_CHI_DESC";
            this.ddlsVoltageLevel.DataValueField = "CODE";
            this.ddlsVoltageLevel.DataBind();
            this.ddlsVoltageLevel.Items.Insert(0, string.Empty);


            DataTable dtPowerStation = ComServies.GetAllPowerStation();
            this.ddlsPowerSupplyCode.DataSource = dtPowerStation;
            this.ddlsPowerSupplyCode.DataTextField = "Organization_Name";
            this.ddlsPowerSupplyCode.DataValueField = "Organization_ID";
            this.ddlsPowerSupplyCode.DataBind();
            this.ddlsPowerSupplyCode.Items.Insert(0, string.Empty);

            //EGMNGS.BLL.SYS_CODE sysCodeBll = new EGMNGS.BLL.SYS_CODE();
            DataSet dtCFStatus = sysCodeBll.GetList("CODE_TYPE='CFStatus' ORDER BY DISPLAY_ORDER");

            this.ddlCFStatus.DataSource = dtCFStatus.Tables[0];
            this.ddlCFStatus.DataTextField = "CODE_CHI_DESC";
            this.ddlCFStatus.DataValueField = "CODE";
            this.ddlCFStatus.DataBind();
        }


        /// <summary>
        /// 绑定后激发事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void rp_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                //Label lblRatedQL = e.Item.FindControl("lblRatedQL") as Label;
                //Label lblAuditorOID = e.Item.FindControl("lblAuditorOID") as Label;


            }
        }

        /// <summary>
        /// 绑定数据，分页
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void pager_PageChanged(object sender, EventArgs e)
        {
            DataBindGrid();
        }

        /// <summary>
        /// 绑定数据源
        /// </summary>
        private void DataBindGrid()
        {
            string sqlStrwhere = "1=1";

            GenFindWhere(ref sqlStrwhere);

            if (PowerSupplyCode.AsTargetType<string>("") != "")
            {
                sqlStrwhere += string.Format(" and PowerSupplyCode='{0}'", PowerSupplyCode);
            }

            int count = bll.GetRecordCount(sqlStrwhere);
            DataSet ds = bll.GetListByPage(sqlStrwhere, string.Empty, PageControl1.PageIndex, PageControl1.PageSize);
            ControlBindHelper.BindRepeaterList(ds.Tables[0], rp_Item);
            this.PageControl1.RecordCount = Convert.ToInt32(count);
        }

        /// <summary>
        /// 绑定数据到对象
        /// </summary>
        private void DataBinder()
        {

            Model.DevCode = this.txtsDevCode.Text;
            Model.ConvertStationCode = this.ddlsConvertStationCode.SelectedValue;
            Model.ConvertStationName = this.ddlsConvertStationCode.SelectedItem.Text;

            Model.PowerSupplyCode = this.ddlsPowerSupplyCode.SelectedValue;
            Model.PowerSupplyName = this.ddlsPowerSupplyCode.SelectedItem.Text;
            Model.VoltageLevel = this.ddlsVoltageLevel.SelectedValue;
            Model.DevClass = this.ddlsDevClass.SelectedValue;
            Model.RunCode = this.txtsRunCode.Text;
            Model.DevName = this.txtsDevName.Text;
            Model.Manufacturer = this.txtsManufacturer.Text;
            Model.DevType = this.txtsDevType.Text;
            Model.DevNum = this.txtsDevNum.Text;
            Model.KA = this.txtkA.Text.AsTargetType<int>(0);
            Model.A = this.txtA.Text.AsTargetType<int>(0);
            Model.KV = this.txtkv.Text.AsTargetType<int>(0);


            if (this.txtPurchaseDate.Text.Trim().Length > 0)
            {
                Model.PurchaseDate = DateTime.Parse(this.txtPurchaseDate.Text);

            }
            if (this.txtsPutDate.Text.Trim().Length > 0)
            {
                Model.PutDate = DateTime.Parse(this.txtsPutDate.Text);
            }
            if (this.txtReturnDate.Text.Trim().Length > 0)
            {
                Model.ReturnDate = DateTime.Parse(this.txtReturnDate.Text);
            }
            if (this.txtRatedQL.Text.Trim().Length > 0)
            {
                Model.RatedQL = Convert.ToDecimal(this.txtRatedQL.Text);
            }
            if (this.txtRatedQY.Text.Trim().Length > 0)
            {
                Model.RatedQY = Convert.ToDecimal(this.txtRatedQY.Text);
            }

            Model.IsOnlineCheck = this.ddlIsOnlineCheck.SelectedValue;

        }

        /// <summary>
        /// 初始化控件
        /// </summary>
        private void InitControl()
        {

            this.txtsDevCode.Text = Model.DevCode;

            this.OID.Value = Model.OID.ToString();
            ListItem li = this.ddlsPowerSupplyCode.Items.FindByValue(Model.PowerSupplyCode);
            if (li != null)
            {
                this.ddlsPowerSupplyCode.SelectedValue = Model.PowerSupplyCode;
            }

            ListItem liv = this.ddlsVoltageLevel.Items.FindByValue(Model.VoltageLevel);
            if (liv != null)
            {
                this.ddlsVoltageLevel.SelectedValue = Model.VoltageLevel;
            }


            ListItem lid = this.ddlsDevClass.Items.FindByValue(Model.DevClass);
            if (liv != null)
            {
                this.ddlsDevClass.SelectedValue = Model.DevClass;
            }

           
            this.txtsRunCode.Text = Model.RunCode;
            this.txtsDevName.Text = Model.DevName;
            this.txtsManufacturer.Text = Model.Manufacturer;
            this.txtsDevType.Text = Model.DevType;
            this.txtsDevNum.Text = Model.DevNum;
            this.txtkv.Text = Model.KV.AsTargetType<string>("");
            this.txtkA.Text = Model.KA.AsTargetType<string>("");
            this.txtA.Text = Model.A.AsTargetType<string>("");

            if (Model.PurchaseDate != null)
            {
                this.txtPurchaseDate.Text = Model.PurchaseDate.ToShortDateString();

            }
            if (Model.PutDate != null)
            {
                this.txtsPutDate.Text = Model.PutDate.ToShortDateString();
            }
            if (Model.ReturnDate != null)
            {
                this.txtReturnDate.Text = Model.ReturnDate.ToShortDateString();
            }

            this.txtRatedQL.Text = Model.RatedQL.ToString();
            this.txtRatedQY.Text = Model.RatedQY.ToString();
            this.ddlCFStatus.SelectedValue = Model.Status;
            this.txtRegistrantoid.Text = GetUserName(Model.RegistrantOID);

            if (Model.RegistrantDate != null)
            {
                this.txtRegistrantDate.Text = Model.RegistrantDate.ToShortDateString();
            }

            LoadPowerSupplyName();

            if (Model.ConvertStationCode != null && this.ddlsConvertStationCode.Items.FindByValue(Model.ConvertStationCode)!=null)
            {
               // this.ddlsConvertStationCode.Items.Add(new ListItem(Model.ConvertStationCode, Model.ConvertStationName));
                this.ddlsConvertStationCode.SelectedValue = Model.ConvertStationCode;
            }
            this.ddlIsOnlineCheck.SelectedValue = Model.IsOnlineCheck;
           // LoadPowerSupplyName();
        }

        /// <summary>
        /// 获取用户名
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        private string GetUserName(string userId)
        {
            DataRow[] dr = UserList.Select(string.Format("User_ID='{0}'", userId));
            if (dr.Length > 0)
            {
                return dr[0][1].ToString();
            }
            return string.Empty;
        }
        #endregion

        #region 按钮事件

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            Model.Status = "1";
            if (bll.Update(Model))
            {
                ShowMsgHelper.Alert("提交成功！");
                InitControl();
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            ActionMode = eActionMode.Search;
            DataBindGrid();
        }

        private void ShowMaxMode()
        {
            Model = bll.GetModel(bll.GetMaxId() - 1);
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                if (actionMode == "Add")
                {
                    DataBinder();
                    Model.RegistrantOID = User.UserId.ToString();

                    int count = bll.GetRecordCount(string.Format("DevNum='{0}'", Model.DevCode));
                    if (count > 0)
                    {
                        ShowMsgHelper.showWarningMsg("设备编号已经存在!");
                        return;
                    }

                    if (bll.Add(Model) > 0)
                    {
                        actionMode = string.Empty;
                        ActionMode = eActionMode.NotExecute;
                        ShowMsgHelper.Alert("添加成功");
                    }

                    DataBindGrid();
                    ShowMaxMode();
                }
                else if (actionMode == "Edit")
                {
                    if (bll.GetModel(Model.OID).Status == "1")
                    {
                        ShowMsgHelper.Alert_Wern("“已确认”状态无法修改数据！");
                        return;
                    }

                    DataBinder();
                    if (bll.Update(Model))
                    {
                        EGMNSShowMsg.ShowEditMsgSuccess();
                        actionMode = string.Empty;
                    }
                    DataBindGrid();
                }
            }
            catch (ArgumentOutOfRangeException ex)
            {
                string[] errs = CommonHelper.GetValue(ex.Message, "@").Split(':');
                ShowMsgHelper.ExecuteScript(string.Format("ChangeCss($('#txt{0}'),'长度不能超{1}字')", errs[0], errs[1]));
            }
        }
        private void ClearControl()
        {
            Model = new EGMNGS.Model.InfoDevPara();
            InitControl();
        }
        protected void btnAdd_Click(object sender, EventArgs e)
        {
            actionMode = "Add";
            Model = new EGMNGS.Model.InfoDevPara(actionMode);
            Model.PowerSupplyCode = PowerSupplyCode;
            Model.PowerSupplyName = PowerSupplyName;
            Model.RegistrantDate = DateTime.Now;
            Model.RegistrantOID = User.UserId.ToString();
            Model.Status = "0";
            InitControl();
        }


        protected void btnDel_Click(object sender, EventArgs e)
        {
            if (bll.GetModel(Model.OID).Status == "1")
            {
                ShowMsgHelper.Alert("“已确认”状态不可以删除");
            }
            else
            {
                bll.Delete(Model.OID);
                Model = new EGMNGS.Model.InfoDevPara();
                InitControl();
                DataBindGrid();
                ShowMsgHelper.Alert("删除成功！");
            }
        }

        protected void rp_Item_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            int oid = Convert.ToInt32(e.CommandArgument);

            Model = bll.GetModel(oid);
            InitControl();
            actionMode = "Edit";
        }
        #endregion

        protected void ddlPowerSupplyName_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadPowerSupplyName();
        }

        private void LoadPowerSupplyName()
        {
            this.ddlsConvertStationCode.Items.Clear();
            EGMNGS.BLL.InfoPowerSupplyConvertStation bll = new EGMNGS.BLL.InfoPowerSupplyConvertStation();
            DataSet dsPowerSupplyCode = bll.GetList(string.Format("PowerSupplyCode='{0}'", this.ddlsPowerSupplyCode.SelectedValue));
            this.ddlsConvertStationCode.DataSource = dsPowerSupplyCode.Tables[0];
            this.ddlsConvertStationCode.DataTextField = "ConvartStationName";
            this.ddlsConvertStationCode.DataValueField = "CovnertStationCode";
            this.ddlsConvertStationCode.DataBind();
            this.ddlsConvertStationCode.Items.Insert(0, string.Empty);
        }

    }
}
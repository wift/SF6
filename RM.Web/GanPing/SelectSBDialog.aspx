﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SelectSBDialog.aspx.cs" Inherits="RM.Web.GanPing.SelectSBDialog" %>
<%@ Register Src="~/UserControl/PageControl.ascx" TagName="PageControl" TagPrefix="uc1" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link href="/Themes/Styles/Site.css" rel="stylesheet" type="text/css" />
    <script src="/Themes/Scripts/Validator/JValidator.js" type="text/javascript"></script>
    <script src="/Themes/Scripts/jquery-1.8.2.min.js" type="text/javascript"></script>
    <script src="/Themes/Scripts/jquery.pullbox.js" type="text/javascript"></script>
     <script src="/Themes/Scripts/artDialog/artDialog.source.js" type="text/javascript"></script>
    <script src="/Themes/Scripts/artDialog/iframeTools.source.js" type="text/javascript"></script>
    <script src="/Themes/Scripts/FunctionJS.js" type="text/javascript"></script>
    <script src="/Themes/Scripts/DatePicker/WdatePicker.js" type="text/javascript"></script>
    <script type="text/javascript">
 
        $(function () {
            InitControl();
        })
        function InitControl() {
            $(".div-body").PullBox({ dv: $(".div-body"), obj: $("#table1").find("tr") });
            divresize(190);

            setTimeout(publicobjcss, 500);
        }

        //查询
        function search() {

            document.getElementById('<%= this.btnSearch.ClientID %>').click();
        }
        function confirm()
        {
       
            var objDev = {};
            if (top.frames[1].objDev) {
                objDev = top.frames[1].objDev;
            }
            else {
                objDev = window.parent.frames[0].objDev;
            }

            objDev.devcode = $(".selected:first").attr("id");
            objDev.devname = $(".selected").eq(2).text().replace(/(^\s+)|(\s+$)/g, "");
            objDev.DevType = $(".selected").eq(3).text().replace(/(^\s+)|(\s+$)/g, "");
            objDev.RatedQL = $(".selected").eq(5).text().replace(/(^\s+)|(\s+$)/g, "");
            objDev.RatedQY = $(".selected").eq(6).text().replace(/(^\s+)|(\s+$)/g, "");
        
            OpenClose();
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional">
            <ContentTemplate>
                <table width="100%">
                    <colgroup>
                        <col width="11%" />
                        <col width="11%" />
                        <col width="11%" />
                        <col width="11%" />
                        <col width="11%" />
                        <col width="11%" />
                        <col width="11%" />
                        <col width="11%" />
                    </colgroup>
                    <tr>

                        <td class="inner_cell_right">设备名称：
                        </td>
                        <td>
                            <asp:TextBox ID="txtsDevName" runat="server" checkexpession="NotNull" datacol="yes"
                                err="此项" />
                        </td>
                        <td class="inner_cell_right">运行编号：
                        </td>
                        <td>
                            <asp:TextBox ID="txtsRunCode" runat="server" checkexpession="NotNull" datacol="yes"
                                err="此项" />
                        </td>
                        <td class="inner_cell_right">设备型号：
                        </td>
                        <td>
                            <asp:TextBox ID="txtsDevType" runat="server" checkexpession="NotNull" datacol="yes"
                                err="此项" />
                        </td>
                        <td class="inner_cell_right">设备编号：
                        </td>
                        <td>
                            <asp:TextBox ID="txtsDevNum" runat="server" checkexpession="NotNull" datacol="yes"
                                err=" 此项" />
                        </td>
                    </tr>


                    <tr>
                        <td class="inner_cell_right">生产厂家：
                        </td>
                        <td>
                            <asp:TextBox ID="txtsManufacturer" runat="server" checkexpession="NotNull" datacol="yes"
                                err="此项" />
                        </td>

                        <td class="inner_cell_right"> 
                             <asp:Button ID="btnSearch" runat="server" OnClick="btnSearch_Click" Text="查询" />

                        </td>

                        <td> <input type="button" value="确认" onclick="confirm()"  />


         
                        </td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>


                </table>
           
                <div style="text-align: right; display: none;">
  
                   
                 
                </div>
                <div class="div-body">
                    <table id="table1" class="grid" singleselect="true">
                        <colgroup>
                            <col width="15%" />
                            <col width="15%" />
                            <col width="15%" />
                            <col width="15%" />
                            <col width="10%" />
                            <col width="10%" />
                            <col width="10%" />
                            <col width="10%" />
                        </colgroup>
                        <thead>
                            <tr>
                                <td style="text-align: center;">供电局名称
                                </td>
                                <td style="text-align: center;">变电站名称
                                </td>
                                <td style="text-align: center;">设备名称
                                </td>
                                <td style="text-align: center;">设备型号
                                </td>
                                <td style="text-align: center;">运行编号
                                </td>
                             
                                <td style="text-align: center;">额定气量
                                </td>
                                <td style="text-align: center;">额定气压
                                </td>
                                   <td style="text-align: center;">生产厂家
                                </td>
                            </tr>
                        </thead>
                        <tbody>
                            <asp:Repeater ID="rp_Item" runat="server" >
                                <ItemTemplate>
                                    <tr>
                                        <td style="text-align: center;" id="<%#Eval("DevCode") %>">
                                     
                                            <%#Eval("PowerSupplyName")%>

                                        </td>
                                        <td style="text-align: center;">
                                            <%#Eval("ConvertStationName")%></a>
                                        </td>
                                        <td style="text-align: center;">
                                            <%#Eval("DevName")%>
                                        </td>
                                        <td style="text-align: center;">
                                            <%#Eval("DevType")%>
                                        </td>
                                        <td style="text-align: center;">
                                            <%#Eval("RunCode")%>
                                        </td>
                                        <td style="text-align: center;">
                                            <%#Eval("RatedQL")%>
                                        </td>
                                        <td style="text-align: center;">
                                            <%#Eval("RatedQY")%>
                                        </td>
                                        <td style="text-align: center;">
                                            <%# Eval("Manufacturer")%>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                        </tbody>
                    </table>
                </div>
                <uc1:pagecontrol id="PageControl1" runat="server" />
            </ContentTemplate>
            <Triggers>

                <asp:AsyncPostBackTrigger ControlID="btnSearch" EventName="Click" />

            </Triggers>
        </asp:UpdatePanel>

 

    </form>
</body>
</html>

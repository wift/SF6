﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using RM.Common.DotNetUI;
using EGMNGS.Common;
using RM.Web.App_Code;

namespace RM.Web.GanPing
{
    public partial class HGTableGasInStoragFastAdd : Page
    {
        public DataTable DetailsTable
        {
            get
            {
                if (ViewState["DetailsTable"] == null)
                {
                    return new DataTable();
                }
                return ViewState["DetailsTable"] as DataTable;
            }
            set { ViewState["DetailsTable"] = value; }
        }
        public Dictionary<string, string> DictIsPass
        {
            get
            {
                if (ViewState["IsPass"] == null)
                {
                    Dictionary<string, string> IsPass = new Dictionary<string, string>();
                    EGMNGS.BLL.SYS_CODE sysCodeBll = new EGMNGS.BLL.SYS_CODE();
                    DataSet dsIsPass = sysCodeBll.GetList("CODE_TYPE='IsPass' ORDER BY DISPLAY_ORDER");
                    foreach (DataRow item in dsIsPass.Tables[0].Rows)
                    {
                        IsPass.Add(item["CODE"].ToString(), item["CODE_CHI_DESC"].ToString());
                    }
                    IsPass.Add(string.Empty, string.Empty);
                    ViewState["IsPass"] = IsPass;
                }

                return ViewState["IsPass"] as Dictionary<string, string>;
            }
            set { ViewState["IsPass"] = value; }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                // EGMNGS.BLL.SYS_CODE sysCodeBll = new EGMNGS.BLL.SYS_CODE();
                // lsCFStatus = sysCodeBll.GetModelList("CODE_TYPE='GPStatus' ORDER BY DISPLAY_ORDER").ToDictionary(o => o.CODE, o => o.CODE_CHI_DESC);

                DataBindGrid();

            }

            //  ShowMsgHelper.Alert(Request["key"].ToString());
        }

        /// <summary>
        /// 绑定数据源
        /// </summary>
        private void DataBindGrid()
        {
            string code = Request["key"].ToString();
            DataSet ds = ComServies.GetHGISPASSDJData(code);
            DetailsTable = ds.Tables[0];
            ControlBindHelper.BindRepeaterList(DetailsTable, rp_Item);
        }


        protected void rp_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {

                Label lblGasSourse = e.Item.FindControl("lblGasSourse") as Label;
                if (lblGasSourse != null && lblGasSourse.Text.Length > 0)
                {
                    if (lblGasSourse.Text == "1")
                    {
                        lblGasSourse.Text = "新采购气";
                    }
                    else if (lblGasSourse.Text == "2")
                    {

                        lblGasSourse.Text = "入网新气";
                    }
                    else if (lblGasSourse.Text == "3")
                    {

                        lblGasSourse.Text = "回收净化气";
                    }
                }


            }
        }

        protected void btnBatchAdd_Click(object sender, EventArgs e)
        {
            if (HiddenField1.Value.Length > 0)
            {
                string code = Request["key"].ToString();
                EGMNGS.BLL.DetailsGasStorage DetailsBLL = new EGMNGS.BLL.DetailsGasStorage();
                int returnValue = 0;
                foreach (DataRow item in DetailsTable.Select(string.Format("OID in({0})", HiddenField1.Value)))
                {
                    var obj = new EGMNGS.Model.DetailsGasStorage() { AmountGas = (decimal)item["AmountGas"], GasCode = item["GasCdoe"].ToString(), CylinderCode = item["CylinderCode"].ToString(), DtGasInStorageCode = code, IsPass = "1", FillStatus="2" };
                    returnValue = DetailsBLL.Add(obj);
                }

                if (returnValue > 0)
                {
                    ShowMsgHelper.AlertMsg("批量添加成功！");
                }
                else
                {
                    ShowMsgHelper.AlertMsg("批量添加失败！");
                }
                DataBindGrid();
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using RM.Common.DotNetCode;
using System.Data;
using RM.Common.DotNetUI;
using RM.Web.App_Code;
using EGMNGS.Common;
using System.Configuration;
using System.Text;
using System.Web.UI.HtmlControls;

namespace RM.Web.GanPing
{
    public partial class BookCylinderInfoPage : PageBase
    {
        #region 属性字段

        EGMNGS.BLL.BookCylinderInfo bll = new EGMNGS.BLL.BookCylinderInfo();


        private DataTable UserList
        {
            get
            {
                return ViewState["UserList"] as DataTable;
            }
            set { ViewState["UserList"] = value; }
        }
        private EGMNGS.Model.BookCylinderInfo Model
        {
            get
            {
                if (ViewState["BookCylinderInfo"] == null)
                {
                    return new EGMNGS.Model.BookCylinderInfo();
                }
                return ViewState["BookCylinderInfo"] as EGMNGS.Model.BookCylinderInfo;
            }
            set { ViewState["BookCylinderInfo"] = value; }
        }
        private string actionMode
        {
            get { return ViewState["ActionMode"] as String; }
            set { ViewState["ActionMode"] = value; }
        }
        public string PowerSupplyCode
        {
            get { return ViewState["PowerSupplyCode"] as String; }
            set { ViewState["PowerSupplyCode"] = value; }
        }
        public string PowerSupplyName
        {
            get { return ViewState["PowerSupplyName"] as String; }
            set { ViewState["PowerSupplyName"] = value; }
        }

        private string WhereStr
        {
            get
            {
                if (ViewState["WhereStr"] == null)
                {
                    return "1=1";
                }
                return ViewState["WhereStr"] as String;
            }
            set { ViewState["WhereStr"] = value; }
        }
        #endregion

        #region 事件方法

        protected void Page_Load(object sender, EventArgs e)
        {
            this.PageControl1.pageHandler += new EventHandler(pager_PageChanged);
            if (!IsPostBack)
            {
                UserList = ComServies.GetAllUserInfo();
                // string[] orgIdOrgName = ComServies.GetPowerApplyByUserID(User.UserId.ToString());
                //PowerSupplyCode = orgIdOrgName[0];
                //PowerSupplyName = orgIdOrgName[1];
                DropDownListBinder();
            }
        }

        /// <summary>
        /// 绑定下拉列
        /// </summary>
        private void DropDownListBinder()
        {
            EGMNGS.BLL.SYS_CODE sysCodeBll = new EGMNGS.BLL.SYS_CODE();

            DataSet dtCFStatus = sysCodeBll.GetList("CODE_TYPE='GPStatus' ORDER BY DISPLAY_ORDER");

            this.ddlCFStatus.DataSource = dtCFStatus.Tables[0];
            this.ddlCFStatus.DataTextField = "CODE_CHI_DESC";
            this.ddlCFStatus.DataValueField = "CODE";
            this.ddlCFStatus.DataBind();

            this.ddlCFStatus.Items.Insert(0, string.Empty);



            DataSet dtCylinderCapacity = sysCodeBll.GetList("CODE_TYPE='GPRL' ORDER BY DISPLAY_ORDER");

            this.ddlCylinderCapacity.DataSource = dtCylinderCapacity.Tables[0];
            this.ddlCylinderCapacity.DataTextField = "CODE_CHI_DESC";
            this.ddlCylinderCapacity.DataValueField = "CODE";
            this.ddlCylinderCapacity.DataBind();

            this.ddlCylinderCapacity.Items.Insert(0, string.Empty);


            DataSet dtddlManufacturer = sysCodeBll.GetList("CODE_TYPE='BelongUnit' ORDER BY DISPLAY_ORDER");

            this.ddlManufacturer.DataSource = dtddlManufacturer.Tables[0];
            this.ddlManufacturer.DataTextField = "CODE_CHI_DESC";
            this.ddlManufacturer.DataValueField = "CODE";
            this.ddlManufacturer.DataBind();
        }


        /// <summary>
        /// 绑定后激发事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void rp_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Label lblUserID = e.Item.FindControl("lblUserID") as Label;
                if (lblUserID != null)
                {
                    lblUserID.Text = GetUserName(lblUserID.Text);
                }


                Label lblStatus = e.Item.FindControl("lblStatus") as Label;
                if (lblStatus != null)
                {
                    if (lblStatus.Text.Length > 0)
                    {
                        lblStatus.Text = this.ddlCFStatus.Items.FindByValue(lblStatus.Text).Text;

                    }
                }

                Label lblCylinderCapacity = e.Item.FindControl("lblCylinderCapacity") as Label;
                if (lblCylinderCapacity != null)
                {
                    if (lblCylinderCapacity.Text.Length > 0)
                    {
                        lblCylinderCapacity.Text = this.ddlCylinderCapacity.Items.FindByValue(lblCylinderCapacity.Text).Text;

                    }
                }


            }
        }

        /// <summary>
        /// 绑定数据，分页
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void pager_PageChanged(object sender, EventArgs e)
        {
            DataBindGrid();
        }

        /// <summary>
        /// 绑定数据源
        /// </summary>
        private void DataBindGrid()
        {

            int count = bll.GetRecordCount(WhereStr);
            DataSet ds = bll.GetListByPage(WhereStr, string.Empty, PageControl1.PageIndex, PageControl1.PageSize);
            ControlBindHelper.BindRepeaterList(ds.Tables[0], rp_Item);
            this.PageControl1.RecordCount = Convert.ToInt32(count);
        }
        /// <summary>
        /// 绑定数据到对象
        /// </summary>
        private void DataBinder()
        {
            Model.CylinderCode = this.txtCylinderCode.Text;
            Model.CylinderSealNo = this.txtCylinderSealNo.Text;
            Model.CylinderSourse = this.txtCylinderSourse.Text;
            Model.Manufacturer = this.ddlManufacturer.SelectedValue;
            Model.CylinderCapacity = this.ddlCylinderCapacity.SelectedValue;
            if (this.txtManufactureDate.Text.Length > 0)
            {
                Model.ManufactureDate = Convert.ToDateTime(this.txtManufactureDate.Text);
            }
            if (this.txtEffectiveDate.Text.Length > 0)
            {
                Model.EffectiveDate = Convert.ToDateTime(this.txtEffectiveDate.Text);

            }
            Model.CurGasCode = this.txtCurGasCode.Text;
            Model.Status = this.ddlCFStatus.SelectedValue;
        }

        /// <summary>
        /// 初始化控件
        /// </summary>
        private void InitControl()
        {
            this.txtCylinderCode.Text = Model.CylinderCode;
            this.txtCylinderSealNo.Text = Model.CylinderSealNo;
            this.txtCylinderSourse.Text = Model.CylinderSourse;
            if (this.ddlManufacturer.Items.FindByValue(Model.Manufacturer) != null)
            {
                this.ddlManufacturer.SelectedValue = Model.Manufacturer;
            }

            this.ddlCylinderCapacity.SelectedValue = Model.CylinderCapacity;
            if (Model.ManufactureDate != null)
            {
                this.txtManufactureDate.Text = Model.ManufactureDate.Value.ToShortDateString();

            }
            else
            {
                this.txtManufactureDate.Text = string.Empty;
            }
            if (Model.EffectiveDate != null)
            {
                this.txtEffectiveDate.Text = Model.EffectiveDate.Value.ToShortDateString();

            }
            else
            {
                this.txtEffectiveDate.Text = string.Empty;

            }
            this.txtCurGasCode.Text = Model.CurGasCode;
            this.ddlCFStatus.SelectedValue = Model.Status;
            this.txtRegistantsOID.Text = GetUserName(Model.RegistantsOID);
            this.txtRegistantsDate.Text = Model.RegistantsDate == null ? string.Empty : Model.RegistantsDate.Value.ToShortDateString();
        }

        /// <summary>
        /// 获取用户名
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        private string GetUserName(string userId)
        {
            DataRow[] dr = UserList.Select(string.Format("User_ID='{0}'", userId));
            if (dr.Length > 0)
            {
                return dr[0][1].ToString();
            }
            return string.Empty;
        }

        private void ClearControl()
        {
            Model = new EGMNGS.Model.BookCylinderInfo();
            InitControl();
        }


        #endregion

        #region 按钮事件

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            Model.Status = "0";
            if (this.ddlManufacturer.SelectedValue == "2")
            {
                Model.EffectiveDate = null;
                Model.ManufactureDate = null;
            }
            if (bll.Update(Model))
            {
                ShowMsgHelper.Alert("提交成功！");
                InitControl();
                DataBindGrid();
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            StringBuilder sb = new StringBuilder("1=1");

            if (this.txtCylinderCode.Text.Trim().Length > 0)
            {
                sb.AppendFormat(" and CylinderCode like '%{0}%' ", this.txtCylinderCode.Text.Trim());
            }
            if (this.txtCylinderSealNo.Text.Trim().Length > 0)
            {
                sb.AppendFormat(" and CylinderSealNo like '%{0}%' ", this.txtCylinderSealNo.Text.Trim());
            }

            if (this.ddlManufacturer.SelectedIndex > -1)
            {
                sb.AppendFormat(" and Manufacturer = '{0}' ", this.ddlManufacturer.SelectedValue);
            }

            WhereStr = sb.ToString();

            DataBindGrid();
        }
        /// <summary>
        /// 获取当前对象
        /// </summary>
        private void ShowMaxMode()
        {
            Model = bll.GetModel(bll.GetMaxId() - 1);
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if ((this.txtCylinderCode.Text.StartsWith("RW")&& this.ddlManufacturer.SelectedValue == "1")||
                    (this.txtCylinderCode.Text.StartsWith("GP") && this.ddlManufacturer.SelectedValue == "2"))
                {

                    ShowMsgHelper.showWarningMsg("此钢瓶编号不属于所当前单位");
                    return;
                }

                if (actionMode == "Add")
                {
                    DataBinder();
                    Model.RegistantsOID = User.UserId.ToString();

                    int count = bll.GetRecordCount(string.Format("CylinderCode='{0}'", Model.CylinderCode));
                    if (count > 0)
                    {
                        ShowMsgHelper.showWarningMsg("钢瓶编号已经存在!");
                        return;
                    }

                    if (bll.Add(Model) > 0)
                    {
                        actionMode = "Edit";
                        ShowMsgHelper.Alert("添加成功！");
                    }
                    WhereStr = "1=1";
                    DataBindGrid();
                    ShowMaxMode();
                }
                else if (actionMode == "Edit")
                {
                    if (!string.IsNullOrEmpty(bll.GetModel(Model.OID).Status))
                    {
                        ShowMsgHelper.Alert_Wern("已有状态无法修改数据！");
                        return;
                    }

                    DataBinder();
                    if (bll.Update(Model))
                    {
                        EGMNSShowMsg.ShowEditMsgSuccess();
                        actionMode = string.Empty;
                    }
                    DataBindGrid();
                }
            }
            catch (ArgumentOutOfRangeException ex)
            {
                string[] errs = CommonHelper.GetValue(ex.Message, "@").Split(':');
                ShowMsgHelper.ExecuteScript(string.Format("ChangeCss($('#txt{0}'),'长度不能超{1}字')", errs[0], errs[1]));
            }
        }

        protected void btnSuperSave_Click(object sender, EventArgs e)
        {
            if (actionMode == "Add")
            {
                DataBinder();
                Model.RegistantsOID = User.UserId.ToString();

                int count = bll.GetRecordCount(string.Format("CylinderCode='{0}'", Model.CylinderCode));
                if (count > 0)
                {
                    ShowMsgHelper.showWarningMsg("钢瓶编号已经存在!");
                    return;
                }

                if (bll.Add(Model) > 0)
                {
                    actionMode = string.Empty;
                }

                DataBindGrid();
                ClearControl();
            }
            else if (actionMode == "Edit")
            {
                DataBinder();
                if (bll.Update(Model))
                {
                    EGMNSShowMsg.ShowEditMsgSuccess();
                    actionMode = string.Empty;
                }
                DataBindGrid();
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            actionMode = "Add";
            Model = new EGMNGS.Model.BookCylinderInfo(actionMode);
            Model.RegistantsDate = DateTime.Now;
            Model.RegistantsOID = User.UserId.ToString();

            //其他单位当【所属单位】=“其它单位”时，编码规则改为XQ+6位数值，从0000001开始流水自动增长；之前的历史数据不管，保留原来的编码。
            //【自编编号】也相应改为，GDJ+6位数值。
            if (this.ddlManufacturer.SelectedValue=="2")
            {
                Model.CylinderCode = ComServies.GetCode("RW");
                Model.CylinderSourse = Model.CylinderCode.Replace("RW", "GDJ");
            }

            InitControl();
        }


        protected void btnDel_Click(object sender, EventArgs e)
        {
            string strID = HiddenField1.Value;
            if (strID.Length == 0)
            {
                return;
            }
            foreach (var item in strID.Split(new char[] { ',' }))
            {

                var obj = bll.GetModel(Convert.ToInt32(item));
                if (obj == null)
                {
                    return;
                }
                if (!string.IsNullOrEmpty(obj.Status))
                {
                    ShowMsgHelper.Alert_Wern("“已有状态不可以删除");
                }
                else
                {
                    bll.Delete(obj.OID);
                    DataBindGrid();
                    ShowMsgHelper.Alert("删除成功！");
                }

            }
        }

        protected void rp_Item_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            int oid = Convert.ToInt32(e.CommandArgument);

            Model = bll.GetModel(oid);
            InitControl();
            actionMode = "Edit";


        }
        #endregion

        private void PrintCode(Dictionary<string, string> DictPrintPara)
        {
            string barCodePath = ConfigurationManager.AppSettings["BarCodePath"];
            //Declare a BarTender application variable 

            BarTender.Application btApp;

            //Declare a BarTender format variable 

            BarTender.Format btFormat;

            //Instantiate a BarTender application variable 

            btApp = new BarTender.Application();

            //Set the BarTender application visible 

            btApp.Visible = true;

            //Open a BarTender label format 

            btFormat = btApp.Formats.Open(barCodePath, false, "");

            //Set the Name substring data 
            foreach (var item in DictPrintPara)
            {
                btFormat.SetNamedSubStringValue("Code", item.Key);
                btFormat.SetNamedSubStringValue("SeaNo", item.Value);
                btFormat.PrintOut(false, false);
            }
            //End the BarTender process 

            btApp.Quit(BarTender.BtSaveOptions.btDoNotSaveChanges);

            System.Runtime.InteropServices.Marshal.ReleaseComObject(btApp);

            GC.Collect();     // 强制对零代到指定代进行垃圾回收。

        }

        protected void btnPrint_Click(object sender, EventArgs e)
        {
            Dictionary<string, string> dictPrint = GetCheckBox();

            PrintCode(dictPrint);
        }

        private Dictionary<string, string> GetCheckBox()
        {
            Dictionary<string, string> PrintCodeList = new Dictionary<string, string>();


            for (int i = 0; i < this.rp_Item.Items.Count; i++)
            {
                System.Web.UI.WebControls.CheckBox checkbox = (System.Web.UI.WebControls.CheckBox)rp_Item.Items[i].FindControl("checkbox");

                if (checkbox.Checked == true)
                {
                    LinkButton txtCylinderCode = this.rp_Item.Items[i].FindControl("LBtnDel") as LinkButton;
                    Label lblCylinderSealNo = this.rp_Item.Items[i].FindControl("lblCylinderSealNo") as Label;

                    PrintCodeList.Add(txtCylinderCode.Text, lblCylinderSealNo.Text);
                }
            }

            return PrintCodeList;

        }
    }
}
﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BookDetectionToolsPage.aspx.cs"
    Inherits="RM.Web.GanPing.BookDetectionToolsPage"  EnableEventValidation ="false"  ClientIDMode="AutoID"%>

<%@ Register Src="~/UserControl/PageControl.ascx" TagName="PageControl" TagPrefix="uc1" %>
<%@ Register Src="~/UserControl/LoadButton.ascx" TagName="LoadButton" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>检测工具台账</title>
    <link href="/Themes/Styles/Site.css" rel="stylesheet" type="text/css" />
    <script src="/Themes/Scripts/Validator/JValidator.js" type="text/javascript"></script>
    <script src="/Themes/Scripts/jquery-1.8.2.min.js" type="text/javascript"></script>
    <script src="/Themes/Scripts/jquery.pullbox.js" type="text/javascript"></script>
    <script src="/Themes/Scripts/FunctionJS.js" type="text/javascript"></script>
    <script src="/Themes/Scripts/DatePicker/WdatePicker.js" type="text/javascript"></script>
    <script type="text/javascript">
        //添加
        function add() {
            document.getElementById('<%= this.btnAdd.ClientID %>').click();
        }
        //查询
        function search() {

            document.getElementById('<%= this.btnSearch.ClientID %>').click();
        }
        //删除
        function Delete() {
            showConfirmMsg("此操作不可恢复，您确定要删除吗？", function (r) {
                if (r) {
                    document.getElementById('<%= this.btnDel.ClientID %>').click();
                }
            });
        }
        //保存
        function SaveForm() {
            document.getElementById('<%= this.btnSave.ClientID %>').click();
        }
        //提交
        function Submit() {
            document.getElementById('<%= this.btnSubmit.ClientID %>').click();
        }

        $(function () {
            InitControl();
        })
        function InitControl() {
            $(".div-body").PullBox({ dv: $(".div-body"), obj: $("#table1").find("tr") });
            divresize(140);

        }


    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional">
        <ContentTemplate>
    <table width="100%">
        <colgroup>
            <col width="11%" />
            <col width="11%" />
            <col width="11%" />
            <col width="11%" />
            <col width="11%" />
            <col width="11%" />
            <col width="11%" />
            <col width="11%" />
        </colgroup>
        <tr>
            <td class="inner_cell_right">
                工具编码：
            </td>
            <td>
                <asp:TextBox ID="txtToolCode" runat="server" class="txt" checkexpession="NotNull"
                    datacol="yes" err="此项" />
            </td>
            <td class="inner_cell_right">
                工具名称：
            </td>
            <td>
                <asp:TextBox ID="txtToolName" runat="server" class="txt" checkexpession="NotNull"
                    datacol="yes" err="此项" />
            </td>
            <td class="inner_cell_right">
                有效日期：
            </td>
            <td>
                <asp:TextBox ID="txtEffectiveDate" runat="server" onfocus="WdatePicker()" checkexpession="NotNull"
                    datacol="yes" err="此项" class="txt" />
            </td>
            <td class="inner_cell_right">
                登记人ID:
            </td>
            <td>
                <asp:TextBox ID="txtRegistrantoid" runat="server" class="txt" Enabled="false" />
            </td>
        </tr>
        <tr>
            <td class="inner_cell_right">
                出厂编码：
            </td>
            <td>
                <asp:TextBox ID="txtFactoryCode" runat="server" class="txt" checkexpession="NotNull"
                    datacol="yes" err="此项" />
            </td>
            <td class="inner_cell_right">
                规格型号：
            </td>
            <td>
                <asp:TextBox ID="txtSpecType" runat="server" class="txt" checkexpession="NotNull"
                    datacol="yes" err="此项" />
            </td>
            <td class="inner_cell_right">
                工具类别：
            </td>
            <td>
                <asp:DropDownList ID="ddlToolType" class="select" runat="server" Width="100%" datacol="yes"
                    err="此项" checkexpession="NotNull">
                </asp:DropDownList>
            </td>
            <td class="inner_cell_right">
                登记日期：
            </td>
            <td>
                <asp:TextBox ID="txtRegistrantDate" runat="server" Enabled="false" class="txt" />
            </td>
        </tr>
        <tr>
            <td class="inner_cell_right">
                状态：
            </td>
            <td>
                <asp:DropDownList ID="ddlCFStatus" runat="server" Width="100%" Enabled="false" class="select">
                </asp:DropDownList>
            </td>
            <td class="inner_cell_right">
            显示顺序：
            </td>
            <td>
            <asp:TextBox ID="txtLAST_UPD_BY" runat="server" class="txt" />
            </td>
            <td class="inner_cell_right">
            </td>
            <td>
            </td>
            <td class="inner_cell_right">
            </td>
            <td>
            </td>
        </tr>
    </table>
    <div style="text-align: left;">
        <uc2:LoadButton ID="LoadButton1" runat="server" />
    </div>
    <div style="text-align: right; display: none;">
        <asp:Button ID="btnSubmit" runat="server" OnClick="btnSubmit_Click" Text="btnSubmit_Click" />
        <asp:Button ID="btnSearch" runat="server" OnClick="btnSearch_Click" Text="btnSearch_Click" />
        <asp:Button ID="btnSave" runat="server" OnClick="btnSave_Click" Text="btnSave_Click"
            OnClientClick="return CheckDataValid('#form1');" />
        <asp:Button ID="btnAdd" runat="server" OnClick="btnAdd_Click" Text="btnAdd_Click" />
        <asp:Button ID="btnDel" runat="server" OnClick="btnDel_Click" Text="btnDel_Click" />
    </div>
    <div class="div-body">
        <table id="table1" class="grid" singleselect="true">
            <colgroup>
                <col width="15%" />
                <col width="15%" />
                <col width="15%" />
                <col width="10%" />
                <col width="15%" />
                <col width="10%" />
                 <col width="10%" />
                <col width="10%" />
            </colgroup>
            <thead>
                <tr>
                    <td style="text-align: center;">
                        工具编码
                    </td>
                    <td style="text-align: center;">
                        工具名称
                    </td>
                    <td style="text-align: center;">
                        规格型号
                    </td>
                    <td style="text-align: center;">
                        <asp:LinkButton ID="LinkButton1" runat="server" OnClick="LinkButton1_Click">有效日期</asp:LinkButton>
                    </td>
                    <td style="text-align: center;">
                        出厂编码
                    </td>
                    <td style="text-align: center;">
                        工具类别
                    </td>
                     <td style="text-align: center;">
                        显示顺序
                    </td>
                    <td style="text-align: center;">
                        状态
                    </td>
                </tr>
            </thead>
            <tbody>
                <asp:Repeater ID="rp_Item" runat="server" OnItemDataBound="rp_ItemDataBound" OnItemCommand="rp_Item_ItemCommand">
                    <ItemTemplate>
                  
                        <tr <asp:Literal ID="Literal1" runat="server"></asp:Literal>>
                            <td style="text-align: center;">
                                <asp:LinkButton ID="LBtnDel" runat="server" OnClientClick="selectrow(this)" CommandArgument='<%#Eval("OID") %>' CommandName="del"><%#Eval("ToolCode")%></asp:LinkButton>
                            </td>
                            <td style="text-align: center;">
                                <%#Eval("ToolName")%>
                            </td>
                            <td style="text-align: center;">
                                <%#Eval("SpecType")%>
                            </td>
                            <td style="text-align: center;">
                                
                                <asp:Label ID="lblEffectiveDate" runat="server" Text='<%#Eval("EffectiveDate","{0:yyyy-MM-dd}")%>'></asp:Label>

                            </td>
                            <td style="text-align: center;">
                                <%#Eval("FactoryCode")%>
                            </td>
                            <td style="text-align: center;">
                                <asp:Label ID="lblToolType" runat="server" Text='<%#Eval("ToolType")%>'></asp:Label>
                            </td>
                             <td style="text-align: center;">
                                <%#Eval("LAST_UPD_BY")%>
                            </td>
                            <td style="text-align: center;">
                                <asp:Label ID="lblStatus" runat="server" Text='<%#Eval("Status")%>'></asp:Label>
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
            </tbody>
        </table>
    </div>
    <uc1:PageControl ID="PageControl1" runat="server" />

      </ContentTemplate>
        <Triggers>
      
            <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>
    </form>
</body>
</html>
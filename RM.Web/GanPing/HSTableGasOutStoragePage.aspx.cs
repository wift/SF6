﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using RM.Common.DotNetCode;
using System.Data;
using RM.Common.DotNetUI;
using RM.Web.App_Code;
using EGMNGS.Common;
using EGMNGS.Model;

namespace RM.Web.GanPing
{
    public partial class HSTableGasOutStoragePage : PageBase
    {

        #region 属性字段

        EGMNGS.BLL.TableGasOutStorage bll = new EGMNGS.BLL.TableGasOutStorage();
        EGMNGS.BLL.DetailsGasOutStorage DetailsBLL = new EGMNGS.BLL.DetailsGasOutStorage();


        private Dictionary<string, string> lsCFStatus
        {
            get
            {
                if (Session["GPStatus"] != null)
                {
                    return Session["GPStatus"] as Dictionary<string, string>;
                }
                return new Dictionary<string, string>();
            }

            set
            {
                Session["GPStatus"] = value;
            }
        }

        private DataTable UserList
        {
            get
            {
                return ViewState["UserList"] as DataTable;
            }
            set { ViewState["UserList"] = value; }
        }
        private EGMNGS.Model.TableGasOutStorage Model
        {
            get
            {
                if (ViewState["TableGasOutStorage"] == null)
                {
                    return new EGMNGS.Model.TableGasOutStorage();
                }
                return ViewState["TableGasOutStorage"] as EGMNGS.Model.TableGasOutStorage;
            }
            set { ViewState["TableGasOutStorage"] = value; }
        }
        private string actionMode
        {
            get { return ViewState["ActionMode"] as String; }
            set { ViewState["ActionMode"] = value; }
        }
        public string PowerSupplyCode
        {
            get { return ViewState["PowerSupplyCode"] as String; }
            set { ViewState["PowerSupplyCode"] = value; }
        }
        public string PowerSupplyName
        {
            get { return ViewState["PowerSupplyName"] as String; }
            set { ViewState["PowerSupplyName"] = value; }
        }

        public DataTable DetailsTable
        {
            get
            {
                if (ViewState["DetailsTable"] == null)
                {
                    return new DataTable();
                }
                return ViewState["DetailsTable"] as DataTable;
            }
            set { ViewState["DetailsTable"] = value; }
        }
        private string actionModeDetails
        {
            get
            {
                if (ViewState["actionModeDetails"] == null)
                {
                    return string.Empty;
                }
                return ViewState["actionModeDetails"] as String;
            }
            set { ViewState["actionModeDetails"] = value; }
        }

        private List<DetailsGasOutStorage> UpdateList
        {
            get
            {
                if (ViewState["UpdateList"] == null)
                {
                    return new List<DetailsGasOutStorage>();
                }
                return ViewState["UpdateList"] as List<DetailsGasOutStorage>;
            }
            set { ViewState["UpdateList"] = value; }
        }
        /// <summary>
        /// 需要添加的数据
        /// </summary>
        private List<DetailsGasOutStorage> AddList
        {
            get
            {
                if (ViewState["AddList"] == null)
                {
                    return new List<DetailsGasOutStorage>();
                }
                return ViewState["AddList"] as List<DetailsGasOutStorage>;
            }
            set { ViewState["AddList"] = value; }
        }
        #endregion

        #region 事件方法

        protected void Page_Load(object sender, EventArgs e)
        {
            this.PageControl1.pageHandler += new EventHandler(pager_PageChanged);

            if (!IsPostBack)
            {
                EGMNGS.BLL.SYS_CODE sysCodeBll = new EGMNGS.BLL.SYS_CODE();
                lsCFStatus = sysCodeBll.GetModelList("CODE_TYPE='GPStatus' ORDER BY DISPLAY_ORDER").ToDictionary(o => o.CODE, o => o.CODE_CHI_DESC);

                UserList = ComServies.GetAllUserInfo();
                string[] orgIdOrgName = ComServies.GetPowerApplyByUserID(User.UserId.ToString());
                PowerSupplyCode = orgIdOrgName[0];
                PowerSupplyName = orgIdOrgName[1];
                DropDownListBinder();
            }
        }

        /// <summary>
        /// 绑定下拉列
        /// </summary>
        private void DropDownListBinder()
        {

            EGMNGS.BLL.SYS_CODE sysCodeBll = new EGMNGS.BLL.SYS_CODE();
            DataSet dsYear = sysCodeBll.GetList("CODE_TYPE='Year' ORDER BY DISPLAY_ORDER");

            this.ddlYear.DataSource = dsYear.Tables[0];
            this.ddlYear.DataTextField = "CODE_CHI_DESC";
            this.ddlYear.DataValueField = "CODE";
            this.ddlYear.DataBind();

            DataSet dsMonth = sysCodeBll.GetList("CODE_TYPE='Month' ORDER BY DISPLAY_ORDER");

            this.ddlMonth.DataSource = dsMonth.Tables[0];
            this.ddlMonth.DataTextField = "CODE_CHI_DESC";
            this.ddlMonth.DataValueField = "CODE";
            this.ddlMonth.DataBind();


            sysCodeBll = new EGMNGS.BLL.SYS_CODE();
            DataSet dsGasType = sysCodeBll.GetList("CODE_TYPE='GasClass' ORDER BY DISPLAY_ORDER");

            ddlGasType.DataSource = dsGasType.Tables[0];
            ddlGasType.DataTextField = "CODE_CHI_DESC";
            ddlGasType.DataValueField = "CODE";
            ddlGasType.DataBind();
            ddlGasType.SelectedValue = "1";


            DataSet dtCFStatus = sysCodeBll.GetList("CODE_TYPE='CFStatus' ORDER BY DISPLAY_ORDER");

            this.ddlStatus.DataSource = dtCFStatus.Tables[0];
            this.ddlStatus.DataTextField = "CODE_CHI_DESC";
            this.ddlStatus.DataValueField = "CODE";
            this.ddlStatus.DataBind();

            this.ddlStatus.Items.Insert(0, string.Empty);


        }


        /// <summary>
        /// 绑定后激发事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void rp_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Label lblUserID = e.Item.FindControl("lblUserID") as Label;
                Label lblGasClass = e.Item.FindControl("lblGasClass") as Label;

                if (lblUserID != null)
                {
                    lblUserID.Text = GetUserName(lblUserID.Text);
                }

                if (lblGasClass != null)
                {
                    lblGasClass.Text = this.ddlGasType.Items.FindByValue(lblGasClass.Text).Text;
                }


            }
        }

        /// <summary>
        /// 绑定数据，分页
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void pager_PageChanged(object sender, EventArgs e)
        {
            DataBindGrid();
        }

        /// <summary>
        /// 绑定数据源
        /// </summary>
        private void DataBindGrid()
        {
            int count = bll.GetRecordCount("GasClass='1'");
            DataSet ds = bll.GetListByPage("GasClass='1'", "Code desc", PageControl1.PageIndex, PageControl1.PageSize);
            ControlBindHelper.BindRepeaterList(ds.Tables[0], rp_Item);
            this.PageControl1.RecordCount = Convert.ToInt32(count);
        }

        /// <summary>
        /// 绑定数据到对象
        /// </summary>
        private void DataBinder()
        {
            Model.Code = this.txtCode.Text;
            Model.Year = this.ddlYear.SelectedValue;
            Model.Month = this.ddlMonth.SelectedValue;
            Model.GasClass = this.ddlGasType.SelectedValue;
            if (this.txtAmountOutStorage.Text.Trim().Length > 0)
            {
                Model.AmountOutStorage = decimal.Parse(this.txtAmountOutStorage.Text);
            }
        }

        /// <summary>
        /// 初始化控件
        /// </summary>
        private void InitControl()
        {
            this.txtCode.Text = Model.Code;
            this.ddlMonth.SelectedValue = Model.Month;
            this.ddlYear.SelectedValue = Model.Year;
            this.ddlGasType.SelectedValue = Model.GasClass;

            this.txtAmountOutStorage.Text = Model.AmountOutStorage.ToString();

            this.ddlStatus.SelectedValue = Model.Status;

            this.txtRegistantOID.Text = GetUserName(Model.RegistantOID);
            this.txtRegistantDate.Text = Model.RegistantDate.ToShortDateString();

        }

        /// <summary>
        /// 获取用户名
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        private string GetUserName(string userId)
        {
            DataRow[] dr = UserList.Select(string.Format("User_ID='{0}'", userId));
            if (dr.Length > 0)
            {
                return dr[0][1].ToString();
            }
            return string.Empty;
        }
        #endregion

        #region 按钮事件

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            if (Model.Status == "1")
            {
                ShowMsgHelper.Alert_Wern("无法重新提交！");
                return;
            }

            if (ComServies.UpdateHSTableGasOutStoragePageStatus(Model.Code))
            {
                Model = bll.GetModel(Model.OID);
                ShowMsgHelper.Alert("提交成功！");
                InitControl();
                DataBindGrid();
            }
        }
        /// <summary>
        /// 判断出库量是否与从表一致。
        /// </summary>
        /// <returns></returns>
        private bool CheckDetails()
        {
            bool flag = false;

            try
            {
                List<EGMNGS.Model.DetailsGasOutStorage> listDt = DetailsBLL.GetModelList(string.Format("TableGasOutStorage_Code='{0}' order by oid desc", Model.Code));
                decimal? SumAmount = 0;
                foreach (DetailsGasOutStorage item in listDt)
                {
                    SumAmount += item.AmountGas;
                }

                if (SumAmount == Model.AmountOutStorage)
                {
                    flag = true;
                }
                else
                {
                    flag = false;
                }
            }
            catch (Exception ex)
            {
                flag = false;

            }

            return flag;
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            DataBindGrid();
        }

        private void ShowMaxMode()
        {
            Model = bll.GetModel(bll.GetMaxId() - 1);
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                if (actionMode == "Add")
                {
                    DataBinder();
                    Model.RegistantOID = User.UserId.ToString();

                    int count = bll.GetRecordCount(string.Format("Code='{0}'", Model.Code));
                    if (count > 0)
                    {
                        ShowMsgHelper.showWarningMsg("出库编号已经存在!");
                        return;
                    }

                    if (bll.Add(Model) > 0)
                    {
                        actionMode = string.Empty;
                    }

                    DataBindGrid();
                    ShowMaxMode();
                    GetDetailsOfRecycleGas();
                }
                else if (actionMode == "Edit")
                {
                    if (bll.GetModel(Model.OID).Status != "0")
                    {
                        ShowMsgHelper.Alert_Wern("“已确认”状态无法修改数据！");
                        return;
                    }

                    DataBinder();
                    if (bll.Update(Model))
                    {
                        EGMNSShowMsg.ShowEditMsgSuccess();
                        actionMode = string.Empty;
                    }
                    DataBindGrid();
                }
            }
            catch (ArgumentOutOfRangeException ex)
            {
                string[] errs = CommonHelper.GetValue(ex.Message, "@").Split(':');
                ShowMsgHelper.ExecuteScript(string.Format("ChangeCss($('#txt{0}'),'长度不能超{1}字')", errs[0], errs[1]));
            }
        }
        private void ClearControl()
        {
            Model = new TableGasOutStorage();
            InitControl();
        }
        protected void btnAdd_Click(object sender, EventArgs e)
        {
            if (ComServies.GetCheckStatus(DateTime.Now))
            {
                ShowMsgHelper.Alert_Wern("已对账");
                return;
            }

            actionMode = "Add";
            Model = new EGMNGS.Model.TableGasOutStorage(actionMode);
            Model.PowerSupplyCode = PowerSupplyCode;
            Model.PowerSupplyName = PowerSupplyName;
            Model.RegistantDate = DateTime.Now;
            Model.RegistantOID = User.UserId.ToString();
            Model.Status = "0";
            Model.GasClass = "1";
            InitControl();

            GetDetailsOfRecycleGas();
        }


        protected void btnDel_Click(object sender, EventArgs e)
        {

            if (bll.GetModel(Model.OID).Status != "0")
            {
                ShowMsgHelper.Alert_Wern("“已确认”状态不可以删除");
            }
            else
            {
                bll.Delete(Model.OID);
                Model = new EGMNGS.Model.TableGasOutStorage();
                InitControl();
                DataBindGrid();
                ShowMsgHelper.Alert("删除成功！");
            }
        }

        protected void rp_Item_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            int oid = Convert.ToInt32(e.CommandArgument);

            Model = bll.GetModel(oid);
            InitControl();
            actionMode = "Edit";

            //绑定明细表
            GetDetailsOfRecycleGas();
        }
        #endregion

        #region Details
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void pager_PageChangedDetails(object sender, EventArgs e)
        {
            GetDetailsOfRecycleGas();
        }

        private void GetDetailsOfRecycleGas()
        {
            DetailsTable = DetailsBLL.GetList(string.Format("TableGasOutStorage_Code='{0}' order by oid desc", Model.Code)).Tables[0];

            //int count = DetailsBLL.GetRecordCount(string.Format("TableGasOutStorage_Code='{0}'", Model.Code));
            ControlBindHelper.BindRepeaterList(DetailsTable, rptDetails);
        }
        protected void btnAddDetails_Click(object sender, EventArgs e)
        {
            if (Model.Status.Length == 0)
            {
                return;
            }
            if (Model.Status == "1")
            {
                ShowMsgHelper.Alert_Wern("该记录已提交！");
                return;
            }
            //首先，恢复数据源
            if (IsRetriRepeter())
            {
                return;
            }

            DataTable dt = DetailsTable;
            // LoadTestData(dt);
            DataRow row = dt.NewRow();
            if (dt.Columns.Count == 0)
            { return; }
            row[2] = string.Empty;
            row[3] = string.Empty;
            row[4] = string.Empty;
            row[5] = 0.0;
            row["TableGasOutStorage_Code"] = Model.Code;
            dt.Rows.Add(row);

            rptDetails.DataSource = dt;
            rptDetails.DataBind();


            actionModeDetails = "Add";
        }

        private bool IsRetriRepeter()
        {
            bool flag = false; ;
            for (int i = 0; i < this.rptDetails.Items.Count; i++)
            {
                Label txtAmountGas = this.rptDetails.Items[i].FindControl("txtAmountGas") as Label;
                TextBox txtCylinderCode = this.rptDetails.Items[i].FindControl("txtCylinderCode") as TextBox;
                var ls = new EGMNGS.BLL.DetailsGasStorage().GetModelList(string.Format("CylinderCode='{0}'", txtCylinderCode.Text));

                if (txtCylinderCode.Text.Length > 0)
                {

                    if (ls.Count == 0 || ls.Where(o => o.FillStatus == "2").Count() == 0 || ls.Where(o => o.FillStatus == "3").Count() > 0)
                    {
                        ShowMsgHelper.Alert_Error(string.Format("此钢瓶{0},不在回收气体入库表中或已出库", txtCylinderCode.Text));
                        flag = true;
                        break;
                    }

                }
                DetailsTable.Rows[i]["AmountGas"] = Convert.ToDecimal(txtAmountGas.Text);

                DetailsTable.Rows[i]["CylinderCode"] = txtCylinderCode.Text;
            }

            return flag;
        }

        protected void btnDelDetails_Click(object sender, EventArgs e)
        {
            try
            {
                if (Model.Status == "1")
                {
                    ShowMsgHelper.Alert_Wern("该记录已提交！");
                    return;
                }

                string strID = HiddenField1.Value;
                if (strID.Trim().Length == 0)
                {
                    return;

                }
                DetailsBLL.DeleteList(strID);
                ShowMsgHelper.Alert("删除成功！");
                GetDetailsOfRecycleGas();
            }
            catch (Exception)
            {
                ShowMsgHelper.Alert("删除失败！");
                throw;
            }
        }

        protected void btnSaveDetails_Click(object sender, EventArgs e)
        {
            try
            {
                if (Model.Status == "1")
                {
                    ShowMsgHelper.Alert_Wern("该记录已提交！");
                    return;
                }
                //首先，恢复数据源
                if (IsRetriRepeter())
                {
                    return;
                }
                GetCheckBox();
                if (actionModeDetails.Equals("Add"))
                {
                    if (AddList.Count > 0)
                    {
                        //批量添加
                        foreach (var item in AddList)
                        {
                            var listDetailsGasStorage = new EGMNGS.BLL.DetailsGasStorage().GetModelList(string.Format("CylinderCode='{0}' AND FillStatus='2'", item.CylinderCode));
                            item.GasCode = listDetailsGasStorage[0].GasCode;
                            item.AmountGas = listDetailsGasStorage[0].AmountGas;
                            item.CylinderStatus = listDetailsGasStorage[0].CylinderStatus;

                            if (DetailsBLL.GetRecordCount(string.Format(@"GasCode='{0}' and exists(select * from TableGasOutStorage t 
where TableGasOutStorage_Code=t.Code and t.GasClass='1') ", item.GasCode)) >= 1)
                            {
                                continue;
                            }

                            DetailsBLL.Add(item);
                        }
                        AddList.Clear();
                    }
                    ShowMsgHelper.Alert("添加成功！");
                    actionModeDetails = string.Empty;

                }

                if (UpdateList.Count > 0)
                {
                    //批量修改
                    foreach (var item in UpdateList)
                    {
                        var listDetailsGasStorage = new EGMNGS.BLL.DetailsGasStorage().GetModelList(string.Format("CylinderCode='{0}' AND FillStatus='2'", item.CylinderCode));
                        item.CylinderCode = listDetailsGasStorage[0].CylinderCode;
                        item.GasCode = listDetailsGasStorage[0].GasCode;
                        item.AmountGas = listDetailsGasStorage[0].AmountGas;
                        item.CylinderStatus = listDetailsGasStorage[0].CylinderStatus;
                        DetailsBLL.Update(item);
                    }
                    UpdateList.Clear();
                    ShowMsgHelper.Alert("修改成功！");
                }

                GetDetailsOfRecycleGas();
            }
            catch (Exception ex)
            {
                ShowMsgHelper.AlertMsg("添加失败！");
            }
        }

        protected void rptDetails_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                CheckBox cb = e.Item.FindControl("checkbox") as CheckBox;
                if (cb.ToolTip.Length == 0)
                {
                    cb.Checked = true;

                    TextBox tb = e.Item.FindControl("txtCylinderCode") as TextBox;
                    tb.Focus();
                }
            }
        }


        /// <summary>
        /// 
        /// </summary>
        private void GetCheckBox()
        {


            UpdateList = new List<DetailsGasOutStorage>();
            AddList = new List<DetailsGasOutStorage>();
            for (int i = 0; i < this.rptDetails.Items.Count; i++)
            {
                System.Web.UI.WebControls.CheckBox checkbox = (System.Web.UI.WebControls.CheckBox)rptDetails.Items[i].FindControl("checkbox");
                TextBox txtCylinderCode = this.rptDetails.Items[i].FindControl("txtCylinderCode") as TextBox;
                if (txtCylinderCode.Text.Trim().Length == 0)
                {
                    continue;
                }
                if (checkbox.Checked == true && checkbox.ToolTip.Length > 0)
                {
                    //  TextBox txtCylinderCode = this.rptDetails.Items[i].FindControl("txtCylinderCode") as TextBox;

                    Label txtAmountGas = this.rptDetails.Items[i].FindControl("txtAmountGas") as Label;

                    DetailsGasOutStorage DetailsOfRecycleGasObj = new EGMNGS.BLL.DetailsGasOutStorage().GetModel(int.Parse(checkbox.ToolTip));
                    // DetailsOfRecycleGasObj.TableGasOutStorage_Code = Model.Code;
                    //DetailsOfRecycleGasObj.OID = int.Parse(checkbox.ToolTip);
                    DetailsOfRecycleGasObj.CylinderCode = txtCylinderCode.Text;
                    if (txtAmountGas.Text.Trim().Length > 0)
                    {
                        DetailsOfRecycleGasObj.AmountGas = decimal.Parse(txtAmountGas.Text);
                    }

                    UpdateList.Add(DetailsOfRecycleGasObj);
                }
                else if (checkbox.Checked == true && checkbox.ToolTip.Length == 0)
                {
                    //   TextBox txtCylinderCode = this.rptDetails.Items[i].FindControl("txtCylinderCode") as TextBox;
                    Label txtAmountGas = this.rptDetails.Items[i].FindControl("txtAmountGas") as Label;

                    DetailsGasOutStorage DetailsOfRecycleGasObj = new EGMNGS.Model.DetailsGasOutStorage();
                    DetailsOfRecycleGasObj.TableGasOutStorage_Code = Model.Code;

                    DetailsOfRecycleGasObj.CylinderCode = txtCylinderCode.Text;
                    if (txtAmountGas.Text.Trim().Length > 0)
                    {
                        DetailsOfRecycleGasObj.AmountGas = decimal.Parse(txtAmountGas.Text);
                    }

                    AddList.Add(DetailsOfRecycleGasObj);
                }

            }



        }

        #endregion
    }
}
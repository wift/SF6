﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ApplDetectionGasPage.aspx.cs"
    Inherits="RM.Web.GanPing.ApplDetectionGasPage" EnableEventValidation="false"
    ClientIDMode="AutoID" %>

<%@ Register Src="~/UserControl/PageControl.ascx" TagName="PageControl" TagPrefix="uc1" %>
<%@ Register Src="~/UserControl/LoadButton.ascx" TagName="LoadButton" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>入网检测申请</title>
    <link href="/Themes/Styles/Site.css" rel="stylesheet" type="text/css" />
    <script src="/Themes/Scripts/Validator/JValidator.js" type="text/javascript"></script>
    <script src="/Themes/Scripts/jquery-1.8.2.min.js" type="text/javascript"></script>
    <script src="/Themes/Scripts/jquery.pullbox.js" type="text/javascript"></script>
    <script src="/Themes/Scripts/FunctionJS.js" type="text/javascript"></script>
    <script src="/Themes/Scripts/DatePicker/WdatePicker.js" type="text/javascript"></script>
    <script type="text/javascript">
        //添加
        function add() {
            document.getElementById('<%= this.btnAdd.ClientID %>').click();
        }
        //查询
        function search() {

            document.getElementById('<%= this.btnSearch.ClientID %>').click();
        }
        //删除
        function Delete() {
            showConfirmMsg("此操作不可恢复，您确定要删除吗？", function (r) {
                if (r) {
                    document.getElementById('<%= this.btnDel.ClientID %>').click();
                }
            });
        }
        //保存
        function SaveForm() {
            document.getElementById('<%= this.btnSave.ClientID %>').click();
        }
        //提交
        function Submit() {
            document.getElementById('<%= this.btnSubmit.ClientID %>').click();
        }


        //明细表

        function DelDetails() {
            var key = CheckboxValue1();
            showConfirmMsg("此操作不可恢复，您确定要删除吗？", function (r) {
                if (r) {
                    document.getElementById('<%= this.HiddenField1.ClientID %>').value = key.toString();
                    document.getElementById('<%= this.btnDelDetails.ClientID %>').click();
                }
            });
        }

        $(function () {
            InitControl();

        })
        function InitControl() {
            $(".div-body").PullBox({ dv: $(".div-body"), obj: $("#table1").find("tr") });
            divresize(160);
            FixedTableHeader("#tableDetails", $(window).height() - 90);
            checkboxdetail();
        }


        var objDev = {};
        function ShowSelectDialog() {
            var $ddlsConvertStationCode = $("#ddlConvertStationName").val();
            var url = "/GanPing/SelectSBDialog.aspx?ConvertStationCode=" + $ddlsConvertStationCode;
           
            top.openDialog(url, 'SelectSBDialog', '详细信息 - 查看', 1000, 500, 50, 50, function myfunction() {
              
                $("#txtDevCode").val(objDev.devcode);
                $("#txtDevName").val(objDev.devname);
                $("#tbDevNo").val(objDev.DevType);
            });


        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:HiddenField ID="HiddenField1" runat="server" />
            <table width="100%">
                <colgroup>
                    <col width="7%" />
                    <col width="7%" />
                    <col width="7%" />
                    <col width="7%" />
                    <col width="7%" />
                    <col width="7%" />
                    <col width="8%" />
                    <col width="7%" />
                </colgroup>
                <tr>
                    <td class="inner_cell_right">
                        申请编号：
                    </td>
                    <td>
                        <asp:TextBox ID="tbAppCode" runat="server" checkexpession="NotNull" datacol="yes"
                            err="此项" />
                    </td>
                    <td class="inner_cell_right">
                        供电局名称：
                    </td>
                    <td>
                        <asp:TextBox ID="tbPowerSupplyName" runat="server" Enabled="false" />
                    </td>
                    <td class="inner_cell_right">
                        变电站名称：
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlConvertStationName" runat="server" Width="100%" 
                             CssClass="select"
                            checkexpession="NotNull" datacol="yes" err="此项">
                        </asp:DropDownList>
                    </td>
                    <td class="inner_cell_right">
                        送检单位：
                    </td>
                    <td>
                        <asp:TextBox ID="tbInspectionUnit" runat="server" checkexpession="NotNull" datacol="yes"
                            err="此项" />
                    </td>
                </tr>
                <tr>
                    <td class="inner_cell_right">
                        设备编码：
                    </td>
                    <td>
                      <%--  <asp:DropDownList ID="ddlDevCode" runat="server" Width="100%" AutoPostBack="true"
                            OnSelectedIndexChanged="ddlDevCode_SelectedIndexChanged" CssClass="select">
                        </asp:DropDownList>--%>

                        <asp:TextBox ID="txtDevCode" runat="server" Width="70%"  />
                        <input id="selectDevcode" value="选择"  type="button" onclick="ShowSelectDialog()" style="width: 33px; padding-left: 3px;" />
                    </td>
                    <td class="inner_cell_right">
                        设备名称：
                    </td>
                    <td>
                        <asp:TextBox ID="txtDevName" runat="server" />
                    </td>
                    <td class="inner_cell_right">
                        设备型号：
                    </td>
                    <td>
                        <asp:TextBox ID="tbDevNo" runat="server" />
                    </td>
                    <td class="inner_cell_right">
                        工程名称：
                    </td>
                    <td>
                        <asp:TextBox ID="tbSampliInspituation" runat="server" checkexpession="NotNull" datacol="yes"
                            err="此项"/>
                    </td>
                </tr>
                <tr>
                    <td class="inner_cell_right">
                        联系人：
                    </td>
                    <td>
                        <asp:TextBox ID="tbContacts" runat="server" checkexpession="NotNull" datacol="yes"
                            err="此项" />
                    </td>
                    <td class="inner_cell_right">
                        手机：
                    </td>
                    <td>
                        <asp:TextBox ID="tbPhoneNum" runat="server" checkexpession="NotNull" datacol="yes"
                            err="此项" />
                    </td>
                    <td class="inner_cell_right">
                        通讯地址：
                    </td>
                    <td>
                        <asp:TextBox ID="tbOfficeTel" runat="server" checkexpession="NotNull" datacol="yes"
                            err="此项" />
                    </td>
                    <td class="inner_cell_right">
                        检测气量（kg）：
                    </td>
                    <td>
                        <asp:TextBox ID="tbAmountDetection" runat="server" Enabled="false" />
                    </td>
                </tr>
                <tr>
                    <td class="inner_cell_right">
                        检测项目类型:
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlCheckPJType" runat="server" Width="100%">
                        </asp:DropDownList>
                    </td>
                    <td class="inner_cell_right">
                        登记人：
                    </td>
                    <td>
                        <asp:TextBox ID="txtRegistantsoid" runat="server" Enabled="false" />
                    </td>
                    <td class="inner_cell_right">
                        登记日期：
                    </td>
                    <td>
                        <asp:TextBox ID="txtRegistantsDate" runat="server" Enabled="false" />
                    </td>
                    <td class="inner_cell_right">
                        状态：
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlStatus" runat="server" Width="100%" Enabled="false">
                        </asp:DropDownList>
                    </td>
                </tr>
            </table>
            <div style="text-align: left;">
                <uc2:LoadButton ID="LoadButton1" runat="server" />
                <div style="clear: left; float: right;">
                    <asp:LinkButton ID="Add" runat="server" class="l-btn" OnClick="btnAddDetails_Click"><span class="l-btn-left">
            <img src="/Themes/Images/13.png" alt="" />添 加</span></asp:LinkButton>
                    <a class="l-btn" onclick="DelDetails()"><span class="l-btn-left">
                        <img src="/Themes/Images/delete.png" alt="" />删 除</span></a>
                    <asp:LinkButton ID="Save" runat="server" class="l-btn" OnClick="btnSaveDetails_Click"><span class="l-btn-left">
            <img src="/Themes/Images/disk.png" alt="" />保 存</span></asp:LinkButton>
                </div>
            </div>
            <div style="text-align: right; display: none;">
                <asp:Button ID="btnSubmit" runat="server" OnClick="btnSubmit_Click" Text="btnSubmit_Click"
                OnClientClick="return CheckDataValid('#tableDetails');" />
                <asp:Button ID="btnSearch" runat="server" OnClick="btnSearch_Click" Text="btnSearch_Click" />
                <asp:Button ID="btnSave" runat="server" OnClick="btnSave_Click" Text="btnSave_Click"
                    OnClientClick="return CheckDataValid('#form1');" />
                <asp:Button ID="btnAdd" runat="server" OnClick="btnAdd_Click" Text="btnAdd_Click" />
                <asp:Button ID="btnDel" runat="server" OnClick="btnDel_Click" Text="btnDel_Click" />
                <asp:Button ID="btnDelDetails" runat="server" OnClick="btnDelDetails_Click" Text="btnDel_Click" />
            </div>
            <div>
                <table width="100%">
                    <tr>
                        <td width="50%" style="vertical-align: top;">
                            <div class="div-body">
                                <table id="table1" class="grid" singleselect="true">
                                    <colgroup>
                                        <col width="20%" />
                                        <col width="15%" />
                                        <col width="15%" />
                                        <col width="15%" />
                                        <col width="15%" />
                                        <col width="15%" />
                                    </colgroup>
                                    <thead>
                                        <tr>
                                            <td style="text-align: center;">
                                                申请编号
                                            </td>
                                            <td style="text-align: center;">
                                                变电站名称
                                            </td>
                                            <td style="text-align: center;">
                                                送检单位
                                            </td>
                                            <td style="text-align: center;">
                                                工程名称
                                            </td>
                                            <td style="text-align: center;">
                                                状态
                                            </td>
                                            <td style="text-align: center;">
                                                登记日期
                                            </td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <asp:Repeater ID="rp_Item" runat="server" OnItemDataBound="rp_ItemDataBound" OnItemCommand="rp_Item_ItemCommand">
                                            <ItemTemplate>
                                                <tr>
                                                    <td style="text-align: center;">
                                                        <asp:LinkButton ID="LBtnDel" runat="server" OnClientClick="selectrow(this)" CommandArgument='<%#Eval("OID") %>'
                                                            CommandName="del"><%#Eval("ApplCode")%></asp:LinkButton>
                                                    </td>
                                                    <td style="text-align: center;">
                                                        <%#Eval("ConvertStationName")%>
                                                    </td>
                                                    <td style="text-align: center;">
                                                        <%#Eval("InspectionUnit")%>
                                                    </td>
                                                    <td style="text-align: center;">
                                                        <%#Eval("SampliInspituation")%>
                                                    </td>
                                                    <td style="text-align: center;">
                                                        <asp:Label ID="lblStatus" runat="server" Text='<%#Eval("Status")%>'></asp:Label>
                                                    </td>
                                                    <td style="text-align: center;">
                                                        <%#Eval("RegistantsDate", "{0:yyyy-MM-dd}")%>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Literal runat="server" ID="ltlEmpty" Text="<tr><td colspan='6' style='color:red;text-align:center'>没有找到您要的相关数据！</td></tr>"
                                                    Visible='<%#bool.Parse((rp_Item.Items.Count==0).ToString())%>'></asp:Literal>
                                            </FooterTemplate>
                                        </asp:Repeater>
                                    </tbody>
                                </table>
                            </div>
                            <uc1:PageControl ID="PageControl1" runat="server" />
                        </td>
                        <td width="50%" style="vertical-align: top;">
                            <div class="div-body1">
                                <table id="tableDetails" class="grid1" singleselect="true">
                                    <colgroup>
                                        <col width="5%" />
                                        <col width="5%" />
                                        <col width="15%" />
                                        <col width="15%" />
                                        <col width="10%" />
                                        <col width="15%" />
                                        <col width="15%" />
                                        <col width="10%" />
                                        <col width="10%" />
                                    </colgroup>
                                    <thead>
                                        <tr>
                                            <td style="text-align: center;">
                                                序号
                                            </td>
                                            <td style="text-align: left;">
                                                <label id="checkAllOff" onclick="CheckAllLine1()" title="全选">
                                                    &nbsp;</label>
                                            </td>
                                            <td style="text-align: center;">
                                                钢瓶编码
                                            </td>
                                            <td style="text-align: center;">
                                                气体编码
                                            </td>
                                            <td style="text-align: center;">
                                                气量(kg)
                                            </td>
                                            <td style="text-align: center;">
                                                出厂编号
                                            </td>
                                            <td style="text-align: center;">
                                                生产厂家
                                            </td>
                                            <td style="text-align: center;">
                                                是否合格
                                            </td>
                                            <td style="text-align: center;">
                                                检测日期
                                            </td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <asp:Repeater ID="rptDetails" runat="server" OnItemDataBound="rptDetails_ItemDataBound">
                                            <ItemTemplate>
                                                <tr>
                                                    <td style="text-align: center;">
                                                        <%# Container.ItemIndex + 1%>
                                                    </td>
                                                    <td style="width: 20px; text-align: left;">
                                                        <asp:CheckBox ID="checkbox" runat="server" ToolTip='<%#Eval("OID")%>' />
                                                    </td>
                                                    <td style="width: 80px; text-align: center;">
                                                        <%#Eval("CylinderCode")%>
                                                    </td>
                                                    <td style="width: 80px; text-align: center;">
                                                        <%#Eval("GasCode")%>
                                                    </td>
                                                    <td style="width: 50px; text-align: center;">
                                                        <asp:TextBox ID="txtAmountGas" runat="server" Text='<%#Eval("AmountGas")%>' Width="100%"></asp:TextBox>
                                                    </td>
                                                    <td style="text-align: center;">
                                                        <asp:TextBox ID="txtProduceNum" runat="server" Text=' <%#Eval("ProduceNum")%>' Width="100%"
                                                            CssClass="txt" checkexpession="NotNull" datacol="yes" err="此项"></asp:TextBox>
                                                    </td>
                                                    <td style="text-align: center;">
                                                        <asp:TextBox ID="txtManufacturer" runat="server" Text=' <%#Eval("Manufacturer")%>'
                                                            Width="100%" CssClass="txt" checkexpession="NotNull" datacol="yes" err="此项"></asp:TextBox>
                                                    </td>
                                                    <td style="width: 50px; text-align: center;">
                                                        <%# DictEGMNS["IsPass"][Eval("IsPass").ToString()]%>
                                                    </td>
                                                    <td style="width: 70px; text-align: center;">
                                                        <%#Eval("CheckDate", "{0:yyyy-MM-dd}")%>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Literal runat="server" ID="ltlEmpty" Text="<tr><td colspan='7' style='color:red;text-align:center'>没有找到您要的相关数据！</td></tr>"
                                                    Visible='<%#bool.Parse((rp_Item.Items.Count==0).ToString())%>'></asp:Literal>
                                            </FooterTemplate>
                                        </asp:Repeater>
                                    </tbody>
                                </table>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="ddlConvertStationName" EventName="SelectedIndexChanged" />
      
            <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnAdd" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>
    </form>
</body>
</html>

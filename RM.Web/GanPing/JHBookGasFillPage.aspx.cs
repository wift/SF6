﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using RM.Common.DotNetCode;
using System.Data;
using RM.Common.DotNetUI;
using RM.Web.App_Code;
using EGMNGS.Common;
namespace RM.Web.GanPing
{
    public partial class JHBookGasFillPage : PageBase
    {
        #region 属性字段

        EGMNGS.BLL.BookGasFill bll = new EGMNGS.BLL.BookGasFill();
        private DataTable UserList
        {
            get
            {
                return ViewState["UserList"] as DataTable;
            }
            set { ViewState["UserList"] = value; }
        }
        private EGMNGS.Model.BookGasFill Model
        {
            get
            {
                if (ViewState["BookGasFill"] == null)
                {
                    return new EGMNGS.Model.BookGasFill();
                }
                return ViewState["BookGasFill"] as EGMNGS.Model.BookGasFill;
            }
            set { ViewState["BookGasFill"] = value; }
        }
        private string actionMode
        {
            get { return ViewState["ActionMode"] as String; }
            set { ViewState["ActionMode"] = value; }
        }
        public string PowerSupplyCode
        {
            get { return ViewState["PowerSupplyCode"] as String; }
            set { ViewState["PowerSupplyCode"] = value; }
        }
        public string PowerSupplyName
        {
            get { return ViewState["PowerSupplyName"] as String; }
            set { ViewState["PowerSupplyName"] = value; }
        }

        #endregion

        #region 事件方法

        protected void Page_Load(object sender, EventArgs e)
        {
            this.PageControl1.pageHandler += new EventHandler(pager_PageChanged);
            if (!IsPostBack)
            {
                UserList = ComServies.GetAllUserInfo();
                string[] orgIdOrgName = ComServies.GetPowerApplyByUserID(User.UserId.ToString());
                PowerSupplyCode = orgIdOrgName[0];
                PowerSupplyName = orgIdOrgName[1];
                DropDownListBinder();
            }
        }

        /// <summary>
        /// 绑定下拉列
        /// </summary>
        private void DropDownListBinder()
        {

            EGMNGS.BLL.SYS_CODE sysCodeBll = new EGMNGS.BLL.SYS_CODE();
            DataSet dsYear = sysCodeBll.GetList("CODE_TYPE='Year' ORDER BY DISPLAY_ORDER");

            this.ddlYear.DataSource = dsYear.Tables[0];
            this.ddlYear.DataTextField = "CODE_CHI_DESC";
            this.ddlYear.DataValueField = "CODE";
            this.ddlYear.DataBind();

            DataSet dsMonth = sysCodeBll.GetList("CODE_TYPE='Month' ORDER BY DISPLAY_ORDER");

            this.ddlMonth.DataSource = dsMonth.Tables[0];
            this.ddlMonth.DataTextField = "CODE_CHI_DESC";
            this.ddlMonth.DataValueField = "CODE";
            this.ddlMonth.DataBind();


            sysCodeBll = new EGMNGS.BLL.SYS_CODE();
            DataSet dsGasType = sysCodeBll.GetList("CODE_TYPE='GasType' ORDER BY DISPLAY_ORDER");

            ddlGasType.DataSource = dsGasType.Tables[0];
            ddlGasType.DataTextField = "CODE_CHI_DESC";
            ddlGasType.DataValueField = "CODE";
            ddlGasType.DataBind();
            ddlGasType.SelectedValue = "4";

            DataSet dtFillStatus = sysCodeBll.GetList("CODE_TYPE='FillStatus' ORDER BY DISPLAY_ORDER");

            this.ddlStatus.DataSource = dtFillStatus.Tables[0];
            this.ddlStatus.DataTextField = "CODE_CHI_DESC";
            this.ddlStatus.DataValueField = "CODE";
            this.ddlStatus.DataBind();

            DataSet dtGasStatus = sysCodeBll.GetList("CODE_TYPE='GasStatus' ORDER BY DISPLAY_ORDER");

            this.ddlGasStatus.DataSource = dtGasStatus.Tables[0];
            this.ddlGasStatus.DataTextField = "CODE_CHI_DESC";
            this.ddlGasStatus.DataValueField = "CODE";
            this.ddlGasStatus.DataBind();


            DataSet dtGPStatus = sysCodeBll.GetList("CODE_TYPE='GPStatus' ORDER BY DISPLAY_ORDER");

            this.ddlCylinderStatus.DataSource = dtGPStatus.Tables[0];
            this.ddlCylinderStatus.DataTextField = "CODE_CHI_DESC";
            this.ddlCylinderStatus.DataValueField = "CODE";
            this.ddlCylinderStatus.DataBind();

            this.ddlBusinessCode.DataSource = new EGMNGS.BLL.RegGasBatch().GetList(@"Status='1' and IsPass='1' AND AmountGas>(SELECT ISNULL(SUM(b.AmountGas),0) FROM dbo.BookGasFill b
				                                                                                                WHERE b.GasType='4' AND b.BusinessCode=BatchCode)").Tables[0];
            this.ddlBusinessCode.DataTextField = "BatchCode";
            this.ddlBusinessCode.DataValueField = "BatchCode";
            this.ddlBusinessCode.DataBind();

            this.ddlBusinessCode.Items.Insert(0, string.Empty);

        }


        /// <summary>
        /// 绑定后激发事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void rp_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Label lblUserID = e.Item.FindControl("lblUserID") as Label;

                if (lblUserID != null)
                {
                    lblUserID.Text = GetUserName(lblUserID.Text);
                }

                Label lblStatus = e.Item.FindControl("lblStatus") as Label;

                if (lblStatus != null)
                {
                    if (lblStatus.Text.Length > 0)
                    {
                        lblStatus.Text = this.ddlStatus.Items.FindByValue(lblStatus.Text).Text;

                    }
                }

                Label lblGasStatus = e.Item.FindControl("lblGasStatus") as Label;

                if (lblGasStatus != null)
                {
                    if (lblGasStatus.Text.Length > 0)
                    {
                        lblGasStatus.Text = this.ddlGasStatus.Items.FindByValue(lblGasStatus.Text).Text;

                    }
                }

                Label lblCylinderStatus = e.Item.FindControl("lblCylinderStatus") as Label;

                if (lblCylinderStatus != null)
                {
                    if (lblCylinderStatus.Text.Length > 0)
                    {
                        lblCylinderStatus.Text = this.ddlCylinderStatus.Items.FindByValue(lblCylinderStatus.Text).Text;

                    }
                }
            }
        }

        /// <summary>
        /// 绑定数据，分页
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void pager_PageChanged(object sender, EventArgs e)
        {
            DataBindGrid();
        }

        /// <summary>
        /// 绑定数据源
        /// </summary>
        private void DataBindGrid()
        {
            int count = bll.GetRecordCount("GasType='4'");
            DataSet ds = bll.GetListByPage("GasType='4'", string.Empty, PageControl1.PageIndex, PageControl1.PageSize);
            ControlBindHelper.BindRepeaterList(ds.Tables[0], rp_Item);
            this.PageControl1.RecordCount = Convert.ToInt32(count);
        }

        /// <summary>
        /// 绑定数据到对象
        /// </summary>
        private void DataBinder()
        {
            Model.GasCode = this.txtGasCode.Text;
            Model.Year = this.ddlYear.SelectedValue;
            Model.Month = this.ddlMonth.SelectedValue;
            if (this.txtAmountGas.Text.Trim().Length > 0)
            {
                Model.AmountGas = decimal.Parse(this.txtAmountGas.Text);
            }
            Model.BusinessCode = this.ddlBusinessCode.SelectedValue;
            Model.CylinderCode = this.txtCylinderCode.Text;
            Model.FillGasCode = this.txtFillGasCode.Text;
            Model.GasType = this.ddlGasType.SelectedValue;

        }

        /// <summary>
        /// 初始化控件
        /// </summary>
        private void InitControl()
        {
            this.txtGasCode.Text = Model.GasCode;
            this.ddlMonth.SelectedValue = Model.Month;
            this.ddlYear.SelectedValue = Model.Year;
            this.txtAmountGas.Text = Model.AmountGas.ToString();

            var vl = this.ddlBusinessCode.Items.FindByValue(Model.BusinessCode);


            if (actionMode == "Edit")
            {

                if (vl == null && Model.BusinessCode != null)
                {
                    this.ddlBusinessCode.Items.Clear();
                    this.ddlBusinessCode.Items.Add(Model.BusinessCode);
                }
                else
                {
                    this.ddlBusinessCode.SelectedValue = Model.BusinessCode;
                }

            }


            this.txtCylinderCode.Text = Model.CylinderCode;
            this.ddlCylinderStatus.SelectedValue = Model.CylinderStatus;
            this.txtFillGasCode.Text = Model.FillGasCode;
            this.ddlGasStatus.Text = Model.GasStatus;
            this.ddlGasType.SelectedValue = Model.GasType;
            this.txtRegistrantOID.Text = GetUserName(Model.RegistrantOID);
            this.txtRegistrantDate.Text = Model.RegistrantDate == null ? string.Empty : Model.RegistrantDate.Value.ToShortDateString();
            this.ddlStatus.SelectedValue = Model.Status;
        }

        /// <summary>
        /// 获取用户名
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        private string GetUserName(string userId)
        {
            DataRow[] dr = UserList.Select(string.Format("User_ID='{0}'", userId));
            if (dr.Length > 0)
            {
                return dr[0][1].ToString();
            }
            return string.Empty;
        }

        private void RefreshMode()
        {
            Model = bll.GetModel(Model.OID);
        }
        #endregion

        #region 按钮事件
        /// <summary>
        /// 点击提交；钢瓶状态由“已抽空清洗”改为“已充装待检气”，并同步更新钢瓶台的“气体编码”和“钢瓶状态”字段。
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            if (Model.OID == 0)
            {
                return;
            }

            if (Model.Status == "1")
            {
                ShowMsgHelper.Alert_Wern("无法重新提交！");
                return;
            }

            if (ComServies.UpdateWithStatusJHBookGasFillPage(Model.CylinderCode, Model.GasCode))
            {
                ShowMsgHelper.Alert("提交成功！");
                RefreshMode();
                InitControl();
                DataBindGrid();
            }
            else
            {
                ShowMsgHelper.Alert("提交失败！");
            }

        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            DataBindGrid();
        }

        private void ShowMaxMode()
        {
            Model = bll.GetModel(bll.GetMaxId() - 1);
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                if (actionMode == "Add")
                {
                    DataBinder();

                    int count = bll.GetRecordCount(string.Format("FillGasCode='{0}'", Model.FillGasCode));
                    if (count > 0)
                    {
                        ShowMsgHelper.showWarningMsg("充气编号已经存在!");
                        return;
                    }

                    EGMNGS.Model.BookCylinderInfo BookCylinderInfoObj = new EGMNGS.BLL.BookCylinderInfo().GetModelByCylinderCode(Model.CylinderCode);
                    if (BookCylinderInfoObj == null || BookCylinderInfoObj.Status != "1")
                    {
                        ShowMsgHelper.showWarningMsg("请输入“已抽空清洗”的钢瓶编码!");
                        return;
                    }
                    ;
                    if (Convert.ToDecimal(DictEGMNS["GPRL"][BookCylinderInfoObj.CylinderCapacity]) < Model.AmountGas)
                    {
                        ShowMsgHelper.showWarningMsg("充装气量不能大于钢瓶的容量!");
                        return;
                    }

                    DataSet ds = ComServies.GetDiffWithBatchAndFill(Model.BusinessCode);

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        int diffNum = Convert.ToInt32(ds.Tables[0].Rows[0][0]);
                        if (diffNum < Model.AmountGas)
                        {
                            ShowMsgHelper.showWarningMsg("气量已经超出!请输入小于" + diffNum.ToString() + "kg");
                            return;
                        }

                    }
                    if (bll.Add(Model) > 0)
                    {
                        actionMode = string.Empty;
                    }

                    DataBindGrid();
                    ShowMaxMode();
                }
                else if (actionMode == "Edit")
                {
                    if (bll.GetModel(Model.OID).Status != "0")
                    {
                        ShowMsgHelper.Alert_Wern("“已确认”状态无法修改数据！");
                        return;
                    }

                    DataBinder();
                    if (bll.Update(Model))
                    {
                        EGMNSShowMsg.ShowEditMsgSuccess();
                        actionMode = string.Empty;
                    }
                    DataBindGrid();
                }
            }
            catch (ArgumentOutOfRangeException ex)
            {
                string[] errs = CommonHelper.GetValue(ex.Message, "@").Split(':');
                ShowMsgHelper.ExecuteScript(string.Format("ChangeCss($('#txt{0}'),'长度不能超{1}字')", errs[0], errs[1]));
            }
        }
        private void ClearControl()
        {
            Model = new EGMNGS.Model.BookGasFill();
            InitControl();
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            actionMode = "Add";
            Model = new EGMNGS.Model.BookGasFill(actionMode);
            Model.RegistrantDate = DateTime.Now;
            Model.RegistrantOID = User.UserId.ToString();
            Model.Status = "0";
            Model.GasType = "4";
            Model.GasStatus = "0";
            DropDownListBinder();
            InitControl();
            this.txtCylinderCode.Focus();
        }


        protected void btnDel_Click(object sender, EventArgs e)
        {
            if (bll.GetModel(Model.OID).Status != "0")
            {
                ShowMsgHelper.Alert_Wern("“已确认”状态不可以删除");
            }
            else
            {
                bll.Delete(Model.OID);
                Model = new EGMNGS.Model.BookGasFill();
                InitControl();
                DataBindGrid();
                ShowMsgHelper.Alert("删除成功！");
            }
        }

        protected void rp_Item_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            int oid = Convert.ToInt32(e.CommandArgument);

            Model = bll.GetModel(oid);

            actionMode = "Edit";
            InitControl();
        }
        #endregion

        protected void txtCylinderCode_TextChanged(object sender, EventArgs e)
        {
            var obj = new EGMNGS.BLL.BookCylinderInfo().GetModelByCylinderCode(this.txtCylinderCode.Text);

            this.txtAmountGas.Text = DictEGMNS["GPRL"][obj.CylinderCapacity];
        }
    }
}
﻿using RM.Web.App_Code;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using RM.Common.DotNetUI;
using RM.Common.DotNetFile;
using EGMNGS.Model;
using DapperData;
using EGMNGS.Common;
using System.Data;

namespace RM.Web.GanPing
{
    public partial class BugDataPage : PageBase
    {
        #region 属性变量
        public string PowerSupplyCode
        {
            get { return ViewState["PowerSupplyCode"] as String; }
            set { ViewState["PowerSupplyCode"] = value; }
        }
        public string PowerSupplyName
        {
            get { return ViewState["PowerSupplyName"] as String; }
            set { ViewState["PowerSupplyName"] = value; }
        }

        public BugData BugDataObj
        {
            get
            {
                if (ViewState["BugData"] == null)
                {
                    ViewState["BugData"] = new BugData();
                }

                return ViewState["BugData"] as BugData;
            }
            set { ViewState["BugData"] = value; }
        }



        private string actionModeDetails
        {
            get
            {
                if (ViewState["actionModeDetails"] == null)
                {
                    return string.Empty;
                }
                return ViewState["actionModeDetails"] as String;
            }
            set { ViewState["actionModeDetails"] = value; }
        }


        Repository _db = new Repository();
        EGMNGS.BLL.InfoDevPara bllInfoDevPara = new EGMNGS.BLL.InfoDevPara();
        #endregion
        #region 事件
        protected void Page_Load(object sender, EventArgs e)
        {
            this.PageControl1.pageHandler += new EventHandler(pager_PageChanged);

            if (!IsPostBack)
            {


                string[] orgIdOrgName = ComServies.GetPowerApplyByUserID(User.UserId.ToString());
                PowerSupplyCode = orgIdOrgName[0];
                PowerSupplyName = orgIdOrgName[1];


                DropDownListBinder();



            }

        }

        protected void rp_Item_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            string oid = Convert.ToString(e.CommandArgument);
            BugDataObj = _db.GetById<BugData>(Convert.ToInt32(oid));


            ActionMode = eActionMode.Update;
            ShowMsgHelper.ShowScriptAjax(string.Format("LoadFileList('{0}')", oid));
        }

        protected override void OnLoad(EventArgs e)
        {
            CheckURLPermission = true;
            base.OnLoad(e);
        }

        /// <summary>
        /// 绑定数据，分页
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void pager_PageChanged(object sender, EventArgs e)
        {
            DataBindGrid();
        }
        #endregion

        #region 主表事件

        protected void btnSave_Click(object sender, EventArgs e)
        {

            Save();

        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            ActionMode = eActionMode.Add;
            BugDataObj = new BugData();
            BugDataObj.CREATED_DATE = DateTime.Now;
            BugDataObj.CREATED_BY = User.UserName.ToString();


        }

        protected void btnDel_Click(object sender, EventArgs e)
        {
            ActionMode = eActionMode.Delete;
            Save();
        }

        #endregion

        #region 私有方法
        //todo 绑定下拉
        private void DropDownListBinder()
        {
            System.Data.DataTable dtPowerStation = ComServies.GetAllPowerStation();
            this.ddlsPowerSupplyCode.DataSource = dtPowerStation;
            this.ddlsPowerSupplyCode.DataTextField = "Organization_Name";
            this.ddlsPowerSupplyCode.DataValueField = "Organization_ID";
            this.ddlsPowerSupplyCode.DataBind();
            //  this.ddlsPowerSupplyCode.Items.Insert(0, string.Empty);
            this.ddlsPowerSupplyCode.SelectedValue = PowerSupplyCode;

            LoadPowerSupplyName();
        }

        #endregion

        #region 基类方法实现


        //todo add
        public override void Add()
        {

            BugDataObj.ConvertStationName = ddlsConvertStationCode.SelectedItem.Text;
            BugDataObj.PowerSupplyName = ddlsPowerSupplyCode.SelectedItem.Text;
            BugDataObj.PowerSupplyCode = ddlsPowerSupplyCode.SelectedValue;
            _db.Insert<BugData>(BugDataObj);
            ActionMode = eActionMode.NotExecute;
            base.Add();

        }
        //todo del
        public override void Delete()
        {

            _db.Delete<BugData>(BugDataObj.OID);
            ActionMode = eActionMode.NotExecute;
            base.Delete();

        }
        //todo Update
        public override void Update()
        {
            BugDataObj.ConvertStationName = ddlsConvertStationCode.SelectedItem.Text;
            BugDataObj.PowerSupplyName = ddlsPowerSupplyCode.SelectedItem.Text;
            BugDataObj.LAST_UPD_DATE = DateTime.Now;
            BugDataObj.LAST_UPD_BY = User.UserName.ToString();
            _db.Update<BugData>(BugDataObj);
            base.Update();
        }

        /// <summary>
        /// 绑定数据源
        /// </summary>
        public override void DataBindGrid()
        {
            string sqlStrwhere = "1=1";

            GenFindWhere(ref sqlStrwhere);


            if (PowerSupplyName.Contains("供电局"))
            {
                if (sqlStrwhere.Length > 0)
                {
                    sqlStrwhere += string.Format(" and PowerSupplyCode='{0}'", PowerSupplyCode);
                }
                else
                {
                    sqlStrwhere = string.Format("PowerSupplyCode='{0}'", PowerSupplyCode);
                }

            }

            Pager p = new Pager();
            p.TableName = "BugData";
            p.FieldName = "*";
            p.strOrderFld = "OID desc";
            p.PageIndex = 1;
            p.PageSize = 10;
            p.StrWhere = sqlStrwhere;
            var listBugData = _db.GetPaged<BugData>(SessionFactory.CreateConnection(), p);
            this.PageControl1.RecordCount = p.TotalRowsCount;
            ControlBindHelper.BindRepeaterList(listBugData, rp_Item);
        }
        #endregion

        protected void ddlPowerSupplyName_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadPowerSupplyName();

        }
        private void LoadPowerSupplyName()
        {
            this.ddlsConvertStationCode.Items.Clear();
            EGMNGS.BLL.InfoPowerSupplyConvertStation bll = new EGMNGS.BLL.InfoPowerSupplyConvertStation();
            System.Data.DataSet dsPowerSupplyCode = bll.GetList(string.Format("PowerSupplyCode='{0}'", this.ddlsPowerSupplyCode.SelectedValue));
            this.ddlsConvertStationCode.DataSource = dsPowerSupplyCode.Tables[0];
            this.ddlsConvertStationCode.DataTextField = "ConvartStationName";
            this.ddlsConvertStationCode.DataValueField = "CovnertStationCode";
            this.ddlsConvertStationCode.DataBind();
            this.ddlsConvertStationCode.Items.Insert(0, string.Empty);

        }

        /// <summary>
        /// 筛选
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lbtSearch_Click(object sender, EventArgs e)
        {
            ActionMode = eActionMode.Search;
            DataBindGrid();
        }
        //protected void ddlConvertStationName_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    BindddlDevCode();

        //}

        //private void BindddlDevCode()
        //{
        //   // DataSet ds = bllInfoDevPara.GetList(string.Format("ConvertStationCode='{0}'", this.ddlsConvertStationCode.SelectedValue));
        //    //this.ddlsDevCode.Items.Clear();
        //    //this.ddlsDevCode.DataSource = ds.Tables[0];
        //    //this.ddlsDevCode.DataTextField = "DevCode";
        //    //this.ddlsDevCode.DataValueField = "DevCode";
        //    //this.ddlsDevCode.DataBind();
        //    //this.ddlsDevCode.Items.Insert(0, string.Empty);
        //}
        protected void ddlDevCode_SelectedIndexChanged(object sender, EventArgs e)
        {
            //string oid = this.ddlsDevCode.SelectedValue;
            //if (oid.Length == 0)
            //{
            //    return;
            //}
            //List<InfoDevPara> infoDevP = bllInfoDevPara.GetModelList(string.Format("DevCode='{0}'", oid));
            //if (infoDevP.Count>0)
            //{
            //    BugDataObj.DevName = infoDevP[0].DevName;
            //}

        }
    }
}
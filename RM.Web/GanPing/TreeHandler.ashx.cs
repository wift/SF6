﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;

namespace RM.Web.GanPing
{
    public class NodeItem
    {

        public string text { set; get; }
        public string id { get; set; }
        public string state { get { return "closed"; } }
        public List<NodeItem> children { set; get; }
    }
    /// <summary>
    /// TreeHandler 的摘要说明
    /// </summary>
    public class TreeHandler : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "application/json";
            string parentNode = context.Request["ParentNode"] ?? "";
            string returnVal = GetNode(parentNode);
            context.Response.Write(returnVal);
        }

        private string GetNode(string parentNode)
        {
            List<NodeItem> list = new List<NodeItem>();
            List<NodeItem> tempList = new List<NodeItem>();

            if (string.IsNullOrWhiteSpace(parentNode))//根节点
            {
                list.Add(new NodeItem { text = "广东电网公司", id = "广东电网公司/", children = tempList });
                return Newtonsoft.Json.JsonConvert.SerializeObject(list);
            }

            if (!parentNode.EndsWith("/"))//已经到设备
            {
                return string.Empty;
            }

            string wheresql = "";

            wheresql = string.Format(" where OutFullPath like  '{0}%'", parentNode);


            StringBuilder treeItem_Table = new StringBuilder();

            DataSet dtroot = Maticsoft.DBUtility.DbHelperSQL.Query(string.Format(@" select
 DISTINCT
case when LEFT(OutFullPath,CHARINDEX('/',OutFullPath ,len('{1}')+1)) != ''
then LEFT(OutFullPath,CHARINDEX('/',OutFullPath ,len('{1}')+1))
else OutFullPath
end

as text
from [EGMNGS].[dbo].[InfoDevPara] {0}", wheresql, parentNode));

            for (int i = 0; i < dtroot.Tables[0].Rows.Count; i++)
            {
                DataRow drv = dtroot.Tables[0].Rows[i];
                string text = drv["text"].AsTargetType<string>("");
                string showtext = string.Empty;//显示处理

                showtext = text.Replace(parentNode, "").TrimEnd('/');


                if (!text.EndsWith("/"))
                {
                    ArrayList al = new ArrayList();
                    al.Add(new { text = showtext, id = text });
                    return Newtonsoft.Json.JsonConvert.SerializeObject(al);
                }
                else
                {
                    list.Add(new NodeItem { text = showtext, id = text, children = tempList });
                }
            }

            return Newtonsoft.Json.JsonConvert.SerializeObject(list);

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;
using RM.Web.App_Code;

namespace RM.Web.GanPing
{
    public partial class SearchGPCount : PageBase
    {
        public EGMNGS.Model.BookCylinderInfo Model
        {
            get
            {
                if (ViewState["BookCylinderInfo"] == null)
                {
                    return new EGMNGS.Model.BookCylinderInfo();
                }
                return ViewState["BookCylinderInfo"] as EGMNGS.Model.BookCylinderInfo;
            }
            set { ViewState["BookCylinderInfo"] = value; }
        }
        private string sqlStr
        {
            get
            {
                if (ViewState["sqlStr"] == null)
                {
                    ViewState["sqlStr"] = "1=0";
                }
                return ViewState["sqlStr"] as string;
            }
            set { ViewState["sqlStr"] = value; }
        }

        public Dictionary<string, string> GPStatus
        {
            get
            {
                if (ViewState["GPStatus"] == null)
                {
                    Dictionary<string, string> GPStatus = new Dictionary<string, string>();
                    EGMNGS.BLL.SYS_CODE sysCodeBll = new EGMNGS.BLL.SYS_CODE();
                    DataSet dsGPStatus = sysCodeBll.GetList("CODE_TYPE='GPStatus' ORDER BY DISPLAY_ORDER");
                    foreach (DataRow item in dsGPStatus.Tables[0].Rows)
                    {
                        GPStatus.Add(item["CODE"].ToString(), item["CODE_CHI_DESC"].ToString());
                    }
                    GPStatus.Add(string.Empty, string.Empty);
                    ViewState["GPStatus"] = GPStatus;
                }

                return ViewState["GPStatus"] as Dictionary<string, string>;
            }
            set { ViewState["GPStatus"] = value; }
        }

        public Dictionary<string, string> DictCylinderCapacity
        {
            get
            {
                if (ViewState["CylinderCapacity"] == null)
                {
                    Dictionary<string, string> CylinderCapacity = new Dictionary<string, string>();
                    EGMNGS.BLL.SYS_CODE sysCodeBll = new EGMNGS.BLL.SYS_CODE();
                    DataSet dsCylinderCapacity = sysCodeBll.GetList("CODE_TYPE='GPRL' ORDER BY DISPLAY_ORDER");
                    foreach (DataRow item in dsCylinderCapacity.Tables[0].Rows)
                    {
                        CylinderCapacity.Add(item["CODE"].ToString(), item["CODE_CHI_DESC"].ToString());
                    }
                    CylinderCapacity.Add(string.Empty, string.Empty);
                    ViewState["CylinderCapacity"] = CylinderCapacity;
                }

                return ViewState["CylinderCapacity"] as Dictionary<string, string>;
            }
            set { ViewState["CylinderCapacity"] = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            StringBuilder sb = new StringBuilder();

            if (this.txtCylinderCode.Text.Trim().Length > 0)
            {
                sb.AppendFormat("and CylinderCode='{0}' ", this.txtCylinderCode.Text.Trim());
            }
            if (this.txtCylinderSealNo.Text.Trim().Length > 0)
            {
                sb.AppendFormat("and CylinderSealNo='{0}' ", this.txtCylinderSealNo.Text.Trim());
            }


            sqlStr = sb.ToString().TrimStart(new char[3] { 'a', 'n', 'd' });

            if (sqlStr.Length == 0)
            {
                sqlStr = "1=0";
            }
            var listModel = new EGMNGS.BLL.BookCylinderInfo().GetModelList(sqlStr);
            if (listModel.Count > 0)
            {
                Model = listModel[0];
                Session["CylinderCode"] = Model.CylinderCode;
            }



        }

        public string Manufacturer
        {
            get
            {
                if (Model.Manufacturer == "1" || Model.Manufacturer == "2")
                {
                    return DictEGMNS["BelongUnit"][Model.Manufacturer];
                }

                return "";
            }
            
        }
        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSearch_Click(object sender, EventArgs e)
        {


        }


    }
}
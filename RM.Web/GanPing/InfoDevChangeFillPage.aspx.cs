﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using RM.Common.DotNetCode;
using System.Data;
using RM.Common.DotNetUI;
using RM.Web.App_Code;
using EGMNGS.Common;
using EGMNGS.Model;

namespace RM.Web.GanPing
{
    public partial class InfoDevChangeFillPage : PageBase
    {
        #region 属性字段

        EGMNGS.BLL.InfoDevChangeFill bll = new EGMNGS.BLL.InfoDevChangeFill();
        private DataTable UserList
        {
            get
            {
                return ViewState["UserList"] as DataTable;
            }
            set { ViewState["UserList"] = value; }
        }
        private EGMNGS.Model.InfoDevChangeFill Model
        {
            get
            {
                if (ViewState["InfoDevChangeFill"] == null)
                {
                    return new EGMNGS.Model.InfoDevChangeFill();
                }
                return ViewState["InfoDevChangeFill"] as EGMNGS.Model.InfoDevChangeFill;
            }
            set { ViewState["InfoDevChangeFill"] = value; }
        }
        private string actionMode
        {
            get { return ViewState["ActionMode"] as String; }
            set { ViewState["ActionMode"] = value; }
        }
        public string PowerSupplyCode
        {
            get { return ViewState["PowerSupplyCode"] as String; }
            set { ViewState["PowerSupplyCode"] = value; }
        }
        public string PowerSupplyName
        {
            get { return ViewState["PowerSupplyName"] as String; }
            set { ViewState["PowerSupplyName"] = value; }
        }

        #endregion

        #region 事件方法

        protected void Page_Load(object sender, EventArgs e)
        {
            this.PageControl1.pageHandler += new EventHandler(pager_PageChanged);
            if (!IsPostBack)
            {
                UserList = ComServies.GetAllUserInfo();
                string[] orgIdOrgName = ComServies.GetPowerApplyByUserID(User.UserId.ToString());
                PowerSupplyCode = orgIdOrgName[0];
                PowerSupplyName = orgIdOrgName[1];
                DropDownListBinder();
            }
        }

        /// <summary>
        /// 绑定下拉列
        /// </summary>
        private void DropDownListBinder()
        {
            EGMNGS.BLL.InfoPowerSupplyConvertStation bll = new EGMNGS.BLL.InfoPowerSupplyConvertStation();
            DataSet dsPowerSupplyCode = bll.GetList(string.Format("PowerSupplyCode='{0}'", PowerSupplyCode));
            this.ddlConvertStationName.DataSource = dsPowerSupplyCode.Tables[0];
            this.ddlConvertStationName.DataTextField = "ConvartStationName";
            this.ddlConvertStationName.DataValueField = "CovnertStationCode";
            this.ddlConvertStationName.DataBind();
            this.ddlConvertStationName.Items.Insert(0, string.Empty);
            //EGMNGS.BLL.SYS_CODE sysCodeBll = new EGMNGS.BLL.SYS_CODE();
            //DataSet dsToolType = sysCodeBll.GetList("CODE_TYPE='ToolType' ORDER BY DISPLAY_ORDER");

            //this.ddlToolType.DataSource = dsToolType.Tables[0];
            //this.ddlToolType.DataTextField = "CODE_CHI_DESC";
            //this.ddlToolType.DataValueField = "CODE";
            //this.ddlToolType.DataBind();

            //DataSet dsMonth = sysCodeBll.GetList("CODE_TYPE='Month' ORDER BY DISPLAY_ORDER");

            //this.ddlMonth.DataSource = dsMonth.Tables[0];
            //this.ddlMonth.DataTextField = "CODE_CHI_DESC";
            //this.ddlMonth.DataValueField = "CODE";
            //this.ddlMonth.DataBind();
            EGMNGS.BLL.SYS_CODE sysCodeBll = new EGMNGS.BLL.SYS_CODE();
            DataSet dtCFStatus = sysCodeBll.GetList("CODE_TYPE='CFStatus' ORDER BY DISPLAY_ORDER");

            this.ddlStatus.DataSource = dtCFStatus.Tables[0];
            this.ddlStatus.DataTextField = "CODE_CHI_DESC";
            this.ddlStatus.DataValueField = "CODE";
            this.ddlStatus.DataBind();

            this.ddlStatus.Items.Insert(0, string.Empty);

        }


        /// <summary>
        /// 绑定后激发事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void rp_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Label lblUserID = e.Item.FindControl("lblRegistantsOID") as Label;
                if (lblUserID != null)
                {
                    lblUserID.Text = GetUserName(lblUserID.Text);
                }

            }
        }

        /// <summary>
        /// 绑定数据，分页
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void pager_PageChanged(object sender, EventArgs e)
        {
            DataBindGrid();
        }

        /// <summary>
        /// 绑定数据源
        /// </summary>
        private void DataBindGrid()
        {
            string sqlwhere = "1=1";
            int count = bll.GetRecordCount(sqlwhere);

            if (!string.IsNullOrEmpty(PowerSupplyCode))
            {
                sqlwhere += string.Format(" and PowerSupplyCode='{0}'", PowerSupplyCode);
            }

            DataSet ds = bll.GetListByPage(sqlwhere, string.Empty, PageControl1.PageIndex, PageControl1.PageSize);
            ControlBindHelper.BindRepeaterList(ds.Tables[0], rp_Item);
            this.PageControl1.RecordCount = Convert.ToInt32(count);
        }
        /// <summary>
        /// 绑定数据到对象
        /// </summary>
        private void DataBinder()
        {
            Model.DevCode = this.txtDevCode.Text;
            Model.DevName = this.txtDevName.Text;
            Model.DevType = this.txtDevType.Text;
            if (this.txtRatedQL.Text.Trim().Length > 0)
            {
                Model.RatedQL = decimal.Parse(this.txtRatedQL.Text);
            }
            if (this.txtRatedQY.Text.Trim().Length > 0)
            {
                Model.RatedQY = decimal.Parse(this.txtRatedQY.Text);
            }
            if (this.txtFillGasdate.Text.Trim().Length > 0)
            {
                Model.FillGasdate = DateTime.Parse(this.txtFillGasdate.Text);
            }
            Model.Operataff = this.txtOperataff.Text;
            Model.GasUsing = this.txtGasUsing.Text;
            if (this.txtAmountFillGas.Text.Trim().Length > 0)
            {
                Model.AmountFillGas = Decimal.Parse(this.txtAmountFillGas.Text);
            }
            if (this.txtAmountPressFillGassbefore.Text.Trim().Length > 0)
            {
                Model.AmountPressFillGassbefore = decimal.Parse(this.txtAmountPressFillGassbefore.Text);
            }
            if (this.txtAmountPressFillGassafter.Text.Trim().Length > 0)
            {
                Model.AmountPressFillGassafter = decimal.Parse(this.txtAmountPressFillGassafter.Text);
            }

            Model.RemarksDesc = this.txtRemarksDesc.Text;
            Model.CylinderCode = this.txtCylinderCode.Text;
            Model.GasCode = this.txtGasCode.Text;

            Model.ConvertStationCode = this.ddlConvertStationName.SelectedValue;
            Model.ConvertStationName = this.ddlConvertStationName.SelectedItem.Text;
        }

        /// <summary>
        /// 初始化控件
        /// </summary>
        private void InitControl()
        {
            this.txtPowerSupplyName.Text = Model.PowerSupplyName;
           // 

            if (this.ddlConvertStationName.Items.FindByValue(Model.ConvertStationCode) == null)
            {
                this.ddlConvertStationName.Items.Clear();
                this.ddlConvertStationName.Items.Add(new ListItem { Text = Model.ConvertStationName, Value = Model.ConvertStationCode, Selected = true });
            }
            else
            {
                this.ddlConvertStationName.SelectedValue = Model.ConvertStationCode;
            }

            //if (!string.IsNullOrEmpty(Model.DevCode))
            //{
            //    this.ddlDevCode.Items.Clear();
            //    this.ddlDevCode.Items.Add(new ListItem(Model.DevCode, Model.DevCode));
            //}
            this.txtDevCode.Text=Model.DevCode;

            this.txtDevName.Text = Model.DevName;
            this.txtDevType.Text = Model.DevType;
            this.txtRatedQL.Text = Model.RatedQL.ToString();
            this.txtRatedQY.Text = Model.RatedQY.ToString();

            if (Model.FillGasdate != null)
            {
                this.txtFillGasdate.Text = Model.FillGasdate.Value.ToShortDateString();
            }
            this.txtOperataff.Text = Model.Operataff;
            this.txtGasUsing.Text = Model.GasUsing;
            this.txtAmountFillGas.Text = Model.AmountFillGas.ToString();
            this.txtAmountPressFillGassbefore.Text = Model.AmountPressFillGassbefore.ToString();
            this.txtAmountPressFillGassafter.Text = Model.AmountPressFillGassafter.ToString();
            this.txtRemarksDesc.Text = Model.RemarksDesc;
            this.txtCylinderCode.Text = Model.CylinderCode;
            this.txtGasCode.Text = Model.GasCode;
            this.ddlStatus.SelectedValue = Model.Status;
            this.txtRegistantsOID.Text = GetUserName(Model.RegistantsOID);
            if (Model.RegistantsDate != null)
            {
                this.txtRegistantsDate.Text = Model.RegistantsDate.Value.ToShortDateString();
            }
        }

        /// <summary>
        /// 获取用户名
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        private string GetUserName(string userId)
        {
            DataRow[] dr = UserList.Select(string.Format("User_ID='{0}'", userId));
            if (dr.Length > 0)
            {
                return dr[0][1].ToString();
            }
            return string.Empty;
        }
        #endregion

        #region 按钮事件
        /// <summary>
        /// 确认
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnConfirm_Click(object sender, EventArgs e)
        {
            if (Model.Status == "1")
            {
                Model.Status = "2";
                bll.Update(Model);
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            Model.Status = "1";
            if (bll.Update(Model))
            {
                ShowMsgHelper.Alert("提交成功！");
                InitControl();
                DataBindGrid();
            }

        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            DataBindGrid();
        }

        private void ShowMaxMode()
        {
            Model = bll.GetModel(bll.GetMaxId() - 1);
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (actionMode == "Add")
                {
                    DataBinder();

                    if (bll.Add(Model) > 0)
                    {
                        actionMode = string.Empty;
                        ActionMode = eActionMode.NotExecute;
                        ShowMsgHelper.Alert("添加成功");
                    }

                    DataBindGrid();
                    ShowMaxMode();

                   
                }
                else if (actionMode == "Edit")
                {
                    if (bll.GetModel(Model.OID).Status != "0")
                    {
                        ShowMsgHelper.Alert_Wern("“已确认”状态无法修改数据！");
                        return;
                    }

                    DataBinder();
                    if (bll.Update(Model))
                    {
                        EGMNSShowMsg.ShowEditMsgSuccess();
                        actionMode = string.Empty;
                    }
                    DataBindGrid();
                }
            }
            catch (ArgumentOutOfRangeException ex)
            {
                string[] errs = CommonHelper.GetValue(ex.Message, "@").Split(':');
                ShowMsgHelper.ExecuteScript(string.Format("ChangeCss($('#txt{0}'),'长度不能超{1}字')", errs[0], errs[1]));
            }
        }
        private void ClearControl()
        {
            Model = new EGMNGS.Model.InfoDevChangeFill();
            InitControl();
        }
        protected void btnAdd_Click(object sender, EventArgs e)
        {
            actionMode = "Add";
            Model = new EGMNGS.Model.InfoDevChangeFill();
            Model.PowerSupplyCode = PowerSupplyCode;
            Model.PowerSupplyName = PowerSupplyName;
            Model.RegistantsDate = DateTime.Now;
            Model.RegistantsOID = User.UserId.ToString();
            Model.Status = "0";
            InitControl();
            LoadPowerSupplyName();
        }


        protected void btnDel_Click(object sender, EventArgs e)
        {
            if (bll.GetModel(Model.OID).Status == "1")
            {
                ShowMsgHelper.Alert_Wern("“已确认”状态不可以删除");
            }
            else
            {
                bll.Delete(Model.OID);
                Model = new EGMNGS.Model.InfoDevChangeFill();
                InitControl();
                DataBindGrid();
                ShowMsgHelper.Alert("删除成功！");

            }
        }

        protected void rp_Item_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            int oid = Convert.ToInt32(e.CommandArgument);

            Model = bll.GetModel(oid);
            InitControl();
            actionMode = "Edit";
        }
        EGMNGS.BLL.InfoDevPara bllInfoDevPara = new EGMNGS.BLL.InfoDevPara();
        protected void ddlConvertStationName_SelectedIndexChanged(object sender, EventArgs e)
        {

      
        }

        private void LoadPowerSupplyName()
        {
            this.ddlConvertStationName.Items.Clear();
            EGMNGS.BLL.InfoPowerSupplyConvertStation bll = new EGMNGS.BLL.InfoPowerSupplyConvertStation();
            DataSet dsPowerSupplyCode = bll.GetList(string.Format("PowerSupplyCode='{0}'", PowerSupplyCode));
            this.ddlConvertStationName.DataSource = dsPowerSupplyCode.Tables[0];
            this.ddlConvertStationName.DataTextField = "ConvartStationName";
            this.ddlConvertStationName.DataValueField = "CovnertStationCode";
            this.ddlConvertStationName.DataBind();
            this.ddlConvertStationName.Items.Insert(0, string.Empty);
        }

        protected void ddlDevCode_SelectedIndexChanged(object sender, EventArgs e)
        {
            //string oid = this.txtDevCode.Text;
            //InfoDevPara infoDevP = bllInfoDevPara.GetModel(int.Parse(oid));
            //if (infoDevP == null)
            //{
            //    return;
            //}
            //this.txtDevName.Text = infoDevP.DevName;
            //this.txtDevType.Text = infoDevP.DevType;
            //this.txtRatedQL.Text = Convert.ToString(infoDevP.RatedQL);
            //this.txtRatedQY.Text = Convert.ToString(infoDevP.RatedQY);
        }


        #endregion
    }
}
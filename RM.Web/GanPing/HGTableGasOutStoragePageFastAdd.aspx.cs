﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using RM.Common.DotNetUI;
using System.Data;
using EGMNGS.Common;

namespace RM.Web.GanPing
{
    public partial class HGTableGasOutStoragePageFastAdd : System.Web.UI.Page
    {
        public DataTable DetailsTable
        {
            get
            {
                if (ViewState["DetailsTable"] == null)
                {
                    return new DataTable();
                }
                return ViewState["DetailsTable"] as DataTable;
            }
            set { ViewState["DetailsTable"] = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                DataBindGrid();

            }

        }

        /// <summary>
        /// 绑定数据源
        /// </summary>
        private void DataBindGrid()
        {
            // string code = Request["key"].ToString();
            DataSet ds = ComServies.GetCanOut();
            DetailsTable = ds.Tables[0];
            ControlBindHelper.BindRepeaterList(DetailsTable, rp_Item);
        }


        protected void rp_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {

                Label lblGasSourse = e.Item.FindControl("lblGasSourse") as Label;
                if (lblGasSourse != null && lblGasSourse.Text.Length > 0)
                {
                    if (lblGasSourse.Text == "1")
                    {
                        lblGasSourse.Text = "新采购气";
                    }
                    else if (lblGasSourse.Text == "2")
                    {

                        lblGasSourse.Text = "入网新气";
                    }
                    else if (lblGasSourse.Text == "3")
                    {

                        lblGasSourse.Text = "回收净化气";
                    }
                }


            }
        }

        protected void btnBatchAdd_Click(object sender, EventArgs e)
        {

            if (HiddenField1.Value.Length > 0)
            {
                string code = Request["key"].ToString();
                EGMNGS.BLL.DetailsGasOutStorage DetailsBLL = new EGMNGS.BLL.DetailsGasOutStorage();

                foreach (DataRow item in DetailsTable.Select(string.Format("OID in({0})", HiddenField1.Value)))
                {
                    if (DetailsBLL.GetRecordCount(string.Format(@"GasCode='{0}' and exists(select * from TableGasOutStorage t 
where TableGasOutStorage_Code=t.Code and t.GasClass='3') ", item["GasCode"].ToString())) >= 1)
                    {
                        continue;
                    }

                    DetailsBLL.Add(new EGMNGS.Model.DetailsGasOutStorage() { AmountGas = (decimal)item["AmountGas"], GasCode = item["GasCode"].ToString(), CylinderCode = item["CylinderCode"].ToString(), TableGasOutStorage_Code = code, IsPass = "1" });
                }


                ShowMsgHelper.AlertMsg("批量添加成功！");

            }
        }
    }
}
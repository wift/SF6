﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using RM.Common.DotNetUI;
using EGMNGS.Common;

namespace RM.Web.GanPing
{
    public partial class CylinderKCMng : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            DataBindGrid();
            this.txtAuditsDate.Text = DateTime.Now.ToShortDateString();
            if (!IsPostBack)
            {
                DropDownListBinder();
            }
        }
        private void DropDownListBinder()
        {

            EGMNGS.BLL.SYS_CODE sysCodeBll = new EGMNGS.BLL.SYS_CODE();

            DataSet dtddlManufacturer = sysCodeBll.GetList("CODE_TYPE='BelongUnit' ORDER BY DISPLAY_ORDER");

            this.ddlManufacturer.DataSource = dtddlManufacturer.Tables[0];
            this.ddlManufacturer.DataTextField = "CODE_CHI_DESC";
            this.ddlManufacturer.DataValueField = "CODE";
            this.ddlManufacturer.DataBind();
        }
        /// <summary>
        /// 绑定数据源
        /// </summary>
        private void DataBindGrid()
        {
            //string cylinderCode = Convert.ToString(Session["CylinderCode"]);
            //string sqlWhere = string.Format("CylinderCode='{0}'", cylinderCode);
            //DataSet ds = ComServies.GetRecordGPSJView(sqlWhere);// new EGMNGS.BLL.BookGasFill().GetList(sqlWhere);// ComServies.GetListByPage(sqlStr, string.Empty, PageControl1.PageIndex, PageControl1.PageSize);
            //ControlBindHelper.BindRepeaterList(ds.Tables[0], rp_Item);
            //this.PageControl1.RecordCount = Convert.ToInt32(ds.Tables[0].Rows.Count);
        }

        protected void btnFind_Click(object sender, EventArgs e)
        {
            // string cylinderCode = this.txtAuditsDate.Text;
            if (this.ddlManufacturer.SelectedIndex > -1)
            {
                string sqlWhere = string.Format("b.Manufacturer='{0}'", this.ddlManufacturer.SelectedValue);
                DataSet ds = ComServies.GeCylinderKCMng(sqlWhere);// new EGMNGS.BLL.BookGasFill().GetList(sqlWhere);// ComServies.GetListByPage(sqlStr, string.Empty, PageControl1.PageIndex, PageControl1.PageSize);
                ControlBindHelper.BindRepeaterList(ds.Tables[0], rp_Item);
            }


        }
    }
}
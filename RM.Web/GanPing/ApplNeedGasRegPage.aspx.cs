﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using RM.Common.DotNetCode;
using System.Data;
using RM.Common.DotNetUI;
using RM.Web.App_Code;
using EGMNGS.Common;
namespace RM.Web.GanPing
{
    public partial class ApplNeedGasRegPage : PageBase
    {
        #region 属性字段

        EGMNGS.BLL.ApplNeedGasReg bll = new EGMNGS.BLL.ApplNeedGasReg();
        private DataTable UserList
        {
            get
            {
                return ViewState["UserList"] as DataTable;
            }
            set { ViewState["UserList"] = value; }
        }
        private EGMNGS.Model.ApplNeedGasReg Model
        {
            get
            {
                if (ViewState["ApplNeedGasReg"] == null)
                {
                    return new EGMNGS.Model.ApplNeedGasReg();
                }
                return ViewState["ApplNeedGasReg"] as EGMNGS.Model.ApplNeedGasReg;
            }
            set { ViewState["ApplNeedGasReg"] = value; }
        }
        private string actionMode
        {
            get { return ViewState["ActionMode"] as String; }
            set { ViewState["ActionMode"] = value; }
        }
        public string PowerSupplyCode
        {
            get { return ViewState["PowerSupplyCode"] as String; }
            set { ViewState["PowerSupplyCode"] = value; }
        }
        public string PowerSupplyName
        {
            get { return ViewState["PowerSupplyName"] as String; }
            set { ViewState["PowerSupplyName"] = value; }
        }

        public List<string> listRole
        {
            get { return ViewState["listRole"] as List<string>; }
            set { ViewState["listRole"] = value; }
        }
        #endregion

        #region 事件方法

        protected void Page_Load(object sender, EventArgs e)
        {
            this.PageControl1.pageHandler += new EventHandler(pager_PageChanged);
            if (!IsPostBack)
            {
                UserList = ComServies.GetAllUserInfo();
                string[] orgIdOrgName = ComServies.GetPowerApplyByUserID(User.UserId.ToString());
                PowerSupplyCode = orgIdOrgName[0];
                PowerSupplyName = orgIdOrgName[1];
                DropDownListBinder();
                listRole = ComServies.GetRoleName(User.UserId.ToString());
            }


        }

        /// <summary>
        /// 绑定下拉列
        /// </summary>
        private void DropDownListBinder()
        {
            EGMNGS.BLL.InfoPowerSupplyConvertStation bll = new EGMNGS.BLL.InfoPowerSupplyConvertStation();
            DataSet dsPowerSupplyCode = bll.GetList(string.Format("PowerSupplyCode='{0}'", PowerSupplyCode));
            this.ddlConvertStationName.DataSource = dsPowerSupplyCode.Tables[0];
            this.ddlConvertStationName.DataTextField = "ConvartStationName";
            this.ddlConvertStationName.DataValueField = "CovnertStationCode";
            this.ddlConvertStationName.DataBind();
            //this.ddlConvertStationName.Items.Insert(0, string.Empty);

            EGMNGS.BLL.SYS_CODE sysCodeBll = new EGMNGS.BLL.SYS_CODE();
            DataSet dsYear = sysCodeBll.GetList("CODE_TYPE='Year' ORDER BY DISPLAY_ORDER");

            this.ddlYear.DataSource = dsYear.Tables[0];
            this.ddlYear.DataTextField = "CODE_CHI_DESC";
            this.ddlYear.DataValueField = "CODE";
            this.ddlYear.DataBind();

            DataSet dsMonth = sysCodeBll.GetList("CODE_TYPE='Month' ORDER BY DISPLAY_ORDER");

            this.ddlMonth.DataSource = dsMonth.Tables[0];
            this.ddlMonth.DataTextField = "CODE_CHI_DESC";
            this.ddlMonth.DataValueField = "CODE";
            this.ddlMonth.DataBind();

            DataSet dtStatus = sysCodeBll.GetList("CODE_TYPE='TaskStatus' ORDER BY DISPLAY_ORDER");

            this.ddlStatus.DataSource = dtStatus.Tables[0];
            this.ddlStatus.DataTextField = "CODE_CHI_DESC";
            this.ddlStatus.DataValueField = "CODE";
            this.ddlStatus.DataBind();

            this.ddlStatus.Items.Insert(0, string.Empty);
        }


        /// <summary>
        /// 绑定后激发事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void rp_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Label lblUserID = e.Item.FindControl("lblUserID") as Label;

                if (lblUserID != null)
                {
                    lblUserID.Text = GetUserName(lblUserID.Text);
                }

                Label lblStatus = e.Item.FindControl("lblStatus") as Label;

                if (lblStatus != null)
                {
                    if (lblStatus.Text.Length > 0)
                    {
                        lblStatus.Text = this.ddlStatus.Items.FindByValue(lblStatus.Text).Text;

                    }
                }

            }
        }

        /// <summary>
        /// 绑定数据，分页
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void pager_PageChanged(object sender, EventArgs e)
        {
            DataBindGrid();
        }

        /// <summary>
        /// 绑定数据源
        /// </summary>
        private void DataBindGrid()
        {
            var b = listRole.Find(o => o.Contains("环保所"));

            if (b != null && b.Length > 0)
            {
                int count = bll.GetRecordCount("1=1");
                DataSet ds = bll.GetListByPage("1=1", string.Empty, PageControl1.PageIndex, PageControl1.PageSize);
                ControlBindHelper.BindRepeaterList(ds.Tables[0], rp_Item);
                this.PageControl1.RecordCount = Convert.ToInt32(count);


            }
            b = listRole.Find(o => o.Contains("供电局"));
            if (b != null && b.Length > 0)
            {
                int count = bll.GetRecordCount(string.Format("PowerSupplyCode='{0}'", PowerSupplyCode));
                DataSet ds = bll.GetListByPage(string.Format("PowerSupplyCode='{0}'", PowerSupplyCode), string.Empty, PageControl1.PageIndex, PageControl1.PageSize);
                ControlBindHelper.BindRepeaterList(ds.Tables[0], rp_Item);
                this.PageControl1.RecordCount = Convert.ToInt32(count);
            }

        }

        /// <summary>
        /// 绑定数据到对象
        /// </summary>
        private void DataBinder()
        {
            Model.GasCode = this.txtGasCode.Text;
            Model.Year = this.ddlYear.SelectedValue;
            Model.Month = this.ddlMonth.SelectedValue;
            Model.ConvertStationCode = this.ddlConvertStationName.SelectedValue;
            Model.ConvertStationName = this.ddlConvertStationName.SelectedItem.Text;
            Model.AirDemand = this.txtAirDemand.Text == "" ? 0 : Convert.ToInt32(this.txtAirDemand.Text);
            Model.QtyDesc = this.txtQtyDesc.Text;
            Model.AppliDesc = this.txtAppliDesc.Text;
            Model.Contacts = this.txtContacts.Text;
            Model.OfficeTel = this.txtOfficeTel.Text;
            Model.PhoneNum = this.txtPhoneNum.Text;
            Model.ReceiptAddress = this.txtReceiptAddress.Text;
            if (this.txtRegistrantDate.Text.Trim().Length > 0)
            {
                Model.RegistrantDate = Convert.ToDateTime(this.txtRegistrantDate.Text);
            }

            Model.PowerSupplyCode = this.ddlPowerSupplyName.SelectedValue;
            Model.PowerSupplyName = this.ddlPowerSupplyName.SelectedItem.Text;
            Model.UseDate = Convert.ToDateTime(this.txtUseDate.Text);
            // Model.AuditorOID = this.txtAuditorOID.Text;
            // Model.AuditsDate = this.txtAuditsDate.Text == "" ? DateTime.Now : Convert.ToDateTime(this.txtAuditsDate.Text);
        }

        /// <summary>
        /// 初始化控件
        /// </summary>
        private void InitControl()
        {
            this.txtGasCode.Text = Model.GasCode;
            this.ddlMonth.SelectedValue = Model.Month;
            this.ddlYear.SelectedValue = Model.Year;

            //this.ddlConvertStationName.SelectedValue = Model.ConvertStationCode;
            this.ddlConvertStationName.Items.Clear();
            this.ddlConvertStationName.Items.Add(Model.ConvertStationName);

            this.txtAirDemand.Text = Model.AirDemand.ToString();
            this.txtQtyDesc.Text = Model.QtyDesc;
            this.txtAppliDesc.Text = Model.AppliDesc;
            this.txtContacts.Text = Model.Contacts;
            this.txtOfficeTel.Text = Model.OfficeTel;
            this.txtPhoneNum.Text = Model.PhoneNum;
            this.txtReceiptAddress.Text = Model.ReceiptAddress;
            this.ddlStatus.SelectedValue = Model.Status;
            this.txtRegistrantOID.Text = GetUserName(Model.RegistrantOID);
            this.txtRegistrantDate.Text = Model.RegistrantDate == null ? string.Empty : Model.RegistrantDate.Value.ToShortDateString();
            this.txtAuditorOID.Text = GetUserName(Model.AuditorOID);
            this.txtAuditsDate.Text = Model.AuditsDate == null ? string.Empty : Model.AuditsDate.Value.ToShortDateString();
            this.txtUseDate.Text = (Model.UseDate.Equals(DateTime.MinValue) ? DateTime.Now : Model.UseDate).ToString("yyyy-MM-dd");
            var b = listRole.Find(o => o.Contains("供电局"));
            if (b != null && b.Length > 0)
            {
                this.ddlPowerSupplyName.Enabled = false;
                this.ddlPowerSupplyName.Items.Clear();
                this.ddlPowerSupplyName.Items.Add(new ListItem(Model.PowerSupplyName, Model.PowerSupplyCode));
            }

            b = listRole.Find(o => o.Contains("环保所"));

            if (b != null && b.Length > 0)
            {

                this.ddlPowerSupplyName.Enabled = true;
                DataTable dtPowerStation = ComServies.GetAllPowerStation();
                this.ddlPowerSupplyName.DataSource = dtPowerStation;
                this.ddlPowerSupplyName.DataTextField = "Organization_Name";
                this.ddlPowerSupplyName.DataValueField = "Organization_ID";
                this.ddlPowerSupplyName.DataBind();

                this.ddlPowerSupplyName.Items.Insert(0, string.Empty);

                this.ddlPowerSupplyName.SelectedValue = Model.PowerSupplyCode;
            }
        }

        /// <summary>
        /// 获取用户名
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        private string GetUserName(string userId)
        {
            DataRow[] dr = UserList.Select(string.Format("User_ID='{0}'", userId));
            if (dr.Length > 0)
            {
                return dr[0][1].ToString();
            }
            return string.Empty;
        }

        private void ClearControl()
        {
            Model = new EGMNGS.Model.ApplNeedGasReg();
            InitControl();
        }
        #endregion

        #region 按钮事件

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            if (Model.OID == 0 || Model.Status != "0")
            {
                return;
            }

            Model.Status = "1";
            if (bll.Update(Model))
            {
                ShowMsgHelper.Alert("提交成功！");
                InitControl();
                DataBindGrid();
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            DataBindGrid();
        }

        /// <summary>
        /// 获取当前对象
        /// </summary>
        private void ShowMaxMode()
        {
            Model = bll.GetModel(bll.GetMaxId() - 1);
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {


                if (actionMode == "Add")
                {
                    DataBinder();
                    Model.RegistrantOID = User.UserId.ToString();

                    int count = bll.GetRecordCount(string.Format("GasCode='{0}'", Model.GasCode));
                    if (count > 0)
                    {
                        ShowMsgHelper.showWarningMsg("需气编号已经存在!");
                        return;
                    }

                    if (bll.Add(Model) > 0)
                    {
                        actionMode = string.Empty;
                    }

                    DataBindGrid();
                    ShowMaxMode();
                }
                else if (actionMode == "Edit")
                {
                    if (Convert.ToInt32(Model.Status) == 2)
                    {
                        ShowMsgHelper.Alert_Wern("“已确认”状态无法修改数据！");
                        return;
                    }

                    DataBinder();
                    if (bll.Update(Model))
                    {
                        EGMNSShowMsg.ShowEditMsgSuccess();
                    }

                    DataBindGrid();
                }
            }
            catch (ArgumentOutOfRangeException ex)
            {
                string[] errs = CommonHelper.GetValue(ex.Message, "@").Split(':');
                ShowMsgHelper.ExecuteScript(string.Format("ChangeCss($('#txt{0}'),'长度不能超{1}字')", errs[0], errs[1]));
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {

            actionMode = "Add";
            Model = new EGMNGS.Model.ApplNeedGasReg(actionMode);
            Model.PowerSupplyCode = PowerSupplyCode;
            Model.PowerSupplyName = PowerSupplyName;
            Model.RegistrantDate = DateTime.Now;
            Model.RegistrantOID = User.UserId.ToString();
            Model.Status = "0";
            Model.QtyDesc = "25kg  个，50kg  个";
            InitControl();
            DropDownListBinder();
        }


        protected void btnDel_Click(object sender, EventArgs e)
        {
            if (Convert.ToInt32(Model.Status) >= 1)
            {
                ShowMsgHelper.Alert_Wern("“已经提交！”状态不可以删除");
            }
            else
            {
                bll.Delete(Model.OID);
                Model = new EGMNGS.Model.ApplNeedGasReg();
                InitControl();
                DataBindGrid();
                ShowMsgHelper.Alert("删除成功！");
            }

        }

        protected void rp_Item_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            int oid = Convert.ToInt32(e.CommandArgument);

            Model = bll.GetModel(oid);
            InitControl();
            actionMode = "Edit";
        }
        #endregion

        protected void btnComfirm_Click(object sender, EventArgs e)
        {
            if (Convert.ToInt32(Model.Status) == 2)
            {
                ShowMsgHelper.Alert_Wern("“已确认”状态不可以再确认！");
                return;
            }

            if (Convert.ToInt32(Model.Status) > 0)
            {
                Model.Status = "2";
                bll.Update(Model);
                InitControl();
                DataBindGrid();
            }
        }
        /// <summary>
        /// 审核
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnaudit_Click(object sender, EventArgs e)
        {
            if (Model.Status == "1")
            {
                Model.Status = "3";
                Model.AuditorOID = User.UserId.ToString();
                Model.AuditsDate = DateTime.Now;
                bll.Update(Model);
                InitControl();
                DataBindGrid();
            }
        }


        protected void ddlPowerSupplyName_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadPowerSupplyName();

        }
        private void LoadPowerSupplyName()
        {
            this.ddlConvertStationName.Items.Clear();
            EGMNGS.BLL.InfoPowerSupplyConvertStation bll = new EGMNGS.BLL.InfoPowerSupplyConvertStation();
            DataSet dsPowerSupplyCode = bll.GetList(string.Format("PowerSupplyCode='{0}'", this.ddlPowerSupplyName.SelectedValue));
            this.ddlConvertStationName.DataSource = dsPowerSupplyCode.Tables[0];
            this.ddlConvertStationName.DataTextField = "ConvartStationName";
            this.ddlConvertStationName.DataValueField = "CovnertStationCode";
            this.ddlConvertStationName.DataBind();
            this.ddlConvertStationName.Items.Insert(0, string.Empty);
        }
    }
}
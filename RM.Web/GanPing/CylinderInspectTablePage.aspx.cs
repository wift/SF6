﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using RM.Common.DotNetCode;
using System.Data;
using RM.Common.DotNetUI;
using RM.Web.App_Code;
using EGMNGS.Common;
namespace RM.Web.GanPing
{
    public partial class CylinderInspectTablePage : PageBase
    {
        #region 属性字段

        EGMNGS.BLL.CylinderInspectTable bll = new EGMNGS.BLL.CylinderInspectTable();
        private DataTable UserList
        {
            get
            {
                return ViewState["UserList"] as DataTable;
            }
            set { ViewState["UserList"] = value; }
        }
        private EGMNGS.Model.CylinderInspectTable Model
        {
            get
            {
                if (ViewState["CylinderInspectTable"] == null)
                {
                    return new EGMNGS.Model.CylinderInspectTable();
                }
                return ViewState["CylinderInspectTable"] as EGMNGS.Model.CylinderInspectTable;
            }
            set { ViewState["CylinderInspectTable"] = value; }
        }
        private string actionMode
        {
            get { return ViewState["ActionMode"] as String; }
            set { ViewState["ActionMode"] = value; }
        }
        public string PowerSupplyCode
        {
            get { return ViewState["PowerSupplyCode"] as String; }
            set { ViewState["PowerSupplyCode"] = value; }
        }
        public string PowerSupplyName
        {
            get { return ViewState["PowerSupplyName"] as String; }
            set { ViewState["PowerSupplyName"] = value; }
        }

        #endregion

        #region 事件方法

        protected void Page_Load(object sender, EventArgs e)
        {
            this.PageControl1.pageHandler += new EventHandler(pager_PageChanged);
            if (!IsPostBack)
            {
                UserList = ComServies.GetAllUserInfo();

                DropDownListBinder();
                this.txtCylinderCode.Focus();
            }
        }

        /// <summary>
        /// 绑定下拉列
        /// </summary>
        private void DropDownListBinder()
        {

            EGMNGS.BLL.SYS_CODE sysCodeBll = new EGMNGS.BLL.SYS_CODE();
            DataSet dtCFStatus = sysCodeBll.GetList("CODE_TYPE='GPStatus' ORDER BY DISPLAY_ORDER");

            this.ddlCFStatus.DataSource = dtCFStatus.Tables[0];
            this.ddlCFStatus.DataTextField = "CODE_CHI_DESC";
            this.ddlCFStatus.DataValueField = "CODE";
            this.ddlCFStatus.DataBind();

            this.ddlCFStatus.Items.Insert(0, string.Empty);

            DataSet dtIsPass = sysCodeBll.GetList("CODE_TYPE='IsPass' ORDER BY DISPLAY_ORDER");

            this.ddlIsPass.DataSource = dtIsPass.Tables[0];
            this.ddlIsPass.DataTextField = "CODE_CHI_DESC";
            this.ddlIsPass.DataValueField = "CODE";
            this.ddlIsPass.DataBind();

            this.ddlIsPass.Items.Insert(0, string.Empty);
        }


        /// <summary>
        /// 绑定后激发事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void rp_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Label lblSendCheckoid = e.Item.FindControl("lblSendCheckoid") as Label;
                if (lblSendCheckoid != null)
                {
                    lblSendCheckoid.Text = GetUserName(lblSendCheckoid.Text);
                }

                Label lblIsPass = e.Item.FindControl("lblIsPass") as Label;
                if (lblIsPass != null)
                {
                    if (lblIsPass.Text.Length > 0)
                    {
                        lblIsPass.Text = this.ddlIsPass.Items.FindByValue(lblIsPass.Text).Text;
                    }
                }

                Label lblStatus = e.Item.FindControl("lblStatus") as Label;
                if (lblStatus != null)
                {
                    if (lblStatus.Text.Length > 0)
                    {
                        lblStatus.Text = this.ddlCFStatus.Items.FindByValue(lblStatus.Text).Text;
                    }
                }


            }
        }

        /// <summary>
        /// 绑定数据，分页
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void pager_PageChanged(object sender, EventArgs e)
        {
            DataBindGrid();
        }

        /// <summary>
        /// 绑定数据源
        /// </summary>
        private void DataBindGrid()
        {
            int count = bll.GetRecordCount("1=1");
            DataSet ds = bll.GetListByPage("1=1", "RegistantsDate DESC", PageControl1.PageIndex, PageControl1.PageSize);
            ControlBindHelper.BindRepeaterList(ds.Tables[0], rp_Item);
            this.PageControl1.RecordCount = Convert.ToInt32(count);
        }
        /// <summary>
        /// 绑定数据到对象
        /// </summary>
        private void DataBinder()
        {
            Model.CylinderCode = this.txtCylinderCode.Text;
            Model.IsPass = this.ddlIsPass.SelectedValue;
            Model.Remarks = this.txtRemarks.Text;

            Model.CylinderCode = this.txtCylinderCode.Text;
            if (this.txtOriginalEffectiveDate.Text.Length > 0)
            {
                Model.OriginalEffectiveDate = DateTime.Parse(this.txtOriginalEffectiveDate.Text);
            }

            if (this.txtNewEffectiveDate.Text.Length > 0)
            {
                Model.NewEffectiveDate = DateTime.Parse(this.txtNewEffectiveDate.Text);
            }

            if (this.txtSendCheckDate.Text.Length > 0)
            {
                Model.SendCheckDate = DateTime.Parse(this.txtSendCheckDate.Text);
            }

            Model.Remarks = this.txtRemarks.Text;
            //Model.Status = this.txtStatus.Text;

        }

        /// <summary>
        /// 初始化控件
        /// </summary>
        private void InitControl()
        {
            txtCylinderCode.Focus();

            this.txtCylinderCode.Text = Model.CylinderCode;
            if (Model.OriginalEffectiveDate != null)
            {
                this.txtOriginalEffectiveDate.Text = Model.OriginalEffectiveDate.Value.ToShortDateString();

            }

            this.txtNewEffectiveDate.Text = Model.NewEffectiveDate == null ? string.Empty : Model.NewEffectiveDate.Value.ToShortDateString();

            this.ddlIsPass.SelectedValue = Model.IsPass;
            this.txtRemarks.Text = Model.Remarks;
            this.ddlCFStatus.SelectedValue = Model.Status;
            this.txtSendCheckOID.Text = GetUserName(Model.SendCheckOID);

            this.txtSendCheckDate.Text = Model.SendCheckDate == null ? string.Empty : Model.SendCheckDate.Value.ToShortDateString();

            this.txtRegistantsOID.Text = GetUserName(Model.RegistantsOID);
            if (Model.RegistantsDate != null)
            {
                this.txtRegistantsDate.Text = Model.RegistantsDate.Value.ToShortDateString();
            }

        }

        /// <summary>
        /// 获取用户名
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        private string GetUserName(string userId)
        {
            DataRow[] dr = UserList.Select(string.Format("User_ID='{0}'", userId));
            if (dr.Length > 0)
            {
                return dr[0][1].ToString();
            }
            return string.Empty;
        }

        private void ClearControl()
        {
            Model = new EGMNGS.Model.CylinderInspectTable();
            InitControl();
        }
        #endregion

        #region 按钮事件

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            if (this.HiddenField1.Value.Length > 0)
            {
                string[] arrayOID = this.HiddenField1.Value.Split(new char[] { ',' });
                foreach (var item in arrayOID)
                {

                    var tempMode = bll.GetModel(Convert.ToInt32(item));
                    //if (tempMode.IsPass != null && tempMode.IsPass.Length > 0)
                    //{
                    //    ShowMsgHelper.Alert_Error("此钢瓶已送检不能再提交！");
                    //    return;
                    //}

                    if (tempMode.Status == "5")
                    {
                        ShowMsgHelper.Alert_Error("此钢瓶已送检不能再提交！");
                        return;
                    }

                    //修改为待送检
                    if (bll.UpdateStatus(tempMode.CylinderCode, "5", tempMode.OID.ToString()))
                    {
                        ShowMsgHelper.Alert("提交成功！");
                    }

                }

                InitControl();
                DataBindGrid();

            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            DataBindGrid();
        }

        private void ShowMaxMode()
        {
            Model = bll.GetModel(bll.GetMaxId() - 1);
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                if (actionMode == "Add")
                {
                    DataBinder();
                    Model.RegistantsOID = User.UserId.ToString();

                    EGMNGS.BLL.BookCylinderInfo bclbll = new EGMNGS.BLL.BookCylinderInfo();
                    EGMNGS.Model.BookCylinderInfo BookCylinderInfoObj = bclbll.GetModelByCylinderCode(Model.CylinderCode);

                    if (BookCylinderInfoObj == null)
                    {
                        ShowMsgHelper.showWarningMsg("此钢瓶编码不存在!");
                        return;
                    }
                    else if (BookCylinderInfoObj.Status == "5" || BookCylinderInfoObj.Status == "6")
                    {
                        ShowMsgHelper.showWarningMsg("此钢瓶为待送检或已报废不能添加!");
                        return;
                    }

                    Model.OriginalEffectiveDate = BookCylinderInfoObj.EffectiveDate;
                    Model.Status = BookCylinderInfoObj.Status;

                    if (bll.Add(Model) > 0)
                    {

                        actionMode = "Edit";
                    }

                    DataBindGrid();
                    ShowMaxMode();
                }
                else if (actionMode == "Edit")
                {
                    if (bll.GetModel(Model.OID).Status == "5")
                    {
                        ShowMsgHelper.Alert_Wern("“已确认”状态无法修改数据！");
                        return;
                    }

                    DataBinder();
                    if (bll.Update(Model))
                    {
                        EGMNSShowMsg.ShowEditMsgSuccess();
                        actionMode = string.Empty;
                    }
                    DataBindGrid();
                }
            }
            catch (ArgumentOutOfRangeException ex)
            {
                string[] errs = CommonHelper.GetValue(ex.Message, "@").Split(':');
                ShowMsgHelper.ExecuteScript(string.Format("ChangeCss($('#txt{0}'),'长度不能超{1}字')", errs[0], errs[1]));
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            actionMode = "Add";
            Model = new EGMNGS.Model.CylinderInspectTable();
            Model.RegistantsDate = DateTime.Now;
            Model.RegistantsOID = User.UserId.ToString();
            //Model.NewEffectiveDate = null;
            //Model.SendCheckDate = null;
            InitControl();
        }


        protected void btnDel_Click(object sender, EventArgs e)
        {
            if (Model.OID == 0)
            {
                ShowMsgHelper.Alert_Wern("请选择一条记录");
                return;
            }
            if (bll.GetModel(Model.OID).Status == "5")
            {
                ShowMsgHelper.Alert_Wern("“已确认”状态不可以删除");
            }
            else
            {
                bll.Delete(Model.OID);
                Model = new EGMNGS.Model.CylinderInspectTable();
                InitControl();
                DataBindGrid();
                ShowMsgHelper.Alert("删除成功！");
            }
        }

        protected void rp_Item_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            int oid = Convert.ToInt32(e.CommandArgument);

            Model = bll.GetModel(oid);
            InitControl();
            actionMode = "Edit";
        }
        #endregion
        /// <summary>
        /// 送检保存
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSJ_Click(object sender, EventArgs e)
        {
            if (Model.Status == "5")
            {
                //送检保存，判断是否合格、送检人、送检日期不能空
                if (this.ddlIsPass.SelectedIndex == -1)
                {
                    ShowMsgHelper.Alert_Error("是否合格不能为空！");
                    return;
                }

                DataBinder();
                Model.SendCheckOID = User.UserId.ToString();
                Model.SendCheckDate = DateTime.Now;

                if (this.ddlIsPass.SelectedValue == "0")
                {
                    Model.Status = "6";
                }
                else if (this.ddlIsPass.SelectedValue == "1")
                {
                    Model.Status = "0";
                }




                if (bll.UpdateSJ(Model))
                {
                    EGMNSShowMsg.ShowEditMsgSuccess();
                }
                else
                {
                    ShowMsgHelper.Alert_Error("送检保存失败!");
                }
                DataBindGrid();
                InitControl();
            }
            else
            {
                ShowMsgHelper.Alert_Wern("请先提交！");
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using RM.Common.DotNetCode;
using System.Data;
using RM.Common.DotNetUI;
using RM.Web.App_Code;
using EGMNGS.Common;
using EGMNGS.Model;
using RM.Common.DotNetFile;
using System.Configuration;

namespace RM.Web.GanPing
{
    public partial class ApplDetectionGasPageDJ : PageBase
    {
        #region 属性字段

        EGMNGS.BLL.ApplDetectionGas bll = new EGMNGS.BLL.ApplDetectionGas();
        EGMNGS.BLL.DetailsApplDetectionGas DetailsBLL = new EGMNGS.BLL.DetailsApplDetectionGas();

        /// <summary>
        /// 需要添加的数据
        /// </summary>
        private Decimal? TotalAmountGas
        {
            get
            {
                if (ViewState["Decimal"] == null)
                {
                    return new Decimal(); ;
                }
                return ViewState["Decimal"] as Decimal?;
            }
            set { ViewState["Decimal"] = value; }
        }
        /// <summary>
        /// 需要添加的数据
        /// </summary>
        private List<DetailsApplDetectionGas> AddList
        {
            get
            {
                if (ViewState["AddList"] == null)
                {
                    return new List<DetailsApplDetectionGas>();
                }
                return ViewState["AddList"] as List<DetailsApplDetectionGas>;
            }
            set { ViewState["AddList"] = value; }
        }

        private DataTable UserList
        {
            get
            {
                return ViewState["UserList"] as DataTable;
            }
            set { ViewState["UserList"] = value; }
        }
        private EGMNGS.Model.ApplDetectionGas Model
        {
            get
            {
                if (ViewState["ApplDetectionGas"] == null)
                {
                    return new EGMNGS.Model.ApplDetectionGas();
                }
                return ViewState["ApplDetectionGas"] as EGMNGS.Model.ApplDetectionGas;
            }
            set { ViewState["ApplDetectionGas"] = value; }
        }
        private string actionMode
        {
            get { return ViewState["ActionMode"] as String; }
            set { ViewState["ActionMode"] = value; }
        }
        public string PowerSupplyCode
        {
            get { return ViewState["PowerSupplyCode"] as String; }
            set { ViewState["PowerSupplyCode"] = value; }
        }
        public string PowerSupplyName
        {
            get { return ViewState["PowerSupplyName"] as String; }
            set { ViewState["PowerSupplyName"] = value; }
        }
        public DataTable DetailsTable
        {
            get
            {
                if (ViewState["DetailsTable"] == null)
                {
                    return new DataTable();
                }
                return ViewState["DetailsTable"] as DataTable;
            }
            set { ViewState["DetailsTable"] = value; }
        }
        private string actionModeDetails
        {
            get
            {
                if (ViewState["actionModeDetails"] == null)
                {
                    return string.Empty;
                }
                return ViewState["actionModeDetails"] as String;
            }
            set { ViewState["actionModeDetails"] = value; }
        }

        private List<DetailsApplDetectionGas> UpdateList
        {
            get
            {
                if (ViewState["UpdateList"] == null)
                {
                    return new List<DetailsApplDetectionGas>();
                }
                return ViewState["UpdateList"] as List<DetailsApplDetectionGas>;
            }
            set { ViewState["UpdateList"] = value; }
        }

        private List<DetailsApplDetectionGas> ConfirmList
        {
            get
            {
                if (ViewState["ConfirmList"] == null)
                {
                    return new List<DetailsApplDetectionGas>();
                }
                return ViewState["ConfirmList"] as List<DetailsApplDetectionGas>;
            }
            set { ViewState["ConfirmList"] = value; }
        }
        public Dictionary<string, string> DictIsPass
        {
            get
            {
                if (ViewState["IsPass"] == null)
                {
                    Dictionary<string, string> IsPass = new Dictionary<string, string>();
                    EGMNGS.BLL.SYS_CODE sysCodeBll = new EGMNGS.BLL.SYS_CODE();
                    DataSet dsIsPass = sysCodeBll.GetList("CODE_TYPE='IsPass' ORDER BY DISPLAY_ORDER");
                    foreach (DataRow item in dsIsPass.Tables[0].Rows)
                    {
                        IsPass.Add(item["CODE"].ToString(), item["CODE_CHI_DESC"].ToString());
                    }
                    IsPass.Add(string.Empty, string.Empty);
                    ViewState["IsPass"] = IsPass;
                }

                return ViewState["IsPass"] as Dictionary<string, string>;
            }
            set { ViewState["IsPass"] = value; }
        }
        #endregion

        #region 事件方法

        protected void Page_Load(object sender, EventArgs e)
        {


            this.PageControl1.pageHandler += new EventHandler(pager_PageChanged);
            if (!IsPostBack)
            {
                UserList = ComServies.GetAllUserInfo();
                string[] orgIdOrgName = ComServies.GetPowerApplyByUserID(User.UserId.ToString());
                PowerSupplyCode = orgIdOrgName[0];
                PowerSupplyName = orgIdOrgName[1];
                DropDownListBinder();


            }
            ScriptManager.GetCurrent(this.Page).RegisterPostBackControl(this.btnExport);

        }

        /// <summary>
        /// 绑定下拉列
        /// </summary>
        private void DropDownListBinder()
        {
            EGMNGS.BLL.InfoPowerSupplyConvertStation bll = new EGMNGS.BLL.InfoPowerSupplyConvertStation();


            EGMNGS.BLL.SYS_CODE sysCodeBll = new EGMNGS.BLL.SYS_CODE();
            DataSet dtCFStatus = sysCodeBll.GetList("CODE_TYPE='TaskStatus' ORDER BY DISPLAY_ORDER");

            this.ddlStatus.DataSource = dtCFStatus.Tables[0];
            this.ddlStatus.DataTextField = "CODE_CHI_DESC";
            this.ddlStatus.DataValueField = "CODE";
            this.ddlStatus.DataBind();

            this.ddlStatus.Items.Insert(0, string.Empty);

            DataTable dtPowerStation = ComServies.GetAllPowerStation();
            this.ddlPowerSupplyName.DataSource = dtPowerStation;
            this.ddlPowerSupplyName.DataTextField = "Organization_Name";
            this.ddlPowerSupplyName.DataValueField = "Organization_ID";
            this.ddlPowerSupplyName.DataBind();
            this.ddlPowerSupplyName.Items.Insert(0, string.Empty);

            DataSet dtCheckPJType = sysCodeBll.GetList("CODE_TYPE='CheckPJTypeStatus' ORDER BY DISPLAY_ORDER");

            this.ddlCheckPJType.DataSource = dtCheckPJType.Tables[0];
            this.ddlCheckPJType.DataTextField = "CODE_CHI_DESC";
            this.ddlCheckPJType.DataValueField = "CODE";
            this.ddlCheckPJType.DataBind();

        }


        /// <summary>
        /// 绑定后激发事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void rp_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Label lblStatus = e.Item.FindControl("lblStatus") as Label;

                if (lblStatus != null)
                {
                    if (lblStatus.Text.Length > 0)
                    {
                        lblStatus.Text = this.ddlStatus.Items.FindByValue(lblStatus.Text).Text;

                    }
                }

            }
        }

        /// <summary>
        /// 绑定数据，分页
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void pager_PageChanged(object sender, EventArgs e)
        {
            DataBindGrid();
        }

        /// <summary>
        /// 绑定数据源
        /// </summary>
        private void DataBindGrid()
        {
            int count = bll.GetRecordCount("1=1");
            DataSet ds = bll.GetListByPage("1=1", string.Empty, PageControl1.PageIndex, PageControl1.PageSize);
            ControlBindHelper.BindRepeaterList(ds.Tables[0], rp_Item);
            this.PageControl1.RecordCount = Convert.ToInt32(count);
        }
        /// <summary>
        /// 绑定数据到对象
        /// </summary>
        private void DataBinder()
        {

            Model.ApplCode = this.tbAppCode.Text;
            Model.DevCode = this.txtDevCode.Text;
            Model.ConvertStationCode = this.ddlConvertStationName.SelectedValue;
            Model.convertStationName = this.ddlConvertStationName.SelectedItem.Text;
            Model.PowerSupplyCode = this.ddlPowerSupplyName.SelectedValue;
            Model.PowerSupplyName = this.ddlPowerSupplyName.SelectedItem.Text;
            Model.DevName = this.txtDevName.Text;
            Model.DevNo = this.tbDevNo.Text;
            Model.InspectionUnit = this.tbInspectionUnit.Text;
            Model.Contacts = this.tbContacts.Text;
            Model.OfficeTel = this.tbOfficeTel.Text;
            Model.PhoneNum = this.tbPhoneNum.Text;
            Model.SampliInspituation = this.tbSampliInspituation.Text;
            if (this.tbAmountDetection.Text.Trim().Length > 0)
            {
                Model.AmountDetection = Convert.ToDecimal(this.tbAmountDetection.Text);
            }
            Model.CheckPJType = this.ddlCheckPJType.SelectedValue;
            if (this.txtRegistantsDate.Text.Length > 0)
            {
                Model.RegistantsDate = Convert.ToDateTime(this.txtRegistantsDate.Text);
            }

        }

        /// <summary>
        /// 初始化控件
        /// </summary>
        private void InitControl()
        {

            this.tbAppCode.Text = Model.ApplCode;

            this.ddlPowerSupplyName.SelectedValue = Model.PowerSupplyCode;

            this.ddlCheckPJType.SelectedValue = Model.CheckPJType;
            // this.ddlConvertStationName.SelectedValue = Model.ConvertStationCode;
            this.ddlConvertStationName.Items.Insert(0, new ListItem(Model.convertStationName, Model.ConvertStationCode));
            this.txtDevName.Text = Model.DevName;
            this.tbDevNo.Text = Model.DevNo;
            this.tbInspectionUnit.Text = Model.InspectionUnit;
            this.tbContacts.Text = Model.Contacts;
            this.tbOfficeTel.Text = Model.OfficeTel;
            this.tbPhoneNum.Text = Model.PhoneNum;
            this.tbSampliInspituation.Text = Model.SampliInspituation;
            this.tbAmountDetection.Text = Model.AmountDetection.ToString();
            this.ddlStatus.SelectedValue = Model.Status;
            this.txtRegistantsoid.Text = GetUserName(Model.RegistantsOID);
            if (Model.RegistantsDate != null)
            {
                this.txtRegistantsDate.Text = Model.RegistantsDate.Value.ToShortDateString();
            }
            LoadPowerSupplyName();


            if (Model.ConvertStationCode != null)
            {
                this.ddlConvertStationName.Items.Add(new ListItem(Model.ConvertStationCode, Model.convertStationName));
                this.ddlConvertStationName.SelectedValue = Model.ConvertStationCode;
            }

           // BindddlDevCode();
            this.txtDevCode.Text = Model.DevCode;


        }

        /// <summary>
        /// 获取用户名
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        private string GetUserName(string userId)
        {
            DataRow[] dr = UserList.Select(string.Format("User_ID='{0}'", userId));
            if (dr.Length > 0)
            {
                return dr[0][1].ToString();
            }
            return string.Empty;
        }

        private void ClearControl()
        {
            Model = new EGMNGS.Model.ApplDetectionGas();
            InitControl();
        }
        #endregion

        #region 按钮事件
        //todo 提交
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            if (Convert.ToInt32(Model.Status) >= 1)
            {
                ShowMsgHelper.Alert("已经提交！");
                return;
            }

            GetCheckBox();

            Model.Status = "1";
            Model.AmountDetection = TotalAmountGas;
            if (ComServies.UpdateRWDJ(Model.Status, Model.AmountDetection, Model.ApplCode))
            {
                //同步更新《钢瓶台账》时没更新【钢瓶状态】改为“已充待检气”。
                ShowMsgHelper.Alert("提交成功！");
                InitControl();
                DataBindGrid();
                GetDetailsOfRecycleGas();

            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            DataBindGrid();
        }

        /// <summary>
        /// 获取当前对象
        /// </summary>
        private void ShowMaxMode()
        {
            Model = bll.GetModel(bll.GetMaxId() - 1);
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {

            try
            {
                if (actionMode == "Add")
                {
                    DataBinder();

                    int count = bll.GetRecordCount(string.Format("ApplCode='{0}'", Model.ApplCode));
                    if (count > 0)
                    {
                        ShowMsgHelper.showWarningMsg("申请编号已经存在!");
                        return;
                    }

                    if (bll.Add(Model) > 0)
                    {
                        actionMode = string.Empty;
                    }

                    DataBindGrid();
                    ShowMaxMode();
                }
                else if (actionMode == "Edit")
                {
                    if (Model.Status == "2")
                    {
                        ShowMsgHelper.Alert_Wern("“已确认”状态无法修改数据！");
                        return;

                    }
                    DataBinder();
                    if (bll.Update(Model))
                    {
                        EGMNSShowMsg.ShowEditMsgSuccess();
                    }
                    DataBindGrid();
                }
            }
            catch (ArgumentOutOfRangeException ex)
            {
                string[] errs = CommonHelper.GetValue(ex.Message, "@").Split(':');
                ShowMsgHelper.ExecuteScript(string.Format("ChangeCss($('#txt{0}'),'长度不能超{1}字')", errs[0], errs[1]));
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            actionMode = "Add";
            Model = new EGMNGS.Model.ApplDetectionGas(actionMode);
            Model.RegistantsDate = DateTime.Now;
            Model.RegistantsOID = User.UserId.ToString();
            Model.Status = "0";
            InitControl();
            GetDetailsOfRecycleGas();
        }


        protected void btnDel_Click(object sender, EventArgs e)
        {
            if (Model.Status == "2")
            {
                ShowMsgHelper.Alert_Wern("“已确认”状态不可以删除");
            }
            else
            {
                bll.Delete(Model.OID);
                Model = new EGMNGS.Model.ApplDetectionGas();
                InitControl();
                DataBindGrid();
                ShowMsgHelper.Alert("删除成功！");
            }
        }

        protected void rp_Item_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            int oid = Convert.ToInt32(e.CommandArgument);

            Model = bll.GetModel(oid);
            InitControl();
            actionMode = "Edit";

            //绑定明细表
            GetDetailsOfRecycleGas();
        }
        #endregion

        #region Details
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void pager_PageChangedDetails(object sender, EventArgs e)
        {
            GetDetailsOfRecycleGas();
        }

        private void GetDetailsOfRecycleGas()
        {
            DetailsTable = DetailsBLL.GetList(string.Format("ApplCode='{0}'", Model.ApplCode)).Tables[0];
            int count = DetailsBLL.GetRecordCount(string.Format("ApplCode='{0}'", Model.ApplCode));
            ControlBindHelper.BindRepeaterList(DetailsTable, rptDetails);
        }
        protected void btnAddDetails_Click(object sender, EventArgs e)
        {
            if (Model.Status == "2")
            {
                ShowMsgHelper.Alert_Wern(Model.ApplCode + "已经确认!");
                return;
            }
            else if (Model.Status == "1")
            {
                ShowMsgHelper.Alert_Wern(Model.ApplCode + "已经提交！");
                return;
            }

            //首先，恢复数据源
            if (IsRetriRepeter())
            {
                return;
            }

            DataTable dt = DetailsTable;
            //LoadTestData();
            DataRow row = dt.NewRow();
            row[2] = string.Empty;
            row[3] = string.Empty;
            row[4] = 50.00;
            dt.Rows.Add(row);

            rptDetails.DataSource = dt;
            rptDetails.DataBind();


            actionModeDetails = "Add";

            ShowMsgHelper.ExecuteScript("Foc()");
        }
        private bool IsRetriRepeter()
        {
            bool isExists = false;

            for (int i = 0; i < this.rptDetails.Items.Count; i++)
            {
                Label txtCylinderCode = this.rptDetails.Items[i].FindControl("txtCylinderCode") as Label;
                TextBox txtAmountGas = this.rptDetails.Items[i].FindControl("txtAmountGas") as TextBox;
                TextBox txtProduceNum = this.rptDetails.Items[i].FindControl("txtProduceNum") as TextBox;
                TextBox txtManufacturer = this.rptDetails.Items[i].FindControl("txtManufacturer") as TextBox;

                DetailsTable.Rows[i]["CylinderCode"] = txtCylinderCode.Text;


                DetailsTable.Rows[i]["AmountGas"] = Convert.ToDecimal(txtAmountGas.Text);
                DetailsTable.Rows[i]["ProduceNum"] = txtProduceNum.Text;
                DetailsTable.Rows[i]["Manufacturer"] = txtManufacturer.Text;
            }

            return isExists;
        }



        protected void btnDelDetails_Click(object sender, EventArgs e)
        {
            try
            {
                if (Model.Status == "2")
                {
                    ShowMsgHelper.Alert_Wern(Model.ApplCode + "已经确认!");
                    return;
                }
                else if (Model.Status == "1")
                {
                    ShowMsgHelper.Alert_Wern(Model.ApplCode + "已经提交！");
                    return;
                }

                string strID = HiddenField1.Value;
                if (strID.Length == 0)
                {
                    return;
                }
                DetailsBLL.DeleteList(strID);
                ShowMsgHelper.Alert("删除成功！");
                GetDetailsOfRecycleGas();
            }
            catch (Exception ex)
            {
                ShowMsgHelper.AlertMsg("删除失败！" + ex.Message);
                throw ex;
            }
        }

        protected void btnSaveDetails_Click(object sender, EventArgs e)
        {
            try
            {
                if (Model.Status == "2")
                {
                    ShowMsgHelper.Alert_Wern(Model.ApplCode + "已经确认!");
                    return;
                }
                else if (Model.Status == "1")
                {
                    ShowMsgHelper.Alert_Wern(Model.ApplCode + "已经提交！");
                    return;
                }

                GetCheckBox();

                if (actionModeDetails.Equals("Add"))
                {
                    if (AddList.Count > 0)
                    {

                        //批量添加
                        foreach (var item in AddList)
                        {
                            DetailsBLL.Add(item);
                        }
                        AddList.Clear();
                    }

                    ShowMsgHelper.Alert("添加成功！");
                    actionModeDetails = string.Empty;
                }

                if (UpdateList.Count > 0)
                {


                    //批量修改
                    foreach (var item in UpdateList)
                    {


                        DetailsBLL.Update(item);
                    }

                    UpdateList.Clear();
                    ShowMsgHelper.Alert("修改成功！");
                }
                GetDetailsOfRecycleGas();
            }
            catch (Exception ex)
            {
                ShowMsgHelper.AlertMsg("添加失败！");
                throw;
            }
        }

        protected void rptDetails_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                CheckBox cb = e.Item.FindControl("checkbox") as CheckBox;
                if (cb.ToolTip.Length == 0)
                {
                    cb.Checked = true;
                }
            }
        }


        /// <summary>
        /// 
        /// </summary>
        private void GetCheckBox()
        {
            UpdateList = new List<DetailsApplDetectionGas>();
            AddList = new List<DetailsApplDetectionGas>();
            TotalAmountGas = new Decimal();
            ConfirmList = new List<DetailsApplDetectionGas>();

            for (int i = 0; i < this.rptDetails.Items.Count; i++)
            {
                System.Web.UI.WebControls.CheckBox checkbox = (System.Web.UI.WebControls.CheckBox)rptDetails.Items[i].FindControl("checkbox");

                TextBox txtAmountGastemp = this.rptDetails.Items[i].FindControl("txtAmountGas") as TextBox;
                TotalAmountGas += Convert.ToDecimal(txtAmountGastemp.Text);
                if (Convert.ToDecimal(txtAmountGastemp.Text) <= 0)
                {
                    continue;
                }

                if (checkbox.Checked == true && checkbox.ToolTip.Length > 0)
                {
                    //  Label txtCylinderCode = this.rptDetails.Items[i].FindControl("txtCylinderCode") as Label;
                    //TextBox txtGasCode = this.rptDetails.Items[i].FindControl("txtGasCode") as TextBox;
                    TextBox txtAmountGas = this.rptDetails.Items[i].FindControl("txtAmountGas") as TextBox;
                    TextBox txtProduceNum = this.rptDetails.Items[i].FindControl("txtProduceNum") as TextBox;
                    TextBox txtManufacturer = this.rptDetails.Items[i].FindControl("txtManufacturer") as TextBox;

                    DetailsApplDetectionGas Obj = new EGMNGS.BLL.DetailsApplDetectionGas().GetModel(int.Parse(checkbox.ToolTip));
                    //Obj.OID = int.Parse(checkbox.ToolTip);
                    //Obj.ApplCode = Model.ApplCode;
                    //  Obj.CylinderCode = txtCylinderCode.Text;
                    if (txtAmountGas.Text.Trim().Length > 0)
                    {
                        Obj.AmountGas = decimal.Parse(txtAmountGas.Text);
                    }

                    Obj.ProduceNum = txtProduceNum.Text;
                    Obj.Manufacturer = txtManufacturer.Text;

                    UpdateList.Add(Obj);
                }
                else if (checkbox.Checked == true && checkbox.ToolTip.Length == 0)
                {
                    // TextBox txtCylinderCode = this.rptDetails.Items[i].FindControl("txtCylinderCode") as TextBox;
                    // TextBox txtGasCode = this.rptDetails.Items[i].FindControl("txtGasCode") as TextBox;
                    TextBox txtAmountGas = this.rptDetails.Items[i].FindControl("txtAmountGas") as TextBox;
                    TextBox txtProduceNum = this.rptDetails.Items[i].FindControl("txtProduceNum") as TextBox;
                    TextBox txtManufacturer = this.rptDetails.Items[i].FindControl("txtManufacturer") as TextBox;

                    DetailsApplDetectionGas Obj = new EGMNGS.Model.DetailsApplDetectionGas();
                    Obj.ApplCode = Model.ApplCode;
                    // Obj.CylinderCode = txtCylinderCode.Text;
                    if (txtAmountGas.Text.Trim().Length > 0)
                    {
                        Obj.AmountGas = decimal.Parse(txtAmountGas.Text);
                    }
                    Obj.ProduceNum = txtProduceNum.Text;
                    Obj.Manufacturer = txtManufacturer.Text;

                    AddList.Add(Obj);
                }
            }
        }

        private void LoadDetails()
        {
            TotalAmountGas = new Decimal();
            ConfirmList = new List<DetailsApplDetectionGas>();

            for (int i = 0; i < this.rptDetails.Items.Count; i++)
            {
                System.Web.UI.WebControls.CheckBox checkbox = (System.Web.UI.WebControls.CheckBox)rptDetails.Items[i].FindControl("checkbox");

                TextBox txtAmountGastemp = this.rptDetails.Items[i].FindControl("txtAmountGas") as TextBox;
                TotalAmountGas += Convert.ToDecimal(txtAmountGastemp.Text);

                Label txtCylinderCode = this.rptDetails.Items[i].FindControl("txtCylinderCode") as Label;

                TextBox txtAmountGas = this.rptDetails.Items[i].FindControl("txtAmountGas") as TextBox;

                DetailsApplDetectionGas Obj = new EGMNGS.BLL.DetailsApplDetectionGas().GetModel(int.Parse(checkbox.ToolTip));
                // Obj.OID = int.Parse(checkbox.ToolTip);
                //  Obj.ApplCode = Model.ApplCode;
                Obj.CylinderCode = txtCylinderCode.Text;
                if (txtAmountGas.Text.Trim().Length > 0)
                {
                    Obj.AmountGas = decimal.Parse(txtAmountGas.Text);
                }

                ConfirmList.Add(Obj);
            }
        }

        #endregion
        EGMNGS.BLL.InfoDevPara bllInfoDevPara = new EGMNGS.BLL.InfoDevPara();
        protected void ddlConvertStationName_SelectedIndexChanged(object sender, EventArgs e)
        {
          //  BindddlDevCode();

        }

        //private void BindddlDevCode()
        //{
        //    DataSet ds = bllInfoDevPara.GetList(string.Format("ConvertStationCode='{0}'", this.ddlConvertStationName.SelectedValue));
        //    this.ddlDevCode.Items.Clear();
        //    this.ddlDevCode.DataSource = ds.Tables[0];
        //    this.ddlDevCode.DataTextField = "DevCode";
        //    this.ddlDevCode.DataValueField = "OID";
        //    this.ddlDevCode.DataBind();
        //    this.ddlDevCode.Items.Insert(0, string.Empty);
        //}
        protected void ddlDevCode_SelectedIndexChanged(object sender, EventArgs e)
        {
            //string oid = this.ddlDevCode.SelectedValue;
            //if (oid.Length == 0)
            //{
            //    return;
            //}
            //InfoDevPara infoDevP = bllInfoDevPara.GetModel(Convert.ToInt32(oid));
            //this.txtDevName.Text = infoDevP.DevName;
            //this.tbDevNo.Text = infoDevP.DevType;
        }
        /// <summary>
        /// 确认
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnConfirm_Click(object sender, EventArgs e)
        {
            if (Model.Status == "1")
            {

                // GetCheckBox();
                // List<DetailsApplDetectionGas> listDetials = DetailsBLL.GetModelList(string.Format("ApplCode='{0}'", Model.Code));
                LoadDetails();

                foreach (DetailsApplDetectionGas item in ConfirmList)
                {
                    item.GasCode = ComServies.GetCode("QT", DateTime.Now.Year.ToString() +

    DateTime.Now.Month.ToString("D2"));
                    DetailsBLL.Update(item);

                    //气体灌装台账表
                    string fillCode = ComServies.GetCode("CQ", DateTime.Now.Year.ToString() +
  DateTime.Now.Month.ToString("D2"));
                    new EGMNGS.BLL.BookGasFill().Add(new BookGasFill() { GasType = "2", BusinessCode = Model.ApplCode, CylinderStatus = "3", AmountGas = item.AmountGas, FillGasCode = fillCode, GasCode = item.GasCode, CylinderCode = item.CylinderCode, RegistrantDate = DateTime.Now, RegistrantOID = User.UserId.ToString() });

                    ComServies.UpdateCylinderInfo(item.CylinderCode, item.GasCode);
                    //气体检测任务   
                    EGMNGS.Model.ApplRegGasQtyInspection tempApplRegGasQtyInspection = new ApplRegGasQtyInspection(GlobalConstants.ActionModeAdd);
                    tempApplRegGasQtyInspection.CylinderCode = item.CylinderCode;
                    tempApplRegGasQtyInspection.GasCdoe = item.GasCode;
                    tempApplRegGasQtyInspection.GasSourse = "2";//入网新气
                    tempApplRegGasQtyInspection.BusinessCode = Model.ApplCode;
                    tempApplRegGasQtyInspection.ApplOID = User.UserId.ToString();
                    tempApplRegGasQtyInspection.AppLDate = DateTime.Now;
                    tempApplRegGasQtyInspection.FlowStatus = "1";
                    tempApplRegGasQtyInspection.CheckPJType = Model.CheckPJType;
                    tempApplRegGasQtyInspection.ReportCode = DateTime.Now.Year + ComServies.GetCode4("R");
                    new EGMNGS.BLL.ApplRegGasQtyInspection().Add(tempApplRegGasQtyInspection);
                }

                Model.Status = "2";
                bll.Update(Model);

                DataBindGrid();
                GetDetailsOfRecycleGas();


                ShowMsgHelper.Alert("确认成功！");
            }
        }

        protected void ddlPowerSupplyName_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadPowerSupplyName();

        }
        private void LoadPowerSupplyName()
        {
            this.ddlConvertStationName.Items.Clear();
            EGMNGS.BLL.InfoPowerSupplyConvertStation bll = new EGMNGS.BLL.InfoPowerSupplyConvertStation();
            DataSet dsPowerSupplyCode = bll.GetList(string.Format("PowerSupplyCode='{0}'", this.ddlPowerSupplyName.SelectedValue));
            this.ddlConvertStationName.DataSource = dsPowerSupplyCode.Tables[0];
            this.ddlConvertStationName.DataTextField = "ConvartStationName";
            this.ddlConvertStationName.DataValueField = "CovnertStationCode";
            this.ddlConvertStationName.DataBind();
            this.ddlConvertStationName.Items.Insert(0, string.Empty);
        }



        /// <summary>
        /// 导出
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnExport_Click(object sender, EventArgs e)
        {
            if (HiddenField1.Value == "ExportXlsSample")
            {
                SendSampleForm();
            }
            else
            {
                if (Model.OID == 0)
                {
                    ShowMsgHelper.Alert_Error("请选择一个记录！");
                    return;
                }
                MouldExportExcel mee = new MouldExportExcel();

                try
                {
                    string physicalPath = Request.PhysicalApplicationPath + "Xls\\";
                    Microsoft.Office.Interop.Excel.Workbook sh = mee.OpenExcel(physicalPath + "入网检测登记.xls");
                    Microsoft.Office.Interop.Excel.Worksheet ws = sh.Worksheets[1] as Microsoft.Office.Interop.Excel.Worksheet;
                    // mee.SetCellValue(ws, 4, 3, "1111");
                    #region 设置值
                    mee.SetCellValue(ws, 2, 2, Model.ApplCode);
                    mee.SetCellValue(ws, 2, 4, Model.PowerSupplyName);
                    mee.SetCellValue(ws, 2, 6, Model.convertStationName);

                    mee.SetCellValue(ws, 3, 2, Model.InspectionUnit);
                    mee.SetCellValue(ws, 3, 4, Convert.ToString(Model.AmountDetection));
                    mee.SetCellValue(ws, 3, 6, Model.SampliInspituation);

                    if (Model.DevCode != null)
                    {
                        mee.SetCellValue(ws, 4, 2, Model.DevCode);
                    }

                    mee.SetCellValue(ws, 4, 4, Model.DevName);
                    mee.SetCellValue(ws, 4, 6, Model.DevNo);

                    mee.SetCellValue(ws, 5, 2, Model.Contacts);
                    mee.SetCellValue(ws, 5, 4, Model.PhoneNum);
                    mee.SetCellValue(ws, 5, 6, Model.OfficeTel);


                    for (int i = 0; i < DetailsTable.Rows.Count; i++)
                    {
                        DataRow dr = DetailsTable.Rows[i];
                        mee.SetCellValue(ws, 8 + i, 1, i + 1);
                        mee.SetCellValue(ws, 8 + i, 2, Convert.ToString(dr["CylinderCode"]));
                        mee.SetCellValue(ws, 8 + i, 3, Convert.ToString(dr["GasCode"]));
                        mee.SetCellValue(ws, 8 + i, 4, Convert.ToDecimal(dr["AmountGas"]).ToString());
                        if (!Convert.IsDBNull(dr["IsPass"]))
                        {
                            if (Convert.ToString(dr["IsPass"]) == "1")
                            {
                                mee.SetCellValue(ws, 8 + i, 5, "合格");
                            }
                            else if (Convert.ToString(dr["IsPass"]) == "0")
                            {
                                mee.SetCellValue(ws, 8 + i, 5, "不合格");
                            }
                        }

                        if (!Convert.IsDBNull(dr["CheckDate"]))
                        {
                            mee.SetCellValue(ws, 8 + i, 6, Convert.ToDateTime(dr["CheckDate"]).ToShortDateString());
                        }
                    }

                    #endregion


                    string fileName = "入网检测登记temp.xls";
                    string pathFile = physicalPath + fileName;
                    mee.SaveExcel(pathFile);
                    ShowMsgHelper.Alert("导出成功!");

                    FileDownHelper.DownLoadold(@"~/Xls/" + fileName);
                }

                finally
                {
                    mee.Dispose();

                }
            }
        }

        /// <summary>
        /// 送样单
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void SendSampleForm()
        {

            if (Model.OID == 0)
            {
                ShowMsgHelper.Alert_Error("请选择一个记录！");
                return;
            }
            MouldExportExcel mee = new MouldExportExcel();

            try
            {
                string physicalPath = Request.PhysicalApplicationPath + "Xls\\";
                Microsoft.Office.Interop.Excel.Workbook sh = mee.OpenExcel(physicalPath + "入网检测送样单.xls");
                Microsoft.Office.Interop.Excel.Worksheet ws = sh.Worksheets[1] as Microsoft.Office.Interop.Excel.Worksheet;
                // mee.SetCellValue(ws, 4, 3, "1111");
                #region 设置值
                mee.SetCellValue(ws, 4, 3, Model.SampliInspituation);
                mee.SetCellValue(ws, 2, 4, Model.RegistantsDate.Value.ToString("yyyy-MM-dd"));
                mee.SetCellValue(ws, 2, 6, DetailsTable.Rows.Count);

                mee.SetCellValue(ws, 5, 4, Model.InspectionUnit);
                mee.SetCellValue(ws, 7, 4, Model.PhoneNum);
                mee.SetCellValue(ws, 7, 6, Model.OfficeTel);


                EGMNGS.BLL.BookCylinderInfo bllCylinder = new EGMNGS.BLL.BookCylinderInfo();
                for (int i = 0; i < DetailsTable.Rows.Count; i++)
                {
                    int beginIndex = 13;
                    DataRow dr = DetailsTable.Rows[i];
                    //mee.SetCellValue(ws, beginIndex + i, 1, i + 1);
                    mee.SetCellValue(ws, beginIndex + i, 2, "六氟化硫");
                    //获取钢瓶容量

                    if (!Convert.IsDBNull(dr["CylinderCode"]))
                    {
                        string gprl = bllCylinder.GetModelByCylinderCode(Convert.ToString(dr["CylinderCode"])).CylinderCapacity;
                        mee.SetCellValue(ws, beginIndex + i, 3, DictEGMNS["GPRL"][gprl]);
                    }

                    mee.SetCellValue(ws, beginIndex + i, 4, dr["ProduceNum"].ToString());
                    mee.SetCellValue(ws, beginIndex + i, 5, ddlCheckPJType.SelectedItem.Text);
                    mee.SetCellValue(ws, beginIndex + i, 6, dr["Manufacturer"].ToString());

                }

                #endregion


                string fileName = "入网检测送样单temp.xls";
                string pathFile = physicalPath + fileName;
                mee.SaveExcel(pathFile);
                //ShowMsgHelper.Alert("导出成功!");

                FileDownHelper.DownLoadold(@"~/Xls/" + fileName);
            }

            finally
            {
                mee.Dispose();

            }
        }

        /// <summary>
        /// 条形码打印
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnPassPrint_Click(object sender, EventArgs e)
        {
            GetCheckBox();
            var modelList = UpdateList;


            if (modelList.Count == 0)
            {
                return;
            }

            string barCodePath = ConfigurationManager.AppSettings["PassBarCodePathPass"];
            //string passPrinterName = ConfigurationManager.AppSettings["PassPrinterName"];
            //Declare a BarTender application variable 

            BarTender.Application btApp;

            //Declare a BarTender format variable 

            BarTender.Format btFormat;

            //Instantiate a BarTender application variable 

            btApp = new BarTender.Application();

            //Set the BarTender application visible 

            btApp.Visible = true;

            //Open a BarTender label format 

            btFormat = btApp.Formats.Open(barCodePath, false);




            foreach (var item in modelList)
            {

                BookCylinderInfo bookCylinderInfoOBJ = new EGMNGS.BLL.BookCylinderInfo().GetModelByCylinderCode(item.CylinderCode);

                btFormat.SetNamedSubStringValue("Code", item.CylinderCode);
                btFormat.SetNamedSubStringValue("SeaNo", bookCylinderInfoOBJ.CylinderSealNo);
                btFormat.SetNamedSubStringValue("IneUnit", Model.InspectionUnit);
                btFormat.PrintOut(false, false);
            }

            btApp.Quit(BarTender.BtSaveOptions.btDoNotSaveChanges);

            System.Runtime.InteropServices.Marshal.ReleaseComObject(btApp);

            GC.Collect();     // 强制对零代到指定代进行垃圾回收。
        }

    }
}
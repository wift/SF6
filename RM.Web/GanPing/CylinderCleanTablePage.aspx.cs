﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using RM.Common.DotNetCode;
using System.Data;
using RM.Common.DotNetUI;
using RM.Web.App_Code;
using EGMNGS.Common;

namespace RM.Web.GanPing
{
    public partial class CylinderCleanTablePage : PageBase
    {
        #region 属性字段

        EGMNGS.BLL.CylinderCleanTable bll = new EGMNGS.BLL.CylinderCleanTable();
        private DataTable UserList
        {
            get
            {
                return Session["UserList"] as DataTable;
            }
            set { Session["UserList"] = value; }
        }
        private EGMNGS.Model.CylinderCleanTable Model
        {
            get
            {
                if (ViewState["CylinderCleanTable"] == null)
                {
                    return new EGMNGS.Model.CylinderCleanTable();
                }
                return ViewState["CylinderCleanTable"] as EGMNGS.Model.CylinderCleanTable;
            }
            set { ViewState["CylinderCleanTable"] = value; }
        }
        private string actionMode
        {
            get { return ViewState["ActionMode"] as String; }
            set { ViewState["ActionMode"] = value; }
        }
        public string PowerSupplyCode
        {
            get { return ViewState["PowerSupplyCode"] as String; }
            set { ViewState["PowerSupplyCode"] = value; }
        }
        public string PowerSupplyName
        {
            get { return ViewState["PowerSupplyName"] as String; }
            set { ViewState["PowerSupplyName"] = value; }
        }

        #endregion

        #region 事件方法

        protected void Page_Load(object sender, EventArgs e)
        {
            this.PageControl1.pageHandler += new EventHandler(pager_PageChanged);
            if (!IsPostBack)
            {
                UserList = ComServies.GetAllUserInfo();

                DropDownListBinder();

                txtCylinderCode.Focus();
            }
        }

        /// <summary>
        /// 绑定下拉列
        /// </summary>
        private void DropDownListBinder()
        {
            EGMNGS.BLL.SYS_CODE sysCodeBll = new EGMNGS.BLL.SYS_CODE();
            DataSet dtCFStatus = sysCodeBll.GetList("CODE_TYPE='GPStatus' ORDER BY DISPLAY_ORDER");

            this.ddlCFStatus.DataSource = dtCFStatus.Tables[0];
            this.ddlCFStatus.DataTextField = "CODE_CHI_DESC";
            this.ddlCFStatus.DataValueField = "CODE";
            this.ddlCFStatus.DataBind();

            this.ddlCFStatus.Items.Insert(0, string.Empty);
        }


        /// <summary>
        /// 绑定后激发事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void rp_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Label lblClearManoid = e.Item.FindControl("lblClearManoid") as Label;
                if (lblClearManoid != null)
                {
                    lblClearManoid.Text = GetUserName(lblClearManoid.Text);
                }

                Label lblStatus = e.Item.FindControl("lblStatus") as Label;
                if (lblStatus != null)
                {
                    if (lblStatus.Text.Length > 0)
                    {
                        lblStatus.Text = this.ddlCFStatus.Items.FindByValue(lblStatus.Text).Text;

                    }
                }
            }
        }

        /// <summary>
        /// 绑定数据，分页
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void pager_PageChanged(object sender, EventArgs e)
        {
            DataBindGrid();
        }

        /// <summary>
        /// 绑定数据源
        /// </summary>
        private void DataBindGrid()
        {
            int count = bll.GetRecordCount("1=1");
            DataSet ds = bll.GetListByPage("1=1", "ClearDate desc", PageControl1.PageIndex, PageControl1.PageSize);
            ControlBindHelper.BindRepeaterList(ds.Tables[0], rp_Item);
            this.PageControl1.RecordCount = Convert.ToInt32(count);
        }
        /// <summary>
        /// 绑定数据到对象
        /// </summary>
        private void DataBinder()
        {
            Model.CylinderCode = this.txtCylinderCode.Text;
            Model.RemarksDesc = this.txtRemarksDesc.Text;
        }

        /// <summary>
        /// 初始化控件
        /// </summary>
        private void InitControl()
        {
            txtCylinderCode.Focus();
            this.txtCylinderCode.Text = Model.CylinderCode;
            this.txtRemarksDesc.Text = Model.RemarksDesc;
            this.ddlCFStatus.SelectedValue = Model.Status;
            this.txtClearManOID.Text = GetUserName(Model.ClearManOID);
            this.txtClearDate.Text = Model.ClearDate == null ? string.Empty : Model.ClearDate.Value.ToShortDateString();
        }

        /// <summary>
        /// 获取用户名
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        private string GetUserName(string userId)
        {
            DataRow[] dr = UserList.Select(string.Format("User_ID='{0}'", userId));
            if (dr.Length > 0)
            {
                return dr[0][1].ToString();
            }
            return string.Empty;
        }

        private void ClearControl()
        {
            Model = new EGMNGS.Model.CylinderCleanTable();
            InitControl();
        }

        #endregion

        #region 按钮事件

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            //1 钢瓶抽空清洗后，台帐信息同步更新“状态”。
            //  Model.Status = "1";
            if (ComServies.UpdateWithStatusCylinderCleanTable(Model.CylinderCode))
            {
                ShowMsgHelper.Alert("提交成功！");
                InitControl();
                DataBindGrid();
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            DataBindGrid();
        }

        /// <summary>
        /// 获取当前对象
        /// </summary>
        private void ShowMaxMode()
        {
            Model = bll.GetModel(bll.GetMaxId() - 1);
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                if (actionMode == "Add")
                {
                    DataBinder();
                    Model.ClearManOID = User.UserId.ToString();

                    int count = new EGMNGS.BLL.BookCylinderInfo().GetRecordCount(string.Format("CylinderCode='{0}'", Model.CylinderCode));
                    if (count == 0)
                    {
                        ShowMsgHelper.showWarningMsg("此钢瓶不存在！");
                        return;
                    }

                    if (bll.Add(Model) > 0)
                    {
                        actionMode = string.Empty;
                    }

                    DataBindGrid();
                    ShowMaxMode();
                }
                else if (actionMode == "Edit")
                {
                    if (bll.GetModel(Model.OID).Status == "1")
                    {
                        ShowMsgHelper.Alert_Wern("“已确认”状态无法修改数据！");
                        return;
                    }

                    DataBinder();
                    if (bll.Update(Model))
                    {
                        EGMNSShowMsg.ShowEditMsgSuccess();
                        actionMode = string.Empty;
                    }
                    DataBindGrid();
                }
            }
            catch (ArgumentOutOfRangeException ex)
            {
                string[] errs = CommonHelper.GetValue(ex.Message, "@").Split(':');
                ShowMsgHelper.ExecuteScript(string.Format("ChangeCss($('#txt{0}'),'长度不能超{1}字')", errs[0], errs[1]));
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            actionMode = "Add";
            Model = new EGMNGS.Model.CylinderCleanTable();
            Model.ClearDate = DateTime.Now;
            Model.ClearManOID = User.UserId.ToString();
            Model.Status = "0";
            InitControl();
        }


        protected void btnDel_Click(object sender, EventArgs e)
        {
            if (bll.GetModel(Model.OID).Status == "1")
            {
                ShowMsgHelper.Alert_Wern("“已确认”状态不可以删除");
            }
            else
            {
                bll.Delete(Model.OID);
                Model = new EGMNGS.Model.CylinderCleanTable();
                InitControl();
                DataBindGrid();
                ShowMsgHelper.Alert("删除成功！");
            }
        }

        protected void rp_Item_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            int oid = Convert.ToInt32(e.CommandArgument);

            Model = bll.GetModel(oid);
            InitControl();
            actionMode = "Edit";
        }
        #endregion
    }
}
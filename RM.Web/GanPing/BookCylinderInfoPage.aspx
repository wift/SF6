﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BookCylinderInfoPage.aspx.cs"
    Inherits="RM.Web.GanPing.BookCylinderInfoPage" EnableEventValidation="false"
    ClientIDMode="AutoID" %>

<%@ Register Src="~/UserControl/PageControl.ascx" TagName="PageControl" TagPrefix="uc1" %>
<%@ Register Src="~/UserControl/LoadButton.ascx" TagName="LoadButton" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>钢瓶台账登记</title>
    <link href="/Themes/Styles/Site.css" rel="stylesheet" type="text/css" />
    <script src="/Themes/Scripts/Validator/JValidator.js" type="text/javascript"></script>
    <script src="/Themes/Scripts/jquery-1.8.2.min.js" type="text/javascript"></script>
    <script src="/Themes/Scripts/jquery.pullbox.js" type="text/javascript"></script>
    <script src="/Themes/Scripts/FunctionJS.js" type="text/javascript"></script>
    <script src="/Themes/Scripts/DatePicker/WdatePicker.js" type="text/javascript"></script>
    <script type="text/javascript">
        //添加
        function add() {
            document.getElementById('<%= this.btnAdd.ClientID %>').click();
        }
        //查询
        function Find() {

            document.getElementById('<%= this.btnSearch.ClientID %>').click();
        }
        //删除
        function Delete() {
            var key = CheckboxValue1();
            showConfirmMsg("此操作不可恢复，您确定要删除吗？", function (r) {
                if (r) {
                    document.getElementById('<%= this.HiddenField1.ClientID %>').value = key.toString();
                    document.getElementById('<%= this.btnDel.ClientID %>').click();
                }
            });
        }
        //保存
        function SaveForm() {
            document.getElementById('<%= this.btnSave.ClientID %>').click();
        }
        //提交
        function Submit() {
            document.getElementById('<%= this.btnSubmit.ClientID %>').click();
        }

        $(function () {
            InitControl();
        

            
        })




        function InitControl() {
            $(".div-body").PullBox({ dv: $(".div-body"), obj: $("#table1").find("tr") });
            divresize(140);
          //  FixedTableHeader("#table1", $(window).height() - 200);
          

            $('#<%= this.ddlManufacturer.ClientID %>').change(function myfunction() {
                if ($(this).val() == 2) {

                    $('#<%= this.txtManufactureDate.ClientID %>').attr("checkexpession", "");


                }
                else {
                    $('#<%= this.txtManufactureDate.ClientID %>').attr("checkexpession", "NotNull");
                }
            });



        }

        function printer() {
            document.getElementById('<%= this.btnPrint.ClientID %>').click();
        }

        function SuperSave() {

            document.getElementById('<%= this.btnSuperSave.ClientID %>').click();
        }
        function changedate() {
            var mh;

            if ($dp.cal.date.M < 10) {
                mh = "0" + $dp.cal.date.M.toString();
            }
            else {
                mh = $dp.cal.date.M.toString();
            }
            var dd;
            if ($dp.cal.date.d < 10) {
                dd = "0" + $dp.cal.date.d.toString();
            }
            else {
                dd = $dp.cal.date.d.toString();
            }
            var d = ($dp.cal.date.y + 3).toString() + "-" + mh + "-" + dd;

            $('#<%= this.txtEffectiveDate.ClientID %>').val(d);
        }

        //        function getCurrentDate() {
        //            var d = new Date();
        //            var year = d.getFullYear();
        //            var mon = d.getMonth() + 1;
        //            var day = d.getDate();

        //            s = year + "-" + (mon < 10 ? "0" + mon : mon) + "-" + (day < 10 ? "0" + day : day);
        //            return s;
        //        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:HiddenField ID="HiddenField1" runat="server" />
            <table width="100%">
                <colgroup>
                    <col width="11%" />
                    <col width="11%" />
                    <col width="11%" />
                    <col width="11%" />
                    <col width="11%" />
                    <col width="11%" />
                    <col width="11%" />
                    <col width="11%" />
                </colgroup>
                <tr>
                    <td class="inner_cell_right">
                        钢瓶编码：
                    </td>
                    <td>
                        <asp:TextBox ID="txtCylinderCode" runat="server" checkexpession="NotNull" datacol="yes"
                            err="钢瓶编码" />
                    </td>
                    <td class="inner_cell_right">
                        钢瓶钢印号：
                    </td>
                    <td>
                        <asp:TextBox ID="txtCylinderSealNo" runat="server" datacol="yes" err="此项" checkexpession="NotNull" />
                    </td>
                    <td class="inner_cell_right">
                        有效日期：
                    </td>
                    <td>
                        <asp:TextBox ID="txtEffectiveDate" runat="server" Enabled="false" />
                    </td>
                    <td class="inner_cell_right">
                        登记人ID：
                    </td>
                    <td>
                        <asp:TextBox ID="txtRegistantsOID" runat="server" Enabled="false" />
                    </td>
                </tr>
                <tr>
                    <td class="inner_cell_right">
                        自编编号：
                    </td>
                    <td>
                        <asp:TextBox ID="txtCylinderSourse" runat="server" />
                    </td>
                    <td class="inner_cell_right">
                        钢瓶容量（kg）：
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlCylinderCapacity" runat="server" Width="100%" datacol="yes"
                            err="此项" checkexpession="NotNull" class="select">
                        </asp:DropDownList>
                    </td>
                    <td class="inner_cell_right">
                        检定日期：
                    </td>
                    <td>
                        <asp:TextBox ID="txtManufactureDate" runat="server" onfocus="WdatePicker({dchanged:changedate})"
                            datacol="yes" err="此项" checkexpession="NotNull" />
                    </td>
                    <td class="inner_cell_right">
                        登记日期：
                    </td>
                    <td>
                        <asp:TextBox ID="txtRegistantsDate" runat="server" Enabled="false" />
                    </td>
                </tr>
                <tr>
                    <td class="inner_cell_right">
                        所属单位：
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlManufacturer" runat="server" Width="100%" err="此项" checkexpession="NotNull"
                            class="select">
                        </asp:DropDownList>
                    </td>
                    <td class="inner_cell_right">
                        当前气体编码：
                    </td>
                    <td>
                        <asp:TextBox ID="txtCurGasCode" runat="server" Enabled="false" />
                    </td>
                    <td class="inner_cell_right">
                        状态：
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlCFStatus" runat="server" Width="100%" class="selected">
                        </asp:DropDownList>
                    </td>
                    <td>
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
            </table>
            <div style="text-align: left;">
                <uc2:LoadButton ID="LoadButton1" runat="server" />
            </div>
            <div style="text-align: right; display: none;">
                <asp:Button ID="btnSubmit" runat="server" OnClick="btnSubmit_Click" Text="btnSubmit_Click" />
                <asp:Button ID="btnSearch" runat="server" OnClick="btnSearch_Click" Text="btnSearch_Click" />
                <asp:Button ID="btnSave" runat="server" OnClick="btnSave_Click" Text="btnSave_Click"
                    OnClientClick="return CheckDataValid('#form1');" />
                <asp:Button ID="btnAdd" runat="server" OnClick="btnAdd_Click" Text="btnAdd_Click" />
                <asp:Button ID="btnDel" runat="server" OnClick="btnDel_Click" Text="btnDel_Click" />
                <asp:Button ID="btnPrint" runat="server" OnClick="btnPrint_Click" />
                <asp:Button ID="btnSuperSave" runat="server" OnClick="btnSuperSave_Click" OnClientClick="return CheckDataValid('#form1');" />
            </div>
            <div class="div-body">
                <table id="table1" class="grid">
                    <colgroup>
                        <col width="5%" />
                        <col width="15%" />
                        <col width="10%" />
                        <col width="15%" />
                        <col width="15%" />
                        <col width="10%" />
                        <col width="10%" />
                        <col width="10%" />
                        <col width="10%" />
                    </colgroup>
                    <thead>
                        <tr>
                            <td>
                                <label id="checkAllOff" onclick="CheckAllLine1()" title="全选">
                                    &nbsp;</label>
                            </td>
                            <td style="text-align: center;">
                                钢瓶编码
                            </td>
                            <td style="text-align: center;">
                                所属单位
                            </td>
                            <td style="text-align: center;">
                                钢瓶钢印号
                            </td>
                            <td style="text-align: center;">
                                钢瓶容量（kg）
                            </td>
                            <td style="text-align: center;">
                                有效日期
                            </td>
                            <td style="text-align: center;">
                                当前气体编码
                            </td>
                            <td style="text-align: center;">
                                状态
                            </td>
                        </tr>
                    </thead>
                    <tbody>
                        <asp:Repeater ID="rp_Item" runat="server" OnItemDataBound="rp_ItemDataBound" OnItemCommand="rp_Item_ItemCommand">
                            <ItemTemplate>
                                <tr id="row" runat="server">
                                    <td style="text-align: center;">
                                        <asp:CheckBox ID="checkbox" runat="server" ToolTip='<%#Eval("OID")%>' />
                                    </td>
                                    <td style="text-align: center;">
                                        <asp:LinkButton ID="LBtnDel" runat="server" CommandArgument='<%#Eval("OID") %>' CommandName="del"
                                            Text='<%#Eval("CylinderCode")%>' OnClientClick="selectrow(this)"></asp:LinkButton>
                                    </td>
                                    <td style="text-align: center;">
                                        <%# DictEGMNS["BelongUnit"][Eval("Manufacturer").ToString()]%>
                                    </td>
                                    <td style="text-align: center;">
                                        <asp:Label ID="lblCylinderSealNo" runat="server" Text='<%#Eval("CylinderSealNo")%>'></asp:Label>
                                    </td>
                                    <td style="text-align: center;">
                                        <asp:Label ID="lblCylinderCapacity" runat="server" Text='<%#Eval("CylinderCapacity")%>'></asp:Label>
                                    </td>
                                    <td style="text-align: center;">
                                        <%#Eval("EffectiveDate", "{0:yyyy-MM-dd}")%>
                                    </td>
                                    <td style="text-align: center;">
                                        <%#Eval("CurGasCode")%>
                                    </td>
                                    <td style="text-align: center;">
                                        <asp:Label ID="lblStatus" runat="server" Text='<%#Eval("Status")%>'></asp:Label>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </tbody>
                </table>
            </div>
            <uc1:PageControl ID="PageControl1" runat="server" />
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnAdd" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="rp_Item" EventName="ItemCommand" />
        </Triggers>
    </asp:UpdatePanel>
    </form>
</body>
</html>

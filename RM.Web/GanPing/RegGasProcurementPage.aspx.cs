﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using RM.Common.DotNetCode;
using System.Data;
using RM.Common.DotNetUI;
using RM.Web.App_Code;
using EGMNGS.Common;
using EGMNGS.Model;
namespace RM.Web.GanPing
{
    public partial class RegGasProcurementPage : PageBase
    {
        #region 属性字段

        EGMNGS.BLL.RegGasProcurement bll = new EGMNGS.BLL.RegGasProcurement();
        EGMNGS.BLL.DetailsGasProcurement DetailsBLL = new EGMNGS.BLL.DetailsGasProcurement();


        /// <summary>
        /// 需要添加的数据
        /// </summary>
        private Decimal? TotalAmountGas
        {
            get
            {
                if (ViewState["Decimal"] == null)
                {
                    return new Decimal(); ;
                }
                return ViewState["Decimal"] as Decimal?;
            }
            set { ViewState["Decimal"] = value; }
        }
        private DataTable UserList
        {
            get
            {
                return ViewState["UserList"] as DataTable;
            }
            set { ViewState["UserList"] = value; }
        }
        private EGMNGS.Model.RegGasProcurement Model
        {
            get
            {
                if (ViewState["RegGasProcurement"] == null)
                {
                    return new EGMNGS.Model.RegGasProcurement();
                }
                return ViewState["RegGasProcurement"] as EGMNGS.Model.RegGasProcurement;
            }
            set { ViewState["RegGasProcurement"] = value; }
        }
        private string actionMode
        {
            get { return ViewState["ActionMode"] as String; }
            set { ViewState["ActionMode"] = value; }
        }
        public string PowerSupplyCode
        {
            get { return ViewState["PowerSupplyCode"] as String; }
            set { ViewState["PowerSupplyCode"] = value; }
        }
        public string PowerSupplyName
        {
            get { return ViewState["PowerSupplyName"] as String; }
            set { ViewState["PowerSupplyName"] = value; }
        }
        public DataTable DetailsTable
        {
            get
            {
                if (ViewState["DetailsTable"] == null)
                {
                    return new DataTable();
                }
                return ViewState["DetailsTable"] as DataTable;
            }
            set { ViewState["DetailsTable"] = value; }
        }

        private List<DetailsGasProcurement> UpdateList
        {
            get
            {
                if (ViewState["UpdateList"] == null)
                {
                    return new List<DetailsGasProcurement>();
                }
                return ViewState["UpdateList"] as List<DetailsGasProcurement>;
            }
            set { ViewState["UpdateList"] = value; }
        }
        /// <summary>
        /// 需要添加的数据
        /// </summary>
        private List<DetailsGasProcurement> AddList
        {
            get
            {
                if (ViewState["AddList"] == null)
                {
                    return new List<DetailsGasProcurement>();
                }
                return ViewState["AddList"] as List<DetailsGasProcurement>;
            }
            set { ViewState["AddList"] = value; }
        }
        private string actionModeDetails
        {
            get
            {
                if (ViewState["actionModeDetails"] == null)
                {
                    return string.Empty;
                }
                return ViewState["actionModeDetails"] as String;
            }
            set { ViewState["actionModeDetails"] = value; }
        }

        private string HSID
        {
            get
            {
                if (ViewState["actionModeDetails"] == null)
                {
                    return string.Empty;
                }
                return ViewState["actionModeDetails"] as String;
            }
            set { ViewState["actionModeDetails"] = value; }
        }

        private bool? IsException
        {
            get
            {
                if (ViewState["IsException"] == null)
                {
                    return false;
                }

                return ViewState["IsException"] as bool?;
            }
            set { ViewState["IsException"] = value; }
        }
        #endregion

        #region 事件方法

        protected void Page_Load(object sender, EventArgs e)
        {
            this.PageControl1.pageHandler += new EventHandler(pager_PageChanged);

            if (!IsPostBack)
            {

                UserList = ComServies.GetAllUserInfo();
                string[] orgIdOrgName = ComServies.GetPowerApplyByUserID(User.UserId.ToString());
                PowerSupplyCode = orgIdOrgName[0];
                PowerSupplyName = orgIdOrgName[1];
                DropDownListBinder();
            }


        }




        /// <summary>
        /// 绑定下拉列
        /// </summary>
        private void DropDownListBinder()
        {

            EGMNGS.BLL.SYS_CODE sysCodeBll = new EGMNGS.BLL.SYS_CODE();
            DataSet dsYear = sysCodeBll.GetList("CODE_TYPE='Year' ORDER BY DISPLAY_ORDER");

            this.ddlYear.DataSource = dsYear.Tables[0];
            this.ddlYear.DataTextField = "CODE_CHI_DESC";
            this.ddlYear.DataValueField = "CODE";
            this.ddlYear.DataBind();

            DataSet dsMonth = sysCodeBll.GetList("CODE_TYPE='Month' ORDER BY DISPLAY_ORDER");

            this.ddlMonth.DataSource = dsMonth.Tables[0];
            this.ddlMonth.DataTextField = "CODE_CHI_DESC";
            this.ddlMonth.DataValueField = "CODE";
            this.ddlMonth.DataBind();

            DataSet dtCFStatus = sysCodeBll.GetList("CODE_TYPE='CheckStatus' ORDER BY DISPLAY_ORDER");

            this.ddlStatus.DataSource = dtCFStatus.Tables[0];
            this.ddlStatus.DataTextField = "CODE_CHI_DESC";
            this.ddlStatus.DataValueField = "CODE";
            this.ddlStatus.DataBind();

            this.ddlStatus.Items.Insert(0, string.Empty);

        }


        /// <summary>
        /// 绑定后激发事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void rp_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Label lblUserID = e.Item.FindControl("lblUserID") as Label;
                Label lblRecycleroid = e.Item.FindControl("lblRecycleroid") as Label;
                if (lblUserID != null)
                {
                    lblUserID.Text = GetUserName(lblUserID.Text);
                }
                if (lblRecycleroid != null)
                {
                    lblRecycleroid.Text = GetUserName(lblRecycleroid.Text);
                }

                Label lblStatus = e.Item.FindControl("lblStatus") as Label;

                if (lblStatus != null)
                {
                    if (lblStatus.Text.Length > 0)
                    {
                        lblStatus.Text = this.ddlStatus.Items.FindByValue(lblStatus.Text).Text;

                    }
                }
            }
        }

        /// <summary>
        /// 绑定数据，分页
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void pager_PageChanged(object sender, EventArgs e)
        {
            DataBindGrid();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void pager_PageChangedDetails(object sender, EventArgs e)
        {
            GetDetailsGasProcurement();
        }
        /// <summary>
        /// 绑定数据源
        /// </summary>
        private void DataBindGrid()
        {
            int count = bll.GetRecordCount("1=1");
            DataSet ds = bll.GetListByPage("1=1", string.Empty, PageControl1.PageIndex, PageControl1.PageSize);
            ControlBindHelper.BindRepeaterList(ds.Tables[0], rp_Item);
            this.PageControl1.RecordCount = Convert.ToInt32(count);
        }

        /// <summary>
        /// 绑定数据到对象
        /// </summary>
        private void DataBinder()
        {
            Model.Code = this.txtCode.Text;
            Model.Year = this.ddlYear.SelectedValue;
            Model.Month = this.ddlMonth.SelectedValue;
            Model.SupplierName = this.txtSupplierName.Text;
            Model.ManufacturerName = this.txtManufacturerName.Text;
            if (this.txtAmountPurchased.Text.Trim().Length > 0)
            {
                Model.AmountPurchased = decimal.Parse(this.txtAmountPurchased.Text);
            }

            Model.PurchasedDesc = this.txtPurchasedDesc.Text; ;
            Model.ContactsName = this.txtContacts.Text;
            Model.OfficeTel = this.txtOfficeTel.Text;
            Model.PhoneNum = this.txtPhoneNum.Text;
        }

        /// <summary>
        /// 初始化控件
        /// </summary>
        private void InitControl()
        {
            this.txtCode.Text = Model.Code;
            this.ddlYear.SelectedValue = Model.Year;
            this.ddlMonth.SelectedValue = Model.Month;
            this.txtSupplierName.Text = Model.SupplierName;
            this.txtManufacturerName.Text = Model.ManufacturerName;
            this.txtAmountPurchased.Text = Model.AmountPurchased.ToString();
            this.txtPurchasedDesc.Text = Model.PurchasedDesc;
            this.txtContacts.Text = Model.ContactsName;
            this.txtOfficeTel.Text = Model.OfficeTel;
            this.txtPhoneNum.Text = Model.PhoneNum;
            this.ddlStatus.SelectedValue = Model.Status;
            this.txtPurchasedID.Text = GetUserName(Model.PurchasedID);
            if (Model.PurchasedDate != null)
            {
                this.txtPurchasedDate.Text = Model.PurchasedDate.Value.ToShortDateString();
            }

            this.txtRegistrantOID.Text = GetUserName(Model.RegistrantOID);
            if (Model.RegistrantDate != null)
            {
                this.txtRegistrantDate.Text = Model.RegistrantDate.Value.ToShortDateString();

            }

            this.txtPurchasedID.Text = GetUserName(Model.PurchasedID);
        }

        /// <summary>
        /// 获取用户名
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        private string GetUserName(string userId)
        {
            DataRow[] dr = UserList.Select(string.Format("User_ID='{0}'", userId));
            if (dr.Length > 0)
            {
                return dr[0][1].ToString();
            }
            return string.Empty;
        }
        #endregion

        #region 按钮事件

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            if (Model.Status == "1")
            {
                ShowMsgHelper.Alert_Wern("无法重新提交！");
                return;
            }

            if (string.IsNullOrEmpty(Model.Code))
            {
                ShowMsgHelper.Alert_Wern("请选中！");
                return;
            }

            if (CheckGPCode() == false)
            {
                ShowMsgHelper.Alert_Error("钢瓶编码不能为空!");
                return;
            }


            Model.Status = "1";
            Model.AmountPurchased = TotalAmountGas;
            if (bll.Update(Model))
            {
                List<DetailsGasProcurement> listDetials = DetailsBLL.GetModelList(string.Format("RegGasProcurement_Code='{0}'", Model.Code));
                var fillBll = new EGMNGS.BLL.BookGasFill();
                foreach (DetailsGasProcurement item in listDetials)
                {
                    item.GasCode = ComServies.GetCode("QT", DateTime.Now.Year.ToString() +

DateTime.Now.Month.ToString("D2"));

                    DetailsBLL.Update(item);

                    ComServies.UpdateCylinderInfo(item.CylinderCode, item.GasCode);

                    fillBll.Add(new BookGasFill(GlobalConstants.ActionModeAdd) { AmountGas = item.AmountGas, BusinessCode = Model.Code, CylinderCode = item.CylinderCode, GasCode = item.GasCode, Month = Model.Month, Year = Model.Year, GasType = "1", RegistrantDate = DateTime.Now, RegistrantOID = User.UserId.ToString(), CylinderStatus = "3", Status = "1", GasStatus = "0" });

                }
                ShowMsgHelper.Alert("提交成功！");
                InitControl();
                DataBindGrid();
                GetDetailsGasProcurement();
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            DataBindGrid();
        }

        /// <summary>
        /// 获取当前对象
        /// </summary>
        private void ShowMaxMode()
        {
            Model = bll.GetModel(bll.GetMaxId() - 1);
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                if (actionMode == "Add")
                {
                    DataBinder();

                    int count = bll.GetRecordCount(string.Format("Code='{0}'", Model.Code));
                    if (count > 0)
                    {
                        ShowMsgHelper.showWarningMsg("回收编号已经存在!");
                        return;
                    }

                    if (bll.Add(Model) > 0)
                    {
                        actionMode = string.Empty;
                    }

                    DataBindGrid();
                    ShowMaxMode();
                    GetDetailsGasProcurement();
                }
                else if (actionMode == "Edit")
                {
                    if (bll.GetModel(Model.OID).Status != "0")
                    {
                        ShowMsgHelper.Alert_Wern("“已确认”状态无法修改数据！");
                        return;
                    }


                    DataBinder();
                    if (bll.Update(Model))
                    {
                        EGMNSShowMsg.ShowEditMsgSuccess();
                        actionMode = string.Empty;
                    }
                    DataBindGrid();
                }
            }
            catch (ArgumentOutOfRangeException ex)
            {
                string[] errs = CommonHelper.GetValue(ex.Message, "@").Split(':');
                ShowMsgHelper.ExecuteScript(string.Format("ChangeCss($('#txt{0}'),'长度不能超{1}字')", errs[0], errs[1]));
            }
        }
        private void ClearControl()
        {
            Model = new RegGasProcurement();
            InitControl();
        }
        protected void btnAdd_Click(object sender, EventArgs e)
        {
            actionMode = "Add";
            Model = new EGMNGS.Model.RegGasProcurement(actionMode);
            Model.PurchasedDate = Model.RegistrantDate = DateTime.Now;
            Model.PurchasedID = Model.RegistrantOID = User.UserId.ToString();
            Model.Status = "0";
            InitControl();

            GetDetailsGasProcurement();
        }


        protected void btnDel_Click(object sender, EventArgs e)
        {
            if (bll.GetModel(Model.OID).Status != "0")
            {
                ShowMsgHelper.Alert_Wern("“已确认”状态不可以删除");
            }
            else
            {
                bll.Delete(Model.OID);
                Model = new EGMNGS.Model.RegGasProcurement();
                InitControl();
                DataBindGrid();
                ShowMsgHelper.Alert("删除成功！");
            }
        }

        protected void rp_Item_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            int oid = Convert.ToInt32(e.CommandArgument);

            Model = bll.GetModel(oid);

            actionMode = "Edit";
            InitControl();

            //绑定明细表
            GetDetailsGasProcurement();
        }
        #endregion

        #region Details

        private void GetDetailsGasProcurement()
        {
            DetailsTable = DetailsBLL.GetList(string.Format("RegGasProcurement_Code='{0}'", Model.Code)).Tables[0];
            ControlBindHelper.BindRepeaterList(DetailsTable, rptDetails);
        }
        protected void btnAddDetails_Click(object sender, EventArgs e)
        {
            if (Model.Status == null || Model.Status == "1")
            {
                ShowMsgHelper.Alert_Error("记录已经提交！");
                return;
            }
            //首先，恢复数据源
            if (IsRetriRepeter())
            {
                IsException = true;
                return;
            }
            DataTable dt = DetailsTable;
            DataRow row = dt.NewRow();
            row[2] = string.Empty;
            row[3] = string.Empty;
            row[4] = 0.0;
            row[5] = string.Empty;
            row[6] = DateTime.Now.ToShortDateString();
            dt.Rows.Add(row);

            rptDetails.DataSource = dt;
            rptDetails.DataBind();
            actionModeDetails = "DetailsAdd";

        }
        private bool IsRetriRepeter()
        {
            bool flag = false;

            for (int i = 0; i < this.rptDetails.Items.Count; i++)
            {
                TextBox txtAmountGas = this.rptDetails.Items[i].FindControl("txtAmountGas") as TextBox;

                TextBox txtCylinderCode = this.rptDetails.Items[i].FindControl("txtCylinderCode") as TextBox;
                if (txtCylinderCode.Text.Length == 0)
                {
                    continue;
                }
                var modeCylinderInfo = new EGMNGS.BLL.BookCylinderInfo().GetModelByCylinderCode(txtCylinderCode.Text);

                if (modeCylinderInfo == null)
                {
                    ShowMsgHelper.Alert_Wern(string.Format("钢瓶{0}不存在!", txtCylinderCode.Text));
                    flag = true;
                    break;
                }
                string cylinderCapacity = DictEGMNS["GPRL"][modeCylinderInfo.CylinderCapacity];

                if (modeCylinderInfo.Status != "1")//没有判断钢瓶状态=“已抽空清洗”
                {
                    ShowMsgHelper.Alert_Wern(string.Format("钢瓶{0}不是已抽空清洗!", txtCylinderCode.Text));
                    flag = true;
                    break;
                }
                if (Convert.ToDecimal(cylinderCapacity) < Convert.ToDecimal(txtAmountGas.Text))
                {
                    ShowMsgHelper.Alert_Wern(string.Format("{0}钢瓶的容量为：{1}kg", txtCylinderCode.Text, cylinderCapacity));
                    flag = true;
                    break;
                }
                DetailsTable.Rows[i][4] = Convert.ToDecimal(txtAmountGas.Text);
                DetailsTable.Rows[i][2] = txtCylinderCode.Text;
            }

            return flag;
        }
        protected void btnDelDetails_Click(object sender, EventArgs e)
        {
            try
            {
                if (Model.Status == null || Model.Status == "1")
                {
                    ShowMsgHelper.Alert("记录已经提交！");
                    return;
                }

                string strID = HiddenField1.Value;

                if (strID.Length > 0)
                {
                    DetailsBLL.DeleteList(strID);
                    ShowMsgHelper.Alert("删除成功！");
                    GetDetailsGasProcurement();
                }
            }
            catch (Exception ex)
            {
                ShowMsgHelper.Alert("删除失败！" + ex.Message);
                throw ex;
            }
        }

        protected void btnSaveDetails_Click(object sender, EventArgs e)
        {
            try
            {
                if (Model.Status == null || Model.Status == "1")
                {
                    ShowMsgHelper.Alert_Error("记录已经提交！");
                    return;
                }
                if (IsRetriRepeter())
                {
                    return;
                }
                GetCheckBox();
                if (actionModeDetails.Equals("DetailsAdd"))
                {
                    if (AddList.Count > 0)
                    {
                        //批量添加
                        foreach (var item in AddList)
                        {
                            DetailsBLL.Add(item);
                        }
                        AddList.Clear();
                    }

                    ShowMsgHelper.Alert("添加成功！");
                    actionModeDetails = string.Empty;

                }

                if (UpdateList.Count > 0)
                {
                    //批量修改
                    foreach (var item in UpdateList)
                    {
                        DetailsBLL.Update(item);
                    }
                    UpdateList.Clear();
                }

                GetDetailsGasProcurement();
            }
            catch (Exception ex)
            {
                ShowMsgHelper.AlertMsg("添加失败！" + ex.Message);
            }
        }

        protected void rptDetails_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {

                CheckBox cb = e.Item.FindControl("checkbox") as CheckBox;
                if (cb.ToolTip.Length == 0)
                {
                    cb.Checked = true;

                    TextBox tb = e.Item.FindControl("txtCylinderCode") as TextBox;
                    tb.Focus();
                }

            }
        }


        /// <summary>
        /// 
        /// </summary>
        private void GetCheckBox()
        {
            UpdateList = new List<DetailsGasProcurement>();
            AddList = new List<DetailsGasProcurement>();

            for (int i = 0; i < this.rptDetails.Items.Count; i++)
            {
                System.Web.UI.WebControls.CheckBox checkbox = (System.Web.UI.WebControls.CheckBox)rptDetails.Items[i].FindControl("checkbox");

                TextBox txtCylinderCode = this.rptDetails.Items[i].FindControl("txtCylinderCode") as TextBox;
                if (txtCylinderCode.Text.Trim().Length == 0)
                {
                    continue;
                }

                if (checkbox.Checked == true && checkbox.ToolTip.Length > 0)
                {
                    // TextBox txtCylinderCode = this.rptDetails.Items[i].FindControl("txtCylinderCode") as TextBox;
                    // TextBox txtGasCode = this.rptDetails.Items[i].FindControl("txtGasCode") as TextBox;
                    TextBox txtAmountGas = this.rptDetails.Items[i].FindControl("txtAmountGas") as TextBox;

                    DetailsGasProcurement DetailsGasProcurementObj = new EGMNGS.BLL.DetailsGasProcurement().GetModel(int.Parse(checkbox.ToolTip));
                    // DetailsGasProcurementObj.OID = int.Parse(checkbox.ToolTip);
                    // DetailsGasProcurementObj.RegGasProcurement_Code = Model.Code;
                    DetailsGasProcurementObj.CylinderCode = txtCylinderCode.Text;
                    if (txtAmountGas.Text.Trim().Length > 0)
                    {
                        DetailsGasProcurementObj.AmountGas = decimal.Parse(txtAmountGas.Text);
                    }
                    UpdateList.Add(DetailsGasProcurementObj);
                }
                else if (checkbox.Checked == true && checkbox.ToolTip.Length == 0)
                {
                    //  TextBox txtCylinderCode = this.rptDetails.Items[i].FindControl("txtCylinderCode") as TextBox;
                    //TextBox txtGasCode = this.rptDetails.Items[i].FindControl("txtGasCode") as TextBox;
                    TextBox txtAmountGas = this.rptDetails.Items[i].FindControl("txtAmountGas") as TextBox;

                    DetailsGasProcurement DetailsGasProcurementObj = new EGMNGS.Model.DetailsGasProcurement();
                    DetailsGasProcurementObj.RegGasProcurement_Code = Model.Code;
                    DetailsGasProcurementObj.CylinderCode = txtCylinderCode.Text;
                    if (txtAmountGas.Text.Trim().Length > 0)
                    {
                        DetailsGasProcurementObj.AmountGas = decimal.Parse(txtAmountGas.Text);
                    }

                    AddList.Add(DetailsGasProcurementObj);
                }
            }
        }

        private bool CheckGPCode()
        {
            TotalAmountGas = new Decimal();

            for (int i = 0; i < this.rptDetails.Items.Count; i++)
            {
                TextBox txtAmountGastemp = this.rptDetails.Items[i].FindControl("txtAmountGas") as TextBox;
                TotalAmountGas += Convert.ToDecimal(txtAmountGastemp.Text);

                TextBox cylinderCode = this.rptDetails.Items[i].FindControl("txtCylinderCode") as TextBox;
                if (cylinderCode.Text.Trim().Length == 0)
                {
                    return false;
                }
            }

            return true;
        }
        #endregion

        //protected void TextChanged(object sender, EventArgs e)
        //{
        //    DetailsTable.Rows[DetailsTable.Rows.Count - 1][2] = (sender as TextBox).Text;
        //   // btnAddDetails_Click(null, null);
        //}


    }
}
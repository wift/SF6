﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using RM.Common.DotNetUI;
using System.Text;
using RM.Web.App_Code;
using EGMNGS.Common;
using Microsoft.Office.Interop.Excel;
using RM.Common.DotNetFile;
using EGMNGS.Model;
using System.Globalization;
using System.Configuration;

namespace RM.Web.GanPing
{
    public partial class ReportGasMng : PageBase
    {
        EGMNGS.BLL.ApplRegGasQtyInspection bll = new EGMNGS.BLL.ApplRegGasQtyInspection();

        private EGMNGS.Model.ApplRegGasQtyInspection Model
        {
            get
            {
                if (ViewState["ApplRegGasQtyInspection"] == null)
                {
                    return new EGMNGS.Model.ApplRegGasQtyInspection();
                }
                return ViewState["ApplRegGasQtyInspection"] as EGMNGS.Model.ApplRegGasQtyInspection;
            }
            set { ViewState["ApplRegGasQtyInspection"] = value; }
        }

        private string sqlStr
        {
            get
            {
                if (ViewState["sqlStr"] == null)
                {
                    ViewState["sqlStr"] = "DATEDIFF(MONTH, AppLDate, GETDATE()) <= 3";
                }
                return ViewState["sqlStr"] as string;
            }
            set { ViewState["sqlStr"] = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //this.PageControl1.pageHandler += new EventHandler(pager_PageChanged);
            if (!IsPostBack)
            {
                DropDownListBinder();
                DataBindGrid();
            }


        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            #region 生成查询条件

            StringBuilder sb = new StringBuilder("1=1");

            if (this.txtBusinessCode.Text.Trim().Length > 0)
            {
                sb.AppendFormat("and BusinessCode like '%{0}%' ", this.txtBusinessCode.Text.Trim());
            }
            if (this.txtCheckCode.Text.Trim().Length > 0)
            {
                sb.AppendFormat("and CheckCode like '%{0}%' ", this.txtCheckCode.Text.Trim());
            }
            if (this.txtConvertStationName.Text.Trim().Length > 0)
            {
                sb.AppendFormat("and BusinessCode like '%{0}%' ", this.txtConvertStationName.Text.Trim());
            }
            if (this.txtCylinderCode.Text.Trim().Length > 0)
            {
                sb.AppendFormat("and CylinderCode like '%{0}%' ", this.txtCylinderCode.Text.Trim());
            }
            if (this.txtGasCode.Text.Trim().Length > 0)
            {
                sb.AppendFormat("and GasCdoe like '%{0}%' ", this.txtGasCode.Text.Trim());
            }
            if (this.ddlGasSourse.Text.Trim().Length > 0)
            {
                sb.AppendFormat("and GasSourse like '%{0}%' ", this.ddlGasSourse.SelectedValue);
            }
            if (this.txtPowerSupplyName.Text.Trim().Length > 0)
            {
                sb.AppendFormat("and PowerSupplyName like '%{0}%' ", this.txtPowerSupplyName.Text.Trim());
            }
            if (this.txtReportCode.Text.Trim().Length > 0)
            {
                sb.AppendFormat("and ReportCode like '%{0}%' ", this.txtReportCode.Text.Trim());
            }

            if (this.ddlCheckPJType.SelectedValue.Length > 0)
            {
                sb.AppendFormat("and CheckPJType like '%{0}%' ", this.ddlCheckPJType.SelectedValue);
            }
            if (this.ddlIsPass.SelectedValue.Length > 0)
            {
                sb.AppendFormat("and IsPass like '%{0}%' ", this.ddlIsPass.SelectedValue);
            }
            if (this.ddlIsPrint.SelectedValue.Length > 0)
            {
                sb.AppendFormat("and IsPrint like '%{0}%' ", this.ddlIsPrint.SelectedValue);
            }
            if (this.tbInspectionUnit.Text.Length > 0)
            {
                sb.AppendFormat("and InspectionUnit like '%{0}%' ", this.tbInspectionUnit.Text);
            }
            if (this.txtCheckDateFrom.Text.Length > 0)
            {
                sb.AppendFormat(" and CheckDate >= '{0}' ", this.txtCheckDateFrom.Text);
            }
            if (this.txtCheckDateTo.Text.Length > 0)
            {
                sb.AppendFormat(" and CheckDate <= '{0}' ", this.txtCheckDateTo.Text);
            }
            #endregion

            sqlStr = sb.ToString().TrimStart(new char[3] { 'a', 'n', 'd' });
            DataBindGrid();
        }

        /// <summary>
        /// 绑定数据，分页
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void pager_PageChanged(object sender, EventArgs e)
        {
            DataBindGrid();
        }

        /// <summary>
        /// 绑定数据源
        /// </summary>
        private void DataBindGrid()
        {
            DataSet ds = ComServies.GetList(sqlStr);
            ControlBindHelper.BindRepeaterList(ds.Tables[0], rp_Item);
            // this.PageControl1.RecordCount = Convert.ToInt32(ds.Tables[0].Rows.Count);
        }

        protected void rp_Item_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            int oid = Convert.ToInt32(e.CommandArgument);

            Model = bll.GetModel(oid);
            //InitControl();
            //actionMode = "Edit";

            ////绑定明细表
            //GetDetailsOfRecycleGas();
        }

        /// <summary>
        /// 绑定后激发事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void rp_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                //Web.UserControl
                //System.Web.UI.WebControls.Label
                System.Web.UI.WebControls.Label lblIsPass = e.Item.FindControl("lblIsPass") as System.Web.UI.WebControls.Label;

                if (lblIsPass != null)
                {
                    if (lblIsPass.Text.Length > 0)
                    {
                        lblIsPass.Text = this.ddlIsPass.Items.FindByValue(lblIsPass.Text).Text;

                    }
                }

                System.Web.UI.WebControls.Label lblGasSourse = e.Item.FindControl("lblGasSourse") as System.Web.UI.WebControls.Label;
                if (lblGasSourse != null)
                {
                    if (lblGasSourse.Text.Length > 0)
                    {
                        lblGasSourse.Text = this.ddlGasSourse.Items.FindByValue(lblGasSourse.Text).Text;

                    }
                }
            }
        }

        /// <summary>
        /// 绑定下拉列
        /// </summary>
        private void DropDownListBinder()
        {

            EGMNGS.BLL.SYS_CODE sysCodeBll = new EGMNGS.BLL.SYS_CODE();
            DataSet dsYear = sysCodeBll.GetList("CODE_TYPE='GasSourse' ORDER BY DISPLAY_ORDER");

            this.ddlGasSourse.DataSource = dsYear.Tables[0];
            this.ddlGasSourse.DataTextField = "CODE_CHI_DESC";
            this.ddlGasSourse.DataValueField = "CODE";
            this.ddlGasSourse.DataBind();

            this.ddlGasSourse.Items.Insert(0, string.Empty);

            DataSet dsIsPass = sysCodeBll.GetList("CODE_TYPE='IsPass' ORDER BY DISPLAY_ORDER");

            this.ddlIsPass.DataSource = dsIsPass.Tables[0];
            this.ddlIsPass.DataTextField = "CODE_CHI_DESC";
            this.ddlIsPass.DataValueField = "CODE";
            this.ddlIsPass.DataBind();
            this.ddlIsPass.Items.Insert(0, string.Empty);

            DataSet dtCheckPJType = sysCodeBll.GetList("CODE_TYPE='CheckPJTypeStatus' ORDER BY DISPLAY_ORDER");

            this.ddlCheckPJType.DataSource = dtCheckPJType.Tables[0];
            this.ddlCheckPJType.DataTextField = "CODE_CHI_DESC";
            this.ddlCheckPJType.DataValueField = "CODE";
            this.ddlCheckPJType.DataBind();
            this.ddlCheckPJType.Items.Insert(0, string.Empty);


        }

        /// <summary>
        /// 导出
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnExport_Click(object sender, EventArgs e)
        {
            string strID = HiddenField1.Value;
            if (strID.Trim().Length == 0)
            {
                return;

            }

            var modelList = bll.GetModelList(string.Format(" OID in({0})", strID));



            MouldExportExcel mee = new MouldExportExcel();
            try
            {
                string physicalPath = Request.PhysicalApplicationPath + "Xls\\";
                Microsoft.Office.Interop.Excel.Workbook sh = mee.OpenExcel(physicalPath + "气体报告管理.xls");
                bool ispass = false;
                foreach (var item in modelList)
                {
                    if (item.IsPass == null)
                    {
                        ShowMsgHelper.Alert(string.Format("{0}此检测单号还未检测不能导出！", item.CheckCode));
                        continue;
                    }
                    ispass = true;
                    Microsoft.Office.Interop.Excel.Worksheet ws = sh.Worksheets[modelList.IndexOf(item) + 1] as Microsoft.Office.Interop.Excel.Worksheet;
                    if (modelList.IndexOf(item) < modelList.Count - 1)
                    {
                        ws.Copy(Type.Missing, ws);
                    }
                    #region 设置值


                    if (item.GasSourse == "3")//回收净化气取“广东电网公司电力科学研究院”。
                    {
                        mee.SetCellValue(ws, 1, 1, "六氟化硫再生气体检测报告");
                        mee.SetCellValue(ws, 4, 2, "气体编码：");
                        mee.SetCellValue(ws, 4, 3, item.GasCdoe);
                        //mee.SetCellValue(ws, 4, 6, "检测单号：");
                        //mee.SetCellValue(ws, 4, 7, item.CheckCode);
                        mee.SetCellValue(ws, 4, 6, "批 次 号：");
                        mee.SetCellValue(ws, 4, 7, item.BusinessCode);
                    }
                    else if (item.GasSourse == "2")//入网新气取“入网检测登记”功能的“送检单位”。
                    {
                        ApplDetectionGas tempObj = new EGMNGS.BLL.ApplDetectionGas().GetModelList(string.Format(@"ApplCode='{0}'", item.BusinessCode))[0];
                        DetailsApplDetectionGas tempDetailsApplDetectionGa = new EGMNGS.BLL.DetailsApplDetectionGas().GetModelList(string.Format("GasCode='{0}'", item.GasCdoe))[0];

                        mee.SetCellValue(ws, 1, 1, "六氟化硫气体检测报告");
                        mee.SetCellValue(ws, 4, 2, "委托单位：");
                        mee.SetCellValue(ws, 4, 3, tempObj.InspectionUnit);
                        mee.SetCellValue(ws, 4, 6, "生产厂家：");
                        mee.SetCellValue(ws, 4, 7, tempDetailsApplDetectionGa.Manufacturer);
                        mee.SetCellValue(ws, 5, 6, "安装地点：");
                        mee.SetCellValue(ws, 5, 7, tempObj.SampliInspituation);
                    }
                    else if (item.GasSourse == "1")//新采购气取“新购气体管理”功能的“生产厂家”。
                    {
                        //  RegGasProcurement tempObj = new EGMNGS.BLL.RegGasProcurement().GetModelList(string.Format(@"Code='{0}'", item.BusinessCode))[0];

                        // mee.SetCellValue(ws, 4, 7, tempObj.ManufacturerName);

                        mee.SetCellValue(ws, 1, 1, "六氟化硫气体检测报告");
                        mee.SetCellValue(ws, 4, 2, "气体编码：");
                        mee.SetCellValue(ws, 4, 3, item.GasCdoe);
                        //mee.SetCellValue(ws, 4, 6, "检测单号：");
                        //mee.SetCellValue(ws, 4, 7, item.CheckCode);
                        mee.SetCellValue(ws, 4, 6, "批 次 号：");
                        mee.SetCellValue(ws, 4, 7, item.BusinessCode);

                    }


                    mee.SetCellValue(ws, 3, 4, item.ReportCode);
                    if (item.CheckDate != null)
                    {
                        mee.SetCellValue(ws, 3, 7, item.CheckDate.Value.ToShortDateString());

                    }
                    string cylinderSealNo = new EGMNGS.BLL.BookCylinderInfo().GetModelByCylinderCode(item.CylinderCode).CylinderSealNo;
                    mee.SetCellValue(ws, 5, 3, string.Format("{0}", item.CylinderCode));
                    //  mee.SetCellValue(ws, 5, 7, item.IsPass == "1" ? "合格" : "不合格");
                    var MyRng = ws.get_Range("A42:I42", Type.Missing);
                    MyRng.WrapText = true;
                    MyRng.Orientation = 0;
                    MyRng.AddIndent = false;
                    MyRng.IndentLevel = 0;
                    MyRng.ShrinkToFit = false;
                    MyRng.MergeCells = true;
                    //  End With

                    mee.SetCellValue(ws, 6, 3, cylinderSealNo);
                    //  mee.SetCellValue(ws, 6, 7, item.BusinessCode);
                    mee.SetCellValue(ws, 18, 3, item.C.ToString());
                    mee.SetCellValue(ws, 19, 3, item.KPA.ToString());
                    mee.SetCellValue(ws, 18, 7, item.RH.ToString());

                    EGMNGS.BLL.BookDetectionTools bllBookDetectionTools = new EGMNGS.BLL.BookDetectionTools();

                    var list = bllBookDetectionTools.GetModelList(string.Format("ToolCode='{0}'", item.SPYID));
                    if (list.Count > 0)
                    {

                        mee.SetCellValue(ws, 23, 3, list[0].SpecType);
                        mee.SetCellValue(ws, 23, 6, list[0].FactoryCode);
                        mee.SetCellValue(ws, 23, 9, list[0].EffectiveDate.Value.ToString("yyyy-MM-dd", DateTimeFormatInfo.InvariantInfo));

                    }
                    list = bllBookDetectionTools.GetModelList(string.Format("ToolCode='{0}'", item.LDYID));
                    if (list.Count > 0)
                    {
                        mee.SetCellValue(ws, 24, 3, list[0].SpecType);
                        mee.SetCellValue(ws, 24, 6, list[0].FactoryCode);
                        mee.SetCellValue(ws, 24, 9, list[0].EffectiveDate.Value.ToString("yyyy-MM-dd", DateTimeFormatInfo.InvariantInfo));
                    }
                    list = bllBookDetectionTools.GetModelList(string.Format("ToolCode='{0}'", item.WSJID));
                    if (list.Count > 0)
                    {
                        mee.SetCellValue(ws, 25, 3, list[0].SpecType);
                        mee.SetCellValue(ws, 25, 6, list[0].FactoryCode);
                        mee.SetCellValue(ws, 25, 9, list[0].EffectiveDate.Value.ToString("yyyy-MM-dd", DateTimeFormatInfo.InvariantInfo));
                    }
                    list = bllBookDetectionTools.GetModelList(string.Format("ToolCode='{0}'", item.QYJID));
                    if (list.Count > 0)
                    {
                        mee.SetCellValue(ws, 26, 3, list[0].SpecType);
                        mee.SetCellValue(ws, 26, 6, list[0].FactoryCode);
                        mee.SetCellValue(ws, 26, 9, list[0].EffectiveDate.Value.ToString("yyyy-MM-dd", DateTimeFormatInfo.InvariantInfo));
                    }
                    list = bllBookDetectionTools.GetModelList(string.Format("ToolCode='{0}'", item.SDJID));
                    if (list.Count > 0)
                    {
                        mee.SetCellValue(ws, 27, 3, list[0].SpecType);
                        mee.SetCellValue(ws, 27, 6, list[0].FactoryCode);
                        mee.SetCellValue(ws, 27, 9, list[0].EffectiveDate.Value.ToString("yyyy-MM-dd", DateTimeFormatInfo.InvariantInfo));
                    }
                    list = bllBookDetectionTools.GetModelList(string.Format("ToolCode='{0}'", item.FGGDJID));
                    if (list.Count > 0)
                    {
                        mee.SetCellValue(ws, 28, 3, list[0].SpecType);
                        mee.SetCellValue(ws, 28, 6, list[0].FactoryCode);
                        mee.SetCellValue(ws, 28, 9, list[0].EffectiveDate.Value.ToString("yyyy-MM-dd", DateTimeFormatInfo.InvariantInfo));
                    }
                    list = bllBookDetectionTools.GetModelList(string.Format("ToolCode='{0}'", item.HWHYFXYID));
                    if (list.Count > 0)
                    {
                        mee.SetCellValue(ws, 29, 3, list[0].SpecType);
                        mee.SetCellValue(ws, 29, 6, list[0].FactoryCode);
                        mee.SetCellValue(ws, 29, 9, list[0].EffectiveDate.Value.ToString("yyyy-MM-dd", DateTimeFormatInfo.InvariantInfo));

                    }


                    if (item.SF6Result >= (decimal)99.99)
                    {
                        mee.SetCellValue(ws, 34, 2, ">99.99");
                    }
                    else
                    {
                        mee.SetCellValue(ws, 34, 2, Convert.ToDecimal(item.SF6Result).ToString("F4"));
                    }

                    //  mee.SetCellValue(ws, 36, 2, item.SF6Index);

                    if ((item.AirQualityScoreResult * 10000) < 1)
                    {
                        mee.SetCellValue(ws, 34, 4, "＜1");
                    }
                    else
                    {
                        mee.SetCellValue(ws, 34, 4, Convert.ToDecimal(item.AirQualityScoreResult * 10000).ToString("F4"));

                    }
                    //  mee.SetCellValue(ws, 36, 3, item.AirQualityScoreIndex.ToString());

                    //小数第4位为0时(＜0.0001)，否则保留4位[if N<0.0001,'＜0.0001',保留4位小数]
                    if (Convert.ToDouble(item.CF4Result * 10000) < 1)
                    {
                        mee.SetCellValue(ws, 34, 6, "＜1");
                    }
                    else
                    {
                        mee.SetCellValue(ws, 34, 6, Convert.ToDecimal(item.CF4Result * 10000).ToString("F4"));
                    }

                    if (Convert.ToDouble(item.C2F6) < 1)
                    {
                        mee.SetCellValue(ws, 34, 8, "＜1");
                    }
                    else
                    {
                        mee.SetCellValue(ws, 34, 8, item.C2F6.AsTargetType<int>(0));
                    }

                    if (Convert.ToDouble(item.C3F8) < 1)
                    {
                        mee.SetCellValue(ws, 34, 9, "＜1");
                    }
                    else
                    {
                        mee.SetCellValue(ws, 34, 9, item.C3F8.AsTargetType<int>(0));
                    }


                    // mee.SetCellValue(ws, 36, 4, item.CF4Index);

                    //小数第4位为0时(＜0.0001)，否则保留4位[if N<0.0001,'＜0.0001',保留4位小数]
                    if (Convert.ToDouble(item.WaterQualityScoreResult) * 10000 < 1)
                    {
                        mee.SetCellValue(ws, 39, 2, "＜1");
                    }
                    else
                    {
                        mee.SetCellValue(ws, 39, 2, Convert.ToDecimal(item.WaterQualityScoreResult * 10000).ToString("F5"));
                    }

                    //  mee.SetCellValue(ws, 36, 5, item.WaterQualityScoreIndex);

                    //气体检测报告：露点，如果录入数据是小于-61.0就导出＜-61.0，其它值就按实际数据导出。

                    mee.SetCellValue(ws, 39, 4, item.WaterDewResult < -61 ? "<-61.0" : Convert.ToString(item.WaterDewResult));

                    //  mee.SetCellValue(ws, 36, 6, item.WaterDewIndex);


                    if (Convert.ToDouble(item.HFResult) * 10000 < 0)
                    {

                        mee.SetCellValue(ws, 39, 6, "0");
                    }
                    else if (Convert.ToDouble(item.HFResult) * 10000 < 0.1)
                    {
                        mee.SetCellValue(ws, 39, 6, Convert.ToDecimal(item.HFResult * 10000).ToString("F7"));
                    }
                    else
                    {
                        mee.SetCellValue(ws, 39, 6, Convert.ToDecimal(item.HFResult * 10000).ToString("F6"));
                    }




                    // mee.SetCellValue(ws, 36, 7, item.HFIndex);

                    //  mee.SetCellValue(ws, 35, 8, item.KSJHFResult.Value.ToString());

                    if (Convert.ToDouble(item.KSJHFResult * 10000) < 0.02)
                    {

                        mee.SetCellValue(ws, 39, 8, "＜0.02");
                    }
                    else if (Convert.ToDouble(item.KSJHFResult * 10000) < 0.00001)
                    {
                        mee.SetCellValue(ws, 39, 8, Convert.ToDecimal(item.KSJHFResult * 10000).ToString("F7"));
                    }
                    else
                    {
                        mee.SetCellValue(ws, 39, 8, Convert.ToDecimal(item.KSJHFResult * 10000).ToString("F6"));
                    }

                    //  mee.SetCellValue(ws, 36, 8, item.KSJHFIndex);

                    //  mee.SetCellValue(ws, 39, 9, item.KWYSorceResult.AsTargetType<string>(""));

                    if (Convert.ToDouble(item.KWYSorceResult * 10000) < 0.02)
                    {

                        mee.SetCellValue(ws, 39, 9, "＜0.02");
                    }
                    else if (Convert.ToDouble(item.KWYSorceResult * 10000) < 0.1)
                    {
                        mee.SetCellValue(ws, 39, 9, Convert.ToDecimal(item.KWYSorceResult * 10000).ToString("F7"));
                    }
                    else
                    {
                        mee.SetCellValue(ws, 39, 9, Convert.ToDecimal(item.KWYSorceResult * 10000).ToString("F6"));
                    }

                    //  mee.SetCellValue(ws, 36, 9, item.KWYSorceIndex);
                    mee.SetCellValue(ws, 43, 1, item.Conclusion);


                    if (item.GasSourse == "2")//入网新气取
                    {
                        mee.SetCellValue(ws, 5, 2, "出厂编号：");
                        mee.SetCellValue(ws, 5, 3, cylinderSealNo);
                        mee.SetCellValue(ws, 6, 2, "");
                        mee.SetCellValue(ws, 6, 3, "");
                    }

                    if (item.CheckPJType == "1")
                    {
                        mee.SetCellValue(ws, 12, 2, "");
                        mee.SetCellValue(ws, 13, 2, "");
                        mee.SetCellValue(ws, 14, 2, "");

                        mee.SetCellValue(ws, 27, 2, "");
                        mee.SetCellValue(ws, 27, 3, "");

                        mee.SetCellValue(ws, 27, 5, "");
                        mee.SetCellValue(ws, 27, 6, "");

                        mee.SetCellValue(ws, 27, 8, "");
                        mee.SetCellValue(ws, 27, 9, "");

                        mee.SetCellValue(ws, 28, 1, "");
                        mee.SetCellValue(ws, 28, 3, "");

                        mee.SetCellValue(ws, 28, 5, "");
                        mee.SetCellValue(ws, 28, 6, "");

                        mee.SetCellValue(ws, 28, 8, "");
                        mee.SetCellValue(ws, 28, 9, "");

                        mee.SetCellValue(ws, 29, 1, "");
                        mee.SetCellValue(ws, 29, 3, "");

                        mee.SetCellValue(ws, 29, 5, "");
                        mee.SetCellValue(ws, 29, 6, "");

                        mee.SetCellValue(ws, 29, 8, "");
                        mee.SetCellValue(ws, 29, 9, "");


                        //mee.SetCellValue(ws, 35, 7, "—");
                        //mee.SetCellValue(ws, 35, 8, "—");
                        //mee.SetCellValue(ws, 35, 9, "—");


                    }

                    #endregion

                    item.IsPrint = true;
                    new EGMNGS.BLL.ApplRegGasQtyInspection().Update(item);
                }
                if (!ispass)
                {
                    return;
                }
                string fileName = "气体报告管理temp.xls";
                string pathFile = physicalPath + fileName;
                mee.SaveExcel(pathFile);

                FileDownHelper.DownLoadold(@"~/Xls/" + fileName);
               
            }
            catch ( Exception ex)
            {
                Common.DotNetCode.LogInstanseHelper.Instance.WriteLog(ex.Message);
            }
            finally
            {
                mee.Dispose();

            }

        }

        /// <summary>
        /// 合格证打印
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnPassPrint_Click(object sender, EventArgs e)
        {

            string strID = HiddenField1.Value;
            if (strID.Trim().Length == 0)
            {
                return;
            }

            var modelList = bll.GetModelList(string.Format(" OID in({0})", strID));



            string barCodePath = ConfigurationManager.AppSettings["PassBarCodePath"];
            string passPrinterName = ConfigurationManager.AppSettings["PassPrinterName"];
            //Declare a BarTender application variable 

            BarTender.Application btApp;

            //Declare a BarTender format variable 

            BarTender.Format btFormat;

            //Instantiate a BarTender application variable 

            btApp = new BarTender.Application();

            //Set the BarTender application visible 

            btApp.Visible = true;

            //Open a BarTender label format 

            btFormat = btApp.Formats.Open(barCodePath, false, passPrinterName);

            foreach (var item in modelList)
            {

                BookCylinderInfo bookCylinderInfoOBJ = new EGMNGS.BLL.BookCylinderInfo().GetModelByCylinderCode(item.CylinderCode);

                btFormat.SetNamedSubStringValue("钢印号", bookCylinderInfoOBJ.CylinderSealNo);
                btFormat.SetNamedSubStringValue("出厂编号", bookCylinderInfoOBJ.CurGasCode);

                string GPRL = string.Format("{0}KG", DictEGMNS["GPRL"][bookCylinderInfoOBJ.CylinderCapacity]);

                string suttle = new EGMNGS.BLL.BookGasFill().GetModeByWhere(string.Format("GasCode='{0}'", bookCylinderInfoOBJ.CurGasCode)).AmountGas.ToString();
                btFormat.SetNamedSubStringValue("净重", suttle);
                btFormat.SetNamedSubStringValue("瓶重", GPRL);

                //小数第4位为0时(＜0.0001)，否则保留4位[if N<0.0001,'＜0.0001',保留4位小数]
                if (Convert.ToDouble(item.CF4Result) < 0.0001)
                {
                    btFormat.SetNamedSubStringValue("四氟化碳", "＜1");
                }
                else
                {
                    btFormat.SetNamedSubStringValue("四氟化碳", Convert.ToDecimal(item.CF4Result * 10000).ToString("F2"));
                }


                if (item.SF6Result >= (decimal)99.99)
                {

                    btFormat.SetNamedSubStringValue("六氟化硫", ">99.99");
                }
                else
                {
                    btFormat.SetNamedSubStringValue("六氟化硫", Convert.ToDecimal(item.SF6Result).ToString("F2"));
                }

                if (Convert.ToDouble(item.WaterQualityScoreResult) < 0.0001)
                {

                    btFormat.SetNamedSubStringValue("水分", "＜1");
                }
                else
                {
                    btFormat.SetNamedSubStringValue("水分", Convert.ToDecimal(item.WaterQualityScoreResult * 10000).ToString("F5"));
                }

                btFormat.SetNamedSubStringValue("空气", Convert.ToDecimal(item.AirQualityScoreResult * 10000).ToString("F2"));

                if (Convert.ToDouble(item.KSJHFResult) < 0.000002)
                {
                    btFormat.SetNamedSubStringValue("可水解氟化物", "＜0.02");

                }
                else if (Convert.ToDouble(item.KSJHFResult) < 0.00001)
                {
                    btFormat.SetNamedSubStringValue("可水解氟化物", Convert.ToDecimal(item.KSJHFResult * 10000).ToString("F2"));

                }
                else
                {
                    btFormat.SetNamedSubStringValue("可水解氟化物", Convert.ToDecimal(item.KSJHFResult * 10000).ToString("F2"));

                }


                if (Convert.ToDouble(item.HFResult) < 0.000002)
                {

                    btFormat.SetNamedSubStringValue("酸度", "＜0.02");
                }
                else if (Convert.ToDouble(item.HFResult) < 0.00001)
                {

                    btFormat.SetNamedSubStringValue("酸度", Convert.ToDecimal(item.HFResult * 10000).ToString("F2"));
                }
                else
                {
                    btFormat.SetNamedSubStringValue("酸度", Convert.ToDecimal(item.HFResult * 10000).ToString("F2"));
                }


                if (item.GasSourse == "3")
                {
                    var listRGB = new EGMNGS.BLL.RegGasBatch().GetModelList(string.Format("BatchCode='{0}'", item.BusinessCode));
                    if (listRGB.Count > 0)
                    {
                        btFormat.SetNamedSubStringValue("生产日期", listRGB[0].RegistantsDate.Value.ToString("yyyy-MM-dd", DateTimeFormatInfo.InvariantInfo));
                    }
                }
                else
                {
                    btFormat.SetNamedSubStringValue("生产日期", item.CheckDate == null ? "" : item.CheckDate.Value.ToString("yyyy-MM-dd", DateTimeFormatInfo.InvariantInfo));
                }

                if (Convert.ToDouble(item.KWYSorceResult) < 0.000002)
                {
                    btFormat.SetNamedSubStringValue("矿物油", "＜0.02");
                }
                else if (Convert.ToDouble(item.KWYSorceResult) < 0.00001)
                {
                    btFormat.SetNamedSubStringValue("矿物油", Convert.ToDecimal(item.KWYSorceResult * 10000).ToString("F2"));
                }
                else
                {
                    btFormat.SetNamedSubStringValue("矿物油", Convert.ToDecimal(item.KWYSorceResult * 10000).ToString("F2"));
                }

                btFormat.SetNamedSubStringValue("报告编号", item.ReportCode);
                btFormat.SetNamedSubStringValue("钢瓶编码", item.CylinderCode);
                btFormat.PrintOut(false, false);
            }

            btApp.Quit(BarTender.BtSaveOptions.btDoNotSaveChanges);

            System.Runtime.InteropServices.Marshal.ReleaseComObject(btApp);

            GC.Collect();     // 强制对零代到指定代进行垃圾回收。
        }

        private string ConvertToString(object d)
        {
            if (d != null)
            {
                return d.ToString();
            }
            else
            {
                return string.Empty;
            }
        }
    }
}
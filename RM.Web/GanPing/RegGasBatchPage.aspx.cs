﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using RM.Common.DotNetCode;
using System.Data;
using RM.Common.DotNetUI;
using RM.Web.App_Code;
using EGMNGS.Common;

namespace RM.Web.GanPing
{
    public partial class RegGasBatchPage : PageBase
    {
        #region 属性字段

        EGMNGS.BLL.RegGasBatch bll = new EGMNGS.BLL.RegGasBatch();
        private DataTable UserList
        {
            get
            {
                return ViewState["UserList"] as DataTable;
            }
            set { ViewState["UserList"] = value; }
        }
        private EGMNGS.Model.RegGasBatch Model
        {
            get
            {
                if (Session["RegGasBatch"] == null)
                {
                    return new EGMNGS.Model.RegGasBatch();
                }
                return Session["RegGasBatch"] as EGMNGS.Model.RegGasBatch;
            }
            set { Session["RegGasBatch"] = value; }
        }
        private string actionMode
        {
            get { return ViewState["ActionMode"] as String; }
            set { ViewState["ActionMode"] = value; }
        }
        public string PowerSupplyCode
        {
            get { return ViewState["PowerSupplyCode"] as String; }
            set { ViewState["PowerSupplyCode"] = value; }
        }
        public string PowerSupplyName
        {
            get { return ViewState["PowerSupplyName"] as String; }
            set { ViewState["PowerSupplyName"] = value; }
        }

        #endregion

        #region 事件方法

        protected void Page_Load(object sender, EventArgs e)
        {
            this.PageControl1.pageHandler += new EventHandler(pager_PageChanged);
            if (!IsPostBack)
            {
                UserList = ComServies.GetAllUserInfo();
                DropDownListBinder();
            }
        }

        /// <summary>
        /// 绑定下拉列
        /// </summary>
        private void DropDownListBinder()
        {
            EGMNGS.BLL.SYS_CODE sysCodeBll = new EGMNGS.BLL.SYS_CODE();
            DataSet dsYear = sysCodeBll.GetList("CODE_TYPE='Year' ORDER BY DISPLAY_ORDER");

            this.ddlYear.DataSource = dsYear.Tables[0];
            this.ddlYear.DataTextField = "CODE_CHI_DESC";
            this.ddlYear.DataValueField = "CODE";
            this.ddlYear.DataBind();

            DataSet dsMonth = sysCodeBll.GetList("CODE_TYPE='Month' ORDER BY DISPLAY_ORDER");

            this.ddlMonth.DataSource = dsMonth.Tables[0];
            this.ddlMonth.DataTextField = "CODE_CHI_DESC";
            this.ddlMonth.DataValueField = "CODE";
            this.ddlMonth.DataBind();


            DataSet dsProcessEquipment = sysCodeBll.GetList("CODE_TYPE='CLDev' ORDER BY DISPLAY_ORDER");

            this.ddlProcessEquipment.DataSource = dsProcessEquipment.Tables[0];
            this.ddlProcessEquipment.DataTextField = "CODE_CHI_DESC";
            this.ddlProcessEquipment.DataValueField = "CODE";
            this.ddlProcessEquipment.DataBind();

            this.ddlProcessEquipment.Items.Insert(0, string.Empty);


            DataSet dtCFStatus = sysCodeBll.GetList("CODE_TYPE='CFStatus' ORDER BY DISPLAY_ORDER");

            this.ddlStatus.DataSource = dtCFStatus.Tables[0];
            this.ddlStatus.DataTextField = "CODE_CHI_DESC";
            this.ddlStatus.DataValueField = "CODE";
            this.ddlStatus.DataBind();

            this.ddlStatus.Items.Insert(0, string.Empty);


            DataSet dsIsPass = sysCodeBll.GetList("CODE_TYPE='IsPass' ORDER BY DISPLAY_ORDER");

            this.ddlIsPass.DataSource = dsIsPass.Tables[0];
            this.ddlIsPass.DataTextField = "CODE_CHI_DESC";
            this.ddlIsPass.DataValueField = "CODE";
            this.ddlIsPass.DataBind();
        }


        /// <summary>
        /// 绑定后激发事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void rp_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Label lblUserID = e.Item.FindControl("lblUserID") as Label;
                if (lblUserID != null)
                {
                    lblUserID.Text = GetUserName(lblUserID.Text);
                }

            }
        }

        /// <summary>
        /// 绑定数据，分页
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void pager_PageChanged(object sender, EventArgs e)
        {
            DataBindGrid();
        }

        /// <summary>
        /// 绑定数据源
        /// </summary>
        private void DataBindGrid()
        {
            int count = bll.GetRecordCount("1=1");
            DataSet ds = bll.GetListByPage("1=1", string.Empty, PageControl1.PageIndex, PageControl1.PageSize);
            ControlBindHelper.BindRepeaterList(ds.Tables[0], rp_Item);
            this.PageControl1.RecordCount = Convert.ToInt32(count);
        }
        /// <summary>
        /// 绑定数据到对象
        /// </summary>
        private void DataBinder()
        {
            Model.BatchCode = this.txtBatchCode.Text;
            Model.Year = this.ddlYear.SelectedValue;
            Model.Month = this.ddlMonth.SelectedValue;

            if (this.txtAcidity.Text.Trim().Length == 0)
            {
                Model.Acidity = null;
            }
            else
            {
                Model.Acidity = Convert.ToDecimal(this.txtAcidity.Text);
            }

            if (this.txtAirContent.Text.Trim().Length == 0)
            {
                Model.AirContent = null;
            }
            else
            {
                Model.AirContent = Convert.ToDecimal(this.txtAirContent.Text);
            }

            if (this.txtAmountGas.Text.Trim().Length == 0)
            {
                Model.AmountGas = null;
            }
            else
            {
                Model.AmountGas = Convert.ToDecimal(this.txtAmountGas.Text);
            }

            if (this.txtKSJFHW.Text.Trim().Length == 0)
            {
                Model.KSJFHW = null;
            }
            else
            {
                Model.KSJFHW = Convert.ToDecimal(this.txtKSJFHW.Text);
            }

            if (this.txtKWY.Text.Trim().Length == 0)
            {
                Model.KWY = null;
            }
            else
            {
                Model.KWY = Convert.ToDecimal(this.txtKWY.Text);
            }

            if (this.txtSFContent.Text.Trim().Length == 0)
            {
                Model.SFContent = null;
            }
            else
            {
                Model.SFContent = Convert.ToDecimal(this.txtSFContent.Text);
            }

            if (this.txtSFHT.Text.Trim().Length == 0)
            {
                Model.SFHT = null;
            }
            else
            {
                Model.SFHT = Convert.ToDecimal(this.txtSFHT.Text);
            }

            if (this.txtWet.Text.Trim().Length == 0)
            {
                Model.Wet = null;
            }
            else
            {
                Model.Wet = Convert.ToDecimal(this.txtWet.Text);
            }

            Model.ProcessEquipment = this.ddlProcessEquipment.SelectedValue;
            Model.IsPass = this.ddlIsPass.SelectedValue;
        }

        /// <summary>
        /// 初始化控件
        /// </summary>
        private void InitControl()
        {
            this.txtBatchCode.Text = Model.BatchCode;
            this.ddlMonth.SelectedValue = Model.Month;
            this.ddlYear.SelectedValue = Model.Year;
            this.ddlProcessEquipment.SelectedValue = Model.ProcessEquipment;
            this.txtAmountGas.Text = Model.AmountGas.ToString();
            this.txtSFContent.Text = Model.SFContent.ToString();
            this.txtAirContent.Text = Model.AirContent.ToString();
            this.txtSFHT.Text = Model.SFHT.ToString();
            this.txtWet.Text = Model.Wet.ToString();
            this.txtAcidity.Text = Model.Acidity.ToString();
            this.txtKSJFHW.Text = Model.KSJFHW.ToString();
            this.txtKWY.Text = Model.KWY.ToString();
            this.ddlStatus.SelectedValue = Model.Status;
            this.txtRegistrantOID.Text = GetUserName(Model.RegistantsOID);
            this.txtRegistrantDate.Text = Model.RegistantsDate == null ? string.Empty : Model.RegistantsDate.Value.ToShortDateString();
            this.ddlIsPass.SelectedValue = Model.IsPass;
        }

        /// <summary>
        /// 获取用户名
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        private string GetUserName(string userId)
        {
            DataRow[] dr = UserList.Select(string.Format("User_ID='{0}'", userId));
            if (dr.Length > 0)
            {
                return dr[0][1].ToString();
            }
            return string.Empty;
        }
        #endregion

        #region 按钮事件

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            if (Model.OID == 0)
            {
                return;
            }

            if (Model.Status == "1")
            {
                ShowMsgHelper.Alert_Wern("无法重新提交！");
                return;
            }

            Model.Status = "1";
            if (bll.Update(Model))
            {
                ShowMsgHelper.Alert("提交成功！");
                InitControl();
                DataBindGrid();
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            DataBindGrid();
        }

        private void ShowMaxMode()
        {
            Model = bll.GetModel(bll.GetMaxId() - 1);
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                if (actionMode == "Add")
                {
                    DataBinder();
                    // Model.RegistantsOID = User.UserId.ToString();

                    int count = bll.GetRecordCount(string.Format("BatchCode='{0}'", Model.BatchCode));
                    if (count > 0)
                    {
                        ShowMsgHelper.showWarningMsg("批次编号已经存在!");
                        return;
                    }

                    if (bll.Add(Model) > 0)
                    {
                        actionMode = string.Empty;
                    }

                    DataBindGrid();
                    ShowMaxMode();
                }
                else if (actionMode == "Edit")
                {
                    //if (bll.GetModel(Model.OID).Status != "0")
                    //{
                    //    ShowMsgHelper.Alert_Wern("“已确认”状态无法修改数据！");
                    //    return;
                    //}


                    DataBinder();
                    if (bll.Update(Model))
                    {
                        EGMNSShowMsg.ShowEditMsgSuccess();
                        actionMode = string.Empty;
                    }
                    DataBindGrid();
                }
            }
            catch (ArgumentOutOfRangeException ex)
            {
                string[] errs = CommonHelper.GetValue(ex.Message, "@").Split(':');
                ShowMsgHelper.ExecuteScript(string.Format("ChangeCss($('#txt{0}'),'长度不能超{1}字')", errs[0], errs[1]));
            }
        }
        private void ClearControl()
        {
            Model = new EGMNGS.Model.RegGasBatch();
            InitControl();
        }
        protected void btnAdd_Click(object sender, EventArgs e)
        {
            if (ddlProcessEquipment.SelectedIndex == 0)
            {
                ShowMsgHelper.Alert_Wern("请选择“处理设备”");
                return;
            }
            actionMode = "Add";
            Model = new EGMNGS.Model.RegGasBatch(actionMode);
            Model.ProcessEquipment = this.ddlProcessEquipment.SelectedValue;
            Model.RegistantsDate = DateTime.Now;
            Model.RegistantsOID = User.UserId.ToString();
            Model.Status = "0";
            Model.AmountGas = 200;
            InitControl();
        }


        protected void btnDel_Click(object sender, EventArgs e)
        {
            if (Model.OID == 0)
            {
                return;
            }
            if (Model.Status != "0")
            {
                ShowMsgHelper.Alert_Wern("“已提交”状态不可以删除");
            }
            else
            {
                bll.Delete(Model.OID);
                Model = new EGMNGS.Model.RegGasBatch();
                InitControl();
                DataBindGrid();
                ShowMsgHelper.Alert("删除成功！");

            }
        }

        protected void rp_Item_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            int oid = Convert.ToInt32(e.CommandArgument);

            Model = bll.GetModel(oid);
            InitControl();
            actionMode = "Edit";
        }
        #endregion
    }
}
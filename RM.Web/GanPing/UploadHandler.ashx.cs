﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RM.Common.DotNetFile;
using DapperData;
using EGMNGS.Model;
using RM.Common.DotNetBean;
using System.Web.SessionState;
using Aspose.Cells;
using System.Data;
using EGMNGS.Common;
using Maticsoft.DBUtility;

namespace RM.Web.GanPing
{
    /// <summary>
    /// UploadHandler 的摘要说明
    /// </summary>
    public class UploadHandler : IHttpHandler, IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            // string Magnetic_CheckPointTemplatesB_OID = context.Request["Magnetic_CheckPointTemplatesB_OID"] ?? "";
            string action = context.Request["action"] ?? "";
            //  string fileClass = context.Request["fileClass"] ?? "";
            string returnMsg = string.Empty;

            if (action == "ImportInfoDevParaPage")
            {
                returnMsg = ImportInfoDevParaPage(context);//导入
            }





            context.Response.Write(returnMsg);
        }

        private string ImportInfoDevParaPage(HttpContext context)
        {
            string retulrMSG = string.Empty;

            try
            {
                string username = RequestSession.GetSessionUser().UserName.ToString();



                HttpPostedFile file = context.Request.Files[0];
                string srcExt = file.FileName;
                string phypath = context.Server.MapPath("~/Files/");
                file.SaveAs(phypath + srcExt);

                Workbook workbook = new Workbook();
                workbook.Open(phypath + srcExt);
                Cells cells = workbook.Worksheets[0].Cells;
                System.Data.DataTable dataTable1 = cells.ExportDataTable(1, 0, cells.MaxDataRow, cells.MaxColumn + 1);
                System.Data.DataTable resutlsTable = EGMNGS.Common.ComServies.Query(@"SELECT    PowerSupplycode,	
                                                                                                PowerSupplyName,
                                                                                                ConvertStationCode,	
                                                                                                ConvertStationName,	
                                                                                                DevNum,	
                                                                                                DevName,	
                                                                                                DevType,
                                                                                                RunCode,
                                                                                                VoltageLevel,
                                                                                                DevClass,	
                                                                                                Manufacturer,	
                                                                                                RatedQL,	
                                                                                                RatedQY,	
                                                                                                KV,	
                                                                                                A,	
                                                                                                KA,	
                                                                                                PurchaseDate,
                                                                                                PutDate,
                                                                                                ReturnDate,
                                                                                                OutDevID,
                                                                                                OutDevUniqueCode,
                                                                                                OutDevStatus,	
                                                                                                OutManufacturerCurName,
                                                                                                OutProvider,
                                                                                                OutVoltageLevel,
                                                                                                OutDevClass,
                                                                                                OutFullPath,
                                                                                                OutGoinCurStatus,
                                                                                                OutOriFactoryWarranty,
                                                                                                OutUnits,
                                                                                                OutDevCoutOrLength,
                                                                                                OutDevCPISDept,	
                                                                                                OutStorageTeam,
                                                                                                OutIsPropertyDev,
                                                                                                OutPropertyCode,
                                                                                                OutIntervalName  from  InfoDevPara
                                                                                            where 1=0").Copy();

                LoadDataFromEls(resutlsTable, ref dataTable1);

                var lm = Common.DotNetData.DataTableHelper.DataTableToIList<InfoDevPara>(dataTable1) as IEnumerable<InfoDevPara>;

                Dictionary<string, Dictionary<string, string>> dict = HttpContext.Current.Session["DictEGMNS"] as Dictionary<string, Dictionary<string, string>>;

                int m_MaxNum = DbHelperSQL.GetSingle("select MaxNum from CodeTypeTb where CodeType='SB'").AsTargetType<int>(0);
                foreach (var item in lm)
                {
                    item.DevCode = "SB" + m_MaxNum.ToString("D6");
                    item.CREATED_BY = username;
                    item.CREATED_DATE = DateTime.Now;
                    item.DevClass = dict["DEVClass"].FirstOrDefault(o => o.Value == item.DevClass).Key;
                    item.VoltageLevel = dict["DYLev"].FirstOrDefault(o => o.Value == item.VoltageLevel).Key;
                    m_MaxNum++;
                }

                Repository _Repositor = new Repository();
                _Repositor.InsertBatch<InfoDevPara>(SessionFactory.CreateConnection(), lm);

                DbHelperSQL.ExecuteSql(string.Format("UPDATE dbo.CodeTypeTb SET MaxNum=MaxNum+{0} WHERE CodeType='SB'", m_MaxNum));

                retulrMSG = "导入成功";
            }
            catch (Exception ex)
            {
                retulrMSG = "导入失败:" + ex.Message;
            }

            return retulrMSG;
        }

        /// <summary>
        /// 从XLS中加载数据到datable中
        /// </summary>
        /// <param name="dataTable1"></param>
        private void LoadDataFromEls(DataTable fromTable, ref DataTable dataTable1)
        {
            for (int i = 0; i < fromTable.Columns.Count; i++)
            {
                dataTable1.Columns[i].ColumnName = fromTable.Columns[i].ColumnName;
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}
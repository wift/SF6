﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RPTGasSFCTable.aspx.cs"
    Inherits="RM.Web.Report.RPTGasSFCTable" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>气体收发存表</title>
    <link href="~/Themes/Styles/Site.css" rel="stylesheet" type="text/css" />
    <script src="/Themes/Scripts/Validator/JValidator.js" type="text/javascript"></script>
    <script src="/Themes/Scripts/jquery-1.8.2.min.js" type="text/javascript"></script>
    <script src="/Themes/Scripts/jquery.pullbox.js" type="text/javascript"></script>
    <script src="/Themes/Scripts/FunctionJS.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {
            InitControl();
        })
        function InitControl() {
            $(".div-body").PullBox({ dv: $(".div-body"), obj: $("#table1").find("tr") });
            divresize(60);
        }
    </script>
    <style type="text/css">
        .style1
        {
            height: 66.75pt;
            width: 71pt;
            color: windowtext;
            font-size: 10.0pt;
            font-weight: 700;
            font-style: normal;
            text-decoration: none;
            font-family: 宋体;
            text-align: center;
            vertical-align: middle;
            white-space: normal;
            border: .5pt solid windowtext;
            padding-left: 1px;
            padding-right: 1px;
            padding-top: 1px;
        }
        .style2
        {
            width: 228pt;
            color: windowtext;
            font-size: 10.0pt;
            font-weight: 700;
            font-style: normal;
            text-decoration: none;
            font-family: 宋体;
            text-align: center;
            vertical-align: middle;
            white-space: nowrap;
            border: .5pt solid windowtext;
            padding-left: 1px;
            padding-right: 1px;
            padding-top: 1px;
        }
        .style3
        {
            height: 18.0pt;
            width: 76pt;
            color: windowtext;
            font-size: 10.0pt;
            font-weight: 700;
            font-style: normal;
            text-decoration: none;
            font-family: 宋体;
            text-align: center;
            vertical-align: middle;
            white-space: normal;
            border-left: .5pt solid windowtext;
            border-right-style: none;
            border-right-color: inherit;
            border-right-width: medium;
            border-top: .5pt solid windowtext;
            border-bottom: .5pt solid windowtext;
            padding-left: 1px;
            padding-right: 1px;
            padding-top: 1px;
        }
        .style4
        {
            width: 76pt;
            color: windowtext;
            font-size: 10.0pt;
            font-weight: 700;
            font-style: normal;
            text-decoration: none;
            font-family: 宋体;
            text-align: center;
            vertical-align: middle;
            white-space: normal;
            border-left: .5pt solid windowtext;
            border-right-style: none;
            border-right-color: inherit;
            border-right-width: medium;
            border-top: .5pt solid windowtext;
            border-bottom: .5pt solid windowtext;
            padding-left: 1px;
            padding-right: 1px;
            padding-top: 1px;
        }
        .style5
        {
            height: 30.75pt;
            width: 38pt;
            color: windowtext;
            font-size: 10.0pt;
            font-weight: 700;
            font-style: normal;
            text-decoration: none;
            font-family: 宋体;
            text-align: center;
            vertical-align: middle;
            white-space: normal;
            border: .5pt solid windowtext;
            padding-left: 1px;
            padding-right: 1px;
            padding-top: 1px;
        }
        .style6
        {
            width: 38pt;
            color: windowtext;
            font-size: 10.0pt;
            font-weight: 700;
            font-style: normal;
            text-decoration: none;
            font-family: 宋体;
            text-align: center;
            vertical-align: middle;
            white-space: normal;
            border: .5pt solid windowtext;
            padding-left: 1px;
            padding-right: 1px;
            padding-top: 1px;
        }
        .style7
        {
            height: 19.5pt;
            color: windowtext;
            font-size: 10.0pt;
            font-weight: 400;
            font-style: normal;
            text-decoration: none;
            font-family: 宋体;
            text-align: center;
            vertical-align: middle;
            white-space: nowrap;
            border: .5pt solid windowtext;
            padding-left: 1px;
            padding-right: 1px;
            padding-top: 1px;
        }
        .style8
        {
            color: windowtext;
            font-size: 10.0pt;
            font-weight: 400;
            font-style: normal;
            text-decoration: none;
            font-family: 宋体;
            text-align: center;
            vertical-align: middle;
            white-space: nowrap;
            border: .5pt solid windowtext;
            padding-left: 1px;
            padding-right: 1px;
            padding-top: 1px;
        }
        .style9
        {
            width: 38pt;
            color: windowtext;
            font-size: 10.0pt;
            font-weight: 400;
            font-style: normal;
            text-decoration: none;
            font-family: 仿宋_GB2312, monospace;
            text-align: center;
            vertical-align: middle;
            white-space: normal;
            border: .5pt solid windowtext;
            padding-left: 1px;
            padding-right: 1px;
            padding-top: 1px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional">
        <ContentTemplate>
            <table width="100%">
                <colgroup>
                    <col width="5%" />
                    <col width="6%" />
                    <col width="5%" />
                    <col width="6%" />
                    <col width="70%" />
                </colgroup>
                <tr>
                    <td class="inner_cell_right">
                        年度:
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlYear" runat="server">
                        </asp:DropDownList>
                    </td>
                    <td class="inner_cell_right">
                        月度:
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlMonth" runat="server">
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:Button ID="btnSearch" runat="server" OnClick="btnSearch_Click" Text="查询" />
                        <asp:Button ID="btnExport" runat="server" Text="导出" OnClick="btnExport_Click" />
                    </td>
                </tr>
            </table>
            <div class="div-body">
                <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse;
                    width: 983pt" width="1318">
                    <colgroup>
                        <col width="94" />
                        <col span="24" width="51" />
                    </colgroup>
                    <tr height="24">
                        <td class="style1" height="89" rowspan="3" width="94">
                            钢瓶容量
                        </td>
                        <td class="style2" colspan="6" width="306">
                            回收气体
                        </td>
                        <td class="style2" colspan="6" width="306">
                            净化气体
                        </td>
                        <td class="style2" colspan="6" width="306">
                            新购气体
                        </td>
                        <td class="style2" colspan="6" width="306">
                            合格气体
                        </td>
                    </tr>
                    <tr height="24">
                        <td class="style3" colspan="2" height="24" width="102">
                            入库
                        </td>
                        <td class="style4" colspan="2" width="102">
                            出库
                        </td>
                        <td class="style4" colspan="2" width="102">
                            库存
                        </td>
                        <td class="style4" colspan="2" width="102">
                            入库
                        </td>
                        <td class="style4" colspan="2" width="102">
                            出库
                        </td>
                        <td class="style4" colspan="2" width="102">
                            库存
                        </td>
                        <td class="style4" colspan="2" width="102">
                            入库
                        </td>
                        <td class="style4" colspan="2" width="102">
                            出库
                        </td>
                        <td class="style4" colspan="2" width="102">
                            库存
                        </td>
                        <td class="style4" colspan="2" width="102">
                            入库
                        </td>
                        <td class="style4" colspan="2" width="102">
                            出库
                        </td>
                        <td class="style4" colspan="2" width="102">
                            库存
                        </td>
                    </tr>
                    <tr height="41">
                        <td class="style5" height="41" width="51">
                            数量
                        </td>
                        <td class="style6" width="51">
                            重量（kg）
                        </td>
                        <td class="style6" width="51">
                            数量
                        </td>
                        <td class="style6" width="51">
                            重量（kg）
                        </td>
                        <td class="style6" width="51">
                            数量
                        </td>
                        <td class="style6" width="51">
                            重量（kg）
                        </td>
                        <td class="style6" width="51">
                            数量
                        </td>
                        <td class="style6" width="51">
                            重量（kg）
                        </td>
                        <td class="style6" width="51">
                            数量
                        </td>
                        <td class="style6" width="51">
                            重量（kg）
                        </td>
                        <td class="style6" width="51">
                            数量
                        </td>
                        <td class="style6" width="51">
                            重量（kg）
                        </td>
                        <td class="style6" width="51">
                            数量
                        </td>
                        <td class="style6" width="51">
                            重量（kg）
                        </td>
                        <td class="style6" width="51">
                            数量
                        </td>
                        <td class="style6" width="51">
                            重量（kg）
                        </td>
                        <td class="style6" width="51">
                            数量
                        </td>
                        <td class="style6" width="51">
                            重量（kg）
                        </td>
                        <td class="style6" width="51">
                            数量
                        </td>
                        <td class="style6" width="51">
                            重量（kg）
                        </td>
                        <td class="style6" width="51">
                            数量
                        </td>
                        <td class="style6" width="51">
                            重量（kg）
                        </td>
                        <td class="style6" width="51">
                            数量
                        </td>
                        <td class="style6" width="51">
                            重量（kg）
                        </td>
                    </tr>
                    <tr height="26">
                        <td class="style7" height="26">
                            小钢瓶（25kg）
                        </td>
                        <td class="style8">
                            <%= Convert.ToInt32(dtRPTGasSFCTable.Rows[0]["RecyCount"])%>
                        </td>
                        <td class="style8">
                            <%= Convert.ToInt32(dtRPTGasSFCTable.Rows[0]["RecyTotal"])%>
                        </td>
                        <td class="style8">
                            <%= Convert.ToInt32(dtRPTGasSFCTable.Rows[0]["RecyOutCount"])%>
                        </td>
                        <td class="style8">
                            <%= Convert.ToInt32(dtRPTGasSFCTable.Rows[0]["RecyOut"])%>
                        </td>
                        <td class="style8">
                            <%= Convert.ToInt32(dtRPTGasSFCTable.Rows[0]["RecyInCount"])%>
                        </td>
                        <td class="style8">
                            <%= Convert.ToInt32(dtRPTGasSFCTable.Rows[0]["RecyIn"])%>
                        </td>
                        <td class="style8">
                         <%= Convert.ToInt32(dtRPTGasSFCTable.Rows[0]["JHCount"])%>
                        </td>
                        <td class="style8">
                            <%= Convert.ToInt32(dtRPTGasSFCTable.Rows[0]["JHTotal"])%>
                        </td>
                        <td class="style8">
                          <%= Convert.ToInt32(dtRPTGasSFCTable.Rows[0]["JHOutCount"])%>
                        </td>
                        <td class="style8">
                            <%= Convert.ToInt32(dtRPTGasSFCTable.Rows[0]["JHOut"])%>
                        </td>
                        <td class="style9" width="51">
                         <%= Convert.ToInt32(dtRPTGasSFCTable.Rows[0]["JHInCount"])%>
                        </td>
                        <td class="style9" width="51">
                            <%= Convert.ToInt32(dtRPTGasSFCTable.Rows[0]["JHIn"])%>
                        </td>
                        <td class="style8">
                         <%= Convert.ToInt32(dtRPTGasSFCTable.Rows[0]["XGCount"])%>
                        </td>
                        <td class="style8">
                            <%= Convert.ToInt32(dtRPTGasSFCTable.Rows[0]["XGTotal"])%>
                        </td>
                        <td class="style8">
                          <%= Convert.ToInt32(dtRPTGasSFCTable.Rows[0]["XGOutCount"])%>
                        </td>
                        <td class="style8">
                            <%= Convert.ToInt32(dtRPTGasSFCTable.Rows[0]["XGOut"])%>
                        </td>
                        <td class="style8">
                           <%= Convert.ToInt32(dtRPTGasSFCTable.Rows[0]["XGInCount"])%>
                        </td>
                        <td class="style8">
                            <%= Convert.ToInt32(dtRPTGasSFCTable.Rows[0]["XGIn"])%>
                        </td>
                        <td class="style8">
                           <%= Convert.ToInt32(dtRPTGasSFCTable.Rows[0]["HGCount"])%>
                        </td>
                        <td class="style8">
                            <%= Convert.ToInt32(dtRPTGasSFCTable.Rows[0]["HGTotal"])%>
                        </td>
                        <td class="style8">
                         <%= Convert.ToInt32(dtRPTGasSFCTable.Rows[0]["HGOutCount"])%>
                        </td>
                        <td class="style8">
                            <%= Convert.ToInt32(dtRPTGasSFCTable.Rows[0]["HGOut"])%>
                        </td>
                        <td class="style8">
                         <%= Convert.ToInt32(dtRPTGasSFCTable.Rows[0]["HGInCount"])%>
                        </td>
                        <td class="style8">
                            <%= Convert.ToInt32(dtRPTGasSFCTable.Rows[0]["HGIn"])%>
                        </td>
                    </tr>
                    <tr height="26">
                        <td class="style7" height="26">
                            大钢瓶（50kg）
                        </td>
                        <td class="style8">
                            <%= Convert.ToInt32(dtRPTGasSFCTable.Rows[1]["RecyCount"])%>
                        </td>
                        <td class="style8">
                            <%= Convert.ToInt32(dtRPTGasSFCTable.Rows[1]["RecyTotal"])%>
                        </td>
                        <td class="style8">
                            <%= Convert.ToInt32(dtRPTGasSFCTable.Rows[1]["RecyOutCount"])%>
                        </td>
                        <td class="style8">
                            <%= Convert.ToInt32(dtRPTGasSFCTable.Rows[1]["RecyOut"])%>
                        </td>
                        <td class="style8">
                            <%= Convert.ToInt32(dtRPTGasSFCTable.Rows[1]["RecyInCount"])%>
                        </td>
                        <td class="style8">
                            <%= Convert.ToInt32(dtRPTGasSFCTable.Rows[1]["RecyIn"])%>
                        </td>
                        <td class="style8">
                         <%= Convert.ToInt32(dtRPTGasSFCTable.Rows[1]["JHCount"])%>
                        </td>
                        <td class="style8">
                            <%= Convert.ToInt32(dtRPTGasSFCTable.Rows[1]["JHTotal"])%>
                        </td>
                        <td class="style8">
                          <%= Convert.ToInt32(dtRPTGasSFCTable.Rows[1]["JHOutCount"])%>
                        </td>
                        <td class="style8">
                            <%= Convert.ToInt32(dtRPTGasSFCTable.Rows[1]["JHOut"])%>
                        </td>
                        <td class="style9" width="51">
                         <%= Convert.ToInt32(dtRPTGasSFCTable.Rows[1]["JHInCount"])%>
                        </td>
                        <td class="style9" width="51">
                            <%= Convert.ToInt32(dtRPTGasSFCTable.Rows[1]["JHIn"])%>
                        </td>
                        <td class="style8">
                         <%= Convert.ToInt32(dtRPTGasSFCTable.Rows[1]["XGCount"])%>
                        </td>
                        <td class="style8">
                            <%= Convert.ToInt32(dtRPTGasSFCTable.Rows[1]["XGTotal"])%>
                        </td>
                        <td class="style8">
                          <%= Convert.ToInt32(dtRPTGasSFCTable.Rows[1]["XGOutCount"])%>
                        </td>
                        <td class="style8">
                            <%= Convert.ToInt32(dtRPTGasSFCTable.Rows[1]["XGOut"])%>
                        </td>
                        <td class="style8">
                           <%= Convert.ToInt32(dtRPTGasSFCTable.Rows[1]["XGInCount"])%>
                        </td>
                        <td class="style8">
                            <%= Convert.ToInt32(dtRPTGasSFCTable.Rows[1]["XGIn"])%>
                        </td>
                        <td class="style8">
                           <%= Convert.ToInt32(dtRPTGasSFCTable.Rows[1]["HGCount"])%>
                        </td>
                        <td class="style8">
                            <%= Convert.ToInt32(dtRPTGasSFCTable.Rows[1]["HGTotal"])%>
                        </td>
                        <td class="style8">
                         <%= Convert.ToInt32(dtRPTGasSFCTable.Rows[1]["HGOutCount"])%>
                        </td>
                        <td class="style8">
                            <%= Convert.ToInt32(dtRPTGasSFCTable.Rows[1]["HGOut"])%>
                        </td>
                        <td class="style8">
                         <%= Convert.ToInt32(dtRPTGasSFCTable.Rows[1]["HGInCount"])%>
                        </td>
                        <td class="style8">
                            <%= Convert.ToInt32(dtRPTGasSFCTable.Rows[1]["HGIn"])%>
                        </td>
                    </tr>
                    <tr height="26">
                        <td class="style7" height="26">
                            合计
                        </td>
                       <td class="style8">
                            <%= Convert.ToInt32(dtRPTGasSFCTable.Rows[2]["RecyCount"])%>
                        </td>
                        <td class="style8">
                            <%= Convert.ToInt32(dtRPTGasSFCTable.Rows[2]["RecyTotal"])%>
                        </td>
                        <td class="style8">
                            <%= Convert.ToInt32(dtRPTGasSFCTable.Rows[2]["RecyOutCount"])%>
                        </td>
                        <td class="style8">
                            <%= Convert.ToInt32(dtRPTGasSFCTable.Rows[2]["RecyOut"])%>
                        </td>
                        <td class="style8">
                            <%= Convert.ToInt32(dtRPTGasSFCTable.Rows[2]["RecyInCount"])%>
                        </td>
                        <td class="style8">
                            <%= Convert.ToInt32(dtRPTGasSFCTable.Rows[2]["RecyIn"])%>
                        </td>
                        <td class="style8">
                         <%= Convert.ToInt32(dtRPTGasSFCTable.Rows[2]["JHCount"])%>
                        </td>
                        <td class="style8">
                            <%= Convert.ToInt32(dtRPTGasSFCTable.Rows[2]["JHTotal"])%>
                        </td>
                        <td class="style8">
                          <%= Convert.ToInt32(dtRPTGasSFCTable.Rows[2]["JHOutCount"])%>
                        </td>
                        <td class="style8">
                            <%= Convert.ToInt32(dtRPTGasSFCTable.Rows[2]["JHOut"])%>
                        </td>
                        <td class="style9" width="51">
                         <%= Convert.ToInt32(dtRPTGasSFCTable.Rows[2]["JHInCount"])%>
                        </td>
                        <td class="style9" width="51">
                            <%= Convert.ToInt32(dtRPTGasSFCTable.Rows[2]["JHIn"])%>
                        </td>
                        <td class="style8">
                         <%= Convert.ToInt32(dtRPTGasSFCTable.Rows[2]["XGCount"])%>
                        </td>
                        <td class="style8">
                            <%= Convert.ToInt32(dtRPTGasSFCTable.Rows[2]["XGTotal"])%>
                        </td>
                        <td class="style8">
                          <%= Convert.ToInt32(dtRPTGasSFCTable.Rows[2]["XGOutCount"])%>
                        </td>
                        <td class="style8">
                            <%= Convert.ToInt32(dtRPTGasSFCTable.Rows[2]["XGOut"])%>
                        </td>
                        <td class="style8">
                           <%= Convert.ToInt32(dtRPTGasSFCTable.Rows[2]["XGInCount"])%>
                        </td>
                        <td class="style8">
                            <%= Convert.ToInt32(dtRPTGasSFCTable.Rows[2]["XGIn"])%>
                        </td>
                        <td class="style8">
                           <%= Convert.ToInt32(dtRPTGasSFCTable.Rows[2]["HGCount"])%>
                        </td>
                        <td class="style8">
                            <%= Convert.ToInt32(dtRPTGasSFCTable.Rows[2]["HGTotal"])%>
                        </td>
                        <td class="style8">
                         <%= Convert.ToInt32(dtRPTGasSFCTable.Rows[2]["HGOutCount"])%>
                        </td>
                        <td class="style8">
                            <%= Convert.ToInt32(dtRPTGasSFCTable.Rows[2]["HGOut"])%>
                        </td>
                        <td class="style8">
                         <%= Convert.ToInt32(dtRPTGasSFCTable.Rows[2]["HGInCount"])%>
                        </td>
                        <td class="style8">
                            <%= Convert.ToInt32(dtRPTGasSFCTable.Rows[2]["HGIn"])%>
                        </td>
                    </tr>
                </table>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnSearch" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>
    </form>
</body>
</html>

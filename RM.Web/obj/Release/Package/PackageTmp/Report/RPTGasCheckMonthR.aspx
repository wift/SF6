﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RPTGasCheckMonthR.aspx.cs"
    Inherits="RM.Web.Report.RPTGasCheckMonthR" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>气体检测年报</title>

    <style type="text/css">
        .auto-style1
        {
            height: 25.5pt;
            width: 656pt;
            color: windowtext;
            font-size: 20.0pt;
            font-weight: 700;
            font-style: normal;
            text-decoration: none;
            font-family: 宋体;
            text-align: center;
            vertical-align: middle;
            white-space: nowrap;
            border: .5pt solid windowtext;
            padding-left: 1px;
            padding-right: 1px;
            padding-top: 1px;
        }

        .auto-style2
        {
            height: 37.5pt;
            width: 71pt;
            color: windowtext;
            font-size: 10.0pt;
            font-weight: 700;
            font-style: normal;
            text-decoration: none;
            font-family: 宋体;
            text-align: center;
            vertical-align: middle;
            white-space: normal;
            border: .5pt solid windowtext;
            padding-left: 1px;
            padding-right: 1px;
            padding-top: 1px;
        }

        .auto-style3
        {
            width: 195pt;
            color: windowtext;
            font-size: 10.0pt;
            font-weight: 700;
            font-style: normal;
            text-decoration: none;
            font-family: 宋体;
            text-align: center;
            vertical-align: middle;
            white-space: normal;
            border: .5pt solid windowtext;
            padding-left: 1px;
            padding-right: 1px;
            padding-top: 1px;
        }

        .auto-style4
        {
            height: 24.0pt;
            width: 65pt;
            color: windowtext;
            font-size: 10.0pt;
            font-weight: 700;
            font-style: normal;
            text-decoration: none;
            font-family: 宋体;
            text-align: center;
            vertical-align: middle;
            white-space: normal;
            border: .5pt solid windowtext;
            padding-left: 1px;
            padding-right: 1px;
            padding-top: 1px;
        }

        .auto-style5
        {
            width: 65pt;
            color: windowtext;
            font-size: 10.0pt;
            font-weight: 700;
            font-style: normal;
            text-decoration: none;
            font-family: 宋体;
            text-align: center;
            vertical-align: middle;
            white-space: normal;
            border: .5pt solid windowtext;
            padding-left: 1px;
            padding-right: 1px;
            padding-top: 1px;
        }

        .auto-style6
        {
            height: 13.5pt;
            color: windowtext;
            font-size: 10.0pt;
            font-weight: 700;
            font-style: normal;
            text-decoration: none;
            font-family: 宋体;
            text-align: center;
            vertical-align: middle;
            white-space: nowrap;
            border: .5pt solid windowtext;
            padding-left: 1px;
            padding-right: 1px;
            padding-top: 1px;
        }

        .auto-style7
        {
            color: windowtext;
            font-size: 10.0pt;
            font-weight: 700;
            font-style: normal;
            text-decoration: none;
            font-family: 宋体;
            text-align: center;
            vertical-align: middle;
            white-space: nowrap;
            border: .5pt solid windowtext;
            padding-left: 1px;
            padding-right: 1px;
            padding-top: 1px;
        }
    </style>

</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional">
            <ContentTemplate>
                <table width="100%">
                    <colgroup>
                        <col width="5%" />
                        <col width="6%" />
                        <col width="70%" />
                    </colgroup>
                    <tr>
                        <td class="inner_cell_right">年度:
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlYear" runat="server">
                            </asp:DropDownList>
                        </td>
                        <td>
                            <asp:Button ID="btnSearch" runat="server" OnClick="btnSearch_Click" Text="查询" />
                            <asp:Button ID="btnExport" runat="server" Text="导出" OnClick="btnExport_Click" />
                        </td>
                    </tr>
                </table>
                <div class="div-body">

                    <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse; width: 656pt"
                        width="868">
                        <colgroup>
                            <col width="94" />
                            <col span="9" width="86" />
                        </colgroup>
                        <tr height="34">
                            <td class="auto-style1" colspan="10" height="34" width="868">六氟化硫检测中心气体品质检测年报</td>
                        </tr>
                        <tr height="18">
                            <td class="auto-style2" height="50" rowspan="2" width="94">月度</td>
                            <td class="auto-style3" colspan="3" width="258">新购气体</td>
                            <td class="auto-style3" colspan="3" width="258">入网气体</td>
                            <td class="auto-style3" colspan="3" width="258">净化气体</td>
                        </tr>
                        <tr height="32">
                            <td class="auto-style4" height="32" width="86">未检测</td>
                            <td class="auto-style5" width="86">已检合格</td>
                            <td class="auto-style5" width="86">已检不合格</td>
                            <td class="auto-style5" width="86">未检测</td>
                            <td class="auto-style5" width="86">已检合格</td>
                            <td class="auto-style5" width="86">已检不合格</td>
                            <td class="auto-style5" width="86">未检测</td>
                            <td class="auto-style5" width="86">已检合格</td>
                            <td class="auto-style5" width="86">已检不合格</td>
                        </tr>
                        <asp:Repeater ID="rp_Item" runat="server" >
                            <ItemTemplate>
                                <tr height="18">
                                    <td class="auto-style6" height="18"><%#Eval("Month")%></td>
                                    <td class="auto-style7"><%#Eval("未检测x")%> </td>
                                    <td class="auto-style7"><%#Eval("已检合格x")%></td>
                                    <td class="auto-style7"><%#Eval("已检不合格x")%> </td>
                                    <td class="auto-style7"><%#Eval("未检测r")%></td>
                                    <td class="auto-style7"><%#Eval("已检合格r")%></td>
                                    <td class="auto-style7"><%#Eval("已检不合格r")%></td>
                                    <td class="auto-style7"><%#Eval("未检测h")%> </td>
                                    <td class="auto-style7"><%#Eval("已检合格h")%></td>
                                    <td class="auto-style7"><%#Eval("已检不合格h")%></td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </table>

                </div>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnSearch" EventName="Click" />
            </Triggers>
        </asp:UpdatePanel>
    </form>
</body>
</html>

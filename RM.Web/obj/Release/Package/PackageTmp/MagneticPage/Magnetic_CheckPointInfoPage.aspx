﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Magnetic_CheckPointInfoPage.aspx.cs" Inherits="RM.Web.MagneticPage.Magnetic_CheckPointInfoPage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <script type="text/javascript" src="../Themes/Scripts/jqueryeasyui/jquery.min.js"></script>
    <link href="../Themes/Scripts/jqueryeasyui/themes/bootstrap/easyui.css" rel="stylesheet" />
    <link href="../Themes/Scripts/jqueryeasyui/themes/icon.css" rel="stylesheet" />
    <link href="../Themes/Scripts/jqueryeasyui/themes/color.css" rel="stylesheet" />
    <link href="/Themes/Scripts/TreeView/treeview.css" rel="stylesheet" type="text/css" />
    <script src="/Themes/Scripts/TreeView/treeview.pack.js" type="text/javascript"></script>
    <script src="/Themes/Scripts/FunctionJS.js" type="text/javascript"></script>

    <title>站点测点信息</title>
    <style type="text/css">
        html, body {
            margin: 0;
            height: 100%;
        }

        form {
            height: 100%;
            width: 100%;
        }

        #containt {
            float: left;
            height: -moz-calc(100% - 0px);
            height: -webkit-calc(100% - 00px);
            height: calc(100% - 0px);
            width: -moz-calc(100% - 0px);
            width: -webkit-calc(100% - 00px);
            width: calc(100% - 0px);
        }

        .Panel {
            float: left;
        }



        object {
            left: 0px;
            width: 100%;
        }
    </style>


</head>
<body>
    <form id="form1" runat="server">
        <div id="containt">
            <div id="leftPanel" class="Panel" style="width: 15%; height: 100%; border-right-width: 1px; border-right-style: solid;">
                <ul class="strTree">
                    <li>
                        <div title="广东电网公司">
                            广东电网公司
                        </div>
                        <ul>
                            <%=treeItem_Table.ToString() %>
                        </ul>
                    </li>
                </ul>
            </div>
            <div id="rightPanel" class="Panel" style="width: 84%; height: 100%;">
                <table id="Magnetic_CheckPointInfoList">
                </table>
                <table id="fileList"></table>
            </div>
        </div>
    </form>

    <script type="text/javascript" src="../Themes/Scripts/highcharts.js"></script>
    <script type="text/javascript" src="../Themes/Scripts/jqueryeasyui/jquery.easyui.min.js"></script>
    <script src="../Themes/Scripts/jqueryeasyui/plugins/jquery.edatagrid.js" type="text/javascript"></script>
    <script src="../Scripts/uploadify/jquery.uploadify.min.js" type="text/javascript"></script>
    <script type="text/javascript">

        var arrRuns = eval('<%= RunStatusStrJson%>');
        var arrVoltageLevel = eval('<%= VoltageLevelStrJson%>');
        //初始化
        $(function () {
            treeAttrCss();
            $.lightTreeview.close('.strTree ul ul', 100);
        })

        function GetTable(code, ConvertStationName) {
            LoadMc_CheckPointTemp_ChildsList("'" + code + "'", ConvertStationName)
            Loading(true);
        }
        function GetTableByPowerSupplyCode(PowerSupplyCode, thiss) {
            var codejoin = '';
            $(thiss).next().children().each(function myfunction() {

                codejoin += "'" + $(this).attr("id") + "',";

            });
            LoadMc_CheckPointTemp_ChildsList(codejoin)
            LoadFileList("");
        }

        function LoadMc_CheckPointTemp_ChildsList(convertStationCode, convertStationName) {
            var $width = $("#rightPanel").width();
            var $height = $("#rightPanel").height() / 2;
            var editRow = undefined;
            var $datagrid = $("#Magnetic_CheckPointInfoList");
            $datagrid.datagrid({
                height: $height,
                width: $width,
                fitColumns: true,
                collapsible: true,
                singleSelect: true,
                url: 'Magnetic_CheckPointInfoHandler.ashx',
                queryParams: { action: 'GetMagnetic_CheckPointInfoList', ConvertStationCode: convertStationCode },
                idField: 'OID',
                columns: [[
                 { field: 'OID', title: 'ID', hidden: true },
                  {
                      field: 'SortNo', width: 50, title: "序号", align: 'center', editor: { type: 'text', options: { required: true } }
                  },
                  {
                      field: 'CheckPointCode', width: 100, title: "测点编码", editor: { type: 'text', options: { required: true } }
                  },
         { field: 'CheckPointName', align: 'center', width: 250, title: '测点名称', editor: { type: 'text', options: { required: true } } },
         { field: 'InstrumentCode', align: 'center', width: 100, title: '仪器编码', editor: { type: 'text', options: { required: true } } },
         { field: 'InstrumentName', align: 'center', width: 200, title: '仪器名称', editor: { type: 'text', options: { required: true } } },
         {
             field: 'RunStatus', align: 'center', width: 150, title: '运行状态', editor: {
                 type: 'combobox',
                 options: {
                     valueField: 'key',
                     textField: 'value',
                     data: arrRuns,
                     required: true
                 }
             },
             formatter: function (value) {

                 for (var i in arrRuns) {
                     if (arrRuns[i].key == value) {
                         return arrRuns[i].value;
                     }
                 }


             }
         },
         { field: 'CheckPointPositionName', align: 'center', width: 250, title: '测点位置', editor: { type: 'text', options: { required: true } } },
         { field: 'Longitude', align: 'center', width: 150, title: '经度', editor: { type: 'text', options: { required: true } } },
         { field: 'Latitude', align: 'center', width: 150, title: '纬度', editor: { type: 'text', options: { required: true } } },
         {
             field: 'VoltageLevel', align: 'center', width: 120, title: '电压等级',
             editor: {
                 type: 'combobox',
                 options: {
                     valueField: 'key',
                     textField: 'value',
                     data: arrVoltageLevel,
                     required: true
                 }
             }, formatter: function (value) {

                 for (var i in arrVoltageLevel) {
                     if (arrVoltageLevel[i].key == value) {
                         return arrVoltageLevel[i].value;
                     }
                 }


             }

         },
    { field: 'Electricity', align: 'center', width: 120, title: '电流(A)', editor: { type: 'text', options: { required: true } } },
    { field: 'Eload', align: 'center', width: 120, title: '负荷(MW)', editor: { type: 'text', options: { required: true } } },
             { field: 'Remarks', align: 'center', width: 150, title: '备注', editor: { type: 'text', options: { required: true } } },

       { field: 'ConvertStationName', align: 'center', width: 150, title: '所属变电站' },
         { field: 'ConvertStationCode', align: 'center', width: 150, title: '所属变电站编码', hidden: true }
                ]],
                toolbar: [{
                    text: '添加', iconCls: 'icon-add', handler: function () {
                        if (editRow != undefined) {
                            $datagrid.datagrid('endEdit', editRow);
                        }
                        if (editRow == undefined) {

                            if (convertStationName == undefined) {
                                showTipsMsg("请选择变电站", 0, 3);
                                return;
                            }

                            $datagrid.datagrid('insertRow', {
                                index: 0,
                                row: { ConvertStationCode: convertStationCode.replace(new RegExp(/(')/g), ''), ConvertStationName: convertStationName }
                            });

                            $datagrid.datagrid('beginEdit', 0);
                            editRow = 0;
                        }
                    }
                }, '-', {
                    text: '保存', iconCls: 'icon-save', handler: function () {

                        var editIndex = editRow;

                        $datagrid.datagrid('endEdit', editRow);

                        $datagrid.datagrid('selectRow', editIndex)
                        var rowstr = $datagrid.datagrid('getSelected', editIndex)
                        //如果调用acceptChanges(),使用getChanges()则获取不到编辑和新增的数据。

                        //使用JSON序列化datarow对象，发送到后台。
                        //  var rows = $("#Student_Table").datagrid('getChanges');

                        //  var rowstr = JSON.stringify(rows[0]);
                        $.post('Magnetic_CheckPointInfoHandler.ashx?action=SaveMagnetic_CheckPointInfo', rowstr, function (data) {

                            showFaceMsg(data);
                        });

                    }
                }, '-', {
                    text: '删除', iconCls: 'icon-remove', handler: function () {
                        var row = $datagrid.datagrid('getSelected');
                        var index = $datagrid.datagrid('getRowIndex', row);


                        $.post('Magnetic_CheckPointInfoHandler.ashx?action=DelMagnetic_CheckPointInfo', { oid: row.OID }, function (data) {
                            if (data == "删除成功") {
                                $datagrid.datagrid('deleteRow', index);
                            }

                            showFaceMsg(data);

                        });

                    }
                }, '-', {
                    text: '修改', iconCls: 'icon-edit', handler: function () {
                        var row = $datagrid.datagrid('getSelected');
                        if (row != null) {
                            if (editRow != undefined) {
                                $datagrid.datagrid('endEdit', editRow);
                            }

                            if (editRow == undefined) {
                                var index = $datagrid.datagrid('getRowIndex', row);
                                $datagrid.datagrid('beginEdit', index);
                                editRow = index;
                                $datagrid.datagrid('unselectAll');
                            }
                        } else {

                        }
                    }
                },'-',{
                text: '刷新', iconCls: 'icon-reload', handler: function () {

                    $datagrid.datagrid("reload");
                }

            }],
            onAfterEdit: function (rowIndex, rowData, changes) {
                editRow = undefined;
            },
            onDblClickRow: function (rowIndex, rowData) {
                if (editRow != undefined) {
                    $datagrid.datagrid('endEdit', editRow);
                }

                if (editRow == undefined) {
                    $datagrid.datagrid('beginEdit', rowIndex);
                    editRow = rowIndex;
                }
            },
            onClickRow: function (rowIndex, rowData) {
                  
                LoadFileList(rowData.OID);
            }

        });
        }


        //加载文件列表
        function LoadFileList(magnetic_CheckPointTemplatesB_OID) {
            var _fileClass = "站点测点文件";
            var editRow = undefined;
            var $datagrid = $("#fileList");
            var $heightNoise = $("#rightPanel").height() / 2;
            var $widthNoise = $("#rightPanel").width();

            $datagrid.datagrid({
                height: $heightNoise - 10,
                width: $widthNoise - 10,
                fitColumns: true,
                collapsible: true,
                singleSelect: true,
                url: 'UploadHandler.ashx',
                queryParams: { action: 'Get_FilesList', Magnetic_CheckPointTemplatesB_OID: magnetic_CheckPointTemplatesB_OID, fileClass: _fileClass },
                idField: 'OID',
                columns: [[
                 { field: 'OID', title: 'ID', hidden: true },
                   {
                       field: 'FileName', width: 150, title: "文件名", align: 'center'
                   },

                  {
                      field: 'FileSize', width: 100, title: "文件大小", align: 'center'
                  },
                    {
                        field: 'CreateDate', width: 100, title: "上传时间", align: 'center'
                    }, {
                        field: 'RegistrantName', width: 100, title: "上传者", align: 'center'
                    }


                ]],
                toolbar: [{
                    text: ' <span id="uploadify" />', iconCls: 'folder_up', handler: function () {

                    }

                }, {
                    text: '下载', iconCls: 'package_down', handler: function () {

                        var editIndex = editRow;

                        $datagrid.datagrid('endEdit', editRow);

                        $datagrid.datagrid('selectRow', editIndex)
                        var rowstr = $datagrid.datagrid('getSelected', editIndex)
                        window.location = 'UploadHandler.ashx?action=DownLoad&url=' + rowstr.FilePath;


                    }
                }, {
                    text: '删除', iconCls: 'icon-remove', handler: function () {
                        var row = $datagrid.datagrid('getSelected');
                        var index = $datagrid.datagrid('getRowIndex', row);


                        $.post('UploadHandler.ashx?action=Del_Files', { oid: row.OID }, function (data) {
                            if (data == "删除成功") {
                                $datagrid.datagrid('deleteRow', index);
                            }

                            showFaceMsg(data);

                        });

                    }
                }],
                onAfterEdit: function (rowIndex, rowData, changes) {
                    editRow = undefined;
                },
                onDblClickRow: function (rowIndex, rowData) {
                    if (editRow != undefined) {
                        $datagrid.datagrid('endEdit', editRow);
                    }

                    if (editRow == undefined) {
                        $datagrid.datagrid('beginEdit', rowIndex);
                        editRow = rowIndex;
                    }
                },
                onClickRow: function (rowIndex, rowData) {
                    if (editRow != undefined) {
                        $datagrid.datagrid('endEdit', editRow);

                    }
                    editRow = rowIndex;
                }

            });


            $('#uploadify').uploadify({
                'formData': {
                    'Magnetic_CheckPointTemplatesB_OID': magnetic_CheckPointTemplatesB_OID,
                    'action': 'upload', fileClass: _fileClass
                },
                'swf': '../Scripts/uploadify/uploadify.swf',
                'uploader': 'UploadHandler.ashx',
                'buttonText': '上传',
                'width': 40,
                'queueID': true,
                onUploadComplete: function myfunction(file) {
                    LoadFileList(magnetic_CheckPointTemplatesB_OID)
                    alert('上传成功');
                }
            });
        }
    </script>
</body>
</html>

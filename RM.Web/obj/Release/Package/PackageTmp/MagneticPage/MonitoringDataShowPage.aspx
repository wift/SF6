﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MonitoringDataShowPage.aspx.cs" Inherits="RM.Web.MagneticPage.MonitoringDataShowPage" %>

<!DOCTYPE html>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">

    <title>监测数据展示</title>
    <link href="~/Themes/Styles/Site.css" rel="stylesheet" type="text/css" />

 <script type="text/javascript" src="../Themes/Scripts/jqueryeasyui/jquery.min.js"></script>
        <script src="../Themes/Scripts/Validator/JValidator.js" type="text/javascript"></script>
    <script src="../Themes/Scripts/jquery.pullbox.js" type="text/javascript"></script>
    <script src="../Themes/Scripts/FunctionJS.js" type="text/javascript"></script>
    <script src="../Themes/Scripts/DatePicker/WdatePicker.js" type="text/javascript"></script>
    <style type="text/css">
        html, body
        {
            margin: 0;
            height: 100%;
        }

        #containt
        {
            height: 100%;
        }

        form
        {
            height: 100%;
        }

        #UpdatePanel
        {
            height: 100%;
        }

        #containt > #UpdatePanel > .Panel
        {
            float: left;
            height: -moz-calc(100% - 0px);
            height: -webkit-calc(100% - 0px);
            height: calc(100% - 0px);
        }

        .itemPanel
        {
            width: 200px;
            height: 140px;
            float: left;
            margin: 10px 10px 10px 10px;
            border-width: 1px;
            border-style: solid;
            cursor: pointer;
        }

            .itemPanel > div
            {
                float: left;
                height: -moz-calc(100% - 0px);
                height: -webkit-calc(100% - 0px);
                height: calc(100% - 0px);
            }

                .itemPanel > div > p
                {
                    margin-top: 0px;
                    margin-bottom: 0px;
                    font-size: 12px;
                }

        .leftItem
        {
            border-right-width: 1px;
            border-right-style: solid;
            text-align:center;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div id="containt">
            <asp:ScriptManager ID="ScriptManager1" runat="server">
            </asp:ScriptManager>
            <asp:UpdatePanel ID="UpdatePanel" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="leftPanel" class="Panel" style="width: 20%; border-right-width: 1px; border-right-style: solid;">
                        <div id="searchDiv" style="height: 135px;">
                            测点查询
                        <table style="width: 100%">
                            <tr>
                                <td>供电局：
                                </td>
                                <td>
                                    <cc1:ExDropDownList ID="ddlPowerSupplyName" runat="server" CssClass="select" FieldName="PowerSupplyCode"
                                        AutoPostBack="true" OnSelectedIndexChanged="ddlPowerSupplyName_SelectedIndexChanged"
                                        BindName="InfoDevParaObj" Width="131" />
                                </td>
                            </tr>
                            <tr>
                                <td>所属变电站：
                                </td>
                                <td>
                                    <cc1:ExDropDownList ID="ddlConvertStationName" runat="server" CssClass="select" FieldName="ConvertStationCode"
                                        BindName="InfoDevParaObj" Width="131" />
                                </td>
                            </tr>


                            <tr>
                                <td colspan="2" style="text-align: center;">
                                    <asp:Button ID="btnSearch" runat="server" OnClick="btnSearch_Click" Text="查询" />
                                </td>
                            </tr>
                        </table>
                        </div>
                        <div class="div-body">
                            <table id="table1" class="grid" singleselect="true">
                                <colgroup>
                                    <col width="20%" />
                                    <col width="50%" />
                                    <col width="30%" />
                                </colgroup>
                                <thead>
                                    <tr>
                                        <td style="text-align: center;">测点编码
                                        </td>
                                        <td style="text-align: center;">测点名称
                                        </td>
                                        <td style="text-align: center;">所属变电站
                                        </td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <asp:Repeater ID="rp_Item" runat="server">
                                        <ItemTemplate>
                                            <tr>
                                                <td style="text-align: center;">
                                                    <%# Eval("CheckPointCode")%>
                                                </td>
                                                <td style="text-align: center; color:blue;">
                                                    <a href="#" onclick="initClick('<%# Eval("OID")%>')" style="color:blue;">  <%# Eval("CheckPointName")%></a>
                                                  
                                                </td>
                                                <td style="text-align: center;">
                                                    <%# Eval("ConvertStationName")%>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div id="rightPanel" class="Panel" style="width: 79%;">
                        <asp:Repeater ID="Repeater1" runat="server">
                            <ItemTemplate>
                                <div class="itemPanel" id='<%# Eval("Magnetic_CheckPointInfo_ID")%>' onclick="initClick('<%# Eval("Magnetic_CheckPointInfo_ID")%>')">
                                    <div class="leftItem" style="width: 40%;">
                                        <span style="font-weight: bold;">
                                            <%# Eval("ConvertStationName")%></span>
                                        <br />
                                        <image src="../Themes/Images/networking.png"></image>
                                        <br />
                                          <%# Eval("CheckPointName")%>
                                        <br />
                                        <span>
                                            <%# Eval("CollectionDatetime")%>
                                        </span>
                                    </div>
                                    <div style="width: 59%">
                                        <p style="color: #7cb5ec">
                                            电场强度:<%# Eval("ElectricFieldRMS")%>V/m
                                        </p>
                                        <p style="color: #434348">
                                           国家限值:4000V/m
                                        </p>
                                        <p style="color: #CCC">
                                            磁感应强度:<%# Eval("MagneticInductionRMS")%>μT
                                        </p>
                                        <p style="color: #f7a35c">
                                             国家限值:100μT
                                        </p>
                                        <p style="color: #8085e9">
                                            温度:<%# Eval("Temperature")%>℃
                                        </p>
                                        <p style="color: blue;">
                                            湿度:<%# Eval("Humidity")%>％
                                        </p>

                                    </div>
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="ddlPowerSupplyName" EventName="SelectedIndexChanged" />
                    <asp:AsyncPostBackTrigger ControlID="btnSearch" EventName="Click" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </form>
    <script type="text/javascript">
        var url = "/MagneticPage/MonitoringDataDialog.aspx";
     
   

        function initClick(checkPointCode)
        {
            
                top.openDialog(url + "?OID=" + checkPointCode, 'MonitoringDataDialog', '显示历史数据', 1250, 620, 50, 50);
          
        }
    </script>
</body>
</html>

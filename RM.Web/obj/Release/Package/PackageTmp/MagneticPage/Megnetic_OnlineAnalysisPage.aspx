﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Megnetic_OnlineAnalysisPage.aspx.cs"
    Inherits="RM.Web.MagneticPage.Megnetic_OnlineAnalysisPage" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>数据分析诊断</title>
    <%-- <link href="~/Themes/Styles/Site.css" rel="stylesheet" type="text/css" />--%>
    <script src="../Themes/Scripts/jquery-1.8.2.min.js" type="text/javascript"></script>
    <script src="../Themes/Scripts/jquery.pullbox.js" type="text/javascript"></script>
    <script src="../Themes/Scripts/FunctionJS.js" type="text/javascript"></script>
    <script src="../Themes/Scripts/DatePicker/WdatePicker.js" type="text/javascript"></script>
    <link href="../Themes/Scripts/jqueryeasyui/themes/bootstrap/easyui.css" rel="stylesheet" />
    <link href="../Themes/Scripts/jqueryeasyui/themes/icon.css" rel="stylesheet" />
    <style type="text/css">
        html, body {
            margin: 0;
            height: 100%;
            font-size: 9pt;
        }

        #containt {
            height: 100%;
        }

        form {
            height: 100%;
        }

        #UpdatePanel {
            height: 100%;
        }

        #containt > #UpdatePanel > .Panel {
            float: left;
            height: -moz-calc(100% - 0px);
            height: -webkit-calc(100% - 00px);
            height: calc(100% - 0px);
        }

        .itemPanel {
            width: 170px;
            height: 170px;
            float: left;
            margin: 10px 10px 10px 10px;
            border-width: 1px;
            border-style: solid;
            cursor: pointer;
        }

            .itemPanel > div {
                float: left;
                height: -moz-calc(100% - 0px);
                height: -webkit-calc(100% - 00px);
                height: calc(100% - 0px);
            }

                .itemPanel > div > p {
                    margin-top: 0px;
                    margin-bottom: 0px;
                    font-size: 12px;
                }

        .leftItem {
            border-right-width: 1px;
            border-right-style: solid;
        }

        .datachart {
            height: -moz-calc(100% - 300px);
            height: -webkit-calc(100% - 300px);
            height: calc(100% - 300px);
        }
        /**表格 begin**/
        .grid {
            margin: 0px;
            border-collapse: collapse;
            width: 100%;
            table-layout: fixed;
        }

            .grid thead td {
                border-top: 1px solid #ccc;
                border-bottom: 1px solid #ccc;
                border-right: 1px dotted #ccc;
                background: url(../Themes/Images/datagrid_header_bg.gif) repeat-x;
                text-align: center;
                padding: 5px 1px;
                font-weight: normal;
                text-overflow: ellipsis;
                word-break: keep-all;
                overflow: hidden;
            }

            .grid tbody td {
                text-align: left;
                border-bottom: 1px dotted #ccc;
                border-right: 1px dotted #ccc;
                padding: 1px 1px;
                height: 20px;
                word-break: break-all;
            }

            .grid tbody .alt {
                background: #F7F7F7;
            }

            .grid tbody .selected {
                background: #e0eccc;
            }

       
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div id="containt">
            <asp:ScriptManager ID="ScriptManager1" runat="server">
            </asp:ScriptManager>
            <asp:UpdatePanel ID="UpdatePanel" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="leftPanel" class="Panel" style="width: 20%; border-right-width: 1px; border-right-style: solid;">
                        <div id="searchDiv" style="height: 165px;">
                            测点查询
                        <table style="width: 100%">
                            <tr>
                                <td>供电局：
                                </td>
                                <td>
                                    <cc1:ExDropDownList ID="ddlPowerSupplyName" runat="server" CssClass="select" FieldName="PowerSupplyCode"
                                        AutoPostBack="true" OnSelectedIndexChanged="ddlPowerSupplyName_SelectedIndexChanged"
                                        BindName="InfoDevParaObj" Width="131" />
                                </td>
                            </tr>
                            <tr>
                                <td>所属变电站：
                                </td>
                                <td>
                                    <cc1:ExDropDownList ID="ddlConvertStationName" runat="server" CssClass="select" FieldName="ConvertStationCode"
                                        BindName="InfoDevParaObj" Width="131" />
                                </td>
                            </tr>

                            <tr>
                                <td>电压等级
                                </td>
                                <td>
                                    <cc1:ExDropDownList ID="ddlVoltageLevel" runat="server" CssClass="select" FieldName="VoltageLevel"
                                        BindName="InfoDevParaObj" Width="131" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" style="text-align: center;">
                                    <asp:Button ID="btnSearch" runat="server" OnClick="btnSearch_Click" Text="查询" />
                                </td>
                            </tr>
                        </table>
                        </div>
                        <div class="div-body">
                            <table id="table1" class="grid" singleselect="false">
                                <colgroup>
                                    <col width="8%" />
                                    <col width="20%" />
                                    <col width="42%" />
                                    <col width="30%" />
                                </colgroup>
                                <thead>
                                    <tr>
                                        <td style="text-align: center;"></td>
                                        <td style="text-align: center;">测点编码
                                        </td>
                                        <td style="text-align: center;">测点名称
                                        </td>
                                        <td style="text-align: center;">所属变电站
                                        </td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <asp:Repeater ID="rp_Item" runat="server">
                                        <ItemTemplate>
                                            <tr>
                                                <td style="text-align: center;">
                                                    <input type="checkbox" id="<%# Eval("OID")%>" title="<%# Eval("CheckPointCode")%>" />
                                                </td>
                                                <td style="text-align: center;">
                                                    <%# Eval("CheckPointCode")%>
                                                </td>
                                                <td style="text-align: center;">
                                                    <%# Eval("CheckPointName")%>
                                                </td>
                                                <td style="text-align: center;">
                                                    <%# Eval("ConvertStationName")%>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div id="rightPanel" class="Panel" style="width: 79.5%;">
                        <div>
                            <span style="float: left;">采集时间起:</span>
                            <div style="float: left;">
                                <input id="formdate" type="text" name="formdate" class="easyui-datebox"></input>
                            </div>
                            <span style="float: left;">采集时间止: </span>
                            <div style="float: left;">
                                <input id="todate" type="text" name="todate" class="easyui-datebox"></input>
                            </div>
                            <div style="float: left;">
                                <input id="btnRefreshList" type="button" value="查询" />
                            </div>
                            <span style="float: right;">单位：电场强度（kV/m）、磁感应强度（μT）</span>
                        </div>

                        <div title="测点历史" style="padding: 10px; width: 1100px;">
                            <table id="listMagnetic_CheckPointInfo">
                            </table>
                            <br />

                            <div>
                                <div style="float: left; padding-top: 2px" id="ConstCheckList">

                                    <span style="font-weight: bold">
                                        <input id="Checkbox1" type="checkbox" value="ElectricFieldMAX" />电场强度MAX</span>
                                    <span style="font-weight: bold">
                                        <input id="Checkbox2" type="checkbox" value="ElectricFieldMIN" />电场强度MIN</span>
                                    <span style="font-weight: bold">
                                        <input id="Checkbox4" type="checkbox" value="ElectricFieldRMS" />电场强度RMS</span>
                                    <span style="font-weight: bold">
                                        <input id="Checkbox5" type="checkbox" value="MagneticInductionMAX" />磁感应强度MAX</span>
                                    <span style="font-weight: bold">
                                        <input id="Checkbox6" type="checkbox" value="MagneticInductionMIN" />磁感应强度MIN</span>
                                    <span style="font-weight: bold">
                                        <input id="Checkbox7" type="checkbox" value="MagneticInductionRMS" />磁感应强度RMS</span>
                                    <span style="font-weight: bold">
                                        <input id="Checkbox8" type="checkbox" value="Temperature" />温度(℃)</span>
                                    <span style="font-weight: bold">
                                        <input id="Checkbox9" type="checkbox" value="Humidity" />湿度(%)</span>
                                </div>
                                <div style="float: left; margin-left: 10px;">
                                    <input id="btnConstRefresh" type="button" value="立即刷新" />
                                </div>
                            </div>
                            <div id="listMagnetic_CheckPointInfoChart">
                            </div>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="ddlPowerSupplyName" EventName="SelectedIndexChanged" />
                    <asp:AsyncPostBackTrigger ControlID="btnSearch" EventName="Click" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </form>
    <script type="text/javascript" src="../Themes/Scripts/highcharts.js"></script>
    <script type="text/javascript" src="../Themes/Scripts/jqueryeasyui/jquery.easyui.min.js"></script>
    <script src="../Themes/Scripts/jqueryeasyui/plugins/jquery.edatagrid.js" type="text/javascript"></script>
    <script type="text/javascript">

        var $arrid = [];
        var $arridnochar = [];
        var $checkPointSourse = [];
        var $fd;
        var $td;
        var $width = 1100;
        $(document).ready(function myfunction() {
            InitControl();
        });
        function InitControl() {
            $.parser.parse();

            var $checkBoxList = $(".div-body").find(":checkbox");
            $checkBoxList.click(function myfunction(o) {
                $arrid = [];
                $checkPointSourse = [];
                $arridnochar = [];
                $checkBoxList.filter(":checked").each(function myfunction() {
                    $arrid.push("\'" + $(this).attr("id") + "\'");
                    $arridnochar.push($(this).attr("id"));
                    $checkPointSourse.push($(this).attr("title"));
                });

            });

            //历史列表刷新
            $("#btnRefreshList").click(function myfunction() {

                if ($("#form1").form('validate')) {

                    $fd = $("#formdate").datebox('getValue');
                    $td = $("#todate").datebox('getValue');
                    GetTable();

                }
            });

            $("#btnConstRefresh").click(function myfunction() {

                GetChart();

            });
        }


        //组分历史数据
        function GetTable() {
            var editRow = undefined;
            var $width = 1100;
            $("#listMagnetic_CheckPointInfo").datagrid({
                height: 250,
                width: $width,
                fitColumns: true,
                collapsible: true,
                singleSelect: true,
                url: 'Magnetic_CheckPointInfoHandler.ashx',
                queryParams: { action: 'GetMagnetic_CheckPointInfoChilds', Magnetic_CheckPointInfo_OID: $arridnochar.join(","), fd: $fd, td: $td },
                idField: 'OID',
                columns: [[
                     { field: 'OID', title: 'ID', hidden: true },
                     
                { field: 'ConvertStationName', align: 'center', width: fixWidth(0.10), title: '所属变电站' },
             { field: 'CheckPointName', align: 'center', width: fixWidth(0.12), title: '测点名称' },
                {
                    field: 'CollectionDatetime', align: 'center', width: fixWidth(0.10), title: '采集时间', formatter: formatDatebox, editor: { type: 'datetimebox', options: { required: true } }
                },
                { field: 'ElectricFieldMAX', title: '电场强度MAX', width: fixWidth(0.07), align: 'center', editor: { type: 'text', options: { required: true } } },
                { field: 'ElectricFieldMIN', title: '电场强度MIN', width: fixWidth(0.07), align: 'center', editor: { type: 'text', options: { required: true } } },
                { field: 'ElectricFieldRMS', title: '电场强度RMS', width: fixWidth(0.07), align: 'center', editor: { type: 'text', options: { required: true } } },
                { field: 'MagneticInductionMAX', title: '磁感应强度MAX', width: fixWidth(0.07), align: 'center', editor: { type: 'text', options: { required: true } } },
                { field: 'MagneticInductionMIN', title: '磁感应强度MIN', width: fixWidth(0.07), align: 'center', editor: { type: 'text', options: { required: true } } },
                { field: 'MagneticInductionRMS', title: '磁感应强度RMS', width: fixWidth(0.07), align: 'center', editor: { type: 'text', options: { required: true } } },
                { field: 'Temperature', title: '温度(℃)', width: fixWidth(0.05), align: 'center', editor: { type: 'text', options: { required: true } } },
                { field: 'Humidity', title: '湿度(%)', width: fixWidth(0.05), align: 'center', editor: { type: 'text', options: { required: true } } },

                  {
                      field: 'CollectionType', width: fixWidth(0.06), title: '采集类型', align: 'center'
                  }]],

                onAfterEdit: function (rowIndex, rowData, changes) {
                    editRow = undefined;
                },
                onDblClickRow: function (rowIndex, rowData) {
                    if (editRow != undefined) {
                        $("#Student_Table").datagrid('endEdit', editRow);
                    }

                    if (editRow == undefined) {
                        $("#Student_Table").datagrid('beginEdit', rowIndex);
                        editRow = rowIndex;
                    }
                },
                onClickRow: function (rowIndex, rowData) {
                    if (editRow != undefined) {
                        $("#Student_Table").datagrid('endEdit', editRow);

                    }

                }

            });
        }

        function GetChart() {
            $fd = $("#formdate").datebox('getValue');
            $td = $("#todate").datebox('getValue');
            var $st = [];
            $("#ConstCheckList > span > :checkbox").filter(":checked").each(function myfunction() {
                $st.push($(this).val());
            });

            $.get('Magnetic_CheckPointInfoHandler.ashx', { action: 'GetMagnetic_CheckPointInfoChildsChart2', fromdate: $fd, todate: $td, checkPoint: $checkPointSourse.join(","), Target: $st.toString() }, function (results) {

                var listDatetimeJson = results.listDatetime;
                var listChartDataJson = results.listChartData;

                $('#listMagnetic_CheckPointInfoChart').highcharts({
                    title: {
                        text: '测点历史数据',
                        x: -20 //center
                    },
                    xAxis: {
                        categories: listDatetimeJson
                    },
                    yAxis: {
                        title: {
                            text: null
                        },
                        plotLines: [{
                            value: 0,
                            width: 1,
                            color: '#808080'
                        }]
                    },
                    tooltip: {

                    },
                    legend: {
                        layout: 'vertical',
                        align: 'right',
                        verticalAlign: 'middle',
                        borderWidth: 0,
                        itemStyle: { color: '#7cb5ec' }

                    },
                    series: listChartDataJson
                });
            });
        }

        function formatDatebox(value) {
            if (value == null || value == '') {
                return '';
            }
            var dt;
            if (value instanceof Date) {
                dt = value;
            } else {
               // dt = new Date(value.replace('T', ' '));
                return value.replace('T', ' ');
            }
            return dt.format("yyyy-MM-dd hh:mm:ss");
        }
        function fixWidth(percent) {
            return (document.body.clientWidth - 5) * percent;
        }


    </script>
</body>
</html>

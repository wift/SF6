﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Magnetic_CheckPointTemplatesBDialog.aspx.cs" Inherits="RM.Web.MagneticPage.Magnetic_CheckPointTemplatesBDialog" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <script src="../Themes/Scripts/Validator/JValidator.js" type="text/javascript"></script>
  <script src="../Scripts/jquery-1.4.1.min.js"></script>
    <script src="../Themes/Scripts/jquery.pullbox.js" type="text/javascript"></script>
    <script src="../Themes/Scripts/FunctionJS.js" type="text/javascript"></script>
    <script src="../Themes/Scripts/DatePicker/WdatePicker.js" type="text/javascript"></script>
    <link href="../Themes/Scripts/jqueryeasyui/themes/bootstrap/easyui.css" rel="stylesheet" />
    <link href="../Themes/Scripts/jqueryeasyui/themes/icon.css" rel="stylesheet" />
    <link href="../Themes/Scripts/jqueryeasyui/themes/color.css" rel="stylesheet" />

    <script type="text/javascript" src="../Themes/Scripts/jqueryeasyui/jquery.easyui.min.js"></script>
    <script src="../Themes/Scripts/jqueryeasyui/plugins/jquery.edatagrid.js" type="text/javascript"></script>

    <script src="../Scripts/uploadify/jquery.uploadify.min.js" type="text/javascript"></script>
    <title>站点测点添加</title>
    <style type="text/css">
        .inner_cell_right {
            text-align: right;
        }

        object {
            left: 0px;
            width:100%;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">

        <div>
            工频电磁场强度
                   <table width="100%" style="font-size: smaller;">
                       <colgroup>
                           <col width="10%" />
                           <col width="8%" />
                           <col width="11%" />
                           <col width="12%" />
                           <col width="11%" />
                           <col width="12%" />
                           <col width="11%" />
                           <col width="16.5%" />
                       </colgroup>
                       <tr>
                           <td>模板类型</td>
                           <td>

                               <cc1:ExDropDownList ID="ddlTemplateType" runat="server" CssClass="select" Width="155"
                                   FieldName="TemplateType" BindName="Magnetic_CheckPointTemplatesBObj" checkexpession="NotNull"
                                   datacol="yes" err="此项" />
                           </td>
                           <td></td>
                           <td></td>
                           <td></td>
                           <td></td>
                           <td></td>
                           <td></td>

                       </tr>
                       <tr>
                           <td class="inner_cell_right">测量日期：
                           </td>
                           <td>
                               <cc1:ExTextBox ID="txtMeasuringDate" onfocus="WdatePicker()" runat="server" datacol="yes" err="此项"
                                   FieldName="MeasuringDate" BindName="Magnetic_CheckPointTemplatesBObj" Width="150" StrFormat="yyyy-MM-dd" />
                           </td>
                           <td class="inner_cell_right">主变电压(kV)：
                           </td>
                           <td>

                               <cc1:ExTextBox ID="ExTextBox1" runat="server"
                                   datacol="yes" err="此项" Width="50"
                                   FieldName="voltage" BindName="Magnetic_CheckPointTemplatesBObj" />
                           </td>

                           <td class="inner_cell_right">主变电流(A)：
                           </td>
                           <td>
                               <cc1:ExTextBox ID="ExTextBox18" runat="server"
                                   datacol="yes" err="此项" Width="50"
                                   FieldName="Electricity" BindName="Magnetic_CheckPointTemplatesBObj" />
                           </td>

                           <td class="inner_cell_right">主变信息：
                           </td>
                           <td>
                               <cc1:ExTextBox ID="ExTextBox19" runat="server"
                                   datacol="yes" err="此项"
                                   FieldName="MasterInfo" BindName="Magnetic_CheckPointTemplatesBObj" />
                           </td>
                       </tr>
                       <tr>
                           <td class="inner_cell_right">测量时间：
                           </td>
                           <td>
                               <cc1:ExTextBox ID="ExTextBox20" runat="server" datacol="yes" err="此项"
                                   FieldName="MeasuringTime" BindName="Magnetic_CheckPointTemplatesBObj" Width="150" />
                           </td>
                           <td class="inner_cell_right">环境温度：
                           </td>
                           <td>
                               <cc1:ExTextBox ID="ExTextBox21" runat="server" datacol="yes" err="此项"
                                   FieldName="Temperature" BindName="Magnetic_CheckPointTemplatesBObj" Width="50" />（℃）
                               
                           </td>

                           <td class="inner_cell_right">环境湿度：
                           </td>
                           <td>
                               <cc1:ExTextBox ID="ExTextBox22" runat="server" datacol="yes" err="此项"
                                   FieldName="Humidity" BindName="Magnetic_CheckPointTemplatesBObj" Width="50" />
                               （%）
                           </td>
                           <td class="inner_cell_right">大气压力：
                           </td>
                           <td>
                               <cc1:ExTextBox ID="ExTextBox23" runat="server" datacol="yes" err="此项"
                                   FieldName="BARO" BindName="Magnetic_CheckPointTemplatesBObj" Width="50" />（kPa）
                           </td>
                       </tr>
                   </table>
            <hr />
            无线电干扰
   
                      <table width="100%" style="font-size: smaller;">
                          <colgroup>
                              <col width="12%" />
                              <col width="12%" />
                              <col width="12%" />
                              <col width="12%" />
                              <col width="12%" />
                              <col width="12%" />
                              <col width="12%" />
                              <col width="12%" />
                          </colgroup>

                          <tr>
                              <td class="inner_cell_right">测量日期：
                              </td>
                              <td>
                                  <cc1:ExTextBox ID="ExTextBox2" onfocus="WdatePicker()" runat="server" datacol="yes" err="此项"
                                      FieldName="MeasuringDate2" BindName="Magnetic_CheckPointTemplatesBObj" Width="150" StrFormat="yyyy-MM-dd" />
                              </td>
                              <td class="inner_cell_right">主变电压(kV)：
                              </td>
                              <td>

                                  <cc1:ExTextBox ID="ExTextBox3" runat="server"
                                      datacol="yes" err="此项" Width="50"
                                      FieldName="voltage2" BindName="Magnetic_CheckPointTemplatesBObj" />
                              </td>

                              <td class="inner_cell_right">主变电流(A)：
                              </td>
                              <td>
                                  <cc1:ExTextBox ID="ExTextBox4" runat="server"
                                      datacol="yes" err="此项" Width="50"
                                      FieldName="Electricity2" BindName="Magnetic_CheckPointTemplatesBObj" />（A）
                              </td>

                              <td class="inner_cell_right">主变信息：
                              </td>
                              <td>
                                  <cc1:ExTextBox ID="ExTextBox5" runat="server"
                                      datacol="yes" err="此项"
                                      FieldName="MasterInfo2" BindName="Magnetic_CheckPointTemplatesBObj" />
                              </td>
                          </tr>
                          <tr>
                              <td class="inner_cell_right">测量时间：
                              </td>
                              <td>
                                  <cc1:ExTextBox ID="ExTextBox6" runat="server" datacol="yes" err="此项"
                                      FieldName="MeasuringTime2" BindName="Magnetic_CheckPointTemplatesBObj" Width="100" />
                              </td>
                              <td class="inner_cell_right">环境温度：
                              </td>
                              <td>
                                  <cc1:ExTextBox ID="ExTextBox7" runat="server" datacol="yes" err="此项"
                                      FieldName="Temperature2" BindName="Magnetic_CheckPointTemplatesBObj" Width="50" />（℃）
                              </td>

                              <td class="inner_cell_right">环境湿度：
                              </td>
                              <td>
                                  <cc1:ExTextBox ID="ExTextBox10" runat="server" datacol="yes" err="此项"
                                      FieldName="Humidity2" BindName="Magnetic_CheckPointTemplatesBObj" Width="50" />（%）
                              </td>
                              <td class="inner_cell_right">大气压力：
                              </td>
                              <td>
                                  <cc1:ExTextBox ID="ExTextBox31" runat="server" datacol="yes" err="此项"
                                      FieldName="BARO2" BindName="Magnetic_CheckPointTemplatesBObj" Width="50" />（kPa）
                              </td>
                          </tr>
                          <tr>
                              <td class="inner_cell_right">风向：
                              </td>
                              <td>
                                  <cc1:ExTextBox ID="ExTextBox32" runat="server" datacol="yes" err="此项"
                                      FieldName="WindDirection" BindName="Magnetic_CheckPointTemplatesBObj" Width="50" />
                              </td>
                              <td class="inner_cell_right">风速：
                              </td>
                              <td>
                                  <cc1:ExTextBox ID="ExTextBox33" runat="server" datacol="yes" err="此项"
                                      FieldName="WindSpeed" BindName="Magnetic_CheckPointTemplatesBObj" Width="50" />(m/s）
                              </td>
                              <td></td>
                              <td></td>
                              <td></td>
                              <td></td>
                          </tr>
                      </table>
            <hr />
            噪声
                   <table width="100%" style="font-size: smaller;">
                       <colgroup>
                           <col width="12%" />
                           <col width="12%" />
                           <col width="12%" />
                           <col width="12%" />
                           <col width="12%" />
                           <col width="12%" />
                           <col width="12%" />
                           <col width="12%" />
                       </colgroup>

                       <tr>
                           <td class="inner_cell_right">测量日期：
                           </td>
                           <td>
                               <cc1:ExTextBox ID="ExTextBox8" onfocus="WdatePicker()" runat="server" datacol="yes" err="此项"
                                   FieldName="MeasuringDate3" BindName="Magnetic_CheckPointTemplatesBObj" Width="150" StrFormat="yyyy-MM-dd" />
                           </td>
                           <td class="inner_cell_right">主变电压(kV)：
                           </td>
                           <td>

                               <cc1:ExTextBox ID="ExTextBox9" runat="server"
                                   datacol="yes" err="此项" Width="50"
                                   FieldName="voltage3" BindName="Magnetic_CheckPointTemplatesBObj" />
                           </td>

                           <td class="inner_cell_right">主变电流(A)：
                           </td>
                           <td>
                               <cc1:ExTextBox ID="ExTextBox11" runat="server"
                                   datacol="yes" err="此项" Width="50"
                                   FieldName="Electricity3" BindName="Magnetic_CheckPointTemplatesBObj" />（A）
                           </td>

                           <td class="inner_cell_right">主变信息：
                           </td>
                           <td>
                               <cc1:ExTextBox ID="ExTextBox12" runat="server"
                                   datacol="yes" err="此项"
                                   FieldName="MasterInfo3" BindName="Magnetic_CheckPointTemplatesBObj" />
                           </td>
                       </tr>
                       <tr>
                           <td class="inner_cell_right">测量时间：
                           </td>
                           <td>
                               <cc1:ExTextBox ID="ExTextBox13" runat="server" datacol="yes" err="此项"
                                   FieldName="MeasuringTime3" BindName="Magnetic_CheckPointTemplatesBObj" Width="100" />
                           </td>
                           <td class="inner_cell_right">环境温度：
                           </td>
                           <td>
                               <cc1:ExTextBox ID="ExTextBox14" runat="server" datacol="yes" err="此项"
                                   FieldName="Temperature3" BindName="Magnetic_CheckPointTemplatesBObj" Width="50" />（℃）
                           </td>

                           <td class="inner_cell_right">环境湿度：
                           </td>
                           <td>
                               <cc1:ExTextBox ID="ExTextBox15" runat="server" datacol="yes" err="此项"
                                   FieldName="Humidity3" BindName="Magnetic_CheckPointTemplatesBObj" Width="50" />（%）
                           </td>
                           <td class="inner_cell_right">大气压力：
                           </td>
                           <td>
                               <cc1:ExTextBox ID="ExTextBox16" runat="server" datacol="yes" err="此项"
                                   FieldName="BARO3" BindName="Magnetic_CheckPointTemplatesBObj" Width="50" />（kPa）
                           </td>
                       </tr>

                   </table>
            <hr />
            <table id="fileList"></table>
            <div style="text-align: center;">
                <asp:Button ID="btnSaveLog" runat="server" Text="保存" OnClick="btnSave_Click" OnClientClick="return CheckDataValid('#form1');" />
            </div>
        </div>




    </form>
    <script type="text/javascript">
        $(document).ready(function myfunction() {
            window.setTimeout(function myfunction() {
                var $magnetic_CheckPointTemplatesB_OID = '<%= Magnetic_CheckPointTemplatesBObj.OID%>'
                LoadFileList($magnetic_CheckPointTemplatesB_OID);
            }, 500)

        });

        function LoadFileList(magnetic_CheckPointTemplatesB_OID) {
            var _fileClass = "站点测点模版";
            var editRow = undefined;
            var $datagrid = $("#fileList");
            var $heightNoise = 230

            var $widthNoise = $(document).width();

            $datagrid.datagrid({
                height: $heightNoise - 10,
                width: $widthNoise - 10,
                fitColumns: true,
                collapsible: true,
                singleSelect: true,
                url: 'UploadHandler.ashx',
                queryParams: { action: 'Get_FilesList', Magnetic_CheckPointTemplatesB_OID: magnetic_CheckPointTemplatesB_OID, fileClass: _fileClass },
                idField: 'OID',
                columns: [[
                 { field: 'OID', title: 'ID', hidden: true },
                   {
                       field: 'FileName', width: 150, title: "文件名", align: 'center'
                   },

                  {
                      field: 'FileSize', width: 100, title: "文件大小", align: 'center'
                  },
                    {
                        field: 'CreateDate', width: 100, title: "上传时间", align: 'center', formatter: formatDatebox
                    }, {
                        field: 'RegistrantName', width: 100, title: "上传者", align: 'center'
                    }


                ]],
                toolbar: [{
                    text: ' <span id="uploadify" />', iconCls: 'folder_up', handler: function () {

                    }

                }, {
                    text: '下载', iconCls: 'package_down', handler: function () {

                        var editIndex = editRow;

                        $datagrid.datagrid('endEdit', editRow);

                        $datagrid.datagrid('selectRow', editIndex)
                        var rowstr = $datagrid.datagrid('getSelected', editIndex)
                        window.location = 'UploadHandler.ashx?action=DownLoad&url=' + rowstr.FilePath;


                    }
                }, {
                    text: '删除', iconCls: 'icon-remove', handler: function () {
                        var row = $datagrid.datagrid('getSelected');
                        var index = $datagrid.datagrid('getRowIndex', row);


                        $.post('UploadHandler.ashx?action=Del_Files', { oid: row.OID }, function (data) {
                            if (data == "删除成功") {
                                $datagrid.datagrid('deleteRow', index);
                            }

                            showFaceMsg(data);

                        });

                    }
                }],
                onAfterEdit: function (rowIndex, rowData, changes) {
                    editRow = undefined;
                },
                onDblClickRow: function (rowIndex, rowData) {
                    if (editRow != undefined) {
                        $datagrid.datagrid('endEdit', editRow);
                    }

                    if (editRow == undefined) {
                        $datagrid.datagrid('beginEdit', rowIndex);
                        editRow = rowIndex;
                    }
                },
                onClickRow: function (rowIndex, rowData) {
                    if (editRow != undefined) {
                        $datagrid.datagrid('endEdit', editRow);

                    }
                    editRow = rowIndex;
                }

            });


            $('#uploadify').uploadify({
                'formData': {
                    'Magnetic_CheckPointTemplatesB_OID': magnetic_CheckPointTemplatesB_OID,
                    'action': 'upload', fileClass: _fileClass
                },
                'swf': '../Scripts/uploadify/uploadify.swf',
                'uploader': 'UploadHandler.ashx',
                'buttonText': '上传',
                'width': 40,
                'queueID': true,
                onUploadComplete: function myfunction(file) {
                    LoadFileList(magnetic_CheckPointTemplatesB_OID)
                    alert('上传成功');
                }
            });
        }
        function formatDatebox(value) {
            if (value == null || value == '') {
                return '';
            }
            var dt;
            if (value instanceof Date) {
                dt = value;
            } else {
                //dt = new Date(value.replace('T', ' '));
                return value.replace('T', ' ');
            }
            return dt.format("yyyy-MM-dd hh:mm:ss");
        }
        function fixWidth(percent) {
            return (document.body.clientWidth - 5) * percent;
        }

    </script>
</body>
</html>

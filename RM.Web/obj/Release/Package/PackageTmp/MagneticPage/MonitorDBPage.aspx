﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MonitorDBPage.aspx.cs" Inherits="RM.Web.MagneticPage.MonitorDBPage" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>监测数据库</title>
    <script src="../Themes/Scripts/jquery-1.8.2.min.js" type="text/javascript"></script>
    <script src="../Themes/Scripts/jquery.pullbox.js" type="text/javascript"></script>
    <script src="../Themes/Scripts/FunctionJS.js" type="text/javascript"></script>
    <script src="../Themes/Scripts/DatePicker/WdatePicker.js" type="text/javascript"></script>
    <link href="../Themes/Scripts/jqueryeasyui/themes/bootstrap/easyui.css" rel="stylesheet" />
    <link href="../Themes/Scripts/jqueryeasyui/themes/icon.css" rel="stylesheet" />
    <script type="text/javascript" src="../Themes/Scripts/jqueryeasyui/jquery.easyui.min.js"></script>
    <script src="../Themes/Scripts/jqueryeasyui/plugins/jquery.edatagrid.js" type="text/javascript"></script>

    <style type="text/css">
        .panel-title:after {
            content: "单位：电场强度（kV/m）、磁感应强度（μT）";
            top: 0px;
            float: right;
            position: relative;
        }
        .searchclass{
            font-size:9pt;
          
        }
        .searchclass > span {
        margin-top:3px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div id="containt">
            <div id="rightPanel" class="Panel" style="width: 99.5%;">
                <div class="searchclass">
                    <span style="float: left;">采集时间起:</span>
                    <div style="float: left;">
                        <input id="formdate" type="text" name="formdate" class="easyui-datebox" required="required"></input>
                    </div>
                    <span style="float: left;">采集时间止: </span>
                    <div style="float: left;">
                        <input id="todate" type="text" name="todate" class="easyui-datebox" required="required"></input>
                    </div>
                      <span style="float: left;">供电局: </span>
                    <div style="float: left;">
                        <input id="cbbPowerSupplyName" name="PowerSupplyName"  class="easyui-combobox" style="width: 100px;"  />
                    </div>
                    <span style="float: left;">所属变电站: </span>
                    <div style="float: left;">
                        <input id="cbbConvertStationName" name="ConvertStationName"  class="easyui-combobox" style="width: 100px;" />
                    </div>


                    <span style="float: left;">采集类型: </span>
                    <div style="float: left;">
                        <select id="cbbCollectionTypeName" class="easyui-combobox" name="CollectionTypeName"  style="width: 60px;">
                        <option value=""></option>
                            <option value="在线">在线</option>
                            <option value="离线">离线</option>
                        </select>
                    </div>
                    <span style="float: left;">测点名称: </span>
                    <div style="float: left;">
                        <input id="txtCheckPointName" name="CheckPointName" class="easyui-textbox"></input>
                    </div>

                    <div style="float: left;">
                        <input id="btnRefreshList" type="button" value="查询" />
                    </div>
                </div>

                <table id="MonitorDBPageList">
                </table>
            </div>
        </div>
    </form>
    <script type="text/javascript">
        var $width = 0;
        var $height = 0;

        $(document).ready(function () {
            //  GetTable('');
            //历史列表刷新

            $width = $("#rightPanel").width();
            $height = $("#rightPanel").height() - 30;


            $("#btnRefreshList").click(function myfunction() {
                if ($("#form1").form('validate')) {

                   var $fd = $("#formdate").datebox('getValue');
                    var $td = $("#todate").datebox('getValue');
                    

                    getMonitorDBPageList($fd, $td);
                }

            });

            getMonitorDBPageList();

            

            $('#cbbPowerSupplyName').combobox({
                url: 'Magnetic_CheckPointInfoHandler.ashx?action=GetPowerSupplyNameList',
                valueField: 'Organization_ID',
                textField: 'Organization_Name',
                onSelect: function myfunction(record) {

                    $('#cbbConvertStationName').combobox({
                        url: 'Magnetic_CheckPointInfoHandler.ashx?action=GetConvertStationNameList&Organization_ID=' + record.Organization_ID,
                        valueField: 'ConvartStationName',
                        textField: 'ConvartStationName'
                    });
                }
            });

         
        });
        function fixWidth(percent) {
            return (document.body.clientWidth - 5) * percent;
        }
        function formatDatebox(value) {
            if (value == null || value == '') {
                return '';
            }
            var dt;
            if (value instanceof Date) {
                dt = value;
            } else {
                // dt = new Date(value.replace('T', ' '));
                return value.replace('T', ' ');
            }
            return dt.format("yyyy-MM-dd hh:mm:ss");
        }
        function getMonitorDBPageList(fromdate,todate) {
            
            var $ConvertStationName = $("#cbbConvertStationName").combobox('getValue');
            var $CollectionTypeName = $("#cbbCollectionTypeName").combobox('getValue');
            var $CheckPointName = $("#txtCheckPointName").textbox('getValue');

            $("#MonitorDBPageList").datagrid({
                height: 500,
                width: $width,
                fitColumns: true,
                title: '采集数据记录',
                collapsible: true,
                singleSelect: true,
                url: 'Magnetic_CheckPointInfoHandler.ashx',
                queryParams: { action: 'MonitorDBPageList', fromDate: fromdate, toDate: todate, ConvertStationName: $ConvertStationName, CollectionTypeName: $CollectionTypeName, CheckPointName: $CheckPointName },
                idField: 'OID',
                columns: [[
                 { field: 'OID', title: 'ID', hidden: true },
                  { field: 'CheckPointCode', align: 'center', width: fixWidth(0.04), title: "测点编码" },
		 { field: 'CheckPointName', align: 'center', width: fixWidth(0.08), title: '测点名称' },
        	{
        	    field: 'CollectionDatetime', align: 'center', width: fixWidth(0.08), title: '采集时间', formatter: formatDatebox
        	},
        	{ field: 'ElectricFieldMAX', title: '电场强度MAX', width: fixWidth(0.06), align: 'center' },
            { field: 'ElectricFieldMIN', title: '电场强度MIN', width: fixWidth(0.06), align: 'center' },
            { field: 'ElectricFieldRMS', title: '电场强度RMS', width: fixWidth(0.06), align: 'center' },
            { field: 'MagneticInductionMAX', title: '磁感应强度MAX', width: fixWidth(0.06), align: 'center' },
            { field: 'MagneticInductionMIN', title: '磁感应强度MIN', width: fixWidth(0.06), align: 'center' },
            { field: 'MagneticInductionRMS', title: '磁感应强度RMS', width: fixWidth(0.06), align: 'center' },
            { field: 'Temperature', title: '温度(℃)', width: fixWidth(0.04), align: 'center' },
            { field: 'Humidity', title: '湿度(%)', width: fixWidth(0.04), align: 'center' },

             {
                 field: 'CollectionType', width: fixWidth(0.04), title: '采集类型', align: 'center'
             },
               { field: 'CollectionBy', width: fixWidth(0.05), title: '采集人', align: 'center' },
        
                 { field: 'ConvertStationName', width: fixWidth(0.06), title: ' 所属变电站', align: 'center' }
                ]]


            });
        }

    </script>
</body>
</html>

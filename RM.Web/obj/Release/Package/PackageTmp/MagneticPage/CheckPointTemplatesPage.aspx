﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CheckPointTemplatesPage.aspx.cs" Inherits="RM.Web.MagneticPage.CheckPointTemplatesPage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
   <script type="text/javascript" src="../Themes/Scripts/jqueryeasyui/jquery.min.js"></script>
    <link href="../Themes/Scripts/jqueryeasyui/themes/bootstrap/easyui.css" rel="stylesheet" />
    <link href="../Themes/Scripts/jqueryeasyui/themes/icon.css" rel="stylesheet" />
    <link href="../Themes/Scripts/jqueryeasyui/themes/color.css" rel="stylesheet" />
    <title>电磁场测点模板</title>
    <style type="text/css">
        html, body {
            margin: 0;
            height: 100%;
        }

        form {
            height: 100%;
            width: 100%;
        }

        #containt {
            float: left;
            height: -moz-calc(100% - 0px);
            height: -webkit-calc(100% - 0px);
            height: calc(100% - 0px);
            width: -moz-calc(100% - 0px);
            width: -webkit-calc(100% - 0px);
            width: calc(100% - 0px);
        }

        .Panel {
            float: left;
        }
    
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div id="containt">
            <div id="leftPanel" class="Panel" style="width: 25%; height: 100%; border-right-width: 1px; border-right-style: solid;">
                <table id="CheckPointTemplatesList">
                </table>
            </div>
            <div id="rightPanel" class="Panel" style="width: 74%; height: 100%;">
                <table id="CheckPointPositionList">
                </table>
            </div>
        </div>
    </form>

    <script type="text/javascript" src="../Themes/Scripts/highcharts.js"></script>
    <script type="text/javascript" src="../Themes/Scripts/jqueryeasyui/jquery.easyui.min.js"></script>
    <script src="../Themes/Scripts/jqueryeasyui/plugins/jquery.edatagrid.js" type="text/javascript"></script>
    <script src="../Themes/Scripts/FunctionJS.js" type="text/javascript"></script>
    <script type="text/javascript">
        var arrRuns = eval('<%= RunStatusStrJson%>');
        var $cptOID = 0;
        $(document).ready(function myfunction() {
            var $width = $("#leftPanel").width();
            var $height = $("#leftPanel").height();
            var editRow = undefined;
            var $datagrid = $("#CheckPointTemplatesList");
            $datagrid.datagrid({
                height: $height,
                width: $width,
                fitColumns: true,
                collapsible: true,
                singleSelect: true,
                url: 'CheckPointTemplatesPageHandler.ashx',
                queryParams: { action: 'GetCheckPointTemplatesList' },
                idField: 'OID',
                columns: [[
                 { field: 'OID', title: 'ID', hidden: true },
                  {
                      field: 'VoltageLevel', align: 'center', width: 80, title: "电压等级", editor: {
                          type: 'combobox',
                          options: {
                              valueField: 'key',
                              textField: 'value',
                              data: arrRuns,
                              required: true
                          }
                      }, formatter: function (value) {

                          for (var i in arrRuns) {
                              if (arrRuns[i].key == value) {
                                  return arrRuns[i].value;
                              }
                          }


                      }
                  },
         { field: 'TemplateType', align: 'center', width: 250, title: '模版类型', editor: { type: 'text', options: { required: true } } }

                ]],
                toolbar: [{
                    text: '添加', iconCls: 'icon-add', handler: function () {
                        if (editRow != undefined) {
                            $datagrid.datagrid('endEdit', editRow);
                        }
                        if (editRow == undefined) {


                            $datagrid.datagrid('insertRow', {
                                index: 0,
                                row: {}//{ CheckPointName: ', CheckPointCode: ', CollectionDatetime: formatDatebox(new Date()), CollectionBy: _userName, CollectionType: "离线" }
                            });

                            $datagrid.datagrid('beginEdit', 0);
                            editRow = 0;
                        }
                    }
                }, '-', {
                    text: '保存', iconCls: 'icon-save', handler: function () {

                        var editIndex = editRow;

                        $datagrid.datagrid('endEdit', editRow);

                        $datagrid.datagrid('selectRow', editIndex)
                        var rowstr = $datagrid.datagrid('getSelected', editIndex)
                        //如果调用acceptChanges(),使用getChanges()则获取不到编辑和新增的数据。

                        //使用JSON序列化datarow对象，发送到后台。
                        //  var rows = $("#Student_Table").datagrid('getChanges');

                        //  var rowstr = JSON.stringify(rows[0]);
                        $.post('CheckPointTemplatesPageHandler.ashx?action=SaveCheckPointTemplates', rowstr, function (data) {

                            showFaceMsg(data);
                        });

                    }
                }, '-', {
                    text: '删除', iconCls: 'icon-remove', handler: function () {
                        var row = $datagrid.datagrid('getSelected');
                        var index = $datagrid.datagrid('getRowIndex', row);


                        $.post('CheckPointTemplatesPageHandler.ashx?action=DeleteCheckPointTemplates', { oid: row.OID }, function (data) {
                            if (data == "删除成功") {
                                $datagrid.datagrid('deleteRow', index);
                            }

                            showFaceMsg(data);


                        });

                    }
                }, '-', {
                    text: '修改', iconCls: 'icon-edit', handler: function () {
                        var row = $datagrid.datagrid('getSelected');
                        if (row != null) {
                            if (editRow != undefined) {
                                $datagrid.datagrid('endEdit', editRow);
                            }

                            if (editRow == undefined) {
                                var index = $datagrid.datagrid('getRowIndex', row);
                                $datagrid.datagrid('beginEdit', index);
                                editRow = index;
                                $datagrid.datagrid('unselectAll');
                            }
                        } else {

                        }
                    }
                }, '-', {
                    text: '复制', iconCls: 'icon-cut', handler: function () {
                        var row = $datagrid.datagrid('getSelected');
                        var index = $datagrid.datagrid('getRowIndex', row);

                        $.get('CheckPointTemplatesPageHandler.ashx?action=CopyCheckPointTemplates', { oid: row.OID }, function (data) {
                            if (data == "复制成功") {
                                $datagrid.datagrid('reload');
                            }

                            showFaceMsg(data);


                        });
                    }
                }],
                onAfterEdit: function (rowIndex, rowData, changes) {
                    editRow = undefined;
                },
                onDblClickRow: function (rowIndex, rowData) {
                    if (editRow != undefined) {
                        $datagrid.datagrid('endEdit', editRow);
                    }

                    if (editRow == undefined) {
                        $datagrid.datagrid('beginEdit', rowIndex);
                        editRow = rowIndex;
                    }
                },
                onClickRow: function (rowIndex, rowData) {
                    if (editRow != undefined) {
                        $datagrid.datagrid('endEdit', editRow);

                    }
                    $cptOID = rowData.OID;
                    LoadCheckPointPositionList(rowData.OID);

                }

            });


        });

        //测点位置列表
        function LoadCheckPointPositionList(cptOID) {
            var $width = $("#rightPanel").width();
            var $height = $("#rightPanel").height();
            var editRow = undefined;
            var $datagrid = $("#CheckPointPositionList");
            $datagrid.datagrid({
                height: $height,
                width: $width,
                fitColumns: true,
                collapsible: true,
                singleSelect: true,
                url: 'CheckPointTemplatesPageHandler.ashx',
                queryParams: { action: 'GetCheckPointPositionList', CheckPointTemplates_OID: cptOID },
                idField: 'OID',
                columns: [[
                 { field: 'OID', title: 'ID', hidden: true },
                    { field: 'CheckPointTemplates_OID', title: 'CheckPointTemplates_OID', hidden: true },
                     {
                         field: 'SortNo', width: 50, title: "序号", align: 'center', editor: { type: 'text', options: { required: true } }
                     },
                  {
                      field: 'CheckPointPosition', align: 'center', width: 150, title: "测点位置", editor: { type: 'text', options: { required: true } }
                  },
         { field: 'PositionName', align: 'center', width: 150, title: '位置名称', editor: { type: 'text', options: { required: true } } },
         { field: 'Height', align: 'center', width: 150, title: '高度', editor: { type: 'text', options: { required: true } } },
         { field: 'Remarks', align: 'center', width: 150, title: '备注', editor: { type: 'text', options: { required: true } } }
                ]],
                toolbar: [{
                    text: '添加', iconCls: 'icon-add', handler: function () {
                        if (editRow != undefined) {
                            $datagrid.datagrid('endEdit', editRow);
                        }
                        if (editRow == undefined) {


                            $datagrid.datagrid('insertRow', {
                                index: 0,
                                row: { CheckPointTemplates_OID: cptOID }//{ CheckPointName: ', CheckPointCode: ', CollectionDatetime: formatDatebox(new Date()), CollectionBy: _userName, CollectionType: "离线" }
                            });

                            $datagrid.datagrid('beginEdit', 0);
                            editRow = 0;
                        }
                    }
                }, '-', {
                    text: '保存', iconCls: 'icon-save', handler: function () {

                        var editIndex = editRow;

                        $datagrid.datagrid('endEdit', editRow);

                        $datagrid.datagrid('selectRow', editIndex)
                        var rowstr = $datagrid.datagrid('getSelected', editIndex)
                        //如果调用acceptChanges(),使用getChanges()则获取不到编辑和新增的数据。

                        //使用JSON序列化datarow对象，发送到后台。
                        //  var rows = $("#Student_Table").datagrid('getChanges');

                        //  var rowstr = JSON.stringify(rows[0]);
                        $.post('CheckPointTemplatesPageHandler.ashx?action=SaveCheckPointPosition', rowstr, function (data) {

                            showFaceMsg(data);
                        });

                    }
                }, '-', {
                    text: '删除', iconCls: 'icon-remove', handler: function () {
                        var row = $datagrid.datagrid('getSelected');
                        var index = $datagrid.datagrid('getRowIndex', row);


                        $.post('CheckPointTemplatesPageHandler.ashx?action=DelCheckPointPosition', { oid: row.OID }, function (data) {
                            if (data == "删除成功") {
                                $datagrid.datagrid('deleteRow', index);
                            }

                            showFaceMsg(data);

                        });

                    }
                }, '-', {
                    text: '修改', iconCls: 'icon-edit', handler: function () {
                        var row = $datagrid.datagrid('getSelected');
                        if (row != null) {
                            if (editRow != undefined) {
                                $datagrid.datagrid('endEdit', editRow);
                            }

                            if (editRow == undefined) {
                                var index = $datagrid.datagrid('getRowIndex', row);
                                $datagrid.datagrid('beginEdit', index);
                                editRow = index;
                                $datagrid.datagrid('unselectAll');
                            }
                        } else {

                        }
                    }
                }],
                onAfterEdit: function (rowIndex, rowData, changes) {
                    editRow = undefined;
                },
                onDblClickRow: function (rowIndex, rowData) {
                    if (editRow != undefined) {
                        $datagrid.datagrid('endEdit', editRow);
                    }

                    if (editRow == undefined) {
                        $datagrid.datagrid('beginEdit', rowIndex);
                        editRow = rowIndex;
                    }
                },
                onClickRow: function (rowIndex, rowData) {
                    if (editRow != undefined) {
                        $datagrid.datagrid('endEdit', editRow);

                    }

                }

            });
        }
    </script>
</body>
</html>

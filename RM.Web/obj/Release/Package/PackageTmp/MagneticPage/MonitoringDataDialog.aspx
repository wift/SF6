﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MonitoringDataDialog.aspx.cs" Inherits="RM.Web.MagneticPage.MonitoringDataDialog" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>测点历史表</title>

 <script type="text/javascript" src="../Themes/Scripts/jqueryeasyui/jquery.min.js"></script>
        <script src="../Themes/Scripts/Validator/JValidator.js" type="text/javascript"></script>
    <script src="../Themes/Scripts/jquery.pullbox.js" type="text/javascript"></script>
    <script src="../Themes/Scripts/FunctionJS.js" type="text/javascript"></script>
    <script src="../Themes/Scripts/DatePicker/WdatePicker.js" type="text/javascript"></script>
    <link href="../Themes/Scripts/jqueryeasyui/themes/bootstrap/easyui.css" rel="stylesheet" />
    <link href="../Themes/Scripts/jqueryeasyui/themes/icon.css" rel="stylesheet" />
    <link href="../Themes/Scripts/jqueryeasyui/themes/color.css" rel="stylesheet" />
    <style type="text/css">
        html, body {
            margin: 0;
            height: 100%;
        }

        #containt {
            height: 100%;
        }

        form {
            height: 100%;
        }

        .datachart {
            height: -moz-calc(100% - 350px);
            height: -webkit-calc(100% - 350px);
            height: calc(100% - 350px);
        }

        .inner_cell_right {
            /*width:100%;*/
            text-align: right;
            vertical-align: middle;
        }

        #infotable {
            font-size: 9pt;
        }

        .floatDiv {
            position: absolute;
            top: 370px;
            right: 10px;
        }
        /**表格 begin**/
        .grid {
            margin: 0px;
            border-collapse: collapse;
            width: 100%;
            table-layout: fixed;
        }

            .grid thead td {
                border-top: 1px solid #ccc;
                border-bottom: 1px solid #ccc;
                border-right: 1px dotted #ccc;
                background: url(../Images/datagrid_header_bg.gif) repeat-x;
                text-align: center;
                padding: 5px 1px;
                font-weight: normal;
                text-overflow: ellipsis;
                word-break: keep-all;
                overflow: hidden;
            }

            .grid tbody td {
                text-align: left;
                border-bottom: 1px dotted #ccc;
                border-right: 1px dotted #ccc;
                padding: 1px 1px;
                height: 20px;
                word-break: break-all;
            }

            .grid tbody .alt {
                background: #F7F7F7;
            }

            .grid tbody .selected {
                background2: #D0E5F5;
                background1: #FBEC88;
                background: #e0eccc;
            }

        .datagrid-toolbar:after {
            content: "单位：电场强度（kV/m）、磁感应强度（μT）";
            top: -20px;
            float:right;
            position: relative;
           clear: both; 
           height: 0; 
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div id="containt">
            <table width="100%" id="infotable">
                <colgroup>
                    <col width="7%" />
                    <col width="7%" />
                    <col width="7%" />
                    <col width="7%" />
                    <col width="7%" />
                    <col width="7%" />
                    <col width="8%" />
                    <col width="7%" />
                </colgroup>
                <tr>
                    <td class="inner_cell_right">测点编码：
                    </td>
                    <td>
                        <%= Magnetic_CheckPointInfoObj.CheckPointCode%>
                    </td>
                    <td class="inner_cell_right">测点名称：
                    </td>
                    <td>
                        <%= Magnetic_CheckPointInfoObj.CheckPointName%>
                    </td>
                    <td class="inner_cell_right">仪器编码：
                    </td>
                    <td>
                        <%= Magnetic_CheckPointInfoObj.InstrumentCode%>
                    </td>
                    <td class="inner_cell_right">仪器名称：
                    </td>
                    <td>
                        <%= Magnetic_CheckPointInfoObj.InstrumentName%>
                    </td>
                </tr>
                <tr>
                    <td class="inner_cell_right">测点位置：
                    </td>
                    <td>
                        <%= Magnetic_CheckPointInfoObj.CheckPointPositionName%>
                    </td>
                    <td class="inner_cell_right">经度：
                    </td>
                    <td>
                        <%= Magnetic_CheckPointInfoObj.Latitude%>
                    </td>
                    <td class="inner_cell_right">纬度：
                    </td>
                    <td>
                        <%= Magnetic_CheckPointInfoObj.Longitude%>
                    </td>
                    <td class="inner_cell_right">所属变电站：
                    </td>
                    <td>
                        <%= Magnetic_CheckPointInfoObj.ConvertStationName%>
                    </td>
                </tr>
                <tr>
                    <td class="inner_cell_right">运行状态：
                    </td>
                    <td>
                        <%= DictEGMNS["RunStatus"][Magnetic_CheckPointInfoObj.RunStatus??""]%>
                    </td>
                    <td class="inner_cell_right">电压等级：
                    </td>
                    <td>
                        <%= DictEGMNS["DYLev"][Magnetic_CheckPointInfoObj.VoltageLevel??""]%>

                    </td>
                    <td class="inner_cell_right">电流（A）：
                    </td>
                    <td>

                        <%= Magnetic_CheckPointInfoObj.Electricity%>
                    </td>
                    <td class="inner_cell_right">负荷（MW）：
                    </td>
                    <td>

                        <%= Magnetic_CheckPointInfoObj.Eload%>
                    </td>

                </tr>
            </table>
            <hr />
            <div style="padding: 10px; width: 1245px; height: 615px;">
                <table id="Student_Table">
                </table>
                <br />
                <div class="datachart">
                </div>
            </div>
            <div class="floatDiv">
                最近
            <select id="rangeSelect" name="rangeDate" runat="server">
                <option>10</option>
                <option>20</option>
                <option>30</option>
            </select>
                天
            <input id="refresh" value="查询" type="button" />
            </div>
        </div>
    </form>
    <script type="text/javascript" src="../Themes/Scripts/highcharts.js"></script>
    <script type="text/javascript" src="../Themes/Scripts/jqueryeasyui/jquery.easyui.min.js"></script>
    <script src="../Themes/Scripts/jqueryeasyui/plugins/jquery.edatagrid.js" type="text/javascript"></script>
    <script type="text/javascript">



        function formatDatebox(value) {
           
            if (value == null || value == '') {
                return '';
            }
     
            var dt;
            if (value instanceof Date) {
                dt = value;
            } else {
               // dt = new Date(value.replace('T', ' '));
                return value.replace('T', ' ');
            }
           
            return dt.format("yyyy-MM-dd hh:mm:ss");
        }

        function fixWidth(percent) {
            return (document.body.clientWidth - 5) * percent;
        }

        //组分历史数据
        function GetTable() {
            var editRow = undefined;
            var $width = 1200;
            $("#Student_Table").datagrid({
                height: 250,
                width: $width,
                fitColumns: true,
                collapsible: true,
                singleSelect: true,
                url: 'Magnetic_CheckPointInfoHandler.ashx',
                queryParams: { action: 'GetMagnetic_CheckPointInfoChilds', Magnetic_CheckPointInfo_OID: '<%= Magnetic_CheckPointInfoObj.OID %>' },
                idField: 'OID',
                columns: [[
                     { field: 'OID', title: 'ID', hidden: true },

             { field: 'CheckPointName', align: 'center', width: fixWidth(0.12), title: '测点名称' },
                {
                    field: 'CollectionDatetime', align: 'center', width: fixWidth(0.10), title: '采集时间', formatter: formatDatebox, editor: { type: 'datetimebox', options: { required: true } }
                },
                { field: 'ElectricFieldMAX', title: '电场强度MAX', width: fixWidth(0.07), align: 'center', editor: { type: 'text', options: { required: true } } },
                { field: 'ElectricFieldMIN', title: '电场强度MIN', width: fixWidth(0.07), align: 'center', editor: { type: 'text', options: { required: true } } },
                { field: 'ElectricFieldRMS', title: '电场强度RMS', width: fixWidth(0.07), align: 'center', editor: { type: 'text', options: { required: true } } },
                { field: 'MagneticInductionMAX', title: '磁感应强度MAX', width: fixWidth(0.07), align: 'center', editor: { type: 'text', options: { required: true } } },
                { field: 'MagneticInductionMIN', title: '磁感应强度MIN', width: fixWidth(0.07), align: 'center', editor: { type: 'text', options: { required: true } } },
                { field: 'MagneticInductionRMS', title: '磁感应强度RMS', width: fixWidth(0.07), align: 'center', editor: { type: 'text', options: { required: true } } },
                { field: 'Temperature', title: '温度(℃)', width: fixWidth(0.05), align: 'center', editor: { type: 'text', options: { required: true } } },
                { field: 'Humidity', title: '湿度(%)', width: fixWidth(0.05), align: 'center', editor: { type: 'text', options: { required: true } } },

                  {
                      field: 'CollectionType', width: fixWidth(0.06), title: '采集类型', align: 'center'
                  },
                   { field: 'CollectionBy', width: fixWidth(0.06), title: '采集人', align: 'center' }
                ]],
                toolbar: [{
                    text: '添加', iconCls: 'icon-add', handler: function () {
                        if (editRow != undefined) {
                            $("#Student_Table").datagrid('endEdit', editRow);
                        }
                        if (editRow == undefined) {
                            var _userName = '<%= User.UserName %>';

                            $("#Student_Table").datagrid('insertRow', {
                                index: 0,
                                row: { CheckPointName: '<%= Magnetic_CheckPointInfoObj.CheckPointName %>', CollectionDatetime: formatDatebox(new Date()), CollectionBy: _userName, CollectionType: "离线", Magnetic_CheckPointInfo_OID: <%= Magnetic_CheckPointInfoObj.OID %> }
                            });

                            $("#Student_Table").datagrid('beginEdit', 0);
                            editRow = 0;
                        }
                    }
                }, '-', {
                    text: '保存', iconCls: 'icon-save', handler: function () {

                        var editIndex = editRow;

                        $("#Student_Table").datagrid('endEdit', editRow);

                        $("#Student_Table").datagrid('selectRow', editIndex)
                        var rowstr = $("#Student_Table").datagrid('getSelected', editIndex)
                        //如果调用acceptChanges(),使用getChanges()则获取不到编辑和新增的数据。

                        //使用JSON序列化datarow对象，发送到后台。
                        //  var rows = $("#Student_Table").datagrid('getChanges');

                        //  var rowstr = JSON.stringify(rows[0]);
                        $.post('Magnetic_CheckPointInfoHandler.ashx?action=SaveMagnetic_CheckPointInfoChilds', rowstr, function (data) {
                            showFaceMsg(data);
                        });

                    }
                }, '-', {
                    text: '删除', iconCls: 'icon-remove', handler: function () {
                        var row = $("#Student_Table").datagrid('getSelected');
                        var index = $("#Student_Table").datagrid('getRowIndex', row);


                        $.post('Magnetic_CheckPointInfoHandler.ashx?action=DelMagnetic_CheckPointInfoChilds', { oid: row.OID }, function (data) {
                            if (data == "删除成功") {
                                $("#Student_Table").datagrid('deleteRow', index);;
                            }  
                            
                            showFaceMsg(data);    
                        });

                    }
                }, '-', {
                    text: '修改', iconCls: 'icon-edit', handler: function () {
                        var row = $("#Student_Table").datagrid('getSelected');
                        if (row != null) {
                            if (editRow != undefined) {
                                $("#Student_Table").datagrid('endEdit', editRow);
                            }

                            if (editRow == undefined) {
                                var index = $("#Student_Table").datagrid('getRowIndex', row);
                                $("#Student_Table").datagrid('beginEdit', index);
                                editRow = index;
                                $("#Student_Table").datagrid('unselectAll');
                            }
                        } else {

                        }
                    }
                }, '-', {
                    text: '导出', iconCls: 'icon-edit', handler: function () {
                        //getExcelXML有一个JSON对象的配置，配置项看了下只有title配置，为excel文档的标题
                        var data = $('#Student_Table').datagrid('getExcelXml', { title: 'datagrid import to excel' }); //获取datagrid数据对应的excel需要的xml格式的内容
                        //用ajax发动到动态页动态写入xls文件中
                        var url = 'datagrid_to_excelHandler.ashx'; //如果为asp注意修改后缀
                        $.ajax({
                            url: url, data: { data: data }, type: 'POST', dataType: 'text',
                            success: function (fn) {
                                alert('导出excel成功！');
                                window.location = fn; //执行下载操作
                            },
                            error: function (xhr) {
                                alert('动态页有问题\nstatus：' + xhr.status + '\nresponseText：' + xhr.responseText)
                            }
                        });
                        return false;
                    }
                }],
                onAfterEdit: function (rowIndex, rowData, changes) {
                    editRow = undefined;
                },
                onDblClickRow: function (rowIndex, rowData) {
                    if (editRow != undefined) {
                        $("#Student_Table").datagrid('endEdit', editRow);
                    }

                    if (editRow == undefined) {
                        $("#Student_Table").datagrid('beginEdit', rowIndex);
                        editRow = rowIndex;
                    }
                },
                onClickRow: function (rowIndex, rowData) {
                    if (editRow != undefined) {
                        $("#Student_Table").datagrid('endEdit', editRow);

                    }

                }

            });


            //  $(".datagrid-toolbar").append('<div>单位：电场强度（kV/m）、磁感应强度（μT）</div>');
        }

        $(document).ready(function myfunction() {
            GetTable();

            GetChart();
            $("#refresh").click(function myfunction() {
                GetChart();
          
            });
        });

        function GetChart() {

            $.get('Magnetic_CheckPointInfoHandler.ashx', { action: 'GetMagnetic_CheckPointInfoChildsChart', Magnetic_CheckPointInfo_OID: '<%= Magnetic_CheckPointInfoObj.OID %>', rangeSelect: $("#rangeSelect").val() }, function (results) {

                var listDatetimeJson = results.listDatetime;
                var listChartDataJson = results.listChartData;

                $('.datachart').highcharts({
                    title: {
                        text: '测点历史数据',
                        x: -20 //center
                    },
                    xAxis: {
                        categories: listDatetimeJson
                    },
                    yAxis: {
                        title: {
                            text: null
                        },
                        plotLines: [{
                            value: 0,
                            width: 1,
                            color: '#808080'
                        }]
                    },
                    tooltip: {
                        formatter: function () {
                            var returnValue = '';

                            if ("ElectricFieldMAX" == this.series.name) {
                                returnValue="电场强度MAX";
                            }
                            else if ("ElectricFieldMIN" == this.series.name) {
                                returnValue="电场强度MIN";
                            }
                            else if ("ElectricFieldRMS" == this.series.name) {

                                returnValue= "电场强度RMS";
                            }
                            else if ("MagneticInductionMAX" == this.series.name) {
                                returnValue= "磁感应强度MAX";
                            }
                            else if ("MagneticInductionMIN" ==this.series.name) {
                                returnValue= "磁感应强度MIN";
                            }
                            else if ("MagneticInductionRMS" == this.series.name) {
                                returnValue= "磁感应强度RMS";
                            }
                            else if ("Temperature" == this.series.name) {
                                returnValue= "温度";
                            }
                            else if ("Humidity" == this.series.name) {
                                returnValue= "湿度";
                            }

                            return this.x+"<br/>"+returnValue + ':' + this.y;
      
                        }
                    },
                    legend: {
                        layout: 'vertical',
                        align: 'right',
                        verticalAlign: 'middle',
                        borderWidth: 0,
                        itemStyle: { color: '#7cb5ec' },
                        labelFormatter: function () {
                            if ("ElectricFieldMAX" == this.name) {
                                return "电场强度MAX";
                            }
                            else if ("ElectricFieldMIN" == this.name) {
                                return "电场强度MIN";
                            }
                            else if ("ElectricFieldRMS" == this.name) {

                                return "电场强度RMS";
                            }
                            else if ("MagneticInductionMAX" == this.name) {
                                return "磁感应强度MAX";
                            }
                            else if ("MagneticInductionMIN" == this.name) {
                                return "磁感应强度MIN";
                            }
                            else if ("MagneticInductionRMS" == this.name) {
                                return "磁感应强度RMS";
                            }
                            else if ("Temperature" == this.name) {
                                return "温度";
                            }
                            else if ("Humidity" == this.name) {
                                return "湿度";
                            }

                        }
                    },
                    series: listChartDataJson
                });

       

            });
        }

        $.extend($.fn.datagrid.methods, {
            getExcelXml: function (jq, param) {
                var worksheet = this.createWorksheet(jq, param);
                //alert($(jq).datagrid('getColumnFields'));
                var totalWidth = 0;
                var cfs = $(jq).datagrid('getColumnFields');
                for (var i = 1; i < cfs.length; i++) {
                    totalWidth += $(jq).datagrid('getColumnOption', cfs[i]).width;
                }
                //var totalWidth = this.getColumnModel().getTotalWidth(includeHidden);
                return '<?xml version="1.0" encoding="utf-8"?>' + //xml申明有问题，以修正，注意是utf-8编码，如果是gb2312，需要修改动态页文件的写入编码
                  '<ss:Workbook xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns:o="urn:schemas-microsoft-com:office:office">' +
                  '<o:DocumentProperties><o:Title>' + param.title + '</o:Title></o:DocumentProperties>' +
                  '<ss:ExcelWorkbook>' +
                  '<ss:WindowHeight>' + worksheet.height + '</ss:WindowHeight>' +
                  '<ss:WindowWidth>' + worksheet.width + '</ss:WindowWidth>' +
                  '<ss:ProtectStructure>False</ss:ProtectStructure>' +
                  '<ss:ProtectWindows>False</ss:ProtectWindows>' +
                  '</ss:ExcelWorkbook>' +
                  '<ss:Styles>' +
                  '<ss:Style ss:ID="Default">' +
                  '<ss:Alignment ss:Vertical="Top"  />' +
                  '<ss:Font ss:FontName="arial" ss:Size="10" />' +
                  '<ss:Borders>' +
                  '<ss:Border  ss:Weight="1" ss:LineStyle="Continuous" ss:Position="Top" />' +
                  '<ss:Border  ss:Weight="1" ss:LineStyle="Continuous" ss:Position="Bottom" />' +
                  '<ss:Border  ss:Weight="1" ss:LineStyle="Continuous" ss:Position="Left" />' +
                  '<ss:Border ss:Weight="1" ss:LineStyle="Continuous" ss:Position="Right" />' +
                  '</ss:Borders>' +
                  '<ss:Interior />' +
                  '<ss:NumberFormat />' +
                  '<ss:Protection />' +
                  '</ss:Style>' +
                  '<ss:Style ss:ID="title">' +
                  '<ss:Borders />' +
                  '<ss:Font />' +
                  '<ss:Alignment  ss:Vertical="Center" ss:Horizontal="Center" />' +
                  '<ss:NumberFormat ss:Format="@" />' +
                  '</ss:Style>' +
                  '<ss:Style ss:ID="headercell">' +
                  '<ss:Font ss:Bold="1" ss:Size="10" />' +
                  '<ss:Alignment  ss:Horizontal="Center" />' +
                  '<ss:Interior ss:Pattern="Solid"  />' +
                  '</ss:Style>' +
                  '<ss:Style ss:ID="even">' +
                  '<ss:Interior ss:Pattern="Solid"  />' +
                  '</ss:Style>' +
                  '<ss:Style ss:Parent="even" ss:ID="evendate">' +
                  '<ss:NumberFormat ss:Format="yyyy-mm-dd" />' +
                  '</ss:Style>' +
                  '<ss:Style ss:Parent="even" ss:ID="evenint">' +
                  '<ss:NumberFormat ss:Format="0" />' +
                  '</ss:Style>' +
                  '<ss:Style ss:Parent="even" ss:ID="evenfloat">' +
                  '<ss:NumberFormat ss:Format="0.00" />' +
                  '</ss:Style>' +
                  '<ss:Style ss:ID="odd">' +
                  '<ss:Interior ss:Pattern="Solid"  />' +
                  '</ss:Style>' +
                  '<ss:Style ss:Parent="odd" ss:ID="odddate">' +
                  '<ss:NumberFormat ss:Format="yyyy-mm-dd" />' +
                  '</ss:Style>' +
                  '<ss:Style ss:Parent="odd" ss:ID="oddint">' +
                  '<ss:NumberFormat ss:Format="0" />' +
                  '</ss:Style>' +
                  '<ss:Style ss:Parent="odd" ss:ID="oddfloat">' +
                  '<ss:NumberFormat ss:Format="0.00" />' +
                  '</ss:Style>' +
                  '</ss:Styles>' +
                  worksheet.xml +
                  '</ss:Workbook>';
            },
            createWorksheet: function (jq, param) {
                // Calculate cell data types and extra class names which affect formatting
                var cellType = [];
                var cellTypeClass = [];
                //var cm = this.getColumnModel();
                var totalWidthInPixels = 0;
                var colXml = '';
                var headerXml = '';
                var visibleColumnCountReduction = 0;
                var cfs = $(jq).datagrid('getColumnFields');
                var colCount = cfs.length;
                for (var i = 1; i < colCount; i++) {
                    if (cfs[i] != '') {
                        var w = $(jq).datagrid('getColumnOption', cfs[i]).width;
                        totalWidthInPixels += w;
                        if (cfs[i] === "") {
                            cellType.push("None");
                            cellTypeClass.push("");
                            ++visibleColumnCountReduction;
                        }
                        else {
                            colXml += '<ss:Column ss:AutoFitWidth="1" ss:Width="130" />';
                            headerXml += '<ss:Cell ss:StyleID="headercell">' +
                              '<ss:Data ss:Type="String">' + $(jq).datagrid('getColumnOption', cfs[i]).title + '</ss:Data>' +
                              '<ss:NamedCell ss:Name="Print_Titles" /></ss:Cell>';
                            cellType.push("String");
                            cellTypeClass.push("");
                        }
                    }
                }
                var visibleColumnCount = cellType.length - visibleColumnCountReduction;
                var result = {
                    height: 9000,
                    width: Math.floor(totalWidthInPixels * 30) + 50
                };
                var rows = $(jq).datagrid('getRows');
                // Generate worksheet header details.
                var t = '<ss:Worksheet ss:Name="' + param.title + '">' +
                  '<ss:Names>' +
                  '<ss:NamedRange ss:Name="Print_Titles" ss:RefersTo="=\'' + param.title + '\'!R1:R2" />' +
                  '</ss:Names>' +
                  '<ss:Table x:FullRows="1" x:FullColumns="1"' +
                  ' ss:ExpandedColumnCount="' + (visibleColumnCount + 2) +
                  '" ss:ExpandedRowCount="' + (rows.length + 2) + '">' +
                  colXml +
                  '<ss:Row ss:AutoFitHeight="1">' +
                  headerXml +
                  '</ss:Row>';
                // Generate the data rows from the data in the Store
                //for (var i = 0, it = this.store.data.items, l = it.length; i < l; i++) {
                for (var i = 0, it = rows, l = it.length; i < l; i++) {
                    t += '<ss:Row>';
                    var cellClass = (i & 1) ? 'odd' : 'even';
                    r = it[i];
                    var k = 0;
                    for (var j = 1; j < colCount; j++) {
                        //if ((cm.getDataIndex(j) != '')
                        if (cfs[j] != '') {
                            //var v = r[cm.getDataIndex(j)];
                            var v = r[cfs[j]];
                            if (cellType[k] !== "None") {
                                debugger;
                                t += '<ss:Cell ss:StyleID="' + cellClass + cellTypeClass[k] + '"><ss:Data ss:Type="' + cellType[k] + '">';
                                if (cellType[k] == 'DateTime') {
                                    t += v.format('Y-m-d')
                                } else {
                                    t += v.toString().replace("T",' ');
                                }
                                t += '</ss:Data></ss:Cell>';
                            }
                            k++;
                        }
                    }
                    t += '</ss:Row>';
                }
                result.xml = t + '</ss:Table>' +
                  '<x:WorksheetOptions>' +
                  '<x:PageSetup>' +
                  '<x:Layout x:CenterHorizontal="1" x:Orientation="Landscape" />' +
                  '<x:Footer x:Data="Page &P of &N" x:Margin="0.5" />' +
                  '<x:PageMargins x:Top="0.5" x:Right="0.5" x:Left="0.5" x:Bottom="0.8" />' +
                  '</x:PageSetup>' +
                  '<x:FitToPage />' +
                  '<x:Print>' +
                  '<x:PrintErrors>Blank</x:PrintErrors>' +
                  '<x:FitWidth>1</x:FitWidth>' +
                  '<x:FitHeight>32767</x:FitHeight>' +
                  '<x:ValidPrinterInfo />' +
                  '<x:VerticalResolution>600</x:VerticalResolution>' +
                  '</x:Print>' +
                  '<x:Selected />' +
                  '<x:DoNotDisplayGridlines />' +
                  '<x:ProtectObjects>False</x:ProtectObjects>' +
                  '<x:ProtectScenarios>False</x:ProtectScenarios>' +
                  '</x:WorksheetOptions>' +
                  '</ss:Worksheet>';
                return result;
            }
        });

        $.extend($.fn.datagrid.defaults.editors, {
            datetimebox: {//datetimebox就是你要自定义editor的名称
                init: function (container, options) {
                    var editor = $('<input />').appendTo(container);
                    editor.enableEdit = false;
                    editor.datetimebox(options);
                    return editor;
                },
                getValue: function (target) {
                    return formatDatebox($(target).datetimebox('getValue'));

                },
                setValue: function (target, value) {
                    if (value)
                        $(target).datetimebox('setValue', formatDatebox1(value));
                    else
                        $(target).datetimebox('setValue', new Date().format("yyyy-MM-dd hh:mm:ss"));
                },
                resize: function (target, width) {
                    $(target).datetimebox('resize', width);
                },
                destroy: function (target) {
                    $(target).datetimebox('destroy');
                }
            }
        });


        function formatDatebox1(value) {
            if (value == null || value == '') {
                return '';
            }
            var dt;
            if (value instanceof Date) {
                dt = value;
            } else {
               // dt = new Date(value.replace('T', ' '));
                return value.replace('T', ' ');
            }

            return dt.format("MM/dd/yyyy hh:mm:ss");
        }




    </script>
</body>
</html>

﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Megnetic_CurrentAnalysisPage.aspx.cs"
    Inherits="RM.Web.MagneticPage.Megnetic_CurrentAnalysisPage" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>数据分析诊断</title>
    <%-- <link href="~/Themes/Styles/Site.css" rel="stylesheet" type="text/css" />--%>
    <script src="../Themes/Scripts/jquery-1.8.2.min.js" type="text/javascript"></script>
    <script src="../Themes/Scripts/jquery.pullbox.js" type="text/javascript"></script>
    <script src="../Themes/Scripts/FunctionJS.js" type="text/javascript"></script>
    <script src="../Themes/Scripts/DatePicker/WdatePicker.js" type="text/javascript"></script>
    <link href="../Themes/Scripts/jqueryeasyui/themes/bootstrap/easyui.css" rel="stylesheet" />
    <link href="../Themes/Scripts/jqueryeasyui/themes/icon.css" rel="stylesheet" />
    <style type="text/css">
        html, body
        {
            margin: 0;
            height: 100%;
            font-size: 9pt;
        }

        #containt
        {
            height: 100%;
        }

        form
        {
            height: 100%;
        }

        #UpdatePanel
        {
            height: 100%;
        }

        #containt > #UpdatePanel > .Panel
        {
            float: left;
            height: -moz-calc(100% - 0px);
            height: -webkit-calc(100% - 00px);
            height: calc(100% - 0px);
        }

        .itemPanel
        {
            width: 170px;
            height: 170px;
            float: left;
            margin: 10px 10px 10px 10px;
            border-width: 1px;
            border-style: solid;
            cursor: pointer;
        }

            .itemPanel > div
            {
                float: left;
                height: -moz-calc(100% - 0px);
                height: -webkit-calc(100% - 00px);
                height: calc(100% - 0px);
            }

                .itemPanel > div > p
                {
                    margin-top: 0px;
                    margin-bottom: 0px;
                    font-size: 12px;
                }

        .leftItem
        {
            border-right-width: 1px;
            border-right-style: solid;
        }

        .datachart
        {
            height: -moz-calc(100% - 300px);
            height: -webkit-calc(100% - 300px);
            height: calc(100% - 300px);
        }
        /**表格 begin**/
        .grid
        {
            margin: 0px;
            border-collapse: collapse;
            width: 100%;
            table-layout: fixed;
        }

            .grid thead td
            {
                border-top: 1px solid #ccc;
                border-bottom: 1px solid #ccc;
                border-right: 1px dotted #ccc;
                background: url(../Themes/Images/datagrid_header_bg.gif) repeat-x;
                text-align: center;
                padding: 5px 1px;
                font-weight: normal;
                text-overflow: ellipsis;
                word-break: keep-all;
                overflow: hidden;
            }

            .grid tbody td
            {
                text-align: left;
                border-bottom: 1px dotted #ccc;
                border-right: 1px dotted #ccc;
                padding: 1px 1px;
                height: 20px;
                word-break: break-all;
            }

            .grid tbody .alt
            {
                background: #F7F7F7;
            }

            .grid tbody .selected
            {
                background: #e0eccc;
            }
        .inner_cell_right
        {
            text-align:right;
        }
        .div-body
        {
            max-height:350px;
            overflow-y:scroll;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div id="containt">
            <asp:ScriptManager ID="ScriptManager1" runat="server">
            </asp:ScriptManager>
            <asp:UpdatePanel ID="UpdatePanel" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="leftPanel" class="Panel" style="width: 30%; border-right-width: 1px; border-right-style: solid;">
                        <div id="searchDiv" style="height: 200px;">
                            测点查询
                        <table style="width: 100%">
                            <tr>
                                <td class="inner_cell_right">供电局：
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlPowerSupplyName" runat="server" CssClass="select" AutoPostBack="true" OnSelectedIndexChanged="ddlPowerSupplyName_SelectedIndexChanged"
                                         Width="131" />
                                </td>
                            </tr>
                            <tr>
                                <td class="inner_cell_right">所属变电站：
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlConvertStationName" runat="server" CssClass="select" Width="131" />
                                </td>
                            </tr>

                            <tr>
                                <td class="inner_cell_right">电压等级
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlVoltageLevel" runat="server" CssClass="select" Width="131" AutoPostBack="true" OnSelectedIndexChanged="ddlVoltageLevel_SelectedIndexChanged" />
                                </td>
                            </tr>
                            <tr>
                                <td class="inner_cell_right">模板类型</td>
                                <td>

                                    <asp:DropDownList ID="ddlTemplateType" runat="server" CssClass="select" Width="155" />
                                </td>
                            </tr>
                            <tr>
                                <td class="inner_cell_right">测点位置：
                                </td>
                                <td>

                                    <asp:TextBox ID="txtCheckPointPositionName" runat="server" />
                                </td>
                            </tr>

                            <tr>
                                <td class="inner_cell_right">位置名称：
                                </td>
                                <td>

                                    <asp:TextBox ID="txtPositionName" runat="server" ></asp:TextBox>

                                </td>

                            </tr>
                            <tr>
                                <td colspan="2" style="text-align: center;">
                                    <asp:Button ID="btnSearch" runat="server" OnClick="btnSearch_Click" Text="查询" />
                                </td>
                            </tr>
                        </table>
                        </div>
                        <div class="div-body">
                            <table id="table1" class="grid" singleselect="false">
                                <colgroup>
                                    <col width="8%" />
                                    <col width="20%" />
                                    <col width="17%" />
                                    <col width="20%" />
                                    <col width="15%" />
                                    <col width="20%" />
                                </colgroup>
                                <thead>
                                    <tr>
                                        <td style="text-align: center;"></td>
                                        <td style="text-align: center;">所属变电站
                                        </td>
                                        <td style="text-align: center;">模板类型
                                        </td>
                                        <td style="text-align: center;">测点位置
                                        </td>
                                         <td style="text-align: center;">位置名称
                                        </td>
                                         <td style="text-align: center;">测量日期
                                        </td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <asp:Repeater ID="rp_Item" runat="server">
                                        <ItemTemplate>
                                            <tr>
                                                <td style="text-align: center;">
                                                    <input type="checkbox" id="<%# Eval("oid")%>" title="<%# Eval("CheckPointPosition")%>" />
                                                </td>
                                                  <td style="text-align: center;">
                                                    <%# Eval("ConvartStationName")%>
                                                </td>
                                                <td style="text-align: center;">
                                                    <%# Eval("TemplateType")%>
                                                </td>
                                                <td style="text-align: center;">
                                                    <%# Eval("CheckPointPosition")%>
                                                </td>
                                                 <td style="text-align: center;">
                                                    <%# Eval("PositionName")%>
                                                </td>
                                                 <td style="text-align: center;">
                                                    <%# Eval("MeasuringDate","{0:d}")%>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div id="rightPanel" class="Panel" style="width: 69.5%;">
                        <div>
                    
                            <span style="float: right;">单位：电场强度（kV/m）、磁感应强度（μT）</span>
                        </div>

                        <div title="测点历史" style="padding: 10px; width: 1100px;">
                            <table id="listMagnetic_CheckPointInfo">
                            </table>
                            <br />

                            <div>
                                <div style="float: left; padding-top: 2px" id="ConstCheckList">

                                    <span style="font-weight: bold">
                                        <input id="Checkbox1" type="checkbox" value="ElectricFieldMAX" />电场强度MAX</span>
                                    <span style="font-weight: bold">
                                        <input id="Checkbox2" type="checkbox" value="ElectricFieldMIN" />电场强度MIN</span>
                                    <span style="font-weight: bold">
                                        <input id="Checkbox4" type="checkbox" value="ElectricFieldRMS" />电场强度RMS</span>
                                    <span style="font-weight: bold">
                                        <input id="Checkbox5" type="checkbox" value="MagneticInductionMAX" />磁感应强度MAX</span>
                                    <span style="font-weight: bold">
                                        <input id="Checkbox6" type="checkbox" value="MagneticInductionMIN" />磁感应强度MIN</span>
                                    <span style="font-weight: bold">
                                        <input id="Checkbox7" type="checkbox" value="MagneticInductionRMS" />磁感应强度RMS</span>
                                    <span style="font-weight: bold">
                                        <input id="Checkbox8" type="checkbox" value="Temperature" />温度(℃)</span>
                                    <span style="font-weight: bold">
                                        <input id="Checkbox9" type="checkbox" value="Humidity" />湿度(%)</span>
                                </div>
                                <div style="float: left; margin-left: 10px;">
                                    <input id="btnConstRefresh" type="button" value="立即刷新" />
                                </div>
                            </div>
                            <div id="listMagnetic_CheckPointInfoChart">
                            </div>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="ddlPowerSupplyName" EventName="SelectedIndexChanged" />
                    <asp:AsyncPostBackTrigger ControlID="btnSearch" EventName="Click" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </form>
    <script type="text/javascript" src="../Themes/Scripts/highcharts.js"></script>
    <script type="text/javascript" src="../Themes/Scripts/jqueryeasyui/jquery.easyui.min.js"></script>
    <script src="../Themes/Scripts/jqueryeasyui/plugins/jquery.edatagrid.js" type="text/javascript"></script>
    <script type="text/javascript">

        var $arrid = [];
        var $arridnochar = [];
        var $checkPointSourse = [];
        var $fd;
        var $td;
        var $width = 1100;
        $(document).ready(function myfunction() {
            InitControl();
        });
        function InitControl() {
            $.parser.parse();

            var $checkBoxList = $(".div-body").find(":checkbox");
            $checkBoxList.click(function myfunction(o) {
           
                $arrid = [];
                $checkPointSourse = [];
                $arridnochar = [];
                $checkBoxList.filter(":checked").each(function myfunction() {

                    $arrid.push("\'" + $(this).attr("id") + "\'");
                    $arridnochar.push($(this).attr("id"));
                    $checkPointSourse.push($(this).attr("title"));
                });

                GetTable();
            });

            //历史列表刷新
            //$("#btnSearchList").click(function myfunction() {
             
            //    if ($("#form1").form('validate')) {

            //        $fd = $("#formdate").datebox('getValue');
            //       // $td = formatDatebox(new Date());
            //        GetTable();

            //    }
            //});

            $("#btnConstRefresh").click(function myfunction() {

                GetChart();

            });
        }


        //组分历史数据
        function GetTable() {
            var editRow = undefined;
            var $width = 1100;
            $("#listMagnetic_CheckPointInfo").datagrid({
                height: 250,
                width: $width,
                fitColumns: true,
                collapsible: true,
                singleSelect: true,
                url: 'Megnetic',
                queryParams: { action: 'Megnetic_CurrentAnalysisPageList', oids: $arridnochar.join(",") },
                idField: 'OID',
                columns: [[
                     { field: 'OID', title: 'ID', hidden: true },

                { field: 'ConvartStationName', align: 'center', width: fixWidth(0.10), title: '所属变电站' },
             { field: 'CheckPointPosition', align: 'center', width: fixWidth(0.12), title: '测点位置' },
                {
                    field: 'PositionName', align: 'center', width: fixWidth(0.10), title: '位置名称'
                },
                { field: 'MeasuringDate', align: 'center', width: fixWidth(0.08), title: '测量日期',formatter: formatDatebox },
                 { field: 'MeasuringTime', align: 'center', width: fixWidth(0.08), title: '测量时间' },
                { field: 'ElectricFieldMAX', title: '电场强度MAX', width: fixWidth(0.07), align: 'center', editor: { type: 'text', options: { required: true } } },
                { field: 'ElectricFieldMIN', title: '电场强度MIN', width: fixWidth(0.07), align: 'center', editor: { type: 'text', options: { required: true } } },
                { field: 'ElectricFieldRMS', title: '电场强度RMS', width: fixWidth(0.07), align: 'center', editor: { type: 'text', options: { required: true } } },
                { field: 'MagneticInductionMAX', title: '磁感应强度MAX', width: fixWidth(0.07), align: 'center', editor: { type: 'text', options: { required: true } } },
                { field: 'MagneticInductionMIN', title: '磁感应强度MIN', width: fixWidth(0.07), align: 'center', editor: { type: 'text', options: { required: true } } },
                { field: 'MagneticInductionRMS', title: '磁感应强度RMS', width: fixWidth(0.07), align: 'center', editor: { type: 'text', options: { required: true } } },
                { field: 'temperature', title: '温度(℃)', width: fixWidth(0.05), align: 'center', editor: { type: 'text', options: { required: true } } },
                { field: 'humidity', title: '湿度(%)', width: fixWidth(0.05), align: 'center', editor: { type: 'text', options: { required: true } } },

                 ]],

                onAfterEdit: function (rowIndex, rowData, changes) {
                    editRow = undefined;
                },
                onDblClickRow: function (rowIndex, rowData) {
                    if (editRow != undefined) {
                        $("#Student_Table").datagrid('endEdit', editRow);
                    }

                    if (editRow == undefined) {
                        $("#Student_Table").datagrid('beginEdit', rowIndex);
                        editRow = rowIndex;
                    }
                },
                onClickRow: function (rowIndex, rowData) {
                    if (editRow != undefined) {
                        $("#Student_Table").datagrid('endEdit', editRow);

                    }

                }

            });
        }

        function GetChart() {

            var $st = [];
            $("#ConstCheckList > span > :checkbox").filter(":checked").each(function myfunction() {
                $st.push($(this).val());
            });

            $.get('Megnetic', { action: 'Megnetic_CurrentAnalysisPageChart', oids: $arridnochar.join(","), target: $st.toString() }, function (results) {

                var listDatetimeJson = results.listDatetime;
                var listChartDataJson = results.listChartData;

                $('#listMagnetic_CheckPointInfoChart').highcharts({
                    title: {
                        text: '测点历史数据',
                        x: -20 //center
                    },
                    xAxis: {
                        categories: listDatetimeJson,
                        labels:{staggerLines:2
                }
                    },
                    yAxis: {
                        title: {
                            text: null
                        },
                        plotLines: [{
                            value: 0,
                            width: 1,
                            color: '#808080'
                        }]
                    },
                    tooltip: {

                    },
                    legend: {
                        layout: 'vertical',
                        align: 'right',
                        verticalAlign: 'middle',
                        borderWidth: 0,
                        itemStyle: { color: '#7cb5ec' }

                    },
                    series: listChartDataJson
                });
            });
        }

        function formatDatebox(value) {
            if (value == null || value == '') {
                return '';
            }
            var dt;
            if (value instanceof Date) {
                dt = value;
            } else {
                // dt = new Date(value.replace('T', ' '));
                return value.replace('T', ' ');
            }
            return dt.format("yyyy-MM-dd");
        }
        function fixWidth(percent) {
            return (document.body.clientWidth - 5) * percent;
        }


    </script>
</body>
</html>

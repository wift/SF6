﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebFormTree.aspx.cs" Inherits="RM.Web.WebFormTree" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link href="/Themes/Styles/Site.css" rel="stylesheet" type="text/css" />
      <link href="/Themes/Scripts/jqueryeasyui/themes/bootstrap/easyui.css" rel="stylesheet" />
    <link href="/Themes/Scripts/jqueryeasyui/themes/icon.css" rel="stylesheet" />
    <script src="/Themes/Scripts/jquery-1.8.2.min.js" type="text/javascript"></script>
 
    <script type="text/javascript" src="/Themes/Scripts/jqueryeasyui/jquery.easyui.min.js"></script>
  
    <script type="text/javascript">
        $(document).ready(function () {


      

            $('#tt').tree({
                checkbox: false,
                url: 'GanPing/TreeHandler.ashx',
                onBeforeExpand: function (node, param) {
                    $('#tt').tree('options').url = "GanPing/TreeHandler.ashx?ParentNode=" + node.id;
                }
       
            });
        });


    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div class="div-body">
            <ul id="tt" class="easyui-tree">         
            </ul>
        </div>
    </form>
</body>
</html>

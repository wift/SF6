﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CylinderKCMng.aspx.cs"
    Inherits="RM.Web.GanPing.CylinderKCMng" %>

<%@ Register Src="~/UserControl/LoadButton.ascx" TagName="LoadButton" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>钢瓶库存管理 </title>
    <link href="/Themes/Styles/Site.css" rel="stylesheet" type="text/css" />
    <script src="/Themes/Scripts/Validator/JValidator.js" type="text/javascript"></script>
    <script src="/Themes/Scripts/jquery-1.8.2.min.js" type="text/javascript"></script>
    <script src="/Themes/Scripts/jquery.pullbox.js" type="text/javascript"></script>
    <script src="/Themes/Scripts/FunctionJS.js" type="text/javascript"></script>
    <script src="/Themes/Scripts/DatePicker/WdatePicker.js" type="text/javascript"></script>
    <script type="text/javascript">
        function Find() {
            document.getElementById('<%= this.btnFind.ClientID %>').click();
        }

    </script>
</head>
<body>
    <form id="form1" runat="server">
    <table width="100%">
        <colgroup>
            <col width="11%" />
            <col width="11%" />
            <col width="11%" />
            <col width="11%" />
        </colgroup>
        <tr>
            <td class="inner_cell_right">
                日期：
            </td>
            <td>
                <asp:TextBox ID="txtAuditsDate" runat="server" Enabled="false" />
            </td>
            <td class="inner_cell_right">
                所属单位：
            </td>
            <td>
                <asp:DropDownList ID="ddlManufacturer" runat="server" Width="100%" err="此项" checkexpession="NotNull"
                    class="select">
                </asp:DropDownList>
            </td>
        </tr>
    </table>
    <div style="text-align: left;">
        <uc2:LoadButton ID="LoadButton1" runat="server" />
    </div>
    <div style="float: left; display: none;">
        <asp:Button ID="btnFind" runat="server" Text="btnFind" OnClick="btnFind_Click" />
    </div>
    <div class="div-body1">
        <table id="tableDetails" class="grid1">
            <colgroup>
                <col width="11%" />
                <col width="11%" />
                <col width="11%" />
                <col width="11%" />
                <col width="11%" />
                <col width="11%" />
                <col width="11%" />
                <col width="11%" />
                <col width="12%" />
            </colgroup>
            <thead>
                <tr>
                    <td style="text-align: center;">
                        序号
                    </td>
                    <td style="text-align: center;">
                        库存类型
                    </td>
                    <td style="text-align: center;">
                        25KG瓶
                    </td>
                    <td style="text-align: center;">
                        50KG瓶
                    </td>
                    <td style="text-align: center;">
                        合计
                    </td>
                </tr>
            </thead>
            <tbody>
                <asp:Repeater ID="rp_Item" runat="server">
                    <ItemTemplate>
                        <tr>
                            <td style="text-align: center;">
                                <%# Container.ItemIndex + 1%>
                            </td>
                            <td style="text-align: center;">
                                <%#Eval("CODE_CHI_DESC")%>
                            </td>
                            <td style="text-align: center;">
                                <%#Eval("25kg")%>
                            </td>
                            <td style="text-align: center;">
                                <%#Eval("50kg")%>
                            </td>
                            <td style="text-align: center;">
                                <%#Eval("合计")%>
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
            </tbody>
        </table>
    </div>
    </form>
</body>
</html>

﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EquipmentPilotTestDataPage.aspx.cs" Inherits="RM.Web.GanPing.EquipmentPilotTestDataPage" %>

<%@ Register Src="~/UserControl/PageControl.ascx" TagName="PageControl" TagPrefix="uc1" %>
<%@ Register Src="~/UserControl/LoadButton.ascx" TagName="LoadButton" TagPrefix="uc2" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>设备预试数据</title>
    <link href="../Themes/Styles/Site.css" rel="stylesheet" type="text/css" />
    <script src="../Themes/Scripts/Validator/JValidator.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-1.4.1.min.js"></script>
    <script src="../Themes/Scripts/jquery.pullbox.js" type="text/javascript"></script>
    <script src="../Themes/Scripts/FunctionJS.js" type="text/javascript"></script>
    <script src="../Themes/Scripts/DatePicker/WdatePicker.js" type="text/javascript"></script>

        <link href="../Themes/Scripts/jqueryeasyui/themes/bootstrap/easyui.css" rel="stylesheet" />
    <link href="../Themes/Scripts/jqueryeasyui/themes/icon.css" rel="stylesheet" />
    <link href="../Themes/Scripts/jqueryeasyui/themes/color.css" rel="stylesheet" />

    <script type="text/javascript" src="../Themes/Scripts/jqueryeasyui/jquery.easyui.min.js"></script>
    <script src="../Themes/Scripts/jqueryeasyui/plugins/jquery.edatagrid.js" type="text/javascript"></script>

    <script src="../Scripts/uploadify/jquery.uploadify.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        function search() {
            document.getElementById('<%= this.btnSearch.ClientID %>').click();
          }
          //添加
          function add() {
              document.getElementById('<%= this.btnAdd.ClientID %>').click();
        }

        //删除
        function Delete() {
            showConfirmMsg("此操作不可恢复，您确定要删除吗？", function (r) {
                if (r) {
                    document.getElementById('<%= this.btnDel.ClientID %>').click();
                }
            });
        }
        //保存
        function SaveForm() {
            document.getElementById('<%= this.btnSave.ClientID %>').click();
        }



        $(function () {

            InitControl();
        })

        function InitControl() {
        
            // FixedTableHeader("#tableDetails", $(window).height() - 300);
            $(".div-body").height(200);
           // FixedTableHeader("#table1",10);
            $(".div-body").PullBox({ dv: $(".div-body"), obj: $("#table1").find("tr") });
        
           

        }

        //加载文件列表
        function LoadFileList($ExportInfo_OID) {
            var _fileClass = "设备预试数据文件";
            var editRow = undefined;
            var $datagrid = $("#fileList");

            var $widthNoise = $("#table1").width();

            $datagrid.datagrid({
                height: 300,
                width: $widthNoise - 10,
                fitColumns: true,
                collapsible: true,
                singleSelect: true,
                url: '../MagneticPage/UploadHandler.ashx',
                queryParams: { action: 'Get_FilesList', Magnetic_CheckPointTemplatesB_OID: $ExportInfo_OID, fileClass: _fileClass },
                idField: 'OID',
                columns: [[
                 { field: 'OID', title: 'ID', hidden: true },
                   {
                       field: 'FileName', width: 150, title: "文件名", align: 'center'
                   },

                  {
                      field: 'FileSize', width: 100, title: "文件大小", align: 'center'
                  },
                    {
                        field: 'CreateDate', width: 100, title: "上传时间", align: 'center'
                    }, {
                        field: 'RegistrantName', width: 100, title: "上传者", align: 'center'
                    }


                ]],
                toolbar: [{
                    text: ' <span id="uploadify" />', iconCls: 'folder_up', handler: function () {

                    }

                }, {
                    text: '下载', iconCls: 'package_down', handler: function () {

                        var editIndex = editRow;

                        $datagrid.datagrid('endEdit', editRow);

                        $datagrid.datagrid('selectRow', editIndex)
                        var rowstr = $datagrid.datagrid('getSelected', editIndex)
                        window.location = '../MagneticPage/UploadHandler.ashx?action=DownLoad&url=' + rowstr.FilePath;


                    }
                }, {
                    text: '删除', iconCls: 'icon-remove', handler: function () {
                        var row = $datagrid.datagrid('getSelected');
                        var index = $datagrid.datagrid('getRowIndex', row);


                        $.post('../MagneticPage/UploadHandler.ashx?action=Del_Files', { oid: row.OID }, function (data) {
                            if (data == "删除成功") {
                                $datagrid.datagrid('deleteRow', index);
                            }

                            showFaceMsg(data);

                        });

                    }
                }],
                onAfterEdit: function (rowIndex, rowData, changes) {
                    editRow = undefined;
                },
                onDblClickRow: function (rowIndex, rowData) {
                    if (editRow != undefined) {
                        $datagrid.datagrid('endEdit', editRow);
                    }

                    if (editRow == undefined) {
                        $datagrid.datagrid('beginEdit', rowIndex);
                        editRow = rowIndex;
                    }
                },
                onClickRow: function (rowIndex, rowData) {
                    if (editRow != undefined) {
                        $datagrid.datagrid('endEdit', editRow);

                    }
                    editRow = rowIndex;
                }

            });


            $('#uploadify').uploadify({
                'formData': {
                    'Magnetic_CheckPointTemplatesB_OID': $ExportInfo_OID,
                    'action': 'upload', fileClass: _fileClass
                },
                'swf': '../Scripts/uploadify/uploadify.swf',
                'uploader': '../MagneticPage/UploadHandler.ashx',
                'buttonText': '上传',
                'width': 40,
                'queueID': true,
                onUploadComplete: function myfunction(file) {
                    LoadFileList($ExportInfo_OID)
                    alert('上传成功');
                }
            });
        }
    
        var objDev = {};
        function ShowSelectDialog() {
            var $ddlsConvertStationCode = $("#ddlsConvertStationCode").val();
            var url = "/GanPing/SelectSBDialog.aspx?ConvertStationCode=" + $ddlsConvertStationCode;
            // returnValue = $ddlsConvertStationCode;
            top.openDialog(url, 'SelectSBDialog', '详细信息 - 查看', 1000, 500, 50, 50, function myfunction() {
                // alert(devcode);
                $("#txtsDevCode").val(objDev.devcode);
                $("#txtsDevName").val(objDev.devname);

            });


        }
    </script>

      <style type="text/css">
        .l-btn > span {
            background: none;
        }

        .uploadify-button {
            line-height: 20px !important;
        }
          object {
            left: 0px;
            width: 100%;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:HiddenField ID="HiddenField1" runat="server" />
                <asp:HiddenField ID="HiddenField2" runat="server" />
                <table width="100%" id="masterContent">
                    <colgroup>
                        <col width="10%" />
                        <col width="11%" />
                        <col width="6%" />
                        <col width="11%" />
                        <col width="7%" />
                        <col width="11%" />
                        <col width="7%" />
                        <col width="11%" />
                    </colgroup>
                    <tr>
                        <td class="inner_cell_right">设备编码：
                        </td>
                        <td>

                               <cc1:ExTextBox ID="txtsDevCode" runat="server" Width="70%"
                                checkexpession="NotNull" datacol="yes" err="此项" FieldName="DevCode" BindName="EquipmentPilotTestDataObj" /><input id="selectDevcode" value="选择"  type="button" onclick="ShowSelectDialog()" />
                        </td>
                        <td class="inner_cell_right">设备名称：
                        </td>
                        <td>
                            <cc1:ExTextBox ID="txtsDevName" runat="server" Width="100%"
                                checkexpession="NotNull" datacol="yes" err="此项" FieldName="DevName" BindName="EquipmentPilotTestDataObj" />
                        </td>
                        <td class="inner_cell_right">预试日期：
                        </td>
                        <td>

                            <cc1:ExTextBox ID="txtsPreTestDate" runat="server" checkexpession="NotNull" datacol="yes" StrFormat="yyyy-MM-dd"
                                err="此项" Width="100%" onfocus="WdatePicker()" FieldName="PreTestDate" BindName="EquipmentPilotTestDataObj" />
                        </td>
                        <td class="inner_cell_right">登记人：
                        </td>
                        <td>
                            <cc1:ExTextBox ID="txtCREATED_BY" runat="server" Width="100%" Enabled="false"
                                checkexpession="NotNull" datacol="yes" err="此项" FieldName="CREATED_BY" BindName="EquipmentPilotTestDataObj" />
                        </td>

                    </tr>
                    <tr>
                        <td class="inner_cell_right">品质检测报告名称：
                        </td>
                        <td>
                            <cc1:ExTextBox ID="txtsReportName" runat="server" checkexpession="NotNull" datacol="yes"
                                err="此项" Width="100%" FieldName="ReportName" BindName="EquipmentPilotTestDataObj" />
                        </td>
                        <td class="inner_cell_right">预试情况：
                        </td>
                        <td>
                            <cc1:ExTextBox ID="txtsPreTestStatus" runat="server" Width="100%"
                                checkexpession="NotNull" datacol="yes" err="此项" FieldName="PreTestStatus" BindName="EquipmentPilotTestDataObj" />
                        </td>
                        <td class="inner_cell_right">供电局名称：
                        </td>
                        <td>
                            <cc1:ExDropDownList ID="ddlsPowerSupplyCode" runat="server" Width="100%" AutoPostBack="true" BindName="EquipmentPilotTestDataObj"
                                OnSelectedIndexChanged="ddlPowerSupplyName_SelectedIndexChanged" CssClass="select" FieldName="PowerSupplyCode">
                            </cc1:ExDropDownList>
                        </td>
                        <td class="inner_cell_right">变电站名称：
                        </td>
                        <td>
                            <cc1:ExDropDownList ID="ddlsConvertStationCode" runat="server" Width="100% " CssClass="select" FieldName="ConvertStationCode" BindName="EquipmentPilotTestDataObj"
                                checkexpession="NotNull" datacol="yes" err="此项" >
                            </cc1:ExDropDownList>
                        </td>

                    </tr>
                    <tr>
                        <td class="inner_cell_right">登记日期：
                        </td>
                        <td>
                            <cc1:ExTextBox ID="txttCREATED_DATE" runat="server" Enabled="false" checkexpession="NotNull" datacol="yes" StrFormat="yyyy-MM-dd"
                                err="此项" Width="100%" FieldName="CREATED_DATE" BindName="EquipmentPilotTestDataObj" />
                        </td>



                    </tr>
                </table>
                <div style="text-align: right;">
                    <uc2:LoadButton ID="LoadButton1" runat="server" />
                </div>
                <div style="display: none;">
                    <asp:Button ID="btnSave" runat="server" OnClick="btnSave_Click" Text="btnSave_Click"
                        OnClientClick="return CheckDataValid('#masterContent');" />
                    <asp:Button ID="btnAdd" runat="server" OnClick="btnAdd_Click" Text="btnAdd_Click" />
                    <asp:Button ID="btnDel" runat="server" OnClick="btnDel_Click" Text="btnDel_Click" />
                    <asp:Button ID="btnSearch" runat="server" OnClick="btnSearch_Click" Text="btnSearch_Click" />
                </div>


                <div class="div-body">
                    <table id="table1" class="grid" singleselect="true">
                        <colgroup>
                               <col width="5%" />
                            <col width="7%" />
                            <col width="20%" />
                            <col width="10%" />
                            <col width="20%" />
                            <col width="20%" />
                            <col width="10%" />
                            <col width="10%" />
                            <col width="10%" />
                            <col width="8%" />
                          <col width="2%" />
                        </colgroup>
                        <thead>
                            <tr>
                                <td style="text-align: center;">序号
                                </td>
                                <td style="text-align: center;">设备编码
                                </td>
                                <td style="text-align: center;">设备名称
                                </td>
                                <td style="text-align: center;">预试日期
                                </td>
                                <td style="text-align: center;">预试情况
                                </td>
                                <td style="text-align: center;">品质检测报告名称
                                </td>
                                <td style="text-align: center;">供电局名称
                                </td>
                                <td style="text-align: center;">变电站名称
                                </td>
                                <td style="text-align: center;">登记人
                                </td>
                                <td style="text-align: center;">登记日期
                                </td>
                            </tr>
                        </thead>
                        <tbody>
                            <asp:Repeater ID="rp_Item" runat="server" OnItemCommand="rp_Item_ItemCommand">
                                <ItemTemplate>
                                    <tr>
                                        <td style="text-align: center;">
                                            <%# Container.ItemIndex + 1%>
                                        </td>
                                        <td style="text-align: left;">
                                            <asp:LinkButton ID="lbtnProjCode" runat="server" OnClientClick="selectrow(this)"
                                                CommandArgument='<%#Eval("OID") %>' CommandName="del">  <%# Eval("DevCode") %></asp:LinkButton>
                                        </td>
                                        <td style="text-align: center;">
                                            <%# Eval("DevName")%>
                                        </td>
                                        <td style="text-align: center;">
                                            <%# Eval("PreTestDate","{0:yyyy-MM-dd}")%>
                                        </td>
                                        <td style="text-align: center;">
                                            <%# Eval("PreTestStatus")%>
                                        </td>
                                        <td style="text-align: center;">
                                            <%# Eval("ReportName")%>
                                        </td>
                                        <td style="text-align: center;">
                                            <%# Eval("PowerSupplyName")%>
                                        </td>
                                        <td style="text-align: center;">
                                            <%# Eval("ConvertStationName")%>
                                        </td>
                                        <td style="text-align: center;">
                                            <%# Eval("CREATED_BY")%>
                                        </td>
                                        <td style="text-align: center;">
                                            <%# Eval("CREATED_DATE","{0:yyyy-MM-dd}")%>
                                        </td>


                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                        </tbody>
                    </table>
                </div>
                <uc1:PageControl ID="PageControl1" runat="server" />


                 <table id="fileList"></table>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
            </Triggers>
        </asp:UpdatePanel>
    </form>
</body>
</html>

﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CylinderScanInOut.aspx.cs"
    Inherits="RM.Web.GanPing.CylinderScanInOut" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="/Themes/Styles/Site.css" rel="stylesheet" type="text/css" />
    <script src="/Themes/Scripts/Validator/JValidator.js" type="text/javascript"></script>
    <script src="/Themes/Scripts/jquery-1.8.2.min.js" type="text/javascript"></script>

    <script src="/Themes/Scripts/FunctionJS.js" type="text/javascript"></script>

    <style type="text/css">
        .jzlayer {
            width: 100%;
            background-color: Blue;
        }

            .jzlayer > tbody > tr {
                height: 100px;
            }

                .jzlayer > tbody > tr > td {
                    width: 100px;
                    height: 100px;
                }

        .circleColor0 {
            width: 50px;
            height: 50px;
            background-color: #CB7676 !important;
            -moz-border-radius: 25px;
            -webkit-border-radius: 25px;
            border-radius: 25px;
            text-align: center;
        }

        .circleColor1 {
            width: 50px;
            height: 50px;
            background-color: #0e4ad8 !important;
            -moz-border-radius: 25px;
            -webkit-border-radius: 25px;
            border-radius: 25px;
            text-align: center;
        }

        .circleColor2 {
            width: 50px;
            height: 50px;
            background-color: #41ed83 !important;
            -moz-border-radius: 25px;
            -webkit-border-radius: 25px;
            border-radius: 25px;
            text-align: center;
        }

        .circleColor3 {
            width: 50px;
            height: 50px;
            background-color: yellow !important;
            -moz-border-radius: 25px;
            -webkit-border-radius: 25px;
            border-radius: 25px;
            text-align: center;
        }

        .circleColor9 {
            width: 50px;
            height: 50px;
            background-color: red !important;
            -moz-border-radius: 25px;
            -webkit-border-radius: 25px;
            border-radius: 25px;
            text-align: center;
        }

        .circleColor4 {
            width: 50px;
            height: 50px;
            background-color: seagreen !important;
            -moz-border-radius: 25px;
            -webkit-border-radius: 25px;
            border-radius: 25px;
            text-align: center;
        }

        .circleColor5 {
            width: 50px;
            height: 50px;
            background-color: #1dd6f7 !important;
            -moz-border-radius: 25px;
            -webkit-border-radius: 25px;
            border-radius: 25px;
            text-align: center;
        }

        .circleColor6 {
            width: 50px;
            height: 50px;
            background-color: #808080 !important;
            -moz-border-radius: 25px;
            -webkit-border-radius: 25px;
            border-radius: 25px;
            text-align: center;
        }


        .circleyellow {
            width: 100px;
            height: 100px;
            background: yellow;
            -moz-border-radius: 50px;
            -webkit-border-radius: 50px;
            border-radius: 50px;
            text-align: center;
        }

            .circleyellow:after {
                display: inline-block;
                width: 0;
                height: 100%;
                vertical-align: middle;
                content: "";
            }

        .circlewhite {
            width: 100px;
            height: 100px;
            background: white !important;
            -moz-border-radius: 50px;
            -webkit-border-radius: 50px;
            border-radius: 50px;
        }

        .circleMin {
            width: 50px;
            height: 50px;
            -moz-border-radius: 50px;
            -webkit-border-radius: 50px;
            border-radius: 50px;
        }

        .circleList {
            margin-left: 100px;
        }

            .circleList > div {
                float: left;
                margin-left: 50px;
            }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <table width="100%">
            <tr>
                <td width="25%">
                    <table>
                        <tr>
                            <td colspan="2">钢瓶入库扫描
                            </td>
                        </tr>
                        <tr>
                            <td>架子编码:
                            </td>
                            <td>
                                <input type="text" id="code" onchange="getLayerallCylinder(this)" />

                            </td>
                        </tr>
                        <tr>
                            <td>钢瓶编码1:
                            </td>
                            <td>
                                <input type="text" id="Text1" name="1" onchange="showposition(this)" class="cylinder" />
                                <input type="button" value="入库" onclick="doshowposition(this)" />
                            </td>
                        </tr>
                        <tr>
                            <td>钢瓶编码2:
                            </td>
                            <td>
                                <input type="text" id="Text2" name="2" onchange="showposition(this)" class="cylinder" />
                                <input type="button" value="入库" onclick="doshowposition(this)" />
                            </td>
                        </tr>
                        <tr>
                            <td>钢瓶编码3:
                            </td>
                            <td>
                                <input type="text" id="Text3" name="3" onchange="showposition(this)" class="cylinder" />
                                <input type="button" value="入库" onclick="doshowposition(this)" />
                            </td>
                        </tr>
                        <tr>
                            <td>钢瓶编码4:
                            </td>
                            <td>
                                <input type="text" id="Text4" name="4" onchange="showposition(this)" class="cylinder" />
                                <input type="button" value="入库" onclick="doshowposition(this)" />
                            </td>
                        </tr>
                        <tr>
                            <td>钢瓶编码5:
                            </td>
                            <td>
                                <input type="text" id="Text5" name="5" onchange="showposition(this)" class="cylinder" />
                                <input type="button" value="入库" onclick="doshowposition(this)" />
                            </td>
                        </tr>
                        <tr>
                            <td>钢瓶编码6:
                            </td>
                            <td>
                                <input type="text" id="Text6" name="6" onchange="showposition(this)" class="cylinder" />
                                <input type="button" value="入库" onclick="doshowposition(this)" />
                            </td>
                        </tr>
                    </table>
                </td>
                <td width="50%">
                    <table class="jzlayer">
                        <tr>
                            <td>
                                <div class="circlewhite" id="c1">
                                </div>
                            </td>
                            <td>
                                <div class="circlewhite" id="c2">
                                </div>
                            </td>
                            <td>
                                <div class="circlewhite" id="c3">
                                </div>
                            </td>
                            <td>
                                <div class="circlewhite" id="c4">
                                </div>
                            </td>
                            <td>
                                <div class="circlewhite" id="c5">
                                </div>
                            </td>
                            <td>
                                <div class="circlewhite" id="c6">
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
                <td width="25%" style="padding-bottom: 160px;">
                    <table>
                        <tr>
                            <td colspan="2">钢瓶出库扫描
                            </td>
                        </tr>
                        <tr>
                            <td>钢瓶编码:
                            </td>
                            <td>
                                <input type="text" id="Text8" onchange="hiddenposition(this)" />
                                <input type="button" value="出库" onclick="dohiddenposition(this)" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>

        </table>
        <div style="width: 100%; height: 60px" class="circleList">
            <div>
                <div class="circleMin" style="background-color: #CB7676">
                </div>
                <span>未抽空清洗

                </span>
            </div>
            <div>
                <div class="circleMin" style="background-color: #0e4ad8">
                </div>
                <span>已抽空清洗
                </span>
            </div>
            <div>
                <div class="circleMin" style="background-color: #41ed83">
                </div>
                <span>充装回收气
                </span>
            </div>
            <div>
                <div class="circleMin" style="background-color: yellow">
                </div>
                <span>已充待检气
                </span>
            </div>
            <div>
                <div class="circleMin" style="background-color: red">
                </div>
                <span>已充待检气<br />
                    (不合格)</span>
            </div>
            <div>
                <div class="circleMin" style="background-color: seagreen">
                </div>
                <span>已充合格气</span>
            </div>
            <div>
                <div class="circleMin" style="background-color: #1dd6f7">
                </div>
                <span>待送检</span>
            </div>
            <div>
                <div class="circleMin" style="background-color: #808080">
                </div>

                <span>已报废</span>
            </div>


        </div>

    </form>

    <script type="text/javascript">

        var $username = '<%= UserName%>';
        function showposition(o) {

            $.get('../Ajax/CylCarCodeRelationsHandler.ashx', { action: "AddCylinderCode", CarcaseCode: $("#code").val(), CylinderCode: $(o).val(), username: $username }, function myfunction(results) {
                if (results == 2) {
                    alert("存中已经存在存钢瓶");
                }
                else if (results == 3) {
                    alert("此钢瓶不存在");
                }
                else {
                    onlyshowposition(o, results);
                    var $name = $(o).attr("name");

                    $("#Text" + (parseInt($name) + 1).toString()).focus();
                }

            })
        }
        function doshowposition(o) {
            showposition($(o).prev());
        }
        function dohiddenposition(o) {
            hiddenposition($(o).prev());
        }
        function onlyshowposition(o, rl) {
            var $firstNode = $("#c" + $(o).attr("name"));
            if ($(o).val() == "") {
                if ($firstNode.hasClass("circleyellow")) {
                    $firstNode.removeClass("circleyellow");
                    $firstNode.addClass("circlewhite");
                }

                $firstNode.text("");
            }
            else {
                if ($firstNode.hasClass("circlewhite")) {
                    $firstNode.removeClass("circlewhite");
                    $firstNode.addClass("circleyellow");

                }
                var gprl = $(o).data("GPRL");
                if (gprl) {
                    //  $firstNode.text(gprl.CylinderCapacity + "KG");
                    setcolorandcapacity($firstNode, gprl);
                }
                else if (rl && rl != "") {

                    // $firstNode.text(rl.rCylinderCapacity + "KG");
                    setcolorandcapacity($firstNode, rl);
                }



            }


        }

        function setcolorandcapacity(targtElment, o) {
            targtElment.text(o.CylinderCapacity + "KG");
            //0	1	NULL	未抽空清洗
            //1	1	NULL	已抽空清洗
            //2	1	NULL	充装回收气
            //3	1	NULL	已充待检气
            //4	1	NULL	已充合格气
            //5	1	NULL	待送检
            //6	1	NULL	已报废
            var classByStatus = "circleyellow";

            if (o.Status == "3")//设置颜色
            {

                var isPass = o.IsPass;
                if (isPass == "0")//已充待检气（而且不合格）
                {
                    classByStatus = "circleColor" + 9;
                }
                else {

                    classByStatus = "circleColor" + o.Status;
                }
            }
            else {
                classByStatus = "circleColor" + o.Status;
            }
            targtElment.addClass(classByStatus);
        }

        function hiddenposition(o) {




            $.get('../Ajax/CylCarCodeRelationsHandler.ashx', { action: "DeleteCylinderCode", CylinderCode: $(o).val() }, function myfunction(results) {
                if (results == 1) {
                    $(".cylinder").each(function myfunction() {
                        if ($(this).val() == $(o).val()) {
                            $(this).val("");
                            $(o).val("");
                            onlyshowposition($(this));
                        }

                    });

                    showTipsMsg($(o).val()+"出库成功", 1000, 4);
                }
                else {
                    showTipsMsg($(o).val()+"不在架子上", 1000, 4);
                }
                $("#Text8").val("");
                $("#Text8").focus();
            })
        }

        function getLayerallCylinder(o) {

            $.get('../Ajax/CylCarCodeRelationsHandler.ashx', { action: "GetCylinderCode", CarcaseCode: $(o).val() }, function myfunction(results) {

                $(".cylinder").each(function myfunction() {
                    var $cycode = results.pop();

                    if ($cycode) {
                        $(this).val($cycode.CylinderCode);
                        $(this).data("GPRL", $cycode);

                        onlyshowposition(this);
                    }
                    else {
                        $(this).val("");
                        onlyshowposition(this);
                    }

                    $("#Text1").focus();
                });
            })
        }
        document.onkeydown = function myfunction() {
            if (event.keyCode == 13) {
                document.body.focus();
                return true;
            }
        }

    </script>
</body>
</html>

﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ShowDetailsInspectionBox.aspx.cs"
    Inherits="RM.Web.GanPing.ShowDetailsInspectionBox" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="/Themes/Styles/Site.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table width="100%">
            <colgroup>
                <col width="11%" />
                <col width="5%" />
                <col width="11%" />
                <col width="5%" />
                <col width="11%" />
                <col width="5%" />
                <col width="11%" />
                <col width="5%" />
            </colgroup>
            <tr>
                <td class="inner_cell_right">
                    六氟化硫(SF6)的质量分数指标（/%）：
                </td>
                <td>
                    <asp:Label ID="lblSF6Index" runat="server" te></asp:Label>
                </td>
                <td class="inner_cell_right">
                    空气的质量分数指标（/%）：
                </td>
                <td>
                    <asp:Label ID="lblAirQualityScoreIndex" runat="server"></asp:Label>
                </td>
                <td class="inner_cell_right">
                    四氟化碳(CF4) 的质量分数指标（/%）：
                </td>
                <td>
                    <asp:Label ID="lblCF4Index" runat="server"></asp:Label>
                </td>
                <td class="inner_cell_right">
                    水的质量分数指标（/%）：
                </td>
                <td>
                    <asp:Label ID="lblWaterQualityScoreIndex" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="inner_cell_right">
                    水的露点指标（/℃）：
                </td>
                <td>
                    <asp:Label ID="lblWaterDewIndex" runat="server"></asp:Label>
                </td>
                <td class="inner_cell_right">
                    酸度 (以HF计 ) 的质量分数指标（/%）：
                </td>
                <td>
                    <asp:Label ID="lblHFIndex" runat="server"></asp:Label>
                </td>
                <td class="inner_cell_right">
                    可水解氟化物(以HF计)的质量分数指标（/%）：
                </td>
                <td>
                    <asp:Label ID="lblKSJHFIndex" runat="server"></asp:Label>
                </td>
                <td class="inner_cell_right">
                    矿物油的质量分数指标（/%）：
                </td>
                <td>
                    <asp:Label ID="lblKWYSorceIndex" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="inner_cell_right">
                    签发人：
                </td>
                <td>
                    <asp:Label ID="lblSignerOID" runat="server"></asp:Label>
                </td>
                <td class="inner_cell_right">
                    签发日期：
                </td>
                <td>
                    <asp:Label ID="lblSignerDate" runat="server"></asp:Label>
                </td>
                <td class="inner_cell_right">
                    批准人ID：
                </td>
                <td>
                    <asp:Label ID="lblApproverOID" runat="server"></asp:Label>
                </td>
                <td class="inner_cell_right">
                    批准日期：
                </td>
                <td>
                    <asp:Label ID="lblApproverDate" runat="server"></asp:Label>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>

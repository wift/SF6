﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CylinderInspectTablePage.aspx.cs"
    Inherits="RM.Web.GanPing.CylinderInspectTablePage"  EnableEventValidation ="false"  ClientIDMode="AutoID"%>

<%@ Register Src="~/UserControl/PageControl.ascx" TagName="PageControl" TagPrefix="uc1" %>
<%@ Register Src="~/UserControl/LoadButton.ascx" TagName="LoadButton" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>钢瓶送检管理</title>
    <link href="/Themes/Styles/Site.css" rel="stylesheet" type="text/css" />
    <script src="/Themes/Scripts/Validator/JValidator.js" type="text/javascript"></script>
    <script src="/Themes/Scripts/jquery-1.8.2.min.js" type="text/javascript"></script>
    <script src="/Themes/Scripts/jquery.pullbox.js" type="text/javascript"></script>
    <script src="/Themes/Scripts/FunctionJS.js" type="text/javascript"></script>
    <script src="/Themes/Scripts/DatePicker/WdatePicker.js" type="text/javascript"></script>
    <script type="text/javascript">
        //添加
        function add() {
            document.getElementById('<%= this.btnAdd.ClientID %>').click();
        }
        //查询
        function search() {

            document.getElementById('<%= this.btnSearch.ClientID %>').click();
        }
        //删除
        function Delete() {
            showConfirmMsg("此操作不可恢复，您确定要删除吗？", function (r) {
                if (r) {
                    document.getElementById('<%= this.btnDel.ClientID %>').click();
                }
            });
        }
        //保存
        function SaveForm() {
            document.getElementById('<%= this.btnSave.ClientID %>').click();
        }
        //提交
        function Submit() {
            var key = CheckboxValue1();
            document.getElementById('<%= this.HiddenField1.ClientID %>').value = key.toString();
            document.getElementById('<%= this.btnSubmit.ClientID %>').click();
        }
        $(function () {
            InitControl();
        })
        function InitControl() {
            $(".div-body").PullBox({ dv: $(".div-body"), obj: $("#table1").find("tr") });
            divresize(140);

        }

        //批量添加
        function BatchAdd() {
            var url = "/GanPing/BatchCylinderInspectTable.aspx";
            top.openDialog(url, 'BatchCylinderInspectTable', '批量添加', 1000, 400, 50, 50);
        }

        function PrintQD() {

            var url = "/GanPing/ShowCylinderInspectDetails.aspx";
            top.openDialog(url, 'ShowCylinderInspectDetails', '送检清单', 1000, 400, 50, 50);
        }
        //送检保存
        function SJSave() {
            document.getElementById('<%= this.btnSJ.ClientID %>').click();
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:HiddenField ID="HiddenField1" runat="server" />
            <table width="100%">
                <colgroup>
                    <col width="11%" />
                    <col width="11%" />
                    <col width="11%" />
                    <col width="11%" />
                    <col width="11%" />
                    <col width="11%" />
                    <col width="11%" />
                    <col width="11%" />
                </colgroup>
                <tr>
                    <td class="inner_cell_right">
                        钢瓶编码：
                    </td>
                    <td>
                        <asp:TextBox ID="txtCylinderCode" runat="server" checkexpession="NotNull" datacol="yes"
                            err="此项" />
                    </td>
                    <td class="inner_cell_right">
                        原有效日期：
                    </td>
                    <td>
                        <asp:TextBox ID="txtOriginalEffectiveDate" runat="server" onfocus="WdatePicker()" />
                    </td>
                    <td class="inner_cell_right">
                        送检人：
                    </td>
                    <td>
                        <asp:TextBox ID="txtSendCheckOID" runat="server" Enabled="false" />
                    </td>
                    <td class="inner_cell_right">
                        登记人ID：
                    </td>
                    <td>
                        <asp:TextBox ID="txtRegistantsOID" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="inner_cell_right">
                        是否合格：
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlIsPass" runat="server" Width="100%" CssClass="select">
                        </asp:DropDownList>
                    </td>
                    <td class="inner_cell_right">
                        新有效日期：
                    </td>
                    <td>
                        <asp:TextBox ID="txtNewEffectiveDate" runat="server" onfocus="WdatePicker()" />
                    </td>
                    <td class="inner_cell_right">
                        送检日期：
                    </td>
                    <td>
                        <asp:TextBox ID="txtSendCheckDate" runat="server" onfocus="WdatePicker()" Enabled="false" />
                    </td>
                    <td class="inner_cell_right">
                        登记日期：
                    </td>
                    <td>
                        <asp:TextBox ID="txtRegistantsDate" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="inner_cell_right">
                        备注说明：
                    </td>
                    <td>
                        <asp:TextBox ID="txtRemarks" runat="server" />
                    </td>
                    <td class="inner_cell_right">
                        状态：
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlCFStatus" runat="server" Width="100%" Enabled="false">
                        </asp:DropDownList>
                    </td>
                    <td class="inner_cell_right">
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                    </td>
                </tr>
            </table>
            <div style="text-align: left;">
                <uc2:LoadButton ID="LoadButton1" runat="server" />
            </div>
            <div style="text-align: right; display: none;">
                <asp:Button ID="btnSubmit" runat="server" OnClick="btnSubmit_Click" Text="btnSubmit_Click" />
                <asp:Button ID="btnSearch" runat="server" OnClick="btnSearch_Click" Text="btnSearch_Click" />
                <asp:Button ID="btnSave" runat="server" OnClick="btnSave_Click" Text="btnSave_Click"
                    OnClientClick="return CheckDataValid('#form1');" />
                <asp:Button ID="btnAdd" runat="server" OnClick="btnAdd_Click" Text="btnAdd_Click" />
                <asp:Button ID="btnDel" runat="server" OnClick="btnDel_Click" Text="btnDel_Click" />
                <asp:Button ID="btnSJ" runat="server" Text="btnDel_Click" OnClick="btnSJ_Click" />
            </div>
            <div class="div-body">
                <table id="table1" class="grid">
                    <colgroup>
                        <col width="5%" />
                        <col width="12%" />
                        <col width="12%" />
                        <col width="12%" />
                        <col width="12%" />
                        <col width="7%" />
                        <col width="12%" />
                        <col width="12%" />
                        <col width="12%" />
                    </colgroup>
                    <thead>
                        <tr>
                            <td style="width: 20px; text-align: left;">
                                <label id="checkAllOff" onclick="CheckAllLine1()" title="全选">
                                    &nbsp;</label>
                            </td>
                            <td style="text-align: center;">
                                钢瓶编码
                            </td>
                            <td style="text-align: center;">
                                原有效日期
                            </td>
                            <td style="text-align: center;">
                                新有效日期
                            </td>
                            <td style="text-align: center;">
                                是否合格
                            </td>
                            <td style="text-align: center;">
                                送检人
                            </td>
                            <td style="text-align: center;">
                                送检日期
                            </td>
                            <td style="text-align: center;">
                                状态
                            </td>
                            <td style="text-align: center;">
                                登记日期
                            </td>
                        </tr>
                    </thead>
                    <tbody>
                        <asp:Repeater ID="rp_Item" runat="server" OnItemDataBound="rp_ItemDataBound" OnItemCommand="rp_Item_ItemCommand">
                            <ItemTemplate>
                                <tr>
                                    <td style="text-align: left;">
                                        <asp:CheckBox ID="checkbox" runat="server" ToolTip='<%# Eval("OID")%>' />
                                    </td>
                                    <td style="text-align: center;">
                                        <asp:LinkButton ID="LBtnDel" runat="server" OnClientClick="selectrow(this)" CommandArgument='<%#Eval("OID") %>' CommandName="del">
                              
                                  <%# Eval("CylinderCode")%>
                                        </asp:LinkButton>
                                    </td>
                                    <td style="text-align: center;">
                                        <%#Eval("OriginalEffectiveDate", "{0:yyyy-MM-dd}")%>
                                    </td>
                                    <td style="text-align: center;">
                                        <%#Eval("NewEffectiveDate", "{0:yyyy-MM-dd}")%>
                                    </td>
                                    <td style="text-align: center;">
                                        <asp:Label ID="lblIsPass" runat="server" Text='<%#Eval("IsPass")%>'></asp:Label>
                                    </td>
                                    <td style="text-align: center;">
                                        <asp:Label ID="lblSendCheckoid" runat="server" Text='<%#Eval("SendCheckoid")%>'></asp:Label>
                                    </td>
                                    <td style="text-align: center;">
                                        <%#Eval("SendCheckDate","{0:yyyy-MM-dd}")%>
                                    </td>
                                    <td style="text-align: center;">
                                        <asp:Label ID="lblStatus" runat="server" Text='<%#Eval("Status")%>'></asp:Label>
                                    </td>
                                    <td style="text-align: center;">
                                        <%#Eval("RegistantsDate", "{0:yyyy-MM-dd}")%>
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                            </FooterTemplate>
                        </asp:Repeater>
                    </tbody>
                </table>
            </div>
            <uc1:PageControl ID="PageControl1" runat="server" />
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnAdd" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>
    </form>
</body>
</html>

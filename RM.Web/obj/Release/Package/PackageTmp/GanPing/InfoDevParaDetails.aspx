﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="InfoDevParaDetails.aspx.cs" Inherits="RM.Web.GanPing.InfoDevParaDetails" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>明细</title>
    <link href="../Themes/Styles/Site.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        table
        {
            border-collapse: collapse;
            border-width: thin;
            border-style: solid;
        }

        td
        {
            border-width: thin;
            border-style: solid;
        }

        .inner_cell_right
        {
            font-weight: bold !important;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table width="100%">
                <colgroup>
                    <col width="12%" />
                    <col width="12%" />
                    <col width="12%" />
                    <col width="12%" />
                    <col width="12%" />
                    <col width="12%" />
                    <col width="12%" />
                    <col width="12%" />
                </colgroup>


                <tr>
                    <td class="inner_cell_right">设备ID：
                    </td>
                    <td>
                        <%= Model.OutDevID%>

                    </td>
                    <td class="inner_cell_right">设备状态：
                    </td>
                    <td>

                        <%= Model.OutDevStatus%>
                    </td>
                    <td class="inner_cell_right">设备身份证编码：
                    </td>
                    <td>
                        <%= Model.OutDevUniqueCode%>

                    </td>

                    <td class="inner_cell_right">生产厂家现用名：
                    </td>
                    <td>
                        <%= Model.OutManufacturerCurName%>

                    </td>
                </tr>
                <tr>
                    <td class="inner_cell_right">设备数量或线路长度(米)：
                    </td>
                    <td><%= Model.OutDevCoutOrLength%></td>
                    <td class="inner_cell_right">电压等级：
                    </td>
                    <td>
                        <%= Model.OutVoltageLevel%>

                    </td>
                    <td class="inner_cell_right">类别名称：
                    </td>
                    <td>
                        <%= Model.OutDevClass%>

                    </td>
                    <td class="inner_cell_right">全路径：
                    </td>
                    <td>

                        <%= Model.OutFullPath%>

                    </td>
                </tr>
                <tr>
                    <td class="inner_cell_right">进入当前状态时间：
                    </td>
                    <td>

                        <%= Model.OutGoinCurStatus%>
                    </td>
                    <td class="inner_cell_right">原厂保修期(月)：
                    </td>
                    <td><%= Model.OutOriFactoryWarranty%></td>
                    <td class="inner_cell_right">计量单位：
                    </td>
                    <td><%= Model.OutUnits%></td>

                    <td class="inner_cell_right">供应商：
                    </td>
                    <td>
                        <%= Model.OutProvider%>


                    </td>

                </tr>

                <tr>
                    <td class="inner_cell_right">设备运维部门：
                    </td>
                    <td><%= Model.OutDevCPISDept%></td>
                    <td class="inner_cell_right">保管班组：
                    </td>
                    <td><%= Model.OutStorageTeam%></td>
                    <td class="inner_cell_right">是否资产级设备：
                    </td>
                    <td><%= Model.OutIsPropertyDev%></td>

                    <td class="inner_cell_right">间隔名称：
                    </td>
                    <td><%= Model.OutDevClass%></td>



                </tr>

                <tr>
                    <td class="inner_cell_right">资产级编码：
                    </td>
                    <td><%= Model.OutPropertyCode%></td>
                    <td class="inner_cell_right"></td>
                    <td></td>
                    <td class="inner_cell_right"></td>
                    <td></td>
                    <td class="inner_cell_right"></td>
                    <td></td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>

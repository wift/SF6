﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ApplNeedGasRegPage.aspx.cs"
    Inherits="RM.Web.GanPing.ApplNeedGasRegPage" EnableEventValidation="false" ClientIDMode="AutoID" %>

<%@ Register Src="~/UserControl/PageControl.ascx" TagName="PageControl" TagPrefix="uc1" %>
<%@ Register Src="~/UserControl/LoadButton.ascx" TagName="LoadButton" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>需气登记管理 检修需气申请 </title>
    <link href="/Themes/Styles/Site.css" rel="stylesheet" type="text/css" />
    <script src="/Themes/Scripts/Validator/JValidator.js" type="text/javascript"></script>
    <script src="/Themes/Scripts/jquery-1.8.2.min.js" type="text/javascript"></script>
    <script src="/Themes/Scripts/jquery.pullbox.js" type="text/javascript"></script>
    <script src="/Themes/Scripts/FunctionJS.js" type="text/javascript"></script>
    <script src="/Themes/Scripts/DatePicker/WdatePicker.js" type="text/javascript"></script>
    <script type="text/javascript">
        //添加
        function add() {
            document.getElementById('<%= this.btnAdd.ClientID %>').click();
        }
        //查询
        function Search() {

            document.getElementById('<%= this.btnSearch.ClientID %>').click();
        }
        //删除
        function Delete() {
            showConfirmMsg("此操作不可恢复，您确定要删除吗？", function (r) {
                if (r) {
                    document.getElementById('<%= this.btnDel.ClientID %>').click();
                }
            });
        }
        //保存
        function SaveForm() {
            document.getElementById('<%= this.btnSave.ClientID %>').click();
        }
        //提交
        function Submit() {
            document.getElementById('<%= this.btnSubmit.ClientID %>').click();
        }

        $(function () {
            InitControl();
        })
        function InitControl() {
            $(".div-body").PullBox({ dv: $(".div-body"), obj: $("#table1").find("tr") });
            divresize(190);
        }
        //确认
        function ComFirm() {
            document.getElementById('<%= this.btnComfirm.ClientID %>').click();
        }
        //审核
        function audit() {
            document.getElementById('<%= this.btnaudit.ClientID %>').click();

        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional">
        <ContentTemplate>
            <table width="100%">
                <colgroup>
                    <col width="11%" />
                    <col width="11%" />
                    <col width="11%" />
                    <col width="11%" />
                    <col width="11%" />
                    <col width="11%" />
                    <col width="11%" />
                    <col width="11%" />
                </colgroup>
                <tr>
                    <td class="inner_cell_right">
                        需气编号:
                    </td>
                    <td>
                        <asp:TextBox ID="txtGasCode" runat="server" checkexpession="NotNull" datacol="yes"
                            err="此项" />
                    </td>
                    <td class="inner_cell_right">
                        供电局名称:
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlPowerSupplyName" runat="server" Width="100%" checkexpession="NotNull"
                            datacol="yes" err="此项" CssClass="select" AutoPostBack="true" OnSelectedIndexChanged="ddlPowerSupplyName_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                    <td class="inner_cell_right">
                        变电站名称:
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlConvertStationName" runat="server" Width="100% " checkexpession="NotNull"
                            datacol="yes" err="此项" CssClass="select">
                        </asp:DropDownList>
                    </td>
                    <td class="inner_cell_right">
                        审核人:
                    </td>
                    <td>
                        <asp:TextBox ID="txtAuditorOID" runat="server" ReadOnly="true" Enabled="false" />
                    </td>
                </tr>
                <tr>
                    <td class="inner_cell_right">
                        需气量（kg）:
                    </td>
                    <td>
                        <asp:TextBox ID="txtAirDemand" runat="server" checkexpession="NotNull" datacol="yes"
                            err="此项" />
                    </td>
                    <td class="inner_cell_right">
                        数量说明:
                    </td>
                    <td>
                        <asp:TextBox ID="txtQtyDesc" runat="server" checkexpession="NotNull" datacol="yes"
                            err="此项" ToolTip="钢瓶容量规格及数量：25kg钢瓶  个，50kg钢瓶  个" Text="25kg  个，50kg  个" />
                    </td>
                    <td class="inner_cell_right">
                        用途说明:
                    </td>
                    <td>
                        <asp:TextBox ID="txtAppliDesc" runat="server" />
                    </td>
                    <td class="inner_cell_right">
                        审核日期:
                    </td>
                    <td>
                        <asp:TextBox ID="txtAuditsDate" runat="server" Enabled="false" />
                    </td>
                </tr>
                <tr>
                    <td class="inner_cell_right">
                        联系人:
                    </td>
                    <td>
                        <asp:TextBox ID="txtContacts" runat="server" checkexpession="NotNull" datacol="yes"
                            err="此项" />
                    </td>
                    <td class="inner_cell_right">
                        手机:
                    </td>
                    <td>
                        <asp:TextBox ID="txtPhoneNum" runat="server" checkexpession="NotNull" datacol="yes"
                            err="此项" />
                    </td>
                    <td class="inner_cell_right">
                        办公电话
                    </td>
                    <td>
                        <asp:TextBox ID="txtOfficeTel" runat="server" />
                    </td>
                    <td class="inner_cell_right">
                        登记人:
                    </td>
                    <td>
                        <asp:TextBox ID="txtRegistrantOID" runat="server" Enabled="false" />
                    </td>
                </tr>
                <tr>
                    <td class="inner_cell_right">
                        年度:
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlYear" runat="server">
                        </asp:DropDownList>
                    </td>
                    <td class="inner_cell_right">
                        月度:
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlMonth" runat="server">
                        </asp:DropDownList>
                    </td>
                    <td class="inner_cell_right">
                        收货地点:
                    </td>
                    <td>
                        <asp:TextBox ID="txtReceiptAddress" runat="server" Width="100%" checkexpession="NotNull"
                            datacol="yes" err="此项" />
                    </td>
                    <td class="inner_cell_right">
                        登记日期:
                    </td>
                    <td>
                        <asp:TextBox ID="txtRegistrantDate" runat="server" Enabled="false" />
                    </td>
                </tr>
                <tr>
                    <td class="inner_cell_right">
                        状态：
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlStatus" runat="server" Width="100%" Enabled="false">
                        </asp:DropDownList>
                    </td>
                    <td class="inner_cell_right">
                        需用日期:
                    </td>
                    <td>
                        <asp:TextBox ID="txtUseDate" runat="server" checkexpession="NotNull" datacol="yes"
                            err="此项" onfocus="WdatePicker()" />
                    </td>
                    <td class="inner_cell_right">
                    </td>
                    <td colspan="3">
                    </td>
                </tr>
            </table>
            <div style="text-align: left;">
                <uc2:LoadButton ID="LoadButton1" runat="server" />
            </div>
            <div style="text-align: right; display: none;">
                <asp:Button ID="btnSubmit" runat="server" OnClick="btnSubmit_Click" Text="btnSubmit_Click" />
                <asp:Button ID="btnSearch" runat="server" OnClick="btnSearch_Click" Text="btnSearch_Click" />
                <asp:Button ID="btnSave" runat="server" OnClick="btnSave_Click" Text="btnSave_Click"
                    OnClientClick="return CheckDataValid('#form1');" />
                <asp:Button ID="btnAdd" runat="server" OnClick="btnAdd_Click" Text="btnAdd_Click" />
                <asp:Button ID="btnDel" runat="server" OnClick="btnDel_Click" Text="btnDel_Click" />btnComfirm
                <asp:Button ID="btnComfirm" runat="server" OnClick="btnComfirm_Click" />
                <asp:Button ID="btnaudit" runat="server" OnClick="btnaudit_Click" />
            </div>
            <div class="div-body">
                <table id="table1" class="grid" singleselect="true">
                    <colgroup>
                        <col width="10%" />
                        <col width="10%" />
                        <col width="10%" />
                        <col width="5%" />
                        <col width="15%" />
                        <col width="10%" />
                        <col width="10%" />
                        <col width="10%" />
                        <col width="10%" />
                        <col width="10%" />
                        <col width="10%" />
                    </colgroup>
                    <thead>
                        <tr>
                            <%-- <td style="width: 20px; text-align: left;">
                        <label id="checkAllOff" onclick="CheckAllLine()" title="全选">
                            &nbsp;</label>
                    </td>--%>
                            <td style="text-align: center;">
                                需气编号
                            </td>
                            <td style="text-align: center;">
                                供电局名称
                            </td>
                            <td style="text-align: center;">
                                变电站名称
                            </td>
                            <td style="text-align: center;">
                                需气量
                            </td>
                            <td style="text-align: center;">
                                数量说明
                            </td>
                            <td style="text-align: center;">
                                用途说明
                            </td>
                            <td style="text-align: center;">
                                联系人
                            </td>
                            <td style="text-align: center;">
                                登记人
                            </td>
                            <td style="text-align: center;">
                                登记日期
                            </td>
                            <td style="text-align: center;">
                                状态
                            </td>
                        </tr>
                    </thead>
                    <tbody>
                        <asp:Repeater ID="rp_Item" runat="server" OnItemDataBound="rp_ItemDataBound" OnItemCommand="rp_Item_ItemCommand">
                            <ItemTemplate>
                                <tr style='background-color: <%#Eval("Green")%>'>
                                    <%--<td style="width: 20px; text-align: left;">
                                <input type="checkbox" value="<%#Eval("OID")%>" name="checkbox" />
                            </td>--%>
                                    <td style="width: 100px; text-align: center;">
                                        <asp:LinkButton ID="LBtnDel" runat="server" OnClientClick="selectrow(this)" CommandArgument='<%#Eval("OID") %>'
                                            CommandName="del"><%#Eval("GasCode")%></asp:LinkButton>
                                    </td>
                                    <td style="text-align: center;">
                                        <%#Eval("PowerSupplyName")%>
                                    </td>
                                    <td style="text-align: center;">
                                        <%#Eval("ConvertStationName")%>
                                    </td>
                                    <td style="text-align: center;">
                                        <%#Eval("AirDemand")%>
                                    </td>
                                    <td style="text-align: center;">
                                        <%#Eval("QtyDesc")%>
                                    </td>
                                    <td style="text-align: center;">
                                        <%#Eval("AppliDesc")%>
                                    </td>
                                    <td style="text-align: center;">
                                        <%#Eval("Contacts")%>
                                    </td>
                                    <td style="text-align: center;">
                                        <asp:Label ID="lblUserID" runat="server" Text='<%#Eval("Registrantoid")%>'></asp:Label>
                                    </td>
                                    <td style="text-align: center;">
                                        <%#Eval("RegistrantDate","{0:yyyy-MM-dd}")%>
                                    </td>
                                    <td style="text-align: center;">
                                        <asp:Label ID="lblStatus" runat="server" Text='<%#Eval("Status")%>'></asp:Label>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </tbody>
                </table>
            </div>
            <uc1:PageControl ID="PageControl1" runat="server" />
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="ddlConvertStationName" EventName="SelectedIndexChanged" />
            <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnAdd" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>
    </form>
</body>
</html>

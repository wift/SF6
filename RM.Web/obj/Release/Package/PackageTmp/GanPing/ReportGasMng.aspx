﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ReportGasMng.aspx.cs" Inherits="RM.Web.GanPing.ReportGasMng" %>

<%@ Register Src="~/UserControl/PageControl.ascx" TagName="PageControl" TagPrefix="uc1" %>
<%@ Register Src="~/UserControl/LoadButton.ascx" TagName="LoadButton" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>气体报告管理</title>
    <link href="/Themes/Styles/Site.css" rel="stylesheet" type="text/css" />
    <script src="/Themes/Scripts/Validator/JValidator.js" type="text/javascript"></script>
    <script src="/Themes/Scripts/jquery-1.8.2.min.js" type="text/javascript"></script>
    <script src="/Themes/Scripts/jquery.pullbox.js" type="text/javascript"></script>
    <script src="/Themes/Scripts/FunctionJS.js" type="text/javascript"></script>
    <script src="/Themes/Scripts/DatePicker/WdatePicker.js" type="text/javascript"></script>
    <script type="text/javascript">
        function Find() {
            document.getElementById('<%= this.btnSearch.ClientID %>').click();
        }

        function Empty() {

            $('input[type = "text"]').each(function () {
                $(this).val('');
            });
            $('select').each(function () {
                this.selectedIndex = 0;
            });
        }

        function ExportXls() {
            var key = CheckboxValue();
            document.getElementById('<%= this.HiddenField1.ClientID %>').value = key.toString();
            document.getElementById('<%= this.btnExport.ClientID %>').click();
           // setTimeout("execFunc()", 5000);

        }
        //function execFunc() {

        //    window.windowload();
        //}
        $(function () {
            InitControl();
        })

        function InitControl() {
            divresize(50);
            FixedTableHeader("#table1", $(window).height() - 100);

            $('.grid tr').each(function myfunction() {
                var isprint = $(this).find('span:last');
                if (isprint.html() == "") {
                    $(this).find('input[type="checkbox"]').attr("disabled", "disabled");
                }

            });
        }

        //合格证打印
        function PassPrint() {
            var key = CheckboxValue();
            document.getElementById('<%= this.HiddenField1.ClientID %>').value = key.toString();
            document.getElementById('<%= this.btnPassPrint.ClientID %>').click();
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:HiddenField ID="HiddenField1" runat="server" />
    <!-- Add.aspx -->
    <table width="100%">
        <colgroup>
            <col width="11%" />
            <col width="11%" />
            <col width="11%" />
            <col width="11%" />
            <col width="11%" />
            <col width="11%" />
            <col width="11%" />
            <col width="11%" />
        </colgroup>
        <tr>
            <td class="inner_cell_right">
                供电局名称：
            </td>
            <td>
                <asp:TextBox ID="txtPowerSupplyName" runat="server" Width="100%" />
            </td>
            <td class="inner_cell_right">
                变电站名称
            </td>
            <td>
                <asp:TextBox ID="txtConvertStationName" runat="server" Width="100%"></asp:TextBox>
            </td>
            <td class="inner_cell_right">
                送检单位：
            </td>
            <td>
                <asp:TextBox ID="tbInspectionUnit" runat="server" Width="100%" />
            </td>
            <td class="inner_cell_right">
                业务单号：
            </td>
            <td>
                <asp:TextBox ID="txtBusinessCode" runat="server" Width="100%" />
            </td>
        </tr>
        <tr>
            <td class="inner_cell_right">
                气体来源：
            </td>
            <td>
                <asp:DropDownList ID="ddlGasSourse" runat="server" Width="100%" CssClass="select">
                </asp:DropDownList>
            </td>
            <td class="inner_cell_right">
                检测单号：
            </td>
            <td>
                <asp:TextBox ID="txtCheckCode" runat="server" Width="100%"></asp:TextBox>
            </td>
            <td class="inner_cell_right">
                钢瓶编码：
            </td>
            <td>
                <asp:TextBox ID="txtCylinderCode" runat="server" Width="100%"></asp:TextBox>
            </td>
            <td class="inner_cell_right">
                气体编码:
            </td>
            <td>
                <asp:TextBox ID="txtGasCode" runat="server" Width="100%"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="inner_cell_right">
                报告编号:
            </td>
            <td>
                <asp:TextBox ID="txtReportCode" runat="server" Width="100%"></asp:TextBox>
            </td>
            <td class="inner_cell_right">
                检测项目类型:
            </td>
            <td>
                <asp:DropDownList ID="ddlCheckPJType" runat="server" Width="100%" CssClass="select">
                </asp:DropDownList>
            </td>
            <td class="inner_cell_right">
                是否合格:
            </td>
            <td>
                <asp:DropDownList ID="ddlIsPass" runat="server" Width="100%" CssClass="select">
                </asp:DropDownList>
            </td>
            <td class="inner_cell_right">
                是否打印报告:
            </td>
            <td>
                <asp:DropDownList ID="ddlIsPrint" runat="server" Width="100%" CssClass="select">
                    <asp:ListItem Text="" Value=""></asp:ListItem>
                    <asp:ListItem Text="是" Value="1"></asp:ListItem>
                    <asp:ListItem Text="否" Value="0"></asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="inner_cell_right">检测日期起:</td><td>  <asp:TextBox ID="txtCheckDateFrom" runat="server" Width="100%" onfocus="WdatePicker()"></asp:TextBox></td>
             <td class="inner_cell_right">检测日期止:</td><td>  <asp:TextBox ID="txtCheckDateTo" runat="server" Width="100%" onfocus="WdatePicker()"></asp:TextBox></td>
            <td colspan="4"></td>
        </tr>
    </table>
    <div style="text-align: left;">
        <uc2:LoadButton ID="LoadButton1" runat="server" />
    </div>
    <div style="text-align: right; display: none;">
        <input id="Reset1" type="reset" value="reset" />
        <asp:Button ID="btnSearch" runat="server" OnClick="btnSearch_Click" Text="btnSearch_Click" />
        <asp:Button ID="btnPassPrint" runat="server" OnClick="btnPassPrint_Click" />
        <asp:Button ID="btnExport" runat="server" OnClick="btnExport_Click" Text="btnExport" />
    </div>
    <div class="div-body1">
        <table id="table1" class="grid">
            <colgroup>
                <col width="5%" />
                <col width="15%" />
                <col width="10%" />
                <col width="10%" />
                <col width="10%" />
                <col width="10%" />
                <col width="10%" />
                <col width="10%" />
                <col width="10%" />
                <col width="10%" />
            </colgroup>
            <thead>
                <tr>
                    <td style="text-align: left;">
                        <label id="checkAllOff" onclick="CheckAllLine()" title="全选">
                            &nbsp;</label>
                    </td>
                    <td style="text-align: center;">
                        检测单号
                    </td>
                    <td style="text-align: center;">
                        报告编号
                    </td>
                    <td style="text-align: center;">
                        钢瓶编码
                    </td>
                    <td style="text-align: center;">
                        气体编码
                    </td>
                    <td style="text-align: center;">
                        气体来源
                    </td>
                    <td style="text-align: center;">
                        业务单号
                    </td>
                    <td style="text-align: center;">
                        检测项目类型
                    </td>
                    <td style="text-align: center;">
                        是否合格
                    </td>
                    <td style="text-align: center;">
                        是否打印报告
                    </td>
                </tr>
            </thead>
            <tbody>
                <asp:Repeater ID="rp_Item" runat="server" OnItemDataBound="rp_ItemDataBound" OnItemCommand="rp_Item_ItemCommand">
                    <ItemTemplate>
                        <tr>
                            <td style="text-align: left;">
                                <input type="checkbox" value="<%#Eval("OID")%>" name="checkbox" />
                            </td>
                            <td style="text-align: center;">
                                <%--  <asp:LinkButton ID="LBtnDel" runat="server" OnClientClick="selectrow(this)" CommandArgument='<%#Eval("OID") %>'
                                    CommandName="del"><%#Eval("CheckCode")%></asp:LinkButton>--%>
                                <%#Eval("CheckCode")%>
                            </td>
                            <td style="text-align: center;">
                                <%#Eval("ReportCode")%>
                            </td>
                            <td style="text-align: center;">
                                <%#Eval("CylinderCode")%>
                            </td>
                            <td style="text-align: center;">
                                <%#Eval("GasCdoe")%>
                            </td>
                            <td style="text-align: center;">
                                <asp:Label ID="lblGasSourse" runat="server" Text=' <%# Eval("GasSourse")%>'></asp:Label>
                            </td>
                            <td style="text-align: center;">
                                <%#Eval("BusinessCode")%>
                            </td>
                            <td style="text-align: center;">
                                <%# DictEGMNS["CheckPJTypeStatus"][Eval("CheckPJType").ToString()]%>
                            </td>
                            <td style="text-align: center;">
                                <asp:Label ID="lblIsPass" runat="server" Text='<%# Eval("IsPass")%>'></asp:Label>
                            </td>
                            <td style="text-align: center;">
                                <%# Convert.ToString(Eval("IsPrint"))=="True"?"是":"否"%>
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
            </tbody>
        </table>
    </div>
    </form>
</body>
</html>

﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RecordGPFIll.aspx.cs" Inherits="RM.Web.GanPing.RecordGPFIll" %>

<%@ Register Src="~/UserControl/PageControl.ascx" TagName="PageControl" TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>钢瓶充气记录 </title>
    <link href="/Themes/Styles/Site.css" rel="stylesheet" type="text/css" />
    <script src="/Themes/Scripts/Validator/JValidator.js" type="text/javascript"></script>
    <script src="/Themes/Scripts/jquery-1.8.2.min.js" type="text/javascript"></script>
    <script src="/Themes/Scripts/jquery.pullbox.js" type="text/javascript"></script>
    <script src="/Themes/Scripts/FunctionJS.js" type="text/javascript"></script>
    <script src="/Themes/Scripts/DatePicker/WdatePicker.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function InitControl() {
            $(".div-body1").PullBox({ dv: $(".div-body1"), obj: $("#tableDetails").find("tr") });
            divresize(155);
        })
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div class="div-body1">
        <table id="tableDetails" class="grid1">
            <colgroup>
                <col width="11%" />
                <col width="11%" />
                <col width="11%" />
                <col width="11%" />
                <col width="11%" />
                <col width="11%" />
                <col width="11%" />
                <col width="11%" />
                <col width="12%" />
            </colgroup>
            <thead>
                <tr>
                    <td style="text-align: center;">
                        钢瓶编码
                    </td>
                    <td style="text-align: center;">
                        气体类型
                    </td>
                    <td style="text-align: center;">
                        充气编号
                    </td>
                    <td style="text-align: center;">
                        业务单号
                    </td>
                    <td style="text-align: center;">
                        气体编码
                    </td>
                    <td style="text-align: center;">
                        气量
                    </td>
                    <td style="text-align: center;">
                        钢瓶状态
                    </td>
                    <td style="text-align: center;">
                        登记人
                    </td>
                    <td style="text-align: center;">
                        登记日期
                    </td>
                </tr>
            </thead>
            <tbody>
                <asp:Repeater ID="rp_Item" runat="server">
                    <ItemTemplate>
                        <tr>
                            <td style="text-align: center;">
                                <%#Eval("CylinderCode")%>
                            </td>
                            <td style="text-align: center;">
                                <%#GasType[Eval("GasType").ToString()]%>
                            </td>
                            <td style="text-align: center;">
                                <asp:Label ID="lblCylinderCapacity" runat="server" Text='<%#Eval("FillGasCode")%>'></asp:Label>
                            </td>
                            <td style="text-align: center;">
                                <%#Eval("BusinessCode")%>
                            </td>
                            <td style="text-align: center;">
                                <%#Eval("GasCode")%>
                            </td>
                            <td style="text-align: center;">
                                <%#Eval("AmountGas")%>
                            </td>
                            <td style="text-align: center;">
                                <%# GPStatus[Eval("CylinderStatus").ToString()]%>

                             
                            </td>
                            <td style="text-align: center;">
                                <%# GetUserName(Eval("RegistrantOID").ToString())%>
                            </td>
                            <td style="text-align: center;">
                                <%#Eval("RegistrantDate", "{0:yyyy-MM-dd}")%>
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
            </tbody>
        </table>
    </div>
    <uc1:PageControl ID="PageControl1" runat="server" />
    </form>
</body>
</html>

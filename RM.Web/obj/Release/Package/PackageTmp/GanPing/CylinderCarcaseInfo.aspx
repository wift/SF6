﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CylinderCarcaseInfo.aspx.cs"
    Inherits="RM.Web.GanPing.CylinderCarcaseInfo" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>库存查询 </title>
    <link href="/Themes/Styles/Site.css" rel="stylesheet" type="text/css" />
    <script src="/Themes/Scripts/Validator/JValidator.js" type="text/javascript"></script>
    <script src="/Themes/Scripts/jquery-1.8.2.min.js" type="text/javascript"></script>
     <script src="/Themes/Scripts/FunctionJS.js" type="text/javascript"></script>
    <style type="text/css">
        .jzlayer
        {
            background-color: #1097d3;
        }

            .jzlayer > tbody > tr
            {
                height: 45px;
            }

                .jzlayer > tbody > tr > td
                {
                    width: 45px;
                    height: 45px;
                }

        .circleColor0
        {
            width: 45px;
            height: 45px;
            background-color: #CB7676 !important;
            -moz-border-radius: 25px;
            -webkit-border-radius: 25px;
            border-radius: 25px;
            text-align: center;
        }

        .circleColor1
        {
            width: 45px;
            height: 45px;
            background-color: #0e4ad8 !important;
            -moz-border-radius: 25px;
            -webkit-border-radius: 25px;
            border-radius: 25px;
            text-align: center;
        }

        .circleColor2
        {
            width: 45px;
            height: 45px;
            background-color: #41ed83 !important;
            -moz-border-radius: 25px;
            -webkit-border-radius: 25px;
            border-radius: 25px;
            text-align: center;
        }

        .circleColor3
        {
            width: 45px;
            height: 45px;
            background-color: yellow !important;
            -moz-border-radius: 25px;
            -webkit-border-radius: 25px;
            border-radius: 25px;
            text-align: center;
        }

        .circleColor9
        {
            width: 45px;
            height: 45px;
            background-color: red !important;
            -moz-border-radius: 25px;
            -webkit-border-radius: 25px;
            border-radius: 25px;
            text-align: center;
        }

        .circleColor4
        {
            width: 45px;
            height: 45px;
            background-color: seagreen !important;
            -moz-border-radius: 25px;
            -webkit-border-radius: 25px;
            border-radius: 25px;
            text-align: center;
        }

        .circleColor5
        {
            width: 45px;
            height: 45px;
            background-color: #1dd6f7 !important;
            -moz-border-radius: 25px;
            -webkit-border-radius: 25px;
            border-radius: 25px;
            text-align: center;
        }

        .circleColor6
        {
            width: 45px;
            height: 45px;
            background-color: #808080 !important;
            -moz-border-radius: 25px;
            -webkit-border-radius: 25px;
            border-radius: 25px;
            text-align: center;
        }

        .circleyellow
        {
            width: 45px;
            height: 45px;
            background: yellow;
            -moz-border-radius: 25px;
            -webkit-border-radius: 25px;
            border-radius: 25px;
            text-align: center;
        }

            .circleyellow:after
            {
                display: inline-block;
                width: 0;
                height: 100%;
                vertical-align: middle;
                content: "";
            }

        .circlewhite
        {
            width: 45px;
            height: 45px;
            background: white;
            -moz-border-radius: 25px;
            -webkit-border-radius: 25px;
            border-radius: 25px;
        }

        .columndiv
        {
            float: left;
            border: 1px;
            border-style: solid;
            margin-right: 5px;
            position: absolute;
            bottom: 0;
        }

      

        .positionDiv
        {
            height: 250px;
            position: relative;
             border-style:ridge;
             border-width:1pt;
            background-color:#F7F9FA;

        }
        .HighText
        {
            border-color:red;
              border-style:solid;
             border-width:thin;
        }
        body {
        overflow-y:auto;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table width="600px">
                <colgroup>
                    <col width="15%" />
                    <col width="15%" />
                    <col width="15%" />
                       <col width="15%" />
                    <col width="40%" />
                </colgroup>
                <tr>
                    <td class="inner_cell_right">库区名称：
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlPositionCode" runat="server">
                        </asp:DropDownList>
                    </td>
                       <td class="inner_cell_right">钢瓶编码：
                    </td>
                    <td>
                      <asp:TextBox ID="txtCPCode" runat="server"
                            datacol="yes"  err="此项" />
                    </td>
                    <td>
                        <asp:Button ID="btnSearch" runat="server" Text="查询" OnClick="btnSearch_Click" />
                    </td>
                </tr>
            </table>
            <div id="middleTitle" style="height: 25px; width: 100%; background-color: Yellow; font-size: large; margin-bottom: 20px;">
                <%= MiddleTitle%>
            </div>
            <div>

                <div id="divrigth" class="positionDiv">
                    <div class="albottom">
                        <asp:Repeater runat="server" ID="rpttopList">
                            <ItemTemplate>
                                <div class="columndiv">
                                    <%# Eval("value")%>
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                </div>
                <div style="clear: left;">
                </div>
                <div id="divmiddle" class="positionDiv" >
     <asp:Repeater runat="server" ID="rptMiddleList">
                        <ItemTemplate>
                            <div class="columndiv">
                                <%# Eval("value")%>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
                <div id="divleft" class="positionDiv">
                    <asp:Repeater runat="server" ID="rptbottomList">
                        <ItemTemplate>
                            <div class="columndiv">
                                <%# Eval("value")%>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
            </div>
        </div>
    </form>
    <script type="text/javascript">
        $(document).ready(function myfunction() {
            var count = $(form1).find(".circlewhite").length;

            var text = $("#middleTitle").text();

            $("#middleTitle").text(text + ' 空位：' + count + '个');
            var $left = 0;
            $("#divrigth").find(".columndiv").each(function myfunction() {
                $(this).css("left", $left);
                $left += 390;
            });
            $left = 0;
            $("#divleft").find(".columndiv").each(function myfunction() {
                $(this).css("left", $left);
                $left += 390;
            });
            $left = 0;
            $("#divmiddle").find(".columndiv").each(function myfunction() {
                $(this).css("left", $left);
                $left += 390;
            });

        });
    </script>
</body>
</html>

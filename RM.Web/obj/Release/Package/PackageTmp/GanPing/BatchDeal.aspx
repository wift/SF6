﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BatchDeal.aspx.cs" Inherits="RM.Web.GanPing.BatchDeal" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>净化气体批量处理功能</title>
    <link href="/Themes/Styles/Site.css" rel="stylesheet" type="text/css" />
    <script src="/Themes/Scripts/Validator/JValidator.js" type="text/javascript"></script>
    <script src="/Themes/Scripts/jquery-1.8.2.min.js" type="text/javascript"></script>
    <script src="/Themes/Scripts/jquery.pullbox.js" type="text/javascript"></script>
    <script src="/Themes/Scripts/FunctionJS.js" type="text/javascript"></script>
    <script src="/Themes/Scripts/DatePicker/WdatePicker.js" type="text/javascript"></script>
    <script src="/Themes/Scripts/artDialog/artDialog.source.js" type="text/javascript"></script>
    <script src="../Themes/Scripts/artDialog/iframeTools.source.js" type="text/javascript"></script>
    <script type="text/javascript">

        function Next(o) {

            if (o.value.length == 8) {

                var str = o.name;
                var intvalue = str[15];

                var nextID = str.substring(0, 15) + (parseInt(intvalue) + 1).toString();
                $('#' + nextID).focus().select();

            }

        }

    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table width="100%">
            <colgroup>
                <col width="11%" />
                <col width="11%" />
                <col width="11%" />
                <col width="11%" />
                <col width="11%" />
                <col width="11%" />
                <col width="11%" />
                <col width="11%" />
            </colgroup>
            <tr>
                <td colspan="8" style="text-align: center;">
                    <h1>
                        净化气体批量处理功能</h1>
                </td>
            </tr>
            <tr>
                <td class="inner_cell_right">
                    气量1:
                </td>
                <td>
                    <asp:TextBox ID="txtGasAmount1" runat="server" Text="50" />
                </td>
                <td class="inner_cell_right">
                    气量2:
                </td>
                <td>
                    <asp:TextBox ID="txtGasAmount2" runat="server" Text="50" />
                </td>
                <td class="inner_cell_right">
                    气量3:
                </td>
                <td>
                    <asp:TextBox ID="txtGasAmount3" runat="server" Text="50" />
                </td>
                <td class="inner_cell_right">
                    气量4:
                </td>
                <td>
                    <asp:TextBox ID="txtGasAmount4" runat="server" Text="50" />
                </td>
            </tr>
            <tr>
                <td class="inner_cell_right">
                    钢瓶1:
                </td>
                <td>
                    <asp:TextBox ID="txtCylinderCode1" runat="server" onkeyup="Next(this)" />
                </td>
                <td class="inner_cell_right">
                    钢瓶2:
                </td>
                <td>
                    <asp:TextBox ID="txtCylinderCode2" runat="server" onkeyup="Next(this)" />
                </td>
                <td class="inner_cell_right">
                    钢瓶3:
                </td>
                <td>
                    <asp:TextBox ID="txtCylinderCode3" runat="server" onkeyup="Next(this)" />
                </td>
                <td class="inner_cell_right">
                    钢瓶4
                </td>
                <td>
                    <asp:TextBox ID="txtCylinderCode4" runat="server" onkeyup="Next(this)" />
                </td>
            </tr>
            <tr>
                <td class="inner_cell_right">
                    钢瓶5:
                </td>
                <td>
                    <asp:TextBox ID="txtCylinderCode5" runat="server" onkeyup="Next(this)" />
                </td>
                <td class="inner_cell_right">
                    钢瓶6:
                </td>
                <td>
                    <asp:TextBox ID="txtCylinderCode6" runat="server" onkeyup="Next(this)" />
                </td>
                <td class="inner_cell_right">
                    钢瓶7:
                </td>
                <td>
                    <asp:TextBox ID="txtCylinderCode7" runat="server" onkeyup="Next(this)" />
                </td>
                <td class="inner_cell_right">
                    钢瓶8:
                </td>
                <td>
                    <asp:TextBox ID="txtCylinderCode8" runat="server" onkeyup="Next(this)" />
                </td>
            </tr>
            <tr>
                <td class="inner_cell_right">
                    气量5:
                </td>
                <td>
                    <asp:TextBox ID="txtGasAmount5" runat="server" />
                </td>
                <td class="inner_cell_right">
                    气量6:
                </td>
                <td>
                    <asp:TextBox ID="txtGasAmount6" runat="server" />
                </td>
                <td class="inner_cell_right">
                    气量7:
                </td>
                <td>
                    <asp:TextBox ID="txtGasAmount7" runat="server" />
                </td>
                <td class="inner_cell_right">
                    气量8:
                </td>
                <td>
                    <asp:TextBox ID="txtGasAmount8" runat="server" />
                </td>
            </tr>
            <tr>
                <td colspan="8" style="text-align: center;">
                    <asp:Button ID="btnBatchDeal" runat="server" OnClick="btnBatchDeal_Click" Text="批量处理"
                        UseSubmitBehavior="false" />
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>

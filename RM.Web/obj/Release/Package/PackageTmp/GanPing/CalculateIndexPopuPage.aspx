﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CalculateIndexPopuPage.aspx.cs"
    Inherits="RM.Web.GanPing.CalculateIndexPopuPage" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>指标计算</title>
    <link href="/Themes/Styles/Site.css" rel="stylesheet" type="text/css" />
    <script src="/Themes/Scripts/Validator/JValidator.js" type="text/javascript"></script>
    <script src="/Themes/Scripts/jquery-1.8.2.min.js" type="text/javascript"></script>
    <script src="/Themes/Scripts/jquery.pullbox.js" type="text/javascript"></script>
    <script src="/Themes/Scripts/FunctionJS.js" type="text/javascript"></script>
    <script src="/Themes/Scripts/DatePicker/WdatePicker.js" type="text/javascript"></script>
    <script type="text/javascript">

        function ClearPipe(id) {
            $('#' + id).val("");
        }

        function Clear3() {

            $('#txtEnvironmentTemperature').val("");
            $('#txtEnvironmentHumidity').val("");
            $('#txtBarometricPressure').val("");
        }

        function Next(o) {

            if (o.value.length == 8) {

                var str = o.name;
                var intvalue = str[7];

                var nextID = str.substring(0, 7) + (parseInt(intvalue) + 1).toString();
                $('#' + nextID).focus().select();

            }

        }

        function SaveFrom() {
            var selectValue = $("#ddlCheckPJType")[0].value
            if (selectValue == "1") {//4
                if ($("#txtsfhl").val() === "") {
                    showWarningMsg("水分含量必填!");
                }
                else {
                    document.getElementById('<%= this.btnSave.ClientID %>').click();
                }

            }
            else {//7
                if (CheckDataValid('.style1') == true) {
                    document.getElementById('<%= this.btnSave.ClientID %>').click();
                }
            }
        }




    </script>
    <style type="text/css">
        body {
        overflow-x:hidden !important;
        overflow-y:scroll  !important;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table width="100%">
                <colgroup>
                    <col />
                    <col width="11%" />
                    <col width="11%" />
                    <col width="11%" />
                    <col width="11%" />
                    <col width="11%" />
                    <col width="11%" />
                    <col width="11%" />
                    <col width="11%" />
                </colgroup>
                <tr>
                    <td class="inner_cell_right">钢瓶编码：
                    </td>
                    <td>
                        <input id="txtCylinderCode" type="text" runat="server" checkexpession="NotNull" datacol="yes"
                            err="此项" disabled="disabled" />
                    </td>
                    <td class="inner_cell_right">钢瓶钢印号：
                    </td>
                    <td>
                        <input id="txtCylinderSeaNo" type="text" runat="server" checkexpession="NotNull"
                            datacol="yes" err="此项" disabled="disabled" />
                    </td>
                    <td class="inner_cell_right">业务编号：
                    </td>
                    <td>
                        <input type="text" id="txtBusinessCode" runat="server" checkexpession="NotNull" datacol="yes"
                            err="此项" disabled="disabled" />
                    </td>
                    <td class="inner_cell_right">气体来源：
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlGasSourse" runat="server" Width="100%" checkexpession="NotNull"
                            datacol="yes" err="此项" CssClass="select" Enabled="false">
                        </asp:DropDownList>
                    </td>
                </tr>
            </table>
            <hr />
            <span style="font-weight:bold; color:blue;">直接录入结果:</span>
            
        <table width="100%">
            <colgroup>
                <col width="10%" />
                <col width="20%" />
                <col width="10%" />
                <col width="20%" />
                <col width="10%" />
                <col width="20%" />
            </colgroup>
            <tr>
                <td class="inner_cell_right">环境温度（℃）：
                </td>
                <td>
                    <input id="txtEnvironmentTemperature" type="text" runat="server" checkexpession="NotNull"
                        datacol="no" err="此项" />
                </td>
                <td class="inner_cell_right">环境湿度（%RH）：
                </td>
                <td>
                    <input id="txtEnvironmentHumidity" type="text" runat="server" checkexpession="NotNull"
                        datacol="no" err="此项" />
                </td>
                <td class="inner_cell_right">大气压（kPa）:
                </td>
                <td>
                    <input id="txtBarometricPressure" type="text" runat="server" checkexpession="NotNull"
                        datacol="no" err="此项" />
                </td>
            </tr>
            <tr>
                <td class="inner_cell_right">六氟化硫(SF6) 的质量分数结果（/%）：
                </td>
                <td>
                    <input id="txtSF6Result" type="text" runat="server" checkexpession="Double" datacol="yes"
                        err="此项" />
                </td>
                <td class="inner_cell_right">空气的质量分数结果（/%）：
                </td>
                <td>
                    <input id="txtAirQualityScoreResult" type="text" runat="server" datacol="yes" err="此项"
                        checkexpession="Double" />
                </td>
                <td class="inner_cell_right">四氟化碳(CF4) 的质量分数结果（/%）：
                </td>
                <td>
                    <input id="txtCF4Result" type="text" runat="server" checkexpession="Double" datacol="yes"
                        err="此项" />
                </td>
            </tr>
            <tr>
                <td class="inner_cell_right">水的露点结果（/℃）：
                </td>
                <td>
                    <input id="txtWaterDewResult" type="text" runat="server" checkexpession="Double"
                        datacol="yes" err="此项" />
                </td>
           

                <td class="inner_cell_right">六氟乙烷(C2F6)质量分数（10^-6）
                </td>
                <td>

                     <input id="txtC2F6" type="text" runat="server" checkexpession="Double"
                        datacol="yes" err="此项" />
                </td>
                <td class="inner_cell_right">八氟丙烷(C3F8)质量分数（10^-6）
                </td>
                <td>

                     <input id="txtC3F8" type="text" runat="server" checkexpession="Double"
                        datacol="yes" err="此项" />
                </td>

            </tr>
            <tr>
                <td colspan="6" style="text-align:right;">
                    <asp:Button ID="btnOnlySave" runat="server" Text="保存" OnClick="btnOnlySave_Click" />
                </td>
            </tr>
            <tr>
              <td colspan="8">
                  <hr />
                 <span style="font-weight:bold; color:blue;"> 通过计算得出：</span>
              </td>
            </tr>
            <tr>
                <td class="inner_cell_right">酸度 (以HF计) 的质量分数结果（/%）：
                </td>
                <td>
                    <input id="txtHFResult" type="text" runat="server" checkexpession="Double" datacol="yes"
                        err="此项" />
                </td>
                <td class="inner_cell_right">可水解氟化物(以HF计)的质量分数结果（/%）：
                </td>
                <td>
                    <input id="txtKSJHFResult" type="text" runat="server" checkexpession="Double" datacol="yes"
                        err="此项" />
                </td>
                <td class="inner_cell_right">矿物油的质量分数结果（/%）：
                </td>
                <td>
                    <input id="txtKWYSorceResult" type="text" runat="server" checkexpession="Double"
                        datacol="yes" err="此项" />
                </td>
            </tr>
            <tr>
                     <td class="inner_cell_right">水的质量分数结果（/%）：
                </td>
                <td>
                    <input id="txtWaterQualityScoreResult" type="text" runat="server" checkexpession="Double"
                        datacol="yes" err="此项" />
                </td>
                     <td class="inner_cell_right">是否合格：
                </td>
                <td>
                    <asp:DropDownList ID="ddlIsPass" runat="server"  Width="130px" checkexpession="NotNull"
                        datacol="yes" err="此项" CssClass="select">
                    </asp:DropDownList>
                </td>
            
             <td class="inner_cell_right">标准体积:</td>
                <td> <input id="txtStandardVolume" type="text" runat="server" checkexpession="Double" datacol="yes"
                        err="此项" /></td>
          
            </tr>
            <tr>
                      <td class="inner_cell_right">检测项目类型:
                </td>
                <td>
                    <asp:DropDownList ID="ddlCheckPJType" runat="server" Width="130px" CssClass="select">
                    </asp:DropDownList>
                </td>
                <td class="inner_cell_right">检测日期:
                </td>
                <td>
                    <asp:TextBox ID="txtCheckDate" runat="server" onfocus="WdatePicker()"></asp:TextBox>
                </td>
            
                <td class="inner_cell_right">报告编号:

                </td>
                <td>
                     <input id="txtReportCode"  type="text" runat="server" checkexpession="NotNull"
                        datacol="no" err="此项" />
             
                </td>
                
            </tr>
            <tr>
                  <td class="inner_cell_right">结论：
                </td>
                <td colspan="4">
                    <input name="txtConclusion" type="text" style=" width:100%" id="txtConclusion" runat="server" checkexpession="NotNull"
                        datacol="yes" err="此项" value="" />
                </td>
                <td  style="text-align:right;">
                       <asp:Button ID="btnSaveIndex" runat="server" Text="保存" OnClick="btnSaveIndex_Click" />
                    <asp:Button ID="btnExport" runat="server" Text="导出" OnClick="btnExport_Click" />

                </td></tr>
            <tr>
                <td colspan="6">
                    <hr />
                   <span style="font-weight:bold; color:blue;"> 酸度指标计算：</span>
                </td>
            </tr>
            <tr id="jsbcRequired" class="style1">
                <td class="inner_cell_right">硫酸浓度(%):
                </td>
                <td>
                    <input id="txtlsnd" type="text" runat="server" checkexpession="Double" datacol="yes"
                        err="此项" />
                </td>
                <td class="inner_cell_right">酸度采气量(L):
                </td>
                <td>
                    <input id="txtcql" type="text" runat="server" checkexpession="Double" datacol="yes"
                        err="此项" />
                </td>
            </tr>
            <tr class="style1">
                <td class="inner_cell_right">始吸收瓶(mL):
                </td>
                <td>
                    <input id="txtsxsp" type="text" runat="server" checkexpession="Double" datacol="yes"
                        err="此项" />
                </td>
                <td class="inner_cell_right">次吸收瓶(mL):
                </td>
                <td>
                    <input id="txtcxsp" type="text" runat="server" checkexpession="Double" datacol="yes"
                        err="此项" />
                </td>
                <td class="inner_cell_right">未吸收瓶(mL):
                </td>
                <td>
                    <input id="txtwxsp" type="text" runat="server" checkexpession="Double" datacol="yes"
                        err="此项" />
                </td>
            </tr>
            <tr>
                <td colspan="6">
                    <hr />
                   <span style="font-weight:bold; color:blue;"> 矿物油指标计算：</span>
                </td>
            </tr>
            <tr class="style1">
                <td class="inner_cell_right">矿物油采气量(L):
                </td>
                <td>
                    <input id="txtKWYCQL" type="text" runat="server" checkexpession="Double" datacol="yes"
                        err="此项" />
                </td>
                <td class="inner_cell_right">测量读数（mg/L）:
                </td>
                <td>
                    <input id="txtclds" type="text" runat="server" checkexpession="Double" datacol="yes"
                        err="此项" />
                </td>
            </tr>
            <tr>
                <td colspan="6">
                    <hr />
                    <span style="font-weight:bold; color:blue;"> 可水解氟化物，水的质量分数指标计算：</span>
                </td>
            </tr>
            <tr class="style1">
                <td class="inner_cell_right">浓度（μg）:
                </td>
                <td>
                    <input id="txtnd" type="text" runat="server" checkexpession="Double" datacol="yes"
                        err="此项" />
                </td>
                <td class="inner_cell_right">采样体积（L）:
                </td>
                <td>
                    <input id="txtcytj" type="text" runat="server" checkexpession="Double" datacol="yes"
                        err="此项" />
                </td>
                <td class="inner_cell_right">水分含量（μL/L）:
                </td>
                <td>
                    <input id="txtsfhl" type="text" runat="server" checkexpession="Double" datacol="yes"
                        err="此项" />
                </td>
            </tr>
              <tr>
                  <td colspan="6">
                    <hr />
                </td>
            </tr>
            <tr class="style1">
                <td class="inner_cell_right">二氧化硫（μL/L）:
                </td>
                <td>
                    <input id="txtMonoxide" type="text" runat="server" checkexpession="Double" datacol="yes"
                        err="此项" />
                </td>
                <td class="inner_cell_right">硫化氢（μL/L）:
                </td>
                <td>
                    <input id="txthydrothion" type="text" runat="server" checkexpession="Double" datacol="yes"
                        err="此项" />
                </td>
                <td class="inner_cell_right">一氧化碳（μL/L）:
                </td>
                <td>
                    <input id="txtSulfurDioxide" type="text" runat="server" checkexpession="Double" datacol="yes"
                        err="此项" />
                </td>
            </tr>
            <tr>
                <td colspan="6" align="right">
                    <div style="display: none;">
                        <asp:Button ID="btnSave" runat="server" Text="计算保存" OnClick="btnSave_Click" UseSubmitBehavior="false" />
                    </div>
                    <input type="button" value="计算保存" onclick="javascript: SaveFrom();" id="btnS" />
                    <asp:Button ID="btnBatchSave" runat="server" Text="同批次同步数据" OnClick="btnBatchSave_Click"
                        UseSubmitBehavior="false" />
                </td>
            </tr>
            <tr>
                <td colspan="6">
                    <hr />
                </td>
            </tr>
            <tr>
                <td class="inner_cell_right">色谱仪：
                </td>
                <td>
                    <asp:DropDownList ID="ddlSPYID" runat="server" Width="100%" checkexpession="NotNull"
                        datacol="yes" err="此项" CssClass="select">
                    </asp:DropDownList>
                </td>
                <td class="inner_cell_right">露点仪：
                </td>
                <td>
                    <asp:DropDownList ID="ddlLDYID" runat="server" Width="100%" checkexpession="NotNull"
                        datacol="yes" err="此项" CssClass="select">
                    </asp:DropDownList>
                </td>
                <td class="inner_cell_right">温湿计：
                </td>
                <td>
                    <asp:DropDownList ID="ddlWSJID" runat="server" Width="100%" checkexpession="NotNull"
                        datacol="yes" err="此项" CssClass="select">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="inner_cell_right">气压计：
                </td>
                <td>
                    <asp:DropDownList ID="ddlQYJID" runat="server" Width="100%" checkexpession="NotNull"
                        datacol="yes" err="此项" CssClass="select">
                    </asp:DropDownList>
                </td>
                <td class="inner_cell_right">酸度计：
                </td>
                <td>
                    <asp:DropDownList ID="ddlSDJID" runat="server" Width="100%" checkexpession="NotNull"
                        datacol="yes" err="此项" CssClass="select">
                    </asp:DropDownList>
                </td>
                <td class="inner_cell_right">分光光度计：
                </td>
                <td>
                    <asp:DropDownList ID="ddlFGGDJID" runat="server" Width="100%" checkexpession="NotNull"
                        datacol="yes" err="此项" CssClass="select">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="inner_cell_right">红外含油分析仪：
                </td>
                <td>
                    <asp:DropDownList ID="ddlHWHYFXYID" runat="server" Width="100%" checkexpession="NotNull"
                        datacol="yes" err="此项" CssClass="select">
                    </asp:DropDownList>
                </td>
                <td colspan="4" align="right"></td>
            </tr>
        </table>
        </div>
    </form>
</body>
</html>

﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CylinderCarcaseCodePage.aspx.cs"
    Inherits="RM.Web.GanPing.CylinderCarcaseCodePage" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Src="~/UserControl/PageControl.ascx" TagName="PageControl" TagPrefix="uc1" %>
<%@ Register Src="~/UserControl/LoadButton.ascx" TagName="LoadButton" TagPrefix="uc2" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>钢瓶架编码</title>
    <link href="/Themes/Styles/Site.css" rel="stylesheet" type="text/css" />
    <script src="/Themes/Scripts/Validator/JValidator.js" type="text/javascript"></script>
    <script src="/Themes/Scripts/jquery-1.8.2.min.js" type="text/javascript"></script>
    <script src="/Themes/Scripts/jquery.pullbox.js" type="text/javascript"></script>
    <script src="/Themes/Scripts/FunctionJS.js" type="text/javascript"></script>
    <script src="/Themes/Scripts/DatePicker/WdatePicker.js" type="text/javascript"></script>
    <script type="text/javascript">
        //添加
        function add() {
            document.getElementById('<%= this.btnAdd.ClientID %>').click();
        }
        //查询
        function search() {

            document.getElementById('<%= this.btnSearch.ClientID %>').click();
        }
        //删除
        function Delete() {
            showConfirmMsg("此操作不可恢复，您确定要删除吗？", function (r) {
                if (r) {
                    document.getElementById('<%= this.btnDel.ClientID %>').click();
                }
            });


        }
        //保存
        function SaveForm() {
            document.getElementById('<%= this.btnSave.ClientID %>').click();
        }


        $(function () {
            InitControl();
        })

        function InitControl() {
            $(".div-body").PullBox({ dv: $(".div-body"), obj: $("#table1").find("tr") });
            divresize(110);



        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional">
        <ContentTemplate>
            <table width="100%" class="layoutTable">
                <colgroup>
                    <col width="11%" />
                    <col width="18%" />
                    <col width="14%" />
                    <col width="17%" />
                    <col width="15%" />
                    <col width="25%" />
                </colgroup>
                <tr>
                    <td class="inner_cell_right">
                        架子编码：
                    </td>
                    <td>
                        <cc1:ExTextBox ID="txtCarcaseCode" runat="server" checkexpession="LenStr" datacol="yes"
                            length="50" err="此项" FieldName="CarcaseCode" BindName="CylinderCarcaseCodeObj" ReadOnly=true />
                    </td>
                    <td class="inner_cell_right">
                        库区名称：
                    </td>
                    <td>
                        <cc1:ExDropDownList ID="ddlPositionCode" runat="server" CssClass="select" FieldName="PositionCode"
                            BindName="CylinderCarcaseCodeObj" Width="131" checkexpession="NotNull" datacol="yes" ReadOnly=true
                            err="此项" />
                    </td>
                    <td class="inner_cell_right">
                        存放方位：
                    </td>
                    <td>
                        <cc1:ExDropDownList ID="ddlDepositPosition" runat="server" CssClass="select" FieldName="DepositPosition"
                            BindName="CylinderCarcaseCodeObj" Width="131" checkexpession="NotNull" datacol="yes" ReadOnly=true
                            err="此项" />
                    </td>
                </tr>
                <tr>
                    <td class="inner_cell_right">
                        存放列数：
                    </td>
                    <td>
                        <cc1:ExTextBox ID="txtDepositColumnCount" runat="server" checkexpession="LenStr" ReadOnly=true
                            datacol="yes" length="50" err="此项" FieldName="DepositColumnCount" BindName="CylinderCarcaseCodeObj" />
                    </td>
                    <td class="inner_cell_right">
                        存放层数：
                    </td>
                    <td>
                        <cc1:ExTextBox ID="txtDepositRowCount" runat="server" checkexpession="LenStr" datacol="yes" ReadOnly=true
                            length="50" err="此项" FieldName="DepositRowCount" BindName="CylinderCarcaseCodeObj" />
                    </td>
                    <td class="inner_cell_right">
                        存放说明：
                    </td>
                    <td>
                        <cc1:ExTextBox ID="txtDepositDesc" runat="server" checkexpession="LenStr" datacol="yes" ReadOnly=true
                            length="50" err="此项" FieldName="DepositDesc" BindName="CylinderCarcaseCodeObj" />
                    </td>
                </tr>
                <tr>
                    <td class="inner_cell_right">
                        气体类型：
                    </td>
                    <td>
                        <cc1:ExDropDownList ID="ddlGasType" runat="server" CssClass="select" FieldName="GasType" ReadOnly=true
                            BindName="CylinderCarcaseCodeObj" Width="131" checkexpession="NotNull" datacol="yes"
                            err="此项" />
                    </td>
                    <td class="inner_cell_right">
                        钢瓶状态：
                    </td>
                    <td>
                        <cc1:ExDropDownList ID="dllCylinderStatus" runat="server" CssClass="select" FieldName="CylinderStatus" ReadOnly=true
                            BindName="CylinderCarcaseCodeObj" Width="131" checkexpession="NotNull" datacol="yes"
                            err="此项" />
                    </td>
                    <td class="inner_cell_right">
                    </td>
                    <td>
                    </td>
                </tr>
            </table>
            <div style="text-align: left;">
                <uc2:LoadButton ID="LoadButton1" runat="server" />
            </div>
            <div style="text-align: right; display: none;">
                <asp:Button ID="btnSearch" runat="server" OnClick="btnSearch_Click" Text="btnSearch_Click" />
                <asp:Button ID="btnSave" runat="server" OnClick="btnSave_Click" Text="btnSave_Click"
                    OnClientClick="return CheckDataValid('#form1');" />
                <asp:Button ID="btnAdd" runat="server" OnClick="btnAdd_Click" Text="btnAdd_Click" />
                <asp:Button ID="btnDel" runat="server" OnClick="btnDel_Click" Text="btnDel_Click" />
            </div>
            <div class="div-body">
                <table id="table1" class="grid" singleselect="true">
                    <colgroup>
                        <col width="10%" />
                        <col width="10%" />
                        <col width="10%" />
                        <col width="10%" />
                        <col width="10%" />
                        <col width="10%" />
                        <col width="10%" />
                        <col width="30%" />
                    </colgroup>
                    <thead>
                        <tr>
                            <td style="text-align: center;">
                                架子编码
                            </td>
                            <td style="text-align: center;">
                                库区名称
                            </td>
                            <td style="text-align: center;">
                                存放位置
                            </td>
                            <td style="text-align: center;">
                                存放列数
                            </td>
                            <td style="text-align: center;">
                                存放层数
                            </td>
                            <td style="text-align: center;">
                                气体类型
                            </td>
                            <td style="text-align: center;">
                                钢瓶状态
                            </td>
                            <td style="text-align: center;">
                                存放说明
                            </td>
                        </tr>
                    </thead>
                    <tbody>
                        <asp:Repeater ID="rp_Item" runat="server" OnItemCommand="rp_Item_ItemCommand">
                            <ItemTemplate>
                                <tr>
                                    <td style="text-align: center;">
                                        <asp:LinkButton ID="LBtnDel" runat="server" OnClientClick="selectrow(this)" CommandArgument='<%#Eval("OID") %>'
                                            CommandName="del"><%#Eval("CarcaseCode")%></asp:LinkButton>
                                    </td>
                                    <td style="text-align: center;">
                                        <%#Eval("PositionName")%>
                                    </td>
                                    <td style="text-align: center;">
                                        <%#Eval("DepositPositionName")%>
                                    </td>
                                    <td style="text-align: center;">
                                        <%#Eval("DepositColumnCount")%>
                                    </td>
                                    <td style="text-align: center;">
                                        <%#Eval("DepositRowCount")%>
                                    </td>
                                    <td style="text-align: center;">
                                        <%#Eval("GasTypeName")%>
                                    </td>
                                    <td style="text-align: center;">
                                        <%#Eval("CylinderStatusName")%>
                                    </td>
                                    <td style="text-align: center;">
                                        <%#Eval("DepositDesc")%>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </tbody>
                </table>
            </div>
            <uc1:PageControl ID="PageControl1" runat="server" />
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnAdd" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>
    </form>
</body>
</html>

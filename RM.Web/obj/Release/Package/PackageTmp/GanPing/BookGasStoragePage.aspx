﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BookGasStoragePage.aspx.cs"
    Inherits="RM.Web.GanPing.BookGasStoragePage"  EnableEventValidation ="false"  ClientIDMode="AutoID"%>

<%@ Register Src="~/UserControl/PageControl.ascx" TagName="PageControl" TagPrefix="uc1" %>
<%@ Register Src="~/UserControl/LoadButton.ascx" TagName="LoadButton" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>气体库存管理</title>
    <link href="/Themes/Styles/Site.css" rel="stylesheet" type="text/css" />
    <script src="/Themes/Scripts/Validator/JValidator.js" type="text/javascript"></script>
    <script src="/Themes/Scripts/jquery-1.8.2.min.js" type="text/javascript"></script>
    <script src="/Themes/Scripts/jquery.pullbox.js" type="text/javascript"></script>
    <script src="/Themes/Scripts/FunctionJS.js" type="text/javascript"></script>
    <script type="text/javascript">
        //添加
        function add() {
            document.getElementById('<%= this.btnAdd.ClientID %>').click();
        }
        //提交
        function Submit() {
            document.getElementById('<%= this.btnSubmit.ClientID %>').click();
        }
        //查询
        function search() {

            document.getElementById('<%= this.btnSearch.ClientID %>').click();
        }
        $(function () {
            InitControl();
        })

        function InitControl() {
            $(".div-body").PullBox({ dv: $(".div-body"), obj: $("#table1").find("tr") });
            divresize(80);
        }
        //保存
        function SaveForm() {
            document.getElementById('<%= this.btnSave.ClientID %>').click();
        }

        //对账复核
        function ReCheck() {
            document.getElementById('<%= this.btnReCheck.ClientID %>').click();
        }
        //计算库存
        function Calculate() {
            document.getElementById('<%= this.btnCalculate.ClientID %>').click();
        }
        //取消对账复核
        function CancelRecheck() {
            document.getElementById('<%= this.btnCancelRecheck.ClientID %>').click();
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional">
        <ContentTemplate>
            <table width="100%">
                <colgroup>
                    <col width="10%" />
                    <col width="10%" />
                    <col width="10%" />
                    <col width="10%" />
                    <col width="10%" />
                    <col width="10%" />
                    <col width="10%" />
                    <col width="10%" />
                    <col width="10%" />
                    <col width="10%" />
                </colgroup>
                <tr>
                    <td class="inner_cell_right">
                        年度：
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlYear" runat="server" Enabled="false">
                        </asp:DropDownList>
                    </td>
                    <td class="inner_cell_right">
                        月度：
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlMonth" runat="server" Enabled="false" />
                    </td>
                    <td class="inner_cell_right">
                        回收气库存：
                    </td>
                    <td>
                        <asp:TextBox ID="txtCountRecycleGas" runat="server" />
                    </td>
                    <td class="inner_cell_right">
                        待检气库存：
                    </td>
                    <td>
                        <asp:TextBox ID="txtCountWaitingGas" runat="server" />
                    </td>
                    <td class="inner_cell_right">
                        合格气库：
                    </td>
                    <td>
                        <asp:TextBox ID="txtCountPassGas" runat="server" />
                    </td>
                </tr>
            </table>
            <div style="text-align: left;">
                <uc2:LoadButton ID="LoadButton1" runat="server" />
            </div>
            <div style="text-align: right; display: none;">
                <asp:Button ID="btnSubmit" runat="server" OnClick="btnSubmit_Click" Text="btnSubmit_Click" />
                <asp:Button ID="btnSearch" runat="server" OnClick="btnSearch_Click" Text="btnSearch_Click" />
                <asp:Button ID="btnAdd" runat="server" OnClick="btnAdd_Click" Text="btnAdd_Click" />
                <asp:Button ID="btnSave" runat="server" OnClick="btnSave_Click" Text="btnSave_Click"
                    OnClientClick="return CheckDataValid('#form1');" />
                <asp:Button ID="btnReCheck" runat="server" Text="btnReCheck" OnClick="btnReCheck_Click" />
                <asp:Button ID="btnCalculate" runat="server" Text="btnCalculate" OnClick="btnCalculate_Click" />
                <asp:Button ID="btnCancelRecheck" runat="server" Text="btnCancelRecheck" OnClick="btnCancelRecheck_Click" />
            </div>
            <div class="div-body">
                <table id="table1" class="grid" singleselect="true">
                    <colgroup>
                        <col width="11%" />
                        <col width="11%" />
                        <col width="11%" />
                        <col width="11%" />
                        <col width="11%" />
                        <col width="11%" />
                        <col width="11%" />
                        <col width="11%" />
                    </colgroup>
                    <thead>
                        <tr>
                            <%-- <td style="width: 20px; text-align: left;">
                        <label id="checkAllOff" onclick="CheckAllLine()" title="全选">
                            &nbsp;</label>
                    </td>--%>
                            <td style="text-align: center;">
                                ID
                            </td>
                            <td style="text-align: center;">
                                年份
                            </td>
                            <td style="text-align: center;">
                                月份
                            </td>
                            <td style="text-align: center;">
                                对账人ID
                            </td>
                            <td style="text-align: center;">
                                回收气库存
                            </td>
                            <td style="text-align: center;">
                                待检气库存
                            </td>
                            <td style="text-align: center;">
                                合格气库存
                            </td>
                            <td style="text-align: center;">
                                对账状态
                            </td>
                        </tr>
                    </thead>
                    <tbody>
                        <asp:Repeater ID="rp_Item" runat="server" OnItemDataBound="rp_ItemDataBound" OnItemCommand="rp_Item_ItemCommand">
                            <ItemTemplate>
                                <tr>
                                    <%--<td style="width: 20px; text-align: left;">
                                <input type="checkbox" value="<%#Eval("OID")%>" name="checkbox" />
                            </td>--%>
                                    <td style="text-align: center;">
                                        <asp:LinkButton ID="LBtnDel" runat="server" OnClientClick="selectrow(this)" CommandArgument='<%#Eval("OID") %>' CommandName="del"><%#Eval("OID") %></asp:LinkButton>
                                    </td>
                                    <td style="text-align: center;">
                                        <%#Eval("Year")%>
                                    </td>
                                    <td style="text-align: center;">
                                        <%#Eval("Month")%>
                                    </td>
                                    <td style="text-align: center;">
                                        <asp:Label ID="lblUserID" runat="server" Text='<%#Eval("CheckOID")%>'></asp:Label>
                                    </td>
                                    <td style="text-align: center;">
                                        <%#Eval("CountRecycleGas")%>
                                    </td>
                                    <td style="text-align: center;">
                                        <%#Eval("CountWaitingGas")%>
                                    </td>
                                    <td style="text-align: center;">
                                        <%#Eval("CountPassGas")%>
                                    </td>
                                    <td style="text-align: center;">
                                        <%# DictEGMNS["DZStatus"][Eval("CheckStatus").ToString()]%>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </tbody>
                </table>
            </div>
            <uc1:PageControl ID="PageControl1" runat="server" />
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnAdd" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>
    </form>
</body>
</html>

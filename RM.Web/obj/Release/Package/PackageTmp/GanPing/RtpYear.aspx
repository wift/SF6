﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RtpYear.aspx.cs" Inherits="RM.Web.GanPing.RtpYear" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>年度统计表</title>
    <link href="~/Themes/Styles/Site.css" rel="stylesheet" type="text/css" />
    <script src="/Themes/Scripts/Validator/JValidator.js" type="text/javascript"></script>
    <script src="/Themes/Scripts/jquery-1.8.2.min.js" type="text/javascript"></script>
    <script src="/Themes/Scripts/jquery.pullbox.js" type="text/javascript"></script>
    <script src="/Themes/Scripts/FunctionJS.js" type="text/javascript"></script>
    <script src="/Themes/Scripts/DatePicker/WdatePicker.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {
            InitControl();
        })
        function InitControl() {
            $(".div-body").PullBox({ dv: $(".div-body"), obj: $("#table1").find("tr") });
            FixedTableHeader("#table1", $(window).height() - 91);
            divresize(60);
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <contenttemplate>
            <table width="100%">
                <colgroup>
                    <col width="10%" />
                    <col width="10%" />
                    <col width="10%" />
                    <col width="10%" />
                    <col width="10%" />
                    <col width="10%" />
                    <col width="10%" />
                    <col width="10%" />
                    <col width="20%" />
                </colgroup>
                <tr>
                    <td class="inner_cell_right">
                        年度:
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlBeginYear" runat="server">
                        </asp:DropDownList>
                    </td>
                    <td class="inner_cell_right">
                        至:
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlEndYear" runat="server">
                        </asp:DropDownList>
                    </td>
                    <td class="inner_cell_right">
                        供电局名称：
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlPowerSupplyName" runat="server" Width="100%" AutoPostBack="true"
                            OnSelectedIndexChanged="ddlPowerSupplyName_SelectedIndexChanged" CssClass="select">
                        </asp:DropDownList>
                    </td>
                    <td class="inner_cell_right">
                        变电站名称：
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlConvertStationName" runat="server" Width="100% " CssClass="select"
                            checkexpession="NotNull" datacol="yes" err="此项">
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:Button ID="btnSearch" runat="server" OnClick="btnSearch_Click" Text="查询" />
                        <asp:Button ID="btnExport" runat="server" Text="导出" OnClick="btnExport_Click" />
                    </td>
                </tr>
            </table>
            <div class="div-body" style="overflow-y: hidden;">
                <table id="table1" class="grid" singleselect="true">
                    <colgroup>
                        <col width="20%" />
                        <col width="20%" />
                        <col width="20%" />
                        <col width="20%" />
                       <%-- <col width="20%" />--%>
                    </colgroup>
                    <thead>
                        <tr>
                            <td style="text-align: center;">
                                年份
                            </td>
                            <td style="text-align: center;">
                                回收气量（kg）
                            </td>
                            <td style="text-align: center;">
                                需气量（kg）
                            </td>
                            <td style="text-align: center;">
                                发送气量（kg）
                            </td>
                         <%--   <td style="text-align: center;">
                                补换气量（kg）
                            </td>--%>
                        </tr>
                    </thead>
                    <tbody>
                        <asp:Repeater ID="rp_Item" runat="server">
                            <ItemTemplate>
                                <tr>
                                    <td style="text-align: center;">
                                        <%#Eval("年份")%>
                                    </td>
                                    <td style="text-align: center;">
                                        <%#Eval("a")%>
                                    </td>
                                    <td style="text-align: center;">
                                        <%#Eval("b")%>
                                    </td>
                                    <td style="text-align: center;">
                                        <%#Eval("c")%>
                                    </td>
                                 <%--   <td style="text-align: center;">
                                        <%#Eval("d")%>
                                    </td>--%>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </tbody>
                </table>
            </div>
        </contenttemplate>
    </form>
</body>
</html>

﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="InfoDevChangeFillPage.aspx.cs"
    Inherits="RM.Web.GanPing.InfoDevChangeFillPage" EnableEventValidation="false"
    ClientIDMode="AutoID" %>

<%@ Register Src="~/UserControl/PageControl.ascx" TagName="PageControl" TagPrefix="uc1" %>
<%@ Register Src="~/UserControl/LoadButton.ascx" TagName="LoadButton" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>检修用气登记</title>
    <link href="/Themes/Styles/Site.css" rel="stylesheet" type="text/css" />
    <script src="/Themes/Scripts/Validator/JValidator.js" type="text/javascript"></script>
    <script src="/Themes/Scripts/jquery-1.8.2.min.js" type="text/javascript"></script>
    <script src="/Themes/Scripts/jquery.pullbox.js" type="text/javascript"></script>
    <script src="/Themes/Scripts/FunctionJS.js" type="text/javascript"></script>
    <script src="/Themes/Scripts/DatePicker/WdatePicker.js" type="text/javascript"></script>
    <script type="text/javascript">
        //添加
        function add() {
            document.getElementById('<%= this.btnAdd.ClientID %>').click();
        }
        //查询
        function search() {

            document.getElementById('<%= this.btnSearch.ClientID %>').click();
        }
        //删除
        function Delete() {
            showConfirmMsg("此操作不可恢复，您确定要删除吗？", function (r) {
                if (r) {
                    document.getElementById('<%= this.btnDel.ClientID %>').click();
                }
            });
        }
        //保存
        function SaveForm() {
            document.getElementById('<%= this.btnSave.ClientID %>').click();
        }
        //提交
        function Submit() {
            document.getElementById('<%= this.btnSubmit.ClientID %>').click();
        }


        $(function () {
            InitControl();
        })
        function InitControl() {
            $(".div-body").PullBox({ dv: $(".div-body"), obj: $("#table1").find("tr") });
            divresize(210);
        }
        //确认
        function ComFirm() {
            document.getElementById('<%= this.btnConfirm.ClientID %>').click();
        }

        var objDev = {};
        //var devcode = '';
        //var devname = '';
        function ShowSelectDialog() {
            var $ddlsConvertStationCode = $("#ddlConvertStationName").val();
            var url = "/GanPing/SelectSBDialog.aspx?ConvertStationCode=" + $ddlsConvertStationCode;
            // returnValue = $ddlsConvertStationCode;
            top.openDialog(url, 'SelectSBDialog', '详细信息 - 查看', 1000, 500, 50, 50, function myfunction() {
                // alert(devcode);
                $("#txtDevCode").val(objDev.devcode);
                $("#txtDevName").val(objDev.devname);
                $("#txtDevType").val(objDev.DevType);
                $("#txtRatedQL").val(objDev.RatedQL);
                $("#txtRatedQY").val(objDev.RatedQY);
                

            });


        }

    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional">
        <ContentTemplate>
            <table width="100%">
                <colgroup>
                    <col width="8%" />
                    <col width="11%" />
                    <col width="16%" />
                    <col width="11%" />
                    <col width="16%" />
                    <col width="11%" />
                    <col width="8%" />
                    <col width="11%" />
                </colgroup>
                <tr>
                    <td class="inner_cell_right">
                        供电局名称：
                    </td>
                    <td>
                        <asp:TextBox ID="txtPowerSupplyName" runat="server" Enabled="false" />
                    </td>
                    <td class="inner_cell_right">
                        变电站名称：
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlConvertStationName" runat="server" Width="100%" AutoPostBack="true"
                             CssClass="select"
                            checkexpession="NotNull" datacol="yes" err="此项">
                        </asp:DropDownList>
                    </td>
                    <td class="inner_cell_right">
                        钢瓶编码：
                    </td>
                    <td>
                        <asp:TextBox ID="txtCylinderCode" runat="server" checkexpession="NotNull" datacol="yes"
                            err="此项" />
                    </td>
                    <td class="inner_cell_right">
                        气体编码：
                    </td>
                    <td>
                        <asp:TextBox ID="txtGasCode" runat="server" checkexpession="NotNull" datacol="yes"
                            err="此项" />
                    </td>
                </tr>
                <tr>
                    <td class="inner_cell_right">
                        设备编码：
                    </td>
                    <td>
                           <asp:TextBox ID="txtDevCode" runat="server" Width="60%" checkexpession="NotNull" datacol="yes"
                            err="此项" />
                        <input id="selectDevcode" value="选择"  type="button" onclick="ShowSelectDialog()" />
                    </td>
                    <td class="inner_cell_right">
                        设备名称：
                    </td>
                    <td>
                        <asp:TextBox ID="txtDevName" runat="server" checkexpession="NotNull" datacol="yes"
                            err="此项" />
                    </td>
                    <td class="inner_cell_right">
                        设备型号：
                    </td>
                    <td>
                        <asp:TextBox ID="txtDevType" runat="server" checkexpession="NotNull" datacol="yes"
                            err="此项" />
                    </td>
                    <td class="inner_cell_right">
                        操作人员：
                    </td>
                    <td>
                        <asp:TextBox ID="txtOperataff" runat="server" checkexpession="NotNull" datacol="yes"
                            err="此项" />
                    </td>
                </tr>
                <tr>
                    <td class="inner_cell_right">
                        额定气量（kg）：
                    </td>
                    <td>
                        <asp:TextBox ID="txtRatedQL" runat="server" checkexpession="NotNull" datacol="yes"
                            err="此项" />
                    </td>
                    <td class="inner_cell_right">
                        额定气压（Mpa）：
                    </td>
                    <td>
                        <asp:TextBox ID="txtRatedQY" runat="server" checkexpession="NotNull" datacol="yes"
                            err="此项" />
                    </td>
                    <td class="inner_cell_right">
                        补换气量（kg）：
                    </td>
                    <td>
                        <asp:TextBox ID="txtAmountFillGas" runat="server" checkexpession="NotNull" datacol="yes"
                            err="此项" />
                    </td>
                    <td class="inner_cell_right">
                        补换气日期：
                    </td>
                    <td>
                        <asp:TextBox ID="txtFillGasdate" runat="server" onfocus="WdatePicker()" checkexpession="NotNull"
                            datacol="yes" err="此项" />
                    </td>
                </tr>
                <tr>
                    <td class="inner_cell_right">
                        补换气前压力(Mpa)：
                    </td>
                    <td>
                        <asp:TextBox ID="txtAmountPressFillGassbefore" runat="server" checkexpession="NotNull"
                            datacol="yes" err="此项" />
                    </td>
                    <td class="inner_cell_right">
                        补换气后压力(Mpa)：
                    </td>
                    <td>
                        <asp:TextBox ID="txtAmountPressFillGassafter" runat="server" checkexpession="NotNull"
                            datacol="yes" err="此项" />
                    </td>
                    <td class="inner_cell_right">
                        气体用途：
                    </td>
                    <td>
                        <asp:TextBox ID="txtGasUsing" runat="server" checkexpession="NotNull" datacol="yes"
                            err="此项" />
                    </td>
                    <td class="inner_cell_right">
                        备注说明：
                    </td>
                    <td>
                        <asp:TextBox ID="txtRemarksDesc" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="inner_cell_right">
                        登记人：
                    </td>
                    <td>
                        <asp:TextBox ID="txtRegistantsOID" runat="server" />
                    </td>
                    <td class="inner_cell_right">
                        登记日期：
                    </td>
                    <td>
                        <asp:TextBox ID="txtRegistantsDate" runat="server" />
                    </td>
                    <td class="inner_cell_right">
                        状态：
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlStatus" runat="server" Width="100%" Enabled="false">
                        </asp:DropDownList>
                    </td>
                </tr>
            </table>
            <div style="text-align: left;">
                <uc2:LoadButton ID="LoadButton1" runat="server" />
            </div>
            <div style="text-align: right; display: none;">
                <asp:Button ID="btnSubmit" runat="server" OnClick="btnSubmit_Click" Text="btnSubmit_Click" />
                <asp:Button ID="btnSearch" runat="server" OnClick="btnSearch_Click" Text="btnSearch_Click" />
                <asp:Button ID="btnSave" runat="server" OnClick="btnSave_Click" Text="btnSave_Click"
                    OnClientClick="return CheckDataValid('#form1');" />
                <asp:Button ID="btnAdd" runat="server" OnClick="btnAdd_Click" Text="btnAdd_Click" />
                <asp:Button ID="btnDel" runat="server" OnClick="btnDel_Click" Text="btnDel_Click" />
                <asp:Button ID="btnConfirm" runat="server" OnClick="btnConfirm_Click" />
            </div>
            <div class="div-body">
                <table id="table1" class="grid" singleselect="true">
                    <colgroup>
                        <col width="10%" />
                        <col width="10%" />
                        <col width="10%" />
                        <col width="10%" />
                        <col width="10%" />
                        <col width="10%" />
                        <col width="10%" />
                        <col width="10%" />
                        <col width="10%" />
                        <col width="10%" />
                    </colgroup>
                    <thead>
                        <tr>
                            <%-- <td style="width: 20px; text-align: left;">
                        <label id="checkAllOff" onclick="CheckAllLine()" title="全选">
                            &nbsp;</label>
                    </td>--%>
                            <td style="text-align: center;">
                                供电局名称
                            </td>
                            <td style="text-align: center;">
                                变电站名称
                            </td>
                            <td style="text-align: center;">
                                设备编码
                            </td>
                            <td style="text-align: center;">
                                设备名称
                            </td>
                            <td style="text-align: center;">
                                补换气量
                            </td>
                            <td style="text-align: center;">
                                补换气日期
                            </td>
                            <td style="text-align: center;">
                                操作人员
                            </td>
                            <td style="text-align: center;">
                                登记人
                            </td>
                            <td style="text-align: center;">
                                登记日期
                            </td>
                            <td style="text-align: center;">
                                状态
                            </td>
                        </tr>
                    </thead>
                    <tbody>
                        <asp:Repeater ID="rp_Item" runat="server" OnItemDataBound="rp_ItemDataBound" OnItemCommand="rp_Item_ItemCommand">
                            <ItemTemplate>
                                <tr>
                                    <td style="text-align: center;">
                                        <%#Eval("PowerSupplyName")%>
                                    </td>
                                    <td style="text-align: center;">
                                        <%#Eval("ConvertStationName")%>
                                    </td>
                                    <td style="text-align: center;">
                                        <%#Eval("DevCode")%>
                                    </td>
                                    <td style="text-align: center;">
                                        <asp:LinkButton ID="LinkButton1" runat="server" OnClientClick="selectrow(this)" CommandArgument='<%#Eval("OID") %>'
                                            CommandName="del"><%#Eval("DevName")%></asp:LinkButton>
                                    </td>
                                    <td style="text-align: center;">
                                        <%#Eval("AmountFillGas")%>
                                    </td>
                                    <td style="text-align: center;">
                                        <%#Eval("FillGasdate", "{0:yyyy-MM-dd}")%>
                                    </td>
                                    <td style="text-align: center;">
                                        <%#Eval("Operataff")%>
                                    </td>
                                    <td style="text-align: center;">
                                        <asp:Label ID="lblRegistantsOID" runat="server" Text='<%#Eval("RegistantsOID")%>'></asp:Label>
                                    </td>
                                    <td style="text-align: center;">
                                        <%#Eval("RegistantsDate", "{0:yyyy-MM-dd}")%>
                                    </td>
                                    <td style="text-align: center;">
                                        <%# DictEGMNS["CFStatus"][Eval("Status").ToString()]%>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </tbody>
                </table>
            </div>
            <uc1:PageControl ID="PageControl1" runat="server" />
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="ddlConvertStationName" EventName="SelectedIndexChanged" />
        
            <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>
    </form>
</body>
</html>

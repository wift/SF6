﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ApplRegGasQtyInspectionPage.aspx.cs"
    Inherits="RM.Web.GanPing.ApplRegGasQtyInspectionPage" EnableEventValidation="false"
    ClientIDMode="AutoID" %>

<%@ Register Src="~/UserControl/PageControl.ascx" TagName="PageControl" TagPrefix="uc1" %>
<%@ Register Src="~/UserControl/LoadButton.ascx" TagName="LoadButton" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>气体检测任务</title>
    <link href="/Themes/Styles/Site.css" rel="stylesheet" type="text/css" />
    <script src="/Themes/Scripts/Validator/JValidator.js" type="text/javascript"></script>
    <script src="/Themes/Scripts/jquery-1.8.2.min.js" type="text/javascript"></script>
    <script src="/Themes/Scripts/jquery.pullbox.js" type="text/javascript"></script>
    <script src="/Themes/Scripts/FunctionJS.js" type="text/javascript"></script>
    <script src="/Themes/Scripts/DatePicker/WdatePicker.js" type="text/javascript"></script>
    <style type="text/css">
        .clear {
            background: none;
            border: 0;
            clear: both;
            display: block;
            float: none;
            font-size: 0;
            margin: 0;
            padding: 0;
            overflow: hidden;
            visibility: hidden;
            width: 60px;
            height: 0;
        }

    </style>
    <script type="text/javascript">
        //添加
        function add() {
            document.getElementById('<%= this.btnAdd.ClientID %>').click();
        }
        //查询
        function search() {

            document.getElementById('<%= this.btnSearch.ClientID %>').click();
        }
        //删除
        function Delete() {
            showConfirmMsg("此操作不可恢复，您确定要删除吗？", function (r) {
                if (r) {
                    document.getElementById('<%= this.btnDel.ClientID %>').click();
                    showTipsMsg("删除成功！", 2000, 4);
                }
            });
        }
        //保存
        function SaveForm() {
            document.getElementById('<%= this.btnSave.ClientID %>').click();
        }
        //提交
        function Submit() {
            document.getElementById('<%= this.btnSubmit.ClientID %>').click();
        }

        function ShowDialog(key) {
            var url = "/GanPing/ShowDetailsInspectionBox.aspx?key=" + key;
            top.openDialog(url, 'ShowDetailsInspectionBox', '详细信息 - 查看', 1000, 400, 50, 50);

        }

        $(function () {
            InitControl();
        })
        function InitControl() {
            // $(".div-body").PullBox({ dv: $(".div-body"), obj: $("#table1").find("tr") });
            divresize(260);
            FixedTableHeader("#table1", $(window).height() - 200);
        }



            function CalculateIndex() {
                if ($("#txtCylinderCode").val() == "") {
                    alert("请选择一个钢瓶！");
                    return;
                }

                var url = "/GanPing/CalculateIndexPopuPage.aspx";
                top.openDialog(url, 'CalculateIndexPopuPage', '指标计算', 1000, 500, 50, 50);
            }

            function Reject() {
                document.getElementById('<%= this.btnReject.ClientID %>').click();
            }


            function ExportXls() {
                var key = CheckboxValue();
                document.getElementById('<%= this.HiddenField1.ClientID %>').value = key.toString();
            document.getElementById('<%= this.btnExport.ClientID %>').click();


        }
        function execFunc() {
            window.windowload();
        }
        //合格证打印
        function PassPrint() {
            var key = CheckboxValue();
            document.getElementById('<%= this.HiddenField1.ClientID %>').value = key.toString();
            document.getElementById('<%= this.btnPassPrint.ClientID %>').click();
        }

        function Next(o) {

            if (o.value.length == 8) {

                var str = o.name;
                var intvalue = str[7];

                var nextID = str.substring(0, 7) + (parseInt(intvalue) + 1).toString();
                $('#' + nextID).focus().select();

            }

        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:HiddenField ID="HiddenField1" runat="server" />
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional">
            <ContentTemplate>
                <!-- Add.aspx -->
                <table width="100%" id="pipetable">
                    <colgroup>
                        <col width="4%" />
                        <col width="5%" />
                        <col width="16%" />
                           <col width="4%" />
                        <col width="5%" />
                        <col width="16%" />
                        <col width="4%" />
                        <col width="6%" />
                        <col width="16%" />
                        <col width="4%" />
                        <col width="6%" />
                        <col width="16%" />
                    </colgroup>
                    <tr>
                        <td class="inner_cell_right">一管道:
                        </td>
                        <td class="style1">
                            <input id="txtPipe1" type="text" runat="server" checkexpession="NotNull" datacol="no"
                                err="此项" onkeyup="Next(this)" style="width: 60px" />
                        </td>
                        <td class="inner_cell_left">
                            <asp:Button ID="btnPipe1Save" runat="server" Text="保存" OnClick="btnPipe1Save_Click"
                                UseSubmitBehavior="false" />
                            <asp:Button ID="btnPipe1Clear" runat="server" Text="清除" OnClick="btnPipe1Clear_Click"
                                UseSubmitBehavior="false" />
                            <asp:Button ID="btnPipe1Checked" runat="server" Text="已检测"
                                UseSubmitBehavior="false" OnClick="btnPipe1Checked_Click" />
                        </td>
                        <td class="inner_cell_right">二管道:
                        </td>
                        <td class="style1">
                            <input id="txtPipe2" type="text" runat="server" checkexpession="NotNull" datacol="no"
                                err="此项" onkeyup="Next(this)" style="width: 60px" />
                        </td>
                        <td class="inner_cell_left">
                            <asp:Button ID="btnPipe2Save" runat="server" Text="保存" OnClick="btnPipe2Save_Click"
                                UseSubmitBehavior="false" />
                            <asp:Button ID="btnPipe2Clear" runat="server" Text="清除" OnClick="btnPipe2Clear_Click"
                                UseSubmitBehavior="false" />
                            <asp:Button ID="btnPipe2Checked" runat="server" Text="已检测"
                                UseSubmitBehavior="false" OnClick="btnPipe2Checked_Click" />
                        </td>
                        <td class="inner_cell_right">三管道:
                        </td>
                        <td class="style1">
                            <input id="txtPipe3" type="text" runat="server" checkexpession="NotNull" datacol="no"
                                err="此项" onkeyup="Next(this)" style="width: 60px" />
                        </td>
                        <td class="inner_cell_left">
                            <asp:Button ID="btnPipe3Save" runat="server" Text="保存" OnClick="btnPipe3Save_Click"
                                UseSubmitBehavior="false" />
                            <asp:Button ID="btnPipe3Clear" runat="server" Text="清除" OnClick="btnPipe3Clear_Click"
                                UseSubmitBehavior="false" />
                            <asp:Button ID="btnPipe3Checked" runat="server" Text="已检测"
                                UseSubmitBehavior="false" OnClick="btnPipe3Checked_Click" />
                        </td>
                        <td class="inner_cell_right">四管道:
                        </td>
                        <td class="style1">
                            <input id="txtPipe4" type="text" runat="server" checkexpession="NotNull" datacol="no"
                                err="此项" onkeyup="Next(this)" style="width: 60px" />
                        </td>
                        <td class="inner_cell_left">
                            <asp:Button ID="btnPipe4Save" runat="server" Text="保存" OnClick="btnPipe4Save_Click"
                                UseSubmitBehavior="false" />
                            <asp:Button ID="btnPipe4Clear" runat="server" Text="清除" OnClick="btnPipe4Clear_Click"
                                UseSubmitBehavior="false" />
                            <asp:Button ID="btnPipe4Checked" runat="server" Text="已检测"
                                UseSubmitBehavior="false" OnClick="btnPipe4Checked_Click" />
                        </td>
                    </tr>
                    <tr>
                        <td class="inner_cell_right">五管道:
                        </td>
                        <td>
                            <input id="txtPipe5" type="text" runat="server" checkexpession="NotNull" datacol="no"
                                err="此项" onkeyup="Next(this)" style="width: 60px" />
                        </td>
                        <td class="inner_cell_left">
                            <asp:Button ID="btnPipe5Save" runat="server" Text="保存" OnClick="btnPipe5Save_Click"
                                UseSubmitBehavior="false" />
                            <asp:Button ID="btnPipe5Clear" runat="server" Text="清除" OnClick="btnPipe5Clear_Click"
                                UseSubmitBehavior="false" />
                            <asp:Button ID="btnPipe5Checked" runat="server" Text="已检测"
                                UseSubmitBehavior="false" OnClick="btnPipe5Checked_Click" />
                        </td>
                        <td class="inner_cell_right">六管道:
                        </td>
                        <td>
                            <input name="txtPipe6" type="text" id="txtPipe6" runat="server" checkexpession="NotNull"
                                datacol="no" err="此项" onkeyup="Next(this)" style="width: 60px" />
                        </td>
                        <td class="inner_cell_left">
                            <asp:Button ID="btnPipe6Save" runat="server" Text="保存" OnClick="btnPipe6Save_Click"
                                UseSubmitBehavior="false" />
                            <asp:Button ID="btnPipe6Clear" runat="server" Text="清除" OnClick="btnPipe6Clear_Click"
                                UseSubmitBehavior="false" />
                            <asp:Button ID="btnPipe6Checked" runat="server" Text="已检测"
                                UseSubmitBehavior="false" OnClick="btnPipe6Checked_Click" />
                        </td>
                        <td class="inner_cell_right">七管道:
                        </td>
                        <td>
                            <input name="txtPipe7" type="text" id="txtPipe7" runat="server" checkexpession="NotNull"
                                datacol="no" err="此项" onkeyup="Next(this)" style="width: 60px" />
                        </td>
                        <td class="inner_cell_left">
                            <asp:Button ID="btnPipe7Save" runat="server" Text="保存" OnClick="btnPipe7Save_Click"
                                UseSubmitBehavior="false" />
                            <asp:Button ID="btnPipe7Clear" runat="server" Text="清除" OnClick="btnPipe7Clear_Click"
                                UseSubmitBehavior="false" />
                            <asp:Button ID="btnPipe7Checked" runat="server" Text="已检测"
                                UseSubmitBehavior="false" OnClick="btnPipe7Checked_Click" />
                        </td>
                        <td class="inner_cell_right">八管道:
                        </td>
                        <td>
                            <input name="txtPipe8" type="text" id="txtPipe8" runat="server" checkexpession="NotNull"
                                datacol="no" err="此项" onkeyup="Next(this)" style="width: 60px" />
                        </td>
                        <td class="inner_cell_left">
                            <asp:Button ID="btnPipe8Pipe" runat="server" Text="保存" OnClick="btnPipe8Pipe_Click"
                                UseSubmitBehavior="false" />
                            <asp:Button ID="btnPipe8Clear" runat="server" Text="清除" OnClick="btnPipe8Clear_Click"
                                UseSubmitBehavior="false" />
                            <asp:Button ID="btnPipe8Checked" runat="server" Text="已检测"
                                UseSubmitBehavior="false" OnClick="btnPipe8Checked_Click" />
                        </td>
                    </tr>
                </table>
                <hr />
                <table>
                    <colgroup>
                        <col width="10%" />
                        <col width="10%" />
                        <col width="10%" />
                        <col width="10%" />
                        <col width="60%" />
                        <tr>
                            <td class="inner_cell_right">钢瓶编码：
                            </td>
                            <td>
                                <input id="txtCylinderCode" type="text" runat="server" checkexpession="NotNull" datacol="yes"
                                    err="此项" disabled="disabled" />
                            </td>
                            <td class="inner_cell_right">业务编号：
                            </td>
                            <td>
                                <input type="text" id="txtBusinessCode" runat="server" disabled="disabled" />
                            </td>
                            <td></td>
                        </tr>
                    </colgroup>
                </table>
                <div style="float: right;">
                    <asp:Button ID="btnInNew" runat="server" Text="入网新气" CssClass="button" OnClick="btnInNew_Click"
                        UseSubmitBehavior="false" /><asp:Button ID="btnReJHGas" runat="server" Text="回收净化气"
                            CssClass="button" OnClick="btnReJHGas_Click" UseSubmitBehavior="false" /><asp:Button
                                ID="btnNewBuy" runat="server" Text="新采购气" CssClass="button" OnClick="btnNewBuy_Click"
                                UseSubmitBehavior="false" />
                </div>
                <div style="text-align: left; margin: 0,0,0,0; height: auto;">
                    <uc2:LoadButton ID="LoadButton1" runat="server" />
                </div>
                <div style="display: none;">
                    <asp:Button ID="btnSubmit" runat="server" OnClick="btnSubmit_Click" />
                    <asp:Button ID="btnSearch" runat="server" OnClick="btnSearch_Click" Text="btnSearch_Click" />
                    <asp:Button ID="btnSave" runat="server" OnClick="btnSave_Click" Text="btnSave_Click"
                        OnClientClick="return CheckDataValid('#form1');" />
                    <asp:Button ID="btnAdd" runat="server" OnClick="btnAdd_Click" Text="btnAdd_Click" />
                    <asp:Button ID="btnDel" runat="server" OnClick="btnDel_Click" Text="btnDel_Click" />
                    <asp:Button ID="btnReject" runat="server" OnClick="btnReject_Click" />
                    <asp:Button ID="btnPassPrint" runat="server" OnClick="btnPassPrint_Click" />
                </div>
                <div class="div-body" style="margin-top: 5px">
                    <table id="table1" class="grid">
                        <colgroup>
                            <col width="2%" />
                            <col width="7%" />
                            <col width="7%" />
                            <col width="10%" />
                            <col width="5%" />
                            <col width="8%" />
                            <col width="8%" />
                            <col width="8%" />
                            <col width="8%" />
                            <col width="5%" />
                            <col width="5%" />
                            <col width="7%" />
                            <col width="8%" />
                            <col width="5%" />
                        </colgroup>
                        <thead>
                            <tr>
                                <td style="text-align: left;">
                                    <label id="checkAllOff" onclick="CheckAllLine()" title="全选">
                                        &nbsp;</label>
                                </td>
                                <td style="text-align: center;">钢瓶编码
                                </td>
                                <td style="text-align: center;">钢瓶钢印号
                                </td>
                                <td style="text-align: center;">业务编号
                                </td>
                                <td style="text-align: center;">检测项目
                                </td>
                                <td style="text-align: center;">气体来源
                                </td>
                                <td style="text-align: center;">报告编号
                                </td>
                                <td style="text-align: center;">检测单号
                                </td>
                                <td style="text-align: center;">气体编码
                                </td>
                                <td style="text-align: center;">是否合格
                                </td>
                                <td style="text-align: center;">检测人
                                </td>
                                <td style="text-align: center;">检测日期
                                </td>
                                <td style="text-align: center;">申请日期
                                </td>
                                <td style="text-align: center;">流程状态
                                </td>
                            </tr>
                        </thead>
                        <tbody>
                            <asp:Repeater ID="rp_Item" runat="server" OnItemDataBound="rp_ItemDataBound" OnItemCommand="rp_Item_ItemCommand">
                                <ItemTemplate>
                                    <tr>
                                        <td style="text-align: center;">
                                            <input type="checkbox" value="<%#Eval("OID")%>" name="checkbox" />
                                        </td>
                                        <td style="text-align: center;">
                                            <asp:LinkButton ID="LBtnDel" runat="server" OnClientClick="selectrow(this)" CommandArgument='<%#Eval("OID") %>'
                                                CommandName="del"><%#Eval("CylinderCode")%></asp:LinkButton>
                                        </td>
                                        <td style="text-align: center;">
                                            <%#Eval("CylinderSealNo")%>
                                        </td>
                                        <td style="text-align: center;">
                                            <%#Eval("BusinessCode")%>
                                        </td>
                                        <td style="text-align: center;">
                                            <%# DictEGMNS["CheckPJTypeStatus"][Eval("CheckPJType").ToString()]%>
                                        </td>
                                        <td style="text-align: center;">
                                            <asp:Label ID="lblGasSourse" runat="server" Text=' <%# Eval("GasSourse")%>'></asp:Label>
                                        </td>
                                        <td style="text-align: center;">
                                            <%#Eval("ReportCode")%>
                                        </td>
                                        <td style="text-align: center;">
                                            <%#Eval("CheckCode")%>
                                        </td>
                                        <td style="text-align: center;">
                                            <a href="javascript:ShowDialog('<%#Eval("OID")%>');">
                                                <%#Eval("GasCdoe")%></a>
                                        </td>
                                        <td style="text-align: center;">
                                            <asp:Label ID="lblIsPass" runat="server" Text='<%# Eval("IsPass")%>'></asp:Label>
                                        </td>
                                        <td style="text-align: center;">
                                            <asp:Label ID="lblApplOID" runat="server" Text='<%#Eval("CheckOID")%>'></asp:Label>
                                        </td>
                                        <td style="text-align: center;">
                                            <%#Eval("CheckDate", "{0:yyyy-MM-dd}")%>
                                        </td>
                                        <td style="text-align: center;">
                                            <%#Eval("AppLDate", "{0:yyyy-MM-dd}")%>
                                        </td>
                                        <td style="text-align: center;">
                                            <%# FlowStatus[Eval("FlowStatus").ToString()]%>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                        </tbody>
                    </table>
                </div>
                <uc1:PageControl ID="PageControl1" runat="server" />
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="btnAdd" EventName="Click" />
            </Triggers>
        </asp:UpdatePanel>
        <div style="text-align: right; display: none;">
            <asp:Button ID="btnExport" runat="server" OnClick="btnExport_Click" Text="btnExport" />
        </div>
    </form>
</body>
</html>

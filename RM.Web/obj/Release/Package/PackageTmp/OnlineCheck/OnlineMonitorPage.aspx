﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="OnlineMonitorPage.aspx.cs"
    Inherits="RM.Web.OnlineCheck.OnlineMonitorPage" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>监测数据库</title>
    <%--    <link href="~/Themes/Styles/Site.css" rel="stylesheet" type="text/css" />--%>
    <script src="../Themes/Scripts/jquery-1.8.2.min.js" type="text/javascript"></script>
    <script src="../Themes/Scripts/jquery.pullbox.js" type="text/javascript"></script>
    <script src="../Themes/Scripts/FunctionJS.js" type="text/javascript"></script>
    <script src="../Themes/Scripts/DatePicker/WdatePicker.js" type="text/javascript"></script>
    <link href="../Themes/Scripts/jqueryeasyui/themes/bootstrap/easyui.css" rel="stylesheet" />
    <link href="../Themes/Scripts/jqueryeasyui/themes/icon.css" rel="stylesheet" />
    <style type="text/css">
        html, body {
            margin: 0;
            height: 100%;
            font-size: 9pt;
            overflow:hidden;
        }

        #containt, #rightPanel {
            height: 100%;
        }

        form {
            height: 100%;
        }

        #UpdatePanel {
            height: 100%;
        }

        #containt > #UpdatePanel > .Panel {
            float: left;
            height: -moz-calc(100% - 0px);
            height: -webkit-calc(100% - 00px);
            height: calc(100% - 0px);
        }

        .itemPanel {
            width: 170px;
            height: 170px;
            float: left;
            margin: 10px 10px 10px 10px;
            border-width: 1px;
            border-style: solid;
            cursor: pointer;
        }

            .itemPanel > div {
                float: left;
                height: -moz-calc(100% - 0px);
                height: -webkit-calc(100% - 00px);
                height: calc(100% - 0px);
            }

                .itemPanel > div > p {
                    margin-top: 0px;
                    margin-bottom: 0px;
                    font-size: 12px;
                }

        .leftItem {
            border-right-width: 1px;
            border-right-style: solid;
        }

        
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div id="containt">
            <div id="rightPanel" class="Panel" style="width: 99.5%;">
                <div>
                    <span style="float: left;">采集时间起:</span>
                    <div style="float: left;">
                        <input id="formdate" type="text" name="formdate" class="easyui-datebox" required="required"></input>
                    </div>
                    <span style="float: left;">采集时间止: </span>
                    <div style="float: left;">
                        <input id="todate" type="text" name="todate" class="easyui-datebox" required="required"></input>
                    </div>
                    <div style="float: left;">
                        <input id="btnRefreshList" type="button" value="查询" />
                    </div>
                </div>
                <div id="tt" class="easyui-tabs" style="width: 100%; height: 620px;">
                    <div title="组分数据采集日志" style="padding: 10px; width: 100%;">
                        <table id="Student_Table">
                        </table>
                          <br />
                    </div>
                    <div title="电气常量采集日志" style="padding: 10px; width: 100%;">
                        <table id="listEConstList">
                        </table>
                    </div>
                </div>
              
            </div>
        </div>
    </form>
    <script type="text/javascript" src="../Themes/Scripts/highcharts.js"></script>
    <script type="text/javascript" src="../Themes/Scripts/jqueryeasyui/jquery.easyui.min.js"></script>
    <script src="../Themes/Scripts/jqueryeasyui/plugins/jquery.edatagrid.js" type="text/javascript"></script>
    <script type="text/javascript">

        var $fd;
        var $td;
        var $width;
        var $height;

        $(document).ready(function () {
            
            $('#tt').tabs({
                border: false,
                onSelect: function (title) {
                    if (title == "组分数据采集日志") {
                        GetTable("1=1");
                    }
                    else if (title == "电气常量采集日志") {
                        GetConstDataList();
                    }
                }
            });

            //历史列表刷新
            $("#btnRefreshList").click(function myfunction() {

                $width = $("#rightPanel").width();
                $height = $("#rightPanel").height() - 30;

                if ($("#form1").form('validate')) {

                    $fd = $("#formdate").datebox('getValue');
                    $td = $("#todate").datebox('getValue');

                    GetTable("1=1");
                    GetConstDataList();
                }

            });

            GetTable("1=1");
        });

        function formatDatebox(value) {
         
            if (value == null || value == '') {
                return '';
            }
          
            var dt;
            if (value instanceof Date) {
                dt = value;
            } else {
                // dt = new Date(value.replace('T', ' '));
                return value.replace('T', ' ');
            }
           
            return dt.format("yyyy-MM-dd hh:mm:ss");

       
        }
        function fixWidth(percent) {
            return (document.body.clientWidth - 5) * percent;
        }
        function GetTable(arridstring) {
            var editRow = undefined;



            $("#Student_Table").datagrid({
        
                width: $width,
                fitColumns: true,
                title: '组分历史数据',
                collapsible: true,
                singleSelect: true,
                url: 'CRUDHandler.ashx',
                queryParams: { action: 'QueryMonitor', fromDate: $fd, toDate: $td },
                idField: 'OID',
                columns: [[
                 { field: 'OID', title: 'ID', hidden: true },
                  { field: 'CheckPointCode', align: 'center', width: fixWidth(0.04), title: "测点编码" },
		 { field: 'CheckPointName', align: 'center', width: fixWidth(0.10), title: '测点名称' },
        	{
        	    field: 'CollectionDatetime', align: 'center', width: fixWidth(0.10), title: '采集时间', formatter: formatDatebox
        	},
        	{ field: 'Pressure', title: '压力(Mpa)', width: fixWidth(0.05), align: 'center' },
            { field: 'Temperture', title: '温度(℃)', width: fixWidth(0.05), align: 'center' },
            { field: 'MicroWater', title: '微水', width: fixWidth(0.05), align: 'center' },
            { field: 'SO2', title: 'SO2', width: fixWidth(0.05), align: 'center' },
            { field: 'CO', title: 'CO', width: fixWidth(0.05), align: 'center' },
            { field: 'CF4', title: 'CF4', width: fixWidth(0.05), align: 'center' },
            { field: 'SO2F2', title: 'SO2F2', width: fixWidth(0.05), align: 'center' },
            { field: 'SOF2', title: 'SOF2', width: fixWidth(0.05), align: 'center' },
            { field: 'CS2', title: 'CS2', width: fixWidth(0.05), align: 'center' },
             { field: 'HF', title: 'HF', width: fixWidth(0.05), align: 'center' },
                           { field: 'H2S', title: 'H2S', width: fixWidth(0.06), align: 'center' },
 { field: 'COSS', title: 'COS', width: fixWidth(0.06), align: 'center' },
 { field: 'C2F6', title: 'C2F6', width: fixWidth(0.06), align: 'center' },
  { field: 'C3F8', title: 'C3F8', width: fixWidth(0.06), align: 'center' },


             {
                 field: 'CollectionTypeName', width: fixWidth(0.05), title: '采集类型', align: 'center', formatter: function (value) {
                     if (value == 1) {
                         return "在线";
                     }
                     else {
                         return "离线";
                     }

                 }
             },
               { field: 'CollectionBy', width: fixWidth(0.05), title: '采集人', align: 'center' },
               { field: 'BelongEquipmentName', width: fixWidth(0.10), title: ' 所属设备', align: 'center' },
                 { field: 'BelongPowerStationName', width: fixWidth(0.06), title: ' 所属变电站', align: 'center' }
                ]]


            });
        }
        //电气常量数据列表
        function GetConstDataList() {

            var editRow = undefined;
            $("#listEConstList").datagrid({
               
                width: $width,
                fitColumns: true,
                collapsible: true,
                singleSelect: true,
                url: 'GetOCInstr_ChildsHandler.ashx',
                queryParams: { action: 'GetConstDataListByTime', fromDate: $fd, toDate: $td },
                idField: 'OID',
                columns: [[
                 { field: 'OID', title: 'ID', hidden: true },
                  { field: 'CheckPointCode', hidden: true, title: "测点编码" },
		 { field: 'CheckPointName', align: 'center', width: fixWidth(0.15), title: '测点名称' },
        	{
        	    field: 'CollectionDatetime', align: 'center', width: fixWidth(0.16), title: '采集时间', formatter: formatDatebox, editor: { type: 'datetimebox', options: { required: true } }
        	},
        	{ field: 'Pressure', title: '压力(Mpa)', width: fixWidth(0.06), align: 'center', editor: { type: 'text', options: { required: true } } },
            { field: 'Temperture', title: '温度(℃)', width: fixWidth(0.06), align: 'center', editor: { type: 'text', options: { required: true } } },
            { field: 'OutElectricity', title: '放电量(pC)', width: fixWidth(0.06), align: 'center', editor: { type: 'text', options: { required: true } } },
            { field: 'CurrentVal', title: '电流(A)', width: fixWidth(0.06), align: 'center', editor: { type: 'text', options: { required: true } } },
            { field: 'Voltage', title: '电压(V)', width: fixWidth(0.06), align: 'center', editor: { type: 'text', options: { required: true } } },
            { field: 'PowerVal', title: '功率(W)', width: fixWidth(0.06), align: 'center', editor: { type: 'text', options: { required: true } } },
              {
                  field: 'CollectionType', width: fixWidth(0.06), title: '采集类型', align: 'center', formatter: function (value) {
                      if (value == 1) {
                          return "在线";
                      }
                      else {
                          return "离线";
                      }

                  }
              },
               { field: 'CollectionBy', width: fixWidth(0.06), title: '采集人', align: 'center' },
                 { field: 'BelongEquipmentName', width: fixWidth(0.10), title: ' 所属设备', align: 'center' },
                 { field: 'BelongPowerStationName', width: fixWidth(0.06), title: ' 所属变电站', align: 'center' }
                ]],
                toolbar: [{
                    text: '刷新', iconCls: 'icon-reload', handler: function () {

                        $("#listEConstList").datagrid("reload");
                    }

                }]


            });

        }
    </script>
</body>
</html>

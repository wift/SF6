﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TestRowEdit.aspx.cs" Inherits="RM.Web.OnlineCheck.TestRowEdit" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../Themes/Scripts/jqueryeasyui/themes/bootstrap/easyui.css" rel="stylesheet" />
    <link href="../Themes/Scripts/jqueryeasyui/themes/icon.css" rel="stylesheet" />
    <link href="../Themes/Scripts/jqueryeasyui/themes/color.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table id="Student_Table">
        </table>
    </div>
    </form>
    <script src="../Themes/Scripts/jqueryeasyui/jquery.min.js"></script>
    <script type="text/javascript" src="../Themes/Scripts/jqueryeasyui/jquery.easyui.min.js"></script>
    <script src="../Themes/Scripts/jqueryeasyui/plugins/jquery.edatagrid.js"></script>
    <script type="text/javascript">
        function fixWidth(percent) {
            return (document.body.clientWidth - 5) * percent;
        }
        function GetTable() {
            var editRow = undefined;
            var $width = document.body.clientWidth - 5;
            $("#Student_Table").datagrid({
                height: 500,
                width: $width,
                fitColumns: true,
                title: '历史列表',
                collapsible: true,
                singleSelect: true,
                url: 'CRUDHandler.ashx',
                queryParams: { action: 'Query', checkPointCode: '' },
                idField: 'OID',
                columns: [[
                 { field: 'OID', title: 'ID', hidden: true },
		 { field: 'CheckPointName', width: fixWidth(0.07), title: '测点名称' },
        	{ field: 'CollectionDatetime', width: fixWidth(0.07), title: '采集时间' },
        	{ field: 'Pressure', title: '压力', width: fixWidth(0.07), align: 'center', editor: { type: 'text', options: { required: true}} },
            { field: 'Temperture', title: '温度', width: fixWidth(0.07), align: 'center', editor: { type: 'text', options: { required: true}} },
            { field: 'MicroWater', title: '微水', width: fixWidth(0.07), align: 'center', editor: { type: 'text', options: { required: true}} },
            { field: 'SO2', title: 'SO2', width: fixWidth(0.07), align: 'center', editor: { type: 'text', options: { required: true}} },
            { field: 'CO', title: 'CO', width: fixWidth(0.07), align: 'center', editor: { type: 'text', options: { required: true}} },
            { field: 'CF4', title: 'CF4', width: fixWidth(0.07), align: 'center', editor: { type: 'text', options: { required: true}} },
            { field: 'SO2F2', title: 'SO2F2', width: fixWidth(0.07), align: 'center', editor: { type: 'text', options: { required: true}} },
            { field: 'SOF2', title: 'SOF2', width: fixWidth(0.07), align: 'center', editor: { type: 'text', options: { required: true}} },
            { field: 'CS2', title: 'CS2', width: fixWidth(0.07), align: 'center', editor: { type: 'text', options: { required: true}} },
             { field: 'HF', title: 'HF', width: fixWidth(0.07), align: 'center', editor: { type: 'text', options: { required: true}} },
              { field: 'CollectionType', width: fixWidth(0.07), title: 'CollectionType', align: 'center', editor: { type: 'text', options: { required: true}} },
               { field: 'CollectionBy', width: fixWidth(0.07), title: 'CollectionBy', align: 'center' }
        ]],
                toolbar: [{
                    text: '添加', iconCls: 'icon-add', handler: function () {
                        if (editRow != undefined) {
                            $("#Student_Table").datagrid('endEdit', editRow);
                        }
                        if (editRow == undefined) {
                            $("#Student_Table").datagrid('insertRow', {
                                index: 0,
                                row: {}
                            });

                            $("#Student_Table").datagrid('beginEdit', 0);
                            editRow = 0;
                        }
                    }
                }, '-', {
                    text: '保存', iconCls: 'icon-save', handler: function () {
                        $("#Student_Table").datagrid('endEdit', editRow);

                        //如果调用acceptChanges(),使用getChanges()则获取不到编辑和新增的数据。

                        //使用JSON序列化datarow对象，发送到后台。
                        var rows = $("#Student_Table").datagrid('getChanges');

                        var rowstr = JSON.stringify(rows);
                        $.post('/Home/Create', rowstr, function (data) {

                        });
                    }
                }, '-', {
                    text: '撤销', iconCls: 'icon-redo', handler: function () {
                        editRow = undefined;
                        $("#Student_Table").datagrid('rejectChanges');
                        $("#Student_Table").datagrid('unselectAll');
                    }
                }, '-', {
                    text: '删除', iconCls: 'icon-remove', handler: function () {
                        var row = $("#Student_Table").datagrid('getSelections');

                    }
                }, '-', {
                    text: '修改', iconCls: 'icon-edit', handler: function () {
                        var row = $("#Student_Table").datagrid('getSelected');
                        if (row != null) {
                            if (editRow != undefined) {
                                $("#Student_Table").datagrid('endEdit', editRow);
                            }

                            if (editRow == undefined) {
                                var index = $("#Student_Table").datagrid('getRowIndex', row);
                                $("#Student_Table").datagrid('beginEdit', index);
                                editRow = index;
                                $("#Student_Table").datagrid('unselectAll');
                            }
                        } else {

                        }
                    }
                }],
                onAfterEdit: function (rowIndex, rowData, changes) {
                    editRow = undefined;
                },
                onDblClickRow: function (rowIndex, rowData) {
                    if (editRow != undefined) {
                        $("#Student_Table").datagrid('endEdit', editRow);
                    }

                    if (editRow == undefined) {
                        $("#Student_Table").datagrid('beginEdit', rowIndex);
                        editRow = rowIndex;
                    }
                },
                onClickRow: function (rowIndex, rowData) {
                    if (editRow != undefined) {
                        $("#Student_Table").datagrid('endEdit', editRow);

                    }

                }

            });
        }

        $(document).ready(function myfunction() {
            GetTable();
        });
    </script>
</body>
</html>

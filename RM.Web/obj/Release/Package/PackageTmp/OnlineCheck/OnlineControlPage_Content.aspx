﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="OnlineControlPage_Content.aspx.cs"
    Inherits="RM.Web.OnlineCheck.OnlineControlPage_Content" %>

<%@ Register Src="~/UserControl/PageControl.ascx" TagName="PageControl" TagPrefix="uc1" %>
<%@ Register Src="~/UserControl/LoadButton.ascx" TagName="LoadButton" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>在线监测控制 </title>
    <link href="~/Themes/Styles/Site.css" rel="stylesheet" type="text/css" />
    <script src="../Themes/Scripts/Validator/JValidator.js" type="text/javascript"></script>
    <script src="../Themes/Scripts/jquery-1.8.2.min.js" type="text/javascript"></script>
    <script src="../Themes/Scripts/jquery.pullbox.js" type="text/javascript"></script>
    <script src="../Themes/Scripts/FunctionJS.js" type="text/javascript"></script>
    <script src="../Themes/Scripts/DatePicker/WdatePicker.js" type="text/javascript"></script>
    <script type="text/javascript">
        var url = "/OnlineCheck/OnlineControlPageEdit.aspx";
        function add() {

            var code = GetQuery("code");
            if (code.indexOf("SB") == -1) {
                alert('请选择一个设备');
            }
            else {
                top.openDialog(url+"?code="+code, 'OnlineControlPageEdit', '添加', 1100, 620, 50, 50);
            }

        }

        function Delete() {

            var id = $("#table1").find(".selected").find("a").attr("id");
            var crudUrl = '/OnlineCheck/CRUDHandler.ashx';
            DeleteData(crudUrl,{ oid: id, table: "OCInstrumentInfo",action:"Delete" });
             
        }
        $(function () {
            InitControl();
        })
        function InitControl() {
            // $(".div-body").PullBox({ dv: $(".div-body"), obj: $("#table1").find("tr") });
            //   divresize(260);
            // FixedTableHeader("#table1", $(window).height());
        }
        function showDialog(oid) {
            top.openDialog(url+"?oid=" + oid, 'OnlineControlPageEdit', '编辑', 1100, 620, 50, 50);
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional">
        <ContentTemplate>
            <div style="text-align: right;">
                <uc2:LoadButton ID="LoadButton1" runat="server" />
            </div>
            <div class="div-body">
                <table id="table1" class="grid" singleselect="true">
                    <colgroup>
                        <col width="16%" />
                        <col width="7%" />
                        <col width="7%" />
                        <col width="11%" />
                        <col width="7%" />
                        <col width="16%" />
                        <col width="11%" />
                        <col width="10%" />
                    </colgroup>
                    <thead>
                        <tr>
                            <td style="text-align: center;">
                                测点名称
                            </td>
                            <td style="text-align: center;">
                                测点编码
                            </td>
                            <td style="text-align: center;">
                                仪器编码
                            </td>
                            <td style="text-align: center;">
                                仪器名称
                            </td>
                            <td style="text-align: center;">
                                运行状态
                            </td>
                            <td style="text-align: center;">
                                安装位置
                            </td>
                            <td style="text-align: center;">
                                所属设备
                            </td>
                            <td style="text-align: center;">
                                所属变电站
                            </td>
                        </tr>
                    </thead>
                    <tbody>
                        <asp:Repeater ID="rp_Item" runat="server" OnItemDataBound="rp_ItemDataBound" OnItemCommand="rp_Item_ItemCommand">
                            <ItemTemplate>
                                <tr>
                                    <td style="width: 100px; text-align: center;">
                                        <a onclick="showDialog(<%# Eval("OID")%>)" id="<%# Eval("OID")%>" href="javascript:void(0)">
                                            <%# Eval("CheckPointName")%>
                                        </a>
                                    </td>
                                    <td style="text-align: center;">
                                        <%# Eval("CheckPointCode")%>
                                    </td>
                                    <td style="text-align: center;">
                                        <%# Eval("InstrumentCode")%>
                                    </td>
                                    <td style="text-align: center;">
                                        <%# Eval("InstrumentName")%>
                                    </td>
                                    <td style="text-align: center;">
                                        <%# DictEGMNS["RunStatus"][Eval("RunStatus").ToString()]%>
                                    </td>
                                    <td style="text-align: center;">
                                        <%# Eval("IntallPostion")%>
                                    </td>
                                    <td style="text-align: center;">
                                        <%# Eval("BelongEquipmentName")%>
                                    </td>
                                    <td style="text-align: center;">
                                        <%# Eval("BelongPowerStationName")%>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </tbody>
                </table>
            </div>
            <uc1:PageControl ID="PageControl1" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>

﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="RM.Web.RMBase.Test.WebForm1" %>

<%@ Register Src="../../UserControl/LoadButton.ascx" TagName="LoadButton" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="/Themes/Styles/Site.css" rel="stylesheet" type="text/css" />
    <script src="/Themes/Scripts/jquery-1.8.2.min.js" type="text/javascript"></script>
    <script src="/Themes/Scripts/jquery.pullbox.js" type="text/javascript"></script>
    <script src="/Themes/Scripts/FunctionJS.js" type="text/javascript"></script>
    <script type="text/javascript">
        function Delete() {
            document.getElementById('<%= this.btnSave.ClientID %>').click();
        }

        function add() {

        }

        function search() {

        }

        function SaveForm() {

        } 
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div style="text-align: right">
       <%-- <uc2:LoadButton ID="LoadButton2" runat="server" />--%>
    </div>
    <div>
        <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
        <div style="display: none;">
            <asp:Button ID="btnSave" runat="server" Text="Button" OnClick="Button1_Click" />
        </div>
    </div>
    </form>
</body>
</html>

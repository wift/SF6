﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DataDictionary.aspx.cs"
    Inherits="RM.Web.RMBase.SysConfig.DataDictionary" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Src="~/UserControl/PageControl.ascx" TagName="PageControl" TagPrefix="uc1" %>
<%@ Register Src="~/UserControl/LoadButton.ascx" TagName="LoadButton" TagPrefix="uc2" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="/Themes/Styles/Site.css" rel="stylesheet" type="text/css" />
    <script src="/Themes/Scripts/jquery-1.8.2.min.js" type="text/javascript"></script>
    <script src="/Themes/Scripts/jquery.pullbox.js" type="text/javascript"></script>
    <script src="/Themes/Scripts/FunctionJS.js" type="text/javascript"></script>
    <script type="text/javascript">
        //添加
        function add() {
            document.getElementById('<%= this.btnAdd.ClientID %>').click();
        }

        //删除
        function Delete() {
            document.getElementById('<%= this.btnDel.ClientID %>').click();
        }
        //保存
        function SaveForm() {
            document.getElementById('<%= this.btnSave.ClientID %>').click();
        }

    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div style="text-align: right;">
        <uc2:LoadButton ID="LoadButton1" runat="server" />
    </div>
    <div style="text-align: right; display: none;">
        <asp:Button ID="btnSave" runat="server" OnClick="btnSave_Click" Text="btnSave_Click" />
        <asp:Button ID="btnAdd" runat="server" OnClick="btnAdd_Click" Text="btnAdd_Click" />
        <asp:Button ID="btnDel" runat="server" OnClick="btnDel_Click" Text="btnDel_Click" />
    </div>
    <table width="100%">
        <colgroup>
            <col width="5%" />
            <col width="20%" />
            <col width="7%" />
            <col width="11%" />
            <col width="5%%" />
            <col width="11%" />
            <col width="5%" />
            <col width="25%" />
        </colgroup>
        <tr>
            <td class="inner_cell_right">
                类型：
            </td>
            <td>
                <asp:DropDownList ID="ddlType" runat="server" Width="100%" AutoPostBack="true" OnSelectedIndexChanged="ddlType_SelectedIndexChanged">
                </asp:DropDownList>
            </td>
            <td class="inner_cell_right">
                编码：
            </td>
            <td>
                <asp:TextBox ID="txtCode" runat="server"></asp:TextBox>
            </td>
            <td class="inner_cell_right">
                名称：
            </td>
            <td>
                <asp:TextBox ID="txtName" runat="server"></asp:TextBox>
            </td>
            <td class="inner_cell_right">
                序号：
            </td>
            <td>
                <asp:TextBox ID="txtIndex" runat="server"></asp:TextBox>
            </td>
        </tr>
    </table>
    <div class="div-body">
        <table id="table1" class="grid" singleselect="true">
            <thead>
                <tr>
                    <td style="width: 60px; text-align: center;">
                        编码
                    </td>
                    <td style="width: 80px; text-align: center;">
                        名称
                    </td>
                    <td style="width: 100px; text-align: center;">
                        序号
                    </td>
                </tr>
            </thead>
            <tbody>
                <asp:Repeater ID="rp_Item" runat="server" OnItemCommand="rp_Item_ItemCommand">
                    <ItemTemplate>
                        <tr>
                            <td style="width: 60px; text-align: center;">
                                <asp:LinkButton ID="lbtnSelect" runat="server" CommandArgument='<%#Eval("Code") %>'><%#Eval("Code")%></asp:LinkButton>
                            </td>
                            <td style="width: 80px; text-align: center;">
                                <%#Eval("CODE_CHI_DESC")%></a>
                            </td>
                            <td style="width: 100px; text-align: center;">
                                <%#Eval("DISPLAY_ORDER")%>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        <% if (rp_Item != null)
                           {
                               if (rp_Item.Items.Count == 0)
                               {
                                   Response.Write("<tr><td colspan='8' style='color:red;text-align:center'>没有找到您要的相关数据！</td></tr>");
                               }
                           } %>
                    </FooterTemplate>
                </asp:Repeater>
            </tbody>
        </table>
        <uc1:PageControl ID="PageControl1" runat="server" />
    </div>
    </form>
</body>
</html>

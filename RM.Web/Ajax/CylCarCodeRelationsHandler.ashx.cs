﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using RM.Common.DotNetBean;

namespace RM.Web.Ajax
{
    /// <summary>
    /// CylCarCodeRelationsHandler 的摘要说明
    /// </summary>
    public class CylCarCodeRelationsHandler : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";

            string action = context.Request["action"] ?? "";

            if (!string.IsNullOrWhiteSpace(action))
            {
                //获取钢瓶
                if (action == "GetCylinderCode")
                {

                    string carcaseCode = context.Request["CarcaseCode"] ?? "";
                    if (!string.IsNullOrWhiteSpace(carcaseCode))
                    {

                        string sql = string.Format(@"select c.CylinderCode,
  (    SELECT CODE_CHI_DESC
    FROM dbo.SYS_CODE 
    WHERE CODE_TYPE='GPRL' AND CODE=b.CylinderCapacity) AS CylinderCapacity,a.IsPass,b.[Status]
   from Cyl_Car_Code_Relations c
   LEFT JOIN [EGMNGS].[dbo].[BookCylinderInfo] b ON b.CylinderCode=c.CylinderCode
 left join ApplRegGasQtyInspection a on a.GasCdoe=b.CurGasCode
   where CarcaseCode='{0}' order by PKID desc", carcaseCode);
                        DataTable dt = EGMNGS.Common.ComServies.Query(sql);

                        context.Response.ContentType = "application/json";
                        context.Response.Write(Newtonsoft.Json.JsonConvert.SerializeObject(dt));
                    }

                }
                else if (action == "AddCylinderCode")
                {
                    string cylinderCode = context.Request["CylinderCode"] ?? "";
                    string carcaseCode = context.Request["CarcaseCode"] ?? "";
                    string username = context.Request["username"] ?? "";
                    //钢瓶入库
                    if (!string.IsNullOrWhiteSpace(cylinderCode))
                    {

                        EGMNGS.BLL.BookCylinderInfo gpbll = new EGMNGS.BLL.BookCylinderInfo();
                        var modelBookCylinderInfo = gpbll.GetModelByCylinderCode(cylinderCode);
                        if (modelBookCylinderInfo==null)
                        {
                            context.Response.Write(3);
                            context.Response.End();
                        }

                        string sqlExists = string.Format("select 1 from [EGMNGS].[dbo].[Cyl_Car_Code_Relations] where CylinderCode='{0}'", cylinderCode);
                        bool exists = Maticsoft.DBUtility.DbHelperSQL.Exists(sqlExists);
                        if (exists)
                        {
                            context.Response.Write(2);
                        }
                        else
                        {
                            int count = Maticsoft.DBUtility.DbHelperSQL.ExecuteSql(string.Format(@"INSERT INTO [EGMNGS].[dbo].[Cyl_Car_Code_Relations]
           ([CarcaseCode]
           ,[CylinderCode]
           ,[Created_By]
           ,[Created_Date])
     VALUES
           ('{0}'
           ,'{1}'
           ,'{2}'
           ,'{3}')", carcaseCode, cylinderCode, username, DateTime.Now));

                            string rlsql = string.Format(@"select  (select s.CODE_CHI_DESC from dbo.SYS_CODE s where  s.CODE_TYPE='GPRL'
 and s.CODE=b.CylinderCapacity) as CylinderCapacity,b.[Status],a.IsPass
 from dbo.BookCylinderInfo  b
 left join ApplRegGasQtyInspection a on a.GasCdoe=b.CurGasCode
 where b.CylinderCode='{0}'", cylinderCode);

                            context.Response.ContentType = "application/json";
                            DataSet ds = Maticsoft.DBUtility.DbHelperSQL.Query(rlsql);
                            string reults = string.Empty;

                            if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                            {
                                DataRow dr=ds.Tables[0].Rows[0];

                                reults = Newtonsoft.Json.JsonConvert.SerializeObject(new { CylinderCapacity = dr["CylinderCapacity"], Status = dr["Status"], IsPass = dr["IsPass"] });
                            }

                            context.Response.Write(reults);
                        }
                    }

                }
                else if (action == "DeleteCylinderCode")
                {
                    string cylinderCode = context.Request["CylinderCode"] ?? "";
                    //钢瓶出库
                    if (!string.IsNullOrWhiteSpace(cylinderCode))
                    {
                        int count = Maticsoft.DBUtility.DbHelperSQL.ExecuteSql(string.Format(@"delete from [EGMNGS].[dbo].[Cyl_Car_Code_Relations] WHERE CylinderCode='{0}'", cylinderCode));
                        context.Response.Write(count);
                    }
                }
            }
            context.Response.End();
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using RM.Common.DotNetUI;
using RM.Web.App_Code;
using RM.Common.DotNetCode;

namespace RM.Web.RMBase.SysConfig
{
    public partial class DataDictionary : PageBase
    {
        EGMNGS.BLL.SYS_CODE bll = new EGMNGS.BLL.SYS_CODE();

        private EGMNGS.Model.SYS_CODE Model
        {
            get
            {
                if (ViewState["SYS_CODE"] == null)
                {
                    return new EGMNGS.Model.SYS_CODE();
                }
                return ViewState["SYS_CODE"] as EGMNGS.Model.SYS_CODE;
            }
            set { ViewState["SYS_CODE"] = value; }
        }
        private string actionMode
        {
            get { return ViewState["ActionMode"] as String; }
            set { ViewState["ActionMode"] = value; }
        }

        private string codeType
        {
            get { return ViewState["CodeType"] as String; }
            set { ViewState["CodeType"] = value; }
        }
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {


                EGMNGS.BLL.SYS_CODE_TYPE SYS_CODE_TYPEBLL = new EGMNGS.BLL.SYS_CODE_TYPE();
                DataSet ds = SYS_CODE_TYPEBLL.GetAllList();

                this.ddlType.DataSource = ds.Tables[0];
                this.ddlType.DataTextField = "CODE_TYPE_DESC";
                this.ddlType.DataValueField = "CODE_TYPE";
                this.ddlType.DataBind();
                this.ddlType.Items.Insert(0, "请选择");

                this.ddlType.SelectedIndex = 0;

            }

            this.PageControl1.pageHandler += new EventHandler(pager_PageChanged);
        }

        /// <summary>
        /// 绑定数据，分页
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void pager_PageChanged(object sender, EventArgs e)
        {
            DataBindGrid();
        }


        /// <summary>
        /// 绑定数据源
        /// </summary>
        private void DataBindGrid()
        {
            int count = bll.GetRecordCount(string.Format("CODE_TYPE='{0}'", codeType));
            DataSet ds = bll.GetListByPage(string.Format("CODE_TYPE='{0}'", codeType), "DISPLAY_ORDER", PageControl1.PageIndex, PageControl1.PageSize);
            //if (ds.Tables[0].Rows.Count == 0)
            //{
            //    return;
            //}
            ControlBindHelper.BindRepeaterList(ds.Tables[0], rp_Item);
            this.PageControl1.RecordCount = count;
        }

        /// <summary>
        /// 绑定数据对象
        /// </summary>
        private void DataBinder()
        {
            Model.CODE_TYPE = codeType;
            Model.CODE = this.txtCode.Text;
            Model.CODE_CHI_DESC = this.txtName.Text;
            Model.DISPLAY_ORDER = Convert.ToInt32(this.txtIndex.Text);
        }
        private void InitControl()
        {
            this.txtCode.Text = Model.CODE;
            this.txtIndex.Text = Model.DISPLAY_ORDER.ToString();
            this.txtName.Text = Model.CODE_CHI_DESC;

        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                if (actionMode == "Add")
                {
                    if (this.ddlType.SelectedIndex == 0)
                    {
                        ShowMsgHelper.showWarningMsg("请选择分类！");
                        return;
                    }

                    DataBinder();
                    int count = bll.GetRecordCount(string.Format("Code='{0}' and Code_Type='{1}'", Model.CODE, Model.CODE_TYPE));
                    if (count > 0)
                    {
                        ShowMsgHelper.showWarningMsg("编码已经存在!");
                        return;
                    }

                    if (bll.Add(Model))
                    {
                        actionMode = string.Empty;
                    }

                    DataBindGrid();
                    actionMode = "";
                }
                else if (actionMode == "Edit")
                {
                    DataBinder();
                    bll.Update(Model);
                    DataBindGrid();
                    actionMode = "";
                }
            }
            catch (ArgumentOutOfRangeException ex)
            {
                string[] errs = CommonHelper.GetValue(ex.Message, "@").Split(':');
                ShowMsgHelper.ExecuteScript(string.Format("ChangeCss($('#txt{0}'),'长度不能超{1}字')", errs[0], errs[1]));
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            actionMode = "Add";
            Model = new EGMNGS.Model.SYS_CODE();
            InitControl();
            this.ddlType.SelectedIndex = 0;
            this.txtCode.Enabled = true;

        }


        protected void btnDel_Click(object sender, EventArgs e)
        {
            bll.Delete(Model.CODE_TYPE, Model.CODE, Model.SUBCODE);

            InitControl();

            DataBindGrid();
        }

        protected void ddlType_SelectedIndexChanged(object sender, EventArgs e)
        {
            codeType = this.ddlType.SelectedValue;
            PageControl1.PageIndex = 1;
            DataBindGrid();
        }

        protected void rp_Item_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            string code = e.CommandArgument as String;
            actionMode = "Edit";
            Model = bll.GetModel(codeType, code, Model.SUBCODE);
            this.txtCode.Text = Model.CODE;
            this.txtIndex.Text = Model.DISPLAY_ORDER.ToString(); ;
            this.txtName.Text = Model.CODE_CHI_DESC;
            this.ddlType.SelectedValue = Model.CODE_TYPE;
            this.txtCode.Enabled = false;
        }
    }
}
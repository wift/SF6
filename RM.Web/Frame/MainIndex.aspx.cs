﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using RM.Common.DotNetBean;

namespace RM.Web.Frame
{

    public partial class MainIndex : System.Web.UI.Page
    {
        public string SystemCapption
        {
            set;
            get;
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            Session["SysType"] = Request["sysType"];
            string SysType = Session["SysType"].ToString();
            if (SysType == "1")
            {
                //SystemCapption = "六氟化硫回收净化处理";
                SystemCapption = Common.DotNetConfig.ConfigHelper.GetAppSettings("Logo1");
            }
            else if (SysType == "2")
            {
               // SystemCapption = "六氟化硫在线监测与远程控制";
                SystemCapption = Common.DotNetConfig.ConfigHelper.GetAppSettings("Logo2");
            }
            else if (SysType == "3")
            {
               // SystemCapption = "电磁环境数据管理";
                SystemCapption = Common.DotNetConfig.ConfigHelper.GetAppSettings("Logo3");
            }
        }
    }
}
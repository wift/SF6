﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;
using RM.Busines.DAL;
using RM.Busines.IDAO;
using System.Data;
using RM.Common.DotNetBean;
using RM.Common.DotNetJson;
using System.Collections;
using RM.Common.DotNetCode;
using RM.Common.DotNetUI;
using System.Text.RegularExpressions;
using RM.Common.DotNetConfig;
using System.Text;


namespace RM.Web.Frame
{
    /// <summary>
    /// Frame 的摘要说明
    /// </summary>
    public class Frame : IHttpHandler, IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            context.Response.Buffer = true;
            context.Response.ExpiresAbsolute = DateTime.Now.AddDays(-1);
            context.Response.AddHeader("pragma", "no-cache");
            context.Response.AddHeader("cache-control", "");
            context.Response.CacheControl = "no-cache";
            string Action = context.Request["action"];                      //提交动作
            string user_Account = context.Request["user_Account"];          //账户
            string userPwd = context.Request["userPwd"];                    //密码
            string code = context.Request["code"];                          //验证码
            RM_UserInfo_IDAO user_idao = new RM_UserInfo_Dal();
            RM_System_IDAO sys_idao = new RM_System_Dal();
            // IPScanerHelper objScan = new IPScanerHelper();
            // objScan.DataPath = context.Server.MapPath("/Themes/IPScaner/QQWry.Dat");
            // objScan.IP = RequestHelper.GetIP();
            string OWNER_address = context.Session.SessionID;

            var sqlp = new SqlParam[] { new SqlParam("@user_account", DbType.String, user_Account) };

            switch (Action)
            {
                case "login":
                    if (context.Session["dt_session_code"] != null && code.ToLower() != context.Session["dt_session_code"].ToString().ToLower())
                    {
                        context.Response.Write("1");//验证码输入不正确！
                        context.Response.End();
                    }
                    DataTable dtlogin = user_idao.UserLogin(user_Account.Trim(), userPwd.Trim());

                    if (dtlogin != null)
                    {
                        if (dtlogin.Rows.Count != 0)
                        {
                            string delmark = dtlogin.Rows[0]["DeleteMark"].AsTargetType<string>("");
                            if (delmark == "1")
                            {



                                if (Islogin(context, user_Account))
                                {


                                    //todo 系统用户判断 不安全密码

                                    //if (dtlogin.Rows[0]["User_ID"].AsTargetType<string>("") == "48f3889c-af8d-401f-ada2-c383031af92d")
                                    //{

                                    //    if (ValidateUtil.IsSystemLevSafePWD(userPwd.Trim()) == false)
                                    //    {
                                    //        context.Response.Write("7");
                                    //        context.Response.End();
                                    //    }

                                    //}
                                    //else//普通用户判断 不安全密码
                                    //{
                                    //    if (ValidateUtil.IsOrdinaryLevSafePWD(userPwd.Trim()) == false)
                                    //    {
                                    //        context.Response.Write("7");
                                    //        context.Response.End();
                                    //    }
                                    //}


                                    if (Convert.ToInt32(context.Application["CurrentUsers"]) >= Convert.ToInt32(ConfigHelper.GetAppSettings("maxThreads")))
                                    {
                                        context.Session.Abandon();
                                        context.Response.Write("8");//在线的人数已经超过上限，请联系管理员
                                        context.Response.End();
                                    }

                                    LoginSuccess(dtlogin);
                                    user_idao.SysLoginLog(user_Account, "1", OWNER_address);
                                    context.Response.Write("3");//验证成功
                                    context.Response.End();
                                }
                                else
                                {
                                    context.Response.Write("6");//该用户已经登录，不允许重复登录
                                    context.Response.End();
                                }
                            }
                            else if (delmark == "2")
                            {
                                //两个分钟后可以登陆
                                StringBuilder checkAllowLoginSQL = new StringBuilder(string.Format(@"select 1  
                                                from [Base_SysLoginlog] 
                                               where User_Account='{0}' and DATEDIFF(n,SYS_LOGINLOG_TIME,GETDATE())<2 and SYS_LOGINLOG_STATUS='2'", user_Account));

                                object checkAllowLogin = RM.Busines.DataFactory.SqlDataBase().GetObjectValue(checkAllowLoginSQL);

                                if (checkAllowLogin == null)//超过两分钟可以登陆
                                {
                                    LoginSuccess(dtlogin);
                                    user_idao.SysLoginLog(user_Account, "1", OWNER_address);
                                    context.Response.Write("3");//验证成功
                                    context.Response.End();
                                }
                                else//还不可以登陆
                                {

                                    user_idao.SysLoginLog(user_Account, "2", OWNER_address);//账户被锁,联系管理员！
                                    context.Response.Write("2");
                                    context.Response.End();
                                }
                            }
                        }
                        else//登陆失败3次锁住账户
                        {
                            user_idao.SysLoginLog(user_Account, "0", OWNER_address);

                            DataTable dt = RM.Busines.DataFactory.SqlDataBase().GetDataTableBySQL(new System.Text.StringBuilder(@"SELECT SUM(T.SYS_LOGINLOG_STATUS) FROM (
                                                                                                                    SELECT  
                                                                                                                          TOP 3 [SYS_LOGINLOG_STATUS]
                                                                                                                      FROM [Base_SysLoginlog] WHERE user_account=@user_account ORDER BY SYS_LOGINLOG_TIME  DESC) T "), sqlp);
                            if (dt.Rows[0][0].ToString() == "0")
                            {
                                RM.Busines.DataFactory.SqlDataBase().ExecuteBySql(new System.Text.StringBuilder(@"UPDATE Base_UserInfo
SET
	DeleteMark = 2	
WHERE User_Account=@user_account"), sqlp);
                                user_idao.SysLoginLog(user_Account, "2", OWNER_address);
                                context.Response.Write("2");
                                context.Response.End();
                            }

                            context.Response.Write("4");//账户或者密码有错误！
                            context.Response.End();
                        }
                    }
                    else
                    {
                        context.Response.Write("5");//服务连接不上！
                        context.Response.End();
                    }
                    break;
                case "Menu":
                    string UserId = RequestSession.GetSessionUser().UserId.ToString();//用户ID
                    object systype = context.Session["SysType"];
                    DataTable dtMenu = sys_idao.GetMenuHtml(UserId);

                    if (systype != null)
                    {
                        dtMenu = sys_idao.GetMenuHtml(UserId).Select("SysType=" + systype.ToString()).CopyToDataTable();
                    }

                    string strMenus = JsonHelper.DataTableToJson(dtMenu, "MENU");
                    context.Response.Write(strMenus);
                    context.Response.End();
                    break;
                case "CheckOut":
                    CheckOut(context);
                    context.Response.End();
                    break;
                default:
                    break;
            }
        }
        /// <summary>
        /// 同一账号不能同时登陆
        /// </summary>
        /// <param name="context"></param>
        /// <param name="User_Account">账户</param>
        /// <returns></returns>
        public bool Islogin(HttpContext context, string User_Account)
        {
            string token = CookieHelper.GetCookie("token");

            if (token == User_Account)//如果token还在不用检测数据是否登陆过。
            {
                return true;
            }

            RM_UserInfo_Dal RM_UserInfo_Dalbll = new RM_UserInfo_Dal();

            int SYS_LOGINLOG_STATUS = RM_UserInfo_Dalbll.CheckLogined(User_Account);
            if (SYS_LOGINLOG_STATUS == 1)//还在线
            {

                return false;
            }
            else
            {
                return true;
            }
        }

        /// <summary>
        /// 退出
        /// </summary>
        /// <param name="User_Account"></param>
        public void CheckOut(HttpContext context)
        {
            RM_UserInfo_IDAO user_idao = new RM_UserInfo_Dal();
            user_idao.SysLoginLog(RequestSession.GetSessionUser().UserAccount.ToString(), "3", context.Session.SessionID);//退出成功！
            CookieHelper.WriteCookie("token","1",0);
        
            context.Session.Abandon();
        }

        private void LoginSuccess(DataTable dtlogin)
        {
            var sqlp = new SqlParam[] { new SqlParam("@user_account", DbType.String, dtlogin.Rows[0]["User_Account"]) };

            SessionUser user = new SessionUser();
            user.UserId = dtlogin.Rows[0]["User_ID"].ToString();
            user.UserAccount = dtlogin.Rows[0]["User_Account"].ToString();
            user.UserName = dtlogin.Rows[0]["User_Name"].ToString() + "(" + dtlogin.Rows[0]["User_Account"].ToString() + ")";
            user.UserPwd = dtlogin.Rows[0]["User_Pwd"].ToString();
            RequestSession.AddSessionUser(user);


            RM.Busines.DataFactory.SqlDataBase().ExecuteBySql(new System.Text.StringBuilder(@"UPDATE Base_UserInfo
SET
	DeleteMark = 1
WHERE User_Account=@user_account"), sqlp);
            CookieHelper.WriteCookie("token", user.UserAccount.ToString(), 30);

        }
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }


    }
}
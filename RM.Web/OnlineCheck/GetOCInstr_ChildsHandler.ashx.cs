﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace RM.Web.OnlineCheck
{
    public class column
    {
        public string field { set; get; }
        public string title { set; get; }
    }
    /// <summary>
    /// GetOCInstr_ChildsHandler 的摘要说明
    /// </summary>
    public class GetOCInstr_ChildsHandler : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "application/json";
            string action = context.Request["action"] ?? "";
            string Jsonstr = string.Empty;
            string p_OCInstrumentInfo_OID = context.Request["OCInstrumentInfo_OID"] ?? "";
            string p_rangeSelect = context.Request["rangeSelect"] ?? "";
            string p_CheckPointCode = context.Request["CheckPointCode"] ?? "";
            string fromDate = context.Request["fromdate"] ?? "";
            string toDate = context.Request["todate"] ?? "";

            if (action == "GetOCInstrumentInfo_Childs")
            {
                string columns = context.Request["columns"] ?? "";
                Jsonstr = GetOCInstrumentInfo_Childs(p_CheckPointCode, columns, fromDate, toDate);
            }
            else if (action == "GetColumns")
            {
                Jsonstr = Newtonsoft.Json.JsonConvert.SerializeObject(GetColumns(p_CheckPointCode));
            }
            else if ((action == "DataBindGrid"))
            {
                string rangeSelect = context.Request["rangeSelect"] ?? "";

                Jsonstr = DataBindGrid(p_CheckPointCode, rangeSelect);
            }
            else if (action == "GetProportionChart")
            {
                Jsonstr = GetProportionChart(p_CheckPointCode, p_rangeSelect);
            }
            else if (action == "GetConstDataList")//电气常量数据列表
            {
                Jsonstr = GetConstDataList(p_CheckPointCode);
            }
            else if (action == "GetConstDataListByTime")//电气常量数据列表
            {
                Jsonstr = GetConstDataListByTime(fromDate, toDate);
            }
            else if (action == "GetConstDataChart")//电气常量数据图表
            {
                Jsonstr = GetConstDataChart(p_CheckPointCode, p_rangeSelect);
            }
            else if (action == "GetConstDataSave")
            {
                context.Response.ContentType = "text/plain";
                Jsonstr = GetConstDataSave(context);
            }
            else if (action == "GetConstDataDelete")
            {
                context.Response.ContentType = "text/plain";
                string oid = context.Request["oid"] ?? "";
                Jsonstr = GetConstDataDelete(oid);
            }
            else if (action== "GetProportionChartMuliPointCode")
            {
                context.Response.ContentType = "application/json";
           
                string checkPoint = context.Request["checkPoint"] ?? "";
                context.Response.Write(GetProportionChartMuliPointCode(fromDate, toDate, checkPoint));
            }
            context.Response.Write(Jsonstr);
        }
        /// <summary>
        /// 按时间获取电气数据
        /// </summary>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <returns></returns>
        private string GetConstDataListByTime(string fromDate, string toDate)
        {
            string resultsJsonStr = string.Empty;
            string sqlstr = @"SELECT {0} e.* ,oc.BelongEquipmentName,oc.BelongPowerStationName 
FROM OCInstrumentElectricalData e
LEFT JOIN OCInstrumentInfo oc ON oc.CheckPointCode=e.CheckPointCode ";

            try
            {
                string wheresql = "";
                if (!string.IsNullOrWhiteSpace(fromDate) && !string.IsNullOrWhiteSpace(toDate))
                {
                    DateTime dtfrom = DateTime.Parse(fromDate);
                    DateTime dtTo = DateTime.Parse(toDate);

                    wheresql += string.Format("where CONVERT(varchar(100), CollectionDatetime, 23) >='{0}' and CONVERT(varchar(100), CollectionDatetime, 23)<='{1}' order by CollectionDatetime desc", dtfrom.ToString("yyyy-MM-dd"), dtTo.ToString("yyyy-MM-dd"));
                    sqlstr = string.Format(sqlstr,"");
                }
                else {

                    sqlstr = string.Format(sqlstr, "top 100");
                }



                DataTable dt = EGMNGS.Common.ComServies.Query(sqlstr + wheresql).Copy();
                resultsJsonStr = Newtonsoft.Json.JsonConvert.SerializeObject(dt);
            }
            catch (Exception ex)
            {
                RM.Common.DotNetCode.LogInstanseHelper.Instance.WriteLog(ex.Message);
            }
            return resultsJsonStr;
        }
        /// <summary>
        /// 电气常量数据列表
        /// </summary>
        /// <param name="p_CheckPointCode"></param>
        /// <returns></returns>
        private string GetConstDataList(string p_CheckPointCode)
        {
            string resultsJsonStr = string.Empty;
            try
            {
                //if (!p_CheckPointCode.Contains(','))
                //{
                //    p_CheckPointCode = p_CheckPointCode.Replace("'", "");
                //}

                DataTable dt = new EGMNGS.BLL.OCInstrumentElectricalData().GetList(string.Format("CheckPointCode in ({0}) order by CollectionDatetime desc ", p_CheckPointCode)).Tables[0];
                resultsJsonStr = Newtonsoft.Json.JsonConvert.SerializeObject(dt);
            }
            catch (Exception ex)
            {
                RM.Common.DotNetCode.LogInstanseHelper.Instance.WriteLog(ex.Message);
            }
            return resultsJsonStr;
        }
        /// <summary>
        /// 电气常量数据保存
        /// </summary>
        /// <returns></returns>
        private string GetConstDataSave(HttpContext context)
        {
            try
            {
                string jsonobj = context.Request.Form[0].ToString();

                EGMNGS.Model.OCInstrumentElectricalData oiinfo = new EGMNGS.Model.OCInstrumentElectricalData();

                oiinfo = RM.Common.DotNetUI.ControlBindHelper.ModelBing<EGMNGS.Model.OCInstrumentElectricalData>(oiinfo);
                if (oiinfo.CollectionType == "在线")
                {
                    oiinfo.CollectionType = "1";
                }
                else
                {
                    oiinfo.CollectionType = "0";
                }

                if (oiinfo.OID == 0)
                {

                    EGMNGS.BLL.OCInstrumentElectricalData bll = new EGMNGS.BLL.OCInstrumentElectricalData();
                    int count = bll.Add(oiinfo);
                    if (count > 0)
                    {
                        return "添加成功";
                    }
                    else
                    {
                        return "添加失败";
                    }

                }
                else
                {
                    EGMNGS.BLL.OCInstrumentElectricalData bll = new EGMNGS.BLL.OCInstrumentElectricalData();
                    var success = bll.Update(oiinfo);
                    if (success)
                    {
                        return "修改成功";
                    }
                    else
                    {
                        return "修改失败";
                    }
                }

            }
            catch (Exception ex)
            {
                RM.Common.DotNetCode.LogInstanseHelper.Instance.WriteLog(ex.Message);
                return "修改失败";
            }
        }

        /// <summary>
        /// 电气常量数据删除
        /// </summary>
        /// <param name="oid"></param>
        /// <returns></returns>
        private string GetConstDataDelete(string oid)
        {
            string results = string.Empty;

            if (!string.IsNullOrWhiteSpace(oid))
            {
                EGMNGS.BLL.OCInstrumentElectricalData bll = new EGMNGS.BLL.OCInstrumentElectricalData();
                bool success = bll.Delete(oid.AsTargetType<int>(0));
                if (success)
                {
                    results = "1";
                }
                else
                {
                    results = "0";
                }
            }

            return results;
        }
        /// <summary>
        /// 电气常量数据列图表
        /// </summary>
        /// <param name="p_OCInstrumentInfo_OID"></param>
        /// <param name="p_CheckPointCode"></param>
        /// <param name="p_rangeSelect"></param>
        /// <returns></returns>
        private string GetConstDataChart(string p_CheckPointCode, string p_rangeSelect)
        {
            string resultsJsonStr = string.Empty;

            var rangedate = p_rangeSelect;
            DateTime dtfrom = DateTime.Now.Subtract(new TimeSpan(rangedate.AsTargetType<int>(10), 0, 0, 0));
            //  List<column> listColumn = GetColumns(p_OCInstrumentInfo_OID);
            List<chartData> listChartData = new List<chartData>();
            List<string> listDatetime = new List<string>();
            string sb = string.Empty;

            listChartData.Add(new chartData { name = "Pressure" });
            listChartData.Add(new chartData { name = "Temperture" });
            listChartData.Add(new chartData { name = "OutElectricity" });
            listChartData.Add(new chartData { name = "CurrentVal" });
            listChartData.Add(new chartData { name = "Voltage" });
            listChartData.Add(new chartData { name = "PowerVal" });

            string sqlwhere = string.Format(@"
SELECT [CollectionDatetime]
      ,[Pressure]
      ,[Temperture]
      ,[OutElectricity]
      ,[CurrentVal]
      ,[Voltage]
      ,[PowerVal]
                                                      from [EGMNGS].[dbo].OCInstrumentElectricalData where 
CheckPointCode in ({2}) and CONVERT(varchar(100), CollectionDatetime, 23) >='{0}' and CONVERT(varchar(100), CollectionDatetime, 23)<='{1}' order by CollectionDatetime", dtfrom.ToString("yyyy-MM-dd"), DateTime.Now.ToString("yyyy-MM-dd"), p_CheckPointCode, sb.ToString().TrimEnd(','));

            DataTable dtdata = EGMNGS.Common.ComServies.Query(sqlwhere);

            foreach (DataRow item in dtdata.Rows)
            {
                listDatetime.Add(item["CollectionDatetime"].ToString());
            }

            foreach (chartData itemChartData in listChartData)
            {
                foreach (DataRow item in dtdata.Rows)
                {
                    itemChartData.data.Add(item[itemChartData.name].AsTargetType<decimal>(0));
                }
            }


            //foreach (var item in listChartData)
            //{
            //    if (item.target == "Pressure")
            //    {
            //        item.target = "压力";
            //    }
            //    else if (item.target == "Temperture")
            //    {
            //        item.target = "温度";
            //    }
            //    else if (item.target == "OutElectricity")
            //    {
            //        item.target = "电流";
            //    }
            //    else if (item.target == "CurrentVal")
            //    {
            //        item.target = "电压";
            //    }
            //    else if (item.target == "PowerVal")
            //    {
            //        item.target = "功率";
            //    }
            //    else if (item.target == "Voltage")
            //    {
            //        item.target = "电压";
            //    }

            //}

            resultsJsonStr = Newtonsoft.Json.JsonConvert.SerializeObject(new { listDatetime = listDatetime, listChartData = listChartData });
            return resultsJsonStr;
        }
        /// <summary>
        /// 获取比例图表数据
        /// </summary>
        /// <param name="p_OCInstrumentInfo_OID"></param>
        /// <returns></returns>
        private string GetProportionChart(string p_CheckPointCode, string p_rangeSelect)
        {
            string resultsStr = string.Empty;
            var rangedate = p_rangeSelect;
            DateTime dtfrom = DateTime.Now.Subtract(new TimeSpan(rangedate.AsTargetType<int>(10), 0, 0, 0));
            List<column> listColumn = GetColumns(p_CheckPointCode);
            List<chartData> listChartData = new List<chartData>();
            List<string> listDatetime = new List<string>();
            string sb = string.Empty;

            for (int i = 0; i < listColumn.Count; i++)
            {

                listChartData.Add(new chartData { name = listColumn[i].field.Replace(' ', '/') });
                sb += string.Format("(CASE WHEN ISNULL({2},0)=0 THEN 0 else  Convert(decimal(18,2),{0})  END)  as '{1}',", listColumn[i].field.Replace(' ', '/'), listColumn[i].field.Replace(' ', '/'), listColumn[i].field.Split(new char[] { ' ' })[1]);
            }


            string sqlwhere = string.Format(@"
SELECT CollectionDatetime,
                                                    {3}
                                                      from [EGMNGS].[dbo].OCInstrumentInfoOnline where 
CheckPointCode in ({2}) and CONVERT(varchar(100), CollectionDatetime, 23) >='{0}' and CONVERT(varchar(100), CollectionDatetime, 23)<='{1}' order by CollectionDatetime", dtfrom.ToString("yyyy-MM-dd"), DateTime.Now.ToString("yyyy-MM-dd"), p_CheckPointCode, sb.ToString().TrimEnd(','));

            DataTable dtdata = EGMNGS.Common.ComServies.Query(sqlwhere);

            foreach (DataRow item in dtdata.Rows)
            {
                listDatetime.Add(item["CollectionDatetime"].ToString());
            }

            foreach (chartData itemChartData in listChartData)
            {
                foreach (DataRow item in dtdata.Rows)
                {
                    itemChartData.data.Add(item[itemChartData.name].AsTargetType<decimal>(0));
                }
            }

            resultsStr = Newtonsoft.Json.JsonConvert.SerializeObject(new { listDatetime = listDatetime, listChartData = listChartData });
            return resultsStr;
        }


        /// <summary>
        ///  获取电气常量yAxis
        /// </summary>
        /// <param name="fromDate">开始时间</param>
        /// <param name="toDate">结束时间</param>
        /// <param name="checkPoint">测点编码</param>
        /// <returns></returns>
        private string GetProportionChartMuliPointCode(string fromDate, string toDate, string checkPoint)
        {
            DateTime dtfrom = DateTime.Parse(fromDate);
            DateTime dtTo = DateTime.Parse(toDate);
            var cparr = checkPoint.Split(',');
            string tempcheckPoint = string.Empty;
            tempcheckPoint = cparr.Aggregate("", (current, i) => current + ("'" + i + "',")).Trim(',');
    
            List<column> listColumn = GetColumns(checkPoint);
            List<chartData> listChartData = new List<chartData>();
            List<string> listDatetime = new List<string>();
            string sb = string.Empty;


          //  string sqlwhere = string.Format("CheckPointCode in ({2}) and CONVERT(varchar(100), CollectionDatetime, 23) >='{0}' and CONVERT(varchar(100), CollectionDatetime, 23)<='{1}' order by CollectionDatetime", dtfrom.ToString("yyyy-MM-dd"), dtTo.ToString("yyyy-MM-dd"), tempcheckPoint);

            var returnValue = new { listDatetime = new List<string>(), listChartData = new List<chartData>() };

    

            foreach (var itemtarget in listColumn)
            {
                foreach (var itemcheck in cparr)
                {
                    listChartData.Add(new chartData() { target = itemtarget.title, ChackName = itemcheck.Trim('\'') });
                }

            }

            for (int i = 0; i < listColumn.Count; i++)
            {

               // listChartData.Add(new chartData { name = listColumn[i].field.Replace(' ', '/') });
                sb += string.Format("(CASE WHEN ISNULL({2},0)=0 THEN 0 else  Convert(decimal(18,2),{0})  END)  as '{1}',", listColumn[i].field.Replace(' ', '/'), listColumn[i].field.Replace(' ', '/'), listColumn[i].field.Split(new char[] { ' ' })[1]);
            }


            string sqlwhere = string.Format(@"
SELECT CheckPointCode,CollectionDatetime,
                                                    {3}
                                                      from [EGMNGS].[dbo].OCInstrumentInfoOnline where 
CheckPointCode in ({2}) and CONVERT(varchar(100), CollectionDatetime, 23) >='{0}' and CONVERT(varchar(100), CollectionDatetime, 23)<='{1}' order by CollectionDatetime", dtfrom.ToString("yyyy-MM-dd"), Convert.ToDateTime(toDate).ToString("yyyy-MM-dd"), checkPoint, sb.ToString().TrimEnd(','));

            DataTable dt = EGMNGS.Common.ComServies.Query(sqlwhere);

            foreach (DataRow item in dt.Rows)
            {
                listDatetime.Add(item["CollectionDatetime"].ToString());

                foreach (var itemChartData in listChartData)
                {    
                        if (dt.Columns.Contains(itemChartData.target) && itemChartData.ChackName == item["CheckPointCode"].ToString())
                        {
                            itemChartData.data.Add(item[itemChartData.target].AsTargetType<decimal>(0));
                        }
                }
            }

            var chartDataSource = new { listDatetime = listDatetime, listChartData = listChartData };

            return Newtonsoft.Json.JsonConvert.SerializeObject(chartDataSource);
        }

        /// <summary>
        /// 获取列
        /// </summary>
        /// <param name="p_OCInstrumentInfo_OID"></param>
        /// <returns></returns>
        private List<column> GetColumns(string p_CheckPointCode)
        {
            int countCode = p_CheckPointCode.Count(o => o.ToString().Contains(","));
            string selectSql = string.Format(@"SELECT 
t.columnname
 FROM (
SELECT 
         [TopTarget]+'/'+
         [BottomTarget] as columnname
     FROM [EGMNGS].[dbo].[OCInstrumentInfo_Childs]
   WHERE CheckPointCode in ({0}) 
) t GROUP BY columnname HAVING(count(t.columnname)>{1})", p_CheckPointCode, countCode);

            System.Text.StringBuilder sb = new System.Text.StringBuilder();

            List<column> columns = new List<column>();
            DataTable dt = EGMNGS.Common.ComServies.Query(selectSql);
            int f = 0;
            foreach (DataRow item in dt.Rows)
            {

                string columnName = item[0].AsTargetType<string>("");
                var column = new column { field = columnName.Replace('/', ' '), title = columnName };
                columns.Add(column);
            }

            return columns;
        }

        /// <summary>
        /// 获取比例历史
        /// </summary>
        /// <param name="p_OCInstrumentInfo_OID"></param>
        private string GetOCInstrumentInfo_Childs(string CheckPointCode, string columns, string fromDate, string toDate)
        {
            List<column> listcolumn = Newtonsoft.Json.JsonConvert.DeserializeObject<List<column>>(columns);

            string sb = string.Empty;

            for (int i = 0; i < listcolumn.Count; i++)
            {
                if (i <= 1)
                {
                    continue;
                }

                sb += string.Format("(CASE WHEN ISNULL({2},0)=0 THEN 0 else  Convert(decimal(18,2),{0})  END)  as '{1}',", listcolumn[i].field.Replace(' ', '/'), listcolumn[i].field.Replace('/', ' '), listcolumn[i].field.Split(new char[] { ' ' })[1]);
            }

            //if (!CheckPointCode.Contains(','))
            //{
            //    CheckPointCode = CheckPointCode.Replace("'", "");
            //}

            string wheresql = string.Empty;

            if (!string.IsNullOrWhiteSpace(fromDate) && !string.IsNullOrWhiteSpace(toDate))
            {
                DateTime dtfrom = DateTime.Parse(fromDate);
                DateTime dtTo = DateTime.Parse(toDate);

                wheresql += string.Format(" and CONVERT(varchar(100), CollectionDatetime, 23) >='{0}' and CONVERT(varchar(100), CollectionDatetime, 23)<='{1}'", dtfrom.ToString("yyyy-MM-dd"), dtTo.ToString("yyyy-MM-dd"));
            }


            //todo获取数据
            string sqlData = string.Format(@"SELECT CheckPointName,CollectionDatetime,
                                                    {1}
                                                      from [EGMNGS].[dbo].OCInstrumentInfoOnline
                                                      where CheckPointCode in ({0}){2} order by CollectionDatetime desc", CheckPointCode, sb.TrimEnd(','), wheresql);
            DataTable dtdata = EGMNGS.Common.ComServies.Query(sqlData);

            return Newtonsoft.Json.JsonConvert.SerializeObject(dtdata);
        }

        /// <summary>
        /// 获取历史数据
        /// </summary>
        /// <param name="CheckPointCode"></param>
        /// <param name="rangeSelect"></param>
        /// <returns></returns>
        private string DataBindGrid(string CheckPointCode, string rangeSelect)
        {
            EGMNGS.BLL.OCInstrumentInfoOnline bllOnline = new EGMNGS.BLL.OCInstrumentInfoOnline();

            string listDatetimeJson = string.Empty;
            string listChartDataJson = string.Empty;

            var rangedate = rangeSelect;
            DateTime dtfrom = DateTime.Now.Subtract(new TimeSpan(rangedate.AsTargetType<int>(10), 0, 0, 0));
            string sqlwhere = string.Format("CheckPointCode={2} and CONVERT(varchar(100), CollectionDatetime, 23) >='{0}' and CONVERT(varchar(100), CollectionDatetime, 23)<='{1}' order by CollectionDatetime desc", dtfrom.ToString("yyyy-MM-dd"), DateTime.Now.ToString("yyyy-MM-dd"), CheckPointCode);
            DataTable dt = bllOnline.GetList(sqlwhere).Tables[0];
            List<chartData> listChartData = new List<chartData>();
            List<string> listDatetime = new List<string>();

            listChartData.Add(new chartData() { name = "压力" });
            listChartData.Add(new chartData() { name = "温度" });
            listChartData.Add(new chartData() { name = "微水" });
            listChartData.Add(new chartData() { name = "SO2" });
            listChartData.Add(new chartData() { name = "CO" });
            listChartData.Add(new chartData() { name = "CF4" });
            listChartData.Add(new chartData() { name = "SO2F2" });
            listChartData.Add(new chartData() { name = "SOF2" });
            listChartData.Add(new chartData() { name = "CS2" });
            listChartData.Add(new chartData() { name = "HF" });
            listChartData.Add(new chartData() { name = "H2S" });
            listChartData.Add(new chartData() { name = "COSS" });
            listChartData.Add(new chartData() { name = "C2F6" });
            listChartData.Add(new chartData() { name = "C3F8" });

            foreach (DataRow item in dt.Rows)
            {
                listDatetime.Add(item["CollectionDatetime"].ToString());

                foreach (var itemChartData in listChartData)
                {
                    if (itemChartData.name == "压力")
                    {
                        itemChartData.data.Add(item["Pressure"].AsTargetType<decimal>(0));
                    }
                    else if (itemChartData.name == "温度")
                    {
                        itemChartData.data.Add(item["Temperture"].AsTargetType<decimal>(0));
                    }
                    else if (itemChartData.name == "微水")
                    {
                        itemChartData.data.Add(item["MicroWater"].AsTargetType<decimal>(0));
                    }
                    else if (itemChartData.name == "SO2")
                    {
                        itemChartData.data.Add(item["SO2"].AsTargetType<decimal>(0));
                    }
                    else if (itemChartData.name == "CO")
                    {
                        itemChartData.data.Add(item["CO"].AsTargetType<decimal>(0));
                    }
                    else if (itemChartData.name == "CF4")
                    {
                        itemChartData.data.Add(item["CF4"].AsTargetType<decimal>(0));
                    }
                    else if (itemChartData.name == "SO2F2")
                    {
                        itemChartData.data.Add(item["SO2F2"].AsTargetType<decimal>(0));
                    }
                    else if (itemChartData.name == "SOF2")
                    {
                        itemChartData.data.Add(item["SOF2"].AsTargetType<decimal>(0));
                    }
                    else if (itemChartData.name == "CS2")
                    {
                        itemChartData.data.Add(item["CS2"].AsTargetType<decimal>(0));
                    }
                    else if (itemChartData.name == "HF")
                    {
                        itemChartData.data.Add(item["HF"].AsTargetType<decimal>(0));
                    }
                }


            }

            //  listDatetimeJson = Newtonsoft.Json.JsonConvert.SerializeObject(listDatetime); 
            //  listChartDataJson = Newtonsoft.Json.JsonConvert.SerializeObject(listChartData);
            return Newtonsoft.Json.JsonConvert.SerializeObject(new { listDatetime = listDatetime, listChartData = listChartData });
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}
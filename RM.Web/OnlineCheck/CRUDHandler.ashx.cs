﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Maticsoft.DBUtility;

namespace RM.Web.OnlineCheck
{
    /// <summary>
    /// CRUDHandler 的摘要说明
    /// </summary>
    public class CRUDHandler : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            string oid = context.Request["oid"] ?? "";
            string table = context.Request["table"] ?? "";
            string action = context.Request["action"] ?? "";

            if (action == "Delete" && !string.IsNullOrWhiteSpace(table))
            {
                context.Response.Write(Delete(oid, table));
            }
            else if (action == "GetOCInstrumentInfo_Childs")
            {
                context.Response.ContentType = "application/json";
                string m_CheckPointCode = context.Request["CheckPointCode"] ?? "";
                string strjson = GetOCInstrumentInfo_ChildsByFID(m_CheckPointCode);
                context.Response.Write(strjson);
            }
            else if (action == "SaveGetOCInstrumentInfo_Childs")
            {
                try
                {
                    //string jsonobj = context.Request.Form[0].ToString();

                    EGMNGS.Model.OCInstrumentInfo_Childs oiinfo = new EGMNGS.Model.OCInstrumentInfo_Childs();

                    oiinfo = RM.Common.DotNetUI.ControlBindHelper.ModelBing<EGMNGS.Model.OCInstrumentInfo_Childs>(oiinfo);

                    EGMNGS.BLL.OCInstrumentInfo_Childs bll = new EGMNGS.BLL.OCInstrumentInfo_Childs();

                    if (oiinfo.OID == 0)
                    {

                        int count = bll.Add(oiinfo);
                        if (count > 0)
                        {
                            context.Response.Write("添加成功");
                        }
                        else
                        {
                            context.Response.Write("添加失败");
                        }

                    }
                    else
                    {
                        var success = bll.Update(oiinfo);
                        if (success)
                        {
                            context.Response.Write("修改成功");
                        }
                        else
                        {
                            context.Response.Write("修改失败");
                        }
                    }

                }
                catch (Exception ex)
                {
                    RM.Common.DotNetCode.LogInstanseHelper.Instance.WriteLog(ex.Message);
                    context.Response.Write("添加失败");
                }
            }
            else if (action == "Query")
            {
                context.Response.ContentType = "application/json";

                string checkPointCode = context.Request["checkPointCode"] ?? "''";
                string fromDate = context.Request["fromdate"] ?? "";
                string toDate = context.Request["todate"] ?? "";
                string wheresql = string.Empty;
                if (checkPointCode == "1=1")
                {
                    wheresql = " 1=1 ";
                }
                else
                {
                    wheresql = string.Format("CheckPointCode in ({0}) ", checkPointCode);
                }

                if (!string.IsNullOrWhiteSpace(fromDate) && !string.IsNullOrWhiteSpace(toDate))
                {
                    DateTime dtfrom = DateTime.Parse(fromDate);
                    DateTime dtTo = DateTime.Parse(toDate);

                    wheresql += string.Format(" and CONVERT(varchar(100), CollectionDatetime, 23) >='{0}' and CONVERT(varchar(100), CollectionDatetime, 23)<='{1}'", dtfrom.ToString("yyyy-MM-dd"), dtTo.ToString("yyyy-MM-dd"));
                }

                string strJson = Query("OCInstrumentInfoOnline", wheresql);
                context.Response.Write(strJson);
            }
            else if (action == "Delete")
            {
                if (!string.IsNullOrWhiteSpace(oid))
                {
                    EGMNGS.BLL.OCInstrumentInfoOnline bll = new EGMNGS.BLL.OCInstrumentInfoOnline();
                    bool success = bll.Delete(oid.AsTargetType<int>(0));
                    if (success)
                    {
                        context.Response.Write("1");
                    }
                    else
                    {
                        context.Response.Write("0");
                    }
                }

            }
            else if (action == "Save")
            {
                try
                {
                    string jsonobj = context.Request.Form[0].ToString();

                    EGMNGS.Model.OCInstrumentInfoOnline oiinfo = new EGMNGS.Model.OCInstrumentInfoOnline();

                    oiinfo = RM.Common.DotNetUI.ControlBindHelper.ModelBing<EGMNGS.Model.OCInstrumentInfoOnline>(oiinfo);
                    if (oiinfo.CollectionType == "在线")
                    {
                        oiinfo.CollectionType = "1";
                    }
                    else
                    {
                        oiinfo.CollectionType = "0";
                    }

                    if (oiinfo.OID == 0)
                    {

                        EGMNGS.BLL.OCInstrumentInfoOnline bll = new EGMNGS.BLL.OCInstrumentInfoOnline();
                        int count = bll.Add(oiinfo);
                        if (count > 0)
                        {
                            context.Response.Write("添加成功");
                        }
                        else
                        {
                            context.Response.Write("添加失败");
                        }

                    }
                    else
                    {
                        EGMNGS.BLL.OCInstrumentInfoOnline bll = new EGMNGS.BLL.OCInstrumentInfoOnline();
                        var success = bll.Update(oiinfo);
                        if (success)
                        {
                            context.Response.Write("修改成功");
                        }
                        else
                        {
                            context.Response.Write("修改失败");
                        }
                    }

                }
                catch (Exception ex)
                {
                    RM.Common.DotNetCode.LogInstanseHelper.Instance.WriteLog(ex.Message);
                    context.Response.Write("添加失败");
                }

            }
            else if (action == "QueryChart")
            {
                context.Response.ContentType = "application/json";
                string fromDate = context.Request["fromdate"] ?? "";
                string toDate = context.Request["todate"] ?? "";
                string checkPoint = context.Request["checkPoint"] ?? "";
                string target = context.Request["Target"] ?? "";
                context.Response.Write(QueryChart(fromDate, toDate, checkPoint, target));

            }
            else if (action == "QueryConstChart")
            {
                context.Response.ContentType = "application/json";
                string fromDate = context.Request["fromdate"] ?? "";
                string toDate = context.Request["todate"] ?? "";
                string checkPoint = context.Request["checkPoint"] ?? "";
                string target = context.Request["Target"] ?? "";
                context.Response.Write(QueryConstChart(fromDate, toDate, checkPoint, target));
            }
            else if (action == "QueryMonitor")
            {
                context.Response.ContentType = "application/json";
                string fromDate = context.Request["fromdate"] ?? "";
                string toDate = context.Request["todate"] ?? "";
                context.Response.Write(QueryMonitor(fromDate, toDate));
            }

        }
        /// <summary>
        /// 获取预警数据
        /// </summary>
        /// <param name="m_OCInstrumentInfo_OID"></param>
        private string GetOCInstrumentInfo_ChildsByFID(string m_CheckPointCode)
        {
            string returnValue = string.Empty;
            try
            {
                string sqlwhere = string.Format("CheckPointCode='{0}'", m_CheckPointCode);
                DataSet ds = new EGMNGS.BLL.OCInstrumentInfo_Childs().GetList(sqlwhere);
                returnValue = Newtonsoft.Json.JsonConvert.SerializeObject(ds.Tables[0]);
            }
            catch (Exception ex)
            {
                Common.DotNetCode.LogInstanseHelper.Instance.WriteLog(ex.Message);
                return "";
            }
            return returnValue;
        }

        private string QueryMonitor(string fromDate, string toDate)
        {
            string sql = string.Format(@"select o.*
      ,(CASE when o.[CollectionType]=1 THEN '在线' ELSE '离线' end) CollectionTypeName
      ,oc.BelongEquipmentName,oc.BelongPowerStationName
from OCInstrumentInfoOnline o
LEFT JOIN OCInstrumentInfo oc ON oc.CheckPointCode=o.CheckPointCode where 1=1 ");
            string results = string.Empty;

            string wheresql = string.Empty;

            if (!string.IsNullOrWhiteSpace(fromDate) && !string.IsNullOrWhiteSpace(toDate))
            {
                DateTime dtfrom = DateTime.Parse(fromDate);
                DateTime dtTo = DateTime.Parse(toDate);

                wheresql += string.Format(" and CONVERT(varchar(100), CollectionDatetime, 23) >='{0}' and CONVERT(varchar(100), CollectionDatetime, 23)<='{1}'", dtfrom.ToString("yyyy-MM-dd"), dtTo.ToString("yyyy-MM-dd"));
            }
            else
            {
                sql = sql.Replace("o.*", "top 100 o.* ");
            }


            wheresql += " order by CollectionDatetime desc";


            try
            {

                DataTable tempDatatable = EGMNGS.Common.ComServies.Query(sql + wheresql).Copy();
                results = Newtonsoft.Json.JsonConvert.SerializeObject(tempDatatable);
            }
            catch (Exception ex)
            {
                LogHelper logHelper = new LogHelper();
                logHelper.WriteLog("CRUDHandler.Query:" + ex.Message);
            }

            return results;
        }
        /// <summary>
        ///  获取yAxis
        /// </summary>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <param name="checkPoint"></param>
        /// <param name="target"></param>
        /// <returns></returns>
        private string QueryChart(string fromDate, string toDate, string checkPoint, string target)
        {
            DateTime dtfrom = DateTime.Parse(fromDate);
            DateTime dtTo = DateTime.Parse(toDate);
            var cparr = checkPoint.Split(',');
            string tempcheckPoint = string.Empty;
            tempcheckPoint = cparr.Aggregate("", (current, i) => current + ("'" + i + "',")).Trim(',');

            string sqlwhere = string.Format("CheckPointCode in ({2}) and CONVERT(varchar(100), CollectionDatetime, 23) >='{0}' and CONVERT(varchar(100), CollectionDatetime, 23)<='{1}' order by CollectionDatetime", dtfrom.ToString("yyyy-MM-dd"), dtTo.ToString("yyyy-MM-dd"), tempcheckPoint);

            var returnValue = new { listDatetime = new List<string>(), listChartData = new List<chartData>() };

            EGMNGS.BLL.OCInstrumentInfoOnline bllOnline = new EGMNGS.BLL.OCInstrumentInfoOnline();

            DataTable dt = bllOnline.GetList(sqlwhere).Tables[0];


            List<chartData> listChartData = new List<chartData>();
            List<string> listDatetime = new List<string>();


            foreach (var itemtarget in target.Split(','))
            {
                foreach (var itemcheck in checkPoint.Split(','))
                {
                    listChartData.Add(new chartData() { target = itemtarget, ChackName = itemcheck });
                }

            }

            foreach (DataRow item in dt.Rows)
            {
                listDatetime.Add(item["CollectionDatetime"].ToString());

                foreach (var itemChartData in listChartData)
                {
                    if (itemChartData.target == "Pressure" && itemChartData.ChackName == item["CheckPointCode"].ToString())
                    {
                        itemChartData.data.Add(item["Pressure"].AsTargetType<decimal>(0));
                    }
                    else if (itemChartData.target == "Temperture" && itemChartData.ChackName == item["CheckPointCode"].ToString())
                    {
                        itemChartData.data.Add(item["Temperture"].AsTargetType<decimal>(0));
                    }
                    else if (itemChartData.target == "MicroWater" && itemChartData.ChackName == item["CheckPointCode"].ToString())
                    {
                        itemChartData.data.Add(item["MicroWater"].AsTargetType<decimal>(0));
                    }
                    else if (itemChartData.target == "SO2" && itemChartData.ChackName == item["CheckPointCode"].ToString())
                    {
                        itemChartData.data.Add(item["SO2"].AsTargetType<decimal>(0));
                    }
                    else if (itemChartData.target == "CO" && itemChartData.ChackName == item["CheckPointCode"].ToString())
                    {
                        itemChartData.data.Add(item["CO"].AsTargetType<decimal>(0));
                    }
                    else if (itemChartData.target == "CF4" && itemChartData.ChackName == item["CheckPointCode"].ToString())
                    {
                        itemChartData.data.Add(item["CF4"].AsTargetType<decimal>(0));
                    }
                    else if (itemChartData.target == "SO2F2" && itemChartData.ChackName == item["CheckPointCode"].ToString())
                    {
                        itemChartData.data.Add(item["SO2F2"].AsTargetType<decimal>(0));
                    }
                    else if (itemChartData.target == "SOF2" && itemChartData.ChackName == item["CheckPointCode"].ToString())
                    {
                        itemChartData.data.Add(item["SOF2"].AsTargetType<decimal>(0));
                    }
                    else if (itemChartData.target == "CS2" && itemChartData.ChackName == item["CheckPointCode"].ToString())
                    {
                        itemChartData.data.Add(item["CS2"].AsTargetType<decimal>(0));
                    }
                    else if (itemChartData.target == "HF" && itemChartData.ChackName == item["CheckPointCode"].ToString())
                    {
                        itemChartData.data.Add(item["HF"].AsTargetType<decimal>(0));
                    }
                    else if (itemChartData.target == "H2S" && itemChartData.ChackName == item["CheckPointCode"].ToString())
                    {
                        itemChartData.data.Add(item["H2S"].AsTargetType<decimal>(0));
                    }
                    else if (itemChartData.target == "COSS" && itemChartData.ChackName == item["CheckPointCode"].ToString())
                    {
                        itemChartData.data.Add(item["COSS"].AsTargetType<decimal>(0));
                    }
                    else if (itemChartData.target == "C2F6" && itemChartData.ChackName == item["CheckPointCode"].ToString())
                    {
                        itemChartData.data.Add(item["C2F6"].AsTargetType<decimal>(0));
                    }
                    else if (itemChartData.target == "C3F8" && itemChartData.ChackName == item["CheckPointCode"].ToString())
                    {
                        itemChartData.data.Add(item["C3F8"].AsTargetType<decimal>(0));
                    }
                }
            }

            foreach (var item in listChartData)
            {
                if (item.target == "Pressure")
                {
                    item.target = "压力";
                }
                if (item.target == "Temperture")
                {
                    item.target = "温度";
                }
                if (item.target == "MicroWater")
                {
                    item.target = "微水";
                }
            }

            var chartDataSource = new { listDatetime = listDatetime, listChartData = listChartData };
            return Newtonsoft.Json.JsonConvert.SerializeObject(chartDataSource);
        }

        /// <summary>
        ///  获取电气常量yAxis
        /// </summary>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <param name="checkPoint"></param>
        /// <param name="target"></param>
        /// <returns></returns>
        private string QueryConstChart(string fromDate, string toDate, string checkPoint, string target)
        {
            DateTime dtfrom = DateTime.Parse(fromDate);
            DateTime dtTo = DateTime.Parse(toDate);
            var cparr = checkPoint.Split(',');
            string tempcheckPoint = string.Empty;
            tempcheckPoint = cparr.Aggregate("", (current, i) => current + ("'" + i + "',")).Trim(',');

            string sqlwhere = string.Format("CheckPointCode in ({2}) and CONVERT(varchar(100), CollectionDatetime, 23) >='{0}' and CONVERT(varchar(100), CollectionDatetime, 23)<='{1}' order by CollectionDatetime", dtfrom.ToString("yyyy-MM-dd"), dtTo.ToString("yyyy-MM-dd"), tempcheckPoint);

            var returnValue = new { listDatetime = new List<string>(), listChartData = new List<chartData>() };

            EGMNGS.BLL.OCInstrumentElectricalData bllOnline = new EGMNGS.BLL.OCInstrumentElectricalData();

            DataTable dt = bllOnline.GetList(sqlwhere).Tables[0];


            List<chartData> listChartData = new List<chartData>();
            List<string> listDatetime = new List<string>();

            foreach (var itemtarget in target.Split(','))
            {
                foreach (var itemcheck in checkPoint.Split(','))
                {
                    listChartData.Add(new chartData() { target = itemtarget, ChackName = itemcheck });
                }

            }

            foreach (DataRow item in dt.Rows)
            {
                listDatetime.Add(item["CollectionDatetime"].ToString());

                foreach (var itemChartData in listChartData)
                {
                    if (itemChartData.target == "Pressure" && itemChartData.ChackName == item["CheckPointCode"].ToString())
                    {
                        itemChartData.data.Add(item["Pressure"].AsTargetType<decimal>(0));
                    }
                    else if (itemChartData.target == "Temperture" && itemChartData.ChackName == item["CheckPointCode"].ToString())
                    {
                        itemChartData.data.Add(item["Temperture"].AsTargetType<decimal>(0));
                    }
                    else if (itemChartData.target == "OutElectricity" && itemChartData.ChackName == item["CheckPointCode"].ToString())
                    {
                        itemChartData.data.Add(item["OutElectricity"].AsTargetType<decimal>(0));
                    }
                    else if (itemChartData.target == "CurrentVal" && itemChartData.ChackName == item["CheckPointCode"].ToString())
                    {
                        itemChartData.data.Add(item["CurrentVal"].AsTargetType<decimal>(0));
                    }
                    else if (itemChartData.target == "Voltage" && itemChartData.ChackName == item["CheckPointCode"].ToString())
                    {
                        itemChartData.data.Add(item["Voltage"].AsTargetType<decimal>(0));
                    }
                    else if (itemChartData.target == "PowerVal" && itemChartData.ChackName == item["CheckPointCode"].ToString())
                    {
                        itemChartData.data.Add(item["PowerVal"].AsTargetType<decimal>(0));
                    }

                }
            }

            foreach (var item in listChartData)
            {
                if (item.target == "Pressure")
                {
                    item.target = "压力";
                }
                else if (item.target == "Temperture")
                {
                    item.target = "温度";
                }
                else if (item.target == "OutElectricity")
                {
                    item.target = "放电量";
                }
                else if (item.target == "CurrentVal")
                {
                    item.target = "电流";
                }
                else if (item.target == "PowerVal")
                {
                    item.target = "功率";
                }
                else if (item.target == "Voltage")
                {
                    item.target = "电压";
                }

            }

            var chartDataSource = new { listDatetime = listDatetime, listChartData = listChartData };
           
            return Newtonsoft.Json.JsonConvert.SerializeObject(chartDataSource);
        }


        private int Delete(string oid, string table)
        {
            string sqlDelete = string.Format("delete {0} where oid={1}", table, oid);
            try
            {

                int seccuss = Maticsoft.DBUtility.DbHelperSQL.ExecuteSql(sqlDelete);
                return seccuss;


            }
            catch (Exception ex)
            {
                Maticsoft.DBUtility.LogHelper log = new Maticsoft.DBUtility.LogHelper();

                log.WriteLog(ex.Message);
                return 0;

            }

        }

        private string Query(string tableName, string where)
        {
            string sql = string.Format(@"select [OID]
      ,[CheckPointCode]
      ,[CheckPointName]
      ,[Pressure]
      ,[Temperture]
      ,[MicroWater]
      ,[SO2]
      ,[CO]
      ,[CF4]
      ,[SO2F2]
      ,[SOF2]
      ,[CS2]
      ,[HF]
      ,(CASE when [CollectionType]=1 THEN '在线' ELSE '离线' end) CollectionType
      ,[CollectionDatetime]
      ,[CollectionBy],[H2S]
      ,[COSS]
      ,[C2F6]
      ,[C3F8] from {0}", tableName);
            string results = string.Empty;

            if (!string.IsNullOrWhiteSpace(where))
            {
                sql += " where " + where + " order by CheckPointName,CollectionDatetime desc";
            }
            try
            {

                DataTable tempDatatable = EGMNGS.Common.ComServies.Query(sql).Copy();
                results = Newtonsoft.Json.JsonConvert.SerializeObject(tempDatatable);
            }
            catch (Exception ex)
            {
                LogHelper logHelper = new LogHelper();
                logHelper.WriteLog("CRUDHandler.Query:" + ex.Message);

            }

            return results;

        }
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}
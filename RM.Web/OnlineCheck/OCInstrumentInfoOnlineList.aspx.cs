﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EGMNGS.Model;
using System.Data;
using RM.Web.App_Code;

namespace RM.Web.OnlineCheck
{
    public class chartData
    {
        private string _name;

        public string name
        {
            set { _name = value; }
            get
            {
                if (string.IsNullOrWhiteSpace(_name))
                {
                    return ChackName + "_" + target;
                }
                else
                {
                    return _name;
                }
            }
        }

        public string ChackName { set; get; }
        public string target { get; set; }
        public List<decimal> data { set; get; }
        public chartData()
        {
            data = new List<decimal>();
        }
    }

    public partial class OCInstrumentInfoOnlineList : PageBase
    {
        private readonly EGMNGS.BLL.OCInstrumentInfo bll = new EGMNGS.BLL.OCInstrumentInfo();
        private readonly EGMNGS.BLL.InfoDevPara bllInfo = new EGMNGS.BLL.InfoDevPara();
        private readonly EGMNGS.BLL.OCInstrumentInfoOnline bllOnline = new EGMNGS.BLL.OCInstrumentInfoOnline();
        public string listDatetimeJson { set; get; }
        public string listChartDataJson { set; get; }
       
        public OCInstrumentInfo OCInstrumentInfoObj
        {
            get
            {
                if (ViewState["OCInstrumentInfoObj"] == null)
                {
                    ViewState["OCInstrumentInfoObj"] = new OCInstrumentInfo();
                }

                return ViewState["OCInstrumentInfoObj"] as OCInstrumentInfo;
            }
            set { ViewState["OCInstrumentInfoObj"] = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                var checkPointCode = Request["checkPointCode"] ?? "";
                var listModel = bll.GetModelList(string.Format("CheckPointCode='{0}'", checkPointCode));
                if (listModel != null && listModel.Count > 0)
                {
                    OCInstrumentInfoObj = listModel[0];
                }

                if (!string.IsNullOrWhiteSpace(OCInstrumentInfoObj.BelongEquipment))
                {
                    var listInfoDevPara = bllInfo.GetModelList(string.Format("DevCode='{0}'", OCInstrumentInfoObj.BelongEquipment));
                    if (listInfoDevPara.Count > 0)
                    {
                        OCInstrumentInfoObj.BelongEquipmentName = listInfoDevPara[0].DevName;
                        OCInstrumentInfoObj.BelongPowerStationName = listInfoDevPara[0].ConvertStationName;
                    }
                }

            }

          //  DataBindGrid();
        }

        protected override void OnLoad(EventArgs e)
        {
            CheckURLPermission = true;
            base.OnLoad(e);
        }
    
    }
}
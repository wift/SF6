﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;

namespace RM.Web.OnlineCheck
{
    public partial class OnlineControlPage_Left : System.Web.UI.Page
    {
        public StringBuilder treeItem_Table = new StringBuilder();
       
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                GetTreeTable();
            }
        }

        // <summary>
        /// 所以变电站设置信息
        /// </summary>
        public void GetTreeTable()
        {
            DataSet dtroot = Maticsoft.DBUtility.DbHelperSQL.Query("select DISTINCT PowerSupplyCode,PowerSupplyName from [EGMNGS].[dbo].[InfoDevPara] where IsOnlineCheck='0'");
           
            foreach (DataRow drv in dtroot.Tables[0].Rows)
            {
                DataSet dssecond = Maticsoft.DBUtility.DbHelperSQL.Query(string.Format("select DISTINCT  [ConvertStationCode],[ConvertStationName] from [EGMNGS].[dbo].[InfoDevPara] where PowerSupplyCode='{0}' and IsOnlineCheck='0'", drv["PowerSupplyCode"]));

                treeItem_Table.Append("<li>");
                treeItem_Table.Append("<div onclick=\"GetTable('" + drv["PowerSupplyCode"] + "')\">" + drv["PowerSupplyName"] + "</div>");
                if (dssecond.Tables.Count>0&&dssecond.Tables[0].Rows.Count>0)
                {
                    treeItem_Table.Append("<ul>");

                    foreach (DataRow item in dssecond.Tables[0].Rows)
                    {
                        treeItem_Table.Append("<li>");
                        treeItem_Table.Append("<div onclick=\"GetTable('" + item["ConvertStationCode"] + "')\">" + item["ConvertStationName"] + "</div>");

                        DataSet dsThree = Maticsoft.DBUtility.DbHelperSQL.Query(string.Format("select DISTINCT  [DevCode],[DevName] from [EGMNGS].[dbo].[InfoDevPara] where ConvertStationCode='{0}' and IsOnlineCheck='0'", item["ConvertStationCode"]));
                       
                        if (dsThree.Tables.Count > 0 && dsThree.Tables[0].Rows.Count > 0)
                        {
                            treeItem_Table.Append("<ul>");
                            foreach (DataRow itemThree in dsThree.Tables[0].Rows)
                            {
                                treeItem_Table.Append("<li><div onclick=\"GetTable('" + itemThree["DevCode"] + "')\"><img src=\"/Themes/Images/20130502112716785_easyicon_net_16.png\" width=\"16\" height=\"16\" />" + itemThree["DevName"] + "</div></li>");
                  
                            }
                            treeItem_Table.Append("</ul>");
                        }
                        treeItem_Table.Append("</li>");
                    }

               
                    treeItem_Table.Append("</ul>");
                }
              
                treeItem_Table.Append("</li>");
            }
        }

        
    }
}
﻿//------------------------------------------------------------------------------
// <自动生成>
//     此代码由工具生成。
//
//     对此文件的更改可能会导致不正确的行为，并且如果
//     重新生成代码，这些更改将会丢失。 
// </自动生成>
//------------------------------------------------------------------------------

namespace RM.Web.OnlineCheck {
    
    
    public partial class OnlineControlPageEdit {
        
        /// <summary>
        /// form1 控件。
        /// </summary>
        /// <remarks>
        /// 自动生成的字段。
        /// 若要进行修改，请将字段声明从设计器文件移到代码隐藏文件。
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlForm form1;
        
        /// <summary>
        /// ScriptManager1 控件。
        /// </summary>
        /// <remarks>
        /// 自动生成的字段。
        /// 若要进行修改，请将字段声明从设计器文件移到代码隐藏文件。
        /// </remarks>
        protected global::System.Web.UI.ScriptManager ScriptManager1;
        
        /// <summary>
        /// UpdatePanel 控件。
        /// </summary>
        /// <remarks>
        /// 自动生成的字段。
        /// 若要进行修改，请将字段声明从设计器文件移到代码隐藏文件。
        /// </remarks>
        protected global::System.Web.UI.UpdatePanel UpdatePanel;
        
        /// <summary>
        /// tbAppCode 控件。
        /// </summary>
        /// <remarks>
        /// 自动生成的字段。
        /// 若要进行修改，请将字段声明从设计器文件移到代码隐藏文件。
        /// </remarks>
        protected global::RM.Web.ExControl.ExTextBox tbAppCode;
        
        /// <summary>
        /// txtCheckPointName 控件。
        /// </summary>
        /// <remarks>
        /// 自动生成的字段。
        /// 若要进行修改，请将字段声明从设计器文件移到代码隐藏文件。
        /// </remarks>
        protected global::RM.Web.ExControl.ExTextBox txtCheckPointName;
        
        /// <summary>
        /// txtInstrumentCode 控件。
        /// </summary>
        /// <remarks>
        /// 自动生成的字段。
        /// 若要进行修改，请将字段声明从设计器文件移到代码隐藏文件。
        /// </remarks>
        protected global::RM.Web.ExControl.ExTextBox txtInstrumentCode;
        
        /// <summary>
        /// txtInstrumentName 控件。
        /// </summary>
        /// <remarks>
        /// 自动生成的字段。
        /// 若要进行修改，请将字段声明从设计器文件移到代码隐藏文件。
        /// </remarks>
        protected global::RM.Web.ExControl.ExTextBox txtInstrumentName;
        
        /// <summary>
        /// ddlRunStatus 控件。
        /// </summary>
        /// <remarks>
        /// 自动生成的字段。
        /// 若要进行修改，请将字段声明从设计器文件移到代码隐藏文件。
        /// </remarks>
        protected global::RM.Web.ExControl.ExDropDownList ddlRunStatus;
        
        /// <summary>
        /// txtIntallPostion 控件。
        /// </summary>
        /// <remarks>
        /// 自动生成的字段。
        /// 若要进行修改，请将字段声明从设计器文件移到代码隐藏文件。
        /// </remarks>
        protected global::RM.Web.ExControl.ExTextBox txtIntallPostion;
        
        /// <summary>
        /// dllVoltageLevel 控件。
        /// </summary>
        /// <remarks>
        /// 自动生成的字段。
        /// 若要进行修改，请将字段声明从设计器文件移到代码隐藏文件。
        /// </remarks>
        protected global::RM.Web.ExControl.ExDropDownList dllVoltageLevel;
        
        /// <summary>
        /// txtManufacturer 控件。
        /// </summary>
        /// <remarks>
        /// 自动生成的字段。
        /// 若要进行修改，请将字段声明从设计器文件移到代码隐藏文件。
        /// </remarks>
        protected global::RM.Web.ExControl.ExTextBox txtManufacturer;
        
        /// <summary>
        /// txtRunDate 控件。
        /// </summary>
        /// <remarks>
        /// 自动生成的字段。
        /// 若要进行修改，请将字段声明从设计器文件移到代码隐藏文件。
        /// </remarks>
        protected global::RM.Web.ExControl.ExTextBox txtRunDate;
        
        /// <summary>
        /// txtCalibrationDate 控件。
        /// </summary>
        /// <remarks>
        /// 自动生成的字段。
        /// 若要进行修改，请将字段声明从设计器文件移到代码隐藏文件。
        /// </remarks>
        protected global::RM.Web.ExControl.ExTextBox txtCalibrationDate;
        
        /// <summary>
        /// btnSave 控件。
        /// </summary>
        /// <remarks>
        /// 自动生成的字段。
        /// 若要进行修改，请将字段声明从设计器文件移到代码隐藏文件。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button btnSave;
        
        /// <summary>
        /// ddlControlRunStatus 控件。
        /// </summary>
        /// <remarks>
        /// 自动生成的字段。
        /// 若要进行修改，请将字段声明从设计器文件移到代码隐藏文件。
        /// </remarks>
        protected global::RM.Web.ExControl.ExDropDownList ddlControlRunStatus;
        
        /// <summary>
        /// txtControlRunFromDate 控件。
        /// </summary>
        /// <remarks>
        /// 自动生成的字段。
        /// 若要进行修改，请将字段声明从设计器文件移到代码隐藏文件。
        /// </remarks>
        protected global::RM.Web.ExControl.ExTextBox txtControlRunFromDate;
        
        /// <summary>
        /// txtControlRunToDate 控件。
        /// </summary>
        /// <remarks>
        /// 自动生成的字段。
        /// 若要进行修改，请将字段声明从设计器文件移到代码隐藏文件。
        /// </remarks>
        protected global::RM.Web.ExControl.ExTextBox txtControlRunToDate;
        
        /// <summary>
        /// txtCycle 控件。
        /// </summary>
        /// <remarks>
        /// 自动生成的字段。
        /// 若要进行修改，请将字段声明从设计器文件移到代码隐藏文件。
        /// </remarks>
        protected global::RM.Web.ExControl.ExTextBox txtCycle;
        
        /// <summary>
        /// txtControlPressureMin 控件。
        /// </summary>
        /// <remarks>
        /// 自动生成的字段。
        /// 若要进行修改，请将字段声明从设计器文件移到代码隐藏文件。
        /// </remarks>
        protected global::RM.Web.ExControl.ExTextBox txtControlPressureMin;
        
        /// <summary>
        /// txtControlPressureMins 控件。
        /// </summary>
        /// <remarks>
        /// 自动生成的字段。
        /// 若要进行修改，请将字段声明从设计器文件移到代码隐藏文件。
        /// </remarks>
        protected global::RM.Web.ExControl.ExTextBox txtControlPressureMins;
        
        /// <summary>
        /// txtControlPressureMax 控件。
        /// </summary>
        /// <remarks>
        /// 自动生成的字段。
        /// 若要进行修改，请将字段声明从设计器文件移到代码隐藏文件。
        /// </remarks>
        protected global::RM.Web.ExControl.ExTextBox txtControlPressureMax;
        
        /// <summary>
        /// txtControlPressureMaxs 控件。
        /// </summary>
        /// <remarks>
        /// 自动生成的字段。
        /// 若要进行修改，请将字段声明从设计器文件移到代码隐藏文件。
        /// </remarks>
        protected global::RM.Web.ExControl.ExTextBox txtControlPressureMaxs;
        
        /// <summary>
        /// txtControlTempertureMin 控件。
        /// </summary>
        /// <remarks>
        /// 自动生成的字段。
        /// 若要进行修改，请将字段声明从设计器文件移到代码隐藏文件。
        /// </remarks>
        protected global::RM.Web.ExControl.ExTextBox txtControlTempertureMin;
        
        /// <summary>
        /// txtControlTempertureMins 控件。
        /// </summary>
        /// <remarks>
        /// 自动生成的字段。
        /// 若要进行修改，请将字段声明从设计器文件移到代码隐藏文件。
        /// </remarks>
        protected global::RM.Web.ExControl.ExTextBox txtControlTempertureMins;
        
        /// <summary>
        /// txtControlTempertureMax 控件。
        /// </summary>
        /// <remarks>
        /// 自动生成的字段。
        /// 若要进行修改，请将字段声明从设计器文件移到代码隐藏文件。
        /// </remarks>
        protected global::RM.Web.ExControl.ExTextBox txtControlTempertureMax;
        
        /// <summary>
        /// txtControlTempertureMaxs 控件。
        /// </summary>
        /// <remarks>
        /// 自动生成的字段。
        /// 若要进行修改，请将字段声明从设计器文件移到代码隐藏文件。
        /// </remarks>
        protected global::RM.Web.ExControl.ExTextBox txtControlTempertureMaxs;
        
        /// <summary>
        /// txtControlMicroWater 控件。
        /// </summary>
        /// <remarks>
        /// 自动生成的字段。
        /// 若要进行修改，请将字段声明从设计器文件移到代码隐藏文件。
        /// </remarks>
        protected global::RM.Web.ExControl.ExTextBox txtControlMicroWater;
        
        /// <summary>
        /// txtControlMicroWaters 控件。
        /// </summary>
        /// <remarks>
        /// 自动生成的字段。
        /// 若要进行修改，请将字段声明从设计器文件移到代码隐藏文件。
        /// </remarks>
        protected global::RM.Web.ExControl.ExTextBox txtControlMicroWaters;
        
        /// <summary>
        /// txtControlSO2 控件。
        /// </summary>
        /// <remarks>
        /// 自动生成的字段。
        /// 若要进行修改，请将字段声明从设计器文件移到代码隐藏文件。
        /// </remarks>
        protected global::RM.Web.ExControl.ExTextBox txtControlSO2;
        
        /// <summary>
        /// txtControlSO2s 控件。
        /// </summary>
        /// <remarks>
        /// 自动生成的字段。
        /// 若要进行修改，请将字段声明从设计器文件移到代码隐藏文件。
        /// </remarks>
        protected global::RM.Web.ExControl.ExTextBox txtControlSO2s;
        
        /// <summary>
        /// txtControlCO 控件。
        /// </summary>
        /// <remarks>
        /// 自动生成的字段。
        /// 若要进行修改，请将字段声明从设计器文件移到代码隐藏文件。
        /// </remarks>
        protected global::RM.Web.ExControl.ExTextBox txtControlCO;
        
        /// <summary>
        /// txtControlCOs 控件。
        /// </summary>
        /// <remarks>
        /// 自动生成的字段。
        /// 若要进行修改，请将字段声明从设计器文件移到代码隐藏文件。
        /// </remarks>
        protected global::RM.Web.ExControl.ExTextBox txtControlCOs;
        
        /// <summary>
        /// txtControlCF4 控件。
        /// </summary>
        /// <remarks>
        /// 自动生成的字段。
        /// 若要进行修改，请将字段声明从设计器文件移到代码隐藏文件。
        /// </remarks>
        protected global::RM.Web.ExControl.ExTextBox txtControlCF4;
        
        /// <summary>
        /// txtControlCF4s 控件。
        /// </summary>
        /// <remarks>
        /// 自动生成的字段。
        /// 若要进行修改，请将字段声明从设计器文件移到代码隐藏文件。
        /// </remarks>
        protected global::RM.Web.ExControl.ExTextBox txtControlCF4s;
        
        /// <summary>
        /// txtControlSO2F2 控件。
        /// </summary>
        /// <remarks>
        /// 自动生成的字段。
        /// 若要进行修改，请将字段声明从设计器文件移到代码隐藏文件。
        /// </remarks>
        protected global::RM.Web.ExControl.ExTextBox txtControlSO2F2;
        
        /// <summary>
        /// txtControlSO2F2s 控件。
        /// </summary>
        /// <remarks>
        /// 自动生成的字段。
        /// 若要进行修改，请将字段声明从设计器文件移到代码隐藏文件。
        /// </remarks>
        protected global::RM.Web.ExControl.ExTextBox txtControlSO2F2s;
        
        /// <summary>
        /// txtControlSOF2 控件。
        /// </summary>
        /// <remarks>
        /// 自动生成的字段。
        /// 若要进行修改，请将字段声明从设计器文件移到代码隐藏文件。
        /// </remarks>
        protected global::RM.Web.ExControl.ExTextBox txtControlSOF2;
        
        /// <summary>
        /// txtControlSOF2s 控件。
        /// </summary>
        /// <remarks>
        /// 自动生成的字段。
        /// 若要进行修改，请将字段声明从设计器文件移到代码隐藏文件。
        /// </remarks>
        protected global::RM.Web.ExControl.ExTextBox txtControlSOF2s;
        
        /// <summary>
        /// txtControlCS2 控件。
        /// </summary>
        /// <remarks>
        /// 自动生成的字段。
        /// 若要进行修改，请将字段声明从设计器文件移到代码隐藏文件。
        /// </remarks>
        protected global::RM.Web.ExControl.ExTextBox txtControlCS2;
        
        /// <summary>
        /// txtControlCS2s 控件。
        /// </summary>
        /// <remarks>
        /// 自动生成的字段。
        /// 若要进行修改，请将字段声明从设计器文件移到代码隐藏文件。
        /// </remarks>
        protected global::RM.Web.ExControl.ExTextBox txtControlCS2s;
        
        /// <summary>
        /// txtControlHF 控件。
        /// </summary>
        /// <remarks>
        /// 自动生成的字段。
        /// 若要进行修改，请将字段声明从设计器文件移到代码隐藏文件。
        /// </remarks>
        protected global::RM.Web.ExControl.ExTextBox txtControlHF;
        
        /// <summary>
        /// txtControlHFs 控件。
        /// </summary>
        /// <remarks>
        /// 自动生成的字段。
        /// 若要进行修改，请将字段声明从设计器文件移到代码隐藏文件。
        /// </remarks>
        protected global::RM.Web.ExControl.ExTextBox txtControlHFs;
        
        /// <summary>
        /// txtPH2S 控件。
        /// </summary>
        /// <remarks>
        /// 自动生成的字段。
        /// 若要进行修改，请将字段声明从设计器文件移到代码隐藏文件。
        /// </remarks>
        protected global::RM.Web.ExControl.ExTextBox txtPH2S;
        
        /// <summary>
        /// txtMH2S 控件。
        /// </summary>
        /// <remarks>
        /// 自动生成的字段。
        /// 若要进行修改，请将字段声明从设计器文件移到代码隐藏文件。
        /// </remarks>
        protected global::RM.Web.ExControl.ExTextBox txtMH2S;
        
        /// <summary>
        /// txtPCOS 控件。
        /// </summary>
        /// <remarks>
        /// 自动生成的字段。
        /// 若要进行修改，请将字段声明从设计器文件移到代码隐藏文件。
        /// </remarks>
        protected global::RM.Web.ExControl.ExTextBox txtPCOS;
        
        /// <summary>
        /// txtMCOS 控件。
        /// </summary>
        /// <remarks>
        /// 自动生成的字段。
        /// 若要进行修改，请将字段声明从设计器文件移到代码隐藏文件。
        /// </remarks>
        protected global::RM.Web.ExControl.ExTextBox txtMCOS;
        
        /// <summary>
        /// txtPC2F6 控件。
        /// </summary>
        /// <remarks>
        /// 自动生成的字段。
        /// 若要进行修改，请将字段声明从设计器文件移到代码隐藏文件。
        /// </remarks>
        protected global::RM.Web.ExControl.ExTextBox txtPC2F6;
        
        /// <summary>
        /// txtMC2F6 控件。
        /// </summary>
        /// <remarks>
        /// 自动生成的字段。
        /// 若要进行修改，请将字段声明从设计器文件移到代码隐藏文件。
        /// </remarks>
        protected global::RM.Web.ExControl.ExTextBox txtMC2F6;
        
        /// <summary>
        /// txtPC3F8 控件。
        /// </summary>
        /// <remarks>
        /// 自动生成的字段。
        /// 若要进行修改，请将字段声明从设计器文件移到代码隐藏文件。
        /// </remarks>
        protected global::RM.Web.ExControl.ExTextBox txtPC3F8;
        
        /// <summary>
        /// txtMC3F8 控件。
        /// </summary>
        /// <remarks>
        /// 自动生成的字段。
        /// 若要进行修改，请将字段声明从设计器文件移到代码隐藏文件。
        /// </remarks>
        protected global::RM.Web.ExControl.ExTextBox txtMC3F8;
        
        /// <summary>
        /// txtAlterCount 控件。
        /// </summary>
        /// <remarks>
        /// 自动生成的字段。
        /// 若要进行修改，请将字段声明从设计器文件移到代码隐藏文件。
        /// </remarks>
        protected global::RM.Web.ExControl.ExTextBox txtAlterCount;
        
        /// <summary>
        /// btnSaveLog 控件。
        /// </summary>
        /// <remarks>
        /// 自动生成的字段。
        /// 若要进行修改，请将字段声明从设计器文件移到代码隐藏文件。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button btnSaveLog;
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using EGMNGS.Common;

namespace RM.Web.OnlineCheck
{

    public partial class OnlineShowPage : System.Web.UI.Page
    {
        public EGMNGS.Model.InfoDevPara InfoDevParaObj
        {
            get
            {
                if (ViewState["InfoDevParaObj"] == null)
                {
                    ViewState["InfoDevParaObj"] = new EGMNGS.Model.InfoDevPara();
                }

                return ViewState["InfoDevParaObj"] as EGMNGS.Model.InfoDevPara;
            }
            set { ViewState["InfoDevParaObj"] = value; }
        }
        private readonly EGMNGS.BLL.OCInstrumentInfo bll = new EGMNGS.BLL.OCInstrumentInfo();
        private readonly EGMNGS.BLL.OCInstrumentInfoOnline bllOnline = new EGMNGS.BLL.OCInstrumentInfoOnline();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                DropDownListBinder();
                DataBindGrid();

            }

        }

        private void DropDownListBinder()
        {
            EGMNGS.BLL.SYS_CODE sysCodeBll = new EGMNGS.BLL.SYS_CODE();
            DataSet dsYear = sysCodeBll.GetList("CODE_TYPE='DEVClass' ORDER BY DISPLAY_ORDER");

            this.ddlDevClass.DataSource = dsYear.Tables[0];
            this.ddlDevClass.DataTextField = "CODE_CHI_DESC";
            this.ddlDevClass.DataValueField = "CODE";
            this.ddlDevClass.DataBind();
            this.ddlDevClass.Items.Insert(0, string.Empty);

            DataSet dsMonth = sysCodeBll.GetList("CODE_TYPE='DYLev' ORDER BY DISPLAY_ORDER");

            this.ddlVoltageLevel.DataSource = dsMonth.Tables[0];
            this.ddlVoltageLevel.DataTextField = "CODE_CHI_DESC";
            this.ddlVoltageLevel.DataValueField = "CODE";
            this.ddlVoltageLevel.DataBind();
            this.ddlVoltageLevel.Items.Insert(0, string.Empty);


            DataTable dtPowerStation = ComServies.GetAllPowerStation();
            this.ddlPowerSupplyName.DataSource = dtPowerStation;
            this.ddlPowerSupplyName.DataTextField = "Organization_Name";
            this.ddlPowerSupplyName.DataValueField = "Organization_ID";
            this.ddlPowerSupplyName.DataBind();
            this.ddlPowerSupplyName.Items.Insert(0, string.Empty);

        }


        protected void ddlPowerSupplyName_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadPowerSupplyName();
        }

        private void LoadPowerSupplyName()
        {
            this.ddlConvertStationName.Items.Clear();
            EGMNGS.BLL.InfoPowerSupplyConvertStation bll = new EGMNGS.BLL.InfoPowerSupplyConvertStation();
            DataSet dsPowerSupplyCode = bll.GetList(string.Format("PowerSupplyCode='{0}'", this.ddlPowerSupplyName.SelectedValue));
            this.ddlConvertStationName.DataSource = dsPowerSupplyCode.Tables[0];
            this.ddlConvertStationName.DataTextField = "ConvartStationName";
            this.ddlConvertStationName.DataValueField = "CovnertStationCode";
            this.ddlConvertStationName.DataBind();
            this.ddlConvertStationName.Items.Insert(0, string.Empty);
        }
        /// <summary>
        /// 绑定数据源
        /// </summary>
        private void DataBindGrid()
        {
            string sqlSearchwhere = string.Format(@" SELECT 
      [DevCode]
  FROM [EGMNGS].[dbo].[InfoDevPara] ");
            string where = "where 1=1";



            if (this.ddlPowerSupplyName.SelectedIndex > 0)
            {
                where += string.Format(" and PowerSupplyCode='{0}' ", this.ddlPowerSupplyName.SelectedValue);
            }


            if (ddlConvertStationName.SelectedIndex > 0)
            {
                where += string.Format(" and ConvertStationCode='{0}' ", ddlConvertStationName.SelectedValue);
            }
            if (this.ddlDevClass.SelectedIndex > 0)
            {
                where += string.Format(" and DevClass='{0}' ", ddlDevClass.SelectedValue);
            }

            if (this.ddlVoltageLevel.SelectedIndex > 0)
            {
                where += string.Format(" and VoltageLevel='{0}' ", ddlVoltageLevel.SelectedValue);
            }
            sqlSearchwhere += where;

            DataTable dt = bll.GetList(string.Format("BelongEquipment IN ({0}) order by CheckPointCode", sqlSearchwhere)).Tables[0];
            this.rp_Item.DataSource = dt;
            this.rp_Item.DataBind();

            string sqlOCInstrumentInfo = string.Format(@"SELECT 
                                              [CheckPointCode]
                                          FROM [EGMNGS].[dbo].[OCInstrumentInfo] where 1=1 and {0}", string.Format("BelongEquipment IN ({0})", sqlSearchwhere));

            string sqltemp = string.Format(@"SELECT *,[ControlPressureMin]
      ,[ControlPressureMax]
      ,[ControlTempertureMin]
      ,[ControlTempertureMax]
      ,[ControlMicroWater]
      ,[ControlSO2]
      ,[ControlCO]
      ,[ControlCF4]
      ,[ControlSO2F2]
      ,[ControlSOF2]
      ,[ControlCS2]
      ,[ControlHF]
      ,[ControlPressureMins]
      ,[ControlPressureMaxs]
      ,[ControlTempertureMins]
      ,[ControlTempertureMaxs]
      ,[ControlMicroWaters]
      ,[ControlSO2s]
      ,[ControlCOs]
      ,[ControlCF4s]
      ,[ControlSO2F2s]
      ,[ControlSOF2s]
      ,[ControlCS2s]
      ,[ControlHFs]
      ,[AlterCount]
      ,[PH2S]
      ,[MH2S]
      ,[PCOS]
      ,[MCOS]
      ,[PC2F6]
      ,[MC2F6]
      ,[PC3F8]
      ,[MC3F8] FROM OCInstrumentInfoOnline oio 
INNER JOIN (
SELECT CheckPointCode,MAX(CollectionDatetime) as CollectionDatetime 
FROM OCInstrumentInfoOnline 
WHERE 
CheckPointCode in 

({0}
)

  GROUP BY CheckPointCode
) joinoio ON joinoio.CheckPointCode=oio.CheckPointCode AND joinoio.CollectionDatetime=oio.CollectionDatetime
left join OCInstrumentInfo oic on oic.CheckPointCode=oio.CheckPointCode
 order by oio.CheckPointCode ", sqlOCInstrumentInfo);

            DataTable tempDt = EGMNGS.Common.ComServies.Query(sqltemp).Copy();


            this.Repeater1.DataSource = tempDt;
            this.Repeater1.DataBind();

        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            DataBindGrid();
        }

        public string GetDataWithColor(object min,object mid,object max)
        {

            decimal m_min = min.AsTargetType<decimal>(0);
            decimal m_mid = mid.AsTargetType<decimal>(0);
            decimal m_max = max.AsTargetType<decimal>(0);

            string color = "black";

            if (m_mid> m_min)
            {
                color = "#FFCC00";
            }
            else if (m_mid> m_max)
            {
                color = "red";
            }
            

            return string.Format("<span style=\"color:{0}\">{1}</span>", color, mid);
        }
    }
}
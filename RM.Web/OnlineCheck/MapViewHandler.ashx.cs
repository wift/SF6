﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using EGMNGS.Model;
using RM.Common.DotNetData;

namespace RM.Web.OnlineCheck
{
    public class InfoPowerSupplyConvertStationBean : InfoPowerSupplyConvertStation
    {
        public List<OCInstrumentInfoBean> listOCInstrumentInfoOnline { set; get; }
    }

    public class OCInstrumentInfoBean : OCInstrumentInfo
    {
        public DateTime CollectionDatetime { set; get; }
    }
    /// <summary>
    /// MapViewHandler 的摘要说明
    /// </summary>
    public class MapViewHandler : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";

            string action = context.Request["action"] ?? "";
                
            if (action == "GetConvartStationName")//获取所有变电站
            {
                DataTable dt = GetConvartStationName();
                var temlist = DataTableHelper.DataTableToIList<InfoPowerSupplyConvertStationBean>(dt);
                foreach (InfoPowerSupplyConvertStationBean item in temlist)
                {
                    DataTable dtc = GetOCInstrumentInfoOnlineAndConStationByStationCode(item.CovnertStationCode);
                    item.listOCInstrumentInfoOnline = DataTableHelper.DataTableToIList<OCInstrumentInfoBean>(dtc) as List<OCInstrumentInfoBean>;
                }
                string returnJsonStr = context.Request["callback"] + "(" + Newtonsoft.Json.JsonConvert.SerializeObject(temlist) + ");";
                context.Response.Write(returnJsonStr);
                context.Response.Flush();
            }
         
        }
        /// <summary>
        /// 获取所有变电站
        /// </summary>
        /// <returns></returns>
        private DataTable GetConvartStationName()
        {
            DataTable dt = null;

            string sqltemp = string.Format(@"select s.*
                                             from InfoPowerSupplyConvertStation s
                                            where s.IsOnlineCheck='0' and Longitude is not null and Latitude is not null");

            dt = EGMNGS.Common.ComServies.Query(sqltemp).Copy();

            return dt;
        }


        /// <summary>
        /// 显示测点信息到地图
        /// </summary>
        /// <returns></returns>
        private DataTable GetOCInstrumentInfoOnlineAndConStationByStationCode(string p_convartStationCode)
        {
            DataTable dt = null;

            string sqltemp = string.Format(@"SELECT   oic.CheckPointCode,oic.CheckPointName,
  oio.CollectionDatetime

  ,[ControlPressureMin]
      ,[ControlPressureMax]
      ,[ControlTempertureMin]
      ,[ControlTempertureMax]
      ,[ControlMicroWater]
      ,[ControlSO2]
      ,[ControlCO]
      ,[ControlCF4]
      ,[ControlSO2F2]
      ,[ControlSOF2]
      ,[ControlCS2]
      ,[ControlHF]
      ,[ControlPressureMins]
      ,[ControlPressureMaxs]
      ,[ControlTempertureMins]
      ,[ControlTempertureMaxs]
      ,[ControlMicroWaters]
      ,[ControlSO2s]
      ,[ControlCOs]
      ,[ControlCF4s]
      ,[ControlSO2F2s]
      ,[ControlSOF2s]
      ,[ControlCS2s]
      ,[ControlHFs]
      ,[AlterCount]
      ,[PH2S]
      ,[MH2S]
      ,[PCOS]
      ,[MCOS]
      ,[PC2F6]
      ,[MC2F6]
      ,[PC3F8]
      ,[MC3F8] FROM OCInstrumentInfoOnline oio 
INNER JOIN (
SELECT CheckPointCode,MAX(CollectionDatetime) as CollectionDatetime 
FROM OCInstrumentInfoOnline 
WHERE 
CheckPointCode in 

(
  SELECT 
     [CheckPointCode]
 FROM [EGMNGS].[dbo].[OCInstrumentInfo]
 where  BelongPowerStation='{0}'
)

  GROUP BY CheckPointCode
) joinoio ON joinoio.CheckPointCode=oio.CheckPointCode AND joinoio.CollectionDatetime=oio.CollectionDatetime
left join OCInstrumentInfo oic on oic.CheckPointCode=oio.CheckPointCode
 order by oio.CheckPointCode ", p_convartStationCode);

            
            dt = EGMNGS.Common.ComServies.Query(sqltemp).Copy();

            return dt;
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}
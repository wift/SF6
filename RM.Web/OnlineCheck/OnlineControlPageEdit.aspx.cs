﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EGMNGS.Model;
using System.Data;
using RM.Common.DotNetUI;
using RM.Common.DotNetCode;
using RM.Common.DotNetBean;

namespace RM.Web.OnlineCheck
{

    public partial class OnlineControlPageEdit : System.Web.UI.Page
    {
        private readonly EGMNGS.BLL.OCInstrumentInfo bll = new EGMNGS.BLL.OCInstrumentInfo();
        private readonly EGMNGS.BLL.OCInstrumentInfo_Log bllLog = new EGMNGS.BLL.OCInstrumentInfo_Log();
        private readonly EGMNGS.BLL.InfoDevPara bllInfo = new EGMNGS.BLL.InfoDevPara();
        private readonly LogHelper logHelper = new LogHelper();
        public Dictionary<string, string> columnDesc
        {
            set { ViewState["columnDesc"] = value; }
            get { return ViewState["columnDesc"] as Dictionary<string, string>; }
        }
        public OCInstrumentInfo OCInstrumentInfoObj
        {
            get
            {
                if (ViewState["OCInstrumentInfoObj"] == null)
                {
                    ViewState["OCInstrumentInfoObj"] = new OCInstrumentInfo();
                }

                return ViewState["OCInstrumentInfoObj"] as OCInstrumentInfo;
            }
            set { ViewState["OCInstrumentInfoObj"] = value; }
        }



        protected void Page_Load(object sender, EventArgs e)
        {
            //  this.PageControl1.pageHandler += new EventHandler(pager_PageChanged);

            if (!IsPostBack)
            {
                string oid = Request["oid"] ?? "";
                string code = Request["code"] ?? "";

                if (!string.IsNullOrWhiteSpace(oid))
                {
                    OCInstrumentInfoObj = bll.GetModel(oid.AsTargetType<int>(0));
                }

                if (!string.IsNullOrWhiteSpace(code))
                {

                    var list = bllInfo.GetModelList(string.Format("DevCode='{0}'", code));
                    if (list.Count > 0)
                    {
                        OCInstrumentInfoObj.BelongEquipmentName = list[0].DevName;
                        OCInstrumentInfoObj.BelongEquipment = code;
                        OCInstrumentInfoObj.BelongPowerStationName = list[0].ConvertStationName;
                        OCInstrumentInfoObj.BelongPowerStation = list[0].ConvertStationCode;
                    }

                }
                if (!string.IsNullOrWhiteSpace(OCInstrumentInfoObj.BelongEquipment))
                {
                    var listInfoDevPara = bllInfo.GetModelList(string.Format("DevCode='{0}'", OCInstrumentInfoObj.BelongEquipment));
                    if (listInfoDevPara.Count > 0)
                    {
                        OCInstrumentInfoObj.BelongEquipmentName = listInfoDevPara[0].DevName;
                        OCInstrumentInfoObj.BelongPowerStationName = listInfoDevPara[0].ConvertStationName;
                    }
                }
                DropDownListBinder();
            }
        }
        protected void pager_PageChanged(object sender, EventArgs e)
        {
            //  DataBindGrid();
        }

        private void DataBindGrid()
        {
            //  string sql = @"1=1";
            //  sql += string.Format("and OCInstrumentInfo_OID={0}", OCInstrumentInfoObj.OID);
            //  int count = bllLog.GetRecordCount(sql);
            //  DataSet ds = bllLog.GetListByPage(sql, "UpdateDate", PageControl1.PageIndex, PageControl1.PageSize);
            // ControlBindHelper.BindRepeaterList(ds.Tables[0], rp_Item);
            //  this.PageControl1.RecordCount = Convert.ToInt32(count);
        }
        private void DropDownListBinder()
        {
            EGMNGS.BLL.SYS_CODE sysCodeBll = new EGMNGS.BLL.SYS_CODE();
            DataSet dsDict = sysCodeBll.GetList("CODE_TYPE='RunStatus' ORDER BY DISPLAY_ORDER");

            this.ddlControlRunStatus.DataSource = dsDict.Tables[0];
            this.ddlControlRunStatus.DataTextField = "CODE_CHI_DESC";
            this.ddlControlRunStatus.DataValueField = "CODE";
            this.ddlControlRunStatus.DataBind();
            ddlControlRunStatus.SelectedIndex = 1;

            this.ddlRunStatus.DataSource = dsDict.Tables[0];
            this.ddlRunStatus.DataTextField = "CODE_CHI_DESC";
            this.ddlRunStatus.DataValueField = "CODE";
            this.ddlRunStatus.DataBind();
            ddlRunStatus.SelectedIndex = 1;

            dsDict = sysCodeBll.GetList("CODE_TYPE='DYLev' ORDER BY DISPLAY_ORDER");

            this.dllVoltageLevel.DataSource = dsDict.Tables[0];
            this.dllVoltageLevel.DataTextField = "CODE_CHI_DESC";
            this.dllVoltageLevel.DataValueField = "CODE";
            this.dllVoltageLevel.DataBind();


            columnDesc = new Dictionary<string, string>();
            //columnDesc.Add("ControlRunStatus", "运行状态");
            //columnDesc.Add("ControlRunFromDate", "运行开始时间");
            //columnDesc.Add("ControlRunToDate", "运行结束时间");
            //columnDesc.Add("Cycle", "周期");
            //columnDesc.Add("ControlPressureMin", "压力最小值");
            //columnDesc.Add("ControlPressureMax", "压力最大值");
            //columnDesc.Add("ControlTempertureMin", "温度最小值");
            //columnDesc.Add("ControlTempertureMax", "温度最大值");
            //columnDesc.Add("ControlMicroWater", "微水");
            columnDesc.Add("ControlSO2", "SO2");
            columnDesc.Add("ControlCO", "CO");
            columnDesc.Add("ControlCF4", "CF4");
            columnDesc.Add("ControlSO2F2", "SO2F2");
            columnDesc.Add("ControlSOF2", "SOF2");
            columnDesc.Add("ControlCS2", "CS2");
            columnDesc.Add("ControlHF", "HF");
        }
        protected void rp_Item_ItemCommand(object source, RepeaterCommandEventArgs e)
        {

        }

        protected void rp_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

        }

        /// <summary>
        /// 保存
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (OCInstrumentInfoObj.OID > 0)
                {
                    OCInstrumentInfoObj.LAST_UPD_BY = RequestSession.GetSessionUser().UserName.ToString();
                    OCInstrumentInfoObj.LAST_UPD_DATE = DateTime.Now;
                    bool seccuss = bll.Update(OCInstrumentInfoObj);
                    if (seccuss)
                    {
                        ShowMsgHelper.AlertMsg("修改成功!");
                    }
                }
                else
                {
                    OCInstrumentInfoObj.CREATED_BY = RequestSession.GetSessionUser().UserName.ToString();
                    OCInstrumentInfoObj.CREATED_DATE = DateTime.Now;
                    int count = bll.Add(OCInstrumentInfoObj);
                    if (count > 0)
                    {
                        ShowMsgHelper.AlertMsg("添加成功!");
                    }
                }


            }
            catch (Exception ex)
            {
                logHelper.WriteLog(ex.Message);
                ShowMsgHelper.Alert("添加失败!");
            }

        }
    }
}
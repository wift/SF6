﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="OnlineAnalysisPage.aspx.cs"
    Inherits="RM.Web.OnlineCheck.OnlineAnalysisPage" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>数据分析诊断</title>
    <%-- <link href="~/Themes/Styles/Site.css" rel="stylesheet" type="text/css" />--%>
    <script src="../Themes/Scripts/jquery-1.8.2.min.js" type="text/javascript"></script>
    <script src="../Themes/Scripts/jquery.pullbox.js" type="text/javascript"></script>
    <script src="../Themes/Scripts/FunctionJS.js" type="text/javascript"></script>
    <script src="../Themes/Scripts/DatePicker/WdatePicker.js" type="text/javascript"></script>
    <link href="../Themes/Scripts/jqueryeasyui/themes/bootstrap/easyui.css" rel="stylesheet" />
    <link href="../Themes/Scripts/jqueryeasyui/themes/icon.css" rel="stylesheet" />
    <style type="text/css">
        html, body {
            margin: 0;
            height: 100%;
            font-size: 9pt;
        }

        #containt {
            height: 100%;
        }

        form {
            height: 100%;
        }

        #UpdatePanel {
            height: 100%;
        }

        #containt > #UpdatePanel > .Panel {
            float: left;
            height: -moz-calc(100% - 0px);
            height: -webkit-calc(100% - 00px);
            height: calc(100% - 0px);
        }

        .itemPanel {
            width: 170px;
            height: 170px;
            float: left;
            margin: 10px 10px 10px 10px;
            border-width: 1px;
            border-style: solid;
            cursor: pointer;
        }

            .itemPanel > div {
                float: left;
                height: -moz-calc(100% - 0px);
                height: -webkit-calc(100% - 00px);
                height: calc(100% - 0px);
            }

                .itemPanel > div > p {
                    margin-top: 0px;
                    margin-bottom: 0px;
                    font-size: 12px;
                }

        .leftItem {
            border-right-width: 1px;
            border-right-style: solid;
        }

        .datachart {
            height: -moz-calc(100% - 300px);
            height: -webkit-calc(100% - 300px);
            height: calc(100% - 300px);
        }
        /**表格 begin**/
        .grid {
            margin: 0px;
            border-collapse: collapse;
            width: 100%;
            table-layout: fixed;
        }

            .grid thead td {
                border-top: 1px solid #ccc;
                border-bottom: 1px solid #ccc;
                border-right: 1px dotted #ccc;
                background: url(../Themes/Images/datagrid_header_bg.gif) repeat-x;
                text-align: center;
                padding: 5px 1px;
                font-weight: normal;
                text-overflow: ellipsis;
                word-break: keep-all;
                overflow: hidden;
            }

            .grid tbody td {
                text-align: left;
                border-bottom: 1px dotted #ccc;
                border-right: 1px dotted #ccc;
                padding: 1px 1px;
                height: 20px;
                word-break: break-all;
            }

            .grid tbody .alt {
                background: #F7F7F7;
            }

            .grid tbody .selected {
                background: #e0eccc;
            }
               .panel-body
        {
            overflow:hidden !important;
        }
        #tt
        {
            width: -moz-calc(100% - 0px);
            width: -webkit-calc(100% - 0px);
            width: calc(100% - 0px);
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div id="containt">
            <asp:ScriptManager ID="ScriptManager1" runat="server">
            </asp:ScriptManager>
            <asp:UpdatePanel ID="UpdatePanel" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="leftPanel" class="Panel" style="width: 20%; border-right-width: 1px; border-right-style: solid;">
                        <div id="searchDiv" style="height: 165px;">
                            测点查询
                        <table style="width: 100%">
                            <tr>
                                <td>供电局：
                                </td>
                                <td>
                                    <cc1:ExDropDownList ID="ddlPowerSupplyName" runat="server" CssClass="select" FieldName="PowerSupplyCode"
                                        AutoPostBack="true" OnSelectedIndexChanged="ddlPowerSupplyName_SelectedIndexChanged"
                                        BindName="InfoDevParaObj" Width="131" />
                                </td>
                            </tr>
                            <tr>
                                <td>所属变电站：
                                </td>
                                <td>
                                    <cc1:ExDropDownList ID="ddlConvertStationName" runat="server" CssClass="select" FieldName="ConvertStationCode"
                                        BindName="InfoDevParaObj" Width="131" />
                                </td>
                            </tr>
                            <tr>
                                <td>设备类型
                                </td>
                                <td>
                                    <cc1:ExDropDownList ID="ddlDevClass" runat="server" CssClass="select" FieldName="DevClass"
                                        BindName="InfoDevParaObj" Width="131" />
                                </td>
                            </tr>
                            <tr>
                                <td>电压等级
                                </td>
                                <td>
                                    <cc1:ExDropDownList ID="ddlVoltageLevel" runat="server" CssClass="select" FieldName="VoltageLevel"
                                        BindName="InfoDevParaObj" Width="131" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" style="text-align: center;">
                                    <asp:Button ID="btnSearch" runat="server" OnClick="btnSearch_Click" Text="查询" />
                                </td>
                            </tr>
                        </table>
                        </div>
                        <div class="div-body">
                            <table id="table1" class="grid" singleselect="false">
                                <colgroup>
                                    <col width="8%" />
                                    <col width="20%" />
                                    <col width="42%" />
                                    <col width="30%" />
                                </colgroup>
                                <thead>
                                    <tr>
                                        <td style="text-align: center;"></td>
                                        <td style="text-align: center;">测点编码
                                        </td>
                                        <td style="text-align: center;">测点名称
                                        </td>
                                        <td style="text-align: center;">所属设备
                                        </td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <asp:Repeater ID="rp_Item" runat="server">
                                        <ItemTemplate>
                                            <tr>
                                                <td style="text-align: center;">
                                                    <input type="checkbox" id="<%# Eval("CheckPointCode")%>" title="<%# Eval("CheckPointName")%>" />
                                                </td>
                                                <td style="text-align: center;">
                                                    <%# Eval("CheckPointCode")%>
                                                </td>
                                                <td style="text-align: center;">
                                                    <%# Eval("CheckPointName")%>
                                                </td>
                                                <td style="text-align: center;">
                                                    <%# Eval("BelongEquipmentName")%>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div id="rightPanel" class="Panel" style="width: 79.5%;">
                        <div>
                            <span style="float: left;">采集时间起:</span>
                            <div style="float: left;">
                                <input id="formdate" type="text" name="formdate" class="easyui-datebox" required="required"></input>
                            </div>
                            <span style="float: left;">采集时间止: </span>
                            <div style="float: left;">
                                <input id="todate" type="text" name="todate" class="easyui-datebox" required="required"></input>
                            </div>
                            <div style="float: left;">
                                <input id="btnRefreshList" type="button" value="查询" />
                            </div>
                        </div>
                        <div id="tt" class="easyui-tabs" style="height: 620px;">
                            <div title="组分历史数据" style="padding: 10px; width: 100%;">
                                <table id="Student_Table">
                                </table>
                                <div>
                                    <div style="float: left; padding-top: 2px" id="targetCheckbox">

                                        <span style="font-weight: bold">
                                            <input id="Pressure'" type="checkbox" value="Pressure" />压力</span> <span style="font-weight: bold">
                                                <input id="Temperture" type="checkbox" value="Temperture" />温度</span> <span style="font-weight: bold">
                                                    <input id="MicroWater" type="checkbox" value="MicroWater" />微水</span>
                                        <span style="font-weight: bold">
                                            <input id="SO2" type="checkbox" value="SO2" />SO2</span> <span style="font-weight: bold">
                                                <input id="CO" type="checkbox" value="CO" />CO</span> <span style="font-weight: bold">
                                                    <input id="CF4" type="checkbox" value="CF4" />CF4</span> <span style="font-weight: bold">
                                                        <input id="SO2F2" type="checkbox" value="SO2F2" />SO2F2</span> <span style="font-weight: bold">
                                                            <input id="SOF2" type="checkbox" value="SOF2" />SOF2</span>
                                        <span style="font-weight: bold">
                                            <input id="CS2" type="checkbox" value="CS2" />CS2</span> <span style="font-weight: bold">
                                                <input id="HF" type="checkbox" value="HF" />HF</span>
                                        <span style="font-weight: bold">
                                            <input id="H2S" type="checkbox" value="H2S" />H2S</span>
                                        <span style="font-weight: bold">
                                            <input id="COSS" type="checkbox" value="COSS" />COS</span>

                                        <span style="font-weight: bold">
                                            <input id="C2F6" type="checkbox" value="C2F6" />C2F6</span>

                                        <span style="font-weight: bold">
                                            <input id="C3F8" type="checkbox" value="C3F8" />C3F8</span>

                             
                                    </div>
                                    <div style="float: left; margin-left: 10px;">
                                        <input id="btnRefresh" type="button" value="立即刷新" />
                                    </div>
                                </div>
                                <div class="datachart">
                                </div>
                            </div>
                            <div title="组分比例数据" style="padding: 10px; width: 100%;">
                                <table id="table_Proportion">
                                </table>
                                <br />
                                <div id="chartProportion">
                                </div>
                            </div>
                            <div title="电气常量数据" style="padding: 10px; width: 100%;">
                                <table id="listEConstData">
                                </table>
                                <br />

                                <div>
                                    <div style="float: left; padding-top: 2px" id="ConstCheckList">

                                        <span style="font-weight: bold">
                                            <input id="Checkbox1" type="checkbox" value="Pressure" />压力</span>
                                        <span style="font-weight: bold">
                                            <input id="Checkbox2" type="checkbox" value="Temperture" />温度</span>
                                        <span style="font-weight: bold">
                                            <input id="Checkbox4" type="checkbox" value="OutElectricity" />放电量</span>
                                        <span style="font-weight: bold">
                                            <input id="Checkbox5" type="checkbox" value="CurrentVal" />电流</span>
                                        <span style="font-weight: bold">
                                            <input id="Checkbox6" type="checkbox" value="Voltage" />电压</span>
                                        <span style="font-weight: bold">
                                            <input id="Checkbox7" type="checkbox" value="PowerVal" />功率</span>

                                    </div>
                                    <div style="float: left; margin-left: 10px;">
                                        <input id="btnConstRefresh" type="button" value="立即刷新" />
                                    </div>
                                </div>
                                <div id="listEConstChart">
                                </div>
                            </div>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="ddlPowerSupplyName" EventName="SelectedIndexChanged" />
                    <asp:AsyncPostBackTrigger ControlID="btnSearch" EventName="Click" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </form>
    <script type="text/javascript" src="../Themes/Scripts/highcharts.js"></script>
    <script type="text/javascript" src="../Themes/Scripts/jqueryeasyui/jquery.easyui.min.js"></script>
    <script src="../Themes/Scripts/jqueryeasyui/plugins/jquery.edatagrid.js" type="text/javascript"></script>
    <script type="text/javascript">

        var $arrid = [];
        var $arridnochar = [];
        var $checkPointSourse = [];
        var $fd;
        var $td;
        var $width = 1100;
        $(document).ready(function () {
            $width = $("#rightPanel").width()-5;
        
            //  GetTable('');
            var $checkBoxList = $(".div-body").find(":checkbox");
            $checkBoxList.click(function myfunction(o) {
                $arrid = [];
                $checkPointSourse = [];
                $arridnochar = [];
                $checkBoxList.filter(":checked").each(function myfunction() {
                    $arrid.push("\'" + $(this).attr("id") + "\'");
                    $arridnochar.push($(this).attr("id"));
                    $checkPointSourse.push({ id: $(this).attr("id"), text: $(this).attr("title") });
                });

            });

            //历史列表刷新
            $("#btnRefreshList").click(function myfunction() {
                if ($("#form1").form('validate')) {
                    var tab = $('#tt').tabs('getSelected');
                    var index = $('#tt').tabs('getTabIndex', tab);
                    $fd = $("#formdate").datebox('getValue');
                    $td = $("#todate").datebox('getValue');

                    if (index == 0) {
                        GetTable();
                    }
                    else if (index == 1) {
                        GetProportionList();
                        GetProportionChart();
                    } else if (index == 2) {
                        GetConstDataList();
                        //GetConstDataChart();
                    }
                }
            });

            //历史图表刷新
            $("#btnRefresh").click(function myfunction() {
                if ($("#form1").form('validate')) {
                    $fd = $("#formdate").datebox('getValue');
                    $td = $("#todate").datebox('getValue');
                    // var $cp = $("#checkPoint").combobox('getValues');
                    var $st = [];
                    $("#targetCheckbox > span > :checkbox").filter(":checked").each(function myfunction() {
                        $st.push($(this).val());
                    });

                    $.post("CRUDHandler.ashx", { action: 'QueryChart', fromdate: $fd, todate: $td, checkPoint: $arridnochar.join(","), Target: $st.toString() }, function myfunction(results) {
                        InitChart(results.listDatetime, results.listChartData);
                    });
                }
            });


            //电气常量数据图表刷新
            $("#btnConstRefresh").click(function myfunction() {
                if ($("#form1").form('validate')) {
                    $fd = $("#formdate").datebox('getValue');
                    $td = $("#todate").datebox('getValue');
                    // var $cp = $("#checkPoint").combobox('getValues');
                    var $st = [];
                    $("#ConstCheckList > span > :checkbox").filter(":checked").each(function myfunction() {
                        $st.push($(this).val());
                    });

                    $.post("CRUDHandler.ashx", { action: 'QueryConstChart', fromdate: $fd, todate: $td, checkPoint: $arridnochar.join(","), Target: $st.toString() }, function myfunction(results) {
                        GetConstDataChart(results.listDatetime, results.listChartData);
                    });
                }
            });

            $('#tt').tabs({
                border: false,
                onSelect: function (title) {
                    if (title == "组分比例数据") {
                        // GetProportionList();
                        //   GetProportionChart();
                    }
                    else if (title == "电气常量数据") {
                        //    GetConstDataList();
                        //   GetConstDataChart();
                    }
                }
            });

        });

        //电气常量数据列表
        function GetConstDataList() {

            var editRow = undefined;
            $("#listEConstData").datagrid({
                height: 250,
                width: $width,
                fitColumns: true,
                collapsible: true,
                singleSelect: true,
                url: 'GetOCInstr_ChildsHandler.ashx',
                queryParams: { action: 'GetConstDataList', checkPointCode: $arrid.join(",") },
                idField: 'OID',
                columns: [[
                 { field: 'OID', title: 'ID', hidden: true },
                  { field: 'CheckPointCode', hidden: true, title: "测点编码" },
		 { field: 'CheckPointName', align: 'center', width: fixWidth(0.15), title: '测点名称' },
        	{
        	    field: 'CollectionDatetime', align: 'center', width: fixWidth(0.16), title: '采集时间', formatter: formatDatebox, editor: { type: 'datetimebox', options: { required: true } }
        	},
        	{ field: 'Pressure', title: '压力(Mpa)', width: fixWidth(0.06), align: 'center', editor: { type: 'text', options: { required: true } } },
            { field: 'Temperture', title: '温度(℃)', width: fixWidth(0.06), align: 'center', editor: { type: 'text', options: { required: true } } },
            { field: 'OutElectricity', title: '放电量(pC)', width: fixWidth(0.06), align: 'center', editor: { type: 'text', options: { required: true } } },
            { field: 'CurrentVal', title: '电流(A)', width: fixWidth(0.06), align: 'center', editor: { type: 'text', options: { required: true } } },
            { field: 'Voltage', title: '电压(V)', width: fixWidth(0.06), align: 'center', editor: { type: 'text', options: { required: true } } },
            { field: 'PowerVal', title: '功率(W)', width: fixWidth(0.06), align: 'center', editor: { type: 'text', options: { required: true } } },
              {
                  field: 'CollectionType', width: fixWidth(0.06), title: '采集类型', align: 'center'
              },
               { field: 'CollectionBy', width: fixWidth(0.06), title: '采集人', align: 'center' }
                ]],
                toolbar: [{
                    text: '刷新', iconCls: 'icon-reload', handler: function () {

                        $("#listEConstData").datagrid("reload");
                    }

                }]


            });

        }


        //电气常量数据图表
        function GetConstDataChart() {
            $.get('GetOCInstr_ChildsHandler.ashx', { action: "GetConstDataChart", rangeSelect: $("#rangeSelect").val(), CheckPointCode: $arrid.join(",") }, function myfunction(results) {
                var listDatetimeJson = results.listDatetime;
                var listChartDataJson = results.listChartData;

                $('#listEConstChart').highcharts({
                    title: {
                        text: null
                    },
                    xAxis: {
                        categories: listDatetimeJson
                    },
                    yAxis: {
                        title: {
                            text: ''
                        },
                        plotLines: [{
                            value: 0,
                            width: 1,
                            color: '#808080'
                        }]
                    },
                    tooltip: {
                        // valueSuffix: '°C'
                    },
                    legend: {
                        layout: 'vertical',
                        align: 'right',
                        verticalAlign: 'middle',
                        borderWidth: 0,
                        itemStyle: { color: '#7cb5ec' }


                    },
                    series: listChartDataJson

                });

            });
        }


        //获取比例列表
        function GetProportionList() {

            $.get('GetOCInstr_ChildsHandler.ashx', { action: 'GetColumns', CheckPointCode: $arrid.join(",") }, function myfunction(results) {

                var columnsarr = [{ title: '测点名称', field: 'CheckPointName', align: 'center', width: 100 }, { title: '采集时间', field: 'CollectionDatetime', align: 'center', width: 150, formatter: formatDatebox }];


                for (var i in results) {
                    columnsarr.push({ title: results[i].title, field: results[i].field, align: 'center', width: 100 });
                }

                var columnsroot = [];
                columnsroot.push(columnsarr);

                var editRow = undefined;

                $fd = $("#formdate").datebox('getValue');
                $td = $("#todate").datebox('getValue');
                $("#table_Proportion").datagrid({
                    height: 250,
                    width: $width,
                    fitColumns: false,
                    collapsible: true,
                    singleSelect: true,
                    url: 'GetOCInstr_ChildsHandler.ashx',
                    queryParams: {
                        action: 'GetOCInstrumentInfo_Childs', CheckPointCode: $arrid.join(","),
                        columns: JSON.stringify(columnsarr), fromdate: $fd, todate: $td
                    },
                    idField: 'CheckPointName',
                    columns: columnsroot,
                    toolbar: [{
                        text: '刷新', iconCls: 'icon-reload', handler: function () {

                            $("#table_Proportion").datagrid("reload");
                        }
                    }]

                });



                $.get('GetOCInstr_ChildsHandler.ashx', { action: "GetProportionChartMuliPointCode", fromdate: $fd, todate: $td, checkPoint: $arrid.join(",") }, function myfunction(results) {
                    var listDatetimeJson = results.listDatetime;
                    var listChartDataJson = results.listChartData;

                    $('#chartProportion').highcharts({
                        chart: {
                            height: 230
                        },
                        title: {
                            text: null
                        },
                        xAxis: {
                            categories: listDatetimeJson
                        },
                        yAxis: {
                            title: {
                                text: ''
                            },
                            plotLines: [{
                                value: 0,
                                width: 1,
                                color: '#808080'
                            }]
                        },
                        tooltip: {
                            // valueSuffix: '°C'
                        },
                        legend: {
                            layout: 'vertical',
                            align: 'right',
                            verticalAlign: 'middle',
                            borderWidth: 0,
                            itemStyle: { color: '#7cb5ec' }

                        },
                        series: listChartDataJson
                    });

                });

            });
        }







        //图表
        function InitChart(listDatetimeJson, listChartDataJson) {

            $('.datachart').highcharts({
                chart: {
                    height: 230
                },
                title: {
                    style: { "fontSize": "9px" },
                    text: null
                },
                xAxis: {
                    categories: listDatetimeJson//['2016-7-1', '2016-7-2', '2016-7-3', '2016-7-4', '2016-7-5', '2016-7-6', '2016-7-7', '2016-7-8', '2016-7-9', '2016-7-10', '2016-7-11', '2016-7-12']
                },
                yAxis: {

                    plotLines: [{
                        value: 0,
                        width: 1,
                        color: '#808080'
                    }]
                },
                tooltip: {
                    // valueSuffix: '°C'
                },
                legend: {
                    layout: 'vertical',
                    align: 'right',
                    verticalAlign: 'middle',
                    borderWidth: 0,
                    itemStyle: { color: '#7cb5ec' }

                },
                series: listChartDataJson
            });
        }

        //电气常量数据图表
        function GetConstDataChart(listDatetimeJson, listChartDataJson) {

            $('#listEConstChart').highcharts({
                chart: {
                    height: 230
                },
                title: {
                    style: { "fontSize": "9px" },
                    text: null
                },
                xAxis: {
                    categories: listDatetimeJson//['2016-7-1', '2016-7-2', '2016-7-3', '2016-7-4', '2016-7-5', '2016-7-6', '2016-7-7', '2016-7-8', '2016-7-9', '2016-7-10', '2016-7-11', '2016-7-12']
                },
                yAxis: {

                    plotLines: [{
                        value: 0,
                        width: 1,
                        color: '#808080'
                    }]
                },
                tooltip: {
                    // valueSuffix: '°C'
                },
                legend: {
                    layout: 'vertical',
                    align: 'right',
                    verticalAlign: 'middle',
                    borderWidth: 0,
                    itemStyle: { color: '#7cb5ec' }

                },
                series: listChartDataJson
            });
        }

        function formatDatebox(value) {
            if (value == null || value == '') {
                return '';
            }
            var dt;
            if (value instanceof Date) {
                dt = value;
            } else {
               // dt = new Date(value.replace('T', ' '));
                return value.replace('T', ' ');
            }
            return dt.format("yyyy-MM-dd hh:mm:ss");
        }
        function fixWidth(percent) {
            return (document.body.clientWidth - 5) * percent;
        }
        function GetTable(arridstring) {
            var editRow = undefined;
            var $width = $("#rightPanel").width();

            arridstring = $arrid.join(",")

            $("#Student_Table").datagrid({
                height: 250,
                width: $width,
                fitColumns: true,
                collapsible: true,
                singleSelect: true,
                url: 'CRUDHandler.ashx',
                queryParams: { action: 'Query', checkPointCode: arridstring, fromdate: $fd, todate: $td },
                idField: 'OID',
                columns: [[
                         { field: 'OID', title: 'ID', hidden: true },
                          { field: 'CheckPointCode', hidden: true, title: "测点编码" },
                 { field: 'CheckPointName', align: 'center', width: fixWidth(0.15), title: '测点名称' },
                    {
                        field: 'CollectionDatetime', align: 'center', width: fixWidth(0.16), title: '采集时间', formatter: formatDatebox, editor: { type: 'datetimebox', options: { required: true } }
                    },
                    { field: 'Pressure', title: '压力(Mpa)', width: fixWidth(0.06), align: 'center', editor: { type: 'text', options: { required: true } } },
                    { field: 'Temperture', title: '温度(℃)', width: fixWidth(0.06), align: 'center', editor: { type: 'text', options: { required: true } } },
                    { field: 'MicroWater', title: '微水', width: fixWidth(0.06), align: 'center', editor: { type: 'text', options: { required: true } } },
                    { field: 'SO2', title: 'SO2', width: fixWidth(0.06), align: 'center', editor: { type: 'text', options: { required: true } } },
                    { field: 'CO', title: 'CO', width: fixWidth(0.06), align: 'center', editor: { type: 'text', options: { required: true } } },
                    { field: 'CF4', title: 'CF4', width: fixWidth(0.06), align: 'center', editor: { type: 'text', options: { required: true } } },
                    { field: 'SO2F2', title: 'SO2F2', width: fixWidth(0.06), align: 'center', editor: { type: 'text', options: { required: true } } },
                    { field: 'SOF2', title: 'SOF2', width: fixWidth(0.06), align: 'center', editor: { type: 'text', options: { required: true } } },
                    { field: 'CS2', title: 'CS2', width: fixWidth(0.06), align: 'center', editor: { type: 'text', options: { required: true } } },
                     { field: 'HF', title: 'HF', width: fixWidth(0.06), align: 'center', editor: { type: 'text', options: { required: true } } },
                                   { field: 'H2S', title: 'H2S', width: fixWidth(0.06), align: 'center', editor: { type: 'text', options: { required: true } } },
 { field: 'COSS', title: 'COS', width: fixWidth(0.06), align: 'center', editor: { type: 'text', options: { required: true } } },
 { field: 'C2F6', title: 'C2F6', width: fixWidth(0.06), align: 'center', editor: { type: 'text', options: { required: true } } },
  { field: 'C3F8', title: 'C3F8', width: fixWidth(0.06), align: 'center', editor: { type: 'text', options: { required: true } } },

                      {
                          field: 'CollectionType', width: fixWidth(0.06), title: '采集类型', align: 'center'
                      },
                       { field: 'CollectionBy', width: fixWidth(0.06), title: '采集人', align: 'center' }
                ]]


            });
        }

    </script>
</body>
</html>

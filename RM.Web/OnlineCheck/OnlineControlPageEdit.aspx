﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="OnlineControlPageEdit.aspx.cs"
    Inherits="RM.Web.OnlineCheck.OnlineControlPageEdit" %>

<%@ Register Src="~/UserControl/PageControl.ascx" TagName="PageControl" TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>在线监测控制编辑</title>

    <link href="../Themes/Styles/Site.css" rel="stylesheet" type="text/css" />
    <script src="../Themes/Scripts/jquery-1.8.2.min.js" type="text/javascript"></script>
    <script src="../Themes/Scripts/Validator/JValidator.js" type="text/javascript"></script>

    <script src="../Themes/Scripts/jquery.pullbox.js" type="text/javascript"></script>
    <script src="../Themes/Scripts/FunctionJS.js" type="text/javascript"></script>
    <script src="../Themes/Scripts/DatePicker/WdatePicker.js" type="text/javascript"></script>
    <link href="../Themes/Scripts/jqueryeasyui/themes/bootstrap/easyui.css" rel="stylesheet" />
    <link href="../Themes/Scripts/jqueryeasyui/themes/icon.css" rel="stylesheet" />
    <link href="../Themes/Scripts/jqueryeasyui/themes/color.css" rel="stylesheet" />
    <style>
        body
        {
            font-size: small;
            overflow-x:hidden;
        }
        .inner_cell_right
        {
            text-align: right;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional">
        <ContentTemplate>
            <div>
                测点仪器信息维护
                <table width="100%" id="baseform">
                    <colgroup>
                        <col width="7%" />
                        <col width="7%" />
                        <col width="7%" />
                        <col width="7%" />
                        <col width="7%" />
                        <col width="7%" />
                        <col width="8%" />
                        <col width="7%" />
                    </colgroup>
                    <tr>
                        <td class="inner_cell_right">
                            测点编码：
                        </td>
                        <td>
                            <cc1:ExTextBox ID="tbAppCode" runat="server" checkexpession="LenStr" datacol="yes"
                                err="此项" FieldName="CheckPointCode" BindName="OCInstrumentInfoObj" length="4" />
                        </td>
                        <td class="inner_cell_right">
                            测点名称：
                        </td>
                        <td>
                            <cc1:ExTextBox ID="txtCheckPointName" runat="server" checkexpession="LenStr" datacol="yes"
                                length="50" err="此项" FieldName="CheckPointName" BindName="OCInstrumentInfoObj" />
                        </td>
                        <td class="inner_cell_right">
                            仪器编码：
                        </td>
                        <td>
                            <cc1:ExTextBox ID="txtInstrumentCode" runat="server" checkexpession="LenStr" datacol="yes"
                                length="4" err="此项" FieldName="InstrumentCode" BindName="OCInstrumentInfoObj" />
                        </td>
                        <td class="inner_cell_right">
                            仪器名称：
                        </td>
                        <td>
                            <cc1:ExTextBox ID="txtInstrumentName" runat="server" checkexpession="LenStr" datacol="yes"
                                length="50" err="此项" FieldName="InstrumentName" BindName="OCInstrumentInfoObj" />
                        </td>
                    </tr>
                    <tr>
                        <td class="inner_cell_right">
                            运行状态：
                        </td>
                        <td>
                            <cc1:ExDropDownList ID="ddlRunStatus" runat="server" CssClass="select" FieldName="RunStatus"
                                BindName="OCInstrumentInfoObj" Width="131" checkexpession="NotNull" datacol="yes"
                                err="此项" />
                        </td>
                        <td class="inner_cell_right">
                            安装位置：
                        </td>
                        <td>
                            <cc1:ExTextBox ID="txtIntallPostion" runat="server" checkexpession="LenStr" datacol="yes"
                                length="50" err="此项" FieldName="IntallPostion" BindName="OCInstrumentInfoObj" />
                        </td>
                        <td class="inner_cell_right">
                            所属设备：
                        </td>
                        <td>
                            <%= OCInstrumentInfoObj.BelongEquipmentName%>
                        </td>
                        <td class="inner_cell_right">
                            所属变电站：
                        </td>
                        <td>
                            <%= OCInstrumentInfoObj.BelongPowerStationName%>
                        </td>
                    </tr>
                    <tr>
                        <td class="inner_cell_right">
                            电压等级：
                        </td>
                        <td>
                            <cc1:ExDropDownList ID="dllVoltageLevel" runat="server" CssClass="select" Width="131"
                                FieldName="VoltageLevel" BindName="OCInstrumentInfoObj" checkexpession="NotNull"
                                datacol="yes" err="此项" />
                        </td>
                        <td class="inner_cell_right">
                            生产厂家：
                        </td>
                        <td>
                            <cc1:ExTextBox ID="txtManufacturer" runat="server" checkexpession="LenStr" datacol="yes"
                                length="50" err="此项" FieldName="Manufacturer" BindName="OCInstrumentInfoObj" />
                        </td>
                        <td class="inner_cell_right">
                            投运日期：
                        </td>
                        <td>
                            <cc1:ExTextBox ID="txtRunDate" runat="server" checkexpession="DateOr" datacol="yes"
                                err="此项" onfocus="WdatePicker()" StrFormat="yyyy-MM-dd" FieldName="RunDate" BindName="OCInstrumentInfoObj" />
                        </td>
                        <td class="inner_cell_right">
                            校准日期：
                        </td>
                        <td>
                            <cc1:ExTextBox ID="txtCalibrationDate" runat="server" checkexpession="Date" datacol="yes"
                                err="此项" onfocus="WdatePicker()" StrFormat="yyyy-MM-dd" FieldName="CalibrationDate"
                                BindName="OCInstrumentInfoObj" />
                        </td>
                    </tr>
                </table>
                <div style="text-align: center;">
                    <asp:Button ID="btnSave" runat="server" Text="保存" OnClick="btnSave_Click" OnClientClick="return CheckDataValid('#baseform'); " />
                </div>
                <hr />
                运行控制
                <table width="100%" style="font-size: smaller;" id="entendfrom">
                    <colgroup>
                        <col width="7%" />
                        <col width="7%" />
                        <col width="7%" />
                        <col width="7%" />
                        <col width="7%" />
                        <col width="7%" />
                        <col width="8%" />
                        <col width="7%" />
                    </colgroup>
                    <tr>
                        <td class="inner_cell_right">
                            运行状态：
                        </td>
                        <td>
                            <cc1:ExDropDownList ID="ddlControlRunStatus" runat="server" CssClass="select" Width="60"
                                FieldName="ControlRunStatus" BindName="OCInstrumentInfoObj" checkexpession="NotNull"
                                datacol="yes" err="此项" />
                        </td>
                        <td class="inner_cell_right">
                            运行开始时间：
                        </td>
                        <td>
                            <cc1:ExTextBox ID="txtControlRunFromDate" runat="server" checkexpession="DateTime"
                                datacol="yes" err="此项" onfocus="WdatePicker({dateFmt: 'yyyy-MM-dd HH:mm:ss'})"
                                StrFormat="yyyy-MM-dd HH:mm:ss" FieldName="ControlRunFromDate" BindName="OCInstrumentInfoObj" />
                        </td>
                        <td class="inner_cell_right">
                            运行结束时间：
                        </td>
                        <td>
                            <cc1:ExTextBox ID="txtControlRunToDate" runat="server" checkexpession="DateTime"
                                datacol="yes" err="此项" onfocus="WdatePicker({dateFmt: 'yyyy-MM-dd HH:mm:ss'})"
                                StrFormat="yyyy-MM-dd HH:mm:ss" FieldName="ControlRunToDate" BindName="OCInstrumentInfoObj" />
                        </td>
                        <td class="inner_cell_right">
                            周期（小时）：
                        </td>
                        <td>
                            <cc1:ExTextBox ID="txtCycle" runat="server" checkexpession="Num" datacol="yes" err="此项"
                                FieldName="Cycle" BindName="OCInstrumentInfoObj" Width="50" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="8" align="left">
                            <hr />
                            预警值设置
                        </td>
                    </tr>
                    <tr>
                        <td colspan="8">
                            <table width="100%" style="font-size: smaller;">
                                <colgroup>
                                    <col width="150px" />
                                    <col width="50px" />
                                    <col width="150px" />
                                    <col width="50px" />
                                    <col width="150px" />
                                    <col width="50px" />
                                    <col width="150px" />
                                    <col width="50px" />
                                </colgroup>
                                <tr>
                                    <td class="inner_cell_right">
                                        压力最小值（MPa）预报：
                                    </td>
                                    <td>
                                        <cc1:ExTextBox ID="txtControlPressureMin" runat="server" checkexpession="Double"
                                            Width="50" datacol="yes" err="此项" FieldName="ControlPressureMin" BindName="OCInstrumentInfoObj" />
                                    </td>
                                    <td class="inner_cell_right">
                                        压力最小值（MPa）高报：
                                    </td>
                                    <td>
                                        <cc1:ExTextBox ID="txtControlPressureMins" runat="server" checkexpession="Double"
                                            Width="50" datacol="yes" err="此项" FieldName="ControlPressureMins" BindName="OCInstrumentInfoObj" />
                                    </td>
                                    <td class="inner_cell_right">
                                        压力最大值(MPa)预报：
                                    </td>
                                    <td>
                                        <cc1:ExTextBox ID="txtControlPressureMax" runat="server" checkexpession="Double"
                                            Width="50" datacol="yes" err="此项" FieldName="ControlPressureMax" BindName="OCInstrumentInfoObj" />
                                    </td>
                                    <td class="inner_cell_right">
                                        压力最大值(MPa)高报：
                                    </td>
                                    <td>
                                        <cc1:ExTextBox ID="txtControlPressureMaxs" runat="server" checkexpession="Double"
                                            Width="50" datacol="yes" err="此项" FieldName="ControlPressureMaxs" BindName="OCInstrumentInfoObj" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="inner_cell_right">
                                        温度最小值(℃) 预报：
                                    </td>
                                    <td>
                                        <cc1:ExTextBox ID="txtControlTempertureMin" runat="server" checkexpession="Double"
                                            Width="50" datacol="yes" err="此项" FieldName="ControlTempertureMin" BindName="OCInstrumentInfoObj" />
                                    </td>
                                    <td class="inner_cell_right">
                                        温度最小值(℃) 高报：
                                    </td>
                                    <td>
                                        <cc1:ExTextBox ID="txtControlTempertureMins" runat="server" checkexpession="Double"
                                            Width="50" datacol="yes" err="此项" FieldName="ControlTempertureMins" BindName="OCInstrumentInfoObj" />
                                    </td>
                                    <td class="inner_cell_right">
                                        温度最大值(℃)预报：
                                    </td>
                                    <td>
                                        <cc1:ExTextBox ID="txtControlTempertureMax" runat="server" checkexpession="Double"
                                            Width="50" datacol="yes" err="此项" FieldName="ControlTempertureMax" BindName="OCInstrumentInfoObj" />
                                    </td>
                                    <td class="inner_cell_right">
                                        温度最大值(℃)高报：
                                    </td>
                                    <td>
                                        <cc1:ExTextBox ID="txtControlTempertureMaxs" runat="server" checkexpession="Double"
                                            Width="50" datacol="yes" err="此项" FieldName="ControlTempertureMaxs" BindName="OCInstrumentInfoObj" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="inner_cell_right">
                                        微水（ug/m³）预报：
                                    </td>
                                    <td>
                                        <cc1:ExTextBox ID="txtControlMicroWater" runat="server" checkexpession="Double" datacol="yes"
                                            err="此项" FieldName="ControlMicroWater" BindName="OCInstrumentInfoObj" Width="50" />
                                    </td>
                                    <td class="inner_cell_right">
                                        微水（ug/m³）高报：
                                    </td>
                                    <td>
                                        <cc1:ExTextBox ID="txtControlMicroWaters" runat="server" checkexpession="Double"
                                            Width="50" datacol="yes" err="此项" FieldName="ControlMicroWaters" BindName="OCInstrumentInfoObj" />
                                    </td>
                                    <td class="inner_cell_right">
                                        SO2（ug/m³）预报：
                                    </td>
                                    <td>
                                        <cc1:ExTextBox ID="txtControlSO2" runat="server" checkexpession="Double" datacol="yes"
                                            err="此项" FieldName="ControlSO2" BindName="OCInstrumentInfoObj" Width="50" />
                                    </td>
                                    <td class="inner_cell_right">
                                        SO2（ug/m³）高报：
                                    </td>
                                    <td>
                                        <cc1:ExTextBox ID="txtControlSO2s" runat="server" checkexpession="Double" datacol="yes"
                                            err="此项" FieldName="ControlSO2s" BindName="OCInstrumentInfoObj" Width="50" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="inner_cell_right">
                                        CO（ug/m³）预报：
                                    </td>
                                    <td>
                                        <cc1:ExTextBox ID="txtControlCO" runat="server" checkexpession="Double" datacol="yes"
                                            err="此项" FieldName="ControlCO" BindName="OCInstrumentInfoObj" Width="50" />
                                    </td>
                                    <td class="inner_cell_right">
                                        CO（ug/m³）高报：
                                    </td>
                                    <td>
                                        <cc1:ExTextBox ID="txtControlCOs" runat="server" checkexpession="Double" datacol="yes"
                                            err="此项" FieldName="ControlCOs" BindName="OCInstrumentInfoObj" Width="50" />
                                    </td>
                                    <td class="inner_cell_right">
                                        CF4（ug/m³）预报：
                                    </td>
                                    <td>
                                        <cc1:ExTextBox ID="txtControlCF4" runat="server" checkexpession="Double" datacol="yes"
                                            err="此项" FieldName="ControlCF4" BindName="OCInstrumentInfoObj" Width="50" />
                                    </td>
                                    <td class="inner_cell_right">
                                        CF4（ug/m³）高报：
                                    </td>
                                    <td>
                                        <cc1:ExTextBox ID="txtControlCF4s" runat="server" checkexpession="Double" datacol="yes"
                                            err="此项" FieldName="ControlCF4s" BindName="OCInstrumentInfoObj" Width="50" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="inner_cell_right">
                                        SO2F2（ug/m³）预报：
                                    </td>
                                    <td>
                                        <cc1:ExTextBox ID="txtControlSO2F2" runat="server" checkexpession="Double" datacol="yes"
                                            err="此项" FieldName="ControlSO2F2" BindName="OCInstrumentInfoObj" Width="50" />
                                    </td>
                                    <td class="inner_cell_right">
                                        SO2F2（ug/m³）高报：
                                    </td>
                                    <td>
                                        <cc1:ExTextBox ID="txtControlSO2F2s" runat="server" checkexpession="Double" datacol="yes"
                                            err="此项" FieldName="ControlSO2F2s" BindName="OCInstrumentInfoObj" Width="50" />
                                    </td>
                                    <td class="inner_cell_right">
                                        SOF2（ug/m³）预报：
                                    </td>
                                    <td>
                                        <cc1:ExTextBox ID="txtControlSOF2" runat="server" checkexpession="Double" datacol="yes"
                                            err="此项" FieldName="ControlSOF2" BindName="OCInstrumentInfoObj" Width="50" />
                                    </td>
                                    <td class="inner_cell_right">
                                        SOF2（ug/m³）高报：
                                    </td>
                                    <td>
                                        <cc1:ExTextBox ID="txtControlSOF2s" runat="server" checkexpession="Double" datacol="yes"
                                            err="此项" FieldName="ControlSOF2s" BindName="OCInstrumentInfoObj" Width="50" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="inner_cell_right">
                                        CS2（ug/m³）预报：
                                    </td>
                                    <td>
                                        <cc1:ExTextBox ID="txtControlCS2" runat="server" checkexpession="Double" datacol="yes"
                                            err="此项" FieldName="ControlCS2" BindName="OCInstrumentInfoObj" Width="50" />
                                    </td>
                                    <td class="inner_cell_right">
                                        CS2（ug/m³）高报：
                                    </td>
                                    <td>
                                        <cc1:ExTextBox ID="txtControlCS2s" runat="server" checkexpession="Double" datacol="yes"
                                            err="此项" FieldName="ControlCS2s" BindName="OCInstrumentInfoObj" Width="50" />
                                    </td>
                                    <td class="inner_cell_right">
                                        HF（ug/m³）预报：
                                    </td>
                                    <td>
                                        <cc1:ExTextBox ID="txtControlHF" runat="server" checkexpession="Double" datacol="yes"
                                            err="此项" FieldName="ControlHF" BindName="OCInstrumentInfoObj" Width="50" />
                                    </td>
                                    <td class="inner_cell_right">
                                        HF（ug/m³）高报：
                                    </td>
                                    <td>
                                        <cc1:ExTextBox ID="txtControlHFs" runat="server" checkexpession="Double" datacol="yes"
                                            err="此项" FieldName="ControlHFs" BindName="OCInstrumentInfoObj" Width="50" />
                                    </td>
                                </tr>

                                    <tr>
                                    <td class="inner_cell_right">
                                        H2S（ug/m³）预报：
                                    </td>
                                    <td>
                                        <cc1:ExTextBox ID="txtPH2S" runat="server" checkexpession="Double" datacol="yes"
                                            err="此项" FieldName="PH2S" BindName="OCInstrumentInfoObj" Width="50" />
                                    </td>
                                    <td class="inner_cell_right">
                                      H2S（ug/m³）高报：
                                    </td>
                                    <td>
                                        <cc1:ExTextBox ID="txtMH2S" runat="server" checkexpession="Double" datacol="yes"
                                            err="此项" FieldName="MH2S" BindName="OCInstrumentInfoObj" Width="50" />
                                    </td>
                                    <td class="inner_cell_right">
                                       COS（ug/m³）预报：
                                    </td>
                                    <td>
                                        <cc1:ExTextBox ID="txtPCOS" runat="server" checkexpession="Double" datacol="yes"
                                            err="此项" FieldName="PCOS" BindName="OCInstrumentInfoObj" Width="50" />
                                    </td>
                                    <td class="inner_cell_right">
                                       COS（ug/m³）高报：
                                    </td>
                                    <td>
                                        <cc1:ExTextBox ID="txtMCOS" runat="server" checkexpession="Double" datacol="yes"
                                            err="此项" FieldName="MCOS" BindName="OCInstrumentInfoObj" Width="50" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="inner_cell_right">
                                        C2F6（ug/m³）预报：
                                    </td>
                                    <td>
                                        <cc1:ExTextBox ID="txtPC2F6" runat="server" checkexpession="Double" datacol="yes"
                                            err="此项" FieldName="PC2F6" BindName="OCInstrumentInfoObj" Width="50" />
                                    </td>
                                    <td class="inner_cell_right">
                                      C2F6（ug/m³）高报：
                                    </td>
                                    <td>
                                        <cc1:ExTextBox ID="txtMC2F6" runat="server" checkexpession="Double" datacol="yes"
                                            err="此项" FieldName="MC2F6" BindName="OCInstrumentInfoObj" Width="50" />
                                    </td>
                                    <td class="inner_cell_right">
                                       C3F8（ug/m³）预报：
                                    </td>
                                    <td>
                                        <cc1:ExTextBox ID="txtPC3F8" runat="server" checkexpession="Double" datacol="yes"
                                            err="此项" FieldName="PC3F8" BindName="OCInstrumentInfoObj" Width="50" />
                                    </td>
                                    <td class="inner_cell_right">
                                       C3F8（ug/m³）高报：
                                    </td>
                                    <td>
                                        <cc1:ExTextBox ID="txtMC3F8" runat="server" checkexpession="Double" datacol="yes"
                                            err="此项" FieldName="MC3F8" BindName="OCInstrumentInfoObj" Width="50" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="inner_cell_right">
                                        组分种数预警：
                                    </td>
                                    <td colspan="6">
                                        <cc1:ExTextBox ID="txtAlterCount" runat="server" checkexpession="Double" datacol="yes"
                                            err="此项" FieldName="AlterCount" BindName="OCInstrumentInfoObj" Width="50" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <div style="text-align: center;">
                    <asp:Button ID="btnSaveLog" runat="server" Text="保存" OnClick="btnSave_Click"  OnClientClick="return CheckDataValid('#entendfrom');" />
             
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <table id="Student_Table">
    </table>
    </form>
    <script type="text/javascript" src="../Themes/Scripts/jqueryeasyui/jquery.easyui.min.js"></script>
    <script src="../Themes/Scripts/jqueryeasyui/plugins/jquery.edatagrid.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function myfunction() {
            GetTable();


           
        });

    

        function fixWidth(percent) {
            return (document.body.clientWidth - 5) * percent;
        }


        function GetTable() {
            var editRow = undefined;
            var $width = 1100;
            var targatarr = [{ id: 'SO2', text: 'SO2' }, { id: 'CO', text: 'CO' },
                           { id: 'CF4', text: 'CF4' },
                            { id: 'SO2F2', text: 'SO2F2' },
                             { id: 'SOF2', text: 'SOF2' },
                              { id: 'CS2', text: 'CS2' },
                               { id: 'HF', text: 'HF' },
                               { id: 'H2S', text: 'H2S' },
                               { id: 'COS', text: 'COS' },
                               { id: 'C2F6', text: 'C2F6' },
                               { id: 'C3F8', text: 'C3F8' },
                               
                          ];
            $("#Student_Table").datagrid({
                height: 250,
                width: $width,
                fitColumns: true,
                title: '组分比例预警值设置',
                collapsible: true,
                singleSelect: true,
                url: 'CRUDHandler.ashx',
                queryParams: { action: 'GetOCInstrumentInfo_Childs', CheckPointCode: '<%= OCInstrumentInfoObj.CheckPointCode %>' },
                idField: 'OID',
                columns: [[
                 { field: 'OID', title: 'ID', hidden: true },
                  { field: 'CheckPointCode', title: 'CheckPointCode', hidden: true },
                  { field: 'TopTarget', align: 'center', width: fixWidth(0.3), title: "分子", editor: { type: 'combobox',
                      options: {
                          data: targatarr,
                          valueField: 'id',
                          textField: 'text',
                          required: true
                      }
                  }
                  },
		 { field: 'BottomTarget', align: 'center', width: fixWidth(0.3), title: '分母', editor: { type: 'combobox',
		     options: {
		         data: targatarr,
		         valueField: 'id',
		         textField: 'text',
		         required: true
		     }
		 }
		 },
        	{ field: 'MaxValue', title: '值', width: fixWidth(0.3), align: 'center', editor: { type: 'text', options: { required: true}} }
        ]],
                toolbar: [{
                    text: '添加', iconCls: 'icon-add', handler: function () {
                        if (editRow != undefined) {
                            $("#Student_Table").datagrid('endEdit', editRow);
                        }
                        if (editRow == undefined) {

                            $("#Student_Table").datagrid('insertRow', {
                                index: 0,
                                row: { CheckPointCode: '<%= OCInstrumentInfoObj.CheckPointCode %>' }
                            });

                            $("#Student_Table").datagrid('beginEdit', 0);
                            editRow = 0;
                        }
                    }
                }, '-', {
                    text: '保存', iconCls: 'icon-save', handler: function () {

                        var editIndex = editRow;

                        $("#Student_Table").datagrid('endEdit', editRow);

                        $("#Student_Table").datagrid('selectRow', editIndex)
                        var rowstr = $("#Student_Table").datagrid('getSelected', editIndex)

                        //如果调用acceptChanges(),使用getChanges()则获取不到编辑和新增的数据。

                        //使用JSON序列化datarow对象，发送到后台。
                        //  var rows = $("#Student_Table").datagrid('getChanges');

                        // var rowstr = JSON.stringify(rowstr);
                        $.post('CRUDHandler.ashx?action=SaveGetOCInstrumentInfo_Childs', rowstr, function (data) {
                            showFaceMsg(data);


                        });

                    }
                }, '-', {
                    text: '撤销', iconCls: 'icon-redo', handler: function () {
                        editRow = undefined;
                        $("#Student_Table").datagrid('rejectChanges');
                        $("#Student_Table").datagrid('unselectAll');
                    }
                }, '-', {
                    text: '删除', iconCls: 'icon-remove', handler: function () {
                        var row = $("#Student_Table").datagrid('getSelected');
                        var index = $("#Student_Table").datagrid('getRowIndex', row);


                        $.post('CRUDHandler.ashx?action=Delete&table=OCInstrumentInfo_Childs', { oid: row.OID }, function (data) {
                            if (data == "1") {
                                $("#Student_Table").datagrid('deleteRow', index);
                                showFaceMsg("删除成功");
                            }
                            else {
                                showFaceMsg("删除失败");
                            }

                        });

                    }
                }, '-', {
                    text: '修改', iconCls: 'icon-edit', handler: function () {
                        var row = $("#Student_Table").datagrid('getSelected');
                        if (row != null) {
                            if (editRow != undefined) {
                                $("#Student_Table").datagrid('endEdit', editRow);
                            }

                            if (editRow == undefined) {
                                var index = $("#Student_Table").datagrid('getRowIndex', row);
                                $("#Student_Table").datagrid('beginEdit', index);
                                editRow = index;
                                $("#Student_Table").datagrid('unselectAll');
                            }
                        }
                    }
                }, '-', {
                    text: '刷新', iconCls: 'icon-reload', handler: function () {

                        $("#Student_Table").datagrid("reload");
                    }

                }
                ],
                onAfterEdit: function (rowIndex, rowData, changes) {
                    editRow = undefined;
                },
                onDblClickRow: function (rowIndex, rowData) {
                    if (editRow != undefined) {
                        $("#Student_Table").datagrid('endEdit', editRow);
                    }

                    if (editRow == undefined) {
                        $("#Student_Table").datagrid('beginEdit', rowIndex);
                        editRow = rowIndex;
                    }
                },
                onClickRow: function (rowIndex, rowData) {
                    if (editRow != undefined) {
                        $("#Student_Table").datagrid('endEdit', editRow);

                    }

                }

            });
        }
    </script>
</body>
</html>

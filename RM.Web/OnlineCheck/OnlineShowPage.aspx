﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="OnlineShowPage.aspx.cs"
    Inherits="RM.Web.OnlineCheck.OnlineShowPage" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>监测数据展示</title>
    <link href="~/Themes/Styles/Site.css" rel="stylesheet" type="text/css" />
    <script src="../Themes/Scripts/Validator/JValidator.js" type="text/javascript"></script>
    <script src="../Themes/Scripts/jquery-1.8.2.min.js" type="text/javascript"></script>
    <script src="../Themes/Scripts/jquery.pullbox.js" type="text/javascript"></script>
    <script src="../Themes/Scripts/FunctionJS.js" type="text/javascript"></script>
    <script src="../Themes/Scripts/DatePicker/WdatePicker.js" type="text/javascript"></script>
    <style type="text/css">
        html, body {
            margin: 0;
            height: 100%;
        }

        #containt {
            height: 100%;
        }

        form {
            height: 100%;
        }

        #UpdatePanel {
            height: 100%;
        }

        #containt > #UpdatePanel > .Panel {
            float: left;
            height: -moz-calc(100% - 0px);
            height: -webkit-calc(100% - 00px);
            height: calc(100% - 0px);
        }

        .itemPanel {
            width: 170px;
            height: 240px;
            float: left;
            margin: 10px 10px 10px 10px;
            border-width: 1px;
            border-style: solid;
            cursor: pointer;
        }

            .itemPanel > div {
                float: left;
                height: -moz-calc(100% - 0px);
                height: -webkit-calc(100% - 0px);
                height: calc(100% - 0px);
            }

                .itemPanel > div > p {
                    margin-top: 0px;
                    margin-bottom: 0px;
                    font-size: 12px;
                }

        .leftItem {
            border-right-width: 1px;
            border-right-style: solid;
            text-align:center;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div id="containt">
            <asp:ScriptManager ID="ScriptManager1" runat="server">
            </asp:ScriptManager>
            <asp:UpdatePanel ID="UpdatePanel" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="leftPanel" class="Panel" style="width: 20%; border-right-width: 1px; border-right-style: solid;">
                        <div id="searchDiv" style="height: 165px;">
                            测点查询
                        <table style="width: 100%">
                            <tr>
                                <td>供电局：
                                </td>
                                <td>
                                    <cc1:ExDropDownList ID="ddlPowerSupplyName" runat="server" CssClass="select" FieldName="PowerSupplyCode"
                                        AutoPostBack="true" OnSelectedIndexChanged="ddlPowerSupplyName_SelectedIndexChanged"
                                        BindName="InfoDevParaObj" Width="131" />
                                </td>
                            </tr>
                            <tr>
                                <td>所属变电站：
                                </td>
                                <td>
                                    <cc1:ExDropDownList ID="ddlConvertStationName" runat="server" CssClass="select" FieldName="ConvertStationCode"
                                        BindName="InfoDevParaObj" Width="131" />
                                </td>
                            </tr>
                            <tr>
                                <td>设备类型
                                </td>
                                <td>
                                    <cc1:ExDropDownList ID="ddlDevClass" runat="server" CssClass="select" FieldName="DevClass"
                                        BindName="InfoDevParaObj" Width="131" />
                                </td>
                            </tr>
                            <tr>
                                <td>电压等级
                                </td>
                                <td>
                                    <cc1:ExDropDownList ID="ddlVoltageLevel" runat="server" CssClass="select" FieldName="VoltageLevel"
                                        BindName="InfoDevParaObj" Width="131" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" style="text-align: center;">
                                    <asp:Button ID="btnSearch" runat="server" OnClick="btnSearch_Click" Text="查询" />
                                </td>
                            </tr>
                        </table>
                        </div>
                        <div class="div-body">
                            <table id="table1" class="grid" singleselect="true">
                                <colgroup>
                                    <col width="20%" />
                                    <col width="50%" />
                                    <col width="30%" />
                                </colgroup>
                                <thead>
                                    <tr>
                                        <td style="text-align: center;">测点编码
                                        </td>
                                        <td style="text-align: center;">测点名称
                                        </td>
                                        <td style="text-align: center;">所属设备
                                        </td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <asp:Repeater ID="rp_Item" runat="server">
                                        <ItemTemplate>
                                            <tr>
                                                <td style="text-align: center;">
                                                    <%# Eval("CheckPointCode")%>
                                                </td>
                                                <td style="text-align: center;">
                                                   <a href="#" onclick="initClick('<%# Eval("CheckPointCode")%>')" style="color:blue;">  <%# Eval("CheckPointName")%></a> 
                                                </td>
                                                <td style="text-align: center;">
                                                    <%# Eval("BelongEquipmentName")%>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div id="rightPanel" class="Panel" style="width: 79%;">
                        <asp:Repeater ID="Repeater1" runat="server">
                            <ItemTemplate>
                                <div class="itemPanel" id="<%# Eval("CheckPointCode")%>" onclick="initClick('<%# Eval("CheckPointCode")%>')">
                                    <div class="leftItem" style="width: 40%;">
                                        <image src="../Themes/Images/networking.png"></image>
                                        <br />
                                        <span style="font-weight: bold;">
                                            <%# Eval("CheckPointName")%></span>
                                        <br />
                                        <span><%# Eval("CollectionDatetime", "{0:yyyy-MM-dd HH:mm:ss}")%></span>
                                    </div>
                                    <div style="width: 59%">
                                        <p>
                                            压力:<%# GetDataWithColor(Eval("ControlPressureMin"),Eval("Pressure"),Eval("ControlPressureMax")) %>Mpa
                                        </p>
                                        <p>
                                            温度:<%# GetDataWithColor(Eval("ControlTempertureMin"),Eval("Temperture"),Eval("ControlTempertureMax"))%>℃ 
                                        </p>
                                        <p>
                                            微水:<%# GetDataWithColor(Eval("ControlMicroWater"),Eval("MicroWater"),Eval("ControlMicroWaters"))%>ug/m3
                                        </p>
                                        <p>
                                            SO2:<%# GetDataWithColor(Eval("ControlSO2"),Eval("SO2"),Eval("ControlSO2s"))%>ug/m³
                                        </p>
                                        <p>
                                            CO:<%# GetDataWithColor(Eval("ControlCO"),Eval("CO"),Eval("ControlCOs"))%>ug/m³
                                        </p>
                                        <p>
                                            CF4:<%# GetDataWithColor(Eval("ControlCF4"),Eval("CF4"),Eval("ControlCF4s"))%>ug/m³
                                        </p>
                                        <p>
                                            SO2F2:<%# GetDataWithColor(Eval("ControlSO2F2"),Eval("SO2F2"),Eval("ControlSO2F2s"))%>ug/m³
                                        </p>
                                        <p>
                                            SOF2:<%# GetDataWithColor(Eval("ControlSOF2"),Eval("SOF2"),Eval("ControlSOF2s"))%>ug/m³
                                        </p>
                                        <p>
                                            CS2:<%# GetDataWithColor(Eval("ControlCS2"),Eval("CS2"),Eval("ControlCS2s"))%>ug/m³
                                        </p>
                                        <p>
                                            HF:<%# GetDataWithColor(Eval("ControlHF"),Eval("HF"),Eval("ControlHFs"))%>ug/m³
                                        </p>
                                        <p>
                                            H2S:<%# GetDataWithColor(Eval("PH2S"),Eval("H2S"),Eval("MH2S"))%>ug/m³
                                        </p>
                                        <p>
                                            COS:<%# GetDataWithColor(Eval("PCOS"),Eval("COSS"),Eval("MCOS"))%>ug/m³
                                        </p>
                                        <p>
                                            C2F6:<%# GetDataWithColor(Eval("PC2F6"),Eval("C2F6"),Eval("MC2F6"))%>ug/m³
                                        </p>
                                        <p>
                                            C3F8:<%# GetDataWithColor(Eval("PC3F8"),Eval("C3F8"),Eval("MC3F8"))%>ug/m³
                                        </p>
                                    </div>
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="ddlPowerSupplyName" EventName="SelectedIndexChanged" />
                    <asp:AsyncPostBackTrigger ControlID="btnSearch" EventName="Click" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </form>
    <script type="text/javascript">
        var url = "/OnlineCheck/OCInstrumentInfoOnlineList.aspx";

        function initClick(checkPointCode) {
            top.openDialog(url + "?CheckPointCode=" + checkPointCode, 'OCInstrumentInfoOnlineList', '显示历史数据', 1250, 620, 50, 50);
        }
    </script>
</body>
</html>

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using EGMNGS.Common;

namespace RM.Web.OnlineCheck
{
    public partial class OnlineAnalysisPage : System.Web.UI.Page
    {
        public EGMNGS.Model.InfoDevPara InfoDevParaObj
        {
            get
            {
                if (ViewState["InfoDevParaObj"] == null)
                {
                    ViewState["InfoDevParaObj"] = new EGMNGS.Model.InfoDevPara();
                }

                return ViewState["InfoDevParaObj"] as EGMNGS.Model.InfoDevPara;
            }
            set { ViewState["InfoDevParaObj"] = value; }
        }
        private readonly EGMNGS.BLL.OCInstrumentInfo bll = new EGMNGS.BLL.OCInstrumentInfo();
        private readonly EGMNGS.BLL.OCInstrumentInfoOnline bllOnline = new EGMNGS.BLL.OCInstrumentInfoOnline();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                DropDownListBinder();
                DataBindGrid();

            }

        }

        private void DropDownListBinder()
        {
            EGMNGS.BLL.SYS_CODE sysCodeBll = new EGMNGS.BLL.SYS_CODE();
            DataSet dsYear = sysCodeBll.GetList("CODE_TYPE='DEVClass' ORDER BY DISPLAY_ORDER");

            this.ddlDevClass.DataSource = dsYear.Tables[0];
            this.ddlDevClass.DataTextField = "CODE_CHI_DESC";
            this.ddlDevClass.DataValueField = "CODE";
            this.ddlDevClass.DataBind();
            this.ddlDevClass.Items.Insert(0, string.Empty);

            DataSet dsMonth = sysCodeBll.GetList("CODE_TYPE='DYLev' ORDER BY DISPLAY_ORDER");

            this.ddlVoltageLevel.DataSource = dsMonth.Tables[0];
            this.ddlVoltageLevel.DataTextField = "CODE_CHI_DESC";
            this.ddlVoltageLevel.DataValueField = "CODE";
            this.ddlVoltageLevel.DataBind();
            this.ddlVoltageLevel.Items.Insert(0, string.Empty);


            DataTable dtPowerStation = ComServies.GetAllPowerStation();
            this.ddlPowerSupplyName.DataSource = dtPowerStation;
            this.ddlPowerSupplyName.DataTextField = "Organization_Name";
            this.ddlPowerSupplyName.DataValueField = "Organization_ID";
            this.ddlPowerSupplyName.DataBind();
            this.ddlPowerSupplyName.Items.Insert(0, string.Empty);

        }


        protected void ddlPowerSupplyName_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadPowerSupplyName();
        }

        private void LoadPowerSupplyName()
        {
            this.ddlConvertStationName.Items.Clear();
            EGMNGS.BLL.InfoPowerSupplyConvertStation bll = new EGMNGS.BLL.InfoPowerSupplyConvertStation();
            DataSet dsPowerSupplyCode = bll.GetList(string.Format("PowerSupplyCode='{0}'", this.ddlPowerSupplyName.SelectedValue));
            this.ddlConvertStationName.DataSource = dsPowerSupplyCode.Tables[0];
            this.ddlConvertStationName.DataTextField = "ConvartStationName";
            this.ddlConvertStationName.DataValueField = "CovnertStationCode";
            this.ddlConvertStationName.DataBind();
            this.ddlConvertStationName.Items.Insert(0, string.Empty);
        }
        /// <summary>
        /// 绑定数据源
        /// </summary>
        private void DataBindGrid()
        {
            string sqlSearchwhere = string.Format(@" SELECT 
      [DevCode]
  FROM [EGMNGS].[dbo].[InfoDevPara] ");
            string where = "where 1=1";



            if (this.ddlPowerSupplyName.SelectedIndex > 0)
            {
                where += string.Format("and PowerSupplyCode='{0}' ", this.ddlPowerSupplyName.SelectedValue);
            }


            if (ddlConvertStationName.SelectedIndex > 0)
            {
                where += string.Format("and ConvertStationCode='{0}' ", ddlConvertStationName.SelectedValue);
            }
            if (this.ddlDevClass.SelectedIndex > 0)
            {
                where += string.Format("and DevClass='{0}' ", ddlDevClass.SelectedValue);
            }

            if (this.ddlVoltageLevel.SelectedIndex > 0)
            {
                where += string.Format("and VoltageLevel='{0}' ", ddlVoltageLevel.SelectedValue);
            }
            sqlSearchwhere += where;

            DataTable dt = bll.GetList(string.Format("BelongEquipment IN ({0}) order by CheckPointCode", sqlSearchwhere)).Tables[0];
            this.rp_Item.DataSource = dt;
            this.rp_Item.DataBind();


        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            DataBindGrid();
        }
    }
}
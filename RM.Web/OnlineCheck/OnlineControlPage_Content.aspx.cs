﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using RM.Web.App_Code;
using RM.Common.DotNetUI;

namespace RM.Web.OnlineCheck
{
    public partial class OnlineControlPage_Content : PageBase
    {
        private readonly EGMNGS.BLL.OCInstrumentInfo bll = new EGMNGS.BLL.OCInstrumentInfo();
        public string code = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            this.PageControl1.pageHandler += new EventHandler(pager_PageChanged);
          

            if (!IsPostBack)
            {

            }
        }

        protected void rp_Item_ItemCommand(object source, RepeaterCommandEventArgs e)
        {

        }

        protected void rp_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

        }
        protected void pager_PageChanged(object sender, EventArgs e)
        {
            DataBindGrid();
        }

        private void DataBindGrid()
        {
            string sql = @"1=1";
            code = Request["code"] ?? "";
            if (code.StartsWith("SB"))
            {
                sql += string.Format(" and BelongEquipment='{0}'",code);
            }
            else if (code.StartsWith("BD"))
            {
                sql += string.Format(" and BelongPowerStation='{0}'", code);
            }
            int count = bll.GetRecordCount(sql);
            DataSet ds = bll.GetListByPage(sql, "CREATED_DATE", PageControl1.PageIndex, PageControl1.PageSize);
            ControlBindHelper.BindRepeaterList(ds.Tables[0], rp_Item);
            this.PageControl1.RecordCount = Convert.ToInt32(count);
        }
    }
}
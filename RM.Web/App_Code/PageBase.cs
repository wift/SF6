﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RM.Common.DotNetBean;
using System.Web.UI;
using System.Collections;
using RM.Common.DotNetUI;
using System.Data;
using System.Text;
using RM.Common.DotNetCode;
using RM.Busines.IDAO;
using RM.Busines.DAL;
using System.Reflection;

namespace RM.Web.App_Code
{
    public enum eActionMode
    {
        NotExecute = 0,
        Add = 1,
        Delete = 2,
        Update = 3,
        Search = 4

    }

    /// <summary>
    /// 基类
    /// </summary>
    public class PageBase : System.Web.UI.Page
    {
        RM_System_IDAO sys_idao = new RM_System_Dal();
        public bool CheckURLPermission
        {
            get;
            set;
        }
        protected override void OnLoad(EventArgs e)
        {
            #region 当Session过期自动跳出登录画面
            if (RequestSession.GetSessionUser() == null)
            {
                Session.Abandon();  //取消当前会话
                Session.Clear();
                Response.Redirect("/Index.htm");
            }
            #endregion

            #region 防止刷新重复提交
            if (null == Session["Token"])
            {
                WebHelper.SetToken();
            }
            #endregion

            #region URL权限验证,拒绝，不合法的请求
            if (!CheckURLPermission)
            {
                URLPermission();
            }
            #endregion

            if (this.FindControl("ScriptManager1") != null)
            {
                string jsStr = "  if (typeof(InitControl)=='function')  InitControl();";//@" $('.div-body').PullBox({ dv: $('.div-body'), obj: $('#table1').find('tr') });divresize(115);";

                ShowMsgHelper.ShowScriptAjax(jsStr);

            }

            base.OnLoad(e);
        }
        #region URL权限验证,拒绝，不合法的请求
        /// <summary>
        /// URL权限验证,拒绝，不合法的请求
        /// </summary>
        public void URLPermission()
        {
            bool IsOK = false;
            //获取当前访问页面地址
            string requestPath = RequestHelper.GetScriptName;
            string[] filterUrl = { "/Frame/HomeIndex.aspx", "/RMBase/SysUser/UpdateUserPwd.aspx" };//过滤特别页面
            //对上传的文件的类型进行一个个匹对
            for (int i = 0; i < filterUrl.Length; i++)
            {
                if (requestPath == filterUrl[i])
                {
                    IsOK = true;
                    break;
                }
            }
            if (!IsOK)
            {
                string UserId = RequestSession.GetSessionUser().UserId.ToString();//用户ID

                DataTable dt = sys_idao.GetPermission_URL(UserId);
                DataView dv = new DataView(dt);
                dv.RowFilter = "NavigateUrl = '" + requestPath + "'";
                if (dv.Count == 0)
                {
                    StringBuilder strHTML = new StringBuilder();
                    strHTML.Append("<div style='text-align: center; line-height: 300px;'>");
                    strHTML.Append("<font style=\"font-size: 13;font-weight: bold; color: red;\">权限不足</font></div>");
                    HttpContext.Current.Response.Write(strHTML.ToString());
                    HttpContext.Current.Response.End();
                }
            }

        }

        public SessionUser User
        {
            get
            {
                return RequestSession.GetSessionUser();
            }
        }

        /// <summary>
        /// 获取平台所有字典
        /// </summary>
        public Dictionary<string, Dictionary<string, string>> DictEGMNS
        {
            get
            {
                if (Session["DictEGMNS"] == null)
                {
                    Dictionary<string, Dictionary<string, string>> tempDictEGMNS = new Dictionary<string, Dictionary<string, string>>();


                    EGMNGS.BLL.SYS_CODE sysCodeBll = new EGMNGS.BLL.SYS_CODE();
                    EGMNGS.BLL.SYS_CODE_TYPE sysCodeTypeBll = new EGMNGS.BLL.SYS_CODE_TYPE();
                    List<EGMNGS.Model.SYS_CODE_TYPE> listSYS_CODE_TYPE = sysCodeTypeBll.GetModelList("1=1");
                    List<EGMNGS.Model.SYS_CODE> listSYS_CODE = sysCodeBll.GetModelList("1=1");
                    foreach (var item in listSYS_CODE_TYPE)
                    {
                        Dictionary<string, string> tempCode = new Dictionary<string, string>();
                        foreach (var itemCode in listSYS_CODE.Where(o => o.CODE_TYPE == item.CODE_TYPE))
                        {
                            tempCode.Add(itemCode.CODE, itemCode.CODE_CHI_DESC);
                        }
                        tempCode.Add("", "");
                        tempDictEGMNS.Add(item.CODE_TYPE, tempCode);
                    }

                    Session["DictEGMNS"] = tempDictEGMNS;
                    return tempDictEGMNS;
                }
                return Session["DictEGMNS"] as Dictionary<string, Dictionary<string, string>>;
            }
            set { Session["DictEGMNS"] = value; }
        }
        #endregion



        protected virtual void btnSearch_Click(object sender, EventArgs e)
        {
            // CurrentButtonEvent = "Search";
            ActionMode = eActionMode.NotExecute;
            DataBindGrid();
        }

        public virtual void DataBindGrid()
        {
            //CurrentButtonEvent = "Search";
        }

        public virtual void Add()
        {
            DataBindGrid();
        }

        public virtual void Delete()
        {
            DataBindGrid();
        }

        public virtual void Update()
        {
            DataBindGrid();
        }

        public virtual void GenFindWhere(ref string sqlStr)
        {
            if (ActionMode == eActionMode.NotExecute)
            {
                return;
            }

            StringBuilder sb = new StringBuilder();
            foreach (var item in Request.Params)
            {
                if (item == null)
                {
                    continue;

                }
                if (item.ToString().StartsWith("txts") || item.ToString().StartsWith("ddls"))
                {
                    string paramsValue = Request.Params.Get(item.ToString());

                    if (string.IsNullOrEmpty(paramsValue))
                    {
                        continue;
                    }

                    if (ValidateUtil.IsDate(paramsValue))
                    {
                        sb.AppendFormat(" and {0} = '{1}' ", item.ToString().Remove(0, 4), paramsValue);
                    }
                    else
                    {
                        sb.AppendFormat(" and {0} like '%{1}%' ", item.ToString().Remove(0, 4), paramsValue);
                    }
                    
                }
            }

            sqlStr += sb.ToString();

        }

        public void Save()
        {
            if (ActionMode == eActionMode.Add)
            {
                try
                {
                    Add();
                    ShowMsgHelper.Alert("添加成功！");
                    ActionMode = eActionMode.Update;
                }
                catch (Exception ex)
                {
                    if (ex is System.Data.SqlClient.SqlException)
                    {
                        string str = ex.Message.Remove(0, ex.Message.IndexOf('_') + 1);
                        string m_name = str.Substring(0, str.IndexOf('\''));

                        bool isRe = (ex as System.Data.SqlClient.SqlException).ErrorCode == -2146232060;
                        ; if (isRe)
                        {
                            ShowMsgHelper.ExecuteScript(string.Format("ChangeCss($('#txt{0}'), '此项已重复！');", m_name));
                            return;
                        }
                    }
                    ShowMsgHelper.Alert("添加失败！");
                    throw ex;

                }
            }
            else if (ActionMode == eActionMode.Delete)
            {
                try
                {
                    Delete();
                    ShowMsgHelper.Alert("删除成功！");

                }
                catch (Exception)
                {
                    ShowMsgHelper.Alert("删除失败！");
                    throw;

                }
                ActionMode = eActionMode.NotExecute;
            }
            else if (ActionMode == eActionMode.Update)
            {
                try
                {
                    Update();
                    ShowMsgHelper.Alert("修改成功！");
                }
                catch (Exception)
                {
                    ShowMsgHelper.Alert("修改失败！");
                    throw;
                }

            }

            ActionMode = eActionMode.NotExecute;
        }

        public eActionMode ActionMode
        {
            get
            {
                if (ViewState["eActionMode"] == null)
                {
                    ViewState["eActionMode"] = eActionMode.NotExecute;
                }

                return (eActionMode)ViewState["eActionMode"];
            }
            set { ViewState["eActionMode"] = value; }
        }

    }


}


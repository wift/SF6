﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using EGMNGS.Common;
using DapperData;
using RM.Web.App_Code;
using EGMNGS.Model;

namespace RM.Web.MagneticPage
{
    public partial class Megnetic_CurrentAnalysisPage : PageBase
    {

     
        private readonly EGMNGS.BLL.OCInstrumentInfo bll = new EGMNGS.BLL.OCInstrumentInfo();
   
        private readonly Repository _Repository = new Repository();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                DropDownListBinder();
                DataBindGrid();
            }

        }

        private void DropDownListBinder()
        {
            EGMNGS.BLL.SYS_CODE sysCodeBll = new EGMNGS.BLL.SYS_CODE();

            DataSet dsMonth = sysCodeBll.GetList("CODE_TYPE='DYLev' ORDER BY DISPLAY_ORDER");

            this.ddlVoltageLevel.DataSource = dsMonth.Tables[0];
            this.ddlVoltageLevel.DataTextField = "CODE_CHI_DESC";
            this.ddlVoltageLevel.DataValueField = "CODE";
            this.ddlVoltageLevel.DataBind();
            this.ddlVoltageLevel.Items.Insert(0, string.Empty);

            DataTable dtPowerStation = ComServies.GetAllPowerStation();
            this.ddlPowerSupplyName.DataSource = dtPowerStation;
            this.ddlPowerSupplyName.DataTextField = "Organization_Name";
            this.ddlPowerSupplyName.DataValueField = "Organization_ID";
            this.ddlPowerSupplyName.DataBind();
            this.ddlPowerSupplyName.Items.Insert(0, string.Empty);

        }


        protected void ddlPowerSupplyName_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadPowerSupplyName();
        }

        private void LoadPowerSupplyName()
        {
            this.ddlConvertStationName.Items.Clear();
            EGMNGS.BLL.InfoPowerSupplyConvertStation bll = new EGMNGS.BLL.InfoPowerSupplyConvertStation();
            DataSet dsPowerSupplyCode = bll.GetList(string.Format("PowerSupplyCode='{0}'", this.ddlPowerSupplyName.SelectedValue));
            this.ddlConvertStationName.DataSource = dsPowerSupplyCode.Tables[0];
            this.ddlConvertStationName.DataTextField = "ConvartStationName";
            this.ddlConvertStationName.DataValueField = "CovnertStationCode";
            this.ddlConvertStationName.DataBind();
            this.ddlConvertStationName.Items.Insert(0, string.Empty);
        }
        /// <summary>
        /// 绑定数据源
        /// </summary>
        private void DataBindGrid()
        {
            
            string wheresql = "";



            if (this.ddlPowerSupplyName.SelectedIndex > 0)
            {
                wheresql += string.Format("and c.PowerSupplyCode='{0}' ", this.ddlPowerSupplyName.SelectedValue);
            }


            if (ddlConvertStationName.SelectedIndex > 0)
            {
                wheresql += string.Format("and b.ConvertStationCode='{0}' ", ddlConvertStationName.SelectedValue);
            }


            if (this.ddlVoltageLevel.SelectedIndex > 0)
            {
                wheresql += string.Format("and d.VoltageLevel='{0}' ", ddlVoltageLevel.SelectedValue);
            }

            if (ddlTemplateType.SelectedIndex > 0)
            {
                wheresql += string.Format("and b.TemplateType='{0}' ", ddlTemplateType.SelectedValue);

            }

            if (txtCheckPointPositionName.Text.Trim().Length > 0)
            {
                wheresql += string.Format("and a.CheckPointPosition like '%{0}%' ", txtCheckPointPositionName.Text.Trim());
            }

            if (txtPositionName.Text.Trim().Length > 0)
            {
                wheresql += string.Format("and a.PositionName like '%{0}%' ", txtPositionName.Text.Trim());
            }

            string sql = @"select a.oid,c.ConvartStationName,b.TemplateType,a.CheckPointPosition,
a.PositionName,b.MeasuringDate
 from Magnetic_StationCheckPointMNGChilds a
left join Magnetic_CheckPointTemplatesB b on a.Magnetic_CheckPointTemplatesB_OID=b.OID
left join InfoPowerSupplyConvertStation c on c.CovnertStationCode=b.ConvertStationCode
left join Magnetic_CheckPointTemplates d on d.TemplateType=b.TemplateType
where 1=1 ";
            DataTable dt = ComServies.Query(sql + wheresql); ;
            this.rp_Item.DataSource = dt;
            this.rp_Item.DataBind();


        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            DataBindGrid();

            
        }
        /// <summary>
        /// 加载模板类型
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlVoltageLevel_SelectedIndexChanged(object sender, EventArgs e)
        {
          var lists=  _Repository.GetByName<Magnetic_CheckPointTemplates>("VoltageLevel", this.ddlVoltageLevel.SelectedValue);
          ddlTemplateType.DataSource = lists;

          this.ddlTemplateType.DataTextField = "TemplateType";
          this.ddlTemplateType.DataValueField = "TemplateType";
          this.ddlTemplateType.DataBind();
          this.ddlTemplateType.Items.Insert(0, string.Empty);
        }
    }
}
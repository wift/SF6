﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using RM.Web.App_Code;

namespace RM.Web.MagneticPage
{
    public partial class Magnetic_CheckPointInfoPage : PageBase
    {
        public StringBuilder treeItem_Table = new StringBuilder();
        public string RunStatusStrJson = string.Empty;
        public string VoltageLevelStrJson = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                GetTreeTable();

                List<object> list = new List<object>();
                List<object> listVoltageLevel = new List<object>();

                foreach (var item in DictEGMNS["RunStatus"])
                {
                    list.Add(new { key = item.Key, value = item.Value });
                }

                foreach (var item in DictEGMNS["DYLev"])
                {
                    listVoltageLevel.Add(new { key = item.Key, value = item.Value });
                }

                RunStatusStrJson = Newtonsoft.Json.JsonConvert.SerializeObject(list);
                VoltageLevelStrJson = Newtonsoft.Json.JsonConvert.SerializeObject(listVoltageLevel);
            }
        }

        // <summary>
        /// 所以变电站设置信息
        /// </summary>
        public void GetTreeTable()
        {
            DataSet dtroot = Maticsoft.DBUtility.DbHelperSQL.Query("select DISTINCT PowerSupplyCode,PowerSupplyName from [EGMNGS].[dbo].[InfoPowerSupplyConvertStation] where IsMagnetic='0'");

            foreach (DataRow drv in dtroot.Tables[0].Rows)
            {
                DataSet dssecond = Maticsoft.DBUtility.DbHelperSQL.Query(string.Format(@"select DISTINCT CovnertStationCode as ConvertStationCode,ConvartStationName as ConvertStationName from [EGMNGS].[dbo].[InfoPowerSupplyConvertStation] where PowerSupplyCode='{0}' and IsMagnetic='0'", drv["PowerSupplyCode"]));

                treeItem_Table.Append("<li>");
                treeItem_Table.Append("<div onclick=\"GetTableByPowerSupplyCode('" + drv["PowerSupplyCode"] + "',this)\">" + drv["PowerSupplyName"] + "</div>");
                if (dssecond.Tables.Count > 0 && dssecond.Tables[0].Rows.Count > 0)
                {
                    treeItem_Table.Append("<ul>");

                    foreach (DataRow item in dssecond.Tables[0].Rows)
                    {
                        treeItem_Table.Append("<li id='"+ item["ConvertStationCode"] + "'>");
                        treeItem_Table.Append("<div onclick=\"GetTable('" + item["ConvertStationCode"] + "','" + item["ConvertStationName"] + "')\">" + item["ConvertStationName"] + "</div>");

                        //DataSet dsThree = Maticsoft.DBUtility.DbHelperSQL.Query(string.Format("select DISTINCT  [DevCode],[DevName] from [EGMNGS].[dbo].[InfoDevPara] where ConvertStationCode='{0}'", item["ConvertStationCode"]));

                        //if (dsThree.Tables.Count > 0 && dsThree.Tables[0].Rows.Count > 0)
                        //{
                        //    treeItem_Table.Append("<ul>");
                        //    foreach (DataRow itemThree in dsThree.Tables[0].Rows)
                        //    {
                        //        treeItem_Table.Append("<li><div onclick=\"GetTable('" + itemThree["DevCode"] + "')\"><img src=\"/Themes/Images/20130502112716785_easyicon_net_16.png\" width=\"16\" height=\"16\" />" + itemThree["DevName"] + "</div></li>");

                        //    }
                        //    treeItem_Table.Append("</ul>");
                        //}
                        treeItem_Table.Append("</li>");
                    }


                    treeItem_Table.Append("</ul>");
                }

                treeItem_Table.Append("</li>");
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;
using DapperData;
using EGMNGS.Model;
using RM.Common.DotNetBean;
using RM.Web.OnlineCheck;
using System.Data;
using Aspose.Cells;
using System.Data.SqlClient;
using Maticsoft.DBUtility;
using EGMNGS.Common;

namespace RM.Web.MagneticPage
{
    /// <summary>
    /// Magnetic_CheckPointInfoHandler 的摘要说明
    /// </summary>
    public class Magnetic_CheckPointInfoHandler : IHttpHandler, IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "application/json";

            string action = context.Request["action"] ?? "";
          
            string returnJsonStr = string.Empty;
            if (action == "GetMagnetic_CheckPointInfoList")//获取站点信息列表
            {
                string m_ConvertStationCode = context.Request["ConvertStationCode"] ?? "";
                if (!string.IsNullOrWhiteSpace(m_ConvertStationCode))
                {
                    returnJsonStr = GetMagnetic_CheckPointInfoList(m_ConvertStationCode.TrimEnd(new char[] { ',' }));
                }
            }
            else if (action == "SaveMagnetic_CheckPointInfo")//保存站点信息
            {
                context.Response.ContentType = "text/plain";

                EGMNGS.Model.Magnetic_CheckPointInfo obj = new EGMNGS.Model.Magnetic_CheckPointInfo();
                obj = RM.Common.DotNetUI.ControlBindHelper.ModelBing<EGMNGS.Model.Magnetic_CheckPointInfo>(obj);

                returnJsonStr = SaveMagnetic_CheckPointInfo(obj);
            }
            else if (action == "DelMagnetic_CheckPointInfo")//删除站点信息
            {
                context.Response.ContentType = "text/plain";
                string m_OID = context.Request["OID"] ?? "";
                returnJsonStr = DelMagnetic_CheckPointInfo(m_OID);
            }
            else if (action == "GetMagnetic_CheckPointInfoChilds")//获取磁场测点采集表列表
            {
                string m_Magnetic_CheckPointInfo_OID = context.Request["Magnetic_CheckPointInfo_OID"] ?? "";
                string fdate= context.Request["fd"] ?? "";
                string tdate = context.Request["td"] ?? "";
                if (!string.IsNullOrWhiteSpace(m_Magnetic_CheckPointInfo_OID))
                {
                     returnJsonStr = GetMagnetic_CheckPointInfoChilds(m_Magnetic_CheckPointInfo_OID, fdate, tdate);
                }    
            }
            else if (action == "GetMagnetic_CheckPointInfoChildsChart")//获取磁场测点采集表图表
            {
                string m_Magnetic_CheckPointInfo_OID = context.Request["Magnetic_CheckPointInfo_OID"] ?? "";
                string p_rangeSelect = context.Request["rangeSelect"] ?? "";
                returnJsonStr = GetMagnetic_CheckPointInfoChildsChart(m_Magnetic_CheckPointInfo_OID, p_rangeSelect);

            }
            else if (action == "SaveMagnetic_CheckPointInfoChilds")//保存磁场测点采集表记录
            {
                context.Response.ContentType = "text/plain";

                EGMNGS.Model.Magnetic_CheckPointInfoChilds obj = new EGMNGS.Model.Magnetic_CheckPointInfoChilds();
                obj = RM.Common.DotNetUI.ControlBindHelper.ModelBing<EGMNGS.Model.Magnetic_CheckPointInfoChilds>(obj);

                returnJsonStr = SaveMagnetic_CheckPointInfoChilds(obj);
            }
            else if (action == "DelMagnetic_CheckPointInfoChilds")//删除磁场测点采集表记录
            {
                context.Response.ContentType = "text/plain";
                string m_OID = context.Request["OID"] ?? "";
                returnJsonStr = DelMagnetic_CheckPointInfoChilds(m_OID);
            }
            else if (action == "Get_Magnetic_CheckPointTemplatesB_List")//站点测点管理
            {
                string m_ConvertStationCode = context.Request["ConvertStationCode"] ?? "";
                returnJsonStr = Get_Magnetic_CheckPointTemplatesB_List(m_ConvertStationCode);
            }
            else if (action == "DelMagnetic_CheckPointTemplatesB")//删除站点测点业务表
            {
                context.Response.ContentType = "text/plain";
                string m_OID = context.Request["OID"] ?? "";
                if (!string.IsNullOrWhiteSpace(m_OID))
                {
                    returnJsonStr = DelMagnetic_CheckPointTemplatesB(m_OID);
                }
            }
            else if (action == "Get_Magnetic_StationCheckPointMNGChilds_List")//站点测点业务数据
            {
                string m_Magnetic_CheckPointTemplatesB_OID = context.Request["Magnetic_CheckPointTemplatesB_OID"] ?? "";
                returnJsonStr = Get_Magnetic_StationCheckPointMNGChilds_List(m_Magnetic_CheckPointTemplatesB_OID);
            }
            else if (action == "Save_Magnetic_StationCheckPointMNGChilds")//保存站点测点业务数据
            {
                context.Response.ContentType = "text/plain";

                EGMNGS.Model.Magnetic_StationCheckPointMNGChilds obj = new EGMNGS.Model.Magnetic_StationCheckPointMNGChilds();
                obj = RM.Common.DotNetUI.ControlBindHelper.ModelBing<EGMNGS.Model.Magnetic_StationCheckPointMNGChilds>(obj);

                returnJsonStr = Save_Magnetic_StationCheckPointMNGChilds(obj);
            }
            else if (action == "DelMagnetic_StationCheckPointMNGChilds")
            {
                context.Response.ContentType = "text/plain";
                string m_OID = context.Request["OID"] ?? "";
                if (!string.IsNullOrWhiteSpace(m_OID))
                {
                    returnJsonStr = DelMagnetic_StationCheckPointMNGChilds(m_OID);
                }
            }
            else if (action == "Get_Magnetic_WirelessDisturbList")//无线电干扰
            {
                string m_Magnetic_CheckPointTemplatesB_OID = context.Request["Magnetic_CheckPointTemplatesB_OID"] ?? "";
                if (!string.IsNullOrWhiteSpace(m_Magnetic_CheckPointTemplatesB_OID))
                {
                    returnJsonStr = Get_Magnetic_WirelessDisturbList(m_Magnetic_CheckPointTemplatesB_OID);
                }
            }
            else if (action == "Save_Magnetic_WirelessDisturb")//保存修改电线 电干扰
            {
                context.Response.ContentType = "text/plain";

                EGMNGS.Model.Magnetic_WirelessDisturb obj = new EGMNGS.Model.Magnetic_WirelessDisturb();
                obj = RM.Common.DotNetUI.ControlBindHelper.ModelBing<EGMNGS.Model.Magnetic_WirelessDisturb>(obj);

                returnJsonStr = Save_Magnetic_WirelessDisturb(obj);
            }
            else if (action == "Get_Magnetic_NoisePointPositionList")//噪声测点列表
            {
                string m_Magnetic_CheckPointTemplatesB_OID = context.Request["Magnetic_CheckPointTemplatesB_OID"] ?? "";
                if (!string.IsNullOrWhiteSpace(m_Magnetic_CheckPointTemplatesB_OID))
                {
                    returnJsonStr = Get_Magnetic_NoisePointPositionList(m_Magnetic_CheckPointTemplatesB_OID);
                }

            }
            else if (action == "Save_Magnetic_NoisePointPosition")//保存噪声测点
            {
                context.Response.ContentType = "text/plain";

                EGMNGS.Model.Magnetic_NoisePointPosition obj = new EGMNGS.Model.Magnetic_NoisePointPosition();
                obj = RM.Common.DotNetUI.ControlBindHelper.ModelBing<EGMNGS.Model.Magnetic_NoisePointPosition>(obj);

                returnJsonStr = Save_Magnetic_NoisePointPosition(obj);

            }
            else if (action == "DelMagnetic_NoisePointPosition")//删除噪声测点
            {
                context.Response.ContentType = "text/plain";
                string m_OID = context.Request["OID"] ?? "";
                if (!string.IsNullOrWhiteSpace(m_OID))
                {
                    returnJsonStr = DelMagnetic_NoisePointPosition(m_OID);
                }
            }
            else if (action == "Get_Magnetic_TransformerList")//变压器列表
            {
                string m_Magnetic_CheckPointTemplatesB_OID = context.Request["Magnetic_CheckPointTemplatesB_OID"] ?? "";
                if (!string.IsNullOrWhiteSpace(m_Magnetic_CheckPointTemplatesB_OID))
                {
                    returnJsonStr = Get_Magnetic_TransformerList(m_Magnetic_CheckPointTemplatesB_OID);
                }
            }
            else if (action == "Save_Magnetic_Transformer")//保存变压器
            {
                context.Response.ContentType = "text/plain";

                EGMNGS.Model.Magnetic_Transformer obj = new EGMNGS.Model.Magnetic_Transformer();
                obj = RM.Common.DotNetUI.ControlBindHelper.ModelBing<EGMNGS.Model.Magnetic_Transformer>(obj);

                returnJsonStr = Save_Magnetic_Transformer(obj);
            }
            else if (action == "Del_Magnetic_Transformer")//删除变压器
            {
                context.Response.ContentType = "text/plain";
                string m_OID = context.Request["OID"] ?? "";
                if (!string.IsNullOrWhiteSpace(m_OID))
                {
                    returnJsonStr = Del_Magnetic_Transformer(m_OID);
                }
            }
            else if (action == "Get_Magnetic_TransformerChildsList")//变压器子表
            {
                string m_Transformer_OID = context.Request["Transformer_OID"] ?? "";
                if (!string.IsNullOrWhiteSpace(m_Transformer_OID))
                {
                    returnJsonStr = Get_Magnetic_TransformerChildsList(m_Transformer_OID);
                }
            }
            else if (action == "Save_Magnetic_TransformerChilds")//保存变压器子表
            {
                context.Response.ContentType = "text/plain";

                EGMNGS.Model.Magnetic_TransformerChilds obj = new EGMNGS.Model.Magnetic_TransformerChilds();
                obj = RM.Common.DotNetUI.ControlBindHelper.ModelBing<EGMNGS.Model.Magnetic_TransformerChilds>(obj);

                returnJsonStr = Save_Magnetic_TransformerChilds(obj);
            }
            else if (action == "Del_Magnetic_TransformerChilds")//删除变压器子表
            {
                context.Response.ContentType = "text/plain";
                string m_OID = context.Request["OID"] ?? "";
                if (!string.IsNullOrWhiteSpace(m_OID))
                {
                    returnJsonStr = Del_Magnetic_TransformerChilds(m_OID);
                }
            }
            else if (action == "GetDataShowToMap")
            {
                context.Response.ContentType = "text/plain";
                returnJsonStr = context.Request["callback"] + "(" + GetDataShowToMap() + ");";
            }
            else if (action == "MonitorDBPageList")
            {

                string fromDate = context.Request["fromdate"] ?? "";
                string toDate = context.Request["todate"] ?? "";

                string m_ConvertStationName = context.Request["ConvertStationName"] ?? "";
                string m_CollectionTypeName = context.Request["CollectionTypeName"] ?? "";
                string m_CheckPointName = context.Request["CheckPointName"] ?? "";

                returnJsonStr = MonitorDBPageList(fromDate, toDate, m_ConvertStationName, m_CollectionTypeName, m_CheckPointName);
            }
            else if (action == "SCPMNGExportXls")//工频电磁场强度导出
            {
                string magnetic_CheckPointTemplatesB_OID = context.Request["Magnetic_CheckPointTemplatesB_OID"] ?? "";
                SCPMNGExportXls(magnetic_CheckPointTemplatesB_OID, context.Response);
            }
            else if (action == "Expert_Magnetic_WirelessDisturbListXls")//无线电导出
            {
                string magnetic_CheckPointTemplatesB_OID = context.Request["Magnetic_CheckPointTemplatesB_OID"] ?? "";
                Expert_Magnetic_WirelessDisturbListXls(magnetic_CheckPointTemplatesB_OID, context.Response);
            }
            else if (action == "Del_Magnetic_WirelessDisturb")//删除无线电
            {
                context.Response.ContentType = "text/plain";
                string m_OID = context.Request["OID"] ?? "";
                if (!string.IsNullOrWhiteSpace(m_OID))
                {
                    returnJsonStr = Del_Magnetic_WirelessDisturb(m_OID);
                }
            }
            else if (action == "Expert_Magnetic_NoisePointPositionListXls")//噪声导出
            {
                string magnetic_CheckPointTemplatesB_OID = context.Request["Magnetic_CheckPointTemplatesB_OID"] ?? "";
                Expert_Magnetic_NoisePointPositionListXls(magnetic_CheckPointTemplatesB_OID, context.Response);
            }
            else if (action == "GetMagnetic_CheckPointInfoChildsChart2")//数据分析诊断图表
            {
                context.Response.ContentType = "application/json";
                string fromDate = context.Request["fromdate"] ?? "";
                string toDate = context.Request["todate"] ?? "";
                string checkPoint = context.Request["checkPoint"] ?? "";
                string target = context.Request["Target"] ?? "";
                returnJsonStr= GetMagnetic_CheckPointInfoChildsChart2(fromDate, toDate, checkPoint, target);

            }
            else if (action=="GetConvertStationNameList")//获取当前用户所在供电局的变电站
            {

                string organization_ID = context.Request["Organization_ID"] ?? "";
                returnJsonStr = Newtonsoft.Json.JsonConvert.SerializeObject(GetConvertStationNameList(organization_ID));
            }
            else if (action== "GetPowerSupplyNameList")//获取供电局
            {
                returnJsonStr = Newtonsoft.Json.JsonConvert.SerializeObject(GetPowerSupplyNameList());

            }
            context.Response.Write(returnJsonStr);
            context.Response.Flush();
        }

        private DataTable GetPowerSupplyNameList()
        {
            DataTable dtPowerStation = ComServies.GetAllPowerStation();
            return dtPowerStation;
        }

        private DataTable GetConvertStationNameList(string m_PowerSupplyCode)
        {
            EGMNGS.BLL.InfoPowerSupplyConvertStation bll = new EGMNGS.BLL.InfoPowerSupplyConvertStation();
            DataSet dsInfoPowerSupplyConvertStation = bll.GetList(string.Format("PowerSupplyCode='{0}'", m_PowerSupplyCode));
            return dsInfoPowerSupplyConvertStation.Tables[0];
        }

        private string GetMagnetic_CheckPointInfoChildsChart2(string fromDate, string toDate, string checkPoint, string target)
        {
            DateTime dtfrom = DateTime.Parse(fromDate);
            DateTime dtTo = DateTime.Parse(toDate);
            var cparr = checkPoint.Split(',');
            string tempcheckPoint = string.Empty;
            tempcheckPoint = cparr.Aggregate("", (current, i) => current + ("'" + i + "',")).Trim(',');

            string sqlwhere = string.Format("m.CheckPointCode in ({2}) and CONVERT(varchar(100), CollectionDatetime, 23) >='{0}' and CONVERT(varchar(100), CollectionDatetime, 23)<='{1}' AND mc.Magnetic_CheckPointInfo_OID=m.OID order by CollectionDatetime", dtfrom.ToString("yyyy-MM-dd"), dtTo.ToString("yyyy-MM-dd"), tempcheckPoint);

            var returnValue = new { listDatetime = new List<string>(), listChartData = new List<chartData>() };


            DataTable dt = EGMNGS.Common.ComServies.Query(@"select mc.*,m.CheckPointCode
  from Magnetic_CheckPointInfoChilds mc,Magnetic_CheckPointInfo m  where " + sqlwhere);//comsev .GetList(sqlwhere).Tables[0];


            List<chartData> listChartData = new List<chartData>();
            List<string> listDatetime = new List<string>();

            foreach (var itemtarget in target.Split(','))
            {
                foreach (var itemcheck in checkPoint.Split(','))
                {
                    listChartData.Add(new chartData() { target = itemtarget, ChackName = itemcheck });
                }

            }

            foreach (DataRow item in dt.Rows)
            {
                listDatetime.Add(item["CollectionDatetime"].ToString());

                foreach (var itemChartData in listChartData)
                {
                    if (itemChartData.target == "ElectricFieldMAX" && itemChartData.ChackName == item["CheckPointCode"].ToString())
                    {
                        itemChartData.data.Add(item["ElectricFieldMAX"].AsTargetType<decimal>(0));
                    }
                    else if (itemChartData.target == "ElectricFieldMIN" && itemChartData.ChackName == item["CheckPointCode"].ToString())
                    {
                        itemChartData.data.Add(item["ElectricFieldMIN"].AsTargetType<decimal>(0));
                    }
                    else if (itemChartData.target == "ElectricFieldRMS" && itemChartData.ChackName == item["CheckPointCode"].ToString())
                    {
                        itemChartData.data.Add(item["ElectricFieldRMS"].AsTargetType<decimal>(0));
                    }
                    else if (itemChartData.target == "MagneticInductionMAX" && itemChartData.ChackName == item["CheckPointCode"].ToString())
                    {
                        itemChartData.data.Add(item["MagneticInductionMAX"].AsTargetType<decimal>(0));
                    }
                    else if (itemChartData.target == "MagneticInductionMIN" && itemChartData.ChackName == item["CheckPointCode"].ToString())
                    {
                        itemChartData.data.Add(item["MagneticInductionMIN"].AsTargetType<decimal>(0));
                    }
                    else if (itemChartData.target == "MagneticInductionRMS" && itemChartData.ChackName == item["CheckPointCode"].ToString())
                    {
                        itemChartData.data.Add(item["MagneticInductionRMS"].AsTargetType<decimal>(0));
                    }
                    else if (itemChartData.target == "Temperature" && itemChartData.ChackName == item["CheckPointCode"].ToString())
                    {
                        itemChartData.data.Add(item["Temperature"].AsTargetType<decimal>(0));
                    }
                    else if (itemChartData.target == "Humidity" && itemChartData.ChackName == item["CheckPointCode"].ToString())
                    {
                        itemChartData.data.Add(item["Humidity"].AsTargetType<decimal>(0));
                    }

                }
            }

            foreach (var item in listChartData)
            {
                if (item.target == "ElectricFieldMAX")
                {
                    item.target = "电场强度MAX";
                }
                else if (item.target == "ElectricFieldMIN")
                {
                    item.target = "电场强度MIN";
                }
                else if (item.target == "ElectricFieldRMS")
                {
                    item.target = "电场强度RMS";
                }
                else if (item.target == "MagneticInductionMAX")
                {
                    item.target = "磁感应强度MAX";
                }
                else if (item.target == "MagneticInductionMIN")
                {
                    item.target = "磁感应强度MIN";
                }
                else if (item.target == "MagneticInductionRMS")
                {
                    item.target = "磁感应强度RMS";
                }
                else if (item.target == "Temperature")
                {
                    item.target = "温度";
                }
                else if (item.target == "Humidity")
                {
                    item.target = "湿度";
                }

            }

            var chartDataSource = new { listDatetime = listDatetime, listChartData = listChartData };

            return Newtonsoft.Json.JsonConvert.SerializeObject(chartDataSource);
        }

        private string Del_Magnetic_WirelessDisturb(string m_OID)
        {
            Repository m_Repository = new Repository();
            bool success = m_Repository.Delete<EGMNGS.Model.Magnetic_WirelessDisturb>(Convert.ToInt32(m_OID));
            if (success)
            {
                return "删除成功";
            }
            else
            {
                return "删除失败";
            }
        }

        /// <summary>
        /// 噪声导出
        /// </summary>
        /// <param name="magnetic_CheckPointTemplatesB_OID"></param>
        /// <param name="httpResponse"></param>
        private void Expert_Magnetic_NoisePointPositionListXls(string magnetic_CheckPointTemplatesB_OID, HttpResponse httpResponse)
        {
            Repository m_Repository = new Repository();
            Magnetic_CheckPointTemplatesB obj = m_Repository.GetById<Magnetic_CheckPointTemplatesB>(magnetic_CheckPointTemplatesB_OID);


            string sqlDYLev = string.Format(@" select code_chi_desc 
from sys_code
where code_type = 'DYLev' and code = (select top 1 voltagelevel
    from dbo.Magnetic_CheckPointTemplatesB a, Magnetic_CheckPointTemplates b
     where  b.oid = a.Magnetic_CheckPointTemplates_oid and a.oid = {0})", magnetic_CheckPointTemplatesB_OID);


            string converStationNameSQL = string.Format(@"select ConvartStationName from dbo.InfoPowerSupplyConvertStation 
     where covnertstationcode='{0}'", obj.ConvertStationCode);

            string converStationName = m_Repository.ExecuteScalar(SessionFactory.CreateConnection(), converStationNameSQL).ToString();

            string dylev = m_Repository.ExecuteScalar(SessionFactory.CreateConnection(), sqlDYLev).AsTargetType<string>("");




            string sqlMagnetic_NoisePointPosition = "select * from Magnetic_NoisePointPosition where Magnetic_CheckPointTemplatesB_OID=" + magnetic_CheckPointTemplatesB_OID.ToString() + "order by oid";
            DataTable tbMagnetic_NoisePointPosition = EGMNGS.Common.ComServies.Query(sqlMagnetic_NoisePointPosition).Copy();
            tbMagnetic_NoisePointPosition.TableName = "tablename";


            string sqlMagnetic_WirelessDisturb = string.Format(@"select mt.*,mtf.TransformerCode,mtf.TransformerType,mtf.Manufacturer from Magnetic_TransformerChilds mt
                          left join Magnetic_Transformer mtf on mtf.oid=mt.Transformer_OID
                          where mtf.Magnetic_CheckPointTemplatesB_OID={0} 
                           order by mtf.Magnetic_CheckPointTemplatesB_OID", magnetic_CheckPointTemplatesB_OID.ToString());

            DataTable Magnetic_WirelessDisturbDatatable = EGMNGS.Common.ComServies.Query(sqlMagnetic_WirelessDisturb).Copy();
            Magnetic_WirelessDisturbDatatable.TableName = "Magnetic_TransformerChilds";


            WorkbookDesigner wd = new WorkbookDesigner();
            wd.Open(HttpContext.Current.Server.MapPath("~/TemplateFiles/噪声-导出模板.xlsx"));
            wd.SetDataSource("DYLev", dylev);
            wd.SetDataSource("ConvertStationName", converStationName);
            wd.SetDataSource("TemplateType", obj.TemplateType);
            wd.SetDataSource("MeasuringDate", obj.MeasuringDate3);
            wd.SetDataSource("MeasuringTime", obj.MeasuringTime3);
            wd.SetDataSource("Temperature", obj.Temperature3);
           // wd.SetDataSource("voltage", obj.voltage);
            wd.SetDataSource("BARO", obj.BARO3);
           // wd.SetDataSource("Electricity", obj.Electricity);
            wd.SetDataSource("Humidity", obj.Humidity3);

            wd.SetDataSource(tbMagnetic_NoisePointPosition);
            // wd.SetDataSource(dtMagnetic_Transformer);
            wd.SetDataSource(Magnetic_WirelessDisturbDatatable);

            wd.Process();
            wd.Save(string.Format("噪声.xls"), SaveType.OpenInExcel, FileFormatType.Excel2003, httpResponse);
            httpResponse.Flush();
            httpResponse.Close();
            httpResponse.End();
        }

        /// <summary>
        /// 无线电导出
        /// </summary>
        /// <param name="magnetic_CheckPointTemplatesB_OID"></param>
        /// <param name="httpResponse"></param>
        private void Expert_Magnetic_WirelessDisturbListXls(string magnetic_CheckPointTemplatesB_OID, HttpResponse httpResponse)
        {
            Repository m_Repository = new Repository();
            Magnetic_CheckPointTemplatesB obj = m_Repository.GetById<Magnetic_CheckPointTemplatesB>(magnetic_CheckPointTemplatesB_OID);

            string sqlDYLev = string.Format(@" select code_chi_desc 
from sys_code
where code_type = 'DYLev' and code = (select top 1 voltagelevel
    from dbo.Magnetic_CheckPointTemplatesB a, Magnetic_CheckPointTemplates b
     where  b.oid = a.Magnetic_CheckPointTemplates_oid and a.oid = {0})", magnetic_CheckPointTemplatesB_OID);


            string converStationNameSQL = string.Format(@"select ConvartStationName from dbo.InfoPowerSupplyConvertStation 
     where covnertstationcode='{0}'", obj.ConvertStationCode);

            string converStationName = m_Repository.ExecuteScalar(SessionFactory.CreateConnection(), converStationNameSQL).ToString();

            string dylev = m_Repository.ExecuteScalar(SessionFactory.CreateConnection(), sqlDYLev).AsTargetType<string>("");



            string sql = @"select [OID]
      ,[Magnetic_CheckPointTemplatesB_OID]
      ,CAST([Distance] as varchar(10))+'米' as Distance
      ,[W0p15]
      ,[W0p25]
      ,[Wp5]
      ,[W1]
      ,[W1p5]
      ,[W3]
      ,[W6]
      ,[W10]
      ,[W15]
      ,[W30]
      ,[CREATED_BY]
      ,[CREATED_DATE]
      ,[LAST_UPD_BY]
      ,[LAST_UPD_DATE]  from Magnetic_WirelessDisturb where Magnetic_CheckPointTemplatesB_OID=" + magnetic_CheckPointTemplatesB_OID.ToString();


            DataTable tempDatatable = EGMNGS.Common.ComServies.Query(sql).Copy();

            tempDatatable.TableName = "tablename";

            WorkbookDesigner wd = new WorkbookDesigner();
            wd.Open(HttpContext.Current.Server.MapPath("~/TemplateFiles/无线电干扰-模板.xlsx"));
            wd.SetDataSource("DYLev", dylev);
            wd.SetDataSource("ConvertStationName", converStationName);
            wd.SetDataSource("TemplateType", obj.TemplateType);
            wd.SetDataSource("MeasuringDate", obj.MeasuringDate2);
            wd.SetDataSource("MeasuringTime", obj.MeasuringTime2);
            wd.SetDataSource("Temperature", obj.Temperature2);
           // wd.SetDataSource("voltage", obj.voltage);
            wd.SetDataSource("BARO", obj.BARO2);
           // wd.SetDataSource("Electricity", obj.Electricity);
            wd.SetDataSource("Humidity", obj.Humidity2);
            wd.SetDataSource("WindDirection", obj.WindDirection);
            wd.SetDataSource("WindSpeed", obj.WindSpeed);
            wd.SetDataSource(tempDatatable);
            wd.Process();
            wd.Save(string.Format("无线电干扰.xls"), SaveType.OpenInExcel, FileFormatType.Excel2003, httpResponse);
            httpResponse.Flush();
            httpResponse.Close();
            httpResponse.End();
        }
        /// <summary>
        /// 工频电磁场强度导出
        /// </summary>
        /// <param name="magnetic_CheckPointTemplatesB_OID"></param>
        /// <param name="httpResponse"></param>
        private void SCPMNGExportXls(string magnetic_CheckPointTemplatesB_OID, HttpResponse httpResponse)
        {

            Repository m_Repository = new Repository();
            Magnetic_CheckPointTemplatesB obj = m_Repository.GetById<Magnetic_CheckPointTemplatesB>(magnetic_CheckPointTemplatesB_OID);

            string sqlDYLev = string.Format(@" select code_chi_desc 
from sys_code
where code_type = 'DYLev' and code = (select top 1 voltagelevel
    from dbo.Magnetic_CheckPointTemplatesB a, Magnetic_CheckPointTemplates b
     where  b.oid = a.Magnetic_CheckPointTemplates_oid and a.oid = {0})", magnetic_CheckPointTemplatesB_OID);


            string converStationNameSQL = string.Format(@"select ConvartStationName from dbo.InfoPowerSupplyConvertStation 
     where covnertstationcode='{0}'", obj.ConvertStationCode);

            string converStationName = m_Repository.ExecuteScalar(SessionFactory.CreateConnection(), converStationNameSQL).ToString();

            string dylev = m_Repository.ExecuteScalar(SessionFactory.CreateConnection(), sqlDYLev).AsTargetType<string>("");


            string sql = "select * from Magnetic_StationCheckPointMNGChilds where Magnetic_CheckPointTemplatesB_OID=" + magnetic_CheckPointTemplatesB_OID.ToString();


          
       
             DataTable tempDatatable = EGMNGS.Common.ComServies.Query(sql).Copy();

            tempDatatable.TableName = "tablename";

            WorkbookDesigner wd = new WorkbookDesigner();
            wd.Open(HttpContext.Current.Server.MapPath("~/TemplateFiles/工频电磁场强度-模板.xlsx"));
            wd.SetDataSource("DYLev", dylev);
            wd.SetDataSource("ConvertStationName", converStationName); 
            wd.SetDataSource("TemplateType", obj.TemplateType);
            wd.SetDataSource("MeasuringDate", obj.MeasuringDate);
            wd.SetDataSource("MeasuringTime", obj.MeasuringTime);
            wd.SetDataSource("Temperature", obj.Temperature);
          //  wd.SetDataSource("voltage", obj.voltage);
            wd.SetDataSource("BARO", obj.BARO);
          //  wd.SetDataSource("Electricity", obj.Electricity);
            wd.SetDataSource("Humidity", obj.Humidity);
            wd.SetDataSource(tempDatatable);
            wd.Process();
            wd.Save(string.Format("工频电磁场强度.xls"), SaveType.OpenInExcel, FileFormatType.Excel2003, httpResponse);
            httpResponse.Flush();
            httpResponse.Close();
            httpResponse.End();


        }

        private string MonitorDBPageList(string fromDate, string toDate, string p_ConvertStationName,string p_CollectionTypeName,string p_CheckPointName)
        {
            string results = string.Empty;
            string wheresql = string.Empty;

            string sql = @"SELECT {0} mpc.*,
mcp.ConvertStationName,
mcp.CheckPointName,
mcp.CheckPointCode
FROM [EGMNGS].[dbo].[Magnetic_CheckPointInfoChilds] mpc
LEFT JOIN Magnetic_CheckPointInfo mcp ON mpc.Magnetic_CheckPointInfo_OID=mcp.OID
where 1=1";

            if (!string.IsNullOrWhiteSpace(fromDate) && !string.IsNullOrWhiteSpace(toDate))
            {
                DateTime dtfrom = DateTime.Parse(fromDate);
                DateTime dtTo = DateTime.Parse(toDate);

                wheresql += string.Format(" and CONVERT(varchar(100), CollectionDatetime, 23) >='{0}' and CONVERT(varchar(100), CollectionDatetime, 23)<='{1}'", dtfrom.ToString("yyyy-MM-dd"), dtTo.ToString("yyyy-MM-dd"));
                sql = string.Format(sql,"");
            }
            else
            {
                sql = string.Format(sql,"top 100");
            }

            if (!string.IsNullOrWhiteSpace(p_ConvertStationName))
            {
                wheresql += string.Format(" and ConvertStationName='{0}'", p_ConvertStationName);
            }

            if (!string.IsNullOrWhiteSpace(p_CollectionTypeName))
            {
                wheresql += string.Format(" and CollectionType='{0}'", p_CollectionTypeName);
            }

            if (!string.IsNullOrWhiteSpace(p_CheckPointName))
            {
                wheresql += string.Format(" and CheckPointName like '%{0}%'", p_CheckPointName);
            }

            wheresql += " order by CollectionDatetime desc";

            DataTable tempDatatable = EGMNGS.Common.ComServies.Query(sql + wheresql).Copy();
            results = Newtonsoft.Json.JsonConvert.SerializeObject(tempDatatable);

            return results;
        }

        private string GetDataShowToMap()
        {
            string sqltemp = string.Format(@"  with t as
            (
            SELECT 	Magnetic_CheckPointInfo_OID,MAX(CollectionDatetime) as CollectionDatetime 
            FROM Magnetic_CheckPointInfoChilds 
            GROUP BY Magnetic_CheckPointInfo_OID
            ),t1 as
            (
            select oid from Magnetic_CheckPointInfoChilds a
            inner join t on t.Magnetic_CheckPointInfo_OID=a.Magnetic_CheckPointInfo_OID 
            and a.CollectionDatetime=t.CollectionDatetime
            )
    
               SELECT mcpic.*,mcpi.OID as Magnetic_CheckPointInfo_ID,mcpi.Longitude,mcpi.Latitude,mcpi.ConvertStationName,mcpi.CheckPointName   FROM Magnetic_CheckPointInfoChilds mcpic
            LEFT JOIN Magnetic_CheckPointInfo mcpi ON mcpi.OID=mcpic.Magnetic_CheckPointInfo_OID
       inner join t1 on t1.oid=mcpic.oid
          
            WHERE 1=1");

            DataTable tempDt = EGMNGS.Common.ComServies.Query(sqltemp).Copy();
            return Newtonsoft.Json.JsonConvert.SerializeObject(tempDt);
        }

        private string DelMagnetic_NoisePointPosition(string m_OID)
        {
            Repository m_Repository = new Repository();
            bool success = m_Repository.Delete<EGMNGS.Model.Magnetic_NoisePointPosition>(Convert.ToInt32(m_OID));
            if (success)
            {
                return "删除成功";
            }
            else
            {
                return "删除失败";
            }
        }

        private string Del_Magnetic_TransformerChilds(string m_OID)
        {
            Repository m_Repository = new Repository();
            bool success = m_Repository.Delete<EGMNGS.Model.Magnetic_TransformerChilds>(Convert.ToInt32(m_OID));
            if (success)
            {
                return "删除成功";
            }
            else
            {
                return "删除失败";
            }
        }

        private string Save_Magnetic_TransformerChilds(Magnetic_TransformerChilds obj)
        {
            string returnValue = string.Empty;

            Repository m_Repository = new Repository();
            try
            {
                if (obj.OID == 0)
                {
                    m_Repository.Insert<Magnetic_TransformerChilds>(obj);
                }
                else
                {
                    m_Repository.Update<Magnetic_TransformerChilds>(obj);
                }
                returnValue = "保存成功";
            }
            catch (Exception ex)
            {
                returnValue = "保存失败";
                throw ex;
            }

            return returnValue;
        }

        private string Get_Magnetic_TransformerChildsList(string m_Transformer_OID)
        {
            Repository repoistory = new Repository();
            var list = repoistory.GetByName<Magnetic_TransformerChilds>("Transformer_OID", m_Transformer_OID);
            if (list.Count == 0)
            {
                string sqlinsert = string.Format(@"INSERT INTO [EGMNGS].[dbo].[Magnetic_TransformerChilds]
                 ([Transformer_OID]
           ,[CheckPointPosition]
           ,[CREATED_BY]
           ,[CREATED_DATE]
         )
  select {0},'#101','System',GETDATE()
 union
 select {0},'#102','System',GETDATE()
union
 select {0},'#103','System',GETDATE()
union
 select {0},'#104','System',GETDATE()
union
 select {0},'#105','System',GETDATE()
union
 select {0},'#106','System',GETDATE()
union
 select {0},'#107','System',GETDATE()
union
  select {0},'#108','System',GETDATE()
 union
 select {0},'#109','System',GETDATE()
union
 select {0},'#110','System',GETDATE()
union
 select {0},'#111','System',GETDATE()
union
 select {0},'#112','System',GETDATE()
union
  select {0},'#201','System',GETDATE()
 union
 select {0},'#202','System',GETDATE()
union
 select {0},'#203','System',GETDATE()
union
 select {0},'#204','System',GETDATE()
union
 select {0},'#205','System',GETDATE()
union
 select {0},'#206','System',GETDATE()
union
 select {0},'#207','System',GETDATE()
union
  select {0},'#208','System',GETDATE()
 union
 select {0},'#209','System',GETDATE()
union
 select {0},'#210','System',GETDATE()
union
 select {0},'#211','System',GETDATE()
union
 select {0},'#212','System',GETDATE();
", m_Transformer_OID);
                repoistory.Execute(SessionFactory.CreateConnection(), sqlinsert);
            }

            list = repoistory.GetByName<Magnetic_TransformerChilds>("Transformer_OID", m_Transformer_OID);

            return Newtonsoft.Json.JsonConvert.SerializeObject(list);
        }

        private string Del_Magnetic_Transformer(string m_OID)
        {
            Repository m_Repository = new Repository();
            bool success = m_Repository.Delete<EGMNGS.Model.Magnetic_Transformer>(Convert.ToInt32(m_OID));
            if (success)
            {
                return "删除成功";
            }
            else
            {
                return "删除失败";
            }
        }

        private string Save_Magnetic_Transformer(Magnetic_Transformer obj)
        {
            string returnValue = string.Empty;

            Repository m_Repository = new Repository();
            try
            {
                if (obj.OID == 0)
                {
                    m_Repository.Insert<Magnetic_Transformer>(obj);
                }
                else
                {
                    m_Repository.Update<Magnetic_Transformer>(obj);
                }
                returnValue = "保存成功";
            }
            catch (Exception ex)
            {
                returnValue = "保存失败";
                throw ex;
            }

            return returnValue;
        }

        private string Get_Magnetic_TransformerList(string m_Magnetic_CheckPointTemplatesB_OID)
        {
            Repository repoistory = new Repository();
            var list = repoistory.GetByName<Magnetic_Transformer>("Magnetic_CheckPointTemplatesB_OID", m_Magnetic_CheckPointTemplatesB_OID);
            return Newtonsoft.Json.JsonConvert.SerializeObject(list.OrderByDescending(o => o.OID));
        }

        private string Save_Magnetic_NoisePointPosition(Magnetic_NoisePointPosition obj)
        {
            string returnValue = string.Empty;

            Repository m_Repository = new Repository();
            try
            {
                if (obj.OID == 0)
                {
                    m_Repository.Insert<Magnetic_NoisePointPosition>(obj);
                }
                else
                {
                    m_Repository.Update<Magnetic_NoisePointPosition>(obj);
                }
                returnValue = "保存成功";
            }
            catch (Exception ex)
            {
                returnValue = "保存失败";
                throw ex;
            }

            return returnValue;
        }

        private string DelMagnetic_StationCheckPointMNGChilds(string m_OID)
        {
            Repository m_Repository = new Repository();
            bool success = m_Repository.Delete<EGMNGS.Model.Magnetic_StationCheckPointMNGChilds>(Convert.ToInt32(m_OID));
            if (success)
            {
                return "删除成功";
            }
            else
            {
                return "删除失败";
            }
        }

        /// <summary>
        /// 噪声测点列表
        /// </summary>
        /// <param name="m_Magnetic_CheckPointTemplatesB_OID"></param>
        /// <returns></returns>
        private string Get_Magnetic_NoisePointPositionList(string m_Magnetic_CheckPointTemplatesB_OID)
        {
            Repository repoistory = new Repository();
            var list = repoistory.GetByName<Magnetic_NoisePointPosition>("Magnetic_CheckPointTemplatesB_OID", m_Magnetic_CheckPointTemplatesB_OID);
            if (list.Count == 0)
            {
                string sqlinsert = string.Format(@"INSERT INTO [EGMNGS].[dbo].[Magnetic_NoisePointPosition]
           ([Magnetic_CheckPointTemplatesB_OID]
           ,[CheckPointPosition]
           ,[CheckPointPositionCode]
           ,[CREATED_BY]
           ,[CREATED_DATE]
         )
  select {0},'变电站门口','#1','System',GETDATE()
 union
 select {0},'主控楼','#2','System',GETDATE()
union
 select {0},'围墙','#3','System',GETDATE()
union
 select {0},'围墙','#4','System',GETDATE()
union
 select {0},'围墙','#5','System',GETDATE()
union
 select {0},'围墙','#6','System',GETDATE()
union
 select {0},'围墙','#7','System',GETDATE();
", m_Magnetic_CheckPointTemplatesB_OID);
                repoistory.Execute(SessionFactory.CreateConnection(), sqlinsert);
            }

            list = repoistory.GetByName<Magnetic_NoisePointPosition>("Magnetic_CheckPointTemplatesB_OID", m_Magnetic_CheckPointTemplatesB_OID);

            return Newtonsoft.Json.JsonConvert.SerializeObject(list.OrderBy(o => o.CheckPointPositionCode));
        }

        /// <summary>
        /// 保存修改电线 电干扰
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        private string Save_Magnetic_WirelessDisturb(Magnetic_WirelessDisturb obj)
        {
            string returnValue = string.Empty;

            Repository m_Repository = new Repository();
            try
            {
                if (obj.OID == 0)
                {
                    m_Repository.Insert<Magnetic_WirelessDisturb>(obj);
                }
                else
                {
                    m_Repository.Update<Magnetic_WirelessDisturb>(obj);
                }
                returnValue = "保存成功";
            }
            catch (Exception ex)
            {
                returnValue = "保存失败";
                throw ex;
            }

            return returnValue;
        }

        /// <summary>
        /// 无线电干扰
        /// </summary>
        /// <returns></returns>
        private string Get_Magnetic_WirelessDisturbList(string m_Magnetic_CheckPointTemplatesB_OID)
        {
            Repository repoistory = new Repository();
            var list = repoistory.GetByName<Magnetic_WirelessDisturb>("Magnetic_CheckPointTemplatesB_OID", m_Magnetic_CheckPointTemplatesB_OID);
            if (list.Count == 0)
            {
                string sqlinsert = string.Format(@"INSERT INTO [Magnetic_WirelessDisturb]
           ([Magnetic_CheckPointTemplatesB_OID]
           ,[Distance]
           ,[CREATED_BY]
           ,[CREATED_DATE]
         )
                select {0},5,'System',GETDATE()
                union
                select {0},10,'System',GETDATE()
                union
                select {0},20,'System',GETDATE()
                union
                select {0},50,'System',GETDATE()
                union
                select {0},100,'System',GETDATE()
                union
                select {0},500,'System',GETDATE()
                union
                select {0},1000,'System',GETDATE()
                union
                select {0},2000,'System',GETDATE();", m_Magnetic_CheckPointTemplatesB_OID);
                repoistory.Execute(SessionFactory.CreateConnection(), sqlinsert);
            }

            list = repoistory.GetByName<Magnetic_WirelessDisturb>("Magnetic_CheckPointTemplatesB_OID", m_Magnetic_CheckPointTemplatesB_OID);

            return Newtonsoft.Json.JsonConvert.SerializeObject(list);
        }

        private string Save_Magnetic_StationCheckPointMNGChilds(Magnetic_StationCheckPointMNGChilds obj)
        {
            string returnValue = string.Empty;

            Repository m_Repository = new Repository();
            try
            {
                if (obj.OID == 0)
                {
                    m_Repository.Insert<Magnetic_StationCheckPointMNGChilds>(obj);
                }
                else
                {
                    m_Repository.Update<Magnetic_StationCheckPointMNGChilds>(obj);
                }
                returnValue = "保存成功";
            }
            catch (Exception ex)
            {
                returnValue = "保存失败";
                throw ex;
            }

            return returnValue;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="m_Magnetic_CheckPointTemplatesB_OID"></param>
        /// <returns></returns>
        private string Get_Magnetic_StationCheckPointMNGChilds_List(string m_Magnetic_CheckPointTemplatesB_OID)
        {
            Repository repoistory = new Repository();
            var list = repoistory.GetByName<Magnetic_StationCheckPointMNGChilds>("Magnetic_CheckPointTemplatesB_OID", m_Magnetic_CheckPointTemplatesB_OID);

            return Newtonsoft.Json.JsonConvert.SerializeObject(list);
        }

        private string DelMagnetic_CheckPointTemplatesB(string m_OID)
        {
            Repository m_Repository = new Repository();
            bool success = m_Repository.Delete<EGMNGS.Model.Magnetic_CheckPointTemplatesB>(Convert.ToInt32(m_OID));
            if (success)
            {
                return "删除成功";
            }
            else
            {
                return "删除失败";
            }
        }

        /// <summary>
        ///  站点测点管理
        /// </summary>
        /// <param name="m_ConvertStationCode">站点编码</param>
        /// <returns></returns>
        private string Get_Magnetic_CheckPointTemplatesB_List(string m_ConvertStationCode)
        {
            Repository repoistory = new Repository();
            var list = repoistory.GetByName<Magnetic_CheckPointTemplatesB>("ConvertStationCode", m_ConvertStationCode);
            return Newtonsoft.Json.JsonConvert.SerializeObject(list);
        }

        private string GetMagnetic_CheckPointInfoChildsChart(string m_Magnetic_CheckPointInfo_OID, string p_rangeSelect)
        {
            string resultsJsonStr = string.Empty;

            var rangedate = p_rangeSelect;
            DateTime dtfrom = DateTime.Now.Subtract(new TimeSpan(rangedate.AsTargetType<int>(10), 0, 0, 0));
            //  List<column> listColumn = GetColumns(p_OCInstrumentInfo_OID);
            List<chartData> listChartData = new List<chartData>();
            List<string> listDatetime = new List<string>();


            listChartData.Add(new chartData { name = "ElectricFieldMAX" });
            listChartData.Add(new chartData { name = "ElectricFieldMIN" });
            listChartData.Add(new chartData { name = "ElectricFieldRMS" });
            listChartData.Add(new chartData { name = "MagneticInductionMAX" });
            listChartData.Add(new chartData { name = "MagneticInductionMIN" });
            listChartData.Add(new chartData { name = "MagneticInductionRMS" });
            listChartData.Add(new chartData { name = "Temperature" });
            listChartData.Add(new chartData { name = "Humidity" });

            string sqlwhere = string.Format(@"
SELECT [CollectionDatetime]
      ,[ElectricFieldMAX]
      ,[ElectricFieldMIN]
      ,[ElectricFieldRMS]
      ,[MagneticInductionMAX]
      ,[MagneticInductionMIN]
      ,[MagneticInductionRMS]
      ,[Temperature]
      ,[Humidity]
                                                      from [EGMNGS].[dbo].Magnetic_CheckPointInfoChilds where 
Magnetic_CheckPointInfo_OID = {2} and CONVERT(varchar(100), CollectionDatetime, 23) >='{0}' and CONVERT(varchar(100), CollectionDatetime, 23)<='{1}' order by CollectionDatetime desc", dtfrom.ToString("yyyy-MM-dd"), DateTime.Now.ToString("yyyy-MM-dd"), m_Magnetic_CheckPointInfo_OID);

            DataTable dtdata = EGMNGS.Common.ComServies.Query(sqlwhere);

            foreach (DataRow item in dtdata.Rows)
            {
                listDatetime.Add(item["CollectionDatetime"].ToString());
            }

            foreach (chartData itemChartData in listChartData)
            {
                foreach (DataRow item in dtdata.Rows)
                {
                    itemChartData.data.Add(item[itemChartData.name].AsTargetType<decimal>(0));
                }
            }


            resultsJsonStr = Newtonsoft.Json.JsonConvert.SerializeObject(new { listDatetime = listDatetime, listChartData = listChartData });
            return resultsJsonStr;
        }


     
        private string DelMagnetic_CheckPointInfoChilds(string m_OID)
        {
            Repository m_Repository = new Repository();
            bool success = m_Repository.Delete<EGMNGS.Model.Magnetic_CheckPointInfoChilds>(m_OID);
            if (success)
            {
                return "删除成功";
            }
            else
            {
                return "删除失败";
            }
        }

        private string SaveMagnetic_CheckPointInfoChilds(Magnetic_CheckPointInfoChilds obj)
        {
            string returnValue = string.Empty;

            Repository m_Repository = new Repository();
            try
            {
                if (obj.OID == 0)
                {
                    m_Repository.Insert<Magnetic_CheckPointInfoChilds>(obj);
                }
                else
                {
                    m_Repository.Update<Magnetic_CheckPointInfoChilds>(obj);
                }
                returnValue = "保存成功";
            }
            catch (Exception ex)
            {
                returnValue = "保存失败";
                throw ex;
            }

            return returnValue;
        }

        private string GetMagnetic_CheckPointInfoChilds(string m_Magnetic_CheckPointInfo_OID,string fdate,string tdate)
        {
            string sql = string.Format(@"SELECT mcpi.CheckPointName,mcpi.ConvertStationName,mci.*
  FROM [EGMNGS].[dbo].[Magnetic_CheckPointInfoChilds] mci
  LEFT JOIN Magnetic_CheckPointInfo mcpi ON mcpi.OID = mci.Magnetic_CheckPointInfo_OID
where ");

            string wheresql = string.Empty;

            if (m_Magnetic_CheckPointInfo_OID.Contains(","))
            {
                wheresql = string.Format("Magnetic_CheckPointInfo_OID in ({0})", m_Magnetic_CheckPointInfo_OID);
            }
            else
            {
                wheresql = string.Format("Magnetic_CheckPointInfo_OID={0}", m_Magnetic_CheckPointInfo_OID);
            }

            if (!string.IsNullOrWhiteSpace(fdate)&&!string.IsNullOrWhiteSpace(tdate))
            {
                wheresql += string.Format(" and CONVERT(varchar(100), CollectionDatetime, 23) >='{0}' and CONVERT(varchar(100), CollectionDatetime, 23)<='{1}'", Convert.ToDateTime(fdate).ToString("yyyy-MM-dd"), Convert.ToDateTime(tdate).ToString("yyyy-MM-dd"));
            }
            wheresql += " order by CollectionDatetime desc ";
            var list = EGMNGS.Common.ComServies.Query(sql+ wheresql);
            //SqlParameter[] parameters = {
            //        new SqlParameter("@intCurrentPage", SqlDbType.Int,4),
            //        new SqlParameter("@intPageSize", SqlDbType.Int,4),
            //        new SqlParameter("@strTableList", SqlDbType.VarChar,8000),
            //        new SqlParameter("@strFieldList", SqlDbType.VarChar,8000),
            //        new SqlParameter("@strOrderFld", SqlDbType.VarChar,8000),
            //        new SqlParameter("@strWhereClause", SqlDbType.VarChar,8000),
            //        new SqlParameter("@intTotoRecords", SqlDbType.Int),
            //        new SqlParameter("@intTotoPages", SqlDbType.Int)
            //};
            //parameters[0].Value = 1;
            //parameters[1].Value = 10;
            //parameters[2].Value = "Magnetic_CheckPointInfo";
            //parameters[3].Value = "*";
            //parameters[4].Value = "OID";
            //parameters[5].Value = "1=1";

            //var ds=  Maticsoft.DBUtility.DbHelperSQL.RunProcedure("pr_sys_QueryPaged", parameters);
            //var p = new DynamicParameters();
            //p.Add("@intCurrentPage", pager.PageIndex);
            //p.Add("@intPageSize", pager.PageSize);
            //p.Add("@strTableList", tblName);
            //p.Add("@strFieldList", pager.FieldName);
            //p.Add("@strOrderFld", pager.strOrderFld);
            //p.Add("@strWhereClause", pager.StrWhere);
            //p.Add("@intTotoRecords", pager.TotalRowsCount, DbType.Int32, ParameterDirection.Output);
            //p.Add("@intTotoPages", pager.TotalPagesCount, DbType.Int32, ParameterDirection.Output);

            // buffered = true;
            //var returnList = conn.Query<T>("pr_sys_QueryPaged", p, null, buffered, null, CommandType.StoredProcedure);
            //pager.TotalRowsCount = p.Get<Int32>("intTotoRecords");
            //pager.TotalPagesCount = p.Get<Int32>("intTotoPages");


            return Newtonsoft.Json.JsonConvert.SerializeObject(list);
        }

        private string DelMagnetic_CheckPointInfo(string m_OID)
        {
            Repository m_Repository = new Repository();
            bool success = m_Repository.Delete<EGMNGS.Model.Magnetic_CheckPointInfo>(m_OID);
            if (success)
            {
                return "删除成功";
            }
            else
            {
                return "删除失败";
            }
        }

        private string SaveMagnetic_CheckPointInfo(EGMNGS.Model.Magnetic_CheckPointInfo obj)
        {
            string returnValue = string.Empty;

            Repository m_Repository = new Repository();
            try
            {
                if (obj.OID == 0)
                {
                    obj.CREATED_DATE = DateTime.Now;
                    obj.CREATED_BY = RequestSession.GetSessionUser().UserName.ToString();
                    m_Repository.Insert<Magnetic_CheckPointInfo>(obj);
                }
                else
                {
                    obj.LAST_UPD_DATE = DateTime.Now;
                    obj.LAST_UPD_BY = RequestSession.GetSessionUser().UserName.ToString();
                    m_Repository.Update<Magnetic_CheckPointInfo>(obj);
                }
                returnValue = "保存成功";
            }
            catch (Exception ex)
            {
                returnValue = "保存失败";
                throw ex;
            }

            return returnValue;
        }

        private string GetMagnetic_CheckPointInfoList(string m_ConvertStationCode)
        {
            string returnValue = string.Empty;

            Repository m_Repository = new Repository();
            ;
            var list = m_Repository.Query<EGMNGS.Model.Magnetic_CheckPointInfo>(string.Format("select * from Magnetic_CheckPointInfo where ConvertStationCode in ({0})", m_ConvertStationCode));
            returnValue = Newtonsoft.Json.JsonConvert.SerializeObject(list);

            return returnValue;
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RM.Common.DotNetFile;
using DapperData;
using EGMNGS.Model;
using RM.Common.DotNetBean;
using System.Web.SessionState;
using Aspose.Cells;
using System.Data;

namespace RM.Web.MagneticPage
{
    /// <summary>
    /// UploadHandler 的摘要说明
    /// </summary>
    public class UploadHandler : IHttpHandler, IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            string Magnetic_CheckPointTemplatesB_OID = context.Request["Magnetic_CheckPointTemplatesB_OID"] ?? "";
            string action = context.Request["action"] ?? "";
            string fileClass = context.Request["fileClass"] ?? "";
            string returnMsg = string.Empty;

            if (action == "Get_FilesList")//文件列表
            {
                if (!string.IsNullOrWhiteSpace(Magnetic_CheckPointTemplatesB_OID) && !string.IsNullOrWhiteSpace(fileClass))
                {
                    returnMsg = Get_FilesList(Magnetic_CheckPointTemplatesB_OID, fileClass);
                }
            }
            else if (action == "ImportMagnetic_StationCheckPointMNGChilds")//导入工频电磁场强度
            {
                if (!string.IsNullOrWhiteSpace(Magnetic_CheckPointTemplatesB_OID))
                {
                    ImportMagnetic_StationCheckPointMNGChilds(Magnetic_CheckPointTemplatesB_OID, context);
                }
            }
            else if (action == "ImportMagnetic_WirelessDisturb")//导入无线电干扰
            {
                if (!string.IsNullOrWhiteSpace(Magnetic_CheckPointTemplatesB_OID))
                {
                    try
                    {

                        ImportMagnetic_WirelessDisturb(Magnetic_CheckPointTemplatesB_OID, context);

                    }
                    catch (Exception)
                    {

                    }
                }
            }
            else if (action == "ImportMagnetic_TransformerChilds")//导入噪声
            {
                string transformer_OID = context.Request["transformer_OID"] ?? "";
                if (!string.IsNullOrWhiteSpace(transformer_OID))
                {
                    ImportMagnetic_TransformerChilds(transformer_OID, context);
                }
            }
            else if (action == "ImportMagnetic_NoisePointPositionList")//导入噪声页面的测点位置
            {
                if (!string.IsNullOrWhiteSpace(Magnetic_CheckPointTemplatesB_OID))
                {
                    try
                    {

                        ImportMagnetic_NoisePointPositionList(Magnetic_CheckPointTemplatesB_OID, context);

                    }
                    catch (Exception ex)
                    {

                    }
                }
            }
            else if (action == "upload")//上传文件
            {
                try
                {
                    HttpPostedFile file = context.Request.Files[0];
                    string srcExt = file.FileName;

                    //  string path = context.Server.MapPath("~/Files/") + srcExt;
                    string phypath = context.Server.MapPath("~/Files/");
                    file.SaveAs(phypath + srcExt);

                    Repository repoistory = new Repository();
                    FileInfo fi = new FileInfo();
                    fi.ExportInfo_OID = Magnetic_CheckPointTemplatesB_OID;
                    fi.FileClass = fileClass;
                    fi.FileName = srcExt;
                    fi.FilePath = "~/Files/" + srcExt;
                    int index = srcExt.IndexOf('.');
                    if (index > 0)
                    {
                        fi.FileType = srcExt.Remove(0, index + 1);

                    }
                    fi.RegistrantName = RequestSession.GetSessionUser().UserName.ToString();
                    fi.FileSize = file.ContentLength;
                    int count = repoistory.Insert<FileInfo>(fi);
                    if (count > 0)
                    {
                        returnMsg = "上传成功";
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else if (action == "Del_Files")//删除
            {
                string oid = context.Request["OID"] ?? "";
                if (!string.IsNullOrWhiteSpace(oid))
                {
                    returnMsg = Del_Files(oid);
                }

            }
            else if (action == "DownLoad")
            {
                string url = context.Request["url"] ?? "";
                FileDownHelper.DownLoad(url);
            }

            context.Response.Write(returnMsg);
        }
        /// <summary>
        /// 导入噪声页面的测点位置
        /// </summary>
        /// <param name="magnetic_CheckPointTemplatesB_OID"></param>
        /// <param name="context"></param>
        private void ImportMagnetic_NoisePointPositionList(string magnetic_CheckPointTemplatesB_OID, HttpContext context)
        {
            HttpPostedFile file = context.Request.Files[0];
            string srcExt = file.FileName;
            string phypath = context.Server.MapPath("~/Files/");
            file.SaveAs(phypath + srcExt);

            Workbook workbook = new Workbook();
            workbook.Open(phypath + srcExt);
            Cells cells = workbook.Worksheets[0].Cells;
            System.Data.DataTable dataTable1 = cells.ExportDataTable(1, 0, cells.MaxDataRow, cells.MaxColumn + 1);
            System.Data.DataTable resutlsTable = EGMNGS.Common.ComServies.Query(@"SELECT CheckPointPosition,CheckPointPositionCode,LeqdBA,Remarks
  FROM Magnetic_NoisePointPosition where 1=0").Copy();

            LoadDataFromEls(resutlsTable, ref dataTable1);

            var lm = Common.DotNetData.DataTableHelper.DataTableToIList<Magnetic_NoisePointPosition>(resutlsTable) as IEnumerable<Magnetic_NoisePointPosition>;

            string username = RequestSession.GetSessionUser().UserName.ToString();
            foreach (var item in lm)
            {
                item.Magnetic_CheckPointTemplatesB_OID = magnetic_CheckPointTemplatesB_OID.AsTargetType<int>(0);
                item.CREATED_BY = username;
                item.CREATED_DATE = DateTime.Now;

            }

            Repository _Repositor = new Repository();
            _Repositor.Execute(SessionFactory.CreateConnection(), string.Format("delete Magnetic_NoisePointPosition where Magnetic_CheckPointTemplatesB_OID={0}", magnetic_CheckPointTemplatesB_OID.AsTargetType<int>(0)));

            _Repositor.InsertBatch<Magnetic_NoisePointPosition>(SessionFactory.CreateConnection(), lm);
        }

        private void ImportMagnetic_TransformerChilds(string transformer_OID, HttpContext context)
        {
            HttpPostedFile file = context.Request.Files[0];
            string srcExt = file.FileName;
            string phypath = context.Server.MapPath("~/Files/");
            file.SaveAs(phypath + srcExt);

            Workbook workbook = new Workbook();
            workbook.Open(phypath + srcExt);
            Cells cells = workbook.Worksheets[0].Cells;
            System.Data.DataTable dataTable1 = cells.ExportDataTable(1, 0, cells.MaxDataRow, cells.MaxColumn + 1);
            System.Data.DataTable resutlsTable = EGMNGS.Common.ComServies.Query(@"SELECT   
  [CheckPointPosition]
      ,[LeqdBA]
      ,[LeqdBZ]
      ,[L63H]
      ,[L125H]
      ,[L250H]
      ,[L500H]
      ,[L1KH]
      ,[L2KH]
      ,[L4KH]
      ,[L8KH]
      ,[Remarks]
  FROM [EGMNGS].[dbo].[Magnetic_TransformerChilds] where 1=0").Copy();

            LoadDataFromEls(resutlsTable, ref dataTable1);

            var lm = DataTableToMagnetic_TransformerChilds(resutlsTable);
            string username = RequestSession.GetSessionUser().UserName.ToString();
            foreach (var item in lm)
            {
                item.Transformer_OID = transformer_OID.AsTargetType<int>(0);
                item.CREATED_BY = username;
                item.CREATED_DATE = DateTime.Now;

            }

            Repository _Repositor = new Repository();
            _Repositor.Execute(SessionFactory.CreateConnection(), string.Format("delete Magnetic_TransformerChilds where Transformer_OID={0}", transformer_OID.AsTargetType<int>(0)));

            _Repositor.InsertBatch<Magnetic_TransformerChilds>(SessionFactory.CreateConnection(), lm);
        }
        private List<Magnetic_TransformerChilds> DataTableToMagnetic_TransformerChilds(DataTable dt)
        {
            List<Magnetic_TransformerChilds> list = new List<Magnetic_TransformerChilds>();
            foreach (DataRow item in dt.Rows)
            {
                Magnetic_TransformerChilds tempobj = new Magnetic_TransformerChilds();
                tempobj.CheckPointPosition = item["CheckPointPosition"].AsTargetType<string>("");
                tempobj.LeqdBA = item["LeqdBA"].AsTargetType<decimal>(0);
                tempobj.LeqdBZ = item["LeqdBZ"].AsTargetType<decimal>(0);
                tempobj.L63H = item["L63H"].AsTargetType<decimal>(0);
                tempobj.L125H = item["L125H"].AsTargetType<decimal>(0);
                tempobj.L250H = item["L250H"].AsTargetType<decimal>(0);
                tempobj.L500H = item["L500H"].AsTargetType<decimal>(0);
                tempobj.L1KH = item["L1KH"].AsTargetType<decimal>(0);
                tempobj.L2KH = item["L2KH"].AsTargetType<decimal>(0);
                tempobj.L4KH = item["L4KH"].AsTargetType<decimal>(0);

                tempobj.L8KH = item["L8KH"].AsTargetType<decimal>(0);
                tempobj.Remarks = item["Remarks"].AsTargetType<string>("");
                list.Add(tempobj);
            }
            return list;
        }
        private void ImportMagnetic_WirelessDisturb(string magnetic_CheckPointTemplatesB_OID, HttpContext context)
        {
            HttpPostedFile file = context.Request.Files[0];
            string srcExt = file.FileName;
            string phypath = context.Server.MapPath("~/Files/");
            file.SaveAs(phypath + srcExt);

            Workbook workbook = new Workbook();
            workbook.Open(phypath + srcExt);
            Cells cells = workbook.Worksheets[0].Cells;
            System.Data.DataTable dataTable1 = cells.ExportDataTable(10, 1, cells.MaxDataRow, cells.MaxColumn);
            System.Data.DataTable resutlsTable = EGMNGS.Common.ComServies.Query(@"SELECT   
                                                                                       [Distance]
                                                                                      ,[W0p15]
                                                                                      ,[W0p25]
                                                                                      ,[Wp5]
                                                                                      ,[W1]
                                                                                      ,[W1p5]
                                                                                      ,[W3]
                                                                                      ,[W6]
                                                                                      ,[W10]
                                                                                      ,[W15]
                                                                                      ,[W30]
                                                                                  FROM [EGMNGS].[dbo].[Magnetic_WirelessDisturb] where 1=0").Copy();

            LoadDataFromEls(resutlsTable, ref dataTable1);

            var lm = Common.DotNetData.DataTableHelper.DataTableToIList<Magnetic_WirelessDisturb>(resutlsTable) as IEnumerable<Magnetic_WirelessDisturb>;
            string username = RequestSession.GetSessionUser().UserName.ToString();
            foreach (var item in lm)
            {
                item.Magnetic_CheckPointTemplatesB_OID = magnetic_CheckPointTemplatesB_OID.AsTargetType<int>(0);
                item.LAST_UPD_BY = item.CREATED_BY = username;
                item.LAST_UPD_DATE = item.CREATED_DATE = DateTime.Now;
            }

            Repository _Repositor = new Repository();


            _Repositor.Execute(SessionFactory.CreateConnection(), string.Format("delete Magnetic_WirelessDisturb where Magnetic_CheckPointTemplatesB_OID={0}", magnetic_CheckPointTemplatesB_OID.AsTargetType<int>(0)));

            _Repositor.InsertBatch<Magnetic_WirelessDisturb>(SessionFactory.CreateConnection(), lm);
        }

        private void ImportMagnetic_StationCheckPointMNGChilds(string Magnetic_CheckPointTemplatesB_OID, HttpContext context)
        {
            HttpPostedFile file = context.Request.Files[0];
            string srcExt = file.FileName;
            string phypath = context.Server.MapPath("~/Files/");
            file.SaveAs(phypath + srcExt);

            Workbook workbook = new Workbook();
            workbook.Open(phypath + srcExt);
            Cells cells = workbook.Worksheets[0].Cells;
            System.Data.DataTable dataTable1 = cells.ExportDataTable(10, 1, cells.MaxDataRow, cells.MaxColumn + 1);
            System.Data.DataTable resutlsTable = EGMNGS.Common.ComServies.Query(@"SELECT   
[CheckPointPosition]
      ,[PositionName]  
      ,[ElectricFieldMAX]
      ,[ElectricFieldMIN]
      ,[ElectricFieldRMS]
      ,[MagneticInductionMAX]
      ,[MagneticInductionMIN]
      ,[MagneticInductionRMS]
      ,[Height]
      ,[Remarks]
  FROM [EGMNGS].[dbo].[Magnetic_StationCheckPointMNGChilds] where 1=0").Copy();

            LoadDataFromEls(resutlsTable, ref dataTable1);

            var lm = Common.DotNetData.DataTableHelper.DataTableToIList<Magnetic_StationCheckPointMNGChilds>(resutlsTable) as IEnumerable<Magnetic_StationCheckPointMNGChilds>;
            string username = RequestSession.GetSessionUser().UserName.ToString();
            foreach (Magnetic_StationCheckPointMNGChilds item in lm)
            {
                item.Magnetic_CheckPointTemplatesB_OID = Magnetic_CheckPointTemplatesB_OID.AsTargetType<int>(0);
                item.CREATED_BY = username;
                item.CREATED_DATE = DateTime.Now;

            }

            Repository _Repositor = new Repository();
            _Repositor.Execute(SessionFactory.CreateConnection(), string.Format("delete Magnetic_StationCheckPointMNGChilds where Magnetic_CheckPointTemplatesB_OID={0}", Magnetic_CheckPointTemplatesB_OID.AsTargetType<int>(0)));

            _Repositor.InsertBatch<Magnetic_StationCheckPointMNGChilds>(SessionFactory.CreateConnection(), lm);
        }
        /// <summary>
        /// 从XLS中加载数据到datable中
        /// </summary>
        /// <param name="dataTable1"></param>
        private void LoadDataFromEls(DataTable fromTable, ref DataTable dataTable1)
        {
            for (int i = 0; i < fromTable.Columns.Count; i++)
            {
                dataTable1.Columns[i].ColumnName = fromTable.Columns[i].ColumnName;
            }

            foreach (DataRow item in dataTable1.Rows)
            {
                var firstValue = item[0].AsTargetType<string>("");
                if ("" == firstValue)
                {
                    continue;
                }

                if (dataTable1.Columns.Contains("Distance"))
                {
                    item["Distance"] = item["Distance"].AsTargetType<string>("").Replace("米", "").AsTargetType<int>(0);
                }

                fromTable.ImportRow(item);
            }

        }

        private string Del_Files(string oid)
        {
            Repository m_Repository = new Repository();
            bool success = m_Repository.Delete<EGMNGS.Model.FileInfo>(Convert.ToInt32(oid));
            if (success)
            {
                return "删除成功";
            }
            else
            {
                return "删除失败";
            }
        }

        private string Get_FilesList(string Magnetic_CheckPointTemplatesB_OID, string fileClass)
        {
            Repository repoistory = new Repository();

            var list = repoistory.Query<FileInfo>(string.Format("select * from FileInfo where ExportInfo_OID='{0}' and FileClass='{1}'", Magnetic_CheckPointTemplatesB_OID, fileClass));
            // list = repoistory.GetByName<FileInfo>("ExportInfo_OID", Magnetic_CheckPointTemplatesB_OID);

            return Newtonsoft.Json.JsonConvert.SerializeObject(list);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}
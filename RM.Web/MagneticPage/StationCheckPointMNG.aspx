﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="StationCheckPointMNG.aspx.cs" Inherits="RM.Web.MagneticPage.StationCheckPointMNG" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <script type="text/javascript" src="../Themes/Scripts/jqueryeasyui/jquery.min.js"></script>

    <link href="../Themes/Scripts/jqueryeasyui/themes/bootstrap/easyui.css" rel="stylesheet" />
    <link href="../Themes/Scripts/jqueryeasyui/themes/icon.css" rel="stylesheet" />
    <link href="../Themes/Scripts/jqueryeasyui/themes/color.css" rel="stylesheet" />
    <link href="/Themes/Scripts/TreeView/treeview.css" rel="stylesheet" type="text/css" />
    <script src="/Themes/Scripts/TreeView/treeview.pack.js" type="text/javascript"></script>
    <script type="text/javascript" src="../Themes/Scripts/jqueryeasyui/jquery.easyui.min.js"></script>
    <script src="../Themes/Scripts/jqueryeasyui/plugins/jquery.edatagrid.js" type="text/javascript"></script>
    <script src="../Themes/Scripts/FunctionJS.js" type="text/javascript"></script>
    <script src="../Scripts/uploadify/jquery.uploadify.min.js" type="text/javascript"></script>

    <title>站点检测管理</title>

    <style type="text/css">
        html, body {
            margin: 0;
            height: 100%;
            overflow:visible;
        }

        form {
            height: 100%;
            width: 100%;
        }

        #containt {
            float: left;
            height: -moz-calc(100% -0px);
            height: -webkit-calc(100% - 0px);
            height: calc(100% - 0px);
            width: -moz-calc(100% - 0px);
            width: -webkit-calc(100% - 0px);
            width: calc(100% - 0px);
        }

        .Panel {
            float: left;
        }

        .leftButtom {
            height: 50%;
        }

        #buttumdiv > div {
            float: left;
        }

        #topdiv > div {
            float: left;
        }

        object {
            left: 0px;
            width: 100%;
        }

        .datagrid-toolbarTitle:after {
            content: "单位：电场强度（kV/m）、磁感应强度（μT）";
            top: -20px;
            float: right;
            position: relative;
        }

        .panel-body {

            overflow:visible;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">


        <div id="containt">
            <div id="leftPanel" class="Panel" style="width: 15%; height: 100%; border-right-width: 1px; border-right-style: solid;">
                <div style="height: 50%; overflow: scroll;">
                    <ul class="strTree">
                        <li>
                            <div title="广东电网公司">
                                广东电网公司
                            </div>
                            <ul>
                                <%=treeItem_Table.ToString() %>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="leftButtom">
                    <table id="Magnetic_CheckPointTemplatesBList">
                    </table>
                </div>
            </div>
            <div id="rightPanel" class="Panel" style="width: 84%; height: 100%;">
                <div id="tt" class="easyui-tabs">
                    <div title="工频电磁场强度" style="padding: 10px;">
                        <table id="Magnetic_StationCheckPointMNGChilds_List">
                        </table>
                    </div>
                    <div title="无线电干扰" style="padding: 10px;">
                        <table id="Magnetic_WirelessDisturbList">
                        </table>
                    </div>
                    <div title="噪声" style="padding: 10px;">
                        <div style="width: 100%; height: 40%;" id="topdiv">
                            <table id="Magnetic_NoisePointPositionList">
                            </table>

                            <table id="fileList"></table>
                        </div>
                        <div style="width: 100%; height: 59%;" id="buttumdiv">
                            <table id="Magnetic_TransformerList">
                            </table>

                            <table id="Magnetic_TransformerChildsList">
                            </table>

                        </div>

                    </div>
                </div>

            </div>


        </div>

    </form>

    <script type="text/javascript">

        var arrRuns = [];
        var $Magnetic_CheckPointTemplatesB_OID;
        var $width = $("#rightPanel").width();
        var $height = $("#rightPanel").height();

        //初始化
        $(function () {

            treeAttrCss();
            $.lightTreeview.close('.strTree ul ul', 100);

            $('#tt').tabs({
                width: $width,
                height: $height,
                border: false,
                onSelect: function (title) {
                    if (title == "无线电干扰") {
                        GetMagnetic_WirelessDisturbList();
                    }
                    else if (title == "噪声") {
                        LoadMagnetic_NoisePointPositionList($Magnetic_CheckPointTemplatesB_OID);
                        LoadMagnetic_TransformerList($Magnetic_CheckPointTemplatesB_OID)
                        LoadFileList($Magnetic_CheckPointTemplatesB_OID);//加载文件列表
                    }

                }
            });
        })

        function refreshList() {
            $("#Magnetic_CheckPointTemplatesBList").datagrid("reload");

        }

        //无线电干扰列表
        function GetMagnetic_WirelessDisturbList() {

            var editRow = undefined;
            var $datagrid = $("#Magnetic_WirelessDisturbList");
            $datagrid.datagrid({
                height: $height - 10,
                width: $width - 10,
                fitColumns: true,
                collapsible: true,
                singleSelect: true,
                url: 'Magnetic_CheckPointInfoHandler.ashx',
                queryParams: { action: 'Get_Magnetic_WirelessDisturbList', Magnetic_CheckPointTemplatesB_OID: $Magnetic_CheckPointTemplatesB_OID },
                idField: 'OID',
                columns: [[
                 { field: 'OID', title: 'ID', hidden: true },
                  { field: 'Magnetic_CheckPointTemplatesB_OID', title: 'Magnetic_CheckPointTemplatesB_OID', hidden: true },

                  {
                      field: 'Distance', width: 100, title: "距离", align: 'center', formatter: function (value) {

                          return value + '米';
                      }, editor: { type: 'text', options: { required: true } }
                  },
                   {
                       field: 'W0p15', width: 100, title: "0.15MHz", align: 'center', editor: { type: 'text', options: { required: true } }
                   },
                  {
                      field: 'W0p25', width: 150, title: "0.25MHz", align: 'center', editor: { type: 'text', options: { required: true } }
                  },
                  {
                      field: 'Wp5', width: 150, title: "0.5MHz", align: 'center', editor: { type: 'text', options: { required: true } }
                  },
                    {
                        field: 'W1', width: 150, title: "1MHz", align: 'center', editor: { type: 'text', options: { required: true } }
                    }, {
                        field: 'W1p5', width: 150, title: "1.5MHz", align: 'center', editor: { type: 'text', options: { required: true } }
                    },
                    {
                        field: 'W3', width: 150, title: "3MHz", align: 'center', editor: { type: 'text', options: { required: true } }
                    },
                    {
                        field: 'W6', width: 150, title: "6MHz", align: 'center', editor: { type: 'text', options: { required: true } }
                    },
                    {
                        field: 'W10', width: 150, title: "10MHz", align: 'center', editor: { type: 'text', options: { required: true } }
                    },
                {
                    field: 'W15', width: 150, title: "15MHz", align: 'center', editor: { type: 'text', options: { required: true } }
                }, {
                    field: 'W30', width: 150, title: "30MHz", align: 'center', editor: { type: 'text', options: { required: true } }
                }


                ]],
                toolbar: [{
                    text: '添加', iconCls: 'icon-add', handler: function () {
                        if (editRow != undefined) {
                            $datagrid.datagrid('endEdit', editRow);
                        }
                        if (editRow == undefined) {


                            $datagrid.datagrid('insertRow', {
                                index: 0,
                                row: { Magnetic_CheckPointTemplatesB_OID: $Magnetic_CheckPointTemplatesB_OID }
                            });

                            $datagrid.datagrid('beginEdit', 0);
                            editRow = 0;
                        }
                    }
                }, {
                    text: '保存', iconCls: 'icon-save', handler: function () {

                        var editIndex = editRow;

                        $datagrid.datagrid('endEdit', editRow);

                        $datagrid.datagrid('selectRow', editIndex)
                        var rowstr = $datagrid.datagrid('getSelected', editIndex)
                        //如果调用acceptChanges(),使用getChanges()则获取不到编辑和新增的数据。

                        //使用JSON序列化datarow对象，发送到后台。
                        //  var rows = $("#Student_Table").datagrid('getChanges');

                        //  var rowstr = JSON.stringify(rows[0]);
                        $.post('Magnetic_CheckPointInfoHandler.ashx?action=Save_Magnetic_WirelessDisturb', rowstr, function (data) {

                            showFaceMsg(data);
                        });

                    }
                }, {
                    text: '修改', iconCls: 'icon-edit', handler: function () {
                        var row = $datagrid.datagrid('getSelected');
                        if (row != null) {
                            if (editRow != undefined) {
                                $datagrid.datagrid('endEdit', editRow);
                            }
                            if (editRow == undefined) {
                                var index = $datagrid.datagrid('getRowIndex', row);
                                $datagrid.datagrid('beginEdit', index);
                                editRow = index;
                                $datagrid.datagrid('unselectAll');
                            }
                        } else {

                        }
                    }
                }, {
                    text: '删除', iconCls: 'icon-remove', handler: function () {
                        var row = $datagrid.datagrid('getSelected');
                        var index = $datagrid.datagrid('getRowIndex', row);


                        $.post('Magnetic_CheckPointInfoHandler.ashx?action=Del_Magnetic_WirelessDisturb', { oid: row.OID }, function (data) {
                            if (data == "删除成功") {
                                $datagrid.datagrid('deleteRow', index);
                            }

                            showFaceMsg(data);

                        });

                    }
                }, {
                    text: '导出', iconCls: 'icon-edit', handler: function () {
                        var row = $datagrid.datagrid('getSelected');
                        window.location = "Magnetic_CheckPointInfoHandler.ashx?action=Expert_Magnetic_WirelessDisturbListXls&Magnetic_CheckPointTemplatesB_OID=" + $Magnetic_CheckPointTemplatesB_OID;

                    }
                }, {
                    text: ' <span id="uploadify2" />', iconCls: 'folder_up', handler: function () {

                    }
                }, {
                    text: '刷新', iconCls: 'icon-reload', handler: function () {

                        $datagrid.datagrid("reload");
                    }

                }],
                onAfterEdit: function (rowIndex, rowData, changes) {
                    editRow = undefined;
                },
                onDblClickRow: function (rowIndex, rowData) {
                    if (editRow != undefined) {
                        $datagrid.datagrid('endEdit', editRow);
                    }

                    if (editRow == undefined) {
                        $datagrid.datagrid('beginEdit', rowIndex);
                        editRow = rowIndex;
                    }
                },
                onClickRow: function (rowIndex, rowData) {
                    //if (editRow != undefined) {
                    //    $datagrid.datagrid('endEdit', editRow);

                    //}

                }

            });

            $('#uploadify2').uploadify({
                'formData': {
                    'Magnetic_CheckPointTemplatesB_OID': $Magnetic_CheckPointTemplatesB_OID,
                    'action': 'ImportMagnetic_WirelessDisturb', fileClass: ""
                },
                'swf': '../Scripts/uploadify/uploadify.swf',
                'uploader': 'UploadHandler.ashx',
                'buttonText': '导入',
                'width': 40,
                'queueID': true,
                'fileTypeExts': '*.xls;*.xlsx',
                onUploadComplete: function myfunction(file) {

                    alert('导入成功');
                    $datagrid.datagrid("reload");
                }
            });
        }

        function GetTable(code, ConvertStationName) {
            LoadMagnetic_CheckPointTemplatesBList(code, ConvertStationName)
            Loading(true);
        }
        var url = "/MagneticPage/Magnetic_CheckPointTemplatesBDialog.aspx";
        function LoadMagnetic_CheckPointTemplatesBList(convertStationCode, convertStationName) {
            var $width = $(".leftButtom").width();
            var $height = $(".leftButtom").height();
            var editRow = undefined;
            var $datagrid = $("#Magnetic_CheckPointTemplatesBList");
            $datagrid.datagrid({
                height: $height,
                width: $width,
                fitColumns: true,
                collapsible: true,
                singleSelect: true,
                url: 'Magnetic_CheckPointInfoHandler.ashx',
                queryParams: { action: 'Get_Magnetic_CheckPointTemplatesB_List', ConvertStationCode: convertStationCode },
                idField: 'OID',
                columns: [[
                 { field: 'OID', title: 'ID', hidden: true },
                  {
                      field: 'MeasuringDate', width: 100, title: "测量日期", align: 'center', formatter:
function formatDatebox1(value) {
    if (value == null || value == '') {
        return '';
    }
    var dt;
    if (value instanceof Date) {
        dt = value;
    } else {
        // dt = new Date(value.replace('T', ' '));
        return value.replace('T', ' ');
    }

    return dt.format("yyyy-MM-dd");
}
                  },
                  {
                      field: 'TemplateType', width: 150, title: "模版类型", align: 'center'
                  }



                ]],
                toolbar: [{
                    text: '添加', iconCls: 'icon-add', handler: function () {


                        top.openDialog(url + "?convertStationCode=" + convertStationCode, 'Magnetic_CheckPointTemplatesBDialog', '站点测点添加', 950, 620, 50, 50);

                    }
                }, {
                    text: '修改', iconCls: 'icon-edit', handler: function () {
                        var row = $datagrid.datagrid('getSelected');
                        top.openDialog(url + "?OID=" + row.OID, 'Magnetic_CheckPointTemplatesBDialog', '站点测点修改', 950, 620, 50, 50);

                    }
                }, {
                    text: '删除', iconCls: 'icon-remove', handler: function () {
                        var row = $datagrid.datagrid('getSelected');
                        var index = $datagrid.datagrid('getRowIndex', row);


                        $.post('Magnetic_CheckPointInfoHandler.ashx?action=DelMagnetic_CheckPointTemplatesB', { oid: row.OID }, function (data) {
                            if (data == "删除成功") {
                                $datagrid.datagrid('deleteRow', index);
                            }

                            showFaceMsg(data);

                        });

                    }
                }

                ],
                onAfterEdit: function (rowIndex, rowData, changes) {
                    editRow = undefined;
                },
                onDblClickRow: function (rowIndex, rowData) {
                    if (editRow != undefined) {
                        $datagrid.datagrid('endEdit', editRow);
                    }

                    if (editRow == undefined) {
                        $datagrid.datagrid('beginEdit', rowIndex);
                        editRow = rowIndex;
                    }
                },
                onClickRow: function (rowIndex, rowData) {

                    $Magnetic_CheckPointTemplatesB_OID = rowData.OID;

                    var tab = $('#tt').tabs('getSelected');
                    var index = $('#tt').tabs('getTabIndex', tab);

                    if (index == 0) {
                        LoadMagnetic_StationCheckPointMNGChilds_List($Magnetic_CheckPointTemplatesB_OID)

                    }
                    else if (index == 1) {
                        GetMagnetic_WirelessDisturbList();
                    }
                    else if (index == 2) {
                        LoadMagnetic_NoisePointPositionList($Magnetic_CheckPointTemplatesB_OID);
                        LoadMagnetic_TransformerList($Magnetic_CheckPointTemplatesB_OID);
                        LoadFileList($Magnetic_CheckPointTemplatesB_OID);//加载文件列表
                    }

                }

            });
        }
        //加载文件列表
        function LoadFileList(magnetic_CheckPointTemplatesB_OID) {
            var _fileClass = "检测模版";
            var editRow = undefined;
            var $datagrid = $("#fileList");
            var $heightNoise = $("#topdiv").height();
            var $widthNoise = 500;

            $datagrid.datagrid({
                height: $heightNoise - 10,
                width: $widthNoise+10,
                fitColumns: true,
                collapsible: true,
                singleSelect: true,
                url: 'UploadHandler.ashx',
                queryParams: { action: 'Get_FilesList', Magnetic_CheckPointTemplatesB_OID: magnetic_CheckPointTemplatesB_OID, fileClass: _fileClass },
                idField: 'OID',
                columns: [[
                 { field: 'OID', title: 'ID', hidden: true },
                   {
                       field: 'FileName', width: 150, title: "文件名", align: 'center'
                   },

                  {
                      field: 'FileSize', width: 100, title: "文件大小", align: 'center'
                  },
                    {
                        field: 'CreateDate', width: 100, title: "上传时间", align: 'center'
                    }, {
                        field: 'RegistrantName', width: 100, title: "上传者", align: 'center'
                    }


                ]],
                toolbar: [{
                    text: ' <span id="uploadify" />', iconCls: 'folder_up', handler: function () {

                    }

                }, {
                    text: '下载', iconCls: 'package_down', handler: function () {

                        var editIndex = editRow;

                        $datagrid.datagrid('endEdit', editRow);

                        $datagrid.datagrid('selectRow', editIndex)
                        var rowstr = $datagrid.datagrid('getSelected', editIndex)
                        window.location = 'UploadHandler.ashx?action=DownLoad&url=' + rowstr.FilePath;


                    }
                }, {
                    text: '删除', iconCls: 'icon-remove', handler: function () {
                        var row = $datagrid.datagrid('getSelected');
                        var index = $datagrid.datagrid('getRowIndex', row);


                        $.post('UploadHandler.ashx?action=Del_Files', { oid: row.OID }, function (data) {
                            if (data == "删除成功") {
                                $datagrid.datagrid('deleteRow', index);
                            }

                            showFaceMsg(data);

                        });

                    }
                }],
                onAfterEdit: function (rowIndex, rowData, changes) {
                    editRow = undefined;
                },
                onDblClickRow: function (rowIndex, rowData) {
                    if (editRow != undefined) {
                        $datagrid.datagrid('endEdit', editRow);
                    }

                    if (editRow == undefined) {
                        $datagrid.datagrid('beginEdit', rowIndex);
                        editRow = rowIndex;
                    }
                },
                onClickRow: function (rowIndex, rowData) {
                    if (editRow != undefined) {
                        $datagrid.datagrid('endEdit', editRow);

                    }
                    editRow = rowIndex;
                }

            });


            $('#uploadify').uploadify({
                'formData': {
                    'Magnetic_CheckPointTemplatesB_OID': magnetic_CheckPointTemplatesB_OID,
                    'action': 'upload', fileClass: _fileClass
                },
                'swf': '../Scripts/uploadify/uploadify.swf',
                'uploader': 'UploadHandler.ashx',
                'buttonText': '上传',
                'width': 40,
                'queueID': true,
                onUploadComplete: function myfunction(file) {
                    LoadFileList(magnetic_CheckPointTemplatesB_OID)
                    alert('上传成功');
                }
            });
        }
        function LoadMagnetic_StationCheckPointMNGChilds_List(magnetic_CheckPointTemplatesB_OID) {

            var editRow = undefined;
            var $datagrid = $("#Magnetic_StationCheckPointMNGChilds_List");
            $datagrid.datagrid({
                rownumbers: true,
                height: $height - 10,
                width: $width - 10,
                fitColumns: true,
                collapsible: true,
                singleSelect: true,
                url: 'Magnetic_CheckPointInfoHandler.ashx',
                queryParams: { action: 'Get_Magnetic_StationCheckPointMNGChilds_List', Magnetic_CheckPointTemplatesB_OID: magnetic_CheckPointTemplatesB_OID },
                idField: 'OID',
                columns: [[
                 { field: 'OID', title: 'ID', hidden: true },
                     { field: 'Magnetic_CheckPointTemplatesB_OID', title: 'Magnetic_CheckPointTemplatesB_OID', hidden: true },
                   {
                       field: 'CheckPointPosition', width: 240, title: "测点位置", align: 'center', editor: { type: 'text', options: { required: true } }
                   },
                  {
                      field: 'PositionName', width: 240, title: "位置名称", align: 'center', editor: { type: 'text', options: { required: true } }
                  },
                  {
                      field: 'ElectricFieldMAX', width: 120, title: "电场强度MAX", align: 'center', editor: { type: 'text', options: { required: true } }
                  },
                    {
                        field: 'ElectricFieldMIN', width: 120, title: "电场强度MIN", align: 'center', editor: { type: 'text', options: { required: true } }
                    }, {
                        field: 'ElectricFieldRMS', width: 120, title: "电场强度RMS", align: 'center', editor: { type: 'text', options: { required: true } }
                    },
                    {
                        field: 'MagneticInductionMAX', width: 120, title: "磁感应强度MAX", align: 'center', editor: { type: 'text', options: { required: true } }
                    },
                    {
                        field: 'MagneticInductionMIN', width: 120, title: "磁感应强度MIN", align: 'center', editor: { type: 'text', options: { required: true } }
                    },
                    {
                        field: 'MagneticInductionRMS', width: 120, title: "磁感应强度RMS", align: 'center', editor: { type: 'text', options: { required: true } }
                    },
                {
                    field: 'Height', width: 70, title: "高度", align: 'center', editor: { type: 'text' }
                }, {
                    field: 'Remarks', width: 150, title: "备注", align: 'center', editor: { type: 'text' }
                }


                ]],
                toolbar: [{
                    text: '添加', iconCls: 'icon-add', handler: function () {
                        if (editRow != undefined) {
                            $datagrid.datagrid('endEdit', editRow);
                        }
                        if (editRow == undefined) {


                            $datagrid.datagrid('insertRow', {
                                index: 0,
                                row: { Magnetic_CheckPointTemplatesB_OID: magnetic_CheckPointTemplatesB_OID }
                            });

                            $datagrid.datagrid('beginEdit', 0);
                            editRow = 0;
                        }
                    }

                }, '-', {
                    text: '保存', iconCls: 'icon-save', handler: function () {

                        var editIndex = editRow;

                        $datagrid.datagrid('endEdit', editRow);

                        $datagrid.datagrid('selectRow', editIndex)
                        var rowstr = $datagrid.datagrid('getSelected', editIndex)
                        //如果调用acceptChanges(),使用getChanges()则获取不到编辑和新增的数据。

                        //使用JSON序列化datarow对象，发送到后台。

                        $.post('Magnetic_CheckPointInfoHandler.ashx?action=Save_Magnetic_StationCheckPointMNGChilds', rowstr, function (data) {

                            showFaceMsg(data);
                        });

                    }
                }, '-', {
                    text: '修改', iconCls: 'icon-edit', handler: function () {
                        var row = $datagrid.datagrid('getSelected');
                        if (row != null) {
                            if (editRow != undefined) {
                                $datagrid.datagrid('endEdit', editRow);
                            }

                            if (editRow == undefined) {
                                var index = $datagrid.datagrid('getRowIndex', row);
                                $datagrid.datagrid('beginEdit', index);
                                editRow = index;
                                $datagrid.datagrid('unselectAll');
                            }
                        } else {

                        }
                    }
                }, '-', {
                    text: '删除', iconCls: 'icon-remove', handler: function () {
                        var row = $datagrid.datagrid('getSelected');
                        var index = $datagrid.datagrid('getRowIndex', row);


                        $.post('Magnetic_CheckPointInfoHandler.ashx?action=DelMagnetic_StationCheckPointMNGChilds', { oid: row.OID }, function (data) {
                            if (data == "删除成功") {
                                $datagrid.datagrid('deleteRow', index);
                            }

                            showFaceMsg(data);

                        });

                    }
                }, '-', {
                    text: '导出', iconCls: 'icon-edit', handler: function () {
                        var row = $datagrid.datagrid('getSelected');
                        window.location = "Magnetic_CheckPointInfoHandler.ashx?action=SCPMNGExportXls&Magnetic_CheckPointTemplatesB_OID=" + magnetic_CheckPointTemplatesB_OID;

                    }
                }, '-', {
                    text: ' <span id="uploadify1" />', iconCls: 'folder_up', handler: function () {

                    }
                }],
                onAfterEdit: function (rowIndex, rowData, changes) {
                    editRow = undefined;
                },
                onDblClickRow: function (rowIndex, rowData) {
                    if (editRow != undefined) {
                        $datagrid.datagrid('endEdit', editRow);
                    }

                    if (editRow == undefined) {
                        $datagrid.datagrid('beginEdit', rowIndex);
                        editRow = rowIndex;
                    }
                },
                onClickRow: function (rowIndex, rowData) {
                    if (editRow != undefined) {
                        $datagrid.datagrid('endEdit', editRow);

                    }

                }

            });


            $('#uploadify1').uploadify({
                'formData': {
                    'Magnetic_CheckPointTemplatesB_OID': magnetic_CheckPointTemplatesB_OID,
                    'action': 'ImportMagnetic_StationCheckPointMNGChilds', fileClass: ""
                },
                'swf': '../Scripts/uploadify/uploadify.swf',
                'uploader': 'UploadHandler.ashx',
                'buttonText': '导入',
                'width': 40,
                'queueID': true,
                'fileTypeExts': '*.xls;*.xlsx',
                onUploadComplete: function myfunction(file) {
                    //  LoadFileList(magnetic_CheckPointTemplatesB_OID)
                    alert('导入成功');
                    $datagrid.datagrid("reload");
                }
            });


            $(".datagrid-toolbar").eq(1).addClass("datagrid-toolbarTitle");
        }

        //噪声
        function LoadMagnetic_NoisePointPositionList(magnetic_CheckPointTemplatesB_OID) {
            var editRow = undefined;
            var $datagrid = $("#Magnetic_NoisePointPositionList");
            var $heightNoise = $("#topdiv").height();
            var $widthNoise = $("#topdiv").width() - 500;

            $datagrid.datagrid({
                height: $heightNoise - 10,
                width: $widthNoise - 10,
                fitColumns: true,
                collapsible: true,
                singleSelect: true,
                url: 'Magnetic_CheckPointInfoHandler.ashx',
                queryParams: { action: 'Get_Magnetic_NoisePointPositionList', Magnetic_CheckPointTemplatesB_OID: magnetic_CheckPointTemplatesB_OID },
                idField: 'OID',
                columns: [[
                 { field: 'OID', title: 'ID', hidden: true },
                  { field: 'Magnetic_CheckPointTemplatesB_OID', title: 'Magnetic_CheckPointTemplatesB_OID', hidden: true },
                   {
                       field: 'CheckPointPosition', width: 150, title: "测点位置", align: 'center', editor: { type: 'text', options: { required: true } }
                   },
                  {
                      field: 'CheckPointPositionCode', width: 100, title: "测点位置编号", align: 'center', editor: { type: 'text', options: { required: true } }
                  },
                  {
                      field: 'LeqdBA', width: 100, title: "Leq/dB(A)", align: 'center', editor: { type: 'text', options: { required: true } }
                  },
                    {
                        field: 'Remarks', width: 100, title: "备注", align: 'center', editor: { type: 'text', options: { required: true } }
                    }
                ]],
                toolbar: [{
                    text: '添加', iconCls: 'icon-add', handler: function () {
                        if (editRow != undefined) {
                            $datagrid.datagrid('endEdit', editRow);
                        }
                        if (editRow == undefined) {


                            $datagrid.datagrid('insertRow', {
                                index: 0,
                                row: { Magnetic_CheckPointTemplatesB_OID: magnetic_CheckPointTemplatesB_OID }
                            });

                            $datagrid.datagrid('beginEdit', 0);
                            editRow = 0;
                        }
                    }

                }, '-', {
                    text: '保存', iconCls: 'icon-save', handler: function () {

                        var editIndex = editRow;

                        $datagrid.datagrid('endEdit', editRow);

                        $datagrid.datagrid('selectRow', editIndex)
                        var rowstr = $datagrid.datagrid('getSelected', editIndex)
                        //如果调用acceptChanges(),使用getChanges()则获取不到编辑和新增的数据。

                        //使用JSON序列化datarow对象，发送到后台。

                        $.post('Magnetic_CheckPointInfoHandler.ashx?action=Save_Magnetic_NoisePointPosition', rowstr, function (data) {
                            showFaceMsg(data);
                        });

                    }
                }, '-', {
                    text: '修改', iconCls: 'icon-edit', handler: function () {
                        var row = $datagrid.datagrid('getSelected');
                        if (row != null) {
                            if (editRow != undefined) {
                                $datagrid.datagrid('endEdit', editRow);
                            }

                            if (editRow == undefined) {
                                var index = $datagrid.datagrid('getRowIndex', row);
                                $datagrid.datagrid('beginEdit', index);
                                editRow = index;
                                $datagrid.datagrid('unselectAll');
                            }
                        } else {

                        }
                    }
                }, '-', {
                    text: '删除', iconCls: 'icon-remove', handler: function () {
                        var row = $datagrid.datagrid('getSelected');
                        var index = $datagrid.datagrid('getRowIndex', row);


                        $.post('Magnetic_CheckPointInfoHandler.ashx?action=DelMagnetic_NoisePointPosition', { oid: row.OID }, function (data) {
                            if (data == "删除成功") {
                                $datagrid.datagrid('deleteRow', index);
                            }

                            showFaceMsg(data);

                        });

                    }
                }, '-', {
                    text: '导出', iconCls: 'icon-edit', handler: function () {
                        var row = $datagrid.datagrid('getSelected');
                        window.location = "Magnetic_CheckPointInfoHandler.ashx?action=Expert_Magnetic_NoisePointPositionListXls&Magnetic_CheckPointTemplatesB_OID=" + magnetic_CheckPointTemplatesB_OID;

                    }
                }, '-', {
                    text: ' <span id="uploadify4" />', iconCls: 'folder_up', handler: function () {

                    }
                }],
                onAfterEdit: function (rowIndex, rowData, changes) {
                    editRow = undefined;
                },
                onDblClickRow: function (rowIndex, rowData) {
                    if (editRow != undefined) {
                        $datagrid.datagrid('endEdit', editRow);
                    }

                    if (editRow == undefined) {
                        $datagrid.datagrid('beginEdit', rowIndex);
                        editRow = rowIndex;
                    }
                },
                onClickRow: function (rowIndex, rowData) {
                    if (editRow != undefined) {
                        $datagrid.datagrid('endEdit', editRow);

                    }

                }

            });


            $('#uploadify4').uploadify({
                'formData': {
                    'Magnetic_CheckPointTemplatesB_OID': magnetic_CheckPointTemplatesB_OID,
                    'action': 'ImportMagnetic_NoisePointPositionList', fileClass: ""
                },
                'swf': '../Scripts/uploadify/uploadify.swf',
                'uploader': 'UploadHandler.ashx',
                'buttonText': '导入',
                'width': 40,
                'queueID': true,
                'fileTypeExts': '*.xls;*.xlsx',
                onUploadComplete: function myfunction(file) {
                    alert('导入成功');
                    $datagrid.datagrid("reload");
                }
            });
        }
        //变压器
        function LoadMagnetic_TransformerList(magnetic_CheckPointTemplatesB_OID) {
            var editRow = undefined;
            var $datagrid = $("#Magnetic_TransformerList");
            var $heightNoise = $("#buttumdiv").height();
            var $widthNoise = $("#buttumdiv").width();

            $datagrid.datagrid({
                height: $heightNoise - 10,
                width: 330,
                fitColumns: true,
                collapsible: true,
                singleSelect: true,
                url: 'Magnetic_CheckPointInfoHandler.ashx',
                queryParams: { action: 'Get_Magnetic_TransformerList', Magnetic_CheckPointTemplatesB_OID: magnetic_CheckPointTemplatesB_OID },
                idField: 'OID',
                columns: [[
                 { field: 'OID', title: 'ID', hidden: true },
                  { field: 'Magnetic_CheckPointTemplatesB_OID', title: 'Magnetic_CheckPointTemplatesB_OID', hidden: true },
                   {
                       field: 'TransformerCode', width: 100, title: "变压器编号", align: 'center', editor: { type: 'text', options: { required: true } }
                   },
                  {
                      field: 'Manufacturer', width: 150, title: "变压器生产厂家", align: 'center', editor: { type: 'text', options: { required: true } }
                  },
                  {
                      field: 'TransformerType', width: 150, title: "变压器器型号", align: 'center', editor: { type: 'text', options: { required: true } }
                  }

                ]],
                toolbar: [{
                    text: '添加', iconCls: 'icon-add', handler: function () {
                        if (editRow != undefined) {
                            $datagrid.datagrid('endEdit', editRow);
                        }
                        if (editRow == undefined) {


                            $datagrid.datagrid('insertRow', {
                                index: 0,
                                row: { Magnetic_CheckPointTemplatesB_OID: magnetic_CheckPointTemplatesB_OID }
                            });

                            $datagrid.datagrid('beginEdit', 0);
                            editRow = 0;
                        }
                    }

                }, '-', {
                    text: '保存', iconCls: 'icon-save', handler: function () {

                        var editIndex = editRow;

                        $datagrid.datagrid('endEdit', editRow);

                        $datagrid.datagrid('selectRow', editIndex)
                        var rowstr = $datagrid.datagrid('getSelected', editIndex)

                        $.post('Magnetic_CheckPointInfoHandler.ashx?action=Save_Magnetic_Transformer', rowstr, function (data) {
                            $datagrid.datagrid('reload');

                            setTimeout(function myfunction() {
                                $datagrid.datagrid('selectRow', editIndex);
                                var rowstr = $datagrid.datagrid('getSelected', editIndex)
                                LoadMagnetic_TransformerChildsList(rowstr.OID);
                            }, 500)



                        });



                        //如果调用acceptChanges(),使用getChanges()则获取不到编辑和新增的数据。

                        //使用JSON序列化datarow对象，发送到后台。
                        //  var rows = $("#Student_Table").datagrid('getChanges');

                        //  var rowstr = JSON.stringify(rows[0]);


                    }
                }, '-', {
                    text: '修改', iconCls: 'icon-edit', handler: function () {
                        var row = $datagrid.datagrid('getSelected');
                        if (row != null) {
                            if (editRow != undefined) {
                                $datagrid.datagrid('endEdit', editRow);
                            }

                            if (editRow == undefined) {
                                var index = $datagrid.datagrid('getRowIndex', row);
                                $datagrid.datagrid('beginEdit', index);
                                editRow = index;
                                $datagrid.datagrid('unselectAll');
                            }
                        } else {

                        }
                    }
                }, '-', {
                    text: '删除', iconCls: 'icon-remove', handler: function () {
                        var row = $datagrid.datagrid('getSelected');
                        var index = $datagrid.datagrid('getRowIndex', row);


                        $.post('Magnetic_CheckPointInfoHandler.ashx?action=Del_Magnetic_Transformer', { oid: row.OID }, function (data) {
                            if (data == "删除成功") {
                                $datagrid.datagrid('deleteRow', index);
                            }

                            showFaceMsg(data);

                        });

                    }
                }],
                onAfterEdit: function (rowIndex, rowData, changes) {
                    editRow = undefined;
                },
                onDblClickRow: function (rowIndex, rowData) {
                    if (editRow != undefined) {
                        $datagrid.datagrid('endEdit', editRow);
                    }

                    if (editRow == undefined) {
                        $datagrid.datagrid('beginEdit', rowIndex);
                        editRow = rowIndex;
                    }
                },
                onClickRow: function (rowIndex, rowData) {
                    //if (editRow != undefined) {
                    //    $datagrid.datagrid('endEdit', editRow);

                    //}
                    LoadMagnetic_TransformerChildsList(rowData.OID);
                }



            });
        }

        //变压器子表
        function LoadMagnetic_TransformerChildsList(transformer_OID) {
            var editRow = undefined;
            var $datagrid = $("#Magnetic_TransformerChildsList");
            var $heightNoise = $("#buttumdiv").height();
            var $widthNoise = $("#buttumdiv").width();

            $datagrid.datagrid({
                height: $heightNoise - 10,
                width: $widthNoise - 330,
                fitColumns: false,
                collapsible: true,
                singleSelect: true,
                url: 'Magnetic_CheckPointInfoHandler.ashx',
                queryParams: { action: 'Get_Magnetic_TransformerChildsList', Transformer_OID: transformer_OID },
                idField: 'OID',
                columns: [[
                 { field: 'OID', title: 'ID', hidden: true },
                  { field: 'Transformer_OID', title: 'Transformer_OID', hidden: true },
                   {
                       field: 'CheckPointPosition', width: 100, title: "测量位置编号", align: 'center', editor: { type: 'text', options: { required: true } }
                   },
                  {
                      field: 'LeqdBA', width: 80, title: "Leq/dB(A)", align: 'center', editor: { type: 'text', options: { required: true } }
                  },
                  {
                      field: 'LeqdBZ', width: 80, title: "Leq/dB(Z)", align: 'center', editor: { type: 'text', options: { required: true } }
                  },
                        {
                            field: 'L63H', width: 50, title: "63Hz", align: 'center', editor: { type: 'text', options: { required: true } }
                        }, {
                            field: 'L125H', width: 50, title: "125Hz", align: 'center', editor: { type: 'text', options: { required: true } }
                        }, {
                            field: 'L250H', width: 50, title: "250Hz", align: 'center', editor: { type: 'text', options: { required: true } }
                        }, {
                            field: 'L500H', width: 50, title: "500Hz", align: 'center', editor: { type: 'text', options: { required: true } }
                        }, {
                            field: 'L1KH', width: 50, title: "1KHz", align: 'center', editor: { type: 'text', options: { required: true } }
                        }, {
                            field: 'L2KH', width: 50, title: "2KHz", align: 'center', editor: { type: 'text', options: { required: true } }
                        }, {
                            field: 'L4KH', width: 50, title: "4KHz", align: 'center', editor: { type: 'text', options: { required: true } }
                        }, {
                            field: 'L8KH', width: 50, title: "8KHz", align: 'center', editor: { type: 'text', options: { required: true } }
                        }, {
                            field: 'Remarks', width: 100, title: "备注", align: 'center', editor: { type: 'text', options: { required: true } }
                        }

                ]],
                toolbar: [{
                    text: '添加', iconCls: 'icon-add', handler: function () {
                        if (editRow != undefined) {
                            $datagrid.datagrid('endEdit', editRow);
                        }
                        if (editRow == undefined) {


                            $datagrid.datagrid('insertRow', {
                                index: 0,
                                row: { Transformer_OID: transformer_OID }
                            });

                            $datagrid.datagrid('beginEdit', 0);
                            editRow = 0;
                        }
                    }

                }, '-', {
                    text: '保存', iconCls: 'icon-save', handler: function () {

                        var editIndex = editRow;

                        $datagrid.datagrid('endEdit', editRow);

                        $datagrid.datagrid('selectRow', editIndex)
                        var rowstr = $datagrid.datagrid('getSelected', editIndex)
                        //如果调用acceptChanges(),使用getChanges()则获取不到编辑和新增的数据。

                        //使用JSON序列化datarow对象，发送到后台。
                        //  var rows = $("#Student_Table").datagrid('getChanges');

                        //  var rowstr = JSON.stringify(rows[0]);
                        $.post('Magnetic_CheckPointInfoHandler.ashx?action=Save_Magnetic_TransformerChilds', rowstr, function (data) {
                            showFaceMsg(data);
                        });

                    }
                }, '-', {
                    text: '修改', iconCls: 'icon-edit', handler: function () {
                        var row = $datagrid.datagrid('getSelected');
                        if (row != null) {
                            if (editRow != undefined) {
                                $datagrid.datagrid('endEdit', editRow);
                            }

                            if (editRow == undefined) {
                                var index = $datagrid.datagrid('getRowIndex', row);
                                $datagrid.datagrid('beginEdit', index);
                                editRow = index;
                                $datagrid.datagrid('unselectAll');
                            }
                        } else {

                        }
                    }
                }, '-', {
                    text: '删除', iconCls: 'icon-remove', handler: function () {
                        var row = $datagrid.datagrid('getSelected');
                        var index = $datagrid.datagrid('getRowIndex', row);


                        $.post('Magnetic_CheckPointInfoHandler.ashx?action=Del_Magnetic_TransformerChilds', { oid: row.OID }, function (data) {
                            if (data == "删除成功") {
                                $datagrid.datagrid('deleteRow', index);
                            }

                            showFaceMsg(data);

                        });

                    }
                }, '-', {
                    text: ' <span id="uploadify3" />', iconCls: 'folder_up', handler: function () {

                    }
                }],
                onAfterEdit: function (rowIndex, rowData, changes) {
                    editRow = undefined;
                },
                onDblClickRow: function (rowIndex, rowData) {
                    if (editRow != undefined) {
                        $datagrid.datagrid('endEdit', editRow);
                    }

                    if (editRow == undefined) {
                        $datagrid.datagrid('beginEdit', rowIndex);
                        editRow = rowIndex;
                    }
                },
                onClickRow: function (rowIndex, rowData) {
                    if (editRow != undefined) {
                        $datagrid.datagrid('endEdit', editRow);

                    }


                }

            });


            $('#uploadify3').uploadify({
                'formData': {
                    'Transformer_OID': transformer_OID,
                    'action': 'ImportMagnetic_TransformerChilds', fileClass: ""
                },
                'swf': '../Scripts/uploadify/uploadify.swf',
                'uploader': 'UploadHandler.ashx',
                'buttonText': '导入',
                'width': 40,
                'queueID': true,
                'fileTypeExts': '*.xls;*.xlsx',
                onUploadComplete: function myfunction(file) {
                    //  LoadFileList(magnetic_CheckPointTemplatesB_OID)
                    alert('导入成功');
                    $datagrid.datagrid("reload");
                }
            });
        }

    </script>

</body>
</html>

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;
using DapperData;
using EGMNGS.Model;
using RM.Common.DotNetBean;

namespace RM.Web.MagneticPage
{
    /// <summary>
    /// CheckPointTemplatesPageHandler 的摘要说明
    /// </summary>
    public class CheckPointTemplatesPageHandler : IHttpHandler, IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "application/json";

            string action = context.Request["action"] ?? "";
            string returnJsonStr = string.Empty;

            if (action == "GetCheckPointTemplatesList")
            {
                returnJsonStr = GetCheckPointTemplatesList();
            }
            else if (action == "SaveCheckPointTemplates")
            {
                context.Response.ContentType = "text/plain";
                returnJsonStr = SaveCheckPointTemplates();
            }
            else if (action == "DeleteCheckPointTemplates")//删除模版
            {
                context.Response.ContentType = "text/plain";
                string m_OID = context.Request["OID"] ?? "";
                returnJsonStr = DeleteCheckPointTemplates(m_OID);

            }
            else if ("GetCheckPointPositionList" == action)
            {
                context.Response.ContentType = "application/json";
                string m_CheckPointTemplates_OID = context.Request["CheckPointTemplates_OID"] ?? "";
                returnJsonStr = GetCheckPointPositionList(m_CheckPointTemplates_OID);
            }
            else if ("SaveCheckPointPosition" == action)// 保存测点位置
            {
                context.Response.ContentType = "text/plain";

                EGMNGS.Model.Magnetic_CheckPointTemplates_Childs obj = new EGMNGS.Model.Magnetic_CheckPointTemplates_Childs();
                obj = RM.Common.DotNetUI.ControlBindHelper.ModelBing<EGMNGS.Model.Magnetic_CheckPointTemplates_Childs>(obj);

                returnJsonStr = SaveCheckPointPosition(obj);
            }
            else if ("DelCheckPointPosition" == action)//删除测点位置
            {
                context.Response.ContentType = "text/plain";
                string m_OID = context.Request["OID"] ?? "";
                returnJsonStr = DelCheckPointPosition(m_OID);
            }
            else if (action == "CopyCheckPointTemplates")//复制模版
            {
                context.Response.ContentType = "text/plain";
                string OID = context.Request["OID"] ?? "";
                returnJsonStr = CopyCheckPointTemplates(OID);
            }
         
            context.Response.Write(returnJsonStr);
        }
        private string CopyCheckPointTemplates(string oID)
        {
            Repository m_Repository = new Repository();
            string returnValue = string.Empty;

            //todo复制主表
            try
            {

                var obj = m_Repository.GetById<Magnetic_CheckPointTemplates>(oID);
                obj.TemplateType += "2";
                int oid = m_Repository.Insert<Magnetic_CheckPointTemplates>(obj);
                //toto复制从表
                var listChilds = m_Repository.GetByName<Magnetic_CheckPointTemplates_Childs>("CheckPointTemplates_OID", oID.ToString());
                string userName = RequestSession.GetSessionUser().UserName.ToString() ;
                foreach (var item in listChilds)
                {
                    item.CheckPointTemplates_OID = oid;
                    item.CREATED_BY = userName;
                    item.CREATED_DATE = DateTime.Now;
                }
                m_Repository.InsertBatch(SessionFactory.CreateConnection(), listChilds);
                returnValue = "复制成功";
            }
            catch (Exception ex)
            {
                returnValue = "复制失败";
                Common.DotNetCode.LogInstanseHelper.Instance.WriteLog(ex.Message);
            }

            return returnValue;
        }
        private string DelCheckPointPosition(string p_OID)
        {
            Repository m_Repository = new Repository();
            bool success = m_Repository.Delete<EGMNGS.Model.Magnetic_CheckPointTemplates_Childs>(p_OID);
            if (success)
            {
                return "删除成功";
            }
            else
            {
                return "删除失败";
            }
        }

        private string DeleteCheckPointTemplates(string p_OID)
        {
            Repository m_Repository = new Repository();
            bool success = m_Repository.Delete<Magnetic_CheckPointTemplates>(p_OID);
            if (success)
            {
                return "删除成功";
            }
            else
            {
                return "删除失败";
            }
        }

        /// <summary>
        /// 保存测点位置
        /// </summary>
        /// <returns></returns>
        private string SaveCheckPointPosition(EGMNGS.Model.Magnetic_CheckPointTemplates_Childs obj)
        {
            string returnValue = string.Empty;

            Repository m_Repository = new Repository();
            try
            {
                if (obj.OID == 0)
                {
                    obj.CREATED_DATE = DateTime.Now;
                    obj.CREATED_BY = RequestSession.GetSessionUser().UserName.ToString();
                    m_Repository.Insert<Magnetic_CheckPointTemplates_Childs>(obj);
                }
                else
                {
                    obj.LAST_UPD_DATE = DateTime.Now;
                    obj.LAST_UPD_BY = RequestSession.GetSessionUser().UserName.ToString();
                    m_Repository.Update<Magnetic_CheckPointTemplates_Childs>(obj);
                }
                returnValue = "保存成功";
            }
            catch (Exception)
            {
                returnValue = "保存失败";
            }

            return returnValue;
        }

        /// <summary>
        /// 获取测点位置列表
        /// </summary>
        /// <returns></returns>
        private string GetCheckPointPositionList(string p_CheckPointTemplates_OID)
        {
            string returnValue = string.Empty;

            Repository m_Repository = new Repository();

            var list = m_Repository.GetByName<EGMNGS.Model.Magnetic_CheckPointTemplates_Childs>("CheckPointTemplates_OID", p_CheckPointTemplates_OID);
            returnValue = Newtonsoft.Json.JsonConvert.SerializeObject(list);

            return returnValue;
        }

        /// <summary>
        /// 保存模版
        /// </summary>
        /// <returns></returns>
        private string SaveCheckPointTemplates()
        {
            string returnValue = string.Empty;

            EGMNGS.Model.Magnetic_CheckPointTemplates obj = new EGMNGS.Model.Magnetic_CheckPointTemplates();

            obj = RM.Common.DotNetUI.ControlBindHelper.ModelBing<EGMNGS.Model.Magnetic_CheckPointTemplates>(obj);

            Repository m_Repository = new Repository();
            try
            {
                if (obj.OID == 0)
                {
                    obj.CREATED_DATE = DateTime.Now;
                    obj.CREATED_BY = RequestSession.GetSessionUser().UserName.ToString();
                    m_Repository.Insert<Magnetic_CheckPointTemplates>(obj);
                }
                else
                {
                    obj.LAST_UPD_DATE = DateTime.Now;
                    obj.LAST_UPD_BY = RequestSession.GetSessionUser().UserName.ToString();
                    m_Repository.Update<Magnetic_CheckPointTemplates>(obj);
                }
                returnValue = "保存成功";
            }
            catch (Exception)
            {
                returnValue = "保存失败";
            }

            return returnValue;
        }

        /// <summary>
        /// 获取模版列表
        /// </summary>
        /// <returns></returns>
        private string GetCheckPointTemplatesList()
        {
            string returnValue = string.Empty;

            Repository m_Repository = new Repository();
          
            var list = m_Repository.GetAll<Magnetic_CheckPointTemplates>();
            returnValue = Newtonsoft.Json.JsonConvert.SerializeObject(list.OrderBy(o=>o.VoltageLevel));

            return returnValue;
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DapperData;
using EGMNGS.Model;
using RM.Web.App_Code;

namespace RM.Web.MagneticPage
{
    public partial class MonitoringDataDialog : PageBase
    {
        public Magnetic_CheckPointInfo Magnetic_CheckPointInfoObj
        {
            get
            {
                if (Session["Magnetic_CheckPointInfoObj"] == null)
                {
                    Session["Magnetic_CheckPointInfoObj"] = new Magnetic_CheckPointInfo();
                }

                return Session["Magnetic_CheckPointInfoObj"] as Magnetic_CheckPointInfo;
            }
            set { Session["Magnetic_CheckPointInfoObj"] = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                var checkPointCode = Request["OID"] ?? "";
                Repository m_Repository = new Repository();
                Magnetic_CheckPointInfoObj = m_Repository.GetById<Magnetic_CheckPointInfo>(checkPointCode.AsTargetType<int>(0));
            }
        }
    }
}
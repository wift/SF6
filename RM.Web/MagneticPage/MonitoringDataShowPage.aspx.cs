﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DapperData;
using EGMNGS.Common;
using EGMNGS.Model;
using RM.Web.App_Code;

namespace RM.Web.MagneticPage
{
    public partial class MonitoringDataShowPage : PageBase
    {
        public EGMNGS.Model.InfoDevPara InfoDevParaObj
        {
            get
            {
                if (ViewState["InfoDevParaObj"] == null)
                {
                    ViewState["InfoDevParaObj"] = new EGMNGS.Model.InfoDevPara();
                }

                return ViewState["InfoDevParaObj"] as EGMNGS.Model.InfoDevPara;
            }
            set { ViewState["InfoDevParaObj"] = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                DropDownListBinder();
                DataBindGrid();
            }

        }

        private void DropDownListBinder()
        {
            EGMNGS.BLL.SYS_CODE sysCodeBll = new EGMNGS.BLL.SYS_CODE();
            DataSet dsYear = sysCodeBll.GetList("CODE_TYPE='DEVClass' ORDER BY DISPLAY_ORDER");




            DataTable dtPowerStation = ComServies.GetAllPowerStation();
            this.ddlPowerSupplyName.DataSource = dtPowerStation;
            this.ddlPowerSupplyName.DataTextField = "Organization_Name";
            this.ddlPowerSupplyName.DataValueField = "Organization_ID";
            this.ddlPowerSupplyName.DataBind();
            this.ddlPowerSupplyName.Items.Insert(0, string.Empty);

        }


        protected void ddlPowerSupplyName_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadPowerSupplyName();
        }

        private void LoadPowerSupplyName()
        {
            this.ddlConvertStationName.Items.Clear();
            EGMNGS.BLL.InfoPowerSupplyConvertStation bll = new EGMNGS.BLL.InfoPowerSupplyConvertStation();
            DataSet dsPowerSupplyCode = bll.GetList(string.Format("PowerSupplyCode='{0}'", this.ddlPowerSupplyName.SelectedValue));
            this.ddlConvertStationName.DataSource = dsPowerSupplyCode.Tables[0];
            this.ddlConvertStationName.DataTextField = "ConvartStationName";
            this.ddlConvertStationName.DataValueField = "CovnertStationCode";
            this.ddlConvertStationName.DataBind();
            this.ddlConvertStationName.Items.Insert(0, string.Empty);
        }
        /// <summary>
        /// 绑定数据源
        /// </summary>
        private void DataBindGrid()
        {
            string where = string.Empty;
            
            if (ddlConvertStationName.SelectedIndex > 0)
            {
                where += string.Format("and ConvertStationCode='{0}' ", ddlConvertStationName.SelectedValue);
            }
            if(ddlPowerSupplyName.SelectedIndex>0)
            {
                where += string.Format(@"and ConvertStationCode in (SELECT CovnertStationCode
                          FROM InfoPowerSupplyConvertStation
                        WHERE PowerSupplyCode = '{0}') ", ddlPowerSupplyName.SelectedValue);
            }

            Repository m_Repository = new Repository();
            var sql = @"SELECT 
                            *
                        FROM Magnetic_CheckPointInfo  where 1=1 " + where;
            var list = m_Repository.Query<Magnetic_CheckPointInfo>(sql, null)
                        .ToList();

            List<string> listcscode = new List<string>();
            foreach (var item in list)
            {
                listcscode.Add(item.ConvertStationCode);
            }

            this.rp_Item.DataSource = list;
            this.rp_Item.DataBind();

            if (listcscode.Count==0)
            {
                return;
            }

            string covnertStationCodes = string.Empty;
            covnertStationCodes = listcscode.Aggregate("", (current, i) => current + ("'" + i + "',")).Trim(',');

            string sqltemp = string.Format(@"  with t as
            (
            SELECT 	Magnetic_CheckPointInfo_OID,MAX(CollectionDatetime) as CollectionDatetime 
            FROM Magnetic_CheckPointInfoChilds 
            GROUP BY Magnetic_CheckPointInfo_OID
            ),t1 as
            (
            select oid from Magnetic_CheckPointInfoChilds a
            inner join t on t.Magnetic_CheckPointInfo_OID=a.Magnetic_CheckPointInfo_OID 
            and a.CollectionDatetime=t.CollectionDatetime
            )
    
               SELECT *,mcpi.OID as Magnetic_CheckPointInfo_ID,mcpi.CheckPointName  FROM Magnetic_CheckPointInfoChilds mcpic
            LEFT JOIN Magnetic_CheckPointInfo mcpi ON mcpi.OID=mcpic.Magnetic_CheckPointInfo_OID
       inner join t1 on t1.oid=mcpic.oid
          
            WHERE mcpi.ConvertStationCode in ({0})", covnertStationCodes);

            DataTable tempDt = EGMNGS.Common.ComServies.Query(sqltemp).Copy();


            this.Repeater1.DataSource = tempDt;
            this.Repeater1.DataBind();

        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            DataBindGrid();
        }
    }
}
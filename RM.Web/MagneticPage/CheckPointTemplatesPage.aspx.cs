﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using RM.Web.App_Code;

namespace RM.Web.MagneticPage
{

    public partial class CheckPointTemplatesPage : PageBase
    {
        public string RunStatusStrJson = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            List<object> list = new List<object>();

            foreach (var item in DictEGMNS["DYLev"])
            {
                list.Add(new { key=item.Key,value=item.Value});
            }

             RunStatusStrJson = Newtonsoft.Json.JsonConvert.SerializeObject(list);
        }
    }
}
﻿//------------------------------------------------------------------------------
// <自动生成>
//     此代码由工具生成。
//
//     对此文件的更改可能导致不正确的行为，如果
//     重新生成代码，则所做更改将丢失。
// </自动生成>
//------------------------------------------------------------------------------

namespace RM.Web.MagneticPage {
    
    
    public partial class Magnetic_CheckPointTemplatesBDialog {
        
        /// <summary>
        /// form1 控件。
        /// </summary>
        /// <remarks>
        /// 自动生成的字段。
        /// 若要进行修改，请将字段声明从设计器文件移到代码隐藏文件。
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlForm form1;
        
        /// <summary>
        /// ddlTemplateType 控件。
        /// </summary>
        /// <remarks>
        /// 自动生成的字段。
        /// 若要进行修改，请将字段声明从设计器文件移到代码隐藏文件。
        /// </remarks>
        protected global::RM.Web.ExControl.ExDropDownList ddlTemplateType;
        
        /// <summary>
        /// txtMeasuringDate 控件。
        /// </summary>
        /// <remarks>
        /// 自动生成的字段。
        /// 若要进行修改，请将字段声明从设计器文件移到代码隐藏文件。
        /// </remarks>
        protected global::RM.Web.ExControl.ExTextBox txtMeasuringDate;
        
        /// <summary>
        /// ExTextBox1 控件。
        /// </summary>
        /// <remarks>
        /// 自动生成的字段。
        /// 若要进行修改，请将字段声明从设计器文件移到代码隐藏文件。
        /// </remarks>
        protected global::RM.Web.ExControl.ExTextBox ExTextBox1;
        
        /// <summary>
        /// ExTextBox18 控件。
        /// </summary>
        /// <remarks>
        /// 自动生成的字段。
        /// 若要进行修改，请将字段声明从设计器文件移到代码隐藏文件。
        /// </remarks>
        protected global::RM.Web.ExControl.ExTextBox ExTextBox18;
        
        /// <summary>
        /// ExTextBox19 控件。
        /// </summary>
        /// <remarks>
        /// 自动生成的字段。
        /// 若要进行修改，请将字段声明从设计器文件移到代码隐藏文件。
        /// </remarks>
        protected global::RM.Web.ExControl.ExTextBox ExTextBox19;
        
        /// <summary>
        /// ExTextBox20 控件。
        /// </summary>
        /// <remarks>
        /// 自动生成的字段。
        /// 若要进行修改，请将字段声明从设计器文件移到代码隐藏文件。
        /// </remarks>
        protected global::RM.Web.ExControl.ExTextBox ExTextBox20;
        
        /// <summary>
        /// ExTextBox21 控件。
        /// </summary>
        /// <remarks>
        /// 自动生成的字段。
        /// 若要进行修改，请将字段声明从设计器文件移到代码隐藏文件。
        /// </remarks>
        protected global::RM.Web.ExControl.ExTextBox ExTextBox21;
        
        /// <summary>
        /// ExTextBox22 控件。
        /// </summary>
        /// <remarks>
        /// 自动生成的字段。
        /// 若要进行修改，请将字段声明从设计器文件移到代码隐藏文件。
        /// </remarks>
        protected global::RM.Web.ExControl.ExTextBox ExTextBox22;
        
        /// <summary>
        /// ExTextBox23 控件。
        /// </summary>
        /// <remarks>
        /// 自动生成的字段。
        /// 若要进行修改，请将字段声明从设计器文件移到代码隐藏文件。
        /// </remarks>
        protected global::RM.Web.ExControl.ExTextBox ExTextBox23;
        
        /// <summary>
        /// ExTextBox2 控件。
        /// </summary>
        /// <remarks>
        /// 自动生成的字段。
        /// 若要进行修改，请将字段声明从设计器文件移到代码隐藏文件。
        /// </remarks>
        protected global::RM.Web.ExControl.ExTextBox ExTextBox2;
        
        /// <summary>
        /// ExTextBox3 控件。
        /// </summary>
        /// <remarks>
        /// 自动生成的字段。
        /// 若要进行修改，请将字段声明从设计器文件移到代码隐藏文件。
        /// </remarks>
        protected global::RM.Web.ExControl.ExTextBox ExTextBox3;
        
        /// <summary>
        /// ExTextBox4 控件。
        /// </summary>
        /// <remarks>
        /// 自动生成的字段。
        /// 若要进行修改，请将字段声明从设计器文件移到代码隐藏文件。
        /// </remarks>
        protected global::RM.Web.ExControl.ExTextBox ExTextBox4;
        
        /// <summary>
        /// ExTextBox5 控件。
        /// </summary>
        /// <remarks>
        /// 自动生成的字段。
        /// 若要进行修改，请将字段声明从设计器文件移到代码隐藏文件。
        /// </remarks>
        protected global::RM.Web.ExControl.ExTextBox ExTextBox5;
        
        /// <summary>
        /// ExTextBox6 控件。
        /// </summary>
        /// <remarks>
        /// 自动生成的字段。
        /// 若要进行修改，请将字段声明从设计器文件移到代码隐藏文件。
        /// </remarks>
        protected global::RM.Web.ExControl.ExTextBox ExTextBox6;
        
        /// <summary>
        /// ExTextBox7 控件。
        /// </summary>
        /// <remarks>
        /// 自动生成的字段。
        /// 若要进行修改，请将字段声明从设计器文件移到代码隐藏文件。
        /// </remarks>
        protected global::RM.Web.ExControl.ExTextBox ExTextBox7;
        
        /// <summary>
        /// ExTextBox10 控件。
        /// </summary>
        /// <remarks>
        /// 自动生成的字段。
        /// 若要进行修改，请将字段声明从设计器文件移到代码隐藏文件。
        /// </remarks>
        protected global::RM.Web.ExControl.ExTextBox ExTextBox10;
        
        /// <summary>
        /// ExTextBox31 控件。
        /// </summary>
        /// <remarks>
        /// 自动生成的字段。
        /// 若要进行修改，请将字段声明从设计器文件移到代码隐藏文件。
        /// </remarks>
        protected global::RM.Web.ExControl.ExTextBox ExTextBox31;
        
        /// <summary>
        /// ExTextBox32 控件。
        /// </summary>
        /// <remarks>
        /// 自动生成的字段。
        /// 若要进行修改，请将字段声明从设计器文件移到代码隐藏文件。
        /// </remarks>
        protected global::RM.Web.ExControl.ExTextBox ExTextBox32;
        
        /// <summary>
        /// ExTextBox33 控件。
        /// </summary>
        /// <remarks>
        /// 自动生成的字段。
        /// 若要进行修改，请将字段声明从设计器文件移到代码隐藏文件。
        /// </remarks>
        protected global::RM.Web.ExControl.ExTextBox ExTextBox33;
        
        /// <summary>
        /// ExTextBox8 控件。
        /// </summary>
        /// <remarks>
        /// 自动生成的字段。
        /// 若要进行修改，请将字段声明从设计器文件移到代码隐藏文件。
        /// </remarks>
        protected global::RM.Web.ExControl.ExTextBox ExTextBox8;
        
        /// <summary>
        /// ExTextBox9 控件。
        /// </summary>
        /// <remarks>
        /// 自动生成的字段。
        /// 若要进行修改，请将字段声明从设计器文件移到代码隐藏文件。
        /// </remarks>
        protected global::RM.Web.ExControl.ExTextBox ExTextBox9;
        
        /// <summary>
        /// ExTextBox11 控件。
        /// </summary>
        /// <remarks>
        /// 自动生成的字段。
        /// 若要进行修改，请将字段声明从设计器文件移到代码隐藏文件。
        /// </remarks>
        protected global::RM.Web.ExControl.ExTextBox ExTextBox11;
        
        /// <summary>
        /// ExTextBox12 控件。
        /// </summary>
        /// <remarks>
        /// 自动生成的字段。
        /// 若要进行修改，请将字段声明从设计器文件移到代码隐藏文件。
        /// </remarks>
        protected global::RM.Web.ExControl.ExTextBox ExTextBox12;
        
        /// <summary>
        /// ExTextBox13 控件。
        /// </summary>
        /// <remarks>
        /// 自动生成的字段。
        /// 若要进行修改，请将字段声明从设计器文件移到代码隐藏文件。
        /// </remarks>
        protected global::RM.Web.ExControl.ExTextBox ExTextBox13;
        
        /// <summary>
        /// ExTextBox14 控件。
        /// </summary>
        /// <remarks>
        /// 自动生成的字段。
        /// 若要进行修改，请将字段声明从设计器文件移到代码隐藏文件。
        /// </remarks>
        protected global::RM.Web.ExControl.ExTextBox ExTextBox14;
        
        /// <summary>
        /// ExTextBox15 控件。
        /// </summary>
        /// <remarks>
        /// 自动生成的字段。
        /// 若要进行修改，请将字段声明从设计器文件移到代码隐藏文件。
        /// </remarks>
        protected global::RM.Web.ExControl.ExTextBox ExTextBox15;
        
        /// <summary>
        /// ExTextBox16 控件。
        /// </summary>
        /// <remarks>
        /// 自动生成的字段。
        /// 若要进行修改，请将字段声明从设计器文件移到代码隐藏文件。
        /// </remarks>
        protected global::RM.Web.ExControl.ExTextBox ExTextBox16;
        
        /// <summary>
        /// btnSaveLog 控件。
        /// </summary>
        /// <remarks>
        /// 自动生成的字段。
        /// 若要进行修改，请将字段声明从设计器文件移到代码隐藏文件。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button btnSaveLog;
    }
}

﻿using RM.Web.App_Code;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace RM.Web.MagneticPage
{

    public partial class StationCheckPointMNG : Page
    {
        public StringBuilder treeItem_Table = new StringBuilder();
 

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                GetTreeTable();
            }
        }


        // <summary>
        /// 所以变电站设置信息
        /// </summary>
        public void GetTreeTable()
        {
            DataSet dtroot = Maticsoft.DBUtility.DbHelperSQL.Query("select DISTINCT PowerSupplyCode,PowerSupplyName from [EGMNGS].[dbo].[InfoPowerSupplyConvertStation] where IsMagnetic='0'");

            foreach (DataRow drv in dtroot.Tables[0].Rows)
            {
                DataSet dssecond = Maticsoft.DBUtility.DbHelperSQL.Query(string.Format(@"select DISTINCT CovnertStationCode as ConvertStationCode,ConvartStationName as ConvertStationName from [EGMNGS].[dbo].[InfoPowerSupplyConvertStation] where PowerSupplyCode='{0}' and IsMagnetic='0'", drv["PowerSupplyCode"]));

                treeItem_Table.Append("<li>");
                treeItem_Table.Append("<div onclick=\"GetTable('" + drv["PowerSupplyCode"] + "')\">" + drv["PowerSupplyName"] + "</div>");
                if (dssecond.Tables.Count > 0 && dssecond.Tables[0].Rows.Count > 0)
                {
                    treeItem_Table.Append("<ul>");

                    foreach (DataRow item in dssecond.Tables[0].Rows)
                    {
                        treeItem_Table.Append("<li>");
                        treeItem_Table.Append("<div onclick=\"GetTable('" + item["ConvertStationCode"] + "','" + item["ConvertStationName"] + "')\">" + item["ConvertStationName"] + "</div>");

                        treeItem_Table.Append("</li>");
                    }


                    treeItem_Table.Append("</ul>");
                }

                treeItem_Table.Append("</li>");
            }
        }
    }
}
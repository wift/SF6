﻿using EGMNGS.Model;
using RM.Common.DotNetBean;

using RM.Common.DotNetUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DapperData;
using RM.Web.App_Code;

namespace RM.Web.MagneticPage
{
    public partial class Magnetic_CheckPointTemplatesBDialog : PageBase
    {
        public Magnetic_CheckPointTemplatesB Magnetic_CheckPointTemplatesBObj
        {
            get
            {
                if (ViewState["Magnetic_CheckPointTemplatesBObj"] == null)
                {
                    ViewState["Magnetic_CheckPointTemplatesBObj"] = new Magnetic_CheckPointTemplatesB();
                }

                return ViewState["Magnetic_CheckPointTemplatesBObj"] as Magnetic_CheckPointTemplatesB;
            }
            set {
                ViewState["Magnetic_CheckPointTemplatesBObj"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                DapperData.Repository b_Repository = new DapperData.Repository();
                var list = b_Repository.GetAll<Magnetic_CheckPointTemplates>();

                this.ddlTemplateType.DataSource = list;
                this.ddlTemplateType.DataTextField = "TemplateType";
                this.ddlTemplateType.DataValueField = "OID";
                this.ddlTemplateType.DataBind();

                string oid = Request["OID"] ?? "";

                if (string.IsNullOrWhiteSpace(oid))
                {
                     ddlTemplateType.Enabled = true;
                }
                else
                {
                    ddlTemplateType.Enabled = false;
                }

                Magnetic_CheckPointTemplatesBObj = b_Repository.GetById<Magnetic_CheckPointTemplatesB>(oid);
          
            }

        }
        //protected override void OnUnload(EventArgs e)
        //{
        //    Session["Magnetic_CheckPointTemplatesBObj"] = null;
        //    base.OnUnload(e);
        //}
        protected override void OnLoad(EventArgs e)
        {
            CheckURLPermission = true;
            base.OnLoad(e);
        }


        /// <summary>
        /// 保存
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
         
                DapperData.Repository m_Repository = new DapperData.Repository();

                if (Magnetic_CheckPointTemplatesBObj.OID > 0)//修改
                {
                    Magnetic_CheckPointTemplatesBObj.LAST_UPD_BY = User.UserName.ToString();
                    Magnetic_CheckPointTemplatesBObj.LAST_UPD_DATE = DateTime.Now;
                    bool success= m_Repository.Update<Magnetic_CheckPointTemplatesB>(Magnetic_CheckPointTemplatesBObj);
                    if (success==true)
                    {
                        ShowMsgHelper.Alert("修改成功!");
                    }
                }
                else//添加
                {
                    string convertStationCode = Request["convertStationCode"] ?? "";
                    Magnetic_CheckPointTemplatesBObj.ConvertStationCode = convertStationCode;
                    Magnetic_CheckPointTemplatesBObj.CREATED_BY = RequestSession.GetSessionUser().UserName.ToString();
                    Magnetic_CheckPointTemplatesBObj.TemplateType = ddlTemplateType.SelectedItem.Text;
                    Magnetic_CheckPointTemplatesBObj.CREATED_DATE = DateTime.Now;
                    int count = m_Repository.Insert<Magnetic_CheckPointTemplatesB>(Magnetic_CheckPointTemplatesBObj);

                    if (count > 0)
                    {

                        string sqlInsert = string.Format(@"INSERT INTO Magnetic_StationCheckPointMNGChilds
                                                (
	                                                Magnetic_CheckPointTemplatesB_OID,
	                                                CheckPointPosition,
	                                                PositionName,
Height,Remarks
                                                    ,CREATED_BY,CREATED_DATE
                                                )
                                                SELECT {1}
                                                      ,[CheckPointPosition]
                                                      ,[PositionName]
,Height,Remarks
                                                      ,'{2}',GETDATE()
                                                  FROM [EGMNGS].[dbo].[Magnetic_CheckPointTemplates_Childs]
                                                 where    [CheckPointTemplates_OID]={0}", ddlTemplateType.SelectedValue, count, User.UserName);

                        if (m_Repository.Execute(DapperData.SessionFactory.CreateConnection(), sqlInsert) > 0)
                        {
                            ShowMsgHelper.AlertMsgCallBack("添加成功!", "refreshList()");
                        }

                    }

                }
            }
            catch (Exception ex)
            {
                ShowMsgHelper.Alert_Error("添加失败!");
            }

        }
    }
}
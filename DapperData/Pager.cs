﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DapperData
{
    /// <summary>
    /// 分页对象
    /// </summary>
    public class Pager
    {
        /// <summary>
        /// 页索引
        /// </summary>
        public int PageIndex { get; set; }

        /// <summary>
        /// 页大小
        /// </summary>
        public int PageSize { get; set; }

        /// <summary>
        /// 表名称
        /// </summary>
        public string TableName { get; set; }

        /// <summary>
        /// 字段
        /// </summary>
        public string FieldName { get; set; }


        public string strOrderFld { get; set; }

        /// <summary>
        /// 查询条件
        /// </summary>
        public string StrWhere { get; set; }

        ///// <summary>
        ///// 排序字段
        ///// </summary>
        //public string FieldOrder { get; set; }

        /// <summary>
        /// 总记录行数
        /// </summary>
        public int TotalRowsCount { get; set; }

        /// <summary>
        /// 总页数
        /// </summary>
        public int TotalPagesCount { get; set; }
    }
}

﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DapperData
{
    /// <summary>
    /// 服务基类
    /// </summary>
    public class ServiceBase
    {
        /// <summary>
        /// Repository 属性
        /// </summary>
        private IRepository _repository;
        public IRepository DBHelper
        {
            get
            {
                if (_repository == null)
                {
                    _repository = new Repository();
                }
                return _repository;
            }
        }
    }
}

﻿using System;
namespace EGMNGS.Model
{
    /// <summary>
    /// 检测气体明细表
    /// </summary>
    [Serializable]
    public partial class DetailsApplDetectionGas
    {
        public DetailsApplDetectionGas()
        { }
        #region Model
        private int _oid;
        private string _applcode;
        private string _cylindercode;
        private string _gascode;
        private decimal? _amountgas;
        private string _ispass;
        private DateTime? _checkdate;
        private string _status;
        private string produceNum;
        private string manufacturer;

        /// <summary>
        /// 出产编号
        /// </summary>
        public string ProduceNum
        {
            get { return produceNum; }
            set { produceNum = value; }
        }

        /// <summary>
        /// 出产厂家
        /// </summary>
        public string Manufacturer
        {
            get { return manufacturer; }
            set { manufacturer = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int OID
        {
            set { _oid = value; }
            get { return _oid; }
        }
        /// <summary>
        /// 申请编号
        /// </summary>
        public string ApplCode
        {
            set { _applcode = value; }
            get { return _applcode; }
        }
        /// <summary>
        /// 钢瓶编码
        /// </summary>
        public string CylinderCode
        {
            set { _cylindercode = value; }
            get { return _cylindercode; }
        }
        /// <summary>
        /// 气体编码
        /// </summary>
        public string GasCode
        {
            set { _gascode = value; }
            get { return _gascode; }
        }
        /// <summary>
        /// 气量（KG）
        /// </summary>
        public decimal? AmountGas
        {
            set { _amountgas = value; }
            get { return _amountgas; }
        }
        /// <summary>
        /// 是否合格
        /// </summary>
        public string IsPass
        {
            set { _ispass = value; }
            get { return _ispass; }
        }
        /// <summary>
        /// 检测日期
        /// </summary>
        public DateTime? CheckDate
        {
            set { _checkdate = value; }
            get { return _checkdate; }
        }
        /// <summary>
        /// 状态
        /// </summary>
        public string Status
        {
            set { _status = value; }
            get { return _status; }
        }
        #endregion Model

    }
}


﻿using System;
namespace EGMNGS.Model
{
    /// <summary>
    /// 气体入库明细表
    /// </summary>
    [Serializable]
    public partial class DetailsGasStorage
    {
        public DetailsGasStorage()
        { }
        #region Model
        private int _oid;
        private string _gascode;
        private string _cylindercode;
        private string _cylinderstatus;
        private string _ispass;
        private decimal? _amountgas;
        private string _created_by;
        private DateTime _created_date;
        private string _last_upd_by;
        private DateTime _last_upd_date;
        private string _dtGasInStorageCode;

        public string DtGasInStorageCode
        {
            get { return _dtGasInStorageCode; }
            set { _dtGasInStorageCode = value; }
        }
        private string _fillStatus;

        public string FillStatus
        {
            get { return _fillStatus; }
            set { _fillStatus = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int OID
        {
            set { _oid = value; }
            get { return _oid; }
        }
        /// <summary>
        /// 气体编码
        /// </summary>
        public string GasCode
        {
            set { _gascode = value; }
            get { return _gascode; }
        }
        /// <summary>
        /// 钢瓶编码
        /// </summary>
        public string CylinderCode
        {
            set { _cylindercode = value; }
            get { return _cylindercode; }
        }
        /// <summary>
        /// 钢瓶状态
        /// </summary>
        public string CylinderStatus
        {
            set { _cylinderstatus = value; }
            get { return _cylinderstatus; }
        }
        /// <summary>
        /// 是否合格
        /// </summary>
        public string IsPass
        {
            set { _ispass = value; }
            get { return _ispass; }
        }
        /// <summary>
        /// 气量（KG）
        /// </summary>
        public decimal? AmountGas
        {
            set { _amountgas = value; }
            get { return _amountgas; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string CREATED_BY
        {
            set { _created_by = value; }
            get { return _created_by; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime CREATED_DATE
        {
            set { _created_date = value; }
            get { return _created_date; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string LAST_UPD_BY
        {
            set { _last_upd_by = value; }
            get { return _last_upd_by; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime LAST_UPD_DATE
        {
            set { _last_upd_date = value; }
            get { return _last_upd_date; }
        }
        #endregion Model

    }
}


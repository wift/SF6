﻿using System;
using EGMNGS.Common;
namespace EGMNGS.Model
{
    /// <summary>
    /// 登记气体品质检测工具的基础信息。
    /// </summary>
    [Serializable]
    public partial class BookDetectionTools
    {
        public BookDetectionTools()
        {
        }

        public BookDetectionTools(string actionCode)
            : base()
        {
            if (actionCode.Equals(GlobalConstants.ActionModeAdd))
            {
                _toolcode = EGMNGS.Common.ComServies.GetCode("GJ");
                _status = "0";
            }
        }
        #region Model
        private int _oid;
        private string _toolcode;
        private string _tooltype;
        private string _toolname;
        private string _spectype;
        private string _factorycode;
        private DateTime? _effectivedate;
        private string _status;
        private string _registantsoid;
        private DateTime? _registantsdate;
        private string _created_by;
        private DateTime _created_date;
        private string _last_upd_by;
        private DateTime _last_upd_date;
        /// <summary>
        /// 
        /// </summary>
        public int OID
        {
            set { _oid = value; }
            get { return _oid; }
        }
        /// <summary>
        /// 工具编码
        /// </summary>
        public string ToolCode
        {
            set { _toolcode = value; }
            get { return _toolcode; }
        }
        /// <summary>
        /// 色谱仪、露点仪、温湿计、气压计、酸度计、分光光度计、红外含油分析仪工具类别
        /// </summary>
        public string ToolType
        {
            set { _tooltype = value; }
            get { return _tooltype; }
        }
        /// <summary>
        /// 工具名称
        /// </summary>
        public string ToolName
        {
            set { _toolname = value; }
            get { return _toolname; }
        }
        /// <summary>
        /// 规格型号
        /// </summary>
        public string SpecType
        {
            set { _spectype = value; }
            get { return _spectype; }
        }
        /// <summary>
        /// 出厂编码
        /// </summary>
        public string FactoryCode
        {
            set { _factorycode = value; }
            get { return _factorycode; }
        }
        /// <summary>
        /// 有效日期
        /// </summary>
        public DateTime? EffectiveDate
        {
            set { _effectivedate = value; }
            get { return _effectivedate; }
        }
        /// <summary>
        /// 状态
        /// </summary>
        public string Status
        {
            set { _status = value; }
            get { return _status; }
        }
        /// <summary>
        /// 登记人ID
        /// </summary>
        public string RegistantsOID
        {
            set { _registantsoid = value; }
            get { return _registantsoid; }
        }
        /// <summary>
        /// 登记日期
        /// </summary>
        public DateTime? RegistantsDate
        {
            set { _registantsdate = value; }
            get { return _registantsdate; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string CREATED_BY
        {
            set { _created_by = value; }
            get { return _created_by; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime CREATED_DATE
        {
            set { _created_date = value; }
            get { return _created_date; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string LAST_UPD_BY
        {
            set { _last_upd_by = value; }
            get { return _last_upd_by; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime LAST_UPD_DATE
        {
            set { _last_upd_date = value; }
            get { return _last_upd_date; }
        }
        #endregion Model

    }
}


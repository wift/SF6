﻿using System;
namespace EGMNGS.Model
{
	/// <summary>
	/// 登记各供电局的变电站基础信息。
	/// </summary>
	[Serializable]
	public partial class Organization
	{
		public Organization()
		{}
		#region Model
		private int _oid;
		private string _user_grp_id;
		private string _orgcode;
		private string _orgname;
		private string _created_by;
		private DateTime _created_date;
		private string _last_upd_by;
		private DateTime _last_upd_date;
		/// <summary>
		/// 
		/// </summary>
		public int OID
		{
			set{ _oid=value;}
			get{return _oid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string User_Grp_ID
		{
			set{ _user_grp_id=value;}
			get{return _user_grp_id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string ORGCode
		{
			set{ _orgcode=value;}
			get{return _orgcode;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string ORGName
		{
			set{ _orgname=value;}
			get{return _orgname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string CREATED_BY
		{
			set{ _created_by=value;}
			get{return _created_by;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime CREATED_DATE
		{
			set{ _created_date=value;}
			get{return _created_date;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string LAST_UPD_BY
		{
			set{ _last_upd_by=value;}
			get{return _last_upd_by;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime LAST_UPD_DATE
		{
			set{ _last_upd_date=value;}
			get{return _last_upd_date;}
		}
		#endregion Model

	}
}


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EGMNGS.Model
{
    /// <summary>
    /// 测点信息名
    /// </summary>
    ///     
    /// 

    [Table("Magnetic_CheckPointInfo")]
    public class Magnetic_CheckPointInfo
    {
        #region Model
        private int _oid;
        private string _checkpointcode;
        private string _checkpointname;
        private string _instrumentcode;
        private string _instrumentname;
        private string _runstatus;
        private string _checkpointpositionname;
        private decimal? _longitude;
        private decimal? _latitude;
        private string _convertstationcode;
        private string _convertstationname;
        private string _created_by;
        private DateTime? _created_date;
        private string _last_upd_by;
        private DateTime? _last_upd_date;
        private string _voltagelevel;
        private decimal? _electricity;
        private decimal? _eload;
        private string _remarks;



        /// <summary>
        /// 
        /// </summary>
        public int OID
        {
            set { _oid = value; }
            get { return _oid; }
        }


        private int _sortNo;

        /// <summary>
        /// 序号
        /// </summary>
        public int SortNo
        {
            get { return _sortNo; }
            set { _sortNo = value; }
        }

        /// <summary>
        /// 测点编码
        /// </summary>
        public string CheckPointCode
        {
            set { _checkpointcode = value; }
            get { return _checkpointcode; }
        }
        /// <summary>
        /// 测点名称
        /// </summary>
        public string CheckPointName
        {
            set { _checkpointname = value; }
            get { return _checkpointname; }
        }
        /// <summary>
        /// 仪器编码
        /// </summary>
        public string InstrumentCode
        {
            set { _instrumentcode = value; }
            get { return _instrumentcode; }
        }
        /// <summary>
        /// 仪器名称
        /// </summary>
        public string InstrumentName
        {
            set { _instrumentname = value; }
            get { return _instrumentname; }
        }
        /// <summary>
        /// 运行状态
        /// </summary>
        public string RunStatus
        {
            set { _runstatus = value; }
            get { return _runstatus; }
        }
        /// <summary>
        /// 测点位置
        /// </summary>
        public string CheckPointPositionName
        {
            set { _checkpointpositionname = value; }
            get { return _checkpointpositionname; }
        }
        /// <summary>
        /// 经度
        /// </summary>
        public decimal? Longitude
        {
            set { _longitude = value; }
            get { return _longitude; }
        }
        /// <summary>
        /// 纬度
        /// </summary>
        public decimal? Latitude
        {
            set { _latitude = value; }
            get { return _latitude; }
        }
        /// <summary>
        /// 所属变电站编码
        /// </summary>
        public string ConvertStationCode
        {
            set { _convertstationcode = value; }
            get { return _convertstationcode; }
        }
        /// <summary>
        /// 所属变电站
        /// </summary>
        public string ConvertStationName
        {
            set { _convertstationname = value; }
            get { return _convertstationname; }
        }
        /// <summary>
        /// 创建人
        /// </summary>
        public string CREATED_BY
        {
            set { _created_by = value; }
            get { return _created_by; }
        }
        /// <summary>
        /// 创建日期
        /// </summary>
        public DateTime? CREATED_DATE
        {
            set { _created_date = value; }
            get { return _created_date; }
        }
        /// <summary>
        /// 最后更新人
        /// </summary>
        public string LAST_UPD_BY
        {
            set { _last_upd_by = value; }
            get { return _last_upd_by; }
        }
        /// <summary>
        /// 最后更新日期
        /// </summary>
        public DateTime? LAST_UPD_DATE
        {
            set { _last_upd_date = value; }
            get { return _last_upd_date; }
        }

        /// <summary>
        /// 电压等级
        /// </summary>
        public string VoltageLevel
        {
            set { _voltagelevel = value; }
            get { return _voltagelevel; }
        }
        /// <summary>
        /// 电流
        /// </summary>
        public decimal? Electricity
        {
            set { _electricity = value; }
            get { return _electricity; }
        }
        /// <summary>
        /// 负荷
        /// </summary>
        public decimal? Eload
        {
            set { _eload = value; }
            get { return _eload; }
        }
        /// <summary>
        /// 备注
        /// </summary>
        public string Remarks
        {
            set { _remarks = value; }
            get { return _remarks; }
        }
        #endregion Model

    }
}

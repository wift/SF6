﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EGMNGS.Model
{
    [Table("Magnetic_StationCheckPointMNGChilds")]
    public class Magnetic_StationCheckPointMNGChilds
    {
        #region Model
        private int _oid;
        private int? _magnetic_checkpointtemplatesb_oid;
        private string _checkpointposition;
        private string _positionname;
        private double _height;
        private double _electricfieldmax;
        private double _electricfieldmin;
        private double _electricfieldrms;
        private double _magneticinductionmax;
        private double _magneticinductionmin;
        private double _magneticinductionrms;
        private string _remarks;
        private string _created_by;
        private DateTime? _created_date;
        private string _last_upd_by;
        private DateTime? _last_upd_date;
        /// <summary>
        /// 
        /// </summary>
        public int OID
        {
            set { _oid = value; }
            get { return _oid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? Magnetic_CheckPointTemplatesB_OID
        {
            set { _magnetic_checkpointtemplatesb_oid = value; }
            get { return _magnetic_checkpointtemplatesb_oid; }
        }
        /// <summary>
        /// 测点位置
        /// </summary>
        public string CheckPointPosition
        {
            set { _checkpointposition = value; }
            get { return _checkpointposition; }
        }
        /// <summary>
        /// 位置名称
        /// </summary>
        public string PositionName
        {
            set { _positionname = value; }
            get { return _positionname; }
        }
        /// <summary>
        /// 高度
        /// </summary>
        public double Height
        {
            set { _height = value; }
            get { return _height; }
        }
        /// <summary>
        /// 电场强度MAX
        /// </summary>
        public double ElectricFieldMAX
        {
            set { _electricfieldmax = value; }
            get { return _electricfieldmax; }
        }
        /// <summary>
        /// 电场强度MIN
        /// </summary>
        public double ElectricFieldMIN
        {
            set { _electricfieldmin = value; }
            get { return _electricfieldmin; }
        }
        /// <summary>
        /// 电场强度RMS
        /// </summary>
        public double ElectricFieldRMS
        {
            set { _electricfieldrms = value; }
            get { return _electricfieldrms; }
        }
        /// <summary>
        /// 磁感应强度MAX
        /// </summary>
        public double MagneticInductionMAX
        {
            set { _magneticinductionmax = value; }
            get { return _magneticinductionmax; }
        }
        /// <summary>
        /// 磁感应强度MAX
        /// </summary>
        public double MagneticInductionMIN
        {
            set { _magneticinductionmin = value; }
            get { return _magneticinductionmin; }
        }
        /// <summary>
        /// 磁感应强度RMS
        /// </summary>
        public double MagneticInductionRMS
        {
            set { _magneticinductionrms = value; }
            get { return _magneticinductionrms; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Remarks
        {
            set { _remarks = value; }
            get { return _remarks; }
        }
        /// <summary>
        /// 创建人
        /// </summary>
        public string CREATED_BY
        {
            set { _created_by = value; }
            get { return _created_by; }
        }
        /// <summary>
        /// 创建日期
        /// </summary>
        public DateTime? CREATED_DATE
        {
            set { _created_date = value; }
            get { return _created_date; }
        }
        /// <summary>
        /// 最后更新人
        /// </summary>
        public string LAST_UPD_BY
        {
            set { _last_upd_by = value; }
            get { return _last_upd_by; }
        }
        /// <summary>
        /// 最后更新日期
        /// </summary>
        public DateTime? LAST_UPD_DATE
        {
            set { _last_upd_date = value; }
            get { return _last_upd_date; }
        }
        #endregion Model
    }
}

﻿using System;
namespace EGMNGS.Model
{
	/// <summary>
	/// 电气常量数据
	/// </summary>
	[Serializable]
	public partial class OCInstrumentElectricalData
	{
		public OCInstrumentElectricalData()
		{}
		#region Model
		private int _oid;
		private string _checkpointcode;
		private string _checkpointname;
		private DateTime? _collectiondatetime;
		private decimal? _pressure;
		private decimal? _temperture;
		private decimal? _outelectricity;
		private decimal? _currentval;
		private decimal? _voltage;
		private decimal? _powerval;
		private string _collectiontype;
		private string _collectionby;
		/// <summary>
		/// 
		/// </summary>
		public int OID
		{
			set{ _oid=value;}
			get{return _oid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string CheckPointCode
		{
			set{ _checkpointcode=value;}
			get{return _checkpointcode;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string CheckPointName
		{
			set{ _checkpointname=value;}
			get{return _checkpointname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? CollectionDatetime
		{
			set{ _collectiondatetime=value;}
			get{return _collectiondatetime;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? Pressure
		{
			set{ _pressure=value;}
			get{return _pressure;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? Temperture
		{
			set{ _temperture=value;}
			get{return _temperture;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? OutElectricity
		{
			set{ _outelectricity=value;}
			get{return _outelectricity;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? CurrentVal
		{
			set{ _currentval=value;}
			get{return _currentval;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? Voltage
		{
			set{ _voltage=value;}
			get{return _voltage;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? PowerVal
		{
			set{ _powerval=value;}
			get{return _powerval;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string CollectionType
		{
			set{ _collectiontype=value;}
			get{return _collectiontype;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string CollectionBy
		{
			set{ _collectionby=value;}
			get{return _collectionby;}
		}
		#endregion Model

	}
}


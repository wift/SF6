﻿using System;
namespace EGMNGS.Model
{
	/// <summary>
	/// 采购员登记新采购气体信息、钢瓶信息及气体编码，并提交新采购
	/// </summary>
	[Serializable]
	public partial class SYS_PARA
	{
		public SYS_PARA()
		{}
		#region Model
		private string _para_code;
		private string _para_type;
		private string _para_desc;
		private string _para_value;
		private string _created_by;
		private DateTime _created_date;
		private string _last_upd_by;
		private DateTime _last_upd_date;
		/// <summary>
		/// 
		/// </summary>
		public string PARA_CODE
		{
			set{ _para_code=value;}
			get{return _para_code;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string PARA_TYPE
		{
			set{ _para_type=value;}
			get{return _para_type;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string PARA_DESC
		{
			set{ _para_desc=value;}
			get{return _para_desc;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string PARA_VALUE
		{
			set{ _para_value=value;}
			get{return _para_value;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string CREATED_BY
		{
			set{ _created_by=value;}
			get{return _created_by;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime CREATED_DATE
		{
			set{ _created_date=value;}
			get{return _created_date;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string LAST_UPD_BY
		{
			set{ _last_upd_by=value;}
			get{return _last_upd_by;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime LAST_UPD_DATE
		{
			set{ _last_upd_date=value;}
			get{return _last_upd_date;}
		}
		#endregion Model

	}
}


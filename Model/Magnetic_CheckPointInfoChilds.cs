﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EGMNGS.Model
{
    [Table("Magnetic_CheckPointInfoChilds")]
    public class Magnetic_CheckPointInfoChilds
    {
        #region Model
        private int _oid;
        private int? _magnetic_checkpointinfo_oid;
        private DateTime? _collectiondatetime;
        private string _collectiontype;
        private string _collectionby;
        private decimal? _electricfieldmax;
        private decimal? _electricfieldmin;
        private decimal? _electricfieldrms;
        private decimal? _magneticinductionmax;
        private decimal? _magneticinductionmin;
        private decimal? _magneticinductionrms;
        private decimal? _temperature;
        private decimal? _humidity;
        /// <summary>
        /// 
        /// </summary>
        public int OID
        {
            set { _oid = value; }
            get { return _oid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? Magnetic_CheckPointInfo_OID
        {
            set { _magnetic_checkpointinfo_oid = value; }
            get { return _magnetic_checkpointinfo_oid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? CollectionDatetime
        {
            set { _collectiondatetime = value; }
            get { return _collectiondatetime; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string CollectionType
        {
            set { _collectiontype = value; }
            get { return _collectiontype; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string CollectionBy
        {
            set { _collectionby = value; }
            get { return _collectionby; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? ElectricFieldMAX
        {
            set { _electricfieldmax = value; }
            get { return _electricfieldmax; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? ElectricFieldMIN
        {
            set { _electricfieldmin = value; }
            get { return _electricfieldmin; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? ElectricFieldRMS
        {
            set { _electricfieldrms = value; }
            get { return _electricfieldrms; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? MagneticInductionMAX
        {
            set { _magneticinductionmax = value; }
            get { return _magneticinductionmax; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? MagneticInductionMIN
        {
            set { _magneticinductionmin = value; }
            get { return _magneticinductionmin; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? MagneticInductionRMS
        {
            set { _magneticinductionrms = value; }
            get { return _magneticinductionrms; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? Temperature
        {
            set { _temperature = value; }
            get { return _temperature; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? Humidity
        {
            set { _humidity = value; }
            get { return _humidity; }
        }
        #endregion Model

    }
}

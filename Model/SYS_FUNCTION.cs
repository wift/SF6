﻿using System;
namespace EGMNGS.Model
{
	/// <summary>
	/// 采购员登记新采购气体信息、钢瓶信息及气体编码，并提交新采购
	/// </summary>
	[Serializable]
	public partial class SYS_FUNCTION
	{
		public SYS_FUNCTION()
		{}
		#region Model
		private string _func_id;
		private string _func_desc;
		private string _parent_func_id;
		private string _page_source;
		private string _created_by;
		private DateTime _created_date;
		private string _last_upd_by;
		private DateTime _last_upd_date;
		/// <summary>
		/// 
		/// </summary>
		public string FUNC_ID
		{
			set{ _func_id=value;}
			get{return _func_id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string FUNC_DESC
		{
			set{ _func_desc=value;}
			get{return _func_desc;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string PARENT_FUNC_ID
		{
			set{ _parent_func_id=value;}
			get{return _parent_func_id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string PAGE_SOURCE
		{
			set{ _page_source=value;}
			get{return _page_source;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string CREATED_BY
		{
			set{ _created_by=value;}
			get{return _created_by;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime CREATED_DATE
		{
			set{ _created_date=value;}
			get{return _created_date;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string LAST_UPD_BY
		{
			set{ _last_upd_by=value;}
			get{return _last_upd_by;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime LAST_UPD_DATE
		{
			set{ _last_upd_date=value;}
			get{return _last_upd_date;}
		}
		#endregion Model

	}
}


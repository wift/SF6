﻿using System;
namespace EGMNGS.Model
{
    /// <summary>
    /// 采购气体明细表
    /// </summary>
    [Serializable]
    public partial class DetailsGasProcurement
    {
        public DetailsGasProcurement()
        { }
        #region Model
        private int _oid;
        private string _reggasprocurement_code;
        private string _cylindercode;
        private string _gascode;
        private decimal? _amountgas;
        private string _ispass;
        private DateTime? _checkdate;
        private string _status;
        private string _created_by;
        private DateTime? _created_date;
        private string _last_upd_by;
        private DateTime? _last_upd_date;
        /// <summary>
        /// 
        /// </summary>
        public int OID
        {
            set { _oid = value; }
            get { return _oid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string RegGasProcurement_Code
        {
            set { _reggasprocurement_code = value; }
            get { return _reggasprocurement_code; }
        }
        /// <summary>
        /// 钢瓶编码
        /// </summary>
        public string CylinderCode
        {
            set { _cylindercode = value; }
            get { return _cylindercode; }
        }
        /// <summary>
        /// 气体编码
        /// </summary>
        public string GasCode
        {
            set { _gascode = value; }
            get { return _gascode; }
        }
        /// <summary>
        /// 气量（KG）
        /// </summary>
        public decimal? AmountGas
        {
            set { _amountgas = value; }
            get { return _amountgas; }
        }
        /// <summary>
        /// 是否合格
        /// </summary>
        public string IsPass
        {
            set { _ispass = value; }
            get { return _ispass; }
        }
        /// <summary>
        /// 检测日期
        /// </summary>
        public DateTime? CheckDate
        {
            set { _checkdate = value; }
            get { return _checkdate; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Status
        {
            set { _status = value; }
            get { return _status; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string CREATED_BY
        {
            set { _created_by = value; }
            get { return _created_by; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? CREATED_DATE
        {
            set { _created_date = value; }
            get { return _created_date; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string LAST_UPD_BY
        {
            set { _last_upd_by = value; }
            get { return _last_upd_by; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? LAST_UPD_DATE
        {
            set { _last_upd_date = value; }
            get { return _last_upd_date; }
        }
        #endregion Model

    }
}


﻿using System;
namespace EGMNGS.Model
{
	/// <summary>
	/// 查看3种气体的实时库存数量和历史库存数量等信息，每月进行收发存对账，生成月度库存台账。
	/// </summary>
	[Serializable]
	public partial class ButtonTB
	{
		public ButtonTB()
		{}
		#region Model
		private int _pkid;
		private string _btnvalue;
		private string _btnname;
		/// <summary>
		/// 
		/// </summary>
		public int PKID
		{
			set{ _pkid=value;}
			get{return _pkid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string BtnValue
		{
			set{ _btnvalue=value;}
			get{return _btnvalue;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string BtnName
		{
			set{ _btnname=value;}
			get{return _btnname;}
		}
		#endregion Model

	}
}


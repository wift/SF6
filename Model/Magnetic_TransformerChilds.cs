﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EGMNGS.Model
{
    [Table("Magnetic_TransformerChilds")]
   public class Magnetic_TransformerChilds
    {
        #region Model
        private int _oid;
        private int? _transformer_oid;
        private string _checkpointposition;
        private decimal? _leqdba;
        private decimal? _leqdbz;
        private decimal? _l63h;
        private decimal? _l125h;
        private decimal? _l250h;
        private decimal? _l500h;
        private decimal? _l1kh;
        private decimal? _l2kh;
        private decimal? _l4kh;
        private decimal? _l8kh;
        private string _remarks;
        private string _created_by;
        private DateTime? _created_date;
        private string _last_upd_by;
        private DateTime? _last_upd_date;
        /// <summary>
        /// 
        /// </summary>
        public int OID
        {
            set { _oid = value; }
            get { return _oid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? Transformer_OID
        {
            set { _transformer_oid = value; }
            get { return _transformer_oid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string CheckPointPosition
        {
            set { _checkpointposition = value; }
            get { return _checkpointposition; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? LeqdBA
        {
            set { _leqdba = value; }
            get { return _leqdba; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? LeqdBZ
        {
            set { _leqdbz = value; }
            get { return _leqdbz; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? L63H
        {
            set { _l63h = value; }
            get { return _l63h; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? L125H
        {
            set { _l125h = value; }
            get { return _l125h; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? L250H
        {
            set { _l250h = value; }
            get { return _l250h; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? L500H
        {
            set { _l500h = value; }
            get { return _l500h; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? L1KH
        {
            set { _l1kh = value; }
            get { return _l1kh; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? L2KH
        {
            set { _l2kh = value; }
            get { return _l2kh; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? L4KH
        {
            set { _l4kh = value; }
            get { return _l4kh; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? L8KH
        {
            set { _l8kh = value; }
            get { return _l8kh; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Remarks
        {
            set { _remarks = value; }
            get { return _remarks; }
        }
        /// <summary>
        /// 创建人
        /// </summary>
        public string CREATED_BY
        {
            set { _created_by = value; }
            get { return _created_by; }
        }
        /// <summary>
        /// 创建日期
        /// </summary>
        public DateTime? CREATED_DATE
        {
            set { _created_date = value; }
            get { return _created_date; }
        }
        /// <summary>
        /// 最后更新人
        /// </summary>
        public string LAST_UPD_BY
        {
            set { _last_upd_by = value; }
            get { return _last_upd_by; }
        }
        /// <summary>
        /// 最后更新日期
        /// </summary>
        public DateTime? LAST_UPD_DATE
        {
            set { _last_upd_date = value; }
            get { return _last_upd_date; }
        }
        #endregion Model
    }
}

﻿using System;
namespace EGMNGS.Model
{
	/// <summary>
	/// 采购员登记新采购气体信息、钢瓶信息及气体编码，并提交新采购
	/// </summary>
	[Serializable]
	public partial class SYS_CODE_TYPE
	{
		public SYS_CODE_TYPE()
		{}
		#region Model
		private string _code_type;
		private string _code_type_desc;
		private decimal? _display_order;
		private string _allow_insert_ind;
		private string _allow_subcode_ind;
		/// <summary>
		/// 
		/// </summary>
		public string CODE_TYPE
		{
			set{ _code_type=value;}
			get{return _code_type;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string CODE_TYPE_DESC
		{
			set{ _code_type_desc=value;}
			get{return _code_type_desc;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? DISPLAY_ORDER
		{
			set{ _display_order=value;}
			get{return _display_order;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string ALLOW_INSERT_IND
		{
			set{ _allow_insert_ind=value;}
			get{return _allow_insert_ind;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string ALLOW_SUBCODE_IND
		{
			set{ _allow_subcode_ind=value;}
			get{return _allow_subcode_ind;}
		}
		#endregion Model

	}
}


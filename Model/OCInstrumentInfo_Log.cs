﻿using System;
namespace EGMNGS.Model
{
	/// <summary>
	/// OCInstrumentInfo_Log:日志表
	/// </summary>
	[Serializable]
	public partial class OCInstrumentInfo_Log
	{
		public OCInstrumentInfo_Log()
		{}
		#region Model
		private int _oid;
		private string _filedname;
		private string _newvalue;
		private string _oldvalue;
		private string _updateby;
		private DateTime? _updatedate;
		/// <summary>
		/// 主键
		/// </summary>
		public int OID
		{
			set{ _oid=value;}
			get{return _oid;}
		}
		/// <summary>
		/// 字段
		/// </summary>
		public string FiledName
		{
			set{ _filedname=value;}
			get{return _filedname;}
		}
		/// <summary>
		/// 当前值
		/// </summary>
		public string NewValue
		{
			set{ _newvalue=value;}
			get{return _newvalue;}
		}
		/// <summary>
		/// 旧值
		/// </summary>
		public string OldValue
		{
			set{ _oldvalue=value;}
			get{return _oldvalue;}
		}
		/// <summary>
		/// 修改人
		/// </summary>
		public string Updateby
		{
			set{ _updateby=value;}
			get{return _updateby;}
		}
		/// <summary>
		/// 修改时间
		/// </summary>
		public DateTime? UpdateDate
		{
			set{ _updatedate=value;}
			get{return _updatedate;}
		}
		#endregion Model

	}
}


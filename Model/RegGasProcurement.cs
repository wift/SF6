﻿using System;
using EGMNGS.Common;
namespace EGMNGS.Model
{
    /// <summary>
    /// 采购员登记新采购气体信息、钢瓶信息及气体编码，并提交新采购
    /// </summary>
    [Serializable]
    public partial class RegGasProcurement
    {
        public RegGasProcurement()
        { }
        public RegGasProcurement(string actionCode)
            : base()
        {
            if (actionCode.Equals(GlobalConstants.ActionModeAdd))
            {
                _code = ComServies.GetCode("CG", DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString("D2"));
            }
        }
        #region Model
        private int _oid;
        private string _code;
        private string _year = DateTime.Now.Year.ToString();
        private string _month = DateTime.Now.Month.ToString();
        private string _suppliername;
        private string _manufacturername;
        private decimal? _amountpurchased;
        private string _purchaseddesc;
        private string _contactsname;
        private string _officetel;
        private string _phonenum;
        private string _status;
        private string _purchasedid;
        private DateTime? _purchaseddate;
        private string _registrantoid;
        private DateTime? _registrantdate;
        private string _created_by;
        private DateTime _created_date;
        private string _last_upd_by;
        private DateTime _last_upd_date;
        /// <summary>
        /// 
        /// </summary>
        public int OID
        {
            set { _oid = value; }
            get { return _oid; }
        }
        /// <summary>
        /// 采购编号
        /// </summary>
        public string Code
        {
            set { _code = value; }
            get { return _code; }
        }
        /// <summary>
        /// 年度
        /// </summary>
        public string Year
        {
            set { _year = value; }
            get { return _year; }
        }
        /// <summary>
        /// 月度
        /// </summary>
        public string Month
        {
            set { _month = value; }
            get { return _month; }
        }
        /// <summary>
        /// 供应商
        /// </summary>
        public string SupplierName
        {
            set { _suppliername = value; }
            get { return _suppliername; }
        }
        /// <summary>
        /// 生产厂家
        /// </summary>
        public string ManufacturerName
        {
            set { _manufacturername = value; }
            get { return _manufacturername; }
        }
        /// <summary>
        /// 采购量（KG）
        /// </summary>
        public decimal? AmountPurchased
        {
            set { _amountpurchased = value; }
            get { return _amountpurchased; }
        }
        /// <summary>
        /// 采购说明
        /// </summary>
        public string PurchasedDesc
        {
            set { _purchaseddesc = value; }
            get { return _purchaseddesc; }
        }
        /// <summary>
        /// 联系人
        /// </summary>
        public string ContactsName
        {
            set { _contactsname = value; }
            get { return _contactsname; }
        }
        /// <summary>
        /// 办公电话
        /// </summary>
        public string OfficeTel
        {
            set { _officetel = value; }
            get { return _officetel; }
        }
        /// <summary>
        /// 手机电话
        /// </summary>
        public string PhoneNum
        {
            set { _phonenum = value; }
            get { return _phonenum; }
        }
        /// <summary>
        /// 状态
        /// </summary>
        public string Status
        {
            set { _status = value; }
            get { return _status; }
        }
        /// <summary>
        /// 采购人ID
        /// </summary>
        public string PurchasedID
        {
            set { _purchasedid = value; }
            get { return _purchasedid; }
        }
        /// <summary>
        /// 采购日期
        /// </summary>
        public DateTime? PurchasedDate
        {
            set { _purchaseddate = value; }
            get { return _purchaseddate; }
        }
        /// <summary>
        /// 登记人ID
        /// </summary>
        public string RegistrantOID
        {
            set { _registrantoid = value; }
            get { return _registrantoid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? RegistrantDate
        {
            set { _registrantdate = value; }
            get { return _registrantdate; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string CREATED_BY
        {
            set { _created_by = value; }
            get { return _created_by; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime CREATED_DATE
        {
            set { _created_date = value; }
            get { return _created_date; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string LAST_UPD_BY
        {
            set { _last_upd_by = value; }
            get { return _last_upd_by; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime LAST_UPD_DATE
        {
            set { _last_upd_date = value; }
            get { return _last_upd_date; }
        }
        #endregion Model

    }
}


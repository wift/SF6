﻿using System;
namespace EGMNGS.Model
{
	/// <summary>
	/// 采购员登记新采购气体信息、钢瓶信息及气体编码，并提交新采购
	/// </summary>
	[Serializable]
	public partial class SYS_USER
	{
		public SYS_USER()
		{}
		#region Model
		private string _user_id;
		private string _user_pwd;
		private string _user_name;
		private string _email;
		private string _post;
		private string _user_status;
		private string _suspend_reason;
		private DateTime? _suspend_date;
		private DateTime? _last_signon;
		private int? _fail_count;
		private DateTime? _pwd_renewal_date;
		private string _org_code;
		private string _created_by;
		private DateTime _created_date;
		private string _last_upd_by;
		private DateTime _last_upd_date;
		/// <summary>
		/// 
		/// </summary>
		public string USER_ID
		{
			set{ _user_id=value;}
			get{return _user_id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string USER_PWD
		{
			set{ _user_pwd=value;}
			get{return _user_pwd;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string USER_NAME
		{
			set{ _user_name=value;}
			get{return _user_name;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string EMAIL
		{
			set{ _email=value;}
			get{return _email;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string POST
		{
			set{ _post=value;}
			get{return _post;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string USER_STATUS
		{
			set{ _user_status=value;}
			get{return _user_status;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string SUSPEND_REASON
		{
			set{ _suspend_reason=value;}
			get{return _suspend_reason;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? SUSPEND_DATE
		{
			set{ _suspend_date=value;}
			get{return _suspend_date;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? LAST_SIGNON
		{
			set{ _last_signon=value;}
			get{return _last_signon;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? FAIL_COUNT
		{
			set{ _fail_count=value;}
			get{return _fail_count;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? PWD_RENEWAL_DATE
		{
			set{ _pwd_renewal_date=value;}
			get{return _pwd_renewal_date;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Org_Code
		{
			set{ _org_code=value;}
			get{return _org_code;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string CREATED_BY
		{
			set{ _created_by=value;}
			get{return _created_by;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime CREATED_DATE
		{
			set{ _created_date=value;}
			get{return _created_date;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string LAST_UPD_BY
		{
			set{ _last_upd_by=value;}
			get{return _last_upd_by;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime LAST_UPD_DATE
		{
			set{ _last_upd_date=value;}
			get{return _last_upd_date;}
		}
		#endregion Model

	}
}


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EGMNGS.Model
{
    [Table("FileInfo")]
    public class FileInfo
    {
        #region Model
        private int _oid = 0;
        private string _exportinfo_oid;
        private string _filename;
        private string _filetype;
        private string _fileclass;
        private string _registrantname;
        private string _filepath;
        private DateTime? _createdate = DateTime.Now;
        private int _FileSize;
        /// <summary>
        /// OID
        /// </summary>
        public int OID
        {
            set { _oid = value; }
            get { return _oid; }
        }
        /// <summary>
        /// 模板ID
        /// </summary>
        public string ExportInfo_OID
        {
            set { _exportinfo_oid = value; }
            get { return _exportinfo_oid; }
        }
        /// <summary>
        /// 文件名
        /// </summary>
        public string FileName
        {
            set { _filename = value; }
            get { return _filename; }
        }
        /// <summary>
        /// 文件类型
        /// </summary>
        public string FileType
        {
            set { _filetype = value; }
            get { return _filetype; }
        }
        /// <summary>
        /// 文件类别
        /// </summary>
        public string FileClass
        {
            set { _fileclass = value; }
            get { return _fileclass; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string RegistrantName
        {
            set { _registrantname = value; }
            get { return _registrantname; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string FilePath
        {
            set { _filepath = value; }
            get { return _filepath; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? CreateDate
        {
            set { _createdate = value; }
            get { return _createdate; }
        }

        public int FileSize
        {
            get
            {
                return _FileSize;
            }

            set
            {
                _FileSize = value;
            }
        }
        #endregion Model
    }
}

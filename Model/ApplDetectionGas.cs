﻿using System;
using EGMNGS.Common;
using System.Reflection;
namespace EGMNGS.Model
{
    /// <summary>
    /// 登记新投产气体品质检测申请。
    /// </summary>
    [Serializable]
    public partial class ApplDetectionGas
    {
        public ApplDetectionGas()
        {

        }
        public ApplDetectionGas(string actionCode)
            : base()
        {
            if (actionCode.Equals(GlobalConstants.ActionModeAdd))
            {
                _applcode = EGMNGS.Common.ComServies.GetCode("SQ", DateTime.Now.Year.ToString() +

DateTime.Now.Month.ToString("D2"));
            }
        }
        #region Model
        private int _oid;
        private string _applcode;
        private string _devcode;
        private string _powersupplycode;
        private string _powersupplyname;
        private string _convertstationcode;
        private string _convertstationname;
        private string _devname;
        private string _devno;
        private string _inspectionunit;
        private string _contacts;
        private string _officetel;
        private string _phonenum;
        private string _sampliinspituation;
        private decimal? _amountdetection = 0;
        private string _status;
        private string _registantsoid;
        private DateTime? _registantsdate;
        private string _created_by;
        private DateTime _created_date;
        private string _last_upd_by;
        private DateTime _last_upd_date;
        private string _checkPJType;

        /// <summary>
        /// 检测项目类型
        /// </summary>
        public string CheckPJType
        {
            get { return _checkPJType; }
            set { _checkPJType = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int OID
        {
            set { _oid = value; }
            get { return _oid; }
        }
        /// <summary>
        /// 申请编号
        /// </summary>
        public string ApplCode
        {
            set { _applcode = value; }
            get { return _applcode; }
        }
        /// <summary>
        /// 设备编码
        /// </summary>
        public string DevCode
        {
            set { _devcode = value; }
            get { return _devcode; }
        }
        /// <summary>
        /// 供电局编码
        /// </summary>
        public string PowerSupplyCode
        {
            set { _powersupplycode = value; }
            get { return _powersupplycode; }
        }
        /// <summary>
        /// 供电局名称
        /// </summary>
        public string PowerSupplyName
        {
            set { _powersupplyname = value; }
            get { return _powersupplyname; }
        }
        /// <summary>
        /// 变电站编码
        /// </summary>
        public string ConvertStationCode
        {
            set { _convertstationcode = value; }
            get { return _convertstationcode; }
        }
        /// <summary>
        /// 变电站名称
        /// </summary>
        public string convertStationName
        {
            set { _convertstationname = value; }
            get { return _convertstationname; }
        }
        /// <summary>
        /// 设备名称
        /// </summary>
        public string DevName
        {
            set { _devname = value; }
            get { return _devname; }
        }
        /// <summary>
        /// 设备型号
        /// </summary>
        public string DevNo
        {
            set { _devno = value; }
            get { return _devno; }
        }
        /// <summary>
        /// 送检单位
        /// </summary>
        public string InspectionUnit
        {
            set { _inspectionunit = value; }
            get { return _inspectionunit; }
        }
        /// <summary>
        /// 联系人
        /// </summary>
        public string Contacts
        {
            set { _contacts = value; }
            get { return _contacts; }
        }
        /// <summary>
        /// 办公电话--通信地址
        /// </summary>
        public string OfficeTel
        {
            set { _officetel = value; }
            get { return _officetel; }
        }
        /// <summary>
        /// 手机电话
        /// </summary>
        public string PhoneNum
        {
            set { _phonenum = value; }
            get { return _phonenum; }
        }
        /// <summary>
        /// 抽检情况--工程名称
        /// </summary>
        public string SampliInspituation
        {
            set { _sampliinspituation = value; }
            get { return _sampliinspituation; }
        }
        /// <summary>
        /// 检测气量（KG）
        /// </summary>
        public decimal? AmountDetection
        {
            set { _amountdetection = value; }
            get { return _amountdetection; }
        }
        /// <summary>
        /// 状态
        /// </summary>
        public string Status
        {
            set { _status = value; }
            get { return _status; }
        }
        /// <summary>
        /// 登记人OID
        /// </summary>
        public string RegistantsOID
        {
            set { _registantsoid = value; }
            get { return _registantsoid; }
        }
        /// <summary>
        /// 登记日期
        /// </summary>
        public DateTime? RegistantsDate
        {
            set { _registantsdate = value; }
            get { return _registantsdate; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string CREATED_BY
        {
            set { _created_by = value; }
            get { return _created_by; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime CREATED_DATE
        {
            set { _created_date = value; }
            get { return _created_date; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string LAST_UPD_BY
        {
            set { _last_upd_by = value; }
            get { return _last_upd_by; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime LAST_UPD_DATE
        {
            set { _last_upd_date = value; }
            get { return _last_upd_date; }
        }
        #endregion Model

    }


   
}


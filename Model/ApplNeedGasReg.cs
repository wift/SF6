﻿using System;
using EGMNGS.Common;
namespace EGMNGS.Model
{
    /// <summary>
    /// 供电局用户在线录入设备检修补气、换气需气量计划申请，并提交。
    /// </summary>
    [Serializable]
    public partial class ApplNeedGasReg
    {
        public ApplNeedGasReg()
        {
        }
        public ApplNeedGasReg(string actionCode)
            : base()
        {
            if (actionCode.Equals(GlobalConstants.ActionModeAdd))
            {
                _gascode = ComServies.GetCode("XQ", DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString("D2"));
            }
        }
        #region Model
        private int _oid;
        private string _gascode;
        private string _year = DateTime.Now.Year.ToString();
        private string _month = DateTime.Now.Month.ToString();
        private string _powersupplycode;
        private string _powersupplyname;
        private string _convertstationcode;
        private string _convertstationname;
        private int? _airdemand;
        private string _qtydesc;
        private string _applidesc;
        private string _contacts;
        private string _officetel;
        private string _phonenum;
        private string _receiptaddress;
        private string _status;
        private string _registrantoid;
        private DateTime? _registrantdate;
        private string _auditoroid;
        private DateTime? _auditsdate;
        private string _created_by;
        private DateTime _created_date;
        private string _last_upd_by;
        private DateTime _last_upd_date;
        private DateTime _useDate;

        /// <summary>
        /// 需用日期
        /// </summary>
        public DateTime UseDate
        {
            get { return _useDate; }
            set { _useDate = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int OID
        {
            set { _oid = value; }
            get { return _oid; }
        }
        /// <summary>
        /// 需气编号
        /// </summary>
        public string GasCode
        {
            set { _gascode = value; }
            get { return _gascode; }
        }
        /// <summary>
        /// 年度
        /// </summary>
        public string Year
        {
            set { _year = value; }
            get { return _year; }
        }
        /// <summary>
        /// 月度
        /// </summary>
        public string Month
        {
            set { _month = value; }
            get { return _month; }
        }
        /// <summary>
        /// 供电局编码
        /// </summary>
        public string PowerSupplyCode
        {
            set { _powersupplycode = value; }
            get { return _powersupplycode; }
        }
        /// <summary>
        /// 供电局名称
        /// </summary>
        public string PowerSupplyName
        {
            set { _powersupplyname = value; }
            get { return _powersupplyname; }
        }
        /// <summary>
        /// 变电站编码
        /// </summary>
        public string ConvertStationCode
        {
            set { _convertstationcode = value; }
            get { return _convertstationcode; }
        }
        /// <summary>
        /// 变电站名称
        /// </summary>
        public string ConvertStationName
        {
            set { _convertstationname = value; }
            get { return _convertstationname; }
        }
        /// <summary>
        /// 需气量（KG）
        /// </summary>
        public int? AirDemand
        {
            set { _airdemand = value; }
            get { return _airdemand; }
        }
        /// <summary>
        /// 数量说明
        /// </summary>
        public string QtyDesc
        {
            set { _qtydesc = value; }
            get { return _qtydesc; }
        }
        /// <summary>
        /// 用途说明
        /// </summary>
        public string AppliDesc
        {
            set { _applidesc = value; }
            get { return _applidesc; }
        }
        /// <summary>
        /// 联系人
        /// </summary>
        public string Contacts
        {
            set { _contacts = value; }
            get { return _contacts; }
        }
        /// <summary>
        /// 办公电话
        /// </summary>
        public string OfficeTel
        {
            set { _officetel = value; }
            get { return _officetel; }
        }
        /// <summary>
        /// 手机电话
        /// </summary>
        public string PhoneNum
        {
            set { _phonenum = value; }
            get { return _phonenum; }
        }
        /// <summary>
        /// 收货地点
        /// </summary>
        public string ReceiptAddress
        {
            set { _receiptaddress = value; }
            get { return _receiptaddress; }
        }
        /// <summary>
        /// 状态
        /// </summary>
        public string Status
        {
            set { _status = value; }
            get { return _status; }
        }
        /// <summary>
        /// 登记人ID
        /// </summary>
        public string RegistrantOID
        {
            set { _registrantoid = value; }
            get { return _registrantoid; }
        }
        /// <summary>
        /// 登记日期
        /// </summary>
        public DateTime? RegistrantDate
        {
            set { _registrantdate = value; }
            get { return _registrantdate; }
        }
        /// <summary>
        /// 审核人OID
        /// </summary>
        public string AuditorOID
        {
            set { _auditoroid = value; }
            get { return _auditoroid; }
        }
        /// <summary>
        /// 审核日期
        /// </summary>
        public DateTime? AuditsDate
        {
            set { _auditsdate = value; }
            get { return _auditsdate; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string CREATED_BY
        {
            set { _created_by = value; }
            get { return _created_by; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime CREATED_DATE
        {
            set { _created_date = value; }
            get { return _created_date; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string LAST_UPD_BY
        {
            set { _last_upd_by = value; }
            get { return _last_upd_by; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime LAST_UPD_DATE
        {
            set { _last_upd_date = value; }
            get { return _last_upd_date; }
        }
        #endregion Model

    }
}


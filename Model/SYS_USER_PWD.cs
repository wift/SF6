﻿using System;
namespace EGMNGS.Model
{
	/// <summary>
	/// 采购员登记新采购气体信息、钢瓶信息及气体编码，并提交新采购
	/// </summary>
	[Serializable]
	public partial class SYS_USER_PWD
	{
		public SYS_USER_PWD()
		{}
		#region Model
		private string _user_id;
		private DateTime _expired_date;
		private string _user_pwd;
		/// <summary>
		/// 
		/// </summary>
		public string USER_ID
		{
			set{ _user_id=value;}
			get{return _user_id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime EXPIRED_DATE
		{
			set{ _expired_date=value;}
			get{return _expired_date;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string USER_PWD
		{
			set{ _user_pwd=value;}
			get{return _user_pwd;}
		}
		#endregion Model

	}
}


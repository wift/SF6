﻿using System;
namespace EGMNGS.Model
{
	/// <summary>
	/// 预警比例
	/// </summary>
	[Serializable]
	public partial class OCInstrumentInfo_Childs
	{
		public OCInstrumentInfo_Childs()
		{}
		#region Model
		private int _oid;
		private string _checkpointcode;
		private string _toptarget;
		private string _bottomtarget;
		private decimal? _maxvalue;
		/// <summary>
		/// 主键
		/// </summary>
		public int OID
		{
			set{ _oid=value;}
			get{return _oid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string CheckPointCode
		{
			set{ _checkpointcode=value;}
			get{return _checkpointcode;}
		}
		/// <summary>
		/// 分子
		/// </summary>
		public string TopTarget
		{
			set{ _toptarget=value;}
			get{return _toptarget;}
		}
		/// <summary>
		/// 分母
		/// </summary>
		public string BottomTarget
		{
			set{ _bottomtarget=value;}
			get{return _bottomtarget;}
		}
		/// <summary>
		/// 预警值
		/// </summary>
		public decimal? MaxValue
		{
			set{ _maxvalue=value;}
			get{return _maxvalue;}
		}
		#endregion Model

	}
}


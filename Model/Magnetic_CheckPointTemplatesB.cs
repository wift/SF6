﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EGMNGS.Model
{

    [Serializable]
    [Table("Magnetic_CheckPointTemplatesB")]
   
    public class Magnetic_CheckPointTemplatesB 
    {
        #region Model
        private int _oid;
        private string _convertstationcode;
        private int _magnetic_checkpointtemplates_oid;
        private string _templatetype;
        private DateTime? _measuringdate;
        private string _measuringtime;
        private decimal? _temperature;
        private decimal? _humidity;
        private decimal? _baro;
        private decimal? _electricity;
        private string _winddirection;
        private decimal? _voltage;
        private decimal? _windspeed;
        private string _created_by;
        private DateTime? _created_date;
        private string _last_upd_by;
        private DateTime? _last_upd_date;
        private DateTime? _measuringdate2;
        private DateTime? _measuringdate3;
        private string _measuringtime2;
        private string _measuringtime3;
        private decimal? _baro2;
        private decimal? _baro3;
        private decimal? _temperature2;
        private decimal? _temperature3;
        private decimal? _humidity2;
        private decimal? _humidity3;
        private decimal? _voltage2;
        private decimal? _voltage3;
        private string _masterinfo;
        private string _masterinfo2;
        private string _masterinfo3;
        private decimal? _electricity2;
        private decimal? _electricity3;
        [Ignore]
        public string Magnetic_CheckPointTemplates_OIDString
        {
            set { _magnetic_checkpointtemplates_oid = Convert.ToInt32(value); }
            get { return _magnetic_checkpointtemplates_oid.ToString(); }
        }
        /// <summary>
        /// 
        /// </summary>
        public int OID
        {
            set { _oid = value; }
            get { return _oid; }
        }
        /// <summary>
        /// 站名
        /// </summary>
        public string ConvertStationCode
        {
            set { _convertstationcode = value; }
            get { return _convertstationcode; }
        }
        /// <summary>
        /// 站名类型
        /// </summary>
        public int Magnetic_CheckPointTemplates_OID
        {
            set { _magnetic_checkpointtemplates_oid = value; }
            get { return _magnetic_checkpointtemplates_oid; }
        }
        /// <summary>
        /// 模版类型
        /// </summary>
        public string TemplateType
        {
            set { _templatetype = value; }
            get { return _templatetype; }
        }
        /// <summary>
        /// 测量日期
        /// </summary>
        public DateTime? MeasuringDate
        {
            set { _measuringdate = value; }
            get { return _measuringdate; }
        }
        /// <summary>
        /// 测量时间
        /// </summary>
        public string MeasuringTime
        {
            set { _measuringtime = value; }
            get { return _measuringtime; }
        }
        /// <summary>
        /// 环境温度
        /// </summary>
        public decimal? Temperature
        {
            set { _temperature = value; }
            get { return _temperature; }
        }
        /// <summary>
        /// 环境湿度
        /// </summary>
        public decimal? Humidity
        {
            set { _humidity = value; }
            get { return _humidity; }
        }
        /// <summary>
        /// 大气压力
        /// </summary>
        public decimal? BARO
        {
            set { _baro = value; }
            get { return _baro; }
        }
        /// <summary>
        /// 电流
        /// </summary>
        public decimal? Electricity
        {
            set { _electricity = value; }
            get { return _electricity; }
        }
        /// <summary>
        /// 风向
        /// </summary>
        public string WindDirection
        {
            set { _winddirection = value; }
            get { return _winddirection; }
        }
        /// <summary>
        /// 电压
        /// </summary>
        public decimal? voltage
        {
            set { _voltage = value; }
            get { return _voltage; }
        }
        /// <summary>
        /// 风速
        /// </summary>
        public decimal? WindSpeed
        {
            set { _windspeed = value; }
            get { return _windspeed; }
        }
        /// <summary>
        /// 创建人
        /// </summary>
        public string CREATED_BY
        {
            set { _created_by = value; }
            get { return _created_by; }
        }
        /// <summary>
        /// 创建日期
        /// </summary>
        public DateTime? CREATED_DATE
        {
            set { _created_date = value; }
            get { return _created_date; }
        }
        /// <summary>
        /// 最后更新人
        /// </summary>
        public string LAST_UPD_BY
        {
            set { _last_upd_by = value; }
            get { return _last_upd_by; }
        }
        /// <summary>
        /// 最后更新日期
        /// </summary>
        public DateTime? LAST_UPD_DATE
        {
            set { _last_upd_date = value; }
            get { return _last_upd_date; }
        }


        /// <summary>
        /// 
        /// </summary>
        public DateTime? MeasuringDate2
        {
            set { _measuringdate2 = value; }
            get { return _measuringdate2; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? MeasuringDate3
        {
            set { _measuringdate3 = value; }
            get { return _measuringdate3; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string MeasuringTime2
        {
            set { _measuringtime2 = value; }
            get { return _measuringtime2; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string MeasuringTime3
        {
            set { _measuringtime3 = value; }
            get { return _measuringtime3; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? BARO2
        {
            set { _baro2 = value; }
            get { return _baro2; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? BARO3
        {
            set { _baro3 = value; }
            get { return _baro3; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? Temperature2
        {
            set { _temperature2 = value; }
            get { return _temperature2; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? Temperature3
        {
            set { _temperature3 = value; }
            get { return _temperature3; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? Humidity2
        {
            set { _humidity2 = value; }
            get { return _humidity2; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? Humidity3
        {
            set { _humidity3 = value; }
            get { return _humidity3; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? voltage2
        {
            set { _voltage2 = value; }
            get { return _voltage2; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? voltage3
        {
            set { _voltage3 = value; }
            get { return _voltage3; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string MasterInfo
        {
            set { _masterinfo = value; }
            get { return _masterinfo; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string MasterInfo2
        {
            set { _masterinfo2 = value; }
            get { return _masterinfo2; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string MasterInfo3
        {
            set { _masterinfo3 = value; }
            get { return _masterinfo3; }
        }

        public decimal? Electricity2
        {
            get
            {
                return _electricity2;
            }

            set
            {
                _electricity2 = value;
            }
        }

        public decimal? Electricity3
        {
            get
            {
                return _electricity3;
            }

            set
            {
                _electricity3 = value;
            }
        }
        #endregion Model
    }
}

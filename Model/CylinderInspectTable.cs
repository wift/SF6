﻿using System;
namespace EGMNGS.Model
{
    /// <summary>
    /// 登记钢瓶送检记录。
    /// </summary>
    [Serializable]
    public partial class CylinderInspectTable
    {
        public CylinderInspectTable()
        { }
        #region Model
        private int _oid;
        private string _cylindercode;
        private DateTime? _originaleffectivedate;
        private DateTime? _neweffectivedate;
        private string _ispass;
        private string _remarks;
        private string _status;
        private string _sendcheckoid;
        private DateTime? _sendcheckdate;
        private string _registantsoid;
        private DateTime? _registantsdate;
        private string _created_by;
        private DateTime _created_date;
        private string _last_upd_by;
        private DateTime _last_upd_date;
        /// <summary>
        /// 
        /// </summary>
        public int OID
        {
            set { _oid = value; }
            get { return _oid; }
        }
        /// <summary>
        /// 钢瓶编码
        /// </summary>
        public string CylinderCode
        {
            set { _cylindercode = value; }
            get { return _cylindercode; }
        }
        /// <summary>
        /// 原有效日期
        /// </summary>
        public DateTime? OriginalEffectiveDate
        {
            set { _originaleffectivedate = value; }
            get { return _originaleffectivedate; }
        }
        /// <summary>
        /// 新有效日期
        /// </summary>
        public DateTime? NewEffectiveDate
        {
            set { _neweffectivedate = value; }
            get { return _neweffectivedate; }
        }
        /// <summary>
        /// 是否合格
        /// </summary>
        public string IsPass
        {
            set { _ispass = value; }
            get { return _ispass; }
        }
        /// <summary>
        /// 备注说明
        /// </summary>
        public string Remarks
        {
            set { _remarks = value; }
            get { return _remarks; }
        }
        /// <summary>
        /// 状态
        /// </summary>
        public string Status
        {
            set { _status = value; }
            get { return _status; }
        }
        /// <summary>
        /// 送检人ID
        /// </summary>
        public string SendCheckOID
        {
            set { _sendcheckoid = value; }
            get { return _sendcheckoid; }
        }
        /// <summary>
        /// 送检日期
        /// </summary>
        public DateTime? SendCheckDate
        {
            set { _sendcheckdate = value; }
            get { return _sendcheckdate; }
        }
        /// <summary>
        /// 登记人ID
        /// </summary>
        public string RegistantsOID
        {
            set { _registantsoid = value; }
            get { return _registantsoid; }
        }
        /// <summary>
        /// 登记日期
        /// </summary>
        public DateTime? RegistantsDate
        {
            set { _registantsdate = value; }
            get { return _registantsdate; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string CREATED_BY
        {
            set { _created_by = value; }
            get { return _created_by; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime CREATED_DATE
        {
            set { _created_date = value; }
            get { return _created_date; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string LAST_UPD_BY
        {
            set { _last_upd_by = value; }
            get { return _last_upd_by; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime LAST_UPD_DATE
        {
            set { _last_upd_date = value; }
            get { return _last_upd_date; }
        }
        #endregion Model

    }
}


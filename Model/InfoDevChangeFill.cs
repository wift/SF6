﻿using System;
namespace EGMNGS.Model
{
	/// <summary>
	/// 登记供电局下属变电站的设备检修补气、换气记录。
	/// </summary>
	[Serializable]
    public partial class InfoDevChangeFill
    {
        public InfoDevChangeFill()
        { }
        #region Model
        private int _oid;
        private string _devcode;
        private string _devname;
        private string _devtype;
        private decimal? _ratedql;
        private decimal? _ratedqy;
        private DateTime? _fillgasdate;
        private string _operataff;
        private string _gasusing;
        private decimal? _amountfillgas;
        private decimal? _amountpressfillgassbefore;
        private decimal? _amountpressfillgassafter;
        private string _remarksdesc;
        private string _cylindercode;
        private string _gascode;
        private string _status;
        private string _registantsoid;
        private DateTime? _registantsdate;
        private string _powersupplycode;
        private string _powersupplyname;
        private string _convertstationcode;
        private string _convertstationname;
        /// <summary>
        /// 
        /// </summary>
        public int OID
        {
            set { _oid = value; }
            get { return _oid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string DevCode
        {
            set { _devcode = value; }
            get { return _devcode; }
        }
        /// <summary>
        /// 设备名称
        /// </summary>
        public string DevName
        {
            set { _devname = value; }
            get { return _devname; }
        }
        /// <summary>
        /// 设备型号
        /// </summary>
        public string DevType
        {
            set { _devtype = value; }
            get { return _devtype; }
        }
        /// <summary>
        /// 额定气量（KG）
        /// </summary>
        public decimal? RatedQL
        {
            set { _ratedql = value; }
            get { return _ratedql; }
        }
        /// <summary>
        /// 额定气压（Mpa）
        /// </summary>
        public decimal? RatedQY
        {
            set { _ratedqy = value; }
            get { return _ratedqy; }
        }
        /// <summary>
        /// 补换气日期
        /// </summary>
        public DateTime? FillGasdate
        {
            set { _fillgasdate = value; }
            get { return _fillgasdate; }
        }
        /// <summary>
        /// 操作人员
        /// </summary>
        public string Operataff
        {
            set { _operataff = value; }
            get { return _operataff; }
        }
        /// <summary>
        /// 气体用途
        /// </summary>
        public string GasUsing
        {
            set { _gasusing = value; }
            get { return _gasusing; }
        }
        /// <summary>
        /// 补换气量（KG）
        /// </summary>
        public decimal? AmountFillGas
        {
            set { _amountfillgas = value; }
            get { return _amountfillgas; }
        }
        /// <summary>
        /// 补换气前压力(Mpa)
        /// </summary>
        public decimal? AmountPressFillGassbefore
        {
            set { _amountpressfillgassbefore = value; }
            get { return _amountpressfillgassbefore; }
        }
        /// <summary>
        /// 补换气后压力(Mpa)
        /// </summary>
        public decimal? AmountPressFillGassafter
        {
            set { _amountpressfillgassafter = value; }
            get { return _amountpressfillgassafter; }
        }
        /// <summary>
        /// 备注说明
        /// </summary>
        public string RemarksDesc
        {
            set { _remarksdesc = value; }
            get { return _remarksdesc; }
        }
        /// <summary>
        /// 钢瓶编码
        /// </summary>
        public string CylinderCode
        {
            set { _cylindercode = value; }
            get { return _cylindercode; }
        }
        /// <summary>
        /// 气体编码
        /// </summary>
        public string GasCode
        {
            set { _gascode = value; }
            get { return _gascode; }
        }
        /// <summary>
        /// 状态
        /// </summary>
        public string Status
        {
            set { _status = value; }
            get { return _status; }
        }
        /// <summary>
        /// 登记人ID
        /// </summary>
        public string RegistantsOID
        {
            set { _registantsoid = value; }
            get { return _registantsoid; }
        }
        /// <summary>
        /// 登记日期
        /// </summary>
        public DateTime? RegistantsDate
        {
            set { _registantsdate = value; }
            get { return _registantsdate; }
        }
        /// <summary>
        /// 供电局编码
        /// </summary>
        public string PowerSupplyCode
        {
            set { _powersupplycode = value; }
            get { return _powersupplycode; }
        }
        /// <summary>
        /// 供电局名称
        /// </summary>
        public string PowerSupplyName
        {
            set { _powersupplyname = value; }
            get { return _powersupplyname; }
        }
        /// <summary>
        /// 变电站编码
        /// </summary>
        public string ConvertStationCode
        {
            set { _convertstationcode = value; }
            get { return _convertstationcode; }
        }
        /// <summary>
        /// 变电站名称
        /// </summary>
        public string ConvertStationName
        {
            set { _convertstationname = value; }
            get { return _convertstationname; }
        }
        #endregion Model

    }
}


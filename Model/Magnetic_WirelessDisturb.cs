﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EGMNGS.Model
{
    [Table("Magnetic_WirelessDisturb")]
   public class Magnetic_WirelessDisturb
    {

        #region Model
        private int _oid;
        private decimal? _w0p15;
        private decimal? _w0p25;
        private decimal? _wp5;
        private decimal? _W1;
        private decimal? _w1p5;
        private decimal? _w3;
        private decimal? _w6;
        private decimal? _w10;
        private decimal? _w15;
        private decimal? _w30;
        private string _created_by;
        private DateTime? _created_date;
        private string _last_upd_by;
        private DateTime? _last_upd_date;
        private int _Distance;
        private int _Magnetic_CheckPointTemplatesB_OID;


        /// <summary>
        /// OID
        /// </summary>
        public int OID
        {
            set { _oid = value; }
            get { return _oid; }
        }
        public int Magnetic_CheckPointTemplatesB_OID
        {
            get { return _Magnetic_CheckPointTemplatesB_OID; }
            set { _Magnetic_CheckPointTemplatesB_OID = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? W0p15
        {
            set { _w0p15 = value; }
            get { return _w0p15; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? W0p25
        {
            set { _w0p25 = value; }
            get { return _w0p25; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? Wp5
        {
            set { _wp5 = value; }
            get { return _wp5; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? W1
        {
            set { _W1 = value; }
            get { return _W1; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? W1p5
        {
            set { _w1p5 = value; }
            get { return _w1p5; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? W3
        {
            set { _w3 = value; }
            get { return _w3; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? W6
        {
            set { _w6 = value; }
            get { return _w6; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? W10
        {
            set { _w10 = value; }
            get { return _w10; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? W15
        {
            set { _w15 = value; }
            get { return _w15; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? W30
        {
            set { _w30 = value; }
            get { return _w30; }
        }
        /// <summary>
        /// 创建人
        /// </summary>
        public string CREATED_BY
        {
            set { _created_by = value; }
            get { return _created_by; }
        }
        /// <summary>
        /// 创建日期
        /// </summary>
        public DateTime? CREATED_DATE
        {
            set { _created_date = value; }
            get { return _created_date; }
        }
        /// <summary>
        /// 最后更新人
        /// </summary>
        public string LAST_UPD_BY
        {
            set { _last_upd_by = value; }
            get { return _last_upd_by; }
        }
        /// <summary>
        /// 最后更新日期
        /// </summary>
        public DateTime? LAST_UPD_DATE
        {
            set { _last_upd_date = value; }
            get { return _last_upd_date; }
        }

        public int Distance
        {
            get
            {
                return _Distance;
            }

            set
            {
                _Distance = value;
            }
        }
        #endregion Model


    }
}

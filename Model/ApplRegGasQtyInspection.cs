﻿using System;
using EGMNGS.Common;
namespace EGMNGS.Model
{
    /// <summary>
    /// 气体品质检测申请登记表
    /// </summary>
    [Serializable]
    public partial class ApplRegGasQtyInspection
    {
        public ApplRegGasQtyInspection()
        {

        }
        public ApplRegGasQtyInspection(string actionCode)
            : base()
        {
            if (actionCode.Equals(GlobalConstants.ActionModeAdd))
            {
                _checkcode = EGMNGS.Common.ComServies.GetCode("JC", DateTime.Now.Year.ToString() +

DateTime.Now.Month.ToString("D2"));

                _reportcode = DateTime.Now.Year + EGMNGS.Common.ComServies.GetCode4("X");
            }
        }
        #region Model
        private int _oid;
        private string _checkcode;
        private string _reportcode;
        private string _businesscode;
        private string _gascdoe;
        private string _cylindercode;
        private string _gassourse;
        private decimal? _c;
        private decimal? _rh;
        private decimal? _kpa;
        private string _spyid;
        private string _ldyid;
        private string _wsjid;
        private string _qyjid;
        private string _sdjid;
        private string _fggdjid;
        private string _hwhyfxyid;
        private string _sf6index = "≥99.9";
        private decimal? _sf6result;
        private string _airqualityscoreindex = "≤0.03";
        private decimal? _airqualityscoreresult;
        private string _cf4index = "≤0.01";
        private decimal? _cf4result;
        private string _waterqualityscoreindex = "≤0.0005";
        private decimal? _waterqualityscoreresult;
        private string _waterdewindex = "≤-49.7";
        private decimal? _waterdewresult;
        private string _hfindex = "≤0.00002";
        private decimal? _hfresult;
        private string _ksjhfindex = "≤0.0001";
        private decimal? _ksjhfresult;
        private string _kwysorceindex = "≤0.0004";
        private decimal? _kwysorceresult;
        private string _conclusion;
        private string _ispass;
        private string _apploid;
        private DateTime? _appldate;
        private string _checkoid;
        private DateTime? _checkdate;
        private string _signeroid;
        private DateTime? _signerdate;
        private string _approveroid;
        private DateTime? _approverdate;
        private string _flowstatus;
        private bool _isprint;
        private string _mtid;
        private decimal? sfhl;
        private decimal? _monoxide;
        private decimal? _C2F6;
        private decimal? _C3F8;
        private decimal? _standardVolume;

        public decimal? StandardVolume
        {
            get
            {
                return _standardVolume;
            }

            set
            {
                _standardVolume = value;
            }
        }
        /// <summary>
        /// 六氟乙烷(C2F6)质量分数
        /// </summary>
        public decimal? C2F6
        {
            get
            {
                return _C2F6;
            }

            set
            {
                _C2F6 = value;
            }
        }
        /// <summary>
        /// 八氟丙烷(C3F8)质量分数
        /// </summary>
        public decimal? C3F8
        {
            get
            {
                return _C3F8;
            }

            set
            {
                _C3F8 = value;
            }
        }

        /// <summary>
        /// 一氧化碳
        /// </summary>
        public decimal? Monoxide
        {
            get { return _monoxide; }
            set { _monoxide = value; }
        }
        private decimal? _hydrothion;
        /// <summary>
        /// 硫化氢
        /// </summary>
        public decimal? Hydrothion
        {
            get { return _hydrothion; }
            set { _hydrothion = value; }
        }
        private decimal? _sulfurDioxide;
        /// <summary>
        /// 二氧化硫
        /// </summary>
        public decimal? SulfurDioxide
        {
            get { return _sulfurDioxide; }
            set { _sulfurDioxide = value; }
        }

        //水分
        public decimal? Sfhl
        {
            get { return sfhl; }
            set { sfhl = value; }
        }

        private string _checkPJType;

        /// <summary>
        /// 检测项目类型
        /// </summary>
        public string CheckPJType
        {
            get { return _checkPJType; }
            set { _checkPJType = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int OID
        {
            set { _oid = value; }
            get { return _oid; }
        }
        /// <summary>
        /// 检测单号
        /// </summary>
        public string CheckCode
        {
            set { _checkcode = value; }
            get { return _checkcode; }
        }
        /// <summary>
        /// 报告编号
        /// </summary>
        public string ReportCode
        {
            set { _reportcode = value; }
            get { return _reportcode; }
        }
        /// <summary>
        /// 业务单号
        /// </summary>
        public string BusinessCode
        {
            set { _businesscode = value; }
            get { return _businesscode; }
        }
        /// <summary>
        /// 气体编码
        /// </summary>
        public string GasCdoe
        {
            set { _gascdoe = value; }
            get { return _gascdoe; }
        }
        /// <summary>
        /// 钢瓶编码
        /// </summary>
        public string CylinderCode
        {
            set { _cylindercode = value; }
            get { return _cylindercode; }
        }
        /// <summary>
        /// 气体来源
        /// </summary>
        public string GasSourse
        {
            set { _gassourse = value; }
            get { return _gassourse; }
        }
        /// <summary>
        /// 环境温度（℃）
        /// </summary>
        public decimal? C
        {
            set { _c = value; }
            get { return _c; }
        }
        /// <summary>
        /// 环境湿度（%RH）
        /// </summary>
        public decimal? RH
        {
            set { _rh = value; }
            get { return _rh; }
        }
        /// <summary>
        /// 大气压（kPa）
        /// </summary>
        public decimal? KPA
        {
            set { _kpa = value; }
            get { return _kpa; }
        }
        /// <summary>
        /// 色谱仪ID
        /// </summary>
        public string SPYID
        {
            set { _spyid = value; }
            get { return _spyid; }
        }
        /// <summary>
        /// 露点仪ID
        /// </summary>
        public string LDYID
        {
            set { _ldyid = value; }
            get { return _ldyid; }
        }
        /// <summary>
        /// 温湿计ID
        /// </summary>
        public string WSJID
        {
            set { _wsjid = value; }
            get { return _wsjid; }
        }
        /// <summary>
        /// 气压计ID
        /// </summary>
        public string QYJID
        {
            set { _qyjid = value; }
            get { return _qyjid; }
        }
        /// <summary>
        /// 酸度计ID
        /// </summary>
        public string SDJID
        {
            set { _sdjid = value; }
            get { return _sdjid; }
        }
        /// <summary>
        /// 分光光度计ID
        /// </summary>
        public string FGGDJID
        {
            set { _fggdjid = value; }
            get { return _fggdjid; }
        }
        /// <summary>
        /// 红外含油分析仪ID
        /// </summary>
        public string HWHYFXYID
        {
            set { _hwhyfxyid = value; }
            get { return _hwhyfxyid; }
        }
        /// <summary>
        /// 六氟化硫(SF6)的质量分数指标（/%）
        /// </summary>
        public string SF6Index
        {
            set { _sf6index = value; }
            get { return _sf6index; }
        }
        /// <summary>
        /// 六氟化硫(SF6) 的质量分数结果（/%）
        /// </summary>
        public decimal? SF6Result
        {
            set { _sf6result = value; }
            get { return _sf6result; }
        }
        /// <summary>
        /// 空气的质量分数指标（/%）
        /// </summary>
        public string AirQualityScoreIndex
        {
            set { _airqualityscoreindex = value; }
            get { return _airqualityscoreindex; }
        }
        /// <summary>
        /// 空气的质量分数结果（/%）
        /// </summary>
        public decimal? AirQualityScoreResult
        {
            set { _airqualityscoreresult = value; }
            get { return _airqualityscoreresult; }
        }
        /// <summary>
        /// 四氟化碳(CF4) 的质量分数指标（/%）
        /// </summary>
        public string CF4Index
        {
            set { _cf4index = value; }
            get { return _cf4index; }
        }
        /// <summary>
        /// 四氟化碳(CF4) 的质量分数结果（/%）
        /// </summary>
        public decimal? CF4Result
        {
            set { _cf4result = value; }
            get { return _cf4result; }
        }
        /// <summary>
        /// 水的质量分数指标（/%）
        /// </summary>
        public string WaterQualityScoreIndex
        {
            set { _waterqualityscoreindex = value; }
            get { return _waterqualityscoreindex; }
        }
        /// <summary>
        /// 水的质量分数结果（/%）
        /// </summary>
        public decimal? WaterQualityScoreResult
        {
            set { _waterqualityscoreresult = value; }
            get { return _waterqualityscoreresult; }
        }
        /// <summary>
        /// 水的露点指标（/℃）
        /// </summary>
        public string WaterDewIndex
        {
            set { _waterdewindex = value; }
            get { return _waterdewindex; }
        }
        /// <summary>
        /// 水的露点结果（/℃）
        /// </summary>
        public decimal? WaterDewResult
        {
            set { _waterdewresult = value; }
            get { return _waterdewresult; }
        }
        /// <summary>
        /// 酸度 (以HF计 )  的质量分数指标（/%）
        /// </summary>
        public string HFIndex
        {
            set { _hfindex = value; }
            get { return _hfindex; }
        }
        /// <summary>
        /// 酸度 (以HF计)  的质量分数结果（%）
        /// </summary>
        public decimal? HFResult
        {
            set { _hfresult = value; }
            get { return _hfresult; }
        }
        /// <summary>
        /// 可水解氟化物(以HF计)的质量分数指标（%）
        /// </summary>
        public string KSJHFIndex
        {
            set { _ksjhfindex = value; }
            get { return _ksjhfindex; }
        }
        /// <summary>
        /// 可水解氟化物(以HF计)的质量分数结果（/%）
        /// </summary>
        public decimal? KSJHFResult
        {
            set { _ksjhfresult = value; }
            get { return _ksjhfresult; }
        }
        /// <summary>
        /// 矿物油的质量分数指标（%）
        /// </summary>
        public string KWYSorceIndex
        {
            set { _kwysorceindex = value; }
            get { return _kwysorceindex; }
        }
        /// <summary>
        /// 矿物油的质量分数结果（/%）
        /// </summary>
        public decimal? KWYSorceResult
        {
            set { _kwysorceresult = value; }
            get { return _kwysorceresult; }
        }
        /// <summary>
        /// 结论
        /// </summary>
        public string Conclusion
        {
            set { _conclusion = value; }
            get { return _conclusion; }
        }
        /// <summary>
        /// 是否合格
        /// </summary>
        public string IsPass
        {
            set { _ispass = value; }
            get { return _ispass; }
        }
        /// <summary>
        /// 申请人ID
        /// </summary>
        public string ApplOID
        {
            set { _apploid = value; }
            get { return _apploid; }
        }
        /// <summary>
        /// 申请日期
        /// </summary>
        public DateTime? AppLDate
        {
            set { _appldate = value; }
            get { return _appldate; }
        }
        /// <summary>
        /// 检测人OID
        /// </summary>
        public string CheckOID
        {
            set { _checkoid = value; }
            get { return _checkoid; }
        }
        /// <summary>
        /// 检测日期
        /// </summary>
        public DateTime? CheckDate
        {
            set { _checkdate = value; }
            get { return _checkdate; }
        }
        /// <summary>
        /// 签发人OID
        /// </summary>
        public string SignerOID
        {
            set { _signeroid = value; }
            get { return _signeroid; }
        }
        /// <summary>
        /// 签发日期
        /// </summary>
        public DateTime? SignerDate
        {
            set { _signerdate = value; }
            get { return _signerdate; }
        }
        /// <summary>
        /// 批准人ID
        /// </summary>
        public string ApproverOID
        {
            set { _approveroid = value; }
            get { return _approveroid; }
        }
        /// <summary>
        /// 批准日期
        /// </summary>
        public DateTime? ApproverDate
        {
            set { _approverdate = value; }
            get { return _approverdate; }
        }
        /// <summary>
        /// 流程状态
        /// </summary>
        public string FlowStatus
        {
            set { _flowstatus = value; }
            get { return _flowstatus; }
        }
        /// <summary>
        /// 报告是否打印
        /// </summary>
        public bool IsPrint
        {
            set { _isprint = value; }
            get { return _isprint; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string MTID
        {
            set { _mtid = value; }
            get { return _mtid; }
        }

    


        #endregion Model

    }
}


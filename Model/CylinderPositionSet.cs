﻿using System;
namespace EGMNGS.Model
{
    /// <summary>
    /// CylinderPositionSet:实体类(属性说明自动提取数据库字段的描述信息)
    /// </summary>
    [Serializable]
    public partial class CylinderPositionSet
    {
        public CylinderPositionSet()
        {
            _last_upd_date = _created_date = DateTime.Now;

        }
        #region Model
        private int _oid;
        private string _positioncode;
        private string _positionname;
        private string _positionremarks;
        private string _created_by;
        private DateTime? _created_date;
        private string _last_upd_by;
        private DateTime? _last_upd_date;
        /// <summary>
        /// 主键
        /// </summary>
        public int OID
        {
            set { _oid = value; }
            get { return _oid; }
        }
        /// <summary>
        /// 库区编码
        /// </summary>
        public string PositionCode
        {
            set { _positioncode = value; }
            get { return _positioncode; }
        }
        /// <summary>
        /// 库区名称
        /// </summary>
        public string PositionName
        {
            set { _positionname = value; }
            get { return _positionname; }
        }
        /// <summary>
        /// 存放说明
        /// </summary>
        public string positionRemarks
        {
            set { _positionremarks = value; }
            get { return _positionremarks; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string CREATED_BY
        {
            set { _created_by = value; }
            get { return _created_by; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? CREATED_DATE
        {
            set { _created_date = value; }
            get { return _created_date; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string LAST_UPD_BY
        {
            set { _last_upd_by = value; }
            get { return _last_upd_by; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? LAST_UPD_DATE
        {
            set { _last_upd_date = value; }
            get { return _last_upd_date; }
        }
        #endregion Model

    }
}


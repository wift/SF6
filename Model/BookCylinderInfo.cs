﻿using System;
using EGMNGS.Common;
namespace EGMNGS.Model
{
    /// <summary>
    /// 登记钢瓶基础信息和钢瓶编码。
    /// </summary>
    [Serializable]
    public partial class BookCylinderInfo
    {
        public BookCylinderInfo()
        {

        }


        public BookCylinderInfo(string actionCode)
            : base()
        {
            if (actionCode.Equals(GlobalConstants.ActionModeAdd))
            {
                _cylindercode = ComServies.GetCode("GP");
                _cylindersourse = _cylindercode.Replace("GP", "DKY");
            }
        }
        #region Model
        private int _oid;
        private string _cylindercode;
        private string _cylindersealno;
        private string _cylindersourse;
        private string _manufacturer;
        private string _cylindercapacity;
        private DateTime? _manufacturedate;
        private DateTime? _effectivedate;
        private string _curgascode;
        private string _status;
        private string _registantsoid;
        private DateTime? _registantsdate;
        private string _created_by;
        private DateTime _created_date;
        private string _last_upd_by;
        private DateTime _last_upd_date;
        /// <summary>
        /// 
        /// </summary>
        public int OID
        {
            set { _oid = value; }
            get { return _oid; }
        }
        /// <summary>
        /// 钢瓶编码
        /// </summary>
        public string CylinderCode
        {
            set { _cylindercode = value; }
            get { return _cylindercode; }
        }
        /// <summary>
        /// 钢瓶钢印号
        /// </summary>
        public string CylinderSealNo
        {
            set { _cylindersealno = value; }
            get { return _cylindersealno; }
        }
        /// <summary>
        /// 钢瓶来源
        /// </summary>
        public string CylinderSourse
        {
            set { _cylindersourse = value; }
            get { return _cylindersourse; }
        }
        /// <summary>
        /// 生产厂家
        /// </summary>
        public string Manufacturer
        {
            set { _manufacturer = value; }
            get { return _manufacturer; }
        }
        /// <summary>
        /// 钢瓶容量（KG）
        /// </summary>
        public string CylinderCapacity
        {
            set { _cylindercapacity = value; }
            get { return _cylindercapacity; }
        }
        /// <summary>
        /// 生产日期
        /// </summary>
        public DateTime? ManufactureDate
        {
            set { _manufacturedate = value; }
            get { return _manufacturedate; }
        }
        public string ManufactureDateString
        {

            get
            {
                if (_manufacturedate == null)
                {
                    return string.Empty;
                }
                else
                {
                    return _manufacturedate.Value.ToShortDateString();
                }
            }
        }
        /// <summary>
        /// 有效日期
        /// </summary>
        public DateTime? EffectiveDate
        {
            set { _effectivedate = value; }
            get { return _effectivedate; }
        }
        public string EffectiveDateString
        {
            get
            {
                if (_effectivedate == null)
                {
                    return string.Empty;
                }
                else
                {
                    return _effectivedate.Value.ToShortDateString();
                }
            }
        }
        /// <summary>
        /// 当前气体编码
        /// </summary>
        public string CurGasCode
        {
            set { _curgascode = value; }
            get { return _curgascode; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Status
        {
            set { _status = value; }
            get { return _status; }
        }
        /// <summary>
        /// 登记人ID
        /// </summary>
        public string RegistantsOID
        {
            set { _registantsoid = value; }
            get { return _registantsoid; }
        }
        /// <summary>
        /// 登记日期
        /// </summary>
        public DateTime? RegistantsDate
        {
            set { _registantsdate = value; }
            get { return _registantsdate; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string CREATED_BY
        {
            set { _created_by = value; }
            get { return _created_by; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime CREATED_DATE
        {
            set { _created_date = value; }
            get { return _created_date; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string LAST_UPD_BY
        {
            set { _last_upd_by = value; }
            get { return _last_upd_by; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime LAST_UPD_DATE
        {
            set { _last_upd_date = value; }
            get { return _last_upd_date; }
        }
        #endregion Model

    }
}


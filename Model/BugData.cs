﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EGMNGS.Model
{
    [Table("BugData")]
    [Serializable]
   public class BugData
    {
        #region Model
        private int _oid;
        private string _devcode;
        private string _devname;
        private DateTime? _bugtdate;
        private string _bugreson;
        private string _reportname;
        private string _powersupplycode;
        private string _powersupplyname;
        private string _convertstationcode;
        private string _convertstationname;
        private string _created_by;
        private DateTime? _created_date;
        private string _last_upd_by;
        private DateTime? _last_upd_date;
        /// <summary>
        /// 主键
        /// </summary>
        public int OID
        {
            set { _oid = value; }
            get { return _oid; }
        }
        /// <summary>
        /// 设备编码
        /// </summary>
        public string DevCode
        {
            set { _devcode = value; }
            get { return _devcode; }
        }
        /// <summary>
        /// 设备名称
        /// </summary>
        public string DevName
        {
            set { _devname = value; }
            get { return _devname; }
        }
        /// <summary>
        /// 故障日期
        /// </summary>
        public DateTime? BugtDate
        {
            set { _bugtdate = value; }
            get { return _bugtdate; }
        }
        /// <summary>
        /// 故障原因
        /// </summary>
        public string BugReson
        {
            set { _bugreson = value; }
            get { return _bugreson; }
        }
        /// <summary>
        /// 品质检测报告名称
        /// </summary>
        public string ReportName
        {
            set { _reportname = value; }
            get { return _reportname; }
        }
        /// <summary>
        /// 供电局编码
        /// </summary>
        public string PowerSupplyCode
        {
            set { _powersupplycode = value; }
            get { return _powersupplycode; }
        }
        /// <summary>
        /// 供电局名称
        /// </summary>
        public string PowerSupplyName
        {
            set { _powersupplyname = value; }
            get { return _powersupplyname; }
        }
        /// <summary>
        /// 变电站编码
        /// </summary>
        public string ConvertStationCode
        {
            set { _convertstationcode = value; }
            get { return _convertstationcode; }
        }
        /// <summary>
        /// 变电站名称
        /// </summary>
        public string ConvertStationName
        {
            set { _convertstationname = value; }
            get { return _convertstationname; }
        }
        /// <summary>
        /// 创建人
        /// </summary>
        public string CREATED_BY
        {
            set { _created_by = value; }
            get { return _created_by; }
        }
        /// <summary>
        /// 创建日期
        /// </summary>
        public DateTime? CREATED_DATE
        {
            set { _created_date = value; }
            get { return _created_date; }
        }
        /// <summary>
        /// 最后更新人
        /// </summary>
        public string LAST_UPD_BY
        {
            set { _last_upd_by = value; }
            get { return _last_upd_by; }
        }
        /// <summary>
        /// 最后更新日期
        /// </summary>
        public DateTime? LAST_UPD_DATE
        {
            set { _last_upd_date = value; }
            get { return _last_upd_date; }
        }
        #endregion Model
    }
}

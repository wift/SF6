﻿using System;
using EGMNGS.Common;
namespace EGMNGS.Model
{
    /// <summary>
    /// 登记各供电局下属变电站的设备基础信息。
    /// </summary>

    [Table("InfoDevPara")]
    [Serializable]
    public partial class InfoDevPara
    {
        public InfoDevPara()
        {

        }
        public InfoDevPara(string actionCode)
            : base()
        {
            if (actionCode.Equals(GlobalConstants.ActionModeAdd))
            {
                _devcode = EGMNGS.Common.ComServies.GetCode("SB");
                _status = "0";
            }

        }
        #region Model
        private int _oid;
        private string _devcode;
        private string _powersupplycode;
        private string _powersupplyname;
        private string _convertstationcode;
        private string _convertstationname;
        private string _voltagelevel;
        private string _devclass;
        private string _runcode;
        private string _devname;
        private string _manufacturer;
        private string _devtype;
        private string _devnum;
        private DateTime _purchasedate;
        private DateTime _putdate;
        private DateTime _returndate;
        private decimal _ratedql;
        private decimal _ratedqy;
        private string _status;
        private string _registrantoid;
        private DateTime _registrantdate;
        private string _created_by;
        private DateTime _created_date;
        private string _last_upd_by;
        private DateTime _last_upd_date;
        private int _kv;
        private int _a;
        private int _ka;

        private string _outdevrunno;
        private string _outdevid;
        private string _outdevuniquecode;
        private string _outdevstatus;
        private string _outmanufacturercurname;
        private string _outprovider;
        private string _outvoltagelevel;
        private string _outdevclass;
        private string _outfullpath;
        private DateTime? _outgoincurstatus;
        private string _outorifactorywarranty;
        private string _outunits;
        private int? _outdevcoutorlength;
        private string _outdevcpisdept;
        private string _outstorageteam;
        private string _outispropertydev;
        private string _outpropertycode;
        private string _outintervalname;

        private string _IsOnlineCheck;
        /// <summary>
        /// 
        /// </summary>
        public int OID
        {
            set { _oid = value; }
            get { return _oid; }
        }
        /// <summary>
        /// 设备编码
        /// </summary>
        public string DevCode
        {
            set { _devcode = value; }
            get { return _devcode; }
        }
        /// <summary>
        /// 供电局编码
        /// </summary>
        public string PowerSupplyCode
        {
            set { _powersupplycode = value; }
            get { return _powersupplycode; }
        }
        /// <summary>
        /// 供电局名称
        /// </summary>
        public string PowerSupplyName
        {
            set { _powersupplyname = value; }
            get { return _powersupplyname; }
        }
        /// <summary>
        /// 变电站编码
        /// </summary>
        public string ConvertStationCode
        {
            set { _convertstationcode = value; }
            get { return _convertstationcode; }
        }
        /// <summary>
        /// 变电站名称
        /// </summary>
        public string ConvertStationName
        {
            set { _convertstationname = value; }
            get { return _convertstationname; }
        }
        /// <summary>
        /// 电压等级
        /// </summary>
        public string VoltageLevel
        {
            set { _voltagelevel = value; }
            get { return _voltagelevel; }
        }
        /// <summary>
        /// 设备分类
        /// </summary>
        public string DevClass
        {
            set { _devclass = value; }
            get { return _devclass; }
        }
        /// <summary>
        /// 运行编号
        /// </summary>
        public string RunCode
        {
            set { _runcode = value; }
            get { return _runcode; }
        }
        /// <summary>
        /// 设备名称
        /// </summary>
        public string DevName
        {
            set { _devname = value; }
            get { return _devname; }
        }
        /// <summary>
        /// 生产厂家
        /// </summary>
        public string Manufacturer
        {
            set { _manufacturer = value; }
            get { return _manufacturer; }
        }
        /// <summary>
        /// 设备型号
        /// </summary>
        public string DevType
        {
            set { _devtype = value; }
            get { return _devtype; }
        }
        /// <summary>
        /// 设备编号
        /// </summary>
        public string DevNum
        {
            set { _devnum = value; }
            get { return _devnum; }
        }
        /// <summary>
        /// 购买日期
        /// </summary>
        public DateTime PurchaseDate
        {
            set { _purchasedate = value; }
            get { return _purchasedate; }
        }
        /// <summary>
        /// 投运日期
        /// </summary>
        public DateTime PutDate
        {
            set { _putdate = value; }
            get { return _putdate; }
        }
        /// <summary>
        /// 退运日期
        /// </summary>
        public DateTime ReturnDate
        {
            set { _returndate = value; }
            get { return _returndate; }
        }
        /// <summary>
        /// 额定气量（KG）
        /// </summary>
        public decimal RatedQL
        {
            set { _ratedql = value; }
            get { return _ratedql; }
        }
        /// <summary>
        /// 额定气压（Mpa）
        /// </summary>
        public decimal RatedQY
        {
            set { _ratedqy = value; }
            get { return _ratedqy; }
        }
        /// <summary>
        /// 状态
        /// </summary>
        public string Status
        {
            set { _status = value; }
            get { return _status; }
        }
        /// <summary>
        /// 登记人ID
        /// </summary>
        public string RegistrantOID
        {
            set { _registrantoid = value; }
            get { return _registrantoid; }
        }
        /// <summary>
        /// 登记日期
        /// </summary>
        public DateTime RegistrantDate
        {
            set { _registrantdate = value; }
            get { return _registrantdate; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string CREATED_BY
        {
            set { _created_by = value; }
            get { return _created_by; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime CREATED_DATE
        {
            set { _created_date = value; }
            get { return _created_date; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string LAST_UPD_BY
        {
            set { _last_upd_by = value; }
            get { return _last_upd_by; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime LAST_UPD_DATE
        {
            set { _last_upd_date = value; }
            get { return _last_upd_date; }
        }


        /// <summary>
        /// 额定电压（kV）
        /// </summary>
        public int KV
        {
            set { _kv = value; }
            get { return _kv; }
        }
        /// <summary>
        /// 额定电流（A）
        /// </summary>
        public int A
        {
            set { _a = value; }
            get { return _a; }
        }
        /// <summary>
        /// 开断电流（kA）
        /// </summary>
        public int KA
        {
            set { _ka = value; }
            get { return _ka; }
        }

        /// <summary>
        /// 设备运行编码
        /// </summary>
        public string OutDevRunNo
        {
            set { _outdevrunno = value; }
            get { return _outdevrunno; }
        }
        /// <summary>
        /// 设备ID
        /// </summary>
        public string OutDevID
        {
            set { _outdevid = value; }
            get { return _outdevid; }
        }
        /// <summary>
        /// 设备身份证编码
        /// </summary>
        public string OutDevUniqueCode
        {
            set { _outdevuniquecode = value; }
            get { return _outdevuniquecode; }
        }
        /// <summary>
        /// 设备状态
        /// </summary>
        public string OutDevStatus
        {
            set { _outdevstatus = value; }
            get { return _outdevstatus; }
        }
        /// <summary>
        /// 生产厂家现用名
        /// </summary>
        public string OutManufacturerCurName
        {
            set { _outmanufacturercurname = value; }
            get { return _outmanufacturercurname; }
        }
        /// <summary>
        /// 供应商
        /// </summary>
        public string OutProvider
        {
            set { _outprovider = value; }
            get { return _outprovider; }
        }
        /// <summary>
        /// 电压等级
        /// </summary>
        public string OutVoltageLevel
        {
            set { _outvoltagelevel = value; }
            get { return _outvoltagelevel; }
        }
        /// <summary>
        /// 类别名称
        /// </summary>
        public string OutDevClass
        {
            set { _outdevclass = value; }
            get { return _outdevclass; }
        }
        /// <summary>
        /// 全路径
        /// </summary>
        public string OutFullPath
        {
            set { _outfullpath = value; }
            get { return _outfullpath; }
        }
        /// <summary>
        /// 进入当前状态时间
        /// </summary>
        public DateTime? OutGoinCurStatus
        {
            set { _outgoincurstatus = value; }
            get { return _outgoincurstatus; }
        }
        /// <summary>
        /// 原厂保修期(月)
        /// </summary>
        public string OutOriFactoryWarranty
        {
            set { _outorifactorywarranty = value; }
            get { return _outorifactorywarranty; }
        }
        /// <summary>
        /// 计量单位
        /// </summary>
        public string OutUnits
        {
            set { _outunits = value; }
            get { return _outunits; }
        }
        /// <summary>
        /// 设备数量或线路长度(米)
        /// </summary>
        public int? OutDevCoutOrLength
        {
            set { _outdevcoutorlength = value; }
            get { return _outdevcoutorlength; }
        }
        /// <summary>
        /// 设备运维部门
        /// </summary>
        public string OutDevCPISDept
        {
            set { _outdevcpisdept = value; }
            get { return _outdevcpisdept; }
        }
        /// <summary>
        /// 保管班组
        /// </summary>
        public string OutStorageTeam
        {
            set { _outstorageteam = value; }
            get { return _outstorageteam; }
        }
        /// <summary>
        /// 是否资产级设备
        /// </summary>
        public string OutIsPropertyDev
        {
            set { _outispropertydev = value; }
            get { return _outispropertydev; }
        }
        /// <summary>
        /// 资产级编码
        /// </summary>
        public string OutPropertyCode
        {
            set { _outpropertycode = value; }
            get { return _outpropertycode; }
        }
        /// <summary>
        /// 间隔名称
        /// </summary>
        public string OutIntervalName
        {
            set { _outintervalname = value; }
            get { return _outintervalname; }
        }


        /// <summary>
        /// 是否在线测试
        /// </summary>
        public string IsOnlineCheck
        {
            get
            {
                return _IsOnlineCheck;
            }

            set
            {
                _IsOnlineCheck = value;
            }
        }
        #endregion Model

    }
}


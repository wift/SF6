﻿using System;
namespace EGMNGS.Model
{
	/// <summary>
	/// OCInstrumentInfoOnline:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class OCInstrumentInfoOnline
	{
		public OCInstrumentInfoOnline()
		{}
		#region Model
		private int _oid;
		private string _checkpointcode;
		private string _checkpointname;
		private decimal? _pressure;
		private decimal? _temperture;
		private decimal? _microwater;
		private decimal? _so2;
		private decimal? _co;
		private decimal? _cf4;
		private decimal? _so2f2;
		private decimal? _sof2;
		private decimal? _cs2;
		private decimal? _hf;
		private string _collectiontype;
		private DateTime? _collectiondatetime;
		private string _collectionby;
        private decimal? _h2s;
        private decimal? _coss;
        private decimal? _c2f6;
        private decimal? _c3f8;
        /// <summary>
        /// ID
        /// </summary>
        public int OID
		{
			set{ _oid=value;}
			get{return _oid;}
		}
		/// <summary>
		/// 测点编码
		/// </summary>
		public string CheckPointCode
		{
			set{ _checkpointcode=value;}
			get{return _checkpointcode;}
		}
		/// <summary>
		/// 测点名称
		/// </summary>
		public string CheckPointName
		{
			set{ _checkpointname=value;}
			get{return _checkpointname;}
		}
		/// <summary>
		/// 压力
		/// </summary>
		public decimal? Pressure
		{
			set{ _pressure=value;}
			get{return _pressure;}
		}
		/// <summary>
		/// 温度
		/// </summary>
		public decimal? Temperture
		{
			set{ _temperture=value;}
			get{return _temperture;}
		}
		/// <summary>
		/// 微水
		/// </summary>
		public decimal? MicroWater
		{
			set{ _microwater=value;}
			get{return _microwater;}
		}
		/// <summary>
		/// SO2
		/// </summary>
		public decimal? SO2
		{
			set{ _so2=value;}
			get{return _so2;}
		}
		/// <summary>
		/// CO
		/// </summary>
		public decimal? CO
		{
			set{ _co=value;}
			get{return _co;}
		}
		/// <summary>
		/// CF4
		/// </summary>
		public decimal? CF4
		{
			set{ _cf4=value;}
			get{return _cf4;}
		}
		/// <summary>
		/// SO2F2
		/// </summary>
		public decimal? SO2F2
		{
			set{ _so2f2=value;}
			get{return _so2f2;}
		}
		/// <summary>
		/// SOF2
		/// </summary>
		public decimal? SOF2
		{
			set{ _sof2=value;}
			get{return _sof2;}
		}
		/// <summary>
		/// CS2
		/// </summary>
		public decimal? CS2
		{
			set{ _cs2=value;}
			get{return _cs2;}
		}
		/// <summary>
		/// HF
		/// </summary>
		public decimal? HF
		{
			set{ _hf=value;}
			get{return _hf;}
		}
		/// <summary>
		/// 采集类型
		/// </summary>
		public string CollectionType
		{
			set{ _collectiontype=value;}
			get{return _collectiontype;}
		}
		/// <summary>
		/// 采集时间
		/// </summary>
		public DateTime? CollectionDatetime
		{
			set{ _collectiondatetime=value;}
			get{return _collectiondatetime;}
		}
		/// <summary>
		/// 采集人
		/// </summary>
		public string CollectionBy
		{
			set{ _collectionby=value;}
			get{return _collectionby;}
		}

        /// <summary>
        /// 
        /// </summary>
        public decimal? H2S
        {
            set { _h2s = value; }
            get { return _h2s; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? COSS
        {
            set { _coss = value; }
            get { return _coss; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? C2F6
        {
            set { _c2f6 = value; }
            get { return _c2f6; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? C3F8
        {
            set { _c3f8 = value; }
            get { return _c3f8; }
        }
        #endregion Model

    }
}


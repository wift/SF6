﻿using System;
namespace EGMNGS.Model
{
    /// <summary>
    /// 采购员登记新采购气体信息、钢瓶信息及气体编码，并提交新采购
    /// </summary>
    [Serializable]
    public partial class SYS_CODE
    {
        public SYS_CODE()
        { }
        #region Model
        private string _code_type;
        private string _code;
        private string _subcode = "1";
        private string _code_eng_desc;
        private string _code_chi_desc;
        private string _short_desc;
        private decimal? _display_order;
        private string _misc_ind;
        private string _depart_code;
        private string _created_by;
        private DateTime? _created_date;
        private string _last_upd_by;
        private DateTime? _last_upd_date;

        public string TempCode
        {
            get { return _code; }
            set { _code = value; }
        }
      
        /// <summary>
        /// 
        /// </summary>
        public string CODE_TYPE
        {
            set { _code_type = value; }
            get { return _code_type; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string CODE
        {
            set { _code = value; }
            get { return _code; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string SUBCODE
        {
            set { _subcode = value; }
            get { return _subcode; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string CODE_ENG_DESC
        {
            set { _code_eng_desc = value; }
            get { return _code_eng_desc; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string CODE_CHI_DESC
        {
            set { _code_chi_desc = value; }
            get { return _code_chi_desc; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string SHORT_DESC
        {
            set { _short_desc = value; }
            get { return _short_desc; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? DISPLAY_ORDER
        {
            set { _display_order = value; }
            get { return _display_order; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string MISC_IND
        {
            set { _misc_ind = value; }
            get { return _misc_ind; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string DEPART_CODE
        {
            set { _depart_code = value; }
            get { return _depart_code; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string CREATED_BY
        {
            set { _created_by = value; }
            get { return _created_by; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? CREATED_DATE
        {
            set { _created_date = value; }
            get { return _created_date; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string LAST_UPD_BY
        {
            set { _last_upd_by = value; }
            get { return _last_upd_by; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? LAST_UPD_DATE
        {
            set { _last_upd_date = value; }
            get { return _last_upd_date; }
        }
        #endregion Model

    }
}


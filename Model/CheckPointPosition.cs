﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EGMNGS.Model
{
    /// <summary>
    /// 测点位置表
    /// </summary>
    [Table("CheckPointPositionInfo")]
    public class CheckPointPositionInfo
    {
        #region Model
        private int _oid;
        private int? _checkpointtemplates_oid;
        private string _checkpointposition;
        private string _positionname;
        private decimal? _height;
        private string _remarks;
        private string _created_by;
        private DateTime? _created_date;
        private string _last_upd_by;
        private DateTime? _last_upd_date;
        /// <summary>
        /// 
        /// </summary>
        public int OID
        {
            set { _oid = value; }
            get { return _oid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? CheckPointTemplates_OID
        {
            set { _checkpointtemplates_oid = value; }
            get { return _checkpointtemplates_oid; }
        }
        /// <summary>
        /// 测点位置
        /// </summary>
        public string CheckPointPosition
        {
            set { _checkpointposition = value; }
            get { return _checkpointposition; }
        }
        /// <summary>
        /// 位置名称
        /// </summary>
        public string PositionName
        {
            set { _positionname = value; }
            get { return _positionname; }
        }
        /// <summary>
        /// 高度
        /// </summary>
        public decimal? Height
        {
            set { _height = value; }
            get { return _height; }
        }
        /// <summary>
        /// 备注
        /// </summary>
        public string Remarks
        {
            set { _remarks = value; }
            get { return _remarks; }
        }
        /// <summary>
        /// 创建人
        /// </summary>
        public string CREATED_BY
        {
            set { _created_by = value; }
            get { return _created_by; }
        }
        /// <summary>
        /// 创建日期
        /// </summary>
        public DateTime? CREATED_DATE
        {
            set { _created_date = value; }
            get { return _created_date; }
        }
        /// <summary>
        /// 最后更新人
        /// </summary>
        public string LAST_UPD_BY
        {
            set { _last_upd_by = value; }
            get { return _last_upd_by; }
        }
        /// <summary>
        /// 最后更新日期
        /// </summary>
        public DateTime? LAST_UPD_DATE
        {
            set { _last_upd_date = value; }
            get { return _last_upd_date; }
        }
        #endregion Model

    }
}

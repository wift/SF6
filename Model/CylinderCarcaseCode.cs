﻿using System;
namespace EGMNGS.Model
{
	/// <summary>
	/// CylinderCarcaseCode:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class CylinderCarcaseCode
	{
		public CylinderCarcaseCode()
		{}
		#region Model
		private int _oid;
		private string _carcasecode;
		private string _positioncode;
		private string _positionname;
		private string _depositposition;
		private string _depositpositionname;
		private int? _depositcolumncount;
		private int? _depositrowcount;
		private string _depositdesc;
		private string _gastype;
		private string _gastypename;
		private string _cylinderstatus;
		private string _cylinderstatusname;
		private string _created_by;
		private DateTime? _created_date;
		private string _last_upd_by;
		private DateTime? _last_upd_date;
		/// <summary>
		/// 
		/// </summary>
		public int OID
		{
			set{ _oid=value;}
			get{return _oid;}
		}
		/// <summary>
		/// 架子编码
		/// </summary>
		public string CarcaseCode
		{
			set{ _carcasecode=value;}
			get{return _carcasecode;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string PositionCode
		{
			set{ _positioncode=value;}
			get{return _positioncode;}
		}
		/// <summary>
		/// 库区名称
		/// </summary>
		public string PositionName
		{
			set{ _positionname=value;}
			get{return _positionname;}
		}
		/// <summary>
		/// 存放方位
		/// </summary>
		public string DepositPosition
		{
			set{ _depositposition=value;}
			get{return _depositposition;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string DepositPositionName
		{
			set{ _depositpositionname=value;}
			get{return _depositpositionname;}
		}
		/// <summary>
		/// 存放列数
		/// </summary>
		public int? DepositColumnCount
		{
			set{ _depositcolumncount=value;}
			get{return _depositcolumncount;}
		}
		/// <summary>
		/// 存放层数
		/// </summary>
		public int? DepositRowCount
		{
			set{ _depositrowcount=value;}
			get{return _depositrowcount;}
		}
		/// <summary>
		/// 存放说明
		/// </summary>
		public string DepositDesc
		{
			set{ _depositdesc=value;}
			get{return _depositdesc;}
		}
		/// <summary>
		/// 气体类型
		/// </summary>
		public string GasType
		{
			set{ _gastype=value;}
			get{return _gastype;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string GasTypeName
		{
			set{ _gastypename=value;}
			get{return _gastypename;}
		}
		/// <summary>
		/// 钢瓶状态
		/// </summary>
		public string CylinderStatus
		{
			set{ _cylinderstatus=value;}
			get{return _cylinderstatus;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string CylinderStatusName
		{
			set{ _cylinderstatusname=value;}
			get{return _cylinderstatusname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string CREATED_BY
		{
			set{ _created_by=value;}
			get{return _created_by;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? CREATED_DATE
		{
			set{ _created_date=value;}
			get{return _created_date;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string LAST_UPD_BY
		{
			set{ _last_upd_by=value;}
			get{return _last_upd_by;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? LAST_UPD_DATE
		{
			set{ _last_upd_date=value;}
			get{return _last_upd_date;}
		}
		#endregion Model

	}
}


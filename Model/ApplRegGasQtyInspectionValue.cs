using System;
namespace EGMNGS.Model
{
	/// <summary>
	/// 实体类ApplRegGasQtyInspectionValue 。(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public class ApplRegGasQtyInspectionValue
	{
		public ApplRegGasQtyInspectionValue()
		{}
		#region Model
		private int _oid;
		private string _checkcode;
		private decimal? _environmenttemperature;
		private decimal? _environmenthumidity;
		private decimal? _barometricpressure;
		private string _pipe1;
		private string _pipe2;
		private string _pipe3;
		private string _pipe4;
		private string _pipe5;
		private string _pipe6;
		private string _pipe7;
		private string _pipe8;
		/// <summary>
		/// 
		/// </summary>
		public int OID
		{
			set{ _oid=value;}
			get{return _oid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string CheckCode
		{
			set{ _checkcode=value;}
			get{return _checkcode;}
		}
		/// <summary>
		/// 环境湿度
		/// </summary>
		public decimal? EnvironmentTemperature
		{
			set{ _environmenttemperature=value;}
			get{return _environmenttemperature;}
		}
		/// <summary>
		/// 环境温度
		/// </summary>
		public decimal? EnvironmentHumidity
		{
			set{ _environmenthumidity=value;}
			get{return _environmenthumidity;}
		}
		/// <summary>
		/// 大气压
		/// </summary>
		public decimal? BarometricPressure
		{
			set{ _barometricpressure=value;}
			get{return _barometricpressure;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Pipe1
		{
			set{ _pipe1=value;}
			get{return _pipe1;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Pipe2
		{
			set{ _pipe2=value;}
			get{return _pipe2;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Pipe3
		{
			set{ _pipe3=value;}
			get{return _pipe3;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Pipe4
		{
			set{ _pipe4=value;}
			get{return _pipe4;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Pipe5
		{
			set{ _pipe5=value;}
			get{return _pipe5;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Pipe6
		{
			set{ _pipe6=value;}
			get{return _pipe6;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Pipe7
		{
			set{ _pipe7=value;}
			get{return _pipe7;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Pipe8
		{
			set{ _pipe8=value;}
			get{return _pipe8;}
		}
		#endregion Model

	}
}


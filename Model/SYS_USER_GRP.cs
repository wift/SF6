﻿using System;
namespace EGMNGS.Model
{
	/// <summary>
	/// 采购员登记新采购气体信息、钢瓶信息及气体编码，并提交新采购
	/// </summary>
	[Serializable]
	public partial class SYS_USER_GRP
	{
		public SYS_USER_GRP()
		{}
		#region Model
		private string _user_grp_id;
		private string _user_grp_name;
		private string _active_ind;
		private string _created_by;
		private DateTime _created_date;
		private string _last_upd_by;
		private DateTime _last_upd_date;
		/// <summary>
		/// 
		/// </summary>
		public string USER_GRP_ID
		{
			set{ _user_grp_id=value;}
			get{return _user_grp_id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string USER_GRP_NAME
		{
			set{ _user_grp_name=value;}
			get{return _user_grp_name;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string ACTIVE_IND
		{
			set{ _active_ind=value;}
			get{return _active_ind;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string CREATED_BY
		{
			set{ _created_by=value;}
			get{return _created_by;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime CREATED_DATE
		{
			set{ _created_date=value;}
			get{return _created_date;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string LAST_UPD_BY
		{
			set{ _last_upd_by=value;}
			get{return _last_upd_by;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime LAST_UPD_DATE
		{
			set{ _last_upd_date=value;}
			get{return _last_upd_date;}
		}
		#endregion Model

	}
}


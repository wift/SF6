﻿using System;
namespace EGMNGS.Model
{
    /// <summary>
    /// 查看3种气体的实时库存数量和历史库存数量等信息，每月进行收发存对账，生成月度库存台账。
    /// </summary>
    [Serializable]
    public partial class BookGasStorage
    {
        public BookGasStorage()
        { }
        #region Model
        private int _oid;
        private string _year = DateTime.Now.Year.ToString();
        private string _month = DateTime.Now.Month.ToString();
        private decimal? _countrecyclegas;
        private decimal? _countwaitinggas;
        private decimal? _countpassgas;
        private string _checkstatus;
        private string _checkoid;
        private DateTime? _checkdate;
        private string _created_by;
        private DateTime _created_date;
        private string _last_upd_by;
        private DateTime _last_upd_date;
        /// <summary>
        /// 
        /// </summary>
        public int OID
        {
            set { _oid = value; }
            get { return _oid; }
        }
        /// <summary>
        /// 年度
        /// </summary>
        public string Year
        {
            set { _year = value; }
            get { return _year; }
        }
        /// <summary>
        /// 月度
        /// </summary>
        public string Month
        {
            set { _month = value; }
            get { return _month; }
        }
        /// <summary>
        /// 回收气库存
        /// </summary>
        public decimal? CountRecycleGas
        {
            set { _countrecyclegas = value; }
            get { return _countrecyclegas; }
        }
        /// <summary>
        /// 待检气库存
        /// </summary>
        public decimal? CountWaitingGas
        {
            set { _countwaitinggas = value; }
            get { return _countwaitinggas; }
        }
        /// <summary>
        /// 合格气库存
        /// </summary>
        public decimal? CountPassGas
        {
            set { _countpassgas = value; }
            get { return _countpassgas; }
        }
        /// <summary>
        /// 对账状态
        /// </summary>
        public string CheckStatus
        {
            set { _checkstatus = value; }
            get { return _checkstatus; }
        }
        /// <summary>
        /// 对账人ID
        /// </summary>
        public string CheckOID
        {
            set { _checkoid = value; }
            get { return _checkoid; }
        }
        /// <summary>
        /// 对账日期
        /// </summary>
        public DateTime? CheckDate
        {
            set { _checkdate = value; }
            get { return _checkdate; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string CREATED_BY
        {
            set { _created_by = value; }
            get { return _created_by; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime CREATED_DATE
        {
            set { _created_date = value; }
            get { return _created_date; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string LAST_UPD_BY
        {
            set { _last_upd_by = value; }
            get { return _last_upd_by; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime LAST_UPD_DATE
        {
            set { _last_upd_date = value; }
            get { return _last_upd_date; }
        }
        #endregion Model

    }
}


﻿using System;
using EGMNGS.Common;
namespace EGMNGS.Model
{
    /// <summary>
    /// 对气体分别进行回收气体出库、待检气体出库和合格气体出库进行操作
    /// </summary>
    [Serializable]
    public partial class TableGasOutStorage
    {
        public TableGasOutStorage()
        {


        }
        public TableGasOutStorage(string actionCode)
            : base()
        {
            if (actionCode.Equals(GlobalConstants.ActionModeAdd))
            {
                _code = EGMNGS.Common.ComServies.GetCode("CK", DateTime.Now.Year.ToString() +

  DateTime.Now.Month.ToString("D2"));
            }

        }
        #region Model
        private int _oid;
        private string _code;
        private string _year = DateTime.Now.Year.ToString();
        private string _month = DateTime.Now.Month.ToString();
        private string _gasclass;
        private string _powersupplycode;
        private string _powersupplyname;
        private string _convertstationcode;
        private string _convertstationname;
        private decimal _amountoutstorage = 0;
        private int _countcyliner;
        private string _address;
        private string _contacts;
        private string _officetel;
        private string _phonenum;
        private string _status;
        private string _usingoid;
        private DateTime? _usingdate;
        private string _registantoid;
        private DateTime _registantdate;
        private string _created_by;
        private DateTime _created_date;
        private string _last_upd_by;
        private DateTime _last_upd_date;
        private string _businessCode;
        private string _countDesc;

        /// <summary>
        /// 数量说明
        /// </summary>
        public string CountDesc
        {
            get { return _countDesc; }
            set { _countDesc = value; }
        }
        public string BusinessCode
        {
            get { return _businessCode; }
            set { _businessCode = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int OID
        {
            set { _oid = value; }
            get { return _oid; }
        }
        /// <summary>
        /// 出库编号
        /// </summary>
        public string Code
        {
            set { _code = value; }
            get { return _code; }
        }
        /// <summary>
        /// 年度
        /// </summary>
        public string Year
        {
            set { _year = value; }
            get { return _year; }
        }
        /// <summary>
        /// 月度
        /// </summary>
        public string Month
        {
            set { _month = value; }
            get { return _month; }
        }
        /// <summary>
        /// 气体类别
        /// </summary>
        public string GasClass
        {
            set { _gasclass = value; }
            get { return _gasclass; }
        }
        /// <summary>
        /// 供电局编码
        /// </summary>
        public string PowerSupplyCode
        {
            set { _powersupplycode = value; }
            get { return _powersupplycode; }
        }
        /// <summary>
        /// 供电局名称
        /// </summary>
        public string PowerSupplyName
        {
            set { _powersupplyname = value; }
            get { return _powersupplyname; }
        }
        /// <summary>
        /// 变电站编码
        /// </summary>
        public string ConvertStationCode
        {
            set { _convertstationcode = value; }
            get { return _convertstationcode; }
        }
        /// <summary>
        /// 变电站名称
        /// </summary>
        public string ConvertStationName
        {
            set { _convertstationname = value; }
            get { return _convertstationname; }
        }
        /// <summary>
        /// 出库量（KG）
        /// </summary>
        public decimal AmountOutStorage
        {
            set { _amountoutstorage = value; }
            get { return _amountoutstorage; }
        }
        /// <summary>
        /// 钢瓶数
        /// </summary>
        public int CountCyliner
        {
            set { _countcyliner = value; }
            get { return _countcyliner; }
        }
        /// <summary>
        /// 联系地址
        /// </summary>
        public string Address
        {
            set { _address = value; }
            get { return _address; }
        }
        /// <summary>
        /// 联系人
        /// </summary>
        public string Contacts
        {
            set { _contacts = value; }
            get { return _contacts; }
        }
        /// <summary>
        /// 办公电话
        /// </summary>
        public string OfficeTel
        {
            set { _officetel = value; }
            get { return _officetel; }
        }
        /// <summary>
        /// 手机电话
        /// </summary>
        public string PhoneNum
        {
            set { _phonenum = value; }
            get { return _phonenum; }
        }
        /// <summary>
        /// 状态
        /// </summary>
        public string Status
        {
            set { _status = value; }
            get { return _status; }
        }
        /// <summary>
        /// 领用人ID
        /// </summary>
        public string UsingOID
        {
            set { _usingoid = value; }
            get { return _usingoid; }
        }
        /// <summary>
        /// 领用日期
        /// </summary>
        public DateTime? UsingDate
        {
            set { _usingdate = value; }
            get { return _usingdate; }
        }
        /// <summary>
        /// 登记人ID
        /// </summary>
        public string RegistantOID
        {
            set { _registantoid = value; }
            get { return _registantoid; }
        }
        /// <summary>
        /// 登记日期
        /// </summary>
        public DateTime RegistantDate
        {
            set { _registantdate = value; }
            get { return _registantdate; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string CREATED_BY
        {
            set { _created_by = value; }
            get { return _created_by; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime CREATED_DATE
        {
            set { _created_date = value; }
            get { return _created_date; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string LAST_UPD_BY
        {
            set { _last_upd_by = value; }
            get { return _last_upd_by; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime LAST_UPD_DATE
        {
            set { _last_upd_date = value; }
            get { return _last_upd_date; }
        }
        #endregion Model

    }
}


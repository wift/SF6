﻿using System;
using EGMNGS.Common;
namespace EGMNGS.Model
{
    /// <summary>
    /// 登记各供电局的变电站基础信息。
    /// </summary>
    [Serializable]
    public partial class InfoPowerSupplyConvertStation
    {
        public InfoPowerSupplyConvertStation()
        { }

        public InfoPowerSupplyConvertStation(string actionMode)
            : base()
        {
            // TODO: Complete member initialization
            if (actionMode.Equals("Add"))
            {
                _covnertstationcode = ComServies.GetCode8("BD");
                _status = "0";

            }
        }
        #region Model
        private int _oid;
        private string _powersupplyid;
        private string _powersupplycode;
        private string _powersupplyname;
        private string _covnertstationcode;
        private string _convartstationname;
        private string _status;
        private string _registrantoid;
        private DateTime? _registrantdate;
        private string _created_by;
        private DateTime _created_date;
        private string _last_upd_by;
        private DateTime _last_upd_date;
        private string _IsOnlineCheck;
        private string _IsMagnetic;
        private decimal _Longitude;
        private decimal _Latitude;

        /// <summary>
        /// 
        /// </summary>
        public int OID
        {
            set { _oid = value; }
            get { return _oid; }
        }
        /// <summary>
        /// 供电局ID
        /// </summary>
        public string PowerSupplyID
        {
            set { _powersupplyid = value; }
            get { return _powersupplyid; }
        }
        /// <summary>
        /// 供电局编码
        /// </summary>
        public string PowerSupplyCode
        {
            set { _powersupplycode = value; }
            get { return _powersupplycode; }
        }
        /// <summary>
        /// 供电局名称
        /// </summary>
        public string PowerSupplyName
        {
            set { _powersupplyname = value; }
            get { return _powersupplyname; }
        }
        /// <summary>
        /// 变电站编码
        /// </summary>
        public string CovnertStationCode
        {
            set { _covnertstationcode = value; }
            get { return _covnertstationcode; }
        }
        /// <summary>
        /// 变电站名称
        /// </summary>
        public string ConvartStationName
        {
            set { _convartstationname = value; }
            get { return _convartstationname; }
        }
        /// <summary>
        /// 状态
        /// </summary>
        public string Status
        {
            set { _status = value; }
            get { return _status; }
        }
        /// <summary>
        /// 登记人ID
        /// </summary>
        public string RegistrantOID
        {
            set { _registrantoid = value; }
            get { return _registrantoid; }
        }
        /// <summary>
        /// 登记日期
        /// </summary>
        public DateTime? RegistrantDate
        {
            set { _registrantdate = value; }
            get { return _registrantdate; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string CREATED_BY
        {
            set { _created_by = value; }
            get { return _created_by; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime CREATED_DATE
        {
            set { _created_date = value; }
            get { return _created_date; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string LAST_UPD_BY
        {
            set { _last_upd_by = value; }
            get { return _last_upd_by; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime LAST_UPD_DATE
        {
            set { _last_upd_date = value; }
            get { return _last_upd_date; }
        }

        public string IsOnlineCheck
        {
            get
            {
                return _IsOnlineCheck;
            }

            set
            {
                _IsOnlineCheck = value;
            }
        }

        public string IsMagnetic
        {
            get
            {
                return _IsMagnetic;
            }

            set
            {
                _IsMagnetic = value;
            }
        }

        public decimal Longitude
        {
            get
            {
                return _Longitude;
            }

            set
            {
                _Longitude = value;
            }
        }

        public decimal Latitude
        {
            get
            {
                return _Latitude;
            }

            set
            {
                _Latitude = value;
            }
        }
        #endregion Model

    }
}


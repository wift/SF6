﻿using System;
using EGMNGS.Common;
namespace EGMNGS.Model
{
    /// <summary>
    /// 供电局用户在线录入设备检修、退役设备的气体回收计划申请，并提交。
    /// </summary>
    [Serializable]
    public partial class ApplRecyGasReg
    {
        public ApplRecyGasReg()
        {
        }
        public ApplRecyGasReg(string actionCode)
            : base()
        {
            if (actionCode.Equals(GlobalConstants.ActionModeAdd))
            {
                _recycode = ComServies.GetCode("HS", DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString("D2"));

            }
        }
        #region Model
        private int _oid;
        private string _recycode;
        private string _year = DateTime.Now.Year.ToString();
        private string _month = DateTime.Now.Month.ToString();
        private string _powersupplycode;
        private string _powersupplyname;
        private string _convertstationcode;
        private string _convertstationname;
        private decimal? _amountrecovery = 0;
        private int? _cylindercount = 0;
        private string _recoverydesc;
        private string _contactsname;
        private string _officetel;
        private string _phonenum;
        private string _status;
        private string _registrantoid;
        private DateTime? _registrantdate;
        private string _auditoroid;
        private DateTime? _auditsdate;
        private string _recycleroid;
        private DateTime? _recydate;
        private string _created_by;
        private DateTime _created_date;
        private string _last_upd_by;
        private DateTime _last_upd_date;
        /// <summary>
        /// 
        /// </summary>
        public int OID
        {
            set { _oid = value; }
            get { return _oid; }
        }
        /// <summary>
        /// 回收编号
        /// </summary>
        public string RecyCode
        {
            set { _recycode = value; }
            get { return _recycode; }
        }
        /// <summary>
        /// 年度
        /// </summary>
        public string Year
        {
            set { _year = value; }
            get { return _year; }
        }
        /// <summary>
        /// 月度
        /// </summary>
        public string Month
        {
            set { _month = value; }
            get { return _month; }
        }
        /// <summary>
        /// 供电局编码
        /// </summary>
        public string PowerSupplyCode
        {
            set { _powersupplycode = value; }
            get { return _powersupplycode; }
        }
        /// <summary>
        /// 供电局名称
        /// </summary>
        public string PowerSupplyName
        {
            set { _powersupplyname = value; }
            get { return _powersupplyname; }
        }
        /// <summary>
        /// 变电站编码
        /// </summary>
        public string ConvertStationCode
        {
            set { _convertstationcode = value; }
            get { return _convertstationcode; }
        }
        /// <summary>
        /// 变电站名称
        /// </summary>
        public string ConvertStationName
        {
            set { _convertstationname = value; }
            get { return _convertstationname; }
        }
        /// <summary>
        /// 回收量（KG）
        /// </summary>
        public decimal? AmountRecovery
        {
            set { _amountrecovery = value; }
            get { return _amountrecovery; }
        }
        /// <summary>
        /// 钢瓶数
        /// </summary>
        public int? CylinderCount
        {
            set { _cylindercount = value; }
            get { return _cylindercount; }
        }
        /// <summary>
        /// 回收说明
        /// </summary>
        public string RecoveryDesc
        {
            set { _recoverydesc = value; }
            get { return _recoverydesc; }
        }
        /// <summary>
        /// 联系人
        /// </summary>
        public string ContactsName
        {
            set { _contactsname = value; }
            get { return _contactsname; }
        }
        /// <summary>
        /// 办公电话
        /// </summary>
        public string OfficeTel
        {
            set { _officetel = value; }
            get { return _officetel; }
        }
        /// <summary>
        /// 手机电话
        /// </summary>
        public string PhoneNum
        {
            set { _phonenum = value; }
            get { return _phonenum; }
        }
        /// <summary>
        /// 状态
        /// </summary>
        public string Status
        {
            set { _status = value; }
            get { return _status; }
        }
        /// <summary>
        /// 登记人ID
        /// </summary>
        public string RegistrantOID
        {
            set { _registrantoid = value; }
            get { return _registrantoid; }
        }
        /// <summary>
        /// 登记日期
        /// </summary>
        public DateTime? RegistrantDate
        {
            set { _registrantdate = value; }
            get { return _registrantdate; }
        }
        /// <summary>
        /// 审核人ID
        /// </summary>
        public string AuditorOID
        {
            set { _auditoroid = value; }
            get { return _auditoroid; }
        }
        /// <summary>
        /// 审核日期
        /// </summary>
        public DateTime? AuditsDate
        {
            set { _auditsdate = value; }
            get { return _auditsdate; }
        }
        /// <summary>
        /// 回收员ID
        /// </summary>
        public string RecyclerOID
        {
            set { _recycleroid = value; }
            get { return _recycleroid; }
        }
        /// <summary>
        /// 回收日期
        /// </summary>
        public DateTime? RecyDate
        {
            set { _recydate = value; }
            get { return _recydate; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string CREATED_BY
        {
            set { _created_by = value; }
            get { return _created_by; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime CREATED_DATE
        {
            set { _created_date = value; }
            get { return _created_date; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string LAST_UPD_BY
        {
            set { _last_upd_by = value; }
            get { return _last_upd_by; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime LAST_UPD_DATE
        {
            set { _last_upd_date = value; }
            get { return _last_upd_date; }
        }
        #endregion Model

    }
}


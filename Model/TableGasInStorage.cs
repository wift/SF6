﻿using System;
using EGMNGS.Common;
namespace EGMNGS.Model
{
    /// <summary>
    /// 对气体分别进行回收气体入库、待检气体入库和合格气体入库进行操作
    /// </summary>
    [Serializable]
    public partial class TableGasInStorage
    {
        public TableGasInStorage()
        {

        }

        public TableGasInStorage(string actionCode)
            : base()
        {
            if (actionCode.Equals(GlobalConstants.ActionModeAdd))
            {
                _code = EGMNGS.Common.ComServies.GetCode("RK", DateTime.Now.Year.ToString() +
    DateTime.Now.Month.ToString("D2"));
            }

        }
        #region Model
        private int _oid;
        private string _code;
        private string _year = DateTime.Now.Year.ToString();
        private string _month = DateTime.Now.Month.ToString();
        private string _gastype;
        private string _businesscode;
        private string _gassourse;
        private decimal? _amountinput=0;
        private string _status;
        private string _registrantoid;
        private DateTime? _registrantdate;
        private string _created_by;
        private DateTime _created_date;
        private string _last_upd_by;
        private DateTime _last_upd_date;

        /// <summary>
        /// 
        /// </summary>
        public int OID
        {
            set { _oid = value; }
            get { return _oid; }
        }
        /// <summary>
        /// 入库编号
        /// </summary>
        public string Code
        {
            set { _code = value; }
            get { return _code; }
        }
        /// <summary>
        /// 年度
        /// </summary>
        public string Year
        {
            set { _year = value; }
            get { return _year; }
        }
        /// <summary>
        /// 月度
        /// </summary>
        public string Month
        {
            set { _month = value; }
            get { return _month; }
        }
        /// <summary>
        /// 气体类别
        /// </summary>
        public string GasType
        {
            set { _gastype = value; }
            get { return _gastype; }
        }
        /// <summary>
        /// 业务单号
        /// </summary>
        public string BusinessCode
        {
            set { _businesscode = value; }
            get { return _businesscode; }
        }
        /// <summary>
        /// 气体来源
        /// </summary>
        public string GasSourse
        {
            set { _gassourse = value; }
            get { return _gassourse; }
        }
        /// <summary>
        /// 入库量（KG）
        /// </summary>
        public decimal? AmountInput
        {
            set { _amountinput = value; }
            get { return _amountinput; }
        }
        /// <summary>
        /// 状态
        /// </summary>
        public string Status
        {
            set { _status = value; }
            get { return _status; }
        }
        /// <summary>
        /// 登记人ID
        /// </summary>
        public string registrantOID
        {
            set { _registrantoid = value; }
            get { return _registrantoid; }
        }
        /// <summary>
        /// 登记日期
        /// </summary>
        public DateTime? RegistrantDate
        {
            set { _registrantdate = value; }
            get { return _registrantdate; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string CREATED_BY
        {
            set { _created_by = value; }
            get { return _created_by; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime CREATED_DATE
        {
            set { _created_date = value; }
            get { return _created_date; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string LAST_UPD_BY
        {
            set { _last_upd_by = value; }
            get { return _last_upd_by; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime LAST_UPD_DATE
        {
            set { _last_upd_date = value; }
            get { return _last_upd_date; }
        }
        #endregion Model

    }
}


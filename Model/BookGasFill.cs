﻿using System;
using EGMNGS.Common;
namespace EGMNGS.Model
{
    /// <summary>
    /// 对回收气体和净化后气体进行装瓶管理。
    /// </summary>
    [Serializable]
    public partial class BookGasFill
    {
        public BookGasFill()
        {

        }
        public BookGasFill(string actionCode)
            : base()
        {
            if (actionCode.Equals(GlobalConstants.ActionModeAdd))
            {
                _fillgascode = ComServies.GetCode("CQ", DateTime.Now.Year.ToString() +

  DateTime.Now.Month.ToString("D2"));
                _gascode = ComServies.GetCode("QT", DateTime.Now.Year.ToString() +

DateTime.Now.Month.ToString("D2"));

            }
        }


        #region Model
        private int _oid;
        private string _fillgascode;
        private string _year = DateTime.Now.Year.ToString();
        private string _month = DateTime.Now.Month.ToString();
        private string _businesscode;
        private string _cylindercode;
        private string _gascode;
        private string _gastype;
        private string _gasstatus;
        private string _cylinderstatus;
        private decimal? _amountgas;
        private string _registrantoid;
        private DateTime? _registrantdate;
        private string _created_by;
        private DateTime _created_date;
        private string _last_upd_by;
        private DateTime _last_upd_date;
        private string _status;
        private BookCylinderInfo bookCylinderInfoModel = new BookCylinderInfo();

        public BookCylinderInfo BookCylinderInfoModel
        {
            get { return bookCylinderInfoModel; }
            set { bookCylinderInfoModel = value; }
        }
        
     
        public string Status
        {
            get { return _status; }
            set { _status = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int OID
        {
            set { _oid = value; }
            get { return _oid; }
        }
        /// <summary>
        /// 充气编号
        /// </summary>
        public string FillGasCode
        {
            set { _fillgascode = value; }
            get { return _fillgascode; }
        }
        /// <summary>
        /// 年度
        /// </summary>
        public string Year
        {
            set { _year = value; }
            get { return _year; }
        }
        /// <summary>
        /// 月度
        /// </summary>
        public string Month
        {
            set { _month = value; }
            get { return _month; }
        }
        /// <summary>
        /// 业务单号
        /// </summary>
        public string BusinessCode
        {
            set { _businesscode = value; }
            get { return _businesscode; }
        }
        /// <summary>
        /// 钢瓶编码
        /// </summary>
        public string CylinderCode
        {
            set { _cylindercode = value; }
            get { return _cylindercode; }
        }
        /// <summary>
        /// 气体编码
        /// </summary>
        public string GasCode
        {
            set { _gascode = value; }
            get { return _gascode; }
        }
        /// <summary>
        /// 气体类别
        /// </summary>
        public string GasType
        {
            set { _gastype = value; }
            get { return _gastype; }
        }
        /// <summary>
        /// 气体状态
        /// </summary>
        public string GasStatus
        {
            set { _gasstatus = value; }
            get { return _gasstatus; }
        }
        /// <summary>
        /// 钢瓶状态
        /// </summary>
        public string CylinderStatus
        {
            set { _cylinderstatus = value; }
            get { return _cylinderstatus; }
        }
        /// <summary>
        /// 气量（KG）
        /// </summary>
        public decimal? AmountGas
        {
            set { _amountgas = value; }
            get { return _amountgas; }
        }
        /// <summary>
        /// 登记人OID
        /// </summary>
        public string RegistrantOID
        {
            set { _registrantoid = value; }
            get { return _registrantoid; }
        }
        /// <summary>
        /// 登记日期
        /// </summary>
        public DateTime? RegistrantDate
        {
            set { _registrantdate = value; }
            get { return _registrantdate; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string CREATED_BY
        {
            set { _created_by = value; }
            get { return _created_by; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime CREATED_DATE
        {
            set { _created_date = value; }
            get { return _created_date; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string LAST_UPD_BY
        {
            set { _last_upd_by = value; }
            get { return _last_upd_by; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime LAST_UPD_DATE
        {
            set { _last_upd_date = value; }
            get { return _last_upd_date; }
        }
        #endregion Model

    }
}


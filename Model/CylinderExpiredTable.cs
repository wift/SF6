﻿using System;
namespace EGMNGS.Model
{
	/// <summary>
	/// 登记钢瓶报废记录。
	/// </summary>
	[Serializable]
	public partial class CylinderExpiredTable
	{
		public CylinderExpiredTable()
		{}
		#region Model
		private int _oid;
		private string _cylindercode;
		private string _remarksdesc;
		private string _status;
		private string _registantsoid;
		private DateTime? _registantsdate;
		private DateTime? _expireddate;
		private string _created_by;
		private DateTime _created_date;
		private string _last_upd_by;
		private DateTime _last_upd_date;
		/// <summary>
		/// 
		/// </summary>
		public int OID
		{
			set{ _oid=value;}
			get{return _oid;}
		}
		/// <summary>
		/// 钢瓶编码
		/// </summary>
		public string CylinderCode
		{
			set{ _cylindercode=value;}
			get{return _cylindercode;}
		}
		/// <summary>
		/// 备注说明
		/// </summary>
		public string RemarksDesc
		{
			set{ _remarksdesc=value;}
			get{return _remarksdesc;}
		}
		/// <summary>
		/// 状态
		/// </summary>
		public string Status
		{
			set{ _status=value;}
			get{return _status;}
		}
		/// <summary>
		/// 登记人ID
		/// </summary>
		public string RegistantsOID
		{
			set{ _registantsoid=value;}
			get{return _registantsoid;}
		}
		/// <summary>
		/// 登记日期
		/// </summary>
		public DateTime? RegistantsDate
		{
			set{ _registantsdate=value;}
			get{return _registantsdate;}
		}
		/// <summary>
		/// 报废日期
		/// </summary>
		public DateTime? ExpiredDate
		{
			set{ _expireddate=value;}
			get{return _expireddate;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string CREATED_BY
		{
			set{ _created_by=value;}
			get{return _created_by;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime CREATED_DATE
		{
			set{ _created_date=value;}
			get{return _created_date;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string LAST_UPD_BY
		{
			set{ _last_upd_by=value;}
			get{return _last_upd_by;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime LAST_UPD_DATE
		{
			set{ _last_upd_date=value;}
			get{return _last_upd_date;}
		}
		#endregion Model

	}
}


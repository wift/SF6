﻿using System;
using EGMNGS.Common;
namespace EGMNGS.Model
{
    /// <summary>
    /// 对气体净化处理后进行批次数据登记。
    /// </summary>
    [Serializable]
    public partial class RegGasBatch
    {
        public RegGasBatch()
        {

        }
        public RegGasBatch(string actionCode)
            : base()
        {
            if (actionCode.Equals(GlobalConstants.ActionModeAdd))
            {
                _batchcode = ComServies.GetCode("PC", DateTime.Now.Year.ToString() +
  DateTime.Now.Month.ToString("D2"));
            }

        }
        #region Model
        private int _oid;
        private string _batchcode;
        private string _year = DateTime.Now.Year.ToString();
        private string _month = DateTime.Now.Month.ToString();
        private string _processequipment;
        private decimal? _amountgas;
        private decimal? _sfcontent;
        private decimal? _aircontent;
        private decimal? _sfht;
        private decimal? _wet;
        private decimal? _acidity;
        private decimal? _ksjfhw;
        private decimal? _kwy;
        private string _status;
        private string _registantsoid;
        private DateTime? _registantsdate;
        private string _created_by;
        private DateTime _created_date;
        private string _last_upd_by;
        private DateTime _last_upd_date;
        private string isPass;

        /// <summary>
        /// 初检是否合格
        /// </summary>
        public string IsPass
        {
            get { return isPass; }
            set { isPass = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int OID
        {
            set { _oid = value; }
            get { return _oid; }
        }
        /// <summary>
        /// 批次编号
        /// </summary>
        public string BatchCode
        {
            set { _batchcode = value; }
            get { return _batchcode; }
        }
        /// <summary>
        /// 年度
        /// </summary>
        public string Year
        {
            set { _year = value; }
            get { return _year; }
        }
        /// <summary>
        /// 月度
        /// </summary>
        public string Month
        {
            set { _month = value; }
            get { return _month; }
        }
        /// <summary>
        /// 处理设备
        /// </summary>
        public string ProcessEquipment
        {
            set
            {
                _processequipment = value;
                if (_batchcode.Length == 12)
                {
                    _batchcode += _processequipment;
                }

            }
            get { return _processequipment; }
        }
        /// <summary>
        /// 气量（KG）
        /// </summary>
        public decimal? AmountGas
        {
            set { _amountgas = value; }
            get { return _amountgas; }
        }
        /// <summary>
        /// 六氟化硫含量
        /// </summary>
        public decimal? SFContent
        {
            set { _sfcontent = value; }
            get { return _sfcontent; }
        }
        /// <summary>
        /// 空气含量
        /// </summary>
        public decimal? AirContent
        {
            set { _aircontent = value; }
            get { return _aircontent; }
        }
        /// <summary>
        /// 四氟化碳
        /// </summary>
        public decimal? SFHT
        {
            set { _sfht = value; }
            get { return _sfht; }
        }
        /// <summary>
        /// 水分
        /// </summary>
        public decimal? Wet
        {
            set { _wet = value; }
            get { return _wet; }
        }
        /// <summary>
        /// 酸度
        /// </summary>
        public decimal? Acidity
        {
            set { _acidity = value; }
            get { return _acidity; }
        }
        /// <summary>
        /// 可水解氟化物
        /// </summary>
        public decimal? KSJFHW
        {
            set { _ksjfhw = value; }
            get { return _ksjfhw; }
        }
        /// <summary>
        /// 矿物油
        /// </summary>
        public decimal? KWY
        {
            set { _kwy = value; }
            get { return _kwy; }
        }
        /// <summary>
        /// 状态
        /// </summary>
        public string Status
        {
            set { _status = value; }
            get { return _status; }
        }
        /// <summary>
        /// 登记人OID
        /// </summary>
        public string RegistantsOID
        {
            set { _registantsoid = value; }
            get { return _registantsoid; }
        }
        /// <summary>
        /// 登记日期
        /// </summary>
        public DateTime? RegistantsDate
        {
            set { _registantsdate = value; }
            get { return _registantsdate; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string CREATED_BY
        {
            set { _created_by = value; }
            get { return _created_by; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime CREATED_DATE
        {
            set { _created_date = value; }
            get { return _created_date; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string LAST_UPD_BY
        {
            set { _last_upd_by = value; }
            get { return _last_upd_by; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime LAST_UPD_DATE
        {
            set { _last_upd_date = value; }
            get { return _last_upd_date; }
        }
        #endregion Model

    }
}


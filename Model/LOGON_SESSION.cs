﻿using System;
namespace EGMNGS.Model
{
	/// <summary>
	/// 登记各供电局的变电站基础信息。
	/// </summary>
	[Serializable]
	public partial class LOGON_SESSION
	{
		public LOGON_SESSION()
		{}
		#region Model
		private DateTime _logon_date;
		private string _user_id;
		private DateTime? _last_action_date;
		private DateTime? _logout_date;
		private string _fail_reason;
		private DateTime? _session_timeout_date;
		/// <summary>
		/// 
		/// </summary>
		public DateTime LOGON_DATE
		{
			set{ _logon_date=value;}
			get{return _logon_date;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string USER_ID
		{
			set{ _user_id=value;}
			get{return _user_id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? LAST_ACTION_DATE
		{
			set{ _last_action_date=value;}
			get{return _last_action_date;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? LOGOUT_DATE
		{
			set{ _logout_date=value;}
			get{return _logout_date;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string FAIL_REASON
		{
			set{ _fail_reason=value;}
			get{return _fail_reason;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? SESSION_TIMEOUT_DATE
		{
			set{ _session_timeout_date=value;}
			get{return _session_timeout_date;}
		}
		#endregion Model

	}
}


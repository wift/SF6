﻿using System;
namespace EGMNGS.Model
{
	/// <summary>
	/// 回收气体明细表
	/// </summary>
	[Serializable]
	public partial class DetailsOfRecycleGas
	{
		public DetailsOfRecycleGas()
		{}
		#region Model
		private int _oid;
		private string _infodevpara_devcode;
		private string _infodevpara_devname;
		private string _gastype;
		private decimal? _amountrecovery;
		private string _recoverydesc;
		private string _cylindercode;
		private string _created_by;
		private DateTime _created_date;
		private string _last_upd_by;
		private DateTime _last_upd_date;
        private string _applRecyGasReg_RecyCode;

        public string ApplRecyGasReg_RecyCode
        {
            get { return _applRecyGasReg_RecyCode; }
            set { _applRecyGasReg_RecyCode = value; }
        }
		/// <summary>
		/// 
		/// </summary>
		public int OID
		{
			set{ _oid=value;}
			get{return _oid;}
		}
		/// <summary>
		/// 设备编码
		/// </summary>
		public string InfoDevPara_DevCode
		{
			set{ _infodevpara_devcode=value;}
			get{return _infodevpara_devcode;}
		}
		/// <summary>
		/// 设备名称
		/// </summary>
		public string InfoDevPara_DevName
		{
			set{ _infodevpara_devname=value;}
			get{return _infodevpara_devname;}
		}
		/// <summary>
		/// 气体类别
		/// </summary>
		public string GasType
		{
			set{ _gastype=value;}
			get{return _gastype;}
		}
		/// <summary>
		/// 回收量（KG）
		/// </summary>
		public decimal? AmountRecovery
		{
			set{ _amountrecovery=value;}
			get{return _amountrecovery;}
		}
		/// <summary>
		/// 回收说明
		/// </summary>
		public string RecoveryDesc
		{
			set{ _recoverydesc=value;}
			get{return _recoverydesc;}
		}
		/// <summary>
		/// 钢瓶编码
		/// </summary>
		public string CylinderCode
		{
			set{ _cylindercode=value;}
			get{return _cylindercode;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string CREATED_BY
		{
			set{ _created_by=value;}
			get{return _created_by;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime CREATED_DATE
		{
			set{ _created_date=value;}
			get{return _created_date;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string LAST_UPD_BY
		{
			set{ _last_upd_by=value;}
			get{return _last_upd_by;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime LAST_UPD_DATE
		{
			set{ _last_upd_date=value;}
			get{return _last_upd_date;}
		}
		#endregion Model

	}
}


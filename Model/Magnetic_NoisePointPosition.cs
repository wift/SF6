﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EGMNGS.Model
{
    [Table("Magnetic_NoisePointPosition")]
    public class Magnetic_NoisePointPosition
    {
        #region Model
        private int _oid;
        private int? _magnetic_checkpointtemplatesb_OID;
        private string _checkpointposition;
        private string _checkpointpositioncode;
        private decimal? _leqdba;
        private string _remarks;
        private string _created_by;
        private DateTime? _created_date;
        private string _last_upd_by;
        private DateTime? _last_upd_date;
        /// <summary>
        /// 
        /// </summary>
        public int OID
        {
            set { _oid = value; }
            get { return _oid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? Magnetic_CheckPointTemplatesB_OID
        {
            set { _magnetic_checkpointtemplatesb_OID = value; }
            get { return _magnetic_checkpointtemplatesb_OID; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string CheckPointPosition
        {
            set { _checkpointposition = value; }
            get { return _checkpointposition; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string CheckPointPositionCode
        {
            set { _checkpointpositioncode = value; }
            get { return _checkpointpositioncode; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? LeqdBA
        {
            set { _leqdba = value; }
            get { return _leqdba; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Remarks
        {
            set { _remarks = value; }
            get { return _remarks; }
        }
        /// <summary>
        /// 创建人
        /// </summary>
        public string CREATED_BY
        {
            set { _created_by = value; }
            get { return _created_by; }
        }
        /// <summary>
        /// 创建日期
        /// </summary>
        public DateTime? CREATED_DATE
        {
            set { _created_date = value; }
            get { return _created_date; }
        }
        /// <summary>
        /// 最后更新人
        /// </summary>
        public string LAST_UPD_BY
        {
            set { _last_upd_by = value; }
            get { return _last_upd_by; }
        }
        /// <summary>
        /// 最后更新日期
        /// </summary>
        public DateTime? LAST_UPD_DATE
        {
            set { _last_upd_date = value; }
            get { return _last_upd_date; }
        }
        #endregion Model
    }
}

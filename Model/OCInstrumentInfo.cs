﻿using System;
namespace EGMNGS.Model
{
	/// <summary>
	/// OCInstrumentInfo:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class OCInstrumentInfo
	{
		public OCInstrumentInfo()
		{}
		#region Model
		private int _oid;
		private string _checkpointcode;
		private string _checkpointname;
		private string _instrumentcode;
		private string _instrumentname;
		private string _runstatus;
		private string _intallpostion;
		private string _belongequipment;
		private string _belongpowerstation;
		private string _voltagelevel;
		private string _manufacturer;
		private DateTime? _rundate;
		private DateTime? _calibrationdate;
		private string _controlrunstatus;
		private DateTime? _controlrunfromdate;
		private DateTime? _controlruntodate;
		private int? _cycle;
		private decimal? _controlpressuremin;
		private decimal? _controlpressuremax;
		private decimal? _controltemperturemin;
		private decimal? _controltemperturemax;
		private decimal? _controlmicrowater;
		private decimal? _controlso2;
		private decimal? _controlco;
		private decimal? _controlcf4;
		private decimal? _controlso2f2;
		private decimal? _controlsof2;
		private decimal? _controlcs2;
		private decimal? _controlhf;
		private string _created_by;
		private DateTime? _created_date= DateTime.Now;
		private string _last_upd_by;
		private DateTime? _last_upd_date= DateTime.Now;
		private string _belongequipmentname;
		private string _belongpowerstationname;
		private decimal? _controlpressuremins;
		private decimal? _controlpressuremaxs;
		private decimal? _controltemperturemins;
		private decimal? _controltemperturemaxs;
		private decimal? _controlmicrowaters;
		private decimal? _controlso2s;
		private decimal? _controlcos;
		private decimal? _controlcf4s;
		private decimal? _controlso2f2s;
		private decimal? _controlsof2s;
		private decimal? _controlcs2s;
		private decimal? _controlhfs;
		private int? _altercount;

        private decimal? _ph2s;
        private decimal? _mh2s;
        private decimal? _pcos;
        private decimal? _mcos;
        private decimal? _pc2f6;
        private decimal? _mc2f6;
        private decimal? _pc3f8;
        private decimal? _mc3f8;
        /// <summary>
        /// 
        /// </summary>
        public int OID
		{
			set{ _oid=value;}
			get{return _oid;}
		}
		/// <summary>
		/// 测点编码
		/// </summary>
		public string CheckPointCode
		{
			set{ _checkpointcode=value;}
			get{return _checkpointcode;}
		}
		/// <summary>
		/// 测点名称
		/// </summary>
		public string CheckPointName
		{
			set{ _checkpointname=value;}
			get{return _checkpointname;}
		}
		/// <summary>
		/// 仪器编码
		/// </summary>
		public string InstrumentCode
		{
			set{ _instrumentcode=value;}
			get{return _instrumentcode;}
		}
		/// <summary>
		/// 仪器名称
		/// </summary>
		public string InstrumentName
		{
			set{ _instrumentname=value;}
			get{return _instrumentname;}
		}
		/// <summary>
		/// 运行状态
		/// </summary>
		public string RunStatus
		{
			set{ _runstatus=value;}
			get{return _runstatus;}
		}
		/// <summary>
		/// 安装位置
		/// </summary>
		public string IntallPostion
		{
			set{ _intallpostion=value;}
			get{return _intallpostion;}
		}
		/// <summary>
		/// 所属设备
		/// </summary>
		public string BelongEquipment
		{
			set{ _belongequipment=value;}
			get{return _belongequipment;}
		}
		/// <summary>
		/// 所属变电站
		/// </summary>
		public string BelongPowerStation
		{
			set{ _belongpowerstation=value;}
			get{return _belongpowerstation;}
		}
		/// <summary>
		/// 电压等级
		/// </summary>
		public string VoltageLevel
		{
			set{ _voltagelevel=value;}
			get{return _voltagelevel;}
		}
		/// <summary>
		/// 生产厂家
		/// </summary>
		public string Manufacturer
		{
			set{ _manufacturer=value;}
			get{return _manufacturer;}
		}
		/// <summary>
		/// 投运日期
		/// </summary>
		public DateTime? RunDate
		{
			set{ _rundate=value;}
			get{return _rundate;}
		}
		/// <summary>
		/// 校准日期
		/// </summary>
		public DateTime? CalibrationDate
		{
			set{ _calibrationdate=value;}
			get{return _calibrationdate;}
		}
		/// <summary>
		/// 运行状态
		/// </summary>
		public string ControlRunStatus
		{
			set{ _controlrunstatus=value;}
			get{return _controlrunstatus;}
		}
		/// <summary>
		/// 运行开始时间
		/// </summary>
		public DateTime? ControlRunFromDate
		{
			set{ _controlrunfromdate=value;}
			get{return _controlrunfromdate;}
		}
		/// <summary>
		/// 运行结束时间
		/// </summary>
		public DateTime? ControlRunToDate
		{
			set{ _controlruntodate=value;}
			get{return _controlruntodate;}
		}
		/// <summary>
		/// 周期
		/// </summary>
		public int? Cycle
		{
			set{ _cycle=value;}
			get{return _cycle;}
		}
		/// <summary>
		/// 压力最小值
		/// </summary>
		public decimal? ControlPressureMin
		{
			set{ _controlpressuremin=value;}
			get{return _controlpressuremin;}
		}
		/// <summary>
		/// 压力最大值
		/// </summary>
		public decimal? ControlPressureMax
		{
			set{ _controlpressuremax=value;}
			get{return _controlpressuremax;}
		}
		/// <summary>
		/// 温度最小值
		/// </summary>
		public decimal? ControlTempertureMin
		{
			set{ _controltemperturemin=value;}
			get{return _controltemperturemin;}
		}
		/// <summary>
		/// 温度最大值
		/// </summary>
		public decimal? ControlTempertureMax
		{
			set{ _controltemperturemax=value;}
			get{return _controltemperturemax;}
		}
		/// <summary>
		/// 微水
		/// </summary>
		public decimal? ControlMicroWater
		{
			set{ _controlmicrowater=value;}
			get{return _controlmicrowater;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? ControlSO2
		{
			set{ _controlso2=value;}
			get{return _controlso2;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? ControlCO
		{
			set{ _controlco=value;}
			get{return _controlco;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? ControlCF4
		{
			set{ _controlcf4=value;}
			get{return _controlcf4;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? ControlSO2F2
		{
			set{ _controlso2f2=value;}
			get{return _controlso2f2;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? ControlSOF2
		{
			set{ _controlsof2=value;}
			get{return _controlsof2;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? ControlCS2
		{
			set{ _controlcs2=value;}
			get{return _controlcs2;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? ControlHF
		{
			set{ _controlhf=value;}
			get{return _controlhf;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string CREATED_BY
		{
			set{ _created_by=value;}
			get{return _created_by;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? CREATED_DATE
		{
			set{ _created_date=value;}
			get{return _created_date;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string LAST_UPD_BY
		{
			set{ _last_upd_by=value;}
			get{return _last_upd_by;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? LAST_UPD_DATE
		{
			set{ _last_upd_date=value;}
			get{return _last_upd_date;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string BelongEquipmentName
		{
			set{ _belongequipmentname=value;}
			get{return _belongequipmentname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string BelongPowerStationName
		{
			set{ _belongpowerstationname=value;}
			get{return _belongpowerstationname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? ControlPressureMins
		{
			set{ _controlpressuremins=value;}
			get{return _controlpressuremins;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? ControlPressureMaxs
		{
			set{ _controlpressuremaxs=value;}
			get{return _controlpressuremaxs;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? ControlTempertureMins
		{
			set{ _controltemperturemins=value;}
			get{return _controltemperturemins;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? ControlTempertureMaxs
		{
			set{ _controltemperturemaxs=value;}
			get{return _controltemperturemaxs;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? ControlMicroWaters
		{
			set{ _controlmicrowaters=value;}
			get{return _controlmicrowaters;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? ControlSO2s
		{
			set{ _controlso2s=value;}
			get{return _controlso2s;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? ControlCOs
		{
			set{ _controlcos=value;}
			get{return _controlcos;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? ControlCF4s
		{
			set{ _controlcf4s=value;}
			get{return _controlcf4s;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? ControlSO2F2s
		{
			set{ _controlso2f2s=value;}
			get{return _controlso2f2s;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? ControlSOF2s
		{
			set{ _controlsof2s=value;}
			get{return _controlsof2s;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? ControlCS2s
		{
			set{ _controlcs2s=value;}
			get{return _controlcs2s;}
		}
		/// <summary>
		/// 
		/// </summary>
		public decimal? ControlHFs
		{
			set{ _controlhfs=value;}
			get{return _controlhfs;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? AlterCount
		{
			set{ _altercount=value;}
			get{return _altercount;}
		}

        /// <summary>
        /// 
        /// </summary>
        public decimal? PH2S
        {
            set { _ph2s = value; }
            get { return _ph2s; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? MH2S
        {
            set { _mh2s = value; }
            get { return _mh2s; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? PCOS
        {
            set { _pcos = value; }
            get { return _pcos; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? MCOS
        {
            set { _mcos = value; }
            get { return _mcos; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? PC2F6
        {
            set { _pc2f6 = value; }
            get { return _pc2f6; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? MC2F6
        {
            set { _mc2f6 = value; }
            get { return _mc2f6; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? PC3F8
        {
            set { _pc3f8 = value; }
            get { return _pc3f8; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? MC3F8
        {
            set { _mc3f8 = value; }
            get { return _mc3f8; }
        }
        #endregion Model

    }
}


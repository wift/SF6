﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using Oracle.DataAccess.Client;
using System.Data.SqlClient;
using RM.Common.DotNetCode;

namespace RM.DataBase.DataBase.Common
{
    /// <summary>
    /// IDbHelperExpand
    /// 数据库访问扩展接口
    /// 版本：1.0
    public class IDbHelperExpand
    {
        /// <summary>
        /// 创建系统异常日志
        /// </summary>
        protected LogHelper Logger = new LogHelper("IDbHelperExpand");

        #region Oracle
        /// <summary>
        /// 利用Net OracleBulkCopy 批量导入数据库,速度超快
        /// </summary>
        /// <param name="dt">内存表</param>
        /// <param name="connectionString">连接字符串</param>
        /// <returns></returns>
        public bool OracleBulkCopyImport(DataTable dt, string connectionString)
        {
            try
            {
                using (OracleConnection connection = new OracleConnection(connectionString))
                {
                    connection.Open();
                    OracleTransaction trans = connection.BeginTransaction();
                    using (OracleBulkCopy bulkCopy = new OracleBulkCopy(connection))
                    {
                        //设置源表名称
                        bulkCopy.DestinationTableName = dt.TableName;
                        //设置超时限制
                        bulkCopy.BulkCopyTimeout = 1000;
                        //要写入列
                        foreach (DataColumn dtColumn in dt.Columns)
                        {
                            bulkCopy.ColumnMappings.Add(dtColumn.ColumnName, dtColumn.ColumnName);
                        }
                        try
                        {
                            // 写入
                            bulkCopy.WriteToServer(dt);
                            // 提交事务
                            trans.Commit();
                            return true;
                        }
                        catch
                        {
                            trans.Rollback();
                            bulkCopy.Close();
                            return false;
                        }
                        finally
                        {
                            connection.Close();
                            bulkCopy.Close();
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Logger.WriteLog("-----------利用Net OracleBulkCopy 批量导入数据库,速度超快-----------\r\n" + e.Message + "\r\n");
                return false;
            }
        }
        #endregion

        #region MsSql
        /// <summary>
        /// 利用Net SqlBulkCopy 批量导入数据库,速度超快
        /// </summary>
        /// <param name="dataTable">源内存数据表</param>
        public bool MsSqlBulkCopyData(DataTable dt, string connectionString)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    SqlTransaction trans = conn.BeginTransaction();

                    SqlBulkCopy sqlbulkCopy = new SqlBulkCopy(conn, SqlBulkCopyOptions.Default, trans);
                    // 设置源表名称
                    sqlbulkCopy.DestinationTableName = dt.TableName;
                    //分几次拷贝
                    //sqlbulkCopy.BatchSize = 10;
                    // 设置超时限制
                    sqlbulkCopy.BulkCopyTimeout = 1000;
                    foreach (DataColumn dtColumn in dt.Columns)
                    {
                        sqlbulkCopy.ColumnMappings.Add(dtColumn.ColumnName, dtColumn.ColumnName);
                    }
                    try
                    {
                        // 写入
                        sqlbulkCopy.WriteToServer(dt);
                        // 提交事务
                        trans.Commit();
                        return true;
                    }
                    catch
                    {
                        trans.Rollback();
                        sqlbulkCopy.Close();
                        return false;
                    }
                    finally
                    {
                        sqlbulkCopy.Close();
                        conn.Close();
                    }
                }
            }
            catch (Exception e)
            {
                Logger.WriteLog("-----------利用Net SqlBulkCopyData 批量导入数据库,速度超快-----------\r\n" + e.Message + "\r\n");
                return false;
            }
        }
        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Wolf.Utility
{
    public class LogHelper
    {
        private static readonly log4net.ILog loginfo = null;

        private static readonly log4net.ILog logerror = null;
        private LogHelper()
        { }
        static LogHelper()
        {

            loginfo = log4net.LogManager.GetLogger("loginfo");
            logerror = log4net.LogManager.GetLogger("logerror");
        }

        private static readonly LogHelper _instance = new LogHelper();
        public static LogHelper Instance
        {
            get
            {
                return _instance;
            }
        }


        public void WriteLog(string info)
        {
            loginfo.Info(info);
        }

        public void ErrorLog(string errorMsg)
        {
            logerror.Error(errorMsg);
        }
        /// <summary>
        /// 错误记录
        /// </summary>
        /// <param name="info">附加信息</param>
        /// <param name="ex">错误</param>
        public void ErrorLog(string info, Exception ex)
        {
            if (!string.IsNullOrEmpty(info) && ex == null)
            {
                logerror.ErrorFormat("【附加信息】 : {0}\n", new object[] { info });
            }
            else if (!string.IsNullOrEmpty(info) && ex != null)
            {
                string errorMsg = BeautyErrorMsg(ex);
                logerror.ErrorFormat("【附加信息】 : {0}\n{1}", new object[] { info, errorMsg });
            }
            else if (string.IsNullOrEmpty(info) && ex != null)
            {
                string errorMsg = BeautyErrorMsg(ex);
                logerror.Error(errorMsg);
            }
        }
        /// <summary>
        /// 美化错误信息
        /// </summary>
        /// <param name="ex">异常</param>
        /// <returns>错误信息</returns>
        private string BeautyErrorMsg(Exception ex)
        {
            string errorMsg = string.Format("【异常类型】：{0} <br>【异常信息】：{1} <br>【堆栈调用】：{2}", new object[] { ex.GetType().Name, ex.Message, ex.StackTrace });
            errorMsg = errorMsg.Replace("\r\n", "<br>");
            errorMsg = errorMsg.Replace("位置", "<strong style=\"color:red\">位置</strong>");
            return errorMsg;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using DapperData;
using EGMNGS.Common;
using EGMNGS.Model;

namespace Megnetic_HandlerLib
{
    public class Megnetic_Handler : IHttpHandler
    {
        /// <summary>
        /// 您将需要在网站的 Web.config 文件中配置此处理程序 
        /// 并向 IIS 注册它，然后才能使用它。有关详细信息，
        /// 请参见下面的链接: http://go.microsoft.com/?linkid=8101007
        /// </summary>
        #region IHttpHandler Members

        public bool IsReusable
        {
            // 如果无法为其他请求重用托管处理程序，则返回 false。
            // 如果按请求保留某些状态信息，则通常这将为 false。
            get { return true; }
        }

        public void ProcessRequest(HttpContext context)
        {
            //在此处写入您的处理程序实现。
            context.Response.ContentType = "application/json";
            string action = context.Request["action"] ?? "";
            string oids = context.Request["oids"] ?? "";//测点ID
            string returnJsonStr = "";
            if (action == "Megnetic_CurrentAnalysisPageList")//分析当前列表
            {
                var results = Megnetic_CurrentAnalysisPageList(oids);
                returnJsonStr = Newtonsoft.Json.JsonConvert.SerializeObject(results);
            }
            else if (action == "Megnetic_CurrentAnalysisPageChart")
            {
                string target = context.Request["target"] ?? "";//测点参数
                returnJsonStr = Megnetic_CurrentAnalysisPageChart(oids, target);
            }
            else if (action == "MonitoringDataDialogForm")
            {
                string oid = context.Request["oid"] ?? "";//OID
                returnJsonStr = GetMagnetic_CheckPointInfo(oid);
            }
            context.Response.Write(returnJsonStr);
            context.Response.Flush();
        }

        /// <summary>
        /// 微信打开地图获取测点信息
        /// </summary>
        /// <param name="oid">主键</param>
        /// <returns></returns>
        private string GetMagnetic_CheckPointInfo(string oid)
        {
            Repository m_Repository = new Repository();
            Magnetic_CheckPointInfo Magnetic_CheckPointInfoObj = m_Repository.GetById<Magnetic_CheckPointInfo>(oid.AsTargetType<int>(0));
            return Newtonsoft.Json.JsonConvert.SerializeObject(Magnetic_CheckPointInfoObj);
        }
        /// <summary>
        /// 当前分析
        /// </summary>
        /// <param name="created_date">搜集时间</param>
        /// <param name="checkPointPosition">测点位置</param>
        /// <returns></returns>
        private DataTable Megnetic_CurrentAnalysisPageList(string oids)
        {
            if (string.IsNullOrWhiteSpace(oids))
            {
                return null;
            }

            DataTable dt = new DataTable();
            string selectsql = @"   SELECT 
 c.ConvartStationName,
 a.CheckPointPosition,
 a.PositionName,
 cast(b.MeasuringDate as varchar(10)) as MeasuringDate,
 b.MeasuringTime,
  ElectricFieldMAX,
  ElectricFieldMIN,
  ElectricFieldRMS,MagneticInductionMAX,
  MagneticInductionMIN,MagneticInductionRMS,
  b.temperature,humidity
 from Magnetic_StationCheckPointMNGChilds a
 LEFT JOIN Magnetic_CheckPointTemplatesB b ON a.Magnetic_CheckPointTemplatesB_OID=b.oid
left join InfoPowerSupplyConvertStation c on c.CovnertStationCode=b.ConvertStationCode
where 1=1 ";

            string wheresql = string.Format(" and a.oid in ({0})", oids);
            dt = ComServies.Query(selectsql + wheresql); ;
            return dt;
        }

        private string Megnetic_CurrentAnalysisPageChart(string checkPointPosition, string target)
        {
            string selectsql = @"   SELECT a.oid,
 c.ConvartStationName,
 a.CheckPointPosition,
 a.PositionName,
 cast(b.MeasuringDate as varchar(10)) as MeasuringDate,
 b.MeasuringTime,
  ElectricFieldMAX,
  ElectricFieldMIN,
  ElectricFieldRMS,MagneticInductionMAX,
  MagneticInductionMIN,MagneticInductionRMS,
  b.Temperature,Humidity
 from Magnetic_StationCheckPointMNGChilds a
 LEFT JOIN Magnetic_CheckPointTemplatesB b ON a.Magnetic_CheckPointTemplatesB_OID=b.oid
left join InfoPowerSupplyConvertStation c on c.CovnertStationCode=b.ConvertStationCode
where 1=1 ";

            string sqlwhere = string.Format(" and a.oid in ({0}) order by a.oid desc", checkPointPosition);

            var returnValue = new { listDatetime = new List<string>(), listChartData = new List<ChartData>() };

            DataTable dt = EGMNGS.Common.ComServies.Query(selectsql + sqlwhere);
           

            List<ChartData> listChartData = new List<ChartData>();
            List<string> listDatetime = new List<string>();

            string[] targetarry = target.Split(',');
            //X轴
            foreach (var item1 in targetarry)
            {
                ChartData itemChartData = new ChartData() { target = item1 };

                listChartData.Add(itemChartData);
            }

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                listDatetime.Add(string.Format("{0}_{1}", dt.Rows[i]["ConvartStationName"].ToString(), dt.Rows[i]["CheckPointPosition"].ToString()));

                for (int k = 0; k < targetarry.Length; k++)
                {
                    string item = targetarry[k];
                    listChartData[k].data.Add(dt.Rows[i][item].AsTargetType<decimal>(0));
                }

            }

            var chartDataSource = new { listDatetime = listDatetime, listChartData = listChartData };

            return Newtonsoft.Json.JsonConvert.SerializeObject(chartDataSource);
        }

        #endregion
    }
}

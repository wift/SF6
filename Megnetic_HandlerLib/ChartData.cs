﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Megnetic_HandlerLib
{
    public class ChartData
    {
         Dictionary<string, string> dictEnum = new Dictionary<string, string>();
  

        private string _name;

        public string name
        {
            set { _name = value; }
            get
            {
                if (string.IsNullOrWhiteSpace(_name))
                {
                    return dictEnum[target];
                }
                else
                {
                    return _name;
                }
            }
        }
    
        public string ChackName { set; get; }
        public string target { get; set; }
        public List<decimal> data { set; get; }
        public ChartData()
        {
            data = new List<decimal>();

            dictEnum.Add("ElectricFieldMAX", "电场强度MAX");
            dictEnum.Add("ElectricFieldMIN", "电场强度MIN");
            dictEnum.Add("ElectricFieldRMS", "电场强度RMS");
            dictEnum.Add("MagneticInductionMAX", "磁感应强度MAX");
            dictEnum.Add("MagneticInductionMIN", "磁感应强度MIN");
            dictEnum.Add("MagneticInductionRMS", "磁感应强度RMS");
            dictEnum.Add("Temperature", "温度");
            dictEnum.Add("Humidity", "湿度");
        }
    }
}

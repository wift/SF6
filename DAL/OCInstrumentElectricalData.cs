﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Maticsoft.DBUtility;//Please add references
namespace EGMNGS.DAL
{
	/// <summary>
	/// 数据访问类:OCInstrumentElectricalData
	/// </summary>
	public partial class OCInstrumentElectricalData
	{
		public OCInstrumentElectricalData()
		{}
		#region  BasicMethod

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
		return DbHelperSQL.GetMaxID("OID", "OCInstrumentElectricalData"); 
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int OID)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from OCInstrumentElectricalData");
			strSql.Append(" where OID=@OID");
			SqlParameter[] parameters = {
					new SqlParameter("@OID", SqlDbType.Int,4)
			};
			parameters[0].Value = OID;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(EGMNGS.Model.OCInstrumentElectricalData model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into OCInstrumentElectricalData(");
			strSql.Append("CheckPointCode,CheckPointName,CollectionDatetime,Pressure,Temperture,OutElectricity,CurrentVal,Voltage,PowerVal,CollectionType,CollectionBy)");
			strSql.Append(" values (");
			strSql.Append("@CheckPointCode,@CheckPointName,@CollectionDatetime,@Pressure,@Temperture,@OutElectricity,@CurrentVal,@Voltage,@PowerVal,@CollectionType,@CollectionBy)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@CheckPointCode", SqlDbType.VarChar,4),
					new SqlParameter("@CheckPointName", SqlDbType.VarChar,50),
					new SqlParameter("@CollectionDatetime", SqlDbType.DateTime),
					new SqlParameter("@Pressure", SqlDbType.Decimal,9),
					new SqlParameter("@Temperture", SqlDbType.Decimal,9),
					new SqlParameter("@OutElectricity", SqlDbType.Decimal,9),
					new SqlParameter("@CurrentVal", SqlDbType.Decimal,9),
					new SqlParameter("@Voltage", SqlDbType.Decimal,9),
					new SqlParameter("@PowerVal", SqlDbType.Decimal,9),
					new SqlParameter("@CollectionType", SqlDbType.VarChar,4),
					new SqlParameter("@CollectionBy", SqlDbType.NVarChar,50)};
			parameters[0].Value = model.CheckPointCode;
			parameters[1].Value = model.CheckPointName;
			parameters[2].Value = model.CollectionDatetime;
			parameters[3].Value = model.Pressure;
			parameters[4].Value = model.Temperture;
			parameters[5].Value = model.OutElectricity;
			parameters[6].Value = model.CurrentVal;
			parameters[7].Value = model.Voltage;
			parameters[8].Value = model.PowerVal;
			parameters[9].Value = model.CollectionType;
			parameters[10].Value = model.CollectionBy;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(EGMNGS.Model.OCInstrumentElectricalData model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update OCInstrumentElectricalData set ");
			strSql.Append("CheckPointCode=@CheckPointCode,");
			strSql.Append("CheckPointName=@CheckPointName,");
			strSql.Append("CollectionDatetime=@CollectionDatetime,");
			strSql.Append("Pressure=@Pressure,");
			strSql.Append("Temperture=@Temperture,");
			strSql.Append("OutElectricity=@OutElectricity,");
			strSql.Append("CurrentVal=@CurrentVal,");
			strSql.Append("Voltage=@Voltage,");
			strSql.Append("PowerVal=@PowerVal,");
			strSql.Append("CollectionType=@CollectionType,");
			strSql.Append("CollectionBy=@CollectionBy");
			strSql.Append(" where OID=@OID");
			SqlParameter[] parameters = {
					new SqlParameter("@CheckPointCode", SqlDbType.VarChar,4),
					new SqlParameter("@CheckPointName", SqlDbType.VarChar,50),
					new SqlParameter("@CollectionDatetime", SqlDbType.DateTime),
					new SqlParameter("@Pressure", SqlDbType.Decimal,9),
					new SqlParameter("@Temperture", SqlDbType.Decimal,9),
					new SqlParameter("@OutElectricity", SqlDbType.Decimal,9),
					new SqlParameter("@CurrentVal", SqlDbType.Decimal,9),
					new SqlParameter("@Voltage", SqlDbType.Decimal,9),
					new SqlParameter("@PowerVal", SqlDbType.Decimal,9),
					new SqlParameter("@CollectionType", SqlDbType.VarChar,4),
					new SqlParameter("@CollectionBy", SqlDbType.NVarChar,50),
					new SqlParameter("@OID", SqlDbType.Int,4)};
			parameters[0].Value = model.CheckPointCode;
			parameters[1].Value = model.CheckPointName;
			parameters[2].Value = model.CollectionDatetime;
			parameters[3].Value = model.Pressure;
			parameters[4].Value = model.Temperture;
			parameters[5].Value = model.OutElectricity;
			parameters[6].Value = model.CurrentVal;
			parameters[7].Value = model.Voltage;
			parameters[8].Value = model.PowerVal;
			parameters[9].Value = model.CollectionType;
			parameters[10].Value = model.CollectionBy;
			parameters[11].Value = model.OID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int OID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from OCInstrumentElectricalData ");
			strSql.Append(" where OID=@OID");
			SqlParameter[] parameters = {
					new SqlParameter("@OID", SqlDbType.Int,4)
			};
			parameters[0].Value = OID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string OIDlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from OCInstrumentElectricalData ");
			strSql.Append(" where OID in ("+OIDlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public EGMNGS.Model.OCInstrumentElectricalData GetModel(int OID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 OID,CheckPointCode,CheckPointName,CollectionDatetime,Pressure,Temperture,OutElectricity,CurrentVal,Voltage,PowerVal,CollectionType,CollectionBy from OCInstrumentElectricalData ");
			strSql.Append(" where OID=@OID");
			SqlParameter[] parameters = {
					new SqlParameter("@OID", SqlDbType.Int,4)
			};
			parameters[0].Value = OID;

			EGMNGS.Model.OCInstrumentElectricalData model=new EGMNGS.Model.OCInstrumentElectricalData();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public EGMNGS.Model.OCInstrumentElectricalData DataRowToModel(DataRow row)
		{
			EGMNGS.Model.OCInstrumentElectricalData model=new EGMNGS.Model.OCInstrumentElectricalData();
			if (row != null)
			{
				if(row["OID"]!=null && row["OID"].ToString()!="")
				{
					model.OID=int.Parse(row["OID"].ToString());
				}
				if(row["CheckPointCode"]!=null)
				{
					model.CheckPointCode=row["CheckPointCode"].ToString();
				}
				if(row["CheckPointName"]!=null)
				{
					model.CheckPointName=row["CheckPointName"].ToString();
				}
				if(row["CollectionDatetime"]!=null && row["CollectionDatetime"].ToString()!="")
				{
					model.CollectionDatetime=DateTime.Parse(row["CollectionDatetime"].ToString());
				}
				if(row["Pressure"]!=null && row["Pressure"].ToString()!="")
				{
					model.Pressure=decimal.Parse(row["Pressure"].ToString());
				}
				if(row["Temperture"]!=null && row["Temperture"].ToString()!="")
				{
					model.Temperture=decimal.Parse(row["Temperture"].ToString());
				}
				if(row["OutElectricity"]!=null && row["OutElectricity"].ToString()!="")
				{
					model.OutElectricity=decimal.Parse(row["OutElectricity"].ToString());
				}
				if(row["CurrentVal"]!=null && row["CurrentVal"].ToString()!="")
				{
					model.CurrentVal=decimal.Parse(row["CurrentVal"].ToString());
				}
				if(row["Voltage"]!=null && row["Voltage"].ToString()!="")
				{
					model.Voltage=decimal.Parse(row["Voltage"].ToString());
				}
				if(row["PowerVal"]!=null && row["PowerVal"].ToString()!="")
				{
					model.PowerVal=decimal.Parse(row["PowerVal"].ToString());
				}
				if(row["CollectionType"]!=null)
				{
					model.CollectionType=row["CollectionType"].ToString();
				}
				if(row["CollectionBy"]!=null)
				{
					model.CollectionBy=row["CollectionBy"].ToString();
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select OID,CheckPointCode,CheckPointName,CollectionDatetime,Pressure,Temperture,OutElectricity,CurrentVal,Voltage,PowerVal,CollectionType,CollectionBy ");
			strSql.Append(" FROM OCInstrumentElectricalData ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" OID,CheckPointCode,CheckPointName,CollectionDatetime,Pressure,Temperture,OutElectricity,CurrentVal,Voltage,PowerVal,CollectionType,CollectionBy ");
			strSql.Append(" FROM OCInstrumentElectricalData ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM OCInstrumentElectricalData ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby + " desc");
			}
			else
			{
				strSql.Append("order by T.OID desc");
			}
			strSql.Append(")AS Row, T.*  from OCInstrumentElectricalData T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", (startIndex - 1) * endIndex + 1, startIndex * endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "OCInstrumentElectricalData";
			parameters[1].Value = "OID";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}


﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Maticsoft.DBUtility;//Please add references
namespace EGMNGS.DAL
{
    public partial class InfoDevChangeFill
    {
        public InfoDevChangeFill()
        { }
        #region  Method

        /// <summary>
        /// 得到最大ID
        /// </summary>
        public int GetMaxId()
        {
            return DbHelperSQL.GetMaxID("OID", "InfoDevChangeFill");
        }

        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int OID)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) from InfoDevChangeFill");
            strSql.Append(" where OID=@OID");
            SqlParameter[] parameters = {
					new SqlParameter("@OID", SqlDbType.Int,4)
			};
            parameters[0].Value = OID;

            return DbHelperSQL.Exists(strSql.ToString(), parameters);
        }


        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(EGMNGS.Model.InfoDevChangeFill model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into InfoDevChangeFill(");
            strSql.Append("DevCode,DevName,DevType,RatedQL,RatedQY,FillGasdate,Operataff,GasUsing,AmountFillGas,AmountPressFillGassbefore,AmountPressFillGassafter,RemarksDesc,CylinderCode,GasCode,Status,RegistantsOID,RegistantsDate,PowerSupplyCode,PowerSupplyName,ConvertStationCode,ConvertStationName)");
            strSql.Append(" values (");
            strSql.Append("@DevCode,@DevName,@DevType,@RatedQL,@RatedQY,@FillGasdate,@Operataff,@GasUsing,@AmountFillGas,@AmountPressFillGassbefore,@AmountPressFillGassafter,@RemarksDesc,@CylinderCode,@GasCode,@Status,@RegistantsOID,@RegistantsDate,@PowerSupplyCode,@PowerSupplyName,@ConvertStationCode,@ConvertStationName)");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
					new SqlParameter("@DevCode", SqlDbType.VarChar,20),
					new SqlParameter("@DevName", SqlDbType.VarChar,50),
					new SqlParameter("@DevType", SqlDbType.VarChar,50),
					new SqlParameter("@RatedQL", SqlDbType.Decimal,9),
					new SqlParameter("@RatedQY", SqlDbType.Decimal,9),
					new SqlParameter("@FillGasdate", SqlDbType.Date,3),
					new SqlParameter("@Operataff", SqlDbType.VarChar,20),
					new SqlParameter("@GasUsing", SqlDbType.NVarChar,200),
					new SqlParameter("@AmountFillGas", SqlDbType.Decimal,9),
					new SqlParameter("@AmountPressFillGassbefore", SqlDbType.Decimal,9),
					new SqlParameter("@AmountPressFillGassafter", SqlDbType.Decimal,9),
					new SqlParameter("@RemarksDesc", SqlDbType.NVarChar,500),
					new SqlParameter("@CylinderCode", SqlDbType.VarChar,20),
					new SqlParameter("@GasCode", SqlDbType.VarChar,20),
					new SqlParameter("@Status", SqlDbType.VarChar,4),
					new SqlParameter("@RegistantsOID", SqlDbType.VarChar,50),
					new SqlParameter("@RegistantsDate", SqlDbType.Date,3),
					new SqlParameter("@PowerSupplyCode", SqlDbType.VarChar,50),
					new SqlParameter("@PowerSupplyName", SqlDbType.VarChar,50),
					new SqlParameter("@ConvertStationCode", SqlDbType.VarChar,50),
					new SqlParameter("@ConvertStationName", SqlDbType.VarChar,50)};
            parameters[0].Value = model.DevCode;
            parameters[1].Value = model.DevName;
            parameters[2].Value = model.DevType;
            parameters[3].Value = model.RatedQL;
            parameters[4].Value = model.RatedQY;
            parameters[5].Value = model.FillGasdate;
            parameters[6].Value = model.Operataff;
            parameters[7].Value = model.GasUsing;
            parameters[8].Value = model.AmountFillGas;
            parameters[9].Value = model.AmountPressFillGassbefore;
            parameters[10].Value = model.AmountPressFillGassafter;
            parameters[11].Value = model.RemarksDesc;
            parameters[12].Value = model.CylinderCode;
            parameters[13].Value = model.GasCode;
            parameters[14].Value = model.Status;
            parameters[15].Value = model.RegistantsOID;
            parameters[16].Value = model.RegistantsDate;
            parameters[17].Value = model.PowerSupplyCode;
            parameters[18].Value = model.PowerSupplyName;
            parameters[19].Value = model.ConvertStationCode;
            parameters[20].Value = model.ConvertStationName;

            object obj = DbHelperSQL.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(EGMNGS.Model.InfoDevChangeFill model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update InfoDevChangeFill set ");
            strSql.Append("DevCode=@DevCode,");
            strSql.Append("DevName=@DevName,");
            strSql.Append("DevType=@DevType,");
            strSql.Append("RatedQL=@RatedQL,");
            strSql.Append("RatedQY=@RatedQY,");
            strSql.Append("FillGasdate=@FillGasdate,");
            strSql.Append("Operataff=@Operataff,");
            strSql.Append("GasUsing=@GasUsing,");
            strSql.Append("AmountFillGas=@AmountFillGas,");
            strSql.Append("AmountPressFillGassbefore=@AmountPressFillGassbefore,");
            strSql.Append("AmountPressFillGassafter=@AmountPressFillGassafter,");
            strSql.Append("RemarksDesc=@RemarksDesc,");
            strSql.Append("CylinderCode=@CylinderCode,");
            strSql.Append("GasCode=@GasCode,");
            strSql.Append("Status=@Status,");
            strSql.Append("RegistantsOID=@RegistantsOID,");
            strSql.Append("RegistantsDate=@RegistantsDate,");
            strSql.Append("PowerSupplyCode=@PowerSupplyCode,");
            strSql.Append("PowerSupplyName=@PowerSupplyName,");
            strSql.Append("ConvertStationCode=@ConvertStationCode,");
            strSql.Append("ConvertStationName=@ConvertStationName");
            strSql.Append(" where OID=@OID");
            SqlParameter[] parameters = {
					new SqlParameter("@DevCode", SqlDbType.VarChar,20),
					new SqlParameter("@DevName", SqlDbType.VarChar,50),
					new SqlParameter("@DevType", SqlDbType.VarChar,50),
					new SqlParameter("@RatedQL", SqlDbType.Decimal,9),
					new SqlParameter("@RatedQY", SqlDbType.Decimal,9),
					new SqlParameter("@FillGasdate", SqlDbType.Date,3),
					new SqlParameter("@Operataff", SqlDbType.VarChar,20),
					new SqlParameter("@GasUsing", SqlDbType.NVarChar,200),
					new SqlParameter("@AmountFillGas", SqlDbType.Decimal,9),
					new SqlParameter("@AmountPressFillGassbefore", SqlDbType.Decimal,9),
					new SqlParameter("@AmountPressFillGassafter", SqlDbType.Decimal,9),
					new SqlParameter("@RemarksDesc", SqlDbType.NVarChar,500),
					new SqlParameter("@CylinderCode", SqlDbType.VarChar,20),
					new SqlParameter("@GasCode", SqlDbType.VarChar,20),
					new SqlParameter("@Status", SqlDbType.VarChar,4),
					new SqlParameter("@RegistantsOID", SqlDbType.VarChar,50),
					new SqlParameter("@RegistantsDate", SqlDbType.Date,3),
					new SqlParameter("@PowerSupplyCode", SqlDbType.VarChar,50),
					new SqlParameter("@PowerSupplyName", SqlDbType.VarChar,50),
					new SqlParameter("@ConvertStationCode", SqlDbType.VarChar,50),
					new SqlParameter("@ConvertStationName", SqlDbType.VarChar,50),
					new SqlParameter("@OID", SqlDbType.Int,4)};
            parameters[0].Value = model.DevCode;
            parameters[1].Value = model.DevName;
            parameters[2].Value = model.DevType;
            parameters[3].Value = model.RatedQL;
            parameters[4].Value = model.RatedQY;
            parameters[5].Value = model.FillGasdate;
            parameters[6].Value = model.Operataff;
            parameters[7].Value = model.GasUsing;
            parameters[8].Value = model.AmountFillGas;
            parameters[9].Value = model.AmountPressFillGassbefore;
            parameters[10].Value = model.AmountPressFillGassafter;
            parameters[11].Value = model.RemarksDesc;
            parameters[12].Value = model.CylinderCode;
            parameters[13].Value = model.GasCode;
            parameters[14].Value = model.Status;
            parameters[15].Value = model.RegistantsOID;
            parameters[16].Value = model.RegistantsDate;
            parameters[17].Value = model.PowerSupplyCode;
            parameters[18].Value = model.PowerSupplyName;
            parameters[19].Value = model.ConvertStationCode;
            parameters[20].Value = model.ConvertStationName;
            parameters[21].Value = model.OID;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int OID)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from InfoDevChangeFill ");
            strSql.Append(" where OID=@OID");
            SqlParameter[] parameters = {
					new SqlParameter("@OID", SqlDbType.Int,4)
			};
            parameters[0].Value = OID;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string OIDlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from InfoDevChangeFill ");
            strSql.Append(" where OID in (" + OIDlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public EGMNGS.Model.InfoDevChangeFill GetModel(int OID)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 OID,DevCode,DevName,DevType,RatedQL,RatedQY,FillGasdate,Operataff,GasUsing,AmountFillGas,AmountPressFillGassbefore,AmountPressFillGassafter,RemarksDesc,CylinderCode,GasCode,Status,RegistantsOID,RegistantsDate,PowerSupplyCode,PowerSupplyName,ConvertStationCode,ConvertStationName from InfoDevChangeFill ");
            strSql.Append(" where OID=@OID");
            SqlParameter[] parameters = {
					new SqlParameter("@OID", SqlDbType.Int,4)
			};
            parameters[0].Value = OID;

            EGMNGS.Model.InfoDevChangeFill model = new EGMNGS.Model.InfoDevChangeFill();
            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0]["OID"] != null && ds.Tables[0].Rows[0]["OID"].ToString() != "")
                {
                    model.OID = int.Parse(ds.Tables[0].Rows[0]["OID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["DevCode"] != null && ds.Tables[0].Rows[0]["DevCode"].ToString() != "")
                {
                    model.DevCode = ds.Tables[0].Rows[0]["DevCode"].ToString();
                }
                if (ds.Tables[0].Rows[0]["DevName"] != null && ds.Tables[0].Rows[0]["DevName"].ToString() != "")
                {
                    model.DevName = ds.Tables[0].Rows[0]["DevName"].ToString();
                }
                if (ds.Tables[0].Rows[0]["DevType"] != null && ds.Tables[0].Rows[0]["DevType"].ToString() != "")
                {
                    model.DevType = ds.Tables[0].Rows[0]["DevType"].ToString();
                }
                if (ds.Tables[0].Rows[0]["RatedQL"] != null && ds.Tables[0].Rows[0]["RatedQL"].ToString() != "")
                {
                    model.RatedQL = decimal.Parse(ds.Tables[0].Rows[0]["RatedQL"].ToString());
                }
                if (ds.Tables[0].Rows[0]["RatedQY"] != null && ds.Tables[0].Rows[0]["RatedQY"].ToString() != "")
                {
                    model.RatedQY = decimal.Parse(ds.Tables[0].Rows[0]["RatedQY"].ToString());
                }
                if (ds.Tables[0].Rows[0]["FillGasdate"] != null && ds.Tables[0].Rows[0]["FillGasdate"].ToString() != "")
                {
                    model.FillGasdate = DateTime.Parse(ds.Tables[0].Rows[0]["FillGasdate"].ToString());
                }
                if (ds.Tables[0].Rows[0]["Operataff"] != null && ds.Tables[0].Rows[0]["Operataff"].ToString() != "")
                {
                    model.Operataff = ds.Tables[0].Rows[0]["Operataff"].ToString();
                }
                if (ds.Tables[0].Rows[0]["GasUsing"] != null && ds.Tables[0].Rows[0]["GasUsing"].ToString() != "")
                {
                    model.GasUsing = ds.Tables[0].Rows[0]["GasUsing"].ToString();
                }
                if (ds.Tables[0].Rows[0]["AmountFillGas"] != null && ds.Tables[0].Rows[0]["AmountFillGas"].ToString() != "")
                {
                    model.AmountFillGas = decimal.Parse(ds.Tables[0].Rows[0]["AmountFillGas"].ToString());
                }
                if (ds.Tables[0].Rows[0]["AmountPressFillGassbefore"] != null && ds.Tables[0].Rows[0]["AmountPressFillGassbefore"].ToString() != "")
                {
                    model.AmountPressFillGassbefore = decimal.Parse(ds.Tables[0].Rows[0]["AmountPressFillGassbefore"].ToString());
                }
                if (ds.Tables[0].Rows[0]["AmountPressFillGassafter"] != null && ds.Tables[0].Rows[0]["AmountPressFillGassafter"].ToString() != "")
                {
                    model.AmountPressFillGassafter = decimal.Parse(ds.Tables[0].Rows[0]["AmountPressFillGassafter"].ToString());
                }
                if (ds.Tables[0].Rows[0]["RemarksDesc"] != null && ds.Tables[0].Rows[0]["RemarksDesc"].ToString() != "")
                {
                    model.RemarksDesc = ds.Tables[0].Rows[0]["RemarksDesc"].ToString();
                }
                if (ds.Tables[0].Rows[0]["CylinderCode"] != null && ds.Tables[0].Rows[0]["CylinderCode"].ToString() != "")
                {
                    model.CylinderCode = ds.Tables[0].Rows[0]["CylinderCode"].ToString();
                }
                if (ds.Tables[0].Rows[0]["GasCode"] != null && ds.Tables[0].Rows[0]["GasCode"].ToString() != "")
                {
                    model.GasCode = ds.Tables[0].Rows[0]["GasCode"].ToString();
                }
                if (ds.Tables[0].Rows[0]["Status"] != null && ds.Tables[0].Rows[0]["Status"].ToString() != "")
                {
                    model.Status = ds.Tables[0].Rows[0]["Status"].ToString();
                }
                if (ds.Tables[0].Rows[0]["RegistantsOID"] != null && ds.Tables[0].Rows[0]["RegistantsOID"].ToString() != "")
                {
                    model.RegistantsOID = ds.Tables[0].Rows[0]["RegistantsOID"].ToString();
                }
                if (ds.Tables[0].Rows[0]["RegistantsDate"] != null && ds.Tables[0].Rows[0]["RegistantsDate"].ToString() != "")
                {
                    model.RegistantsDate = DateTime.Parse(ds.Tables[0].Rows[0]["RegistantsDate"].ToString());
                }
                if (ds.Tables[0].Rows[0]["PowerSupplyCode"] != null && ds.Tables[0].Rows[0]["PowerSupplyCode"].ToString() != "")
                {
                    model.PowerSupplyCode = ds.Tables[0].Rows[0]["PowerSupplyCode"].ToString();
                }
                if (ds.Tables[0].Rows[0]["PowerSupplyName"] != null && ds.Tables[0].Rows[0]["PowerSupplyName"].ToString() != "")
                {
                    model.PowerSupplyName = ds.Tables[0].Rows[0]["PowerSupplyName"].ToString();
                }
                if (ds.Tables[0].Rows[0]["ConvertStationCode"] != null && ds.Tables[0].Rows[0]["ConvertStationCode"].ToString() != "")
                {
                    model.ConvertStationCode = ds.Tables[0].Rows[0]["ConvertStationCode"].ToString();
                }
                if (ds.Tables[0].Rows[0]["ConvertStationName"] != null && ds.Tables[0].Rows[0]["ConvertStationName"].ToString() != "")
                {
                    model.ConvertStationName = ds.Tables[0].Rows[0]["ConvertStationName"].ToString();
                }
                return model;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select OID,DevCode,DevName,DevType,RatedQL,RatedQY,FillGasdate,Operataff,GasUsing,AmountFillGas,AmountPressFillGassbefore,AmountPressFillGassafter,RemarksDesc,CylinderCode,GasCode,Status,RegistantsOID,RegistantsDate,PowerSupplyCode,PowerSupplyName,ConvertStationCode,ConvertStationName ");
            strSql.Append(" FROM InfoDevChangeFill ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" OID,DevCode,DevName,DevType,RatedQL,RatedQY,FillGasdate,Operataff,GasUsing,AmountFillGas,AmountPressFillGassbefore,AmountPressFillGassafter,RemarksDesc,CylinderCode,GasCode,Status,RegistantsOID,RegistantsDate,PowerSupplyCode,PowerSupplyName,ConvertStationCode,ConvertStationName ");
            strSql.Append(" FROM InfoDevChangeFill ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM InfoDevChangeFill ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.OID desc");
            }
            strSql.Append(")AS Row, T.*  from InfoDevChangeFill T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", (startIndex - 1) * endIndex + 1, startIndex * endIndex);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /*
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        {
            SqlParameter[] parameters = {
                    new SqlParameter("@tblName", SqlDbType.VarChar, 255),
                    new SqlParameter("@fldName", SqlDbType.VarChar, 255),
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@PageIndex", SqlDbType.Int),
                    new SqlParameter("@IsReCount", SqlDbType.Bit),
                    new SqlParameter("@OrderType", SqlDbType.Bit),
                    new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
                    };
            parameters[0].Value = "InfoDevChangeFill";
            parameters[1].Value = "OID";
            parameters[2].Value = PageSize;
            parameters[3].Value = PageIndex;
            parameters[4].Value = 0;
            parameters[5].Value = 0;
            parameters[6].Value = strWhere;	
            return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
        }*/

        #endregion  Method
    }
}


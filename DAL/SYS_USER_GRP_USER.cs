﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Maticsoft.DBUtility;//Please add references
namespace EGMNGS.DAL
{
	/// <summary>
	/// 数据访问类:SYS_USER_GRP_USER
	/// </summary>
	public partial class SYS_USER_GRP_USER
	{
		public SYS_USER_GRP_USER()
		{}
		#region  Method

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(string USER_GRP_ID,string USER_ID)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from SYS_USER_GRP_USER");
			strSql.Append(" where USER_GRP_ID=@USER_GRP_ID and USER_ID=@USER_ID ");
			SqlParameter[] parameters = {
					new SqlParameter("@USER_GRP_ID", SqlDbType.VarChar,4),
					new SqlParameter("@USER_ID", SqlDbType.VarChar,12)			};
			parameters[0].Value = USER_GRP_ID;
			parameters[1].Value = USER_ID;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(EGMNGS.Model.SYS_USER_GRP_USER model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into SYS_USER_GRP_USER(");
			strSql.Append("USER_GRP_ID,USER_ID,CREATED_BY,CREATED_DATE,LAST_UPD_BY,LAST_UPD_DATE)");
			strSql.Append(" values (");
			strSql.Append("@USER_GRP_ID,@USER_ID,@CREATED_BY,@CREATED_DATE,@LAST_UPD_BY,@LAST_UPD_DATE)");
			SqlParameter[] parameters = {
					new SqlParameter("@USER_GRP_ID", SqlDbType.VarChar,4),
					new SqlParameter("@USER_ID", SqlDbType.VarChar,12),
					new SqlParameter("@CREATED_BY", SqlDbType.VarChar,12),
					new SqlParameter("@CREATED_DATE", SqlDbType.DateTime),
					new SqlParameter("@LAST_UPD_BY", SqlDbType.VarChar,12),
					new SqlParameter("@LAST_UPD_DATE", SqlDbType.DateTime)};
			parameters[0].Value = model.USER_GRP_ID;
			parameters[1].Value = model.USER_ID;
			parameters[2].Value = model.CREATED_BY;
			parameters[3].Value = model.CREATED_DATE;
			parameters[4].Value = model.LAST_UPD_BY;
			parameters[5].Value = model.LAST_UPD_DATE;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(EGMNGS.Model.SYS_USER_GRP_USER model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update SYS_USER_GRP_USER set ");
			strSql.Append("CREATED_BY=@CREATED_BY,");
			strSql.Append("CREATED_DATE=@CREATED_DATE,");
			strSql.Append("LAST_UPD_BY=@LAST_UPD_BY,");
			strSql.Append("LAST_UPD_DATE=@LAST_UPD_DATE");
			strSql.Append(" where USER_GRP_ID=@USER_GRP_ID and USER_ID=@USER_ID ");
			SqlParameter[] parameters = {
					new SqlParameter("@CREATED_BY", SqlDbType.VarChar,12),
					new SqlParameter("@CREATED_DATE", SqlDbType.DateTime),
					new SqlParameter("@LAST_UPD_BY", SqlDbType.VarChar,12),
					new SqlParameter("@LAST_UPD_DATE", SqlDbType.DateTime),
					new SqlParameter("@USER_GRP_ID", SqlDbType.VarChar,4),
					new SqlParameter("@USER_ID", SqlDbType.VarChar,12)};
			parameters[0].Value = model.CREATED_BY;
			parameters[1].Value = model.CREATED_DATE;
			parameters[2].Value = model.LAST_UPD_BY;
			parameters[3].Value = model.LAST_UPD_DATE;
			parameters[4].Value = model.USER_GRP_ID;
			parameters[5].Value = model.USER_ID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(string USER_GRP_ID,string USER_ID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from SYS_USER_GRP_USER ");
			strSql.Append(" where USER_GRP_ID=@USER_GRP_ID and USER_ID=@USER_ID ");
			SqlParameter[] parameters = {
					new SqlParameter("@USER_GRP_ID", SqlDbType.VarChar,4),
					new SqlParameter("@USER_ID", SqlDbType.VarChar,12)			};
			parameters[0].Value = USER_GRP_ID;
			parameters[1].Value = USER_ID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public EGMNGS.Model.SYS_USER_GRP_USER GetModel(string USER_GRP_ID,string USER_ID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 USER_GRP_ID,USER_ID,CREATED_BY,CREATED_DATE,LAST_UPD_BY,LAST_UPD_DATE from SYS_USER_GRP_USER ");
			strSql.Append(" where USER_GRP_ID=@USER_GRP_ID and USER_ID=@USER_ID ");
			SqlParameter[] parameters = {
					new SqlParameter("@USER_GRP_ID", SqlDbType.VarChar,4),
					new SqlParameter("@USER_ID", SqlDbType.VarChar,12)			};
			parameters[0].Value = USER_GRP_ID;
			parameters[1].Value = USER_ID;

			EGMNGS.Model.SYS_USER_GRP_USER model=new EGMNGS.Model.SYS_USER_GRP_USER();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				if(ds.Tables[0].Rows[0]["USER_GRP_ID"]!=null && ds.Tables[0].Rows[0]["USER_GRP_ID"].ToString()!="")
				{
					model.USER_GRP_ID=ds.Tables[0].Rows[0]["USER_GRP_ID"].ToString();
				}
				if(ds.Tables[0].Rows[0]["USER_ID"]!=null && ds.Tables[0].Rows[0]["USER_ID"].ToString()!="")
				{
					model.USER_ID=ds.Tables[0].Rows[0]["USER_ID"].ToString();
				}
				if(ds.Tables[0].Rows[0]["CREATED_BY"]!=null && ds.Tables[0].Rows[0]["CREATED_BY"].ToString()!="")
				{
					model.CREATED_BY=ds.Tables[0].Rows[0]["CREATED_BY"].ToString();
				}
				if(ds.Tables[0].Rows[0]["CREATED_DATE"]!=null && ds.Tables[0].Rows[0]["CREATED_DATE"].ToString()!="")
				{
					model.CREATED_DATE=DateTime.Parse(ds.Tables[0].Rows[0]["CREATED_DATE"].ToString());
				}
				if(ds.Tables[0].Rows[0]["LAST_UPD_BY"]!=null && ds.Tables[0].Rows[0]["LAST_UPD_BY"].ToString()!="")
				{
					model.LAST_UPD_BY=ds.Tables[0].Rows[0]["LAST_UPD_BY"].ToString();
				}
				if(ds.Tables[0].Rows[0]["LAST_UPD_DATE"]!=null && ds.Tables[0].Rows[0]["LAST_UPD_DATE"].ToString()!="")
				{
					model.LAST_UPD_DATE=DateTime.Parse(ds.Tables[0].Rows[0]["LAST_UPD_DATE"].ToString());
				}
				return model;
			}
			else
			{
				return null;
			}
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select USER_GRP_ID,USER_ID,CREATED_BY,CREATED_DATE,LAST_UPD_BY,LAST_UPD_DATE ");
			strSql.Append(" FROM SYS_USER_GRP_USER ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" USER_GRP_ID,USER_ID,CREATED_BY,CREATED_DATE,LAST_UPD_BY,LAST_UPD_DATE ");
			strSql.Append(" FROM SYS_USER_GRP_USER ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM SYS_USER_GRP_USER ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.USER_ID desc");
			}
			strSql.Append(")AS Row, T.*  from SYS_USER_GRP_USER T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", (startIndex - 1) * endIndex + 1, startIndex * endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "SYS_USER_GRP_USER";
			parameters[1].Value = "USER_ID";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  Method
	}
}


﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Maticsoft.DBUtility;//Please add references
namespace EGMNGS.DAL
{
    /// <summary>
    /// 数据访问类:SYS_CODE
    /// </summary>
    public partial class SYS_CODE
    {
        public SYS_CODE()
        { }
        #region  Method

        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(string CODE_TYPE, string CODE, string SUBCODE)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) from SYS_CODE");
            strSql.Append(" where CODE_TYPE=@CODE_TYPE and CODE=@CODE and SUBCODE=@SUBCODE ");
            SqlParameter[] parameters = {
					new SqlParameter("@CODE_TYPE", SqlDbType.VarChar,20),
					new SqlParameter("@CODE", SqlDbType.VarChar,4),
					new SqlParameter("@SUBCODE", SqlDbType.VarChar,4)			};
            parameters[0].Value = CODE_TYPE;
            parameters[1].Value = CODE;
            parameters[2].Value = SUBCODE;

            return DbHelperSQL.Exists(strSql.ToString(), parameters);
        }


        /// <summary>
        /// 增加一条数据
        /// </summary>
        public bool Add(EGMNGS.Model.SYS_CODE model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into SYS_CODE(");
            strSql.Append("CODE_TYPE,CODE,SUBCODE,CODE_ENG_DESC,CODE_CHI_DESC,SHORT_DESC,DISPLAY_ORDER,MISC_IND,DEPART_CODE,CREATED_BY,CREATED_DATE,LAST_UPD_BY,LAST_UPD_DATE)");
            strSql.Append(" values (");
            strSql.Append("@CODE_TYPE,@CODE,@SUBCODE,@CODE_ENG_DESC,@CODE_CHI_DESC,@SHORT_DESC,@DISPLAY_ORDER,@MISC_IND,@DEPART_CODE,@CREATED_BY,@CREATED_DATE,@LAST_UPD_BY,@LAST_UPD_DATE)");
            SqlParameter[] parameters = {
					new SqlParameter("@CODE_TYPE", SqlDbType.VarChar,20),
					new SqlParameter("@CODE", SqlDbType.VarChar,4),
					new SqlParameter("@SUBCODE", SqlDbType.VarChar,4),
					new SqlParameter("@CODE_ENG_DESC", SqlDbType.VarChar,150),
					new SqlParameter("@CODE_CHI_DESC", SqlDbType.NVarChar,60),
					new SqlParameter("@SHORT_DESC", SqlDbType.VarChar,10),
					new SqlParameter("@DISPLAY_ORDER", SqlDbType.Decimal,5),
					new SqlParameter("@MISC_IND", SqlDbType.VarChar,1),
					new SqlParameter("@DEPART_CODE", SqlDbType.VarChar,4),
					new SqlParameter("@CREATED_BY", SqlDbType.VarChar,12),
					new SqlParameter("@CREATED_DATE", SqlDbType.DateTime),
					new SqlParameter("@LAST_UPD_BY", SqlDbType.VarChar,12),
					new SqlParameter("@LAST_UPD_DATE", SqlDbType.DateTime)};
            parameters[0].Value = model.CODE_TYPE;
            parameters[1].Value = model.CODE;
            parameters[2].Value = model.SUBCODE;
            parameters[3].Value = model.CODE_ENG_DESC;
            parameters[4].Value = model.CODE_CHI_DESC;
            parameters[5].Value = model.SHORT_DESC;
            parameters[6].Value = model.DISPLAY_ORDER;
            parameters[7].Value = model.MISC_IND;
            parameters[8].Value = model.DEPART_CODE;
            parameters[9].Value = model.CREATED_BY;
            parameters[10].Value = model.CREATED_DATE;
            parameters[11].Value = model.LAST_UPD_BY;
            parameters[12].Value = model.LAST_UPD_DATE;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(EGMNGS.Model.SYS_CODE model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update SYS_CODE set ");
            strSql.Append("CODE_ENG_DESC=@CODE_ENG_DESC,");
            strSql.Append("CODE_CHI_DESC=@CODE_CHI_DESC,");
            strSql.Append("SHORT_DESC=@SHORT_DESC,");
            strSql.Append("DISPLAY_ORDER=@DISPLAY_ORDER,");
            strSql.Append("MISC_IND=@MISC_IND,");
            strSql.Append("DEPART_CODE=@DEPART_CODE,");
            strSql.Append("CREATED_BY=@CREATED_BY,");
            strSql.Append("CREATED_DATE=@CREATED_DATE,");
            strSql.Append("LAST_UPD_BY=@LAST_UPD_BY,");
            strSql.Append("LAST_UPD_DATE=@LAST_UPD_DATE");
            strSql.Append(" where CODE_TYPE=@CODE_TYPE and CODE=@CODE and SUBCODE=@SUBCODE ");
            SqlParameter[] parameters = {
					new SqlParameter("@CODE_ENG_DESC", SqlDbType.VarChar,150),
					new SqlParameter("@CODE_CHI_DESC", SqlDbType.NVarChar,60),
					new SqlParameter("@SHORT_DESC", SqlDbType.VarChar,10),
					new SqlParameter("@DISPLAY_ORDER", SqlDbType.Decimal,5),
					new SqlParameter("@MISC_IND", SqlDbType.VarChar,1),
					new SqlParameter("@DEPART_CODE", SqlDbType.VarChar,4),
					new SqlParameter("@CREATED_BY", SqlDbType.VarChar,12),
					new SqlParameter("@CREATED_DATE", SqlDbType.DateTime),
					new SqlParameter("@LAST_UPD_BY", SqlDbType.VarChar,12),
					new SqlParameter("@LAST_UPD_DATE", SqlDbType.DateTime),
					new SqlParameter("@CODE_TYPE", SqlDbType.VarChar,20),
					new SqlParameter("@CODE", SqlDbType.VarChar,4),
					new SqlParameter("@SUBCODE", SqlDbType.VarChar,4)};
            parameters[0].Value = model.CODE_ENG_DESC;
            parameters[1].Value = model.CODE_CHI_DESC;
            parameters[2].Value = model.SHORT_DESC;
            parameters[3].Value = model.DISPLAY_ORDER;
            parameters[4].Value = model.MISC_IND;
            parameters[5].Value = model.DEPART_CODE;
            parameters[6].Value = model.CREATED_BY;
            parameters[7].Value = model.CREATED_DATE;
            parameters[8].Value = model.LAST_UPD_BY;
            parameters[9].Value = model.LAST_UPD_DATE;
            parameters[10].Value = model.CODE_TYPE;
            parameters[11].Value = model.CODE;
            parameters[12].Value = model.SUBCODE;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(string CODE_TYPE, string CODE, string SUBCODE)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from SYS_CODE ");
            strSql.Append(" where CODE_TYPE=@CODE_TYPE and CODE=@CODE and SUBCODE=@SUBCODE ");
            SqlParameter[] parameters = {
					new SqlParameter("@CODE_TYPE", SqlDbType.VarChar,20),
					new SqlParameter("@CODE", SqlDbType.VarChar,4),
					new SqlParameter("@SUBCODE", SqlDbType.VarChar,4)			};
            parameters[0].Value = CODE_TYPE;
            parameters[1].Value = CODE;
            parameters[2].Value = SUBCODE;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public EGMNGS.Model.SYS_CODE GetModel(string CODE_TYPE, string CODE, string SUBCODE)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 CODE_TYPE,CODE,SUBCODE,CODE_ENG_DESC,CODE_CHI_DESC,SHORT_DESC,DISPLAY_ORDER,MISC_IND,DEPART_CODE,CREATED_BY,CREATED_DATE,LAST_UPD_BY,LAST_UPD_DATE from SYS_CODE ");
            strSql.Append(" where CODE_TYPE=@CODE_TYPE and CODE=@CODE and SUBCODE=@SUBCODE ");
            SqlParameter[] parameters = {
					new SqlParameter("@CODE_TYPE", SqlDbType.VarChar,20),
					new SqlParameter("@CODE", SqlDbType.VarChar,4),
					new SqlParameter("@SUBCODE", SqlDbType.VarChar,4)			};
            parameters[0].Value = CODE_TYPE;
            parameters[1].Value = CODE;
            parameters[2].Value = SUBCODE;

            EGMNGS.Model.SYS_CODE model = new EGMNGS.Model.SYS_CODE();
            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0]["CODE_TYPE"] != null && ds.Tables[0].Rows[0]["CODE_TYPE"].ToString() != "")
                {
                    model.CODE_TYPE = ds.Tables[0].Rows[0]["CODE_TYPE"].ToString();
                }
                if (ds.Tables[0].Rows[0]["CODE"] != null && ds.Tables[0].Rows[0]["CODE"].ToString() != "")
                {
                    model.CODE = ds.Tables[0].Rows[0]["CODE"].ToString();
                }
                if (ds.Tables[0].Rows[0]["SUBCODE"] != null && ds.Tables[0].Rows[0]["SUBCODE"].ToString() != "")
                {
                    model.SUBCODE = ds.Tables[0].Rows[0]["SUBCODE"].ToString();
                }
                if (ds.Tables[0].Rows[0]["CODE_ENG_DESC"] != null && ds.Tables[0].Rows[0]["CODE_ENG_DESC"].ToString() != "")
                {
                    model.CODE_ENG_DESC = ds.Tables[0].Rows[0]["CODE_ENG_DESC"].ToString();
                }
                if (ds.Tables[0].Rows[0]["CODE_CHI_DESC"] != null && ds.Tables[0].Rows[0]["CODE_CHI_DESC"].ToString() != "")
                {
                    model.CODE_CHI_DESC = ds.Tables[0].Rows[0]["CODE_CHI_DESC"].ToString();
                }
                if (ds.Tables[0].Rows[0]["SHORT_DESC"] != null && ds.Tables[0].Rows[0]["SHORT_DESC"].ToString() != "")
                {
                    model.SHORT_DESC = ds.Tables[0].Rows[0]["SHORT_DESC"].ToString();
                }
                if (ds.Tables[0].Rows[0]["DISPLAY_ORDER"] != null && ds.Tables[0].Rows[0]["DISPLAY_ORDER"].ToString() != "")
                {
                    model.DISPLAY_ORDER = decimal.Parse(ds.Tables[0].Rows[0]["DISPLAY_ORDER"].ToString());
                }
                if (ds.Tables[0].Rows[0]["MISC_IND"] != null && ds.Tables[0].Rows[0]["MISC_IND"].ToString() != "")
                {
                    model.MISC_IND = ds.Tables[0].Rows[0]["MISC_IND"].ToString();
                }
                if (ds.Tables[0].Rows[0]["DEPART_CODE"] != null && ds.Tables[0].Rows[0]["DEPART_CODE"].ToString() != "")
                {
                    model.DEPART_CODE = ds.Tables[0].Rows[0]["DEPART_CODE"].ToString();
                }
                if (ds.Tables[0].Rows[0]["CREATED_BY"] != null && ds.Tables[0].Rows[0]["CREATED_BY"].ToString() != "")
                {
                    model.CREATED_BY = ds.Tables[0].Rows[0]["CREATED_BY"].ToString();
                }
                if (ds.Tables[0].Rows[0]["CREATED_DATE"] != null && ds.Tables[0].Rows[0]["CREATED_DATE"].ToString() != "")
                {
                    model.CREATED_DATE = DateTime.Parse(ds.Tables[0].Rows[0]["CREATED_DATE"].ToString());
                }
                if (ds.Tables[0].Rows[0]["LAST_UPD_BY"] != null && ds.Tables[0].Rows[0]["LAST_UPD_BY"].ToString() != "")
                {
                    model.LAST_UPD_BY = ds.Tables[0].Rows[0]["LAST_UPD_BY"].ToString();
                }
                if (ds.Tables[0].Rows[0]["LAST_UPD_DATE"] != null && ds.Tables[0].Rows[0]["LAST_UPD_DATE"].ToString() != "")
                {
                    model.LAST_UPD_DATE = DateTime.Parse(ds.Tables[0].Rows[0]["LAST_UPD_DATE"].ToString());
                }
                return model;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select CODE_TYPE,CODE,SUBCODE,CODE_ENG_DESC,CODE_CHI_DESC,SHORT_DESC,DISPLAY_ORDER,MISC_IND,DEPART_CODE,CREATED_BY,CREATED_DATE,LAST_UPD_BY,LAST_UPD_DATE ");
            strSql.Append(" FROM SYS_CODE ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" CODE_TYPE,CODE,SUBCODE,CODE_ENG_DESC,CODE_CHI_DESC,SHORT_DESC,DISPLAY_ORDER,MISC_IND,DEPART_CODE,CREATED_BY,CREATED_DATE,LAST_UPD_BY,LAST_UPD_DATE ");
            strSql.Append(" FROM SYS_CODE ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM SYS_CODE ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.SUBCODE desc");
            }
            strSql.Append(")AS Row, T.*  from SYS_CODE T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", (startIndex - 1) * endIndex + 1, startIndex * endIndex);
            return DbHelperSQL.Query(strSql.ToString());
        }


        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize, int PageIndex, string strWhere)
        {
            SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
            parameters[0].Value = "SYS_CODE";
            parameters[1].Value = "SUBCODE";
            parameters[2].Value = PageSize;
            parameters[3].Value = PageIndex;
            parameters[4].Value = 0;
            parameters[5].Value = 0;
            parameters[6].Value = strWhere;
            return DbHelperSQL.RunProcedure("UP_GetRecordByPage", parameters, "ds");
        }

        #endregion  Method
    }
}


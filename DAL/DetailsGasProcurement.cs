﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Maticsoft.DBUtility;//Please add references
namespace EGMNGS.DAL
{
    /// <summary>
    /// 数据访问类:DetailsGasProcurement
    /// </summary>
    public partial class DetailsGasProcurement
    {
        public DetailsGasProcurement()
        { }
        #region  Method

        /// <summary>
        /// 得到最大ID
        /// </summary>
        public int GetMaxId()
        {
            return DbHelperSQL.GetMaxID("OID", "DetailsGasProcurement");
        }

        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int OID)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) from DetailsGasProcurement");
            strSql.Append(" where OID=@OID");
            SqlParameter[] parameters = {
					new SqlParameter("@OID", SqlDbType.Int,4)
			};
            parameters[0].Value = OID;

            return DbHelperSQL.Exists(strSql.ToString(), parameters);
        }


        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(EGMNGS.Model.DetailsGasProcurement model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into DetailsGasProcurement(");
            strSql.Append("RegGasProcurement_Code,CylinderCode,GasCode,AmountGas,IsPass,CheckDate,Status,CREATED_BY,CREATED_DATE,LAST_UPD_BY,LAST_UPD_DATE)");
            strSql.Append(" values (");
            strSql.Append("@RegGasProcurement_Code,@CylinderCode,@GasCode,@AmountGas,@IsPass,@CheckDate,@Status,@CREATED_BY,@CREATED_DATE,@LAST_UPD_BY,@LAST_UPD_DATE)");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
					new SqlParameter("@RegGasProcurement_Code", SqlDbType.VarChar,20),
					new SqlParameter("@CylinderCode", SqlDbType.VarChar,20),
					new SqlParameter("@GasCode", SqlDbType.VarChar,20),
					new SqlParameter("@AmountGas", SqlDbType.Decimal,9),
					new SqlParameter("@IsPass", SqlDbType.VarChar,4),
					new SqlParameter("@CheckDate", SqlDbType.Date,3),
					new SqlParameter("@Status", SqlDbType.VarChar,4),
					new SqlParameter("@CREATED_BY", SqlDbType.NVarChar,12),
					new SqlParameter("@CREATED_DATE", SqlDbType.Date,3),
					new SqlParameter("@LAST_UPD_BY", SqlDbType.NVarChar,12),
					new SqlParameter("@LAST_UPD_DATE", SqlDbType.Date,3)};
            parameters[0].Value = model.RegGasProcurement_Code;
            parameters[1].Value = model.CylinderCode;
            parameters[2].Value = model.GasCode;
            parameters[3].Value = model.AmountGas;
            parameters[4].Value = model.IsPass;
            parameters[5].Value = model.CheckDate;
            parameters[6].Value = model.Status;
            parameters[7].Value = model.CREATED_BY;
            parameters[8].Value = model.CREATED_DATE;
            parameters[9].Value = model.LAST_UPD_BY;
            parameters[10].Value = model.LAST_UPD_DATE;

            object obj = DbHelperSQL.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(EGMNGS.Model.DetailsGasProcurement model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update DetailsGasProcurement set ");
            strSql.Append("RegGasProcurement_Code=@RegGasProcurement_Code,");
            strSql.Append("CylinderCode=@CylinderCode,");
            strSql.Append("GasCode=@GasCode,");
            strSql.Append("AmountGas=@AmountGas,");
            strSql.Append("IsPass=@IsPass,");
            strSql.Append("CheckDate=@CheckDate,");
            strSql.Append("Status=@Status,");
            strSql.Append("CREATED_BY=@CREATED_BY,");
            strSql.Append("CREATED_DATE=@CREATED_DATE,");
            strSql.Append("LAST_UPD_BY=@LAST_UPD_BY,");
            strSql.Append("LAST_UPD_DATE=@LAST_UPD_DATE");
            strSql.Append(" where OID=@OID");
            SqlParameter[] parameters = {
					new SqlParameter("@RegGasProcurement_Code", SqlDbType.VarChar,20),
					new SqlParameter("@CylinderCode", SqlDbType.VarChar,20),
					new SqlParameter("@GasCode", SqlDbType.VarChar,20),
					new SqlParameter("@AmountGas", SqlDbType.Decimal,9),
					new SqlParameter("@IsPass", SqlDbType.VarChar,4),
					new SqlParameter("@CheckDate", SqlDbType.Date,3),
					new SqlParameter("@Status", SqlDbType.VarChar,4),
					new SqlParameter("@CREATED_BY", SqlDbType.NVarChar,12),
					new SqlParameter("@CREATED_DATE", SqlDbType.Date,3),
					new SqlParameter("@LAST_UPD_BY", SqlDbType.NVarChar,12),
					new SqlParameter("@LAST_UPD_DATE", SqlDbType.Date,3),
					new SqlParameter("@OID", SqlDbType.Int,4)};
            parameters[0].Value = model.RegGasProcurement_Code;
            parameters[1].Value = model.CylinderCode;
            parameters[2].Value = model.GasCode;
            parameters[3].Value = model.AmountGas;
            parameters[4].Value = model.IsPass;
            parameters[5].Value = model.CheckDate;
            parameters[6].Value = model.Status;
            parameters[7].Value = model.CREATED_BY;
            parameters[8].Value = model.CREATED_DATE;
            parameters[9].Value = model.LAST_UPD_BY;
            parameters[10].Value = model.LAST_UPD_DATE;
            parameters[11].Value = model.OID;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int OID)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from DetailsGasProcurement ");
            strSql.Append(" where OID=@OID");
            SqlParameter[] parameters = {
					new SqlParameter("@OID", SqlDbType.Int,4)
			};
            parameters[0].Value = OID;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string OIDlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from DetailsGasProcurement ");
            strSql.Append(" where OID in (" + OIDlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public EGMNGS.Model.DetailsGasProcurement GetModel(int OID)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 OID,RegGasProcurement_Code,CylinderCode,GasCode,AmountGas,IsPass,CheckDate,Status,CREATED_BY,CREATED_DATE,LAST_UPD_BY,LAST_UPD_DATE from DetailsGasProcurement ");
            strSql.Append(" where OID=@OID");
            SqlParameter[] parameters = {
					new SqlParameter("@OID", SqlDbType.Int,4)
			};
            parameters[0].Value = OID;

            EGMNGS.Model.DetailsGasProcurement model = new EGMNGS.Model.DetailsGasProcurement();
            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0]["OID"] != null && ds.Tables[0].Rows[0]["OID"].ToString() != "")
                {
                    model.OID = int.Parse(ds.Tables[0].Rows[0]["OID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["RegGasProcurement_Code"] != null && ds.Tables[0].Rows[0]["RegGasProcurement_Code"].ToString() != "")
                {
                    model.RegGasProcurement_Code = ds.Tables[0].Rows[0]["RegGasProcurement_Code"].ToString();
                }
                if (ds.Tables[0].Rows[0]["CylinderCode"] != null && ds.Tables[0].Rows[0]["CylinderCode"].ToString() != "")
                {
                    model.CylinderCode = ds.Tables[0].Rows[0]["CylinderCode"].ToString();
                }
                if (ds.Tables[0].Rows[0]["GasCode"] != null && ds.Tables[0].Rows[0]["GasCode"].ToString() != "")
                {
                    model.GasCode = ds.Tables[0].Rows[0]["GasCode"].ToString();
                }
                if (ds.Tables[0].Rows[0]["AmountGas"] != null && ds.Tables[0].Rows[0]["AmountGas"].ToString() != "")
                {
                    model.AmountGas = decimal.Parse(ds.Tables[0].Rows[0]["AmountGas"].ToString());
                }
                if (ds.Tables[0].Rows[0]["IsPass"] != null && ds.Tables[0].Rows[0]["IsPass"].ToString() != "")
                {
                    model.IsPass = ds.Tables[0].Rows[0]["IsPass"].ToString();
                }
                if (ds.Tables[0].Rows[0]["CheckDate"] != null && ds.Tables[0].Rows[0]["CheckDate"].ToString() != "")
                {
                    model.CheckDate = DateTime.Parse(ds.Tables[0].Rows[0]["CheckDate"].ToString());
                }
                if (ds.Tables[0].Rows[0]["Status"] != null && ds.Tables[0].Rows[0]["Status"].ToString() != "")
                {
                    model.Status = ds.Tables[0].Rows[0]["Status"].ToString();
                }
                if (ds.Tables[0].Rows[0]["CREATED_BY"] != null && ds.Tables[0].Rows[0]["CREATED_BY"].ToString() != "")
                {
                    model.CREATED_BY = ds.Tables[0].Rows[0]["CREATED_BY"].ToString();
                }
                if (ds.Tables[0].Rows[0]["CREATED_DATE"] != null && ds.Tables[0].Rows[0]["CREATED_DATE"].ToString() != "")
                {
                    model.CREATED_DATE = DateTime.Parse(ds.Tables[0].Rows[0]["CREATED_DATE"].ToString());
                }
                if (ds.Tables[0].Rows[0]["LAST_UPD_BY"] != null && ds.Tables[0].Rows[0]["LAST_UPD_BY"].ToString() != "")
                {
                    model.LAST_UPD_BY = ds.Tables[0].Rows[0]["LAST_UPD_BY"].ToString();
                }
                if (ds.Tables[0].Rows[0]["LAST_UPD_DATE"] != null && ds.Tables[0].Rows[0]["LAST_UPD_DATE"].ToString() != "")
                {
                    model.LAST_UPD_DATE = DateTime.Parse(ds.Tables[0].Rows[0]["LAST_UPD_DATE"].ToString());
                }
                return model;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select OID,RegGasProcurement_Code,CylinderCode,GasCode,AmountGas,IsPass,CheckDate,Status,CREATED_BY,CREATED_DATE,LAST_UPD_BY,LAST_UPD_DATE,(select CODE_CHI_DESC FROM dbo.SYS_CODE WHERE CODE_TYPE='IsPass' AND CODE=IsPass) 'IsPassName',(select CODE_CHI_DESC FROM dbo.SYS_CODE WHERE CODE_TYPE='CFStatus' AND CODE=Status) 'CFStatusName' ");
            strSql.Append(" FROM DetailsGasProcurement ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" OID,RegGasProcurement_Code,CylinderCode,GasCode,AmountGas,IsPass,CheckDate,Status,CREATED_BY,CREATED_DATE,LAST_UPD_BY,LAST_UPD_DATE ");
            strSql.Append(" FROM DetailsGasProcurement ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM DetailsGasProcurement ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.OID desc");
            }
            strSql.Append(")AS Row, T.*  from DetailsGasProcurement T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", (startIndex - 1) * endIndex + 1, startIndex * endIndex);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /*
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        {
            SqlParameter[] parameters = {
                    new SqlParameter("@tblName", SqlDbType.VarChar, 255),
                    new SqlParameter("@fldName", SqlDbType.VarChar, 255),
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@PageIndex", SqlDbType.Int),
                    new SqlParameter("@IsReCount", SqlDbType.Bit),
                    new SqlParameter("@OrderType", SqlDbType.Bit),
                    new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
                    };
            parameters[0].Value = "DetailsGasProcurement";
            parameters[1].Value = "OID";
            parameters[2].Value = PageSize;
            parameters[3].Value = PageIndex;
            parameters[4].Value = 0;
            parameters[5].Value = 0;
            parameters[6].Value = strWhere;	
            return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
        }*/

        #endregion  Method
    }
}


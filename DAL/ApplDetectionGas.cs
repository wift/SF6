﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Maticsoft.DBUtility;//Please add references
namespace EGMNGS.DAL
{
    /// <summary>
    /// 数据访问类:ApplDetectionGas
    /// </summary>
    public partial class ApplDetectionGas
    {
        public ApplDetectionGas()
        {

        }
        #region  Method

        /// <summary>
        /// 得到最大ID
        /// </summary>
        public int GetMaxId()
        {
            return DbHelperSQL.GetMaxID("OID", "ApplDetectionGas");
        }

        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int OID)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) from ApplDetectionGas");
            strSql.Append(" where OID=@OID");
            SqlParameter[] parameters = {
					new SqlParameter("@OID", SqlDbType.Int,4)
			};
            parameters[0].Value = OID;

            return DbHelperSQL.Exists(strSql.ToString(), parameters);
        }


        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(EGMNGS.Model.ApplDetectionGas model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into ApplDetectionGas(");
            strSql.Append("ApplCode,DevCode,PowerSupplyCode,PowerSupplyName,ConvertStationCode,convertStationName,DevName,DevNo,InspectionUnit,Contacts,OfficeTel,PhoneNum,SampliInspituation,AmountDetection,Status,RegistantsOID,RegistantsDate,CREATED_BY,CREATED_DATE,LAST_UPD_BY,LAST_UPD_DATE,CheckPJType)");
            strSql.Append(" values (");
            strSql.Append("@ApplCode,@DevCode,@PowerSupplyCode,@PowerSupplyName,@ConvertStationCode,@convertStationName,@DevName,@DevNo,@InspectionUnit,@Contacts,@OfficeTel,@PhoneNum,@SampliInspituation,@AmountDetection,@Status,@RegistantsOID,@RegistantsDate,@CREATED_BY,@CREATED_DATE,@LAST_UPD_BY,@LAST_UPD_DATE,@CheckPJType)");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
					new SqlParameter("@ApplCode", SqlDbType.VarChar,20),
					new SqlParameter("@DevCode", SqlDbType.VarChar,20),
					new SqlParameter("@PowerSupplyCode", SqlDbType.VarChar,50),
					new SqlParameter("@PowerSupplyName", SqlDbType.VarChar,50),
					new SqlParameter("@ConvertStationCode", SqlDbType.VarChar,20),
					new SqlParameter("@convertStationName", SqlDbType.VarChar,50),
					new SqlParameter("@DevName", SqlDbType.VarChar,50),
					new SqlParameter("@DevNo", SqlDbType.VarChar,50),
					new SqlParameter("@InspectionUnit", SqlDbType.VarChar,50),
					new SqlParameter("@Contacts", SqlDbType.VarChar,50),
					new SqlParameter("@OfficeTel", SqlDbType.NVarChar,100),
					new SqlParameter("@PhoneNum", SqlDbType.VarChar,20),
					new SqlParameter("@SampliInspituation", SqlDbType.NVarChar,500),
					new SqlParameter("@AmountDetection", SqlDbType.Decimal,9),
					new SqlParameter("@Status", SqlDbType.VarChar,4),
					new SqlParameter("@RegistantsOID", SqlDbType.VarChar,50),
					new SqlParameter("@RegistantsDate", SqlDbType.Date,3),
					new SqlParameter("@CREATED_BY", SqlDbType.NVarChar,12),
					new SqlParameter("@CREATED_DATE", SqlDbType.Date,3),
					new SqlParameter("@LAST_UPD_BY", SqlDbType.NVarChar,12),
					new SqlParameter("@LAST_UPD_DATE", SqlDbType.Date,3),
                    new SqlParameter("@CheckPJType", SqlDbType.VarChar,4)
                                        };
            parameters[0].Value = model.ApplCode;
            parameters[1].Value = model.DevCode;
            parameters[2].Value = model.PowerSupplyCode;
            parameters[3].Value = model.PowerSupplyName;
            parameters[4].Value = model.ConvertStationCode;
            parameters[5].Value = model.convertStationName;
            parameters[6].Value = model.DevName;
            parameters[7].Value = model.DevNo;
            parameters[8].Value = model.InspectionUnit;
            parameters[9].Value = model.Contacts;
            parameters[10].Value = model.OfficeTel;
            parameters[11].Value = model.PhoneNum;
            parameters[12].Value = model.SampliInspituation;
            parameters[13].Value = model.AmountDetection;
            parameters[14].Value = model.Status;
            parameters[15].Value = model.RegistantsOID;
            parameters[16].Value = model.RegistantsDate;
            parameters[17].Value = model.CREATED_BY;
            parameters[18].Value = model.CREATED_DATE;
            parameters[19].Value = model.LAST_UPD_BY;
            parameters[20].Value = model.LAST_UPD_DATE;
            parameters[21].Value = model.CheckPJType;

            object obj = DbHelperSQL.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {
                EGMNGS.Common.ComServies.UpdCodeNum(model.ApplCode);
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(EGMNGS.Model.ApplDetectionGas model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update ApplDetectionGas set ");
            strSql.Append("ApplCode=@ApplCode,");
            strSql.Append("DevCode=@DevCode,");
            strSql.Append("PowerSupplyCode=@PowerSupplyCode,");
            strSql.Append("PowerSupplyName=@PowerSupplyName,");
            strSql.Append("ConvertStationCode=@ConvertStationCode,");
            strSql.Append("convertStationName=@convertStationName,");
            strSql.Append("DevName=@DevName,");
            strSql.Append("DevNo=@DevNo,");
            strSql.Append("InspectionUnit=@InspectionUnit,");
            strSql.Append("Contacts=@Contacts,");
            strSql.Append("OfficeTel=@OfficeTel,");
            strSql.Append("PhoneNum=@PhoneNum,");
            strSql.Append("SampliInspituation=@SampliInspituation,");
            strSql.Append("AmountDetection=@AmountDetection,");
            strSql.Append("Status=@Status,");
            strSql.Append("RegistantsOID=@RegistantsOID,");
            strSql.Append("RegistantsDate=@RegistantsDate,");
            strSql.Append("CREATED_BY=@CREATED_BY,");
            strSql.Append("CREATED_DATE=@CREATED_DATE,");
            strSql.Append("LAST_UPD_BY=@LAST_UPD_BY,");
            strSql.Append("CheckPJType=@CheckPJType");
            strSql.Append(" where OID=@OID");
            SqlParameter[] parameters = {
					new SqlParameter("@ApplCode", SqlDbType.VarChar,20),
					new SqlParameter("@DevCode", SqlDbType.VarChar,20),
					new SqlParameter("@PowerSupplyCode", SqlDbType.VarChar,50),
					new SqlParameter("@PowerSupplyName", SqlDbType.VarChar,50),
					new SqlParameter("@ConvertStationCode", SqlDbType.VarChar,20),
					new SqlParameter("@convertStationName", SqlDbType.VarChar,50),
					new SqlParameter("@DevName", SqlDbType.VarChar,50),
					new SqlParameter("@DevNo", SqlDbType.VarChar,50),
					new SqlParameter("@InspectionUnit", SqlDbType.VarChar,50),
					new SqlParameter("@Contacts", SqlDbType.VarChar,50),
					new SqlParameter("@OfficeTel", SqlDbType.NVarChar,100),
					new SqlParameter("@PhoneNum", SqlDbType.VarChar,20),
					new SqlParameter("@SampliInspituation", SqlDbType.NVarChar,500),
					new SqlParameter("@AmountDetection", SqlDbType.Decimal,9),
					new SqlParameter("@Status", SqlDbType.VarChar,4),
					new SqlParameter("@RegistantsOID", SqlDbType.VarChar,50),
					new SqlParameter("@RegistantsDate", SqlDbType.Date,3),
					new SqlParameter("@CREATED_BY", SqlDbType.NVarChar,12),
					new SqlParameter("@CREATED_DATE", SqlDbType.Date,3),
					new SqlParameter("@LAST_UPD_BY", SqlDbType.NVarChar,12),
					new SqlParameter("@CheckPJType", SqlDbType.VarChar,4),
					new SqlParameter("@OID", SqlDbType.Int,4)};
            parameters[0].Value = model.ApplCode;
            parameters[1].Value = model.DevCode;
            parameters[2].Value = model.PowerSupplyCode;
            parameters[3].Value = model.PowerSupplyName;
            parameters[4].Value = model.ConvertStationCode;
            parameters[5].Value = model.convertStationName;
            parameters[6].Value = model.DevName;
            parameters[7].Value = model.DevNo;
            parameters[8].Value = model.InspectionUnit;
            parameters[9].Value = model.Contacts;
            parameters[10].Value = model.OfficeTel;
            parameters[11].Value = model.PhoneNum;
            parameters[12].Value = model.SampliInspituation;
            parameters[13].Value = model.AmountDetection;
            parameters[14].Value = model.Status;
            parameters[15].Value = model.RegistantsOID;
            parameters[16].Value = model.RegistantsDate;
            parameters[17].Value = model.CREATED_BY;
            parameters[18].Value = model.CREATED_DATE;
            parameters[19].Value = model.LAST_UPD_BY;
            parameters[20].Value = model.CheckPJType;
            parameters[21].Value = model.OID;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int OID)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from ApplDetectionGas ");
            strSql.Append(" where OID=@OID");
            SqlParameter[] parameters = {
					new SqlParameter("@OID", SqlDbType.Int,4)
			};
            parameters[0].Value = OID;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string OIDlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from ApplDetectionGas ");
            strSql.Append(" where OID in (" + OIDlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public EGMNGS.Model.ApplDetectionGas GetModel(int OID)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 OID,ApplCode,DevCode,PowerSupplyCode,PowerSupplyName,ConvertStationCode,convertStationName,DevName,DevNo,InspectionUnit,Contacts,OfficeTel,PhoneNum,SampliInspituation,AmountDetection,Status,RegistantsOID,RegistantsDate,CREATED_BY,CREATED_DATE,LAST_UPD_BY,CheckPJType from ApplDetectionGas ");
            strSql.Append(" where OID=@OID");
            SqlParameter[] parameters = {
					new SqlParameter("@OID", SqlDbType.Int,4)
			};
            parameters[0].Value = OID;

            EGMNGS.Model.ApplDetectionGas model = new EGMNGS.Model.ApplDetectionGas();
            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0]["OID"] != null && ds.Tables[0].Rows[0]["OID"].ToString() != "")
                {
                    model.OID = int.Parse(ds.Tables[0].Rows[0]["OID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["ApplCode"] != null && ds.Tables[0].Rows[0]["ApplCode"].ToString() != "")
                {
                    model.ApplCode = ds.Tables[0].Rows[0]["ApplCode"].ToString();
                }
                if (ds.Tables[0].Rows[0]["DevCode"] != null && ds.Tables[0].Rows[0]["DevCode"].ToString() != "")
                {
                    model.DevCode = ds.Tables[0].Rows[0]["DevCode"].ToString();
                }
                if (ds.Tables[0].Rows[0]["PowerSupplyCode"] != null && ds.Tables[0].Rows[0]["PowerSupplyCode"].ToString() != "")
                {
                    model.PowerSupplyCode = ds.Tables[0].Rows[0]["PowerSupplyCode"].ToString();
                }
                if (ds.Tables[0].Rows[0]["PowerSupplyName"] != null && ds.Tables[0].Rows[0]["PowerSupplyName"].ToString() != "")
                {
                    model.PowerSupplyName = ds.Tables[0].Rows[0]["PowerSupplyName"].ToString();
                }
                if (ds.Tables[0].Rows[0]["ConvertStationCode"] != null && ds.Tables[0].Rows[0]["ConvertStationCode"].ToString() != "")
                {
                    model.ConvertStationCode = ds.Tables[0].Rows[0]["ConvertStationCode"].ToString();
                }
                if (ds.Tables[0].Rows[0]["convertStationName"] != null && ds.Tables[0].Rows[0]["convertStationName"].ToString() != "")
                {
                    model.convertStationName = ds.Tables[0].Rows[0]["convertStationName"].ToString();
                }
                if (ds.Tables[0].Rows[0]["DevName"] != null && ds.Tables[0].Rows[0]["DevName"].ToString() != "")
                {
                    model.DevName = ds.Tables[0].Rows[0]["DevName"].ToString();
                }
                if (ds.Tables[0].Rows[0]["DevNo"] != null && ds.Tables[0].Rows[0]["DevNo"].ToString() != "")
                {
                    model.DevNo = ds.Tables[0].Rows[0]["DevNo"].ToString();
                }
                if (ds.Tables[0].Rows[0]["InspectionUnit"] != null && ds.Tables[0].Rows[0]["InspectionUnit"].ToString() != "")
                {
                    model.InspectionUnit = ds.Tables[0].Rows[0]["InspectionUnit"].ToString();
                }
                if (ds.Tables[0].Rows[0]["Contacts"] != null && ds.Tables[0].Rows[0]["Contacts"].ToString() != "")
                {
                    model.Contacts = ds.Tables[0].Rows[0]["Contacts"].ToString();
                }
                if (ds.Tables[0].Rows[0]["OfficeTel"] != null && ds.Tables[0].Rows[0]["OfficeTel"].ToString() != "")
                {
                    model.OfficeTel = ds.Tables[0].Rows[0]["OfficeTel"].ToString();
                }
                if (ds.Tables[0].Rows[0]["PhoneNum"] != null && ds.Tables[0].Rows[0]["PhoneNum"].ToString() != "")
                {
                    model.PhoneNum = ds.Tables[0].Rows[0]["PhoneNum"].ToString();
                }
                if (ds.Tables[0].Rows[0]["SampliInspituation"] != null && ds.Tables[0].Rows[0]["SampliInspituation"].ToString() != "")
                {
                    model.SampliInspituation = ds.Tables[0].Rows[0]["SampliInspituation"].ToString();
                }
                if (ds.Tables[0].Rows[0]["AmountDetection"] != null && ds.Tables[0].Rows[0]["AmountDetection"].ToString() != "")
                {
                    model.AmountDetection = decimal.Parse(ds.Tables[0].Rows[0]["AmountDetection"].ToString());
                }
                if (ds.Tables[0].Rows[0]["Status"] != null && ds.Tables[0].Rows[0]["Status"].ToString() != "")
                {
                    model.Status = ds.Tables[0].Rows[0]["Status"].ToString();
                }
                if (ds.Tables[0].Rows[0]["RegistantsOID"] != null && ds.Tables[0].Rows[0]["RegistantsOID"].ToString() != "")
                {
                    model.RegistantsOID = ds.Tables[0].Rows[0]["RegistantsOID"].ToString();
                }
                if (ds.Tables[0].Rows[0]["RegistantsDate"] != null && ds.Tables[0].Rows[0]["RegistantsDate"].ToString() != "")
                {
                    model.RegistantsDate = DateTime.Parse(ds.Tables[0].Rows[0]["RegistantsDate"].ToString());
                }
                if (ds.Tables[0].Rows[0]["CREATED_BY"] != null && ds.Tables[0].Rows[0]["CREATED_BY"].ToString() != "")
                {
                    model.CREATED_BY = ds.Tables[0].Rows[0]["CREATED_BY"].ToString();
                }
                if (ds.Tables[0].Rows[0]["CREATED_DATE"] != null && ds.Tables[0].Rows[0]["CREATED_DATE"].ToString() != "")
                {
                    model.CREATED_DATE = DateTime.Parse(ds.Tables[0].Rows[0]["CREATED_DATE"].ToString());
                }
                if (ds.Tables[0].Rows[0]["LAST_UPD_BY"] != null && ds.Tables[0].Rows[0]["LAST_UPD_BY"].ToString() != "")
                {
                    model.LAST_UPD_BY = ds.Tables[0].Rows[0]["LAST_UPD_BY"].ToString();
                }
                if (ds.Tables[0].Rows[0]["CheckPJType"] != null && ds.Tables[0].Rows[0]["CheckPJType"].ToString() != "")
                {
                    model.CheckPJType = ds.Tables[0].Rows[0]["CheckPJType"].ToString(); ;
                }
                return model;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select OID,ApplCode,DevCode,PowerSupplyCode,PowerSupplyName,ConvertStationCode,convertStationName,DevName,DevNo,InspectionUnit,Contacts,OfficeTel,PhoneNum,SampliInspituation,AmountDetection,Status,RegistantsOID,RegistantsDate,CREATED_BY,CREATED_DATE,LAST_UPD_BY,LAST_UPD_DATE,CheckPJType ");
            strSql.Append(" FROM ApplDetectionGas ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" OID,ApplCode,DevCode,PowerSupplyCode,PowerSupplyName,ConvertStationCode,convertStationName,DevName,DevNo,InspectionUnit,Contacts,OfficeTel,PhoneNum,SampliInspituation,AmountDetection,Status,RegistantsOID,RegistantsDate,CREATED_BY,CREATED_DATE,LAST_UPD_BY,LAST_UPD_DATE,CheckPJType ");
            strSql.Append(" FROM ApplDetectionGas ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM ApplDetectionGas ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.OID desc");
            }
            strSql.Append(")AS Row, T.*  from ApplDetectionGas T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", (startIndex - 1) * endIndex + 1, startIndex * endIndex);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /*
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        {
            SqlParameter[] parameters = {
                    new SqlParameter("@tblName", SqlDbType.VarChar, 255),
                    new SqlParameter("@fldName", SqlDbType.VarChar, 255),
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@PageIndex", SqlDbType.Int),
                    new SqlParameter("@IsReCount", SqlDbType.Bit),
                    new SqlParameter("@OrderType", SqlDbType.Bit),
                    new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
                    };
            parameters[0].Value = "ApplDetectionGas";
            parameters[1].Value = "OID";
            parameters[2].Value = PageSize;
            parameters[3].Value = PageIndex;
            parameters[4].Value = 0;
            parameters[5].Value = 0;
            parameters[6].Value = strWhere;	
            return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
        }*/

        #endregion  Method
    }
}


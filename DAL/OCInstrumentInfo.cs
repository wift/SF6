﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Maticsoft.DBUtility;//Please add references
namespace EGMNGS.DAL
{
	/// <summary>
	/// 数据访问类:OCInstrumentInfo
	/// </summary>
	public partial class OCInstrumentInfo
	{
		public OCInstrumentInfo()
		{}
		#region  BasicMethod

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
		return DbHelperSQL.GetMaxID("OID", "OCInstrumentInfo"); 
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int OID)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from OCInstrumentInfo");
			strSql.Append(" where OID=@OID");
			SqlParameter[] parameters = {
					new SqlParameter("@OID", SqlDbType.Int,4)
			};
			parameters[0].Value = OID;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(EGMNGS.Model.OCInstrumentInfo model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into OCInstrumentInfo(");
			strSql.Append("CheckPointCode,CheckPointName,InstrumentCode,InstrumentName,RunStatus,IntallPostion,BelongEquipment,BelongPowerStation,VoltageLevel,Manufacturer,RunDate,CalibrationDate,ControlRunStatus,ControlRunFromDate,ControlRunToDate,Cycle,ControlPressureMin,ControlPressureMax,ControlTempertureMin,ControlTempertureMax,ControlMicroWater,ControlSO2,ControlCO,ControlCF4,ControlSO2F2,ControlSOF2,ControlCS2,ControlHF,CREATED_BY,CREATED_DATE,LAST_UPD_BY,LAST_UPD_DATE,BelongEquipmentName,BelongPowerStationName,ControlPressureMins,ControlPressureMaxs,ControlTempertureMins,ControlTempertureMaxs,ControlMicroWaters,ControlSO2s,ControlCOs,ControlCF4s,ControlSO2F2s,ControlSOF2s,ControlCS2s,ControlHFs,AlterCount,PH2S,MH2S,PCOS,MCOS,PC2F6,MC2F6,PC3F8,MC3F8)");
			strSql.Append(" values (");
			strSql.Append("@CheckPointCode,@CheckPointName,@InstrumentCode,@InstrumentName,@RunStatus,@IntallPostion,@BelongEquipment,@BelongPowerStation,@VoltageLevel,@Manufacturer,@RunDate,@CalibrationDate,@ControlRunStatus,@ControlRunFromDate,@ControlRunToDate,@Cycle,@ControlPressureMin,@ControlPressureMax,@ControlTempertureMin,@ControlTempertureMax,@ControlMicroWater,@ControlSO2,@ControlCO,@ControlCF4,@ControlSO2F2,@ControlSOF2,@ControlCS2,@ControlHF,@CREATED_BY,@CREATED_DATE,@LAST_UPD_BY,@LAST_UPD_DATE,@BelongEquipmentName,@BelongPowerStationName,@ControlPressureMins,@ControlPressureMaxs,@ControlTempertureMins,@ControlTempertureMaxs,@ControlMicroWaters,@ControlSO2s,@ControlCOs,@ControlCF4s,@ControlSO2F2s,@ControlSOF2s,@ControlCS2s,@ControlHFs,@AlterCount,@PH2S,@MH2S,@PCOS,@MCOS,@PC2F6,@MC2F6,@PC3F8,@MC3F8)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@CheckPointCode", SqlDbType.VarChar,4),
					new SqlParameter("@CheckPointName", SqlDbType.NVarChar,50),
					new SqlParameter("@InstrumentCode", SqlDbType.VarChar,4),
					new SqlParameter("@InstrumentName", SqlDbType.NVarChar,50),
					new SqlParameter("@RunStatus", SqlDbType.VarChar,4),
					new SqlParameter("@IntallPostion", SqlDbType.VarChar,50),
					new SqlParameter("@BelongEquipment", SqlDbType.VarChar,50),
					new SqlParameter("@BelongPowerStation", SqlDbType.VarChar,50),
					new SqlParameter("@VoltageLevel", SqlDbType.VarChar,4),
					new SqlParameter("@Manufacturer", SqlDbType.VarChar,50),
					new SqlParameter("@RunDate", SqlDbType.DateTime),
					new SqlParameter("@CalibrationDate", SqlDbType.DateTime),
					new SqlParameter("@ControlRunStatus", SqlDbType.VarChar,4),
					new SqlParameter("@ControlRunFromDate", SqlDbType.DateTime),
					new SqlParameter("@ControlRunToDate", SqlDbType.DateTime),
					new SqlParameter("@Cycle", SqlDbType.Int,4),
					new SqlParameter("@ControlPressureMin", SqlDbType.Decimal,9),
					new SqlParameter("@ControlPressureMax", SqlDbType.Decimal,9),
					new SqlParameter("@ControlTempertureMin", SqlDbType.Decimal,9),
					new SqlParameter("@ControlTempertureMax", SqlDbType.Decimal,9),
					new SqlParameter("@ControlMicroWater", SqlDbType.Decimal,9),
					new SqlParameter("@ControlSO2", SqlDbType.Decimal,9),
					new SqlParameter("@ControlCO", SqlDbType.Decimal,9),
					new SqlParameter("@ControlCF4", SqlDbType.Decimal,9),
					new SqlParameter("@ControlSO2F2", SqlDbType.Decimal,9),
					new SqlParameter("@ControlSOF2", SqlDbType.Decimal,9),
					new SqlParameter("@ControlCS2", SqlDbType.Decimal,9),
					new SqlParameter("@ControlHF", SqlDbType.Decimal,9),
					new SqlParameter("@CREATED_BY", SqlDbType.NVarChar,12),
					new SqlParameter("@CREATED_DATE", SqlDbType.Date,3),
					new SqlParameter("@LAST_UPD_BY", SqlDbType.NVarChar,12),
					new SqlParameter("@LAST_UPD_DATE", SqlDbType.Date,3),
					new SqlParameter("@BelongEquipmentName", SqlDbType.VarChar,50),
					new SqlParameter("@BelongPowerStationName", SqlDbType.VarChar,50),
					new SqlParameter("@ControlPressureMins", SqlDbType.Decimal,9),
					new SqlParameter("@ControlPressureMaxs", SqlDbType.Decimal,9),
					new SqlParameter("@ControlTempertureMins", SqlDbType.Decimal,9),
					new SqlParameter("@ControlTempertureMaxs", SqlDbType.Decimal,9),
					new SqlParameter("@ControlMicroWaters", SqlDbType.Decimal,9),
					new SqlParameter("@ControlSO2s", SqlDbType.Decimal,9),
					new SqlParameter("@ControlCOs", SqlDbType.Decimal,9),
					new SqlParameter("@ControlCF4s", SqlDbType.Decimal,9),
					new SqlParameter("@ControlSO2F2s", SqlDbType.Decimal,9),
					new SqlParameter("@ControlSOF2s", SqlDbType.Decimal,9),
					new SqlParameter("@ControlCS2s", SqlDbType.Decimal,9),
					new SqlParameter("@ControlHFs", SqlDbType.Decimal,9),
					new SqlParameter("@AlterCount", SqlDbType.Int,4),
                        new SqlParameter("@PH2S", SqlDbType.Decimal,9),
                    new SqlParameter("@MH2S", SqlDbType.Decimal,9),
                    new SqlParameter("@PCOS", SqlDbType.Decimal,9),
                    new SqlParameter("@MCOS", SqlDbType.Decimal,9),
                    new SqlParameter("@PC2F6", SqlDbType.Decimal,9),
                    new SqlParameter("@MC2F6", SqlDbType.Decimal,9),
                    new SqlParameter("@PC3F8", SqlDbType.Decimal,9),
                    new SqlParameter("@MC3F8", SqlDbType.Decimal,9)};
            parameters[0].Value = model.CheckPointCode;
			parameters[1].Value = model.CheckPointName;
			parameters[2].Value = model.InstrumentCode;
			parameters[3].Value = model.InstrumentName;
			parameters[4].Value = model.RunStatus;
			parameters[5].Value = model.IntallPostion;
			parameters[6].Value = model.BelongEquipment;
			parameters[7].Value = model.BelongPowerStation;
			parameters[8].Value = model.VoltageLevel;
			parameters[9].Value = model.Manufacturer;
			parameters[10].Value = model.RunDate;
			parameters[11].Value = model.CalibrationDate;
			parameters[12].Value = model.ControlRunStatus;
			parameters[13].Value = model.ControlRunFromDate;
			parameters[14].Value = model.ControlRunToDate;
			parameters[15].Value = model.Cycle;
			parameters[16].Value = model.ControlPressureMin;
			parameters[17].Value = model.ControlPressureMax;
			parameters[18].Value = model.ControlTempertureMin;
			parameters[19].Value = model.ControlTempertureMax;
			parameters[20].Value = model.ControlMicroWater;
			parameters[21].Value = model.ControlSO2;
			parameters[22].Value = model.ControlCO;
			parameters[23].Value = model.ControlCF4;
			parameters[24].Value = model.ControlSO2F2;
			parameters[25].Value = model.ControlSOF2;
			parameters[26].Value = model.ControlCS2;
			parameters[27].Value = model.ControlHF;
			parameters[28].Value = model.CREATED_BY;
			parameters[29].Value = model.CREATED_DATE;
			parameters[30].Value = model.LAST_UPD_BY;
			parameters[31].Value = model.LAST_UPD_DATE;
			parameters[32].Value = model.BelongEquipmentName;
			parameters[33].Value = model.BelongPowerStationName;
			parameters[34].Value = model.ControlPressureMins;
			parameters[35].Value = model.ControlPressureMaxs;
			parameters[36].Value = model.ControlTempertureMins;
			parameters[37].Value = model.ControlTempertureMaxs;
			parameters[38].Value = model.ControlMicroWaters;
			parameters[39].Value = model.ControlSO2s;
			parameters[40].Value = model.ControlCOs;
			parameters[41].Value = model.ControlCF4s;
			parameters[42].Value = model.ControlSO2F2s;
			parameters[43].Value = model.ControlSOF2s;
			parameters[44].Value = model.ControlCS2s;
			parameters[45].Value = model.ControlHFs;
			parameters[46].Value = model.AlterCount;
            parameters[47].Value = model.PH2S;
            parameters[48].Value = model.MH2S;
            parameters[49].Value = model.PCOS;
            parameters[50].Value = model.MCOS;
            parameters[51].Value = model.PC2F6;
            parameters[52].Value = model.MC2F6;
            parameters[53].Value = model.PC3F8;
            parameters[54].Value = model.MC3F8;

            object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(EGMNGS.Model.OCInstrumentInfo model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update OCInstrumentInfo set ");
			strSql.Append("CheckPointCode=@CheckPointCode,");
			strSql.Append("CheckPointName=@CheckPointName,");
			strSql.Append("InstrumentCode=@InstrumentCode,");
			strSql.Append("InstrumentName=@InstrumentName,");
			strSql.Append("RunStatus=@RunStatus,");
			strSql.Append("IntallPostion=@IntallPostion,");
			strSql.Append("BelongEquipment=@BelongEquipment,");
			strSql.Append("BelongPowerStation=@BelongPowerStation,");
			strSql.Append("VoltageLevel=@VoltageLevel,");
			strSql.Append("Manufacturer=@Manufacturer,");
			strSql.Append("RunDate=@RunDate,");
			strSql.Append("CalibrationDate=@CalibrationDate,");
			strSql.Append("ControlRunStatus=@ControlRunStatus,");
			strSql.Append("ControlRunFromDate=@ControlRunFromDate,");
			strSql.Append("ControlRunToDate=@ControlRunToDate,");
			strSql.Append("Cycle=@Cycle,");
			strSql.Append("ControlPressureMin=@ControlPressureMin,");
			strSql.Append("ControlPressureMax=@ControlPressureMax,");
			strSql.Append("ControlTempertureMin=@ControlTempertureMin,");
			strSql.Append("ControlTempertureMax=@ControlTempertureMax,");
			strSql.Append("ControlMicroWater=@ControlMicroWater,");
			strSql.Append("ControlSO2=@ControlSO2,");
			strSql.Append("ControlCO=@ControlCO,");
			strSql.Append("ControlCF4=@ControlCF4,");
			strSql.Append("ControlSO2F2=@ControlSO2F2,");
			strSql.Append("ControlSOF2=@ControlSOF2,");
			strSql.Append("ControlCS2=@ControlCS2,");
			strSql.Append("ControlHF=@ControlHF,");
			strSql.Append("CREATED_BY=@CREATED_BY,");
			strSql.Append("CREATED_DATE=@CREATED_DATE,");
			strSql.Append("LAST_UPD_BY=@LAST_UPD_BY,");
			strSql.Append("LAST_UPD_DATE=@LAST_UPD_DATE,");
			strSql.Append("BelongEquipmentName=@BelongEquipmentName,");
			strSql.Append("BelongPowerStationName=@BelongPowerStationName,");
			strSql.Append("ControlPressureMins=@ControlPressureMins,");
			strSql.Append("ControlPressureMaxs=@ControlPressureMaxs,");
			strSql.Append("ControlTempertureMins=@ControlTempertureMins,");
			strSql.Append("ControlTempertureMaxs=@ControlTempertureMaxs,");
			strSql.Append("ControlMicroWaters=@ControlMicroWaters,");
			strSql.Append("ControlSO2s=@ControlSO2s,");
			strSql.Append("ControlCOs=@ControlCOs,");
			strSql.Append("ControlCF4s=@ControlCF4s,");
			strSql.Append("ControlSO2F2s=@ControlSO2F2s,");
			strSql.Append("ControlSOF2s=@ControlSOF2s,");
			strSql.Append("ControlCS2s=@ControlCS2s,");
			strSql.Append("ControlHFs=@ControlHFs,");
			strSql.Append("AlterCount=@AlterCount,");
            strSql.Append("PH2S=@PH2S,");
            strSql.Append("MH2S=@MH2S,");
            strSql.Append("PCOS=@PCOS,");
            strSql.Append("MCOS=@MCOS,");
            strSql.Append("PC2F6=@PC2F6,");
            strSql.Append("MC2F6=@MC2F6,");
            strSql.Append("PC3F8=@PC3F8,");
            strSql.Append("MC3F8=@MC3F8");
            strSql.Append(" where OID=@OID");
			SqlParameter[] parameters = {
					new SqlParameter("@CheckPointCode", SqlDbType.VarChar,4),
					new SqlParameter("@CheckPointName", SqlDbType.NVarChar,50),
					new SqlParameter("@InstrumentCode", SqlDbType.VarChar,4),
					new SqlParameter("@InstrumentName", SqlDbType.NVarChar,50),
					new SqlParameter("@RunStatus", SqlDbType.VarChar,4),
					new SqlParameter("@IntallPostion", SqlDbType.VarChar,50),
					new SqlParameter("@BelongEquipment", SqlDbType.VarChar,50),
					new SqlParameter("@BelongPowerStation", SqlDbType.VarChar,50),
					new SqlParameter("@VoltageLevel", SqlDbType.VarChar,4),
					new SqlParameter("@Manufacturer", SqlDbType.VarChar,50),
					new SqlParameter("@RunDate", SqlDbType.DateTime),
					new SqlParameter("@CalibrationDate", SqlDbType.DateTime),
					new SqlParameter("@ControlRunStatus", SqlDbType.VarChar,4),
					new SqlParameter("@ControlRunFromDate", SqlDbType.DateTime),
					new SqlParameter("@ControlRunToDate", SqlDbType.DateTime),
					new SqlParameter("@Cycle", SqlDbType.Int,4),
					new SqlParameter("@ControlPressureMin", SqlDbType.Decimal,9),
					new SqlParameter("@ControlPressureMax", SqlDbType.Decimal,9),
					new SqlParameter("@ControlTempertureMin", SqlDbType.Decimal,9),
					new SqlParameter("@ControlTempertureMax", SqlDbType.Decimal,9),
					new SqlParameter("@ControlMicroWater", SqlDbType.Decimal,9),
					new SqlParameter("@ControlSO2", SqlDbType.Decimal,9),
					new SqlParameter("@ControlCO", SqlDbType.Decimal,9),
					new SqlParameter("@ControlCF4", SqlDbType.Decimal,9),
					new SqlParameter("@ControlSO2F2", SqlDbType.Decimal,9),
					new SqlParameter("@ControlSOF2", SqlDbType.Decimal,9),
					new SqlParameter("@ControlCS2", SqlDbType.Decimal,9),
					new SqlParameter("@ControlHF", SqlDbType.Decimal,9),
					new SqlParameter("@CREATED_BY", SqlDbType.NVarChar,12),
					new SqlParameter("@CREATED_DATE", SqlDbType.Date,3),
					new SqlParameter("@LAST_UPD_BY", SqlDbType.NVarChar,12),
					new SqlParameter("@LAST_UPD_DATE", SqlDbType.Date,3),
					new SqlParameter("@BelongEquipmentName", SqlDbType.VarChar,50),
					new SqlParameter("@BelongPowerStationName", SqlDbType.VarChar,50),
					new SqlParameter("@ControlPressureMins", SqlDbType.Decimal,9),
					new SqlParameter("@ControlPressureMaxs", SqlDbType.Decimal,9),
					new SqlParameter("@ControlTempertureMins", SqlDbType.Decimal,9),
					new SqlParameter("@ControlTempertureMaxs", SqlDbType.Decimal,9),
					new SqlParameter("@ControlMicroWaters", SqlDbType.Decimal,9),
					new SqlParameter("@ControlSO2s", SqlDbType.Decimal,9),
					new SqlParameter("@ControlCOs", SqlDbType.Decimal,9),
					new SqlParameter("@ControlCF4s", SqlDbType.Decimal,9),
					new SqlParameter("@ControlSO2F2s", SqlDbType.Decimal,9),
					new SqlParameter("@ControlSOF2s", SqlDbType.Decimal,9),
					new SqlParameter("@ControlCS2s", SqlDbType.Decimal,9),
					new SqlParameter("@ControlHFs", SqlDbType.Decimal,9),
					new SqlParameter("@AlterCount", SqlDbType.Int,4),
                    new SqlParameter("@PH2S", SqlDbType.Decimal,9),
                    new SqlParameter("@MH2S", SqlDbType.Decimal,9),
                    new SqlParameter("@PCOS", SqlDbType.Decimal,9),
                    new SqlParameter("@MCOS", SqlDbType.Decimal,9),
                    new SqlParameter("@PC2F6", SqlDbType.Decimal,9),
                    new SqlParameter("@MC2F6", SqlDbType.Decimal,9),
                    new SqlParameter("@PC3F8", SqlDbType.Decimal,9),
                    new SqlParameter("@MC3F8", SqlDbType.Decimal,9),
                    new SqlParameter("@OID", SqlDbType.Int,4)};
			parameters[0].Value = model.CheckPointCode;
			parameters[1].Value = model.CheckPointName;
			parameters[2].Value = model.InstrumentCode;
			parameters[3].Value = model.InstrumentName;
			parameters[4].Value = model.RunStatus;
			parameters[5].Value = model.IntallPostion;
			parameters[6].Value = model.BelongEquipment;
			parameters[7].Value = model.BelongPowerStation;
			parameters[8].Value = model.VoltageLevel;
			parameters[9].Value = model.Manufacturer;
			parameters[10].Value = model.RunDate;
			parameters[11].Value = model.CalibrationDate;
			parameters[12].Value = model.ControlRunStatus;
			parameters[13].Value = model.ControlRunFromDate;
			parameters[14].Value = model.ControlRunToDate;
			parameters[15].Value = model.Cycle;
			parameters[16].Value = model.ControlPressureMin;
			parameters[17].Value = model.ControlPressureMax;
			parameters[18].Value = model.ControlTempertureMin;
			parameters[19].Value = model.ControlTempertureMax;
			parameters[20].Value = model.ControlMicroWater;
			parameters[21].Value = model.ControlSO2;
			parameters[22].Value = model.ControlCO;
			parameters[23].Value = model.ControlCF4;
			parameters[24].Value = model.ControlSO2F2;
			parameters[25].Value = model.ControlSOF2;
			parameters[26].Value = model.ControlCS2;
			parameters[27].Value = model.ControlHF;
			parameters[28].Value = model.CREATED_BY;
			parameters[29].Value = model.CREATED_DATE;
			parameters[30].Value = model.LAST_UPD_BY;
			parameters[31].Value = model.LAST_UPD_DATE;
			parameters[32].Value = model.BelongEquipmentName;
			parameters[33].Value = model.BelongPowerStationName;
			parameters[34].Value = model.ControlPressureMins;
			parameters[35].Value = model.ControlPressureMaxs;
			parameters[36].Value = model.ControlTempertureMins;
			parameters[37].Value = model.ControlTempertureMaxs;
			parameters[38].Value = model.ControlMicroWaters;
			parameters[39].Value = model.ControlSO2s;
			parameters[40].Value = model.ControlCOs;
			parameters[41].Value = model.ControlCF4s;
			parameters[42].Value = model.ControlSO2F2s;
			parameters[43].Value = model.ControlSOF2s;
			parameters[44].Value = model.ControlCS2s;
			parameters[45].Value = model.ControlHFs;
			parameters[46].Value = model.AlterCount;
            parameters[47].Value = model.PH2S;
            parameters[48].Value = model.MH2S;
            parameters[49].Value = model.PCOS;
            parameters[50].Value = model.MCOS;
            parameters[51].Value = model.PC2F6;
            parameters[52].Value = model.MC2F6;
            parameters[53].Value = model.PC3F8;
            parameters[54].Value = model.MC3F8;
            parameters[55].Value = model.OID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int OID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from OCInstrumentInfo ");
			strSql.Append(" where OID=@OID");
			SqlParameter[] parameters = {
					new SqlParameter("@OID", SqlDbType.Int,4)
			};
			parameters[0].Value = OID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string OIDlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from OCInstrumentInfo ");
			strSql.Append(" where OID in ("+OIDlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public EGMNGS.Model.OCInstrumentInfo GetModel(int OID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 OID,CheckPointCode,CheckPointName,InstrumentCode,InstrumentName,RunStatus,IntallPostion,BelongEquipment,BelongPowerStation,VoltageLevel,Manufacturer,RunDate,CalibrationDate,ControlRunStatus,ControlRunFromDate,ControlRunToDate,Cycle,ControlPressureMin,ControlPressureMax,ControlTempertureMin,ControlTempertureMax,ControlMicroWater,ControlSO2,ControlCO,ControlCF4,ControlSO2F2,ControlSOF2,ControlCS2,ControlHF,CREATED_BY,CREATED_DATE,LAST_UPD_BY,LAST_UPD_DATE,BelongEquipmentName,BelongPowerStationName,ControlPressureMins,ControlPressureMaxs,ControlTempertureMins,ControlTempertureMaxs,ControlMicroWaters,ControlSO2s,ControlCOs,ControlCF4s,ControlSO2F2s,ControlSOF2s,ControlCS2s,ControlHFs,AlterCount,PH2S,MH2S,PCOS,MCOS,PC2F6,MC2F6,PC3F8,MC3F8 from OCInstrumentInfo ");
			strSql.Append(" where OID=@OID");
			SqlParameter[] parameters = {
					new SqlParameter("@OID", SqlDbType.Int,4)
			};
			parameters[0].Value = OID;

			EGMNGS.Model.OCInstrumentInfo model=new EGMNGS.Model.OCInstrumentInfo();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public EGMNGS.Model.OCInstrumentInfo DataRowToModel(DataRow row)
		{
			EGMNGS.Model.OCInstrumentInfo model=new EGMNGS.Model.OCInstrumentInfo();
			if (row != null)
			{
				if(row["OID"]!=null && row["OID"].ToString()!="")
				{
					model.OID=int.Parse(row["OID"].ToString());
				}
				if(row["CheckPointCode"]!=null)
				{
					model.CheckPointCode=row["CheckPointCode"].ToString();
				}
				if(row["CheckPointName"]!=null)
				{
					model.CheckPointName=row["CheckPointName"].ToString();
				}
				if(row["InstrumentCode"]!=null)
				{
					model.InstrumentCode=row["InstrumentCode"].ToString();
				}
				if(row["InstrumentName"]!=null)
				{
					model.InstrumentName=row["InstrumentName"].ToString();
				}
				if(row["RunStatus"]!=null)
				{
					model.RunStatus=row["RunStatus"].ToString();
				}
				if(row["IntallPostion"]!=null)
				{
					model.IntallPostion=row["IntallPostion"].ToString();
				}
				if(row["BelongEquipment"]!=null)
				{
					model.BelongEquipment=row["BelongEquipment"].ToString();
				}
				if(row["BelongPowerStation"]!=null)
				{
					model.BelongPowerStation=row["BelongPowerStation"].ToString();
				}
				if(row["VoltageLevel"]!=null)
				{
					model.VoltageLevel=row["VoltageLevel"].ToString();
				}
				if(row["Manufacturer"]!=null)
				{
					model.Manufacturer=row["Manufacturer"].ToString();
				}
				if(row["RunDate"]!=null && row["RunDate"].ToString()!="")
				{
					model.RunDate=DateTime.Parse(row["RunDate"].ToString());
				}
				if(row["CalibrationDate"]!=null && row["CalibrationDate"].ToString()!="")
				{
					model.CalibrationDate=DateTime.Parse(row["CalibrationDate"].ToString());
				}
				if(row["ControlRunStatus"]!=null)
				{
					model.ControlRunStatus=row["ControlRunStatus"].ToString();
				}
				if(row["ControlRunFromDate"]!=null && row["ControlRunFromDate"].ToString()!="")
				{
					model.ControlRunFromDate=DateTime.Parse(row["ControlRunFromDate"].ToString());
				}
				if(row["ControlRunToDate"]!=null && row["ControlRunToDate"].ToString()!="")
				{
					model.ControlRunToDate=DateTime.Parse(row["ControlRunToDate"].ToString());
				}
				if(row["Cycle"]!=null && row["Cycle"].ToString()!="")
				{
					model.Cycle=int.Parse(row["Cycle"].ToString());
				}
				if(row["ControlPressureMin"]!=null && row["ControlPressureMin"].ToString()!="")
				{
					model.ControlPressureMin=decimal.Parse(row["ControlPressureMin"].ToString());
				}
				if(row["ControlPressureMax"]!=null && row["ControlPressureMax"].ToString()!="")
				{
					model.ControlPressureMax=decimal.Parse(row["ControlPressureMax"].ToString());
				}
				if(row["ControlTempertureMin"]!=null && row["ControlTempertureMin"].ToString()!="")
				{
					model.ControlTempertureMin=decimal.Parse(row["ControlTempertureMin"].ToString());
				}
				if(row["ControlTempertureMax"]!=null && row["ControlTempertureMax"].ToString()!="")
				{
					model.ControlTempertureMax=decimal.Parse(row["ControlTempertureMax"].ToString());
				}
				if(row["ControlMicroWater"]!=null && row["ControlMicroWater"].ToString()!="")
				{
					model.ControlMicroWater=decimal.Parse(row["ControlMicroWater"].ToString());
				}
				if(row["ControlSO2"]!=null && row["ControlSO2"].ToString()!="")
				{
					model.ControlSO2=decimal.Parse(row["ControlSO2"].ToString());
				}
				if(row["ControlCO"]!=null && row["ControlCO"].ToString()!="")
				{
					model.ControlCO=decimal.Parse(row["ControlCO"].ToString());
				}
				if(row["ControlCF4"]!=null && row["ControlCF4"].ToString()!="")
				{
					model.ControlCF4=decimal.Parse(row["ControlCF4"].ToString());
				}
				if(row["ControlSO2F2"]!=null && row["ControlSO2F2"].ToString()!="")
				{
					model.ControlSO2F2=decimal.Parse(row["ControlSO2F2"].ToString());
				}
				if(row["ControlSOF2"]!=null && row["ControlSOF2"].ToString()!="")
				{
					model.ControlSOF2=decimal.Parse(row["ControlSOF2"].ToString());
				}
				if(row["ControlCS2"]!=null && row["ControlCS2"].ToString()!="")
				{
					model.ControlCS2=decimal.Parse(row["ControlCS2"].ToString());
				}
				if(row["ControlHF"]!=null && row["ControlHF"].ToString()!="")
				{
					model.ControlHF=decimal.Parse(row["ControlHF"].ToString());
				}
				if(row["CREATED_BY"]!=null)
				{
					model.CREATED_BY=row["CREATED_BY"].ToString();
				}
				if(row["CREATED_DATE"]!=null && row["CREATED_DATE"].ToString()!="")
				{
					model.CREATED_DATE=DateTime.Parse(row["CREATED_DATE"].ToString());
				}
				if(row["LAST_UPD_BY"]!=null)
				{
					model.LAST_UPD_BY=row["LAST_UPD_BY"].ToString();
				}
				if(row["LAST_UPD_DATE"]!=null && row["LAST_UPD_DATE"].ToString()!="")
				{
					model.LAST_UPD_DATE=DateTime.Parse(row["LAST_UPD_DATE"].ToString());
				}
				if(row["BelongEquipmentName"]!=null)
				{
					model.BelongEquipmentName=row["BelongEquipmentName"].ToString();
				}
				if(row["BelongPowerStationName"]!=null)
				{
					model.BelongPowerStationName=row["BelongPowerStationName"].ToString();
				}
				if(row["ControlPressureMins"]!=null && row["ControlPressureMins"].ToString()!="")
				{
					model.ControlPressureMins=decimal.Parse(row["ControlPressureMins"].ToString());
				}
				if(row["ControlPressureMaxs"]!=null && row["ControlPressureMaxs"].ToString()!="")
				{
					model.ControlPressureMaxs=decimal.Parse(row["ControlPressureMaxs"].ToString());
				}
				if(row["ControlTempertureMins"]!=null && row["ControlTempertureMins"].ToString()!="")
				{
					model.ControlTempertureMins=decimal.Parse(row["ControlTempertureMins"].ToString());
				}
				if(row["ControlTempertureMaxs"]!=null && row["ControlTempertureMaxs"].ToString()!="")
				{
					model.ControlTempertureMaxs=decimal.Parse(row["ControlTempertureMaxs"].ToString());
				}
				if(row["ControlMicroWaters"]!=null && row["ControlMicroWaters"].ToString()!="")
				{
					model.ControlMicroWaters=decimal.Parse(row["ControlMicroWaters"].ToString());
				}
				if(row["ControlSO2s"]!=null && row["ControlSO2s"].ToString()!="")
				{
					model.ControlSO2s=decimal.Parse(row["ControlSO2s"].ToString());
				}
				if(row["ControlCOs"]!=null && row["ControlCOs"].ToString()!="")
				{
					model.ControlCOs=decimal.Parse(row["ControlCOs"].ToString());
				}
				if(row["ControlCF4s"]!=null && row["ControlCF4s"].ToString()!="")
				{
					model.ControlCF4s=decimal.Parse(row["ControlCF4s"].ToString());
				}
				if(row["ControlSO2F2s"]!=null && row["ControlSO2F2s"].ToString()!="")
				{
					model.ControlSO2F2s=decimal.Parse(row["ControlSO2F2s"].ToString());
				}
				if(row["ControlSOF2s"]!=null && row["ControlSOF2s"].ToString()!="")
				{
					model.ControlSOF2s=decimal.Parse(row["ControlSOF2s"].ToString());
				}
				if(row["ControlCS2s"]!=null && row["ControlCS2s"].ToString()!="")
				{
					model.ControlCS2s=decimal.Parse(row["ControlCS2s"].ToString());
				}
				if(row["ControlHFs"]!=null && row["ControlHFs"].ToString()!="")
				{
					model.ControlHFs=decimal.Parse(row["ControlHFs"].ToString());
				}
				if(row["AlterCount"]!=null && row["AlterCount"].ToString()!="")
				{
					model.AlterCount=int.Parse(row["AlterCount"].ToString());
				}
                if (row["PH2S"] != null && row["PH2S"].ToString() != "")
                {
                    model.PH2S = decimal.Parse(row["PH2S"].ToString());
                }
                if (row["MH2S"] != null && row["MH2S"].ToString() != "")
                {
                    model.MH2S = decimal.Parse(row["MH2S"].ToString());
                }
                if (row["PCOS"] != null && row["PCOS"].ToString() != "")
                {
                    model.PCOS = decimal.Parse(row["PCOS"].ToString());
                }
                if (row["MCOS"] != null && row["MCOS"].ToString() != "")
                {
                    model.MCOS = decimal.Parse(row["MCOS"].ToString());
                }
                if (row["PC2F6"] != null && row["PC2F6"].ToString() != "")
                {
                    model.PC2F6 = decimal.Parse(row["PC2F6"].ToString());
                }
                if (row["MC2F6"] != null && row["MC2F6"].ToString() != "")
                {
                    model.MC2F6 = decimal.Parse(row["MC2F6"].ToString());
                }
                if (row["PC3F8"] != null && row["PC3F8"].ToString() != "")
                {
                    model.PC3F8 = decimal.Parse(row["PC3F8"].ToString());
                }
                if (row["MC3F8"] != null && row["MC3F8"].ToString() != "")
                {
                    model.MC3F8 = decimal.Parse(row["MC3F8"].ToString());
                }
            }
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select * ");
			strSql.Append(" FROM OCInstrumentInfo ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" * ");
			strSql.Append(" FROM OCInstrumentInfo ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM OCInstrumentInfo ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby + " desc");
			}
			else
			{
				strSql.Append("order by T.OID desc");
			}
			strSql.Append(")AS Row, T.*  from OCInstrumentInfo T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", (startIndex - 1) * endIndex + 1, startIndex * endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "OCInstrumentInfo";
			parameters[1].Value = "OID";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}


﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Maticsoft.DBUtility;//Please add references
namespace EGMNGS.DAL
{
    /// <summary>
    /// 数据访问类:DetailsApplDetectionGas
    /// </summary>
    public partial class DetailsApplDetectionGas
    {
        public DetailsApplDetectionGas()
        { }
        #region  Method

        /// <summary>
        /// 得到最大ID
        /// </summary>
        public int GetMaxId()
        {
            return DbHelperSQL.GetMaxID("OID", "DetailsApplDetectionGas");
        }

        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int OID)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) from DetailsApplDetectionGas");
            strSql.Append(" where OID=@OID");
            SqlParameter[] parameters = {
					new SqlParameter("@OID", SqlDbType.Int,4)
			};
            parameters[0].Value = OID;

            return DbHelperSQL.Exists(strSql.ToString(), parameters);
        }


        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(EGMNGS.Model.DetailsApplDetectionGas model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into DetailsApplDetectionGas(");
            strSql.Append("ApplCode,CylinderCode,GasCode,AmountGas,IsPass,CheckDate,Status,ProduceNum,Manufacturer)");
            strSql.Append(" values (");
            strSql.Append("@ApplCode,@CylinderCode,@GasCode,@AmountGas,@IsPass,@CheckDate,@Status,@ProduceNum,@Manufacturer)");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
					new SqlParameter("@ApplCode", SqlDbType.VarChar,20),
					new SqlParameter("@CylinderCode", SqlDbType.VarChar,20),
					new SqlParameter("@GasCode", SqlDbType.VarChar,20),
					new SqlParameter("@AmountGas", SqlDbType.Decimal,9),
					new SqlParameter("@IsPass", SqlDbType.VarChar,4),
					new SqlParameter("@CheckDate", SqlDbType.Date,3),
					new SqlParameter("@Status", SqlDbType.VarChar,4),
                    new SqlParameter("@ProduceNum", SqlDbType.VarChar,50),
                    new SqlParameter("@Manufacturer", SqlDbType.NVarChar,200)
                                        };
            parameters[0].Value = model.ApplCode;
            parameters[1].Value = model.CylinderCode;
            parameters[2].Value = model.GasCode;
            parameters[3].Value = model.AmountGas;
            parameters[4].Value = model.IsPass;
            parameters[5].Value = model.CheckDate;
            parameters[6].Value = model.Status;
            parameters[7].Value = model.ProduceNum;
            parameters[8].Value = model.Manufacturer;

            object obj = DbHelperSQL.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {
                // EGMNGS.Common.ComServies.UpdCodeNum(model.ApplCode);
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(EGMNGS.Model.DetailsApplDetectionGas model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update DetailsApplDetectionGas set ");
            strSql.Append("ApplCode=@ApplCode,");
            strSql.Append("CylinderCode=@CylinderCode,");
            strSql.Append("GasCode=@GasCode,");
            strSql.Append("AmountGas=@AmountGas,");
            strSql.Append("IsPass=@IsPass,");
            strSql.Append("CheckDate=@CheckDate,");
            strSql.Append("Status=@Status,");
            strSql.Append("ProduceNum=@ProduceNum,");
            strSql.Append("Manufacturer=@Manufacturer");
            strSql.Append(" where OID=@OID");
            SqlParameter[] parameters = {
					new SqlParameter("@ApplCode", SqlDbType.VarChar,20),
					new SqlParameter("@CylinderCode", SqlDbType.VarChar,20),
					new SqlParameter("@GasCode", SqlDbType.VarChar,20),
					new SqlParameter("@AmountGas", SqlDbType.Decimal,9),
					new SqlParameter("@IsPass", SqlDbType.VarChar,4),
					new SqlParameter("@CheckDate", SqlDbType.Date,3),
					new SqlParameter("@Status", SqlDbType.VarChar,4),
					new SqlParameter("@OID", SqlDbType.Int,4),
                    new SqlParameter("@ProduceNum", SqlDbType.VarChar,50),
                    new SqlParameter("@Manufacturer", SqlDbType.NVarChar,200)};
            parameters[0].Value = model.ApplCode;
            parameters[1].Value = model.CylinderCode;
            parameters[2].Value = model.GasCode;
            parameters[3].Value = model.AmountGas;
            parameters[4].Value = model.IsPass;
            parameters[5].Value = model.CheckDate;
            parameters[6].Value = model.Status;
            parameters[7].Value = model.OID;
            parameters[8].Value = model.ProduceNum;
            parameters[9].Value = model.Manufacturer;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int OID)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from DetailsApplDetectionGas ");
            strSql.Append(" where OID=@OID");
            SqlParameter[] parameters = {
					new SqlParameter("@OID", SqlDbType.Int,4)
			};
            parameters[0].Value = OID;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string OIDlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from DetailsApplDetectionGas ");
            strSql.Append(" where OID in (" + OIDlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public EGMNGS.Model.DetailsApplDetectionGas GetModel(int OID)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 OID,ApplCode,CylinderCode,GasCode,AmountGas,IsPass,CheckDate,Status,ProduceNum,Manufacturer from DetailsApplDetectionGas ");
            strSql.Append(" where OID=@OID");
            SqlParameter[] parameters = {
					new SqlParameter("@OID", SqlDbType.Int,4)
			};
            parameters[0].Value = OID;

            EGMNGS.Model.DetailsApplDetectionGas model = new EGMNGS.Model.DetailsApplDetectionGas();
            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0]["OID"] != null && ds.Tables[0].Rows[0]["OID"].ToString() != "")
                {
                    model.OID = int.Parse(ds.Tables[0].Rows[0]["OID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["ApplCode"] != null && ds.Tables[0].Rows[0]["ApplCode"].ToString() != "")
                {
                    model.ApplCode = ds.Tables[0].Rows[0]["ApplCode"].ToString();
                }
                if (ds.Tables[0].Rows[0]["CylinderCode"] != null && ds.Tables[0].Rows[0]["CylinderCode"].ToString() != "")
                {
                    model.CylinderCode = ds.Tables[0].Rows[0]["CylinderCode"].ToString();
                }
                if (ds.Tables[0].Rows[0]["GasCode"] != null && ds.Tables[0].Rows[0]["GasCode"].ToString() != "")
                {
                    model.GasCode = ds.Tables[0].Rows[0]["GasCode"].ToString();
                }
                if (ds.Tables[0].Rows[0]["AmountGas"] != null && ds.Tables[0].Rows[0]["AmountGas"].ToString() != "")
                {
                    model.AmountGas = decimal.Parse(ds.Tables[0].Rows[0]["AmountGas"].ToString());
                }
                if (ds.Tables[0].Rows[0]["IsPass"] != null && ds.Tables[0].Rows[0]["IsPass"].ToString() != "")
                {
                    model.IsPass = ds.Tables[0].Rows[0]["IsPass"].ToString();
                }
                if (ds.Tables[0].Rows[0]["CheckDate"] != null && ds.Tables[0].Rows[0]["CheckDate"].ToString() != "")
                {
                    model.CheckDate = DateTime.Parse(ds.Tables[0].Rows[0]["CheckDate"].ToString());
                }
                if (ds.Tables[0].Rows[0]["Status"] != null && ds.Tables[0].Rows[0]["Status"].ToString() != "")
                {
                    model.Status = ds.Tables[0].Rows[0]["Status"].ToString();
                }
                if (ds.Tables[0].Rows[0]["ProduceNum"] != null && ds.Tables[0].Rows[0]["ProduceNum"].ToString() != "")
                {
                    model.ProduceNum = ds.Tables[0].Rows[0]["ProduceNum"].ToString();
                }
                if (ds.Tables[0].Rows[0]["Manufacturer"] != null && ds.Tables[0].Rows[0]["Manufacturer"].ToString() != "")
                {
                    model.Manufacturer = ds.Tables[0].Rows[0]["Manufacturer"].ToString();
                }
                return model;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select OID,ApplCode,CylinderCode,GasCode,AmountGas,IsPass,CheckDate,Status,ProduceNum,Manufacturer ");
            strSql.Append(" FROM DetailsApplDetectionGas ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" OID,ApplCode,CylinderCode,GasCode,AmountGas,IsPass,CheckDate,Status,ProduceNum,Manufacturer ");
            strSql.Append(" FROM DetailsApplDetectionGas ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM DetailsApplDetectionGas ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.OID desc");
            }
            strSql.Append(")AS Row, T.*  from DetailsApplDetectionGas T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /*
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        {
            SqlParameter[] parameters = {
                    new SqlParameter("@tblName", SqlDbType.VarChar, 255),
                    new SqlParameter("@fldName", SqlDbType.VarChar, 255),
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@PageIndex", SqlDbType.Int),
                    new SqlParameter("@IsReCount", SqlDbType.Bit),
                    new SqlParameter("@OrderType", SqlDbType.Bit),
                    new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
                    };
            parameters[0].Value = "DetailsApplDetectionGas";
            parameters[1].Value = "OID";
            parameters[2].Value = PageSize;
            parameters[3].Value = PageIndex;
            parameters[4].Value = 0;
            parameters[5].Value = 0;
            parameters[6].Value = strWhere;	
            return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
        }*/

        #endregion  Method
    }
}


﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Maticsoft.DBUtility;//Please add references
namespace EGMNGS.DAL
{
    /// <summary>
    /// 数据访问类:RegGasBatch
    /// </summary>
    public partial class RegGasBatch
    {
        public RegGasBatch()
        { }
        #region  Method

        /// <summary>
        /// 得到最大ID
        /// </summary>
        public int GetMaxId()
        {
            return DbHelperSQL.GetMaxID("OID", "RegGasBatch");
        }

        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int OID)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) from RegGasBatch");
            strSql.Append(" where OID=@OID");
            SqlParameter[] parameters = {
					new SqlParameter("@OID", SqlDbType.Int,4)
			};
            parameters[0].Value = OID;

            return DbHelperSQL.Exists(strSql.ToString(), parameters);
        }


        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(EGMNGS.Model.RegGasBatch model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into RegGasBatch(");
            strSql.Append("BatchCode,Year,Month,ProcessEquipment,AmountGas,SFContent,AirContent,SFHT,Wet,Acidity,KSJFHW,KWY,Status,RegistantsOID,RegistantsDate,CREATED_BY,CREATED_DATE,LAST_UPD_BY,LAST_UPD_DATE,IsPass)");
            strSql.Append(" values (");
            strSql.Append("@BatchCode,@Year,@Month,@ProcessEquipment,@AmountGas,@SFContent,@AirContent,@SFHT,@Wet,@Acidity,@KSJFHW,@KWY,@Status,@RegistantsOID,@RegistantsDate,@CREATED_BY,@CREATED_DATE,@LAST_UPD_BY,@LAST_UPD_DATE,@IsPass)");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
					new SqlParameter("@BatchCode", SqlDbType.VarChar,20),
					new SqlParameter("@Year", SqlDbType.VarChar,4),
					new SqlParameter("@Month", SqlDbType.VarChar,2),
					new SqlParameter("@ProcessEquipment", SqlDbType.VarChar,4),
					new SqlParameter("@AmountGas", SqlDbType.Decimal,9),
					new SqlParameter("@SFContent", SqlDbType.Decimal,9),
					new SqlParameter("@AirContent", SqlDbType.Decimal,9),
					new SqlParameter("@SFHT", SqlDbType.Decimal,9),
					new SqlParameter("@Wet", SqlDbType.Decimal,9),
					new SqlParameter("@Acidity", SqlDbType.Decimal,9),
					new SqlParameter("@KSJFHW", SqlDbType.Decimal,9),
					new SqlParameter("@KWY", SqlDbType.Decimal,9),
					new SqlParameter("@Status", SqlDbType.VarChar,4),
					new SqlParameter("@RegistantsOID", SqlDbType.VarChar,50),
					new SqlParameter("@RegistantsDate", SqlDbType.Date,3),
					new SqlParameter("@CREATED_BY", SqlDbType.NVarChar,12),
					new SqlParameter("@CREATED_DATE", SqlDbType.Date,3),
					new SqlParameter("@LAST_UPD_BY", SqlDbType.NVarChar,12),
					new SqlParameter("@LAST_UPD_DATE", SqlDbType.Date,3),
                    new SqlParameter("@IsPass", SqlDbType.VarChar,4)};
            parameters[0].Value = model.BatchCode;
            parameters[1].Value = model.Year;
            parameters[2].Value = model.Month;
            parameters[3].Value = model.ProcessEquipment;
            parameters[4].Value = model.AmountGas;
            parameters[5].Value = model.SFContent;
            parameters[6].Value = model.AirContent;
            parameters[7].Value = model.SFHT;
            parameters[8].Value = model.Wet;
            parameters[9].Value = model.Acidity;
            parameters[10].Value = model.KSJFHW;
            parameters[11].Value = model.KWY;
            parameters[12].Value = model.Status;
            parameters[13].Value = model.RegistantsOID;
            parameters[14].Value = model.RegistantsDate;
            parameters[15].Value = model.CREATED_BY;
            parameters[16].Value = model.CREATED_DATE;
            parameters[17].Value = model.LAST_UPD_BY;
            parameters[18].Value = model.LAST_UPD_DATE;
            parameters[19].Value = model.IsPass;
            object obj = DbHelperSQL.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {
                EGMNGS.Common.ComServies.UpdCodeNum(model.BatchCode);
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(EGMNGS.Model.RegGasBatch model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update RegGasBatch set ");
            strSql.Append("BatchCode=@BatchCode,");
            strSql.Append("Year=@Year,");
            strSql.Append("Month=@Month,");
            strSql.Append("ProcessEquipment=@ProcessEquipment,");
            strSql.Append("AmountGas=@AmountGas,");
            strSql.Append("SFContent=@SFContent,");
            strSql.Append("AirContent=@AirContent,");
            strSql.Append("SFHT=@SFHT,");
            strSql.Append("Wet=@Wet,");
            strSql.Append("Acidity=@Acidity,");
            strSql.Append("KSJFHW=@KSJFHW,");
            strSql.Append("KWY=@KWY,");
            strSql.Append("Status=@Status,");
            strSql.Append("RegistantsOID=@RegistantsOID,");
            strSql.Append("RegistantsDate=@RegistantsDate,");
            strSql.Append("CREATED_BY=@CREATED_BY,");
            strSql.Append("CREATED_DATE=@CREATED_DATE,");
            strSql.Append("LAST_UPD_BY=@LAST_UPD_BY,");
            strSql.Append("LAST_UPD_DATE=@LAST_UPD_DATE,");
            strSql.Append("IsPass=@IsPass");
            strSql.Append(" where OID=@OID");
            SqlParameter[] parameters = {
					new SqlParameter("@BatchCode", SqlDbType.VarChar,20),
					new SqlParameter("@Year", SqlDbType.VarChar,4),
					new SqlParameter("@Month", SqlDbType.VarChar,2),
					new SqlParameter("@ProcessEquipment", SqlDbType.VarChar,4),
					new SqlParameter("@AmountGas", SqlDbType.Decimal,9),
					new SqlParameter("@SFContent", SqlDbType.Decimal,9),
					new SqlParameter("@AirContent", SqlDbType.Decimal,9),
					new SqlParameter("@SFHT", SqlDbType.Decimal,9),
					new SqlParameter("@Wet", SqlDbType.Decimal,9),
					new SqlParameter("@Acidity", SqlDbType.Decimal,9),
					new SqlParameter("@KSJFHW", SqlDbType.Decimal,9),
					new SqlParameter("@KWY", SqlDbType.Decimal,9),
					new SqlParameter("@Status", SqlDbType.VarChar,4),
					new SqlParameter("@RegistantsOID", SqlDbType.VarChar,50),
					new SqlParameter("@RegistantsDate", SqlDbType.Date,3),
					new SqlParameter("@CREATED_BY", SqlDbType.NVarChar,12),
					new SqlParameter("@CREATED_DATE", SqlDbType.Date,3),
					new SqlParameter("@LAST_UPD_BY", SqlDbType.NVarChar,12),
					new SqlParameter("@LAST_UPD_DATE", SqlDbType.Date,3),
					new SqlParameter("@OID", SqlDbType.Int,4),
                    new SqlParameter("@IsPass", SqlDbType.VarChar,4)};
            parameters[0].Value = model.BatchCode;
            parameters[1].Value = model.Year;
            parameters[2].Value = model.Month;
            parameters[3].Value = model.ProcessEquipment;
            parameters[4].Value = model.AmountGas;
            parameters[5].Value = model.SFContent;
            parameters[6].Value = model.AirContent;
            parameters[7].Value = model.SFHT;
            parameters[8].Value = model.Wet;
            parameters[9].Value = model.Acidity;
            parameters[10].Value = model.KSJFHW;
            parameters[11].Value = model.KWY;
            parameters[12].Value = model.Status;
            parameters[13].Value = model.RegistantsOID;
            parameters[14].Value = model.RegistantsDate;
            parameters[15].Value = model.CREATED_BY;
            parameters[16].Value = model.CREATED_DATE;
            parameters[17].Value = model.LAST_UPD_BY;
            parameters[18].Value = model.LAST_UPD_DATE;
            parameters[19].Value = model.OID;
            parameters[20].Value = model.IsPass;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int OID)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from RegGasBatch ");
            strSql.Append(" where OID=@OID");
            SqlParameter[] parameters = {
					new SqlParameter("@OID", SqlDbType.Int,4)
			};
            parameters[0].Value = OID;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string OIDlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from RegGasBatch ");
            strSql.Append(" where OID in (" + OIDlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public EGMNGS.Model.RegGasBatch GetModel(int OID)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 OID,BatchCode,Year,Month,ProcessEquipment,AmountGas,SFContent,AirContent,SFHT,Wet,Acidity,KSJFHW,KWY,Status,RegistantsOID,RegistantsDate,CREATED_BY,CREATED_DATE,LAST_UPD_BY,LAST_UPD_DATE,IsPass from RegGasBatch ");
            strSql.Append(" where OID=@OID");
            SqlParameter[] parameters = {
					new SqlParameter("@OID", SqlDbType.Int,4)
			};
            parameters[0].Value = OID;

            EGMNGS.Model.RegGasBatch model = new EGMNGS.Model.RegGasBatch();
            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0]["OID"] != null && ds.Tables[0].Rows[0]["OID"].ToString() != "")
                {
                    model.OID = int.Parse(ds.Tables[0].Rows[0]["OID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["BatchCode"] != null && ds.Tables[0].Rows[0]["BatchCode"].ToString() != "")
                {
                    model.BatchCode = ds.Tables[0].Rows[0]["BatchCode"].ToString();
                }
                if (ds.Tables[0].Rows[0]["Year"] != null && ds.Tables[0].Rows[0]["Year"].ToString() != "")
                {
                    model.Year = ds.Tables[0].Rows[0]["Year"].ToString();
                }
                if (ds.Tables[0].Rows[0]["Month"] != null && ds.Tables[0].Rows[0]["Month"].ToString() != "")
                {
                    model.Month = ds.Tables[0].Rows[0]["Month"].ToString();
                }
                if (ds.Tables[0].Rows[0]["ProcessEquipment"] != null && ds.Tables[0].Rows[0]["ProcessEquipment"].ToString() != "")
                {
                    model.ProcessEquipment = ds.Tables[0].Rows[0]["ProcessEquipment"].ToString();
                }
                if (ds.Tables[0].Rows[0]["AmountGas"] != null && ds.Tables[0].Rows[0]["AmountGas"].ToString() != "")
                {
                    model.AmountGas = decimal.Parse(ds.Tables[0].Rows[0]["AmountGas"].ToString());
                }
                if (ds.Tables[0].Rows[0]["SFContent"] != null && ds.Tables[0].Rows[0]["SFContent"].ToString() != "")
                {
                    model.SFContent = decimal.Parse(ds.Tables[0].Rows[0]["SFContent"].ToString());
                }
                if (ds.Tables[0].Rows[0]["AirContent"] != null && ds.Tables[0].Rows[0]["AirContent"].ToString() != "")
                {
                    model.AirContent = decimal.Parse(ds.Tables[0].Rows[0]["AirContent"].ToString());
                }
                if (ds.Tables[0].Rows[0]["SFHT"] != null && ds.Tables[0].Rows[0]["SFHT"].ToString() != "")
                {
                    model.SFHT = decimal.Parse(ds.Tables[0].Rows[0]["SFHT"].ToString());
                }
                if (ds.Tables[0].Rows[0]["Wet"] != null && ds.Tables[0].Rows[0]["Wet"].ToString() != "")
                {
                    model.Wet = decimal.Parse(ds.Tables[0].Rows[0]["Wet"].ToString());
                }
                if (ds.Tables[0].Rows[0]["Acidity"] != null && ds.Tables[0].Rows[0]["Acidity"].ToString() != "")
                {
                    model.Acidity = decimal.Parse(ds.Tables[0].Rows[0]["Acidity"].ToString());
                }
                if (ds.Tables[0].Rows[0]["KSJFHW"] != null && ds.Tables[0].Rows[0]["KSJFHW"].ToString() != "")
                {
                    model.KSJFHW = decimal.Parse(ds.Tables[0].Rows[0]["KSJFHW"].ToString());
                }
                if (ds.Tables[0].Rows[0]["KWY"] != null && ds.Tables[0].Rows[0]["KWY"].ToString() != "")
                {
                    model.KWY = decimal.Parse(ds.Tables[0].Rows[0]["KWY"].ToString());
                }
                if (ds.Tables[0].Rows[0]["Status"] != null && ds.Tables[0].Rows[0]["Status"].ToString() != "")
                {
                    model.Status = ds.Tables[0].Rows[0]["Status"].ToString();
                }
                if (ds.Tables[0].Rows[0]["RegistantsOID"] != null && ds.Tables[0].Rows[0]["RegistantsOID"].ToString() != "")
                {
                    model.RegistantsOID = ds.Tables[0].Rows[0]["RegistantsOID"].ToString();
                }
                if (ds.Tables[0].Rows[0]["RegistantsDate"] != null && ds.Tables[0].Rows[0]["RegistantsDate"].ToString() != "")
                {
                    model.RegistantsDate = DateTime.Parse(ds.Tables[0].Rows[0]["RegistantsDate"].ToString());
                }
                if (ds.Tables[0].Rows[0]["CREATED_BY"] != null && ds.Tables[0].Rows[0]["CREATED_BY"].ToString() != "")
                {
                    model.CREATED_BY = ds.Tables[0].Rows[0]["CREATED_BY"].ToString();
                }
                if (ds.Tables[0].Rows[0]["CREATED_DATE"] != null && ds.Tables[0].Rows[0]["CREATED_DATE"].ToString() != "")
                {
                    model.CREATED_DATE = DateTime.Parse(ds.Tables[0].Rows[0]["CREATED_DATE"].ToString());
                }
                if (ds.Tables[0].Rows[0]["LAST_UPD_BY"] != null && ds.Tables[0].Rows[0]["LAST_UPD_BY"].ToString() != "")
                {
                    model.LAST_UPD_BY = ds.Tables[0].Rows[0]["LAST_UPD_BY"].ToString();
                }
                if (ds.Tables[0].Rows[0]["LAST_UPD_DATE"] != null && ds.Tables[0].Rows[0]["LAST_UPD_DATE"].ToString() != "")
                {
                    model.LAST_UPD_DATE = DateTime.Parse(ds.Tables[0].Rows[0]["LAST_UPD_DATE"].ToString());
                }
                if (ds.Tables[0].Rows[0]["IsPass"] != null && ds.Tables[0].Rows[0]["IsPass"].ToString() != "")
                {
                    model.IsPass = ds.Tables[0].Rows[0]["IsPass"].ToString();
                }
                return model;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select OID,BatchCode,Year,Month,ProcessEquipment,AmountGas,SFContent,AirContent,SFHT,Wet,Acidity,KSJFHW,KWY,Status,RegistantsOID,RegistantsDate,CREATED_BY,CREATED_DATE,LAST_UPD_BY,LAST_UPD_DATE,IsPass ");
            strSql.Append(" FROM RegGasBatch ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" OID,BatchCode,Year,Month,ProcessEquipment,AmountGas,SFContent,AirContent,SFHT,Wet,Acidity,KSJFHW,KWY,Status,RegistantsOID,RegistantsDate,CREATED_BY,CREATED_DATE,LAST_UPD_BY,LAST_UPD_DATE,IsPass ");
            strSql.Append(" FROM RegGasBatch ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM RegGasBatch ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.OID desc");
            }
            strSql.Append(")AS Row, T.*  from RegGasBatch T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", (startIndex - 1) * endIndex + 1, startIndex * endIndex);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /*
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        {
            SqlParameter[] parameters = {
                    new SqlParameter("@tblName", SqlDbType.VarChar, 255),
                    new SqlParameter("@fldName", SqlDbType.VarChar, 255),
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@PageIndex", SqlDbType.Int),
                    new SqlParameter("@IsReCount", SqlDbType.Bit),
                    new SqlParameter("@OrderType", SqlDbType.Bit),
                    new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
                    };
            parameters[0].Value = "RegGasBatch";
            parameters[1].Value = "OID";
            parameters[2].Value = PageSize;
            parameters[3].Value = PageIndex;
            parameters[4].Value = 0;
            parameters[5].Value = 0;
            parameters[6].Value = strWhere;	
            return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
        }*/

        #endregion  Method
    }
}


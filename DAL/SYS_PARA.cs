﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Maticsoft.DBUtility;//Please add references
namespace EGMNGS.DAL
{
	/// <summary>
	/// 数据访问类:SYS_PARA
	/// </summary>
	public partial class SYS_PARA
	{
		public SYS_PARA()
		{}
		#region  Method

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(string PARA_CODE)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from SYS_PARA");
			strSql.Append(" where PARA_CODE=@PARA_CODE ");
			SqlParameter[] parameters = {
					new SqlParameter("@PARA_CODE", SqlDbType.VarChar,20)			};
			parameters[0].Value = PARA_CODE;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(EGMNGS.Model.SYS_PARA model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into SYS_PARA(");
			strSql.Append("PARA_CODE,PARA_TYPE,PARA_DESC,PARA_VALUE,CREATED_BY,CREATED_DATE,LAST_UPD_BY,LAST_UPD_DATE)");
			strSql.Append(" values (");
			strSql.Append("@PARA_CODE,@PARA_TYPE,@PARA_DESC,@PARA_VALUE,@CREATED_BY,@CREATED_DATE,@LAST_UPD_BY,@LAST_UPD_DATE)");
			SqlParameter[] parameters = {
					new SqlParameter("@PARA_CODE", SqlDbType.VarChar,20),
					new SqlParameter("@PARA_TYPE", SqlDbType.VarChar,50),
					new SqlParameter("@PARA_DESC", SqlDbType.VarChar,200),
					new SqlParameter("@PARA_VALUE", SqlDbType.VarChar,10),
					new SqlParameter("@CREATED_BY", SqlDbType.VarChar,12),
					new SqlParameter("@CREATED_DATE", SqlDbType.DateTime),
					new SqlParameter("@LAST_UPD_BY", SqlDbType.VarChar,12),
					new SqlParameter("@LAST_UPD_DATE", SqlDbType.DateTime)};
			parameters[0].Value = model.PARA_CODE;
			parameters[1].Value = model.PARA_TYPE;
			parameters[2].Value = model.PARA_DESC;
			parameters[3].Value = model.PARA_VALUE;
			parameters[4].Value = model.CREATED_BY;
			parameters[5].Value = model.CREATED_DATE;
			parameters[6].Value = model.LAST_UPD_BY;
			parameters[7].Value = model.LAST_UPD_DATE;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(EGMNGS.Model.SYS_PARA model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update SYS_PARA set ");
			strSql.Append("PARA_TYPE=@PARA_TYPE,");
			strSql.Append("PARA_DESC=@PARA_DESC,");
			strSql.Append("PARA_VALUE=@PARA_VALUE,");
			strSql.Append("CREATED_BY=@CREATED_BY,");
			strSql.Append("CREATED_DATE=@CREATED_DATE,");
			strSql.Append("LAST_UPD_BY=@LAST_UPD_BY,");
			strSql.Append("LAST_UPD_DATE=@LAST_UPD_DATE");
			strSql.Append(" where PARA_CODE=@PARA_CODE ");
			SqlParameter[] parameters = {
					new SqlParameter("@PARA_TYPE", SqlDbType.VarChar,50),
					new SqlParameter("@PARA_DESC", SqlDbType.VarChar,200),
					new SqlParameter("@PARA_VALUE", SqlDbType.VarChar,10),
					new SqlParameter("@CREATED_BY", SqlDbType.VarChar,12),
					new SqlParameter("@CREATED_DATE", SqlDbType.DateTime),
					new SqlParameter("@LAST_UPD_BY", SqlDbType.VarChar,12),
					new SqlParameter("@LAST_UPD_DATE", SqlDbType.DateTime),
					new SqlParameter("@PARA_CODE", SqlDbType.VarChar,20)};
			parameters[0].Value = model.PARA_TYPE;
			parameters[1].Value = model.PARA_DESC;
			parameters[2].Value = model.PARA_VALUE;
			parameters[3].Value = model.CREATED_BY;
			parameters[4].Value = model.CREATED_DATE;
			parameters[5].Value = model.LAST_UPD_BY;
			parameters[6].Value = model.LAST_UPD_DATE;
			parameters[7].Value = model.PARA_CODE;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(string PARA_CODE)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from SYS_PARA ");
			strSql.Append(" where PARA_CODE=@PARA_CODE ");
			SqlParameter[] parameters = {
					new SqlParameter("@PARA_CODE", SqlDbType.VarChar,20)			};
			parameters[0].Value = PARA_CODE;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string PARA_CODElist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from SYS_PARA ");
			strSql.Append(" where PARA_CODE in ("+PARA_CODElist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public EGMNGS.Model.SYS_PARA GetModel(string PARA_CODE)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 PARA_CODE,PARA_TYPE,PARA_DESC,PARA_VALUE,CREATED_BY,CREATED_DATE,LAST_UPD_BY,LAST_UPD_DATE from SYS_PARA ");
			strSql.Append(" where PARA_CODE=@PARA_CODE ");
			SqlParameter[] parameters = {
					new SqlParameter("@PARA_CODE", SqlDbType.VarChar,20)			};
			parameters[0].Value = PARA_CODE;

			EGMNGS.Model.SYS_PARA model=new EGMNGS.Model.SYS_PARA();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				if(ds.Tables[0].Rows[0]["PARA_CODE"]!=null && ds.Tables[0].Rows[0]["PARA_CODE"].ToString()!="")
				{
					model.PARA_CODE=ds.Tables[0].Rows[0]["PARA_CODE"].ToString();
				}
				if(ds.Tables[0].Rows[0]["PARA_TYPE"]!=null && ds.Tables[0].Rows[0]["PARA_TYPE"].ToString()!="")
				{
					model.PARA_TYPE=ds.Tables[0].Rows[0]["PARA_TYPE"].ToString();
				}
				if(ds.Tables[0].Rows[0]["PARA_DESC"]!=null && ds.Tables[0].Rows[0]["PARA_DESC"].ToString()!="")
				{
					model.PARA_DESC=ds.Tables[0].Rows[0]["PARA_DESC"].ToString();
				}
				if(ds.Tables[0].Rows[0]["PARA_VALUE"]!=null && ds.Tables[0].Rows[0]["PARA_VALUE"].ToString()!="")
				{
					model.PARA_VALUE=ds.Tables[0].Rows[0]["PARA_VALUE"].ToString();
				}
				if(ds.Tables[0].Rows[0]["CREATED_BY"]!=null && ds.Tables[0].Rows[0]["CREATED_BY"].ToString()!="")
				{
					model.CREATED_BY=ds.Tables[0].Rows[0]["CREATED_BY"].ToString();
				}
				if(ds.Tables[0].Rows[0]["CREATED_DATE"]!=null && ds.Tables[0].Rows[0]["CREATED_DATE"].ToString()!="")
				{
					model.CREATED_DATE=DateTime.Parse(ds.Tables[0].Rows[0]["CREATED_DATE"].ToString());
				}
				if(ds.Tables[0].Rows[0]["LAST_UPD_BY"]!=null && ds.Tables[0].Rows[0]["LAST_UPD_BY"].ToString()!="")
				{
					model.LAST_UPD_BY=ds.Tables[0].Rows[0]["LAST_UPD_BY"].ToString();
				}
				if(ds.Tables[0].Rows[0]["LAST_UPD_DATE"]!=null && ds.Tables[0].Rows[0]["LAST_UPD_DATE"].ToString()!="")
				{
					model.LAST_UPD_DATE=DateTime.Parse(ds.Tables[0].Rows[0]["LAST_UPD_DATE"].ToString());
				}
				return model;
			}
			else
			{
				return null;
			}
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select PARA_CODE,PARA_TYPE,PARA_DESC,PARA_VALUE,CREATED_BY,CREATED_DATE,LAST_UPD_BY,LAST_UPD_DATE ");
			strSql.Append(" FROM SYS_PARA ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" PARA_CODE,PARA_TYPE,PARA_DESC,PARA_VALUE,CREATED_BY,CREATED_DATE,LAST_UPD_BY,LAST_UPD_DATE ");
			strSql.Append(" FROM SYS_PARA ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM SYS_PARA ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.PARA_CODE desc");
			}
			strSql.Append(")AS Row, T.*  from SYS_PARA T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", (startIndex - 1) * endIndex + 1, startIndex * endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "SYS_PARA";
			parameters[1].Value = "PARA_CODE";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  Method
	}
}


﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Maticsoft.DBUtility;//Please add references
namespace EGMNGS.DAL
{
	/// <summary>
	/// 数据访问类:OCInstrumentInfoOnline
	/// </summary>
	public partial class OCInstrumentInfoOnline
	{
		public OCInstrumentInfoOnline()
		{}
		#region  BasicMethod

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
		return DbHelperSQL.GetMaxID("OID", "OCInstrumentInfoOnline"); 
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int OID)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from OCInstrumentInfoOnline");
			strSql.Append(" where OID=@OID");
			SqlParameter[] parameters = {
					new SqlParameter("@OID", SqlDbType.Int,4)
			};
			parameters[0].Value = OID;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(EGMNGS.Model.OCInstrumentInfoOnline model)
		{
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into OCInstrumentInfoOnline(");
            strSql.Append("CheckPointCode,CheckPointName,Pressure,Temperture,MicroWater,SO2,CO,CF4,SO2F2,SOF2,CS2,HF,CollectionType,CollectionDatetime,CollectionBy,H2S,COSS,C2F6,C3F8)");
            strSql.Append(" values (");
            strSql.Append("@CheckPointCode,@CheckPointName,@Pressure,@Temperture,@MicroWater,@SO2,@CO,@CF4,@SO2F2,@SOF2,@CS2,@HF,@CollectionType,@CollectionDatetime,@CollectionBy,@H2S,@COSS,@C2F6,@C3F8)");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
                    new SqlParameter("@CheckPointCode", SqlDbType.VarChar,4),
                    new SqlParameter("@CheckPointName", SqlDbType.NVarChar,50),
                    new SqlParameter("@Pressure", SqlDbType.Decimal,9),
                    new SqlParameter("@Temperture", SqlDbType.Decimal,9),
                    new SqlParameter("@MicroWater", SqlDbType.Decimal,9),
                    new SqlParameter("@SO2", SqlDbType.Decimal,9),
                    new SqlParameter("@CO", SqlDbType.Decimal,9),
                    new SqlParameter("@CF4", SqlDbType.Decimal,9),
                    new SqlParameter("@SO2F2", SqlDbType.Decimal,9),
                    new SqlParameter("@SOF2", SqlDbType.Decimal,9),
                    new SqlParameter("@CS2", SqlDbType.Decimal,9),
                    new SqlParameter("@HF", SqlDbType.Decimal,9),
                    new SqlParameter("@CollectionType", SqlDbType.VarChar,4),
                    new SqlParameter("@CollectionDatetime", SqlDbType.DateTime),
                    new SqlParameter("@CollectionBy", SqlDbType.NVarChar,50),
                    new SqlParameter("@H2S", SqlDbType.Decimal,9),
                    new SqlParameter("@COSS", SqlDbType.Decimal,9),
                    new SqlParameter("@C2F6", SqlDbType.Decimal,9),
                    new SqlParameter("@C3F8", SqlDbType.Decimal,9)};
            parameters[0].Value = model.CheckPointCode;
            parameters[1].Value = model.CheckPointName;
            parameters[2].Value = model.Pressure;
            parameters[3].Value = model.Temperture;
            parameters[4].Value = model.MicroWater;
            parameters[5].Value = model.SO2;
            parameters[6].Value = model.CO;
            parameters[7].Value = model.CF4;
            parameters[8].Value = model.SO2F2;
            parameters[9].Value = model.SOF2;
            parameters[10].Value = model.CS2;
            parameters[11].Value = model.HF;
            parameters[12].Value = model.CollectionType;
            parameters[13].Value = model.CollectionDatetime;
            parameters[14].Value = model.CollectionBy;
            parameters[15].Value = model.H2S;
            parameters[16].Value = model.COSS;
            parameters[17].Value = model.C2F6;
            parameters[18].Value = model.C3F8;

            object obj = DbHelperSQL.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(EGMNGS.Model.OCInstrumentInfoOnline model)
		{
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update OCInstrumentInfoOnline set ");
            strSql.Append("CheckPointCode=@CheckPointCode,");
            strSql.Append("CheckPointName=@CheckPointName,");
            strSql.Append("Pressure=@Pressure,");
            strSql.Append("Temperture=@Temperture,");
            strSql.Append("MicroWater=@MicroWater,");
            strSql.Append("SO2=@SO2,");
            strSql.Append("CO=@CO,");
            strSql.Append("CF4=@CF4,");
            strSql.Append("SO2F2=@SO2F2,");
            strSql.Append("SOF2=@SOF2,");
            strSql.Append("CS2=@CS2,");
            strSql.Append("HF=@HF,");
            strSql.Append("CollectionType=@CollectionType,");
            strSql.Append("CollectionDatetime=@CollectionDatetime,");
            strSql.Append("CollectionBy=@CollectionBy,");
            strSql.Append("H2S=@H2S,");
            strSql.Append("COSS=@COSS,");
            strSql.Append("C2F6=@C2F6,");
            strSql.Append("C3F8=@C3F8");
            strSql.Append(" where OID=@OID");
            SqlParameter[] parameters = {
                    new SqlParameter("@CheckPointCode", SqlDbType.VarChar,4),
                    new SqlParameter("@CheckPointName", SqlDbType.NVarChar,50),
                    new SqlParameter("@Pressure", SqlDbType.Decimal,9),
                    new SqlParameter("@Temperture", SqlDbType.Decimal,9),
                    new SqlParameter("@MicroWater", SqlDbType.Decimal,9),
                    new SqlParameter("@SO2", SqlDbType.Decimal,9),
                    new SqlParameter("@CO", SqlDbType.Decimal,9),
                    new SqlParameter("@CF4", SqlDbType.Decimal,9),
                    new SqlParameter("@SO2F2", SqlDbType.Decimal,9),
                    new SqlParameter("@SOF2", SqlDbType.Decimal,9),
                    new SqlParameter("@CS2", SqlDbType.Decimal,9),
                    new SqlParameter("@HF", SqlDbType.Decimal,9),
                    new SqlParameter("@CollectionType", SqlDbType.VarChar,4),
                    new SqlParameter("@CollectionDatetime", SqlDbType.DateTime),
                    new SqlParameter("@CollectionBy", SqlDbType.NVarChar,50),
                    new SqlParameter("@H2S", SqlDbType.Decimal,9),
                    new SqlParameter("@COSS", SqlDbType.Decimal,9),
                    new SqlParameter("@C2F6", SqlDbType.Decimal,9),
                    new SqlParameter("@C3F8", SqlDbType.Decimal,9),
                    new SqlParameter("@OID", SqlDbType.Int,4)};
            parameters[0].Value = model.CheckPointCode;
            parameters[1].Value = model.CheckPointName;
            parameters[2].Value = model.Pressure;
            parameters[3].Value = model.Temperture;
            parameters[4].Value = model.MicroWater;
            parameters[5].Value = model.SO2;
            parameters[6].Value = model.CO;
            parameters[7].Value = model.CF4;
            parameters[8].Value = model.SO2F2;
            parameters[9].Value = model.SOF2;
            parameters[10].Value = model.CS2;
            parameters[11].Value = model.HF;
            parameters[12].Value = model.CollectionType;
            parameters[13].Value = model.CollectionDatetime;
            parameters[14].Value = model.CollectionBy;
            parameters[15].Value = model.H2S;
            parameters[16].Value = model.COSS;
            parameters[17].Value = model.C2F6;
            parameters[18].Value = model.C3F8;
            parameters[19].Value = model.OID;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int OID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from OCInstrumentInfoOnline ");
			strSql.Append(" where OID=@OID");
			SqlParameter[] parameters = {
					new SqlParameter("@OID", SqlDbType.Int,4)
			};
			parameters[0].Value = OID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string OIDlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from OCInstrumentInfoOnline ");
			strSql.Append(" where OID in ("+OIDlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public EGMNGS.Model.OCInstrumentInfoOnline GetModel(int OID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 * from OCInstrumentInfoOnline ");
			strSql.Append(" where OID=@OID");
			SqlParameter[] parameters = {
					new SqlParameter("@OID", SqlDbType.Int,4)
			};
			parameters[0].Value = OID;

			EGMNGS.Model.OCInstrumentInfoOnline model=new EGMNGS.Model.OCInstrumentInfoOnline();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public EGMNGS.Model.OCInstrumentInfoOnline DataRowToModel(DataRow row)
		{
			EGMNGS.Model.OCInstrumentInfoOnline model=new EGMNGS.Model.OCInstrumentInfoOnline();
			if (row != null)
			{
				if(row["OID"]!=null && row["OID"].ToString()!="")
				{
					model.OID=int.Parse(row["OID"].ToString());
				}
				if(row["CheckPointCode"]!=null)
				{
					model.CheckPointCode=row["CheckPointCode"].ToString();
				}
				if(row["CheckPointName"]!=null)
				{
					model.CheckPointName=row["CheckPointName"].ToString();
				}
				if(row["Pressure"]!=null && row["Pressure"].ToString()!="")
				{
					model.Pressure=decimal.Parse(row["Pressure"].ToString());
				}
				if(row["Temperture"]!=null && row["Temperture"].ToString()!="")
				{
					model.Temperture=decimal.Parse(row["Temperture"].ToString());
				}
				if(row["MicroWater"]!=null && row["MicroWater"].ToString()!="")
				{
					model.MicroWater=decimal.Parse(row["MicroWater"].ToString());
				}
				if(row["SO2"]!=null && row["SO2"].ToString()!="")
				{
					model.SO2=decimal.Parse(row["SO2"].ToString());
				}
				if(row["CO"]!=null && row["CO"].ToString()!="")
				{
					model.CO=decimal.Parse(row["CO"].ToString());
				}
				if(row["CF4"]!=null && row["CF4"].ToString()!="")
				{
					model.CF4=decimal.Parse(row["CF4"].ToString());
				}
				if(row["SO2F2"]!=null && row["SO2F2"].ToString()!="")
				{
					model.SO2F2=decimal.Parse(row["SO2F2"].ToString());
				}
				if(row["SOF2"]!=null && row["SOF2"].ToString()!="")
				{
					model.SOF2=decimal.Parse(row["SOF2"].ToString());
				}
				if(row["CS2"]!=null && row["CS2"].ToString()!="")
				{
					model.CS2=decimal.Parse(row["CS2"].ToString());
				}
				if(row["HF"]!=null && row["HF"].ToString()!="")
				{
					model.HF=decimal.Parse(row["HF"].ToString());
				}
				if(row["CollectionType"]!=null)
				{
					model.CollectionType=row["CollectionType"].ToString();
				}
				if(row["CollectionDatetime"]!=null && row["CollectionDatetime"].ToString()!="")
				{
					model.CollectionDatetime=DateTime.Parse(row["CollectionDatetime"].ToString());
				}
				if(row["CollectionBy"]!=null)
				{
					model.CollectionBy=row["CollectionBy"].ToString();
				}

                if (row["H2S"] != null && row["H2S"].ToString() != "")
                {
                    model.H2S = decimal.Parse(row["H2S"].ToString());
                }
                if (row["COSS"] != null && row["COSS"].ToString() != "")
                {
                    model.COSS = decimal.Parse(row["COSS"].ToString());
                }
                if (row["C2F6"] != null && row["C2F6"].ToString() != "")
                {
                    model.C2F6 = decimal.Parse(row["C2F6"].ToString());
                }
                if (row["C3F8"] != null && row["C3F8"].ToString() != "")
                {
                    model.C3F8 = decimal.Parse(row["C3F8"].ToString());
                }
            }
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select * ");
			strSql.Append(" FROM OCInstrumentInfoOnline ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" * ");
			strSql.Append(" FROM OCInstrumentInfoOnline ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM OCInstrumentInfoOnline ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby + " desc");
			}
			else
			{
				strSql.Append("order by T.OID desc");
			}
			strSql.Append(")AS Row, T.*  from OCInstrumentInfoOnline T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", (startIndex - 1) * endIndex + 1, startIndex * endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "OCInstrumentInfoOnline";
			parameters[1].Value = "OID";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}


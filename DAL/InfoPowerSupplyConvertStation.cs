﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Maticsoft.DBUtility;
using EGMNGS.Common;//Please add references
namespace EGMNGS.DAL
{
    /// <summary>
    /// 数据访问类:InfoPowerSupplyConvertStation
    /// </summary>
    public partial class InfoPowerSupplyConvertStation
    {
        public InfoPowerSupplyConvertStation()
        { }
        #region  Method

        /// <summary>
        /// 得到最大ID
        /// </summary>
        public int GetMaxId()
        {
            return DbHelperSQL.GetMaxID("OID", "InfoPowerSupplyConvertStation");
        }

        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int OID)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) from InfoPowerSupplyConvertStation");
            strSql.Append(" where OID=@OID");
            SqlParameter[] parameters = {
					new SqlParameter("@OID", SqlDbType.Int,4)
			};
            parameters[0].Value = OID;

            return DbHelperSQL.Exists(strSql.ToString(), parameters);
        }


        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(EGMNGS.Model.InfoPowerSupplyConvertStation model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into InfoPowerSupplyConvertStation(");
            strSql.Append("PowerSupplyID,PowerSupplyCode,PowerSupplyName,CovnertStationCode,ConvartStationName,Status,RegistrantOID,RegistrantDate,CREATED_BY,CREATED_DATE,LAST_UPD_BY,LAST_UPD_DATE,IsOnlineCheck,IsMagnetic,Longitude,Latitude)");
            strSql.Append(" values (");
            strSql.Append("@PowerSupplyID,@PowerSupplyCode,@PowerSupplyName,@CovnertStationCode,@ConvartStationName,@Status,@RegistrantOID,@RegistrantDate,@CREATED_BY,@CREATED_DATE,@LAST_UPD_BY,@LAST_UPD_DATE,@IsOnlineCheck,@IsMagnetic,@Longitude,@Latitude)");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
					new SqlParameter("@PowerSupplyID", SqlDbType.VarChar,20),
					new SqlParameter("@PowerSupplyCode", SqlDbType.VarChar,50),
					new SqlParameter("@PowerSupplyName", SqlDbType.NVarChar,50),
					new SqlParameter("@CovnertStationCode", SqlDbType.VarChar,20),
					new SqlParameter("@ConvartStationName", SqlDbType.NVarChar,50),
					new SqlParameter("@Status", SqlDbType.VarChar,4),
					new SqlParameter("@RegistrantOID", SqlDbType.VarChar,50),
					new SqlParameter("@RegistrantDate", SqlDbType.Date,3),
					new SqlParameter("@CREATED_BY", SqlDbType.NVarChar,12),
					new SqlParameter("@CREATED_DATE", SqlDbType.Date,3),
					new SqlParameter("@LAST_UPD_BY", SqlDbType.NVarChar,12),
					new SqlParameter("@LAST_UPD_DATE", SqlDbType.Date,3),
            new SqlParameter("@IsOnlineCheck", SqlDbType.VarChar,10),
            new SqlParameter("@IsMagnetic", SqlDbType.VarChar,10),
            new SqlParameter("@Longitude", SqlDbType.Decimal,10),
            new SqlParameter("@Latitude", SqlDbType.Decimal,10)};
            parameters[0].Value = model.PowerSupplyID;
            parameters[1].Value = model.PowerSupplyCode;
            parameters[2].Value = model.PowerSupplyName;
            parameters[3].Value = model.CovnertStationCode;
            parameters[4].Value = model.ConvartStationName;
            parameters[5].Value = model.Status;
            parameters[6].Value = model.RegistrantOID;
            parameters[7].Value = model.RegistrantDate;
            parameters[8].Value = model.CREATED_BY;
            parameters[9].Value = model.CREATED_DATE;
            parameters[10].Value = model.LAST_UPD_BY;
            parameters[11].Value = model.LAST_UPD_DATE;
            parameters[12].Value = model.IsOnlineCheck;
            parameters[13].Value = model.IsMagnetic;
            parameters[14].Value = model.Longitude;
            parameters[15].Value = model.Latitude;
            object obj = DbHelperSQL.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {
                ComServies.UpdCodeNum(model.CovnertStationCode);
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(EGMNGS.Model.InfoPowerSupplyConvertStation model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update InfoPowerSupplyConvertStation set ");
            strSql.Append("PowerSupplyID=@PowerSupplyID,");
            strSql.Append("PowerSupplyCode=@PowerSupplyCode,");
            strSql.Append("PowerSupplyName=@PowerSupplyName,");
            strSql.Append("CovnertStationCode=@CovnertStationCode,");
            strSql.Append("ConvartStationName=@ConvartStationName,");
            strSql.Append("Status=@Status,");
            strSql.Append("RegistrantOID=@RegistrantOID,");
            strSql.Append("RegistrantDate=@RegistrantDate,");
            strSql.Append("CREATED_BY=@CREATED_BY,");
            strSql.Append("CREATED_DATE=@CREATED_DATE,");
            strSql.Append("LAST_UPD_BY=@LAST_UPD_BY,");
            strSql.Append("LAST_UPD_DATE=@LAST_UPD_DATE,");
            strSql.Append("IsOnlineCheck=@IsOnlineCheck,");
            strSql.Append("IsMagnetic=@IsMagnetic,");
            strSql.Append("Longitude=@Longitude,");
            strSql.Append("Latitude=@Latitude");
            strSql.Append(" where OID=@OID");
            SqlParameter[] parameters = {
					new SqlParameter("@PowerSupplyID", SqlDbType.VarChar,20),
					new SqlParameter("@PowerSupplyCode", SqlDbType.VarChar,50),
					new SqlParameter("@PowerSupplyName", SqlDbType.VarChar,50),
					new SqlParameter("@CovnertStationCode", SqlDbType.VarChar,20),
					new SqlParameter("@ConvartStationName", SqlDbType.VarChar,50),
					new SqlParameter("@Status", SqlDbType.VarChar,4),
					new SqlParameter("@RegistrantOID", SqlDbType.VarChar,50),
					new SqlParameter("@RegistrantDate", SqlDbType.Date,3),
					new SqlParameter("@CREATED_BY", SqlDbType.NVarChar,12),
					new SqlParameter("@CREATED_DATE", SqlDbType.Date,3),
					new SqlParameter("@LAST_UPD_BY", SqlDbType.NVarChar,12),
					new SqlParameter("@LAST_UPD_DATE", SqlDbType.Date,3),
					new SqlParameter("@OID", SqlDbType.Int,4),
                    new SqlParameter("@IsOnlineCheck", SqlDbType.VarChar,10),
            new SqlParameter("@IsMagnetic", SqlDbType.VarChar,10),
            new SqlParameter("@Longitude", SqlDbType.Decimal,10),
            new SqlParameter("@Latitude", SqlDbType.Decimal,10)
            };
            parameters[0].Value = model.PowerSupplyID;
            parameters[1].Value = model.PowerSupplyCode;
            parameters[2].Value = model.PowerSupplyName;
            parameters[3].Value = model.CovnertStationCode;
            parameters[4].Value = model.ConvartStationName;
            parameters[5].Value = model.Status;
            parameters[6].Value = model.RegistrantOID;
            parameters[7].Value = model.RegistrantDate;
            parameters[8].Value = model.CREATED_BY;
            parameters[9].Value = model.CREATED_DATE;
            parameters[10].Value = model.LAST_UPD_BY;
            parameters[11].Value = model.LAST_UPD_DATE;
            parameters[12].Value = model.OID;
            parameters[13].Value = model.IsOnlineCheck;
            parameters[14].Value = model.IsMagnetic;
            parameters[15].Value = model.Longitude;
            parameters[16].Value = model.Latitude;
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int OID)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from InfoPowerSupplyConvertStation ");
            strSql.Append(" where OID=@OID");
            SqlParameter[] parameters = {
					new SqlParameter("@OID", SqlDbType.Int,4)
			};
            parameters[0].Value = OID;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string OIDlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from InfoPowerSupplyConvertStation ");
            strSql.Append(" where OID in (" + OIDlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public EGMNGS.Model.InfoPowerSupplyConvertStation GetModel(int OID)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 * from InfoPowerSupplyConvertStation ");
            strSql.Append(" where OID=@OID");
            SqlParameter[] parameters = {
					new SqlParameter("@OID", SqlDbType.Int,4)
			};
            parameters[0].Value = OID;

            EGMNGS.Model.InfoPowerSupplyConvertStation model = new EGMNGS.Model.InfoPowerSupplyConvertStation();
            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0]["OID"] != null && ds.Tables[0].Rows[0]["OID"].ToString() != "")
                {
                    model.OID = int.Parse(ds.Tables[0].Rows[0]["OID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["PowerSupplyID"] != null && ds.Tables[0].Rows[0]["PowerSupplyID"].ToString() != "")
                {
                    model.PowerSupplyID = ds.Tables[0].Rows[0]["PowerSupplyID"].ToString();
                }
                if (ds.Tables[0].Rows[0]["PowerSupplyCode"] != null && ds.Tables[0].Rows[0]["PowerSupplyCode"].ToString() != "")
                {
                    model.PowerSupplyCode = ds.Tables[0].Rows[0]["PowerSupplyCode"].ToString();
                }
                if (ds.Tables[0].Rows[0]["PowerSupplyName"] != null && ds.Tables[0].Rows[0]["PowerSupplyName"].ToString() != "")
                {
                    model.PowerSupplyName = ds.Tables[0].Rows[0]["PowerSupplyName"].ToString();
                }
                if (ds.Tables[0].Rows[0]["CovnertStationCode"] != null && ds.Tables[0].Rows[0]["CovnertStationCode"].ToString() != "")
                {
                    model.CovnertStationCode = ds.Tables[0].Rows[0]["CovnertStationCode"].ToString();
                }
                if (ds.Tables[0].Rows[0]["ConvartStationName"] != null && ds.Tables[0].Rows[0]["ConvartStationName"].ToString() != "")
                {
                    model.ConvartStationName = ds.Tables[0].Rows[0]["ConvartStationName"].ToString();
                }
                if (ds.Tables[0].Rows[0]["Status"] != null && ds.Tables[0].Rows[0]["Status"].ToString() != "")
                {
                    model.Status = ds.Tables[0].Rows[0]["Status"].ToString();
                }
                if (ds.Tables[0].Rows[0]["RegistrantOID"] != null && ds.Tables[0].Rows[0]["RegistrantOID"].ToString() != "")
                {
                    model.RegistrantOID = ds.Tables[0].Rows[0]["RegistrantOID"].ToString();
                }
                if (ds.Tables[0].Rows[0]["RegistrantDate"] != null && ds.Tables[0].Rows[0]["RegistrantDate"].ToString() != "")
                {
                    model.RegistrantDate = DateTime.Parse(ds.Tables[0].Rows[0]["RegistrantDate"].ToString());
                }
                if (ds.Tables[0].Rows[0]["CREATED_BY"] != null && ds.Tables[0].Rows[0]["CREATED_BY"].ToString() != "")
                {
                    model.CREATED_BY = ds.Tables[0].Rows[0]["CREATED_BY"].ToString();
                }
                if (ds.Tables[0].Rows[0]["CREATED_DATE"] != null && ds.Tables[0].Rows[0]["CREATED_DATE"].ToString() != "")
                {
                    model.CREATED_DATE = DateTime.Parse(ds.Tables[0].Rows[0]["CREATED_DATE"].ToString());
                }
                if (ds.Tables[0].Rows[0]["LAST_UPD_BY"] != null && ds.Tables[0].Rows[0]["LAST_UPD_BY"].ToString() != "")
                {
                    model.LAST_UPD_BY = ds.Tables[0].Rows[0]["LAST_UPD_BY"].ToString();
                }
                if (ds.Tables[0].Rows[0]["LAST_UPD_DATE"] != null && ds.Tables[0].Rows[0]["LAST_UPD_DATE"].ToString() != "")
                {
                    model.LAST_UPD_DATE = DateTime.Parse(ds.Tables[0].Rows[0]["LAST_UPD_DATE"].ToString());
                }

                if (ds.Tables[0].Rows[0]["IsOnlineCheck"] != null && ds.Tables[0].Rows[0]["IsOnlineCheck"].ToString() != "")
                {
                    model.IsOnlineCheck = ds.Tables[0].Rows[0]["IsOnlineCheck"].ToString();
                }

                if (ds.Tables[0].Rows[0]["IsMagnetic"] != null && ds.Tables[0].Rows[0]["IsMagnetic"].ToString() != "")
                {
                    model.IsMagnetic = ds.Tables[0].Rows[0]["IsMagnetic"].ToString();
                }

                if (ds.Tables[0].Rows[0]["Longitude"] != null && ds.Tables[0].Rows[0]["Longitude"].ToString() != "")
                {
                    model.Longitude = Convert.ToDecimal(ds.Tables[0].Rows[0]["Longitude"]);
                }

                if (ds.Tables[0].Rows[0]["Latitude"] != null && ds.Tables[0].Rows[0]["Latitude"].ToString() != "")
                {
                    model.Latitude = Convert.ToDecimal(ds.Tables[0].Rows[0]["Latitude"]);
                }
                return model;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select * ");
            strSql.Append(" FROM InfoPowerSupplyConvertStation ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" * ");
            strSql.Append(" FROM InfoPowerSupplyConvertStation ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM InfoPowerSupplyConvertStation ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.OID desc");
            }
            strSql.Append(")AS Row, T.*  from InfoPowerSupplyConvertStation T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", (startIndex - 1) * endIndex + 1, startIndex * endIndex);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /*
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        {
            SqlParameter[] parameters = {
                    new SqlParameter("@tblName", SqlDbType.VarChar, 255),
                    new SqlParameter("@fldName", SqlDbType.VarChar, 255),
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@PageIndex", SqlDbType.Int),
                    new SqlParameter("@IsReCount", SqlDbType.Bit),
                    new SqlParameter("@OrderType", SqlDbType.Bit),
                    new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
                    };
            parameters[0].Value = "InfoPowerSupplyConvertStation";
            parameters[1].Value = "OID";
            parameters[2].Value = PageSize;
            parameters[3].Value = PageIndex;
            parameters[4].Value = 0;
            parameters[5].Value = 0;
            parameters[6].Value = strWhere;	
            return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
        }*/

        #endregion  Method
    }
}


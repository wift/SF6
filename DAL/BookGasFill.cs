﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Maticsoft.DBUtility;//Please add references
namespace EGMNGS.DAL
{
    /// <summary>
    /// 数据访问类:BookGasFill
    /// </summary>
    public partial class BookGasFill
    {
        public BookGasFill()
        { }
        #region  Method

        /// <summary>
        /// 得到最大ID
        /// </summary>
        public int GetMaxId()
        {
            return DbHelperSQL.GetMaxID("OID", "BookGasFill");
        }

        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int OID)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) from BookGasFill");
            strSql.Append(" where OID=@OID");
            SqlParameter[] parameters = {
					new SqlParameter("@OID", SqlDbType.Int,4)
			};
            parameters[0].Value = OID;

            return DbHelperSQL.Exists(strSql.ToString(), parameters);
        }


        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(EGMNGS.Model.BookGasFill model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into BookGasFill(");
            strSql.Append("FillGasCode,Year,Month,BusinessCode,CylinderCode,GasCode,GasType,GasStatus,CylinderStatus,AmountGas,RegistrantOID,RegistrantDate,CREATED_BY,CREATED_DATE,LAST_UPD_BY,LAST_UPD_DATE,Status)");
            strSql.Append(" values (");
            strSql.Append("@FillGasCode,@Year,@Month,@BusinessCode,@CylinderCode,@GasCode,@GasType,@GasStatus,@CylinderStatus,@AmountGas,@RegistrantOID,@RegistrantDate,@CREATED_BY,@CREATED_DATE,@LAST_UPD_BY,@LAST_UPD_DATE,@Status)");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
					new SqlParameter("@FillGasCode", SqlDbType.VarChar,20),
					new SqlParameter("@Year", SqlDbType.VarChar,4),
					new SqlParameter("@Month", SqlDbType.VarChar,2),
					new SqlParameter("@BusinessCode", SqlDbType.VarChar,20),
					new SqlParameter("@CylinderCode", SqlDbType.VarChar,20),
					new SqlParameter("@GasCode", SqlDbType.VarChar,20),
					new SqlParameter("@GasType", SqlDbType.VarChar,4),
					new SqlParameter("@GasStatus", SqlDbType.VarChar,4),
					new SqlParameter("@CylinderStatus", SqlDbType.VarChar,4),
					new SqlParameter("@AmountGas", SqlDbType.Decimal,9),
					new SqlParameter("@RegistrantOID", SqlDbType.VarChar,50),
					new SqlParameter("@RegistrantDate", SqlDbType.Date,3),
					new SqlParameter("@CREATED_BY", SqlDbType.NVarChar,12),
					new SqlParameter("@CREATED_DATE", SqlDbType.Date,3),
					new SqlParameter("@LAST_UPD_BY", SqlDbType.NVarChar,12),
					new SqlParameter("@LAST_UPD_DATE", SqlDbType.Date,3),
                    new SqlParameter("@Status", SqlDbType.VarChar,4)};
            parameters[0].Value = model.FillGasCode;
            parameters[1].Value = model.Year;
            parameters[2].Value = model.Month;
            parameters[3].Value = model.BusinessCode;
            parameters[4].Value = model.CylinderCode;
            parameters[5].Value = model.GasCode;
            parameters[6].Value = model.GasType;
            parameters[7].Value = model.GasStatus;
            parameters[8].Value = model.CylinderStatus;
            parameters[9].Value = model.AmountGas;
            parameters[10].Value = model.RegistrantOID;
            parameters[11].Value = model.RegistrantDate;
            parameters[12].Value = model.CREATED_BY;
            parameters[13].Value = model.CREATED_DATE;
            parameters[14].Value = model.LAST_UPD_BY;
            parameters[15].Value = model.LAST_UPD_DATE;
            parameters[16].Value = model.Status;
            object obj = DbHelperSQL.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {
                EGMNGS.Common.ComServies.UpdCodeNum(model.FillGasCode);
                EGMNGS.Common.ComServies.UpdCodeNum(model.GasCode);
                return Convert.ToInt32(obj);
            }
        }

        public CommandInfo AddCommandInfo(Model.BookGasFill model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into BookGasFill(");
            strSql.Append("FillGasCode,Year,Month,BusinessCode,CylinderCode,GasCode,GasType,GasStatus,CylinderStatus,AmountGas,RegistrantOID,RegistrantDate,CREATED_BY,CREATED_DATE,LAST_UPD_BY,LAST_UPD_DATE,Status)");
            strSql.Append(" values (");
            strSql.Append("@FillGasCode,@Year,@Month,@BusinessCode,@CylinderCode,@GasCode,@GasType,@GasStatus,@CylinderStatus,@AmountGas,@RegistrantOID,@RegistrantDate,@CREATED_BY,@CREATED_DATE,@LAST_UPD_BY,@LAST_UPD_DATE,@Status)");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
					new SqlParameter("@FillGasCode", SqlDbType.VarChar,20),
					new SqlParameter("@Year", SqlDbType.VarChar,4),
					new SqlParameter("@Month", SqlDbType.VarChar,2),
					new SqlParameter("@BusinessCode", SqlDbType.VarChar,20),
					new SqlParameter("@CylinderCode", SqlDbType.VarChar,20),
					new SqlParameter("@GasCode", SqlDbType.VarChar,20),
					new SqlParameter("@GasType", SqlDbType.VarChar,4),
					new SqlParameter("@GasStatus", SqlDbType.VarChar,4),
					new SqlParameter("@CylinderStatus", SqlDbType.VarChar,4),
					new SqlParameter("@AmountGas", SqlDbType.Decimal,9),
					new SqlParameter("@RegistrantOID", SqlDbType.VarChar,50),
					new SqlParameter("@RegistrantDate", SqlDbType.Date,3),
					new SqlParameter("@CREATED_BY", SqlDbType.NVarChar,12),
					new SqlParameter("@CREATED_DATE", SqlDbType.Date,3),
					new SqlParameter("@LAST_UPD_BY", SqlDbType.NVarChar,12),
					new SqlParameter("@LAST_UPD_DATE", SqlDbType.Date,3),
                    new SqlParameter("@Status", SqlDbType.VarChar,4)};
            parameters[0].Value = model.FillGasCode;
            parameters[1].Value = model.Year;
            parameters[2].Value = model.Month;
            parameters[3].Value = model.BusinessCode;
            parameters[4].Value = model.CylinderCode;
            parameters[5].Value = model.GasCode;
            parameters[6].Value = model.GasType;
            parameters[7].Value = model.GasStatus;
            parameters[8].Value = model.CylinderStatus;
            parameters[9].Value = model.AmountGas;
            parameters[10].Value = model.RegistrantOID;
            parameters[11].Value = model.RegistrantDate;
            parameters[12].Value = model.CREATED_BY;
            parameters[13].Value = model.CREATED_DATE;
            parameters[14].Value = model.LAST_UPD_BY;
            parameters[15].Value = model.LAST_UPD_DATE;
            parameters[16].Value = model.Status;

            return new CommandInfo(strSql.ToString(), parameters);
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(EGMNGS.Model.BookGasFill model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update BookGasFill set ");
            strSql.Append("FillGasCode=@FillGasCode,");
            strSql.Append("Year=@Year,");
            strSql.Append("Month=@Month,");
            strSql.Append("BusinessCode=@BusinessCode,");
            strSql.Append("CylinderCode=@CylinderCode,");
            strSql.Append("GasCode=@GasCode,");
            strSql.Append("GasType=@GasType,");
            strSql.Append("GasStatus=@GasStatus,");
            strSql.Append("CylinderStatus=@CylinderStatus,");
            strSql.Append("AmountGas=@AmountGas,");
            strSql.Append("RegistrantOID=@RegistrantOID,");
            strSql.Append("RegistrantDate=@RegistrantDate,");
            strSql.Append("CREATED_BY=@CREATED_BY,");
            strSql.Append("CREATED_DATE=@CREATED_DATE,");
            strSql.Append("LAST_UPD_BY=@LAST_UPD_BY,");
            strSql.Append("LAST_UPD_DATE=@LAST_UPD_DATE,");
            strSql.Append("Status=@Status");
            strSql.Append(" where OID=@OID");
            SqlParameter[] parameters = {
					new SqlParameter("@FillGasCode", SqlDbType.VarChar,20),
					new SqlParameter("@Year", SqlDbType.VarChar,4),
					new SqlParameter("@Month", SqlDbType.VarChar,2),
					new SqlParameter("@BusinessCode", SqlDbType.VarChar,20),
					new SqlParameter("@CylinderCode", SqlDbType.VarChar,20),
					new SqlParameter("@GasCode", SqlDbType.VarChar,20),
					new SqlParameter("@GasType", SqlDbType.VarChar,4),
					new SqlParameter("@GasStatus", SqlDbType.VarChar,4),
					new SqlParameter("@CylinderStatus", SqlDbType.VarChar,4),
					new SqlParameter("@AmountGas", SqlDbType.Decimal,9),
					new SqlParameter("@RegistrantOID", SqlDbType.VarChar,50),
					new SqlParameter("@RegistrantDate", SqlDbType.Date,3),
					new SqlParameter("@CREATED_BY", SqlDbType.NVarChar,12),
					new SqlParameter("@CREATED_DATE", SqlDbType.Date,3),
					new SqlParameter("@LAST_UPD_BY", SqlDbType.NVarChar,12),
					new SqlParameter("@LAST_UPD_DATE", SqlDbType.Date,3),
					new SqlParameter("@OID", SqlDbType.Int,4),
                    new SqlParameter("@Status", SqlDbType.VarChar,4)};
            parameters[0].Value = model.FillGasCode;
            parameters[1].Value = model.Year;
            parameters[2].Value = model.Month;
            parameters[3].Value = model.BusinessCode;
            parameters[4].Value = model.CylinderCode;
            parameters[5].Value = model.GasCode;
            parameters[6].Value = model.GasType;
            parameters[7].Value = model.GasStatus;
            parameters[8].Value = model.CylinderStatus;
            parameters[9].Value = model.AmountGas;
            parameters[10].Value = model.RegistrantOID;
            parameters[11].Value = model.RegistrantDate;
            parameters[12].Value = model.CREATED_BY;
            parameters[13].Value = model.CREATED_DATE;
            parameters[14].Value = model.LAST_UPD_BY;
            parameters[15].Value = model.LAST_UPD_DATE;
            parameters[16].Value = model.OID;
            parameters[17].Value = model.Status;
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int OID)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from BookGasFill ");
            strSql.Append(" where OID=@OID");
            SqlParameter[] parameters = {
					new SqlParameter("@OID", SqlDbType.Int,4)
			};
            parameters[0].Value = OID;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string OIDlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from BookGasFill ");
            strSql.Append(" where OID in (" + OIDlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public EGMNGS.Model.BookGasFill GetModel(int OID)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 OID,FillGasCode,Year,Month,BusinessCode,CylinderCode,GasCode,GasType,GasStatus,CylinderStatus,AmountGas,RegistrantOID,RegistrantDate,CREATED_BY,CREATED_DATE,LAST_UPD_BY,LAST_UPD_DATE,Status from BookGasFill ");
            strSql.Append(" where OID=@OID");
            SqlParameter[] parameters = {
					new SqlParameter("@OID", SqlDbType.Int,4)
			};
            parameters[0].Value = OID;

            EGMNGS.Model.BookGasFill model = new EGMNGS.Model.BookGasFill();
            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0]["OID"] != null && ds.Tables[0].Rows[0]["OID"].ToString() != "")
                {
                    model.OID = int.Parse(ds.Tables[0].Rows[0]["OID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["FillGasCode"] != null && ds.Tables[0].Rows[0]["FillGasCode"].ToString() != "")
                {
                    model.FillGasCode = ds.Tables[0].Rows[0]["FillGasCode"].ToString();
                }
                if (ds.Tables[0].Rows[0]["Year"] != null && ds.Tables[0].Rows[0]["Year"].ToString() != "")
                {
                    model.Year = ds.Tables[0].Rows[0]["Year"].ToString();
                }
                if (ds.Tables[0].Rows[0]["Month"] != null && ds.Tables[0].Rows[0]["Month"].ToString() != "")
                {
                    model.Month = ds.Tables[0].Rows[0]["Month"].ToString();
                }
                if (ds.Tables[0].Rows[0]["BusinessCode"] != null && ds.Tables[0].Rows[0]["BusinessCode"].ToString() != "")
                {
                    model.BusinessCode = ds.Tables[0].Rows[0]["BusinessCode"].ToString();
                }
                if (ds.Tables[0].Rows[0]["CylinderCode"] != null && ds.Tables[0].Rows[0]["CylinderCode"].ToString() != "")
                {
                    model.CylinderCode = ds.Tables[0].Rows[0]["CylinderCode"].ToString();
                }
                if (ds.Tables[0].Rows[0]["GasCode"] != null && ds.Tables[0].Rows[0]["GasCode"].ToString() != "")
                {
                    model.GasCode = ds.Tables[0].Rows[0]["GasCode"].ToString();
                }
                if (ds.Tables[0].Rows[0]["GasType"] != null && ds.Tables[0].Rows[0]["GasType"].ToString() != "")
                {
                    model.GasType = ds.Tables[0].Rows[0]["GasType"].ToString();
                }
                if (ds.Tables[0].Rows[0]["GasStatus"] != null && ds.Tables[0].Rows[0]["GasStatus"].ToString() != "")
                {
                    model.GasStatus = ds.Tables[0].Rows[0]["GasStatus"].ToString();
                }
                if (ds.Tables[0].Rows[0]["CylinderStatus"] != null && ds.Tables[0].Rows[0]["CylinderStatus"].ToString() != "")
                {
                    model.CylinderStatus = ds.Tables[0].Rows[0]["CylinderStatus"].ToString();
                }
                if (ds.Tables[0].Rows[0]["AmountGas"] != null && ds.Tables[0].Rows[0]["AmountGas"].ToString() != "")
                {
                    model.AmountGas = decimal.Parse(ds.Tables[0].Rows[0]["AmountGas"].ToString());
                }
                if (ds.Tables[0].Rows[0]["RegistrantOID"] != null && ds.Tables[0].Rows[0]["RegistrantOID"].ToString() != "")
                {
                    model.RegistrantOID = ds.Tables[0].Rows[0]["RegistrantOID"].ToString();
                }
                if (ds.Tables[0].Rows[0]["RegistrantDate"] != null && ds.Tables[0].Rows[0]["RegistrantDate"].ToString() != "")
                {
                    model.RegistrantDate = DateTime.Parse(ds.Tables[0].Rows[0]["RegistrantDate"].ToString());
                }
                if (ds.Tables[0].Rows[0]["CREATED_BY"] != null && ds.Tables[0].Rows[0]["CREATED_BY"].ToString() != "")
                {
                    model.CREATED_BY = ds.Tables[0].Rows[0]["CREATED_BY"].ToString();
                }
                if (ds.Tables[0].Rows[0]["CREATED_DATE"] != null && ds.Tables[0].Rows[0]["CREATED_DATE"].ToString() != "")
                {
                    model.CREATED_DATE = DateTime.Parse(ds.Tables[0].Rows[0]["CREATED_DATE"].ToString());
                }
                if (ds.Tables[0].Rows[0]["LAST_UPD_BY"] != null && ds.Tables[0].Rows[0]["LAST_UPD_BY"].ToString() != "")
                {
                    model.LAST_UPD_BY = ds.Tables[0].Rows[0]["LAST_UPD_BY"].ToString();
                }
                if (ds.Tables[0].Rows[0]["LAST_UPD_DATE"] != null && ds.Tables[0].Rows[0]["LAST_UPD_DATE"].ToString() != "")
                {
                    model.LAST_UPD_DATE = DateTime.Parse(ds.Tables[0].Rows[0]["LAST_UPD_DATE"].ToString());
                }
                if (ds.Tables[0].Rows[0]["Status"] != null && ds.Tables[0].Rows[0]["Status"].ToString() != "")
                {
                    model.Status = ds.Tables[0].Rows[0]["Status"] as string;
                }
                return model;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select OID,FillGasCode,Year,Month,BusinessCode,CylinderCode,GasCode,GasType,GasStatus,CylinderStatus,AmountGas,RegistrantOID,RegistrantDate,CREATED_BY,CREATED_DATE,LAST_UPD_BY,LAST_UPD_DATE,Status");
            strSql.Append(" FROM BookGasFill ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" OID,FillGasCode,Year,Month,BusinessCode,CylinderCode,GasCode,GasType,GasStatus,CylinderStatus,AmountGas,RegistrantOID,RegistrantDate,CREATED_BY,CREATED_DATE,LAST_UPD_BY,LAST_UPD_DATE ");
            strSql.Append(" FROM BookGasFill ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM BookGasFill ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.OID desc");
            }
            strSql.Append(")AS Row, T.*  from BookGasFill T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", (startIndex - 1) * endIndex + 1, startIndex * endIndex);
            return DbHelperSQL.Query(strSql.ToString());
        }


        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize, int PageIndex, string strWhere)
        {
            SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
            parameters[0].Value = "BookGasFill";
            parameters[1].Value = "OID";
            parameters[2].Value = PageSize;
            parameters[3].Value = PageIndex;
            parameters[4].Value = 0;
            parameters[5].Value = 0;
            parameters[6].Value = strWhere;
            return DbHelperSQL.RunProcedure("UP_GetRecordByPage", parameters, "ds");


        #endregion  Method
        }


    }
}


﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Maticsoft.DBUtility;//Please add references
namespace EGMNGS.DAL
{
	/// <summary>
	/// 数据访问类:OCInstrumentInfo_Childs
	/// </summary>
	public partial class OCInstrumentInfo_Childs
	{
		public OCInstrumentInfo_Childs()
		{}
		#region  BasicMethod

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
		return DbHelperSQL.GetMaxID("OID", "OCInstrumentInfo_Childs"); 
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int OID)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from OCInstrumentInfo_Childs");
			strSql.Append(" where OID=@OID");
			SqlParameter[] parameters = {
					new SqlParameter("@OID", SqlDbType.Int,4)
			};
			parameters[0].Value = OID;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(EGMNGS.Model.OCInstrumentInfo_Childs model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into OCInstrumentInfo_Childs(");
			strSql.Append("CheckPointCode,TopTarget,BottomTarget,MaxValue)");
			strSql.Append(" values (");
			strSql.Append("@CheckPointCode,@TopTarget,@BottomTarget,@MaxValue)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@CheckPointCode", SqlDbType.VarChar,50),
					new SqlParameter("@TopTarget", SqlDbType.VarChar,10),
					new SqlParameter("@BottomTarget", SqlDbType.VarChar,10),
					new SqlParameter("@MaxValue", SqlDbType.Float,8)};
			parameters[0].Value = model.CheckPointCode;
			parameters[1].Value = model.TopTarget;
			parameters[2].Value = model.BottomTarget;
			parameters[3].Value = model.MaxValue;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(EGMNGS.Model.OCInstrumentInfo_Childs model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update OCInstrumentInfo_Childs set ");
			strSql.Append("CheckPointCode=@CheckPointCode,");
			strSql.Append("TopTarget=@TopTarget,");
			strSql.Append("BottomTarget=@BottomTarget,");
			strSql.Append("MaxValue=@MaxValue");
			strSql.Append(" where OID=@OID");
			SqlParameter[] parameters = {
					new SqlParameter("@CheckPointCode", SqlDbType.VarChar,50),
					new SqlParameter("@TopTarget", SqlDbType.VarChar,10),
					new SqlParameter("@BottomTarget", SqlDbType.VarChar,10),
					new SqlParameter("@MaxValue", SqlDbType.Float,8),
					new SqlParameter("@OID", SqlDbType.Int,4)};
			parameters[0].Value = model.CheckPointCode;
			parameters[1].Value = model.TopTarget;
			parameters[2].Value = model.BottomTarget;
			parameters[3].Value = model.MaxValue;
			parameters[4].Value = model.OID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int OID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from OCInstrumentInfo_Childs ");
			strSql.Append(" where OID=@OID");
			SqlParameter[] parameters = {
					new SqlParameter("@OID", SqlDbType.Int,4)
			};
			parameters[0].Value = OID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string OIDlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from OCInstrumentInfo_Childs ");
			strSql.Append(" where OID in ("+OIDlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public EGMNGS.Model.OCInstrumentInfo_Childs GetModel(int OID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 OID,CheckPointCode,TopTarget,BottomTarget,MaxValue from OCInstrumentInfo_Childs ");
			strSql.Append(" where OID=@OID");
			SqlParameter[] parameters = {
					new SqlParameter("@OID", SqlDbType.Int,4)
			};
			parameters[0].Value = OID;

			EGMNGS.Model.OCInstrumentInfo_Childs model=new EGMNGS.Model.OCInstrumentInfo_Childs();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public EGMNGS.Model.OCInstrumentInfo_Childs DataRowToModel(DataRow row)
		{
			EGMNGS.Model.OCInstrumentInfo_Childs model=new EGMNGS.Model.OCInstrumentInfo_Childs();
			if (row != null)
			{
				if(row["OID"]!=null && row["OID"].ToString()!="")
				{
					model.OID=int.Parse(row["OID"].ToString());
				}
				if(row["CheckPointCode"]!=null)
				{
					model.CheckPointCode=row["CheckPointCode"].ToString();
				}
				if(row["TopTarget"]!=null)
				{
					model.TopTarget=row["TopTarget"].ToString();
				}
				if(row["BottomTarget"]!=null)
				{
					model.BottomTarget=row["BottomTarget"].ToString();
				}
				if(row["MaxValue"]!=null && row["MaxValue"].ToString()!="")
				{
					model.MaxValue=decimal.Parse(row["MaxValue"].ToString());
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select OID,CheckPointCode,TopTarget,BottomTarget,MaxValue ");
			strSql.Append(" FROM OCInstrumentInfo_Childs ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" OID,CheckPointCode,TopTarget,BottomTarget,MaxValue ");
			strSql.Append(" FROM OCInstrumentInfo_Childs ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM OCInstrumentInfo_Childs ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby + " desc");
			}
			else
			{
				strSql.Append("order by T.OID desc");
			}
			strSql.Append(")AS Row, T.*  from OCInstrumentInfo_Childs T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", (startIndex - 1) * endIndex + 1, startIndex * endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "OCInstrumentInfo_Childs";
			parameters[1].Value = "OID";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}


﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Maticsoft.DBUtility;
using EGMNGS.Common;//Please add references
namespace EGMNGS.DAL
{
    /// <summary>
    /// 数据访问类:TableGasOutStorage
    /// </summary>
    public partial class TableGasOutStorage
    {
        public TableGasOutStorage()
        { }
        #region  Method
        /// <summary>
        /// 得到最大ID
        /// </summary>
        public int GetMaxId()
        {
            return DbHelperSQL.GetMaxID("OID", "TableGasOutStorage");
        }

        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int OID)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) from TableGasOutStorage");
            strSql.Append(" where OID=@OID");
            SqlParameter[] parameters = {
					new SqlParameter("@OID", SqlDbType.Int,4)
			};
            parameters[0].Value = OID;

            return DbHelperSQL.Exists(strSql.ToString(), parameters);
        }


        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(EGMNGS.Model.TableGasOutStorage model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into TableGasOutStorage(");
            strSql.Append("Code,Year,Month,GasClass,PowerSupplyCode,PowerSupplyName,ConvertStationCode,ConvertStationName,AmountOutStorage,CountCyliner,Address,Contacts,OfficeTel,PhoneNum,Status,UsingOID,UsingDate,RegistantOID,RegistantDate,CREATED_BY,CREATED_DATE,LAST_UPD_BY,LAST_UPD_DATE,BusinessCode,CountDesc)");
            strSql.Append(" values (");
            strSql.Append("@Code,@Year,@Month,@GasClass,@PowerSupplyCode,@PowerSupplyName,@ConvertStationCode,@ConvertStationName,@AmountOutStorage,@CountCyliner,@Address,@Contacts,@OfficeTel,@PhoneNum,@Status,@UsingOID,@UsingDate,@RegistantOID,@RegistantDate,@CREATED_BY,@CREATED_DATE,@LAST_UPD_BY,@LAST_UPD_DATE,@BusinessCode,@CountDesc)");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
					new SqlParameter("@Code", SqlDbType.VarChar,20),
					new SqlParameter("@Year", SqlDbType.VarChar,4),
					new SqlParameter("@Month", SqlDbType.VarChar,2),
					new SqlParameter("@GasClass", SqlDbType.VarChar,4),
					new SqlParameter("@PowerSupplyCode", SqlDbType.VarChar,50),
					new SqlParameter("@PowerSupplyName", SqlDbType.NVarChar,50),
					new SqlParameter("@ConvertStationCode", SqlDbType.VarChar,50),
					new SqlParameter("@ConvertStationName", SqlDbType.NVarChar,50),
					new SqlParameter("@AmountOutStorage", SqlDbType.Decimal,9),
					new SqlParameter("@CountCyliner", SqlDbType.Int,4),
					new SqlParameter("@Address", SqlDbType.NVarChar,200),
					new SqlParameter("@Contacts", SqlDbType.VarChar,50),
					new SqlParameter("@OfficeTel", SqlDbType.VarChar,20),
					new SqlParameter("@PhoneNum", SqlDbType.VarChar,20),
					new SqlParameter("@Status", SqlDbType.VarChar,20),
					new SqlParameter("@UsingOID", SqlDbType.VarChar,50),
					new SqlParameter("@UsingDate", SqlDbType.Date,3),
					new SqlParameter("@RegistantOID", SqlDbType.VarChar,50),
					new SqlParameter("@RegistantDate", SqlDbType.Date,3),
					new SqlParameter("@CREATED_BY", SqlDbType.NVarChar,12),
					new SqlParameter("@CREATED_DATE", SqlDbType.Date,3),
					new SqlParameter("@LAST_UPD_BY", SqlDbType.NVarChar,12),
					new SqlParameter("@LAST_UPD_DATE", SqlDbType.Date,3),
					new SqlParameter("@BusinessCode", SqlDbType.VarChar,20),
                    new SqlParameter("@CountDesc", SqlDbType.NVarChar,200)};
            parameters[0].Value = model.Code;
            parameters[1].Value = model.Year;
            parameters[2].Value = model.Month;
            parameters[3].Value = model.GasClass;
            parameters[4].Value = model.PowerSupplyCode;
            parameters[5].Value = model.PowerSupplyName;
            parameters[6].Value = model.ConvertStationCode;
            parameters[7].Value = model.ConvertStationName;
            parameters[8].Value = model.AmountOutStorage;
            parameters[9].Value = model.CountCyliner;
            parameters[10].Value = model.Address;
            parameters[11].Value = model.Contacts;
            parameters[12].Value = model.OfficeTel;
            parameters[13].Value = model.PhoneNum;
            parameters[14].Value = model.Status;
            parameters[15].Value = model.UsingOID;
            parameters[16].Value = model.UsingDate;
            parameters[17].Value = model.RegistantOID;
            parameters[18].Value = model.RegistantDate;
            parameters[19].Value = model.CREATED_BY;
            parameters[20].Value = model.CREATED_DATE;
            parameters[21].Value = model.LAST_UPD_BY;
            parameters[22].Value = model.LAST_UPD_DATE;
            parameters[23].Value = model.BusinessCode;
            parameters[24].Value = model.CountDesc;

            object obj = DbHelperSQL.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {
                ComServies.UpdCodeNum(model.Code);
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(EGMNGS.Model.TableGasOutStorage model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update TableGasOutStorage set ");
            strSql.Append("Code=@Code,");
            strSql.Append("Year=@Year,");
            strSql.Append("Month=@Month,");
            strSql.Append("GasClass=@GasClass,");
            strSql.Append("PowerSupplyCode=@PowerSupplyCode,");
            strSql.Append("PowerSupplyName=@PowerSupplyName,");
            strSql.Append("ConvertStationCode=@ConvertStationCode,");
            strSql.Append("ConvertStationName=@ConvertStationName,");
            strSql.Append("AmountOutStorage=@AmountOutStorage,");
            strSql.Append("CountCyliner=@CountCyliner,");
            strSql.Append("Address=@Address,");
            strSql.Append("Contacts=@Contacts,");
            strSql.Append("OfficeTel=@OfficeTel,");
            strSql.Append("PhoneNum=@PhoneNum,");
            strSql.Append("Status=@Status,");
            strSql.Append("UsingOID=@UsingOID,");
            strSql.Append("UsingDate=@UsingDate,");
            strSql.Append("RegistantOID=@RegistantOID,");
            strSql.Append("RegistantDate=@RegistantDate,");
            strSql.Append("CREATED_BY=@CREATED_BY,");
            strSql.Append("CREATED_DATE=@CREATED_DATE,");
            strSql.Append("LAST_UPD_BY=@LAST_UPD_BY,");
            strSql.Append("LAST_UPD_DATE=@LAST_UPD_DATE,");
            strSql.Append("BusinessCode=@BusinessCode");
            strSql.Append("CountDesc=@CountDesc");
            strSql.Append(" where OID=@OID");
            SqlParameter[] parameters = {
					new SqlParameter("@Code", SqlDbType.VarChar,20),
					new SqlParameter("@Year", SqlDbType.VarChar,4),
					new SqlParameter("@Month", SqlDbType.VarChar,2),
					new SqlParameter("@GasClass", SqlDbType.VarChar,4),
					new SqlParameter("@PowerSupplyCode", SqlDbType.VarChar,20),
					new SqlParameter("@PowerSupplyName", SqlDbType.VarChar,50),
					new SqlParameter("@ConvertStationCode", SqlDbType.VarChar,50),
					new SqlParameter("@ConvertStationName", SqlDbType.VarChar,50),
					new SqlParameter("@AmountOutStorage", SqlDbType.Decimal,9),
					new SqlParameter("@CountCyliner", SqlDbType.Int,4),
					new SqlParameter("@Address", SqlDbType.NVarChar,200),
					new SqlParameter("@Contacts", SqlDbType.VarChar,50),
					new SqlParameter("@OfficeTel", SqlDbType.VarChar,20),
					new SqlParameter("@PhoneNum", SqlDbType.VarChar,20),
					new SqlParameter("@Status", SqlDbType.VarChar,20),
					new SqlParameter("@UsingOID", SqlDbType.VarChar,50),
					new SqlParameter("@UsingDate", SqlDbType.Date,3),
					new SqlParameter("@RegistantOID", SqlDbType.VarChar,50),
					new SqlParameter("@RegistantDate", SqlDbType.Date,3),
					new SqlParameter("@CREATED_BY", SqlDbType.NVarChar,12),
					new SqlParameter("@CREATED_DATE", SqlDbType.Date,3),
					new SqlParameter("@LAST_UPD_BY", SqlDbType.NVarChar,12),
					new SqlParameter("@LAST_UPD_DATE", SqlDbType.Date,3),
					new SqlParameter("@BusinessCode", SqlDbType.VarChar,20),
					new SqlParameter("@OID", SqlDbType.Int,4),
                     new SqlParameter("@CountDesc", SqlDbType.NVarChar,20)};
            parameters[0].Value = model.Code;
            parameters[1].Value = model.Year;
            parameters[2].Value = model.Month;
            parameters[3].Value = model.GasClass;
            parameters[4].Value = model.PowerSupplyCode;
            parameters[5].Value = model.PowerSupplyName;
            parameters[6].Value = model.ConvertStationCode;
            parameters[7].Value = model.ConvertStationName;
            parameters[8].Value = model.AmountOutStorage;
            parameters[9].Value = model.CountCyliner;
            parameters[10].Value = model.Address;
            parameters[11].Value = model.Contacts;
            parameters[12].Value = model.OfficeTel;
            parameters[13].Value = model.PhoneNum;
            parameters[14].Value = model.Status;
            parameters[15].Value = model.UsingOID;
            parameters[16].Value = model.UsingDate;
            parameters[17].Value = model.RegistantOID;
            parameters[18].Value = model.RegistantDate;
            parameters[19].Value = model.CREATED_BY;
            parameters[20].Value = model.CREATED_DATE;
            parameters[21].Value = model.LAST_UPD_BY;
            parameters[22].Value = model.LAST_UPD_DATE;
            parameters[23].Value = model.BusinessCode;
            parameters[24].Value = model.OID;
            parameters[25].Value = model.CountDesc;
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int OID)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from TableGasOutStorage ");
            strSql.Append(" where OID=@OID");
            SqlParameter[] parameters = {
					new SqlParameter("@OID", SqlDbType.Int,4)
			};
            parameters[0].Value = OID;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string OIDlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from TableGasOutStorage ");
            strSql.Append(" where OID in (" + OIDlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public EGMNGS.Model.TableGasOutStorage GetModel(int OID)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 OID,Code,Year,Month,GasClass,PowerSupplyCode,PowerSupplyName,ConvertStationCode,ConvertStationName,AmountOutStorage,CountCyliner,Address,Contacts,OfficeTel,PhoneNum,Status,UsingOID,UsingDate,RegistantOID,RegistantDate,CREATED_BY,CREATED_DATE,LAST_UPD_BY,LAST_UPD_DATE,BusinessCode,CountDesc from TableGasOutStorage ");
            strSql.Append(" where OID=@OID");
            SqlParameter[] parameters = {
					new SqlParameter("@OID", SqlDbType.Int,4)
			};
            parameters[0].Value = OID;

            EGMNGS.Model.TableGasOutStorage model = new EGMNGS.Model.TableGasOutStorage();
            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0]["OID"] != null && ds.Tables[0].Rows[0]["OID"].ToString() != "")
                {
                    model.OID = int.Parse(ds.Tables[0].Rows[0]["OID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["Code"] != null && ds.Tables[0].Rows[0]["Code"].ToString() != "")
                {
                    model.Code = ds.Tables[0].Rows[0]["Code"].ToString();
                }
                if (ds.Tables[0].Rows[0]["Year"] != null && ds.Tables[0].Rows[0]["Year"].ToString() != "")
                {
                    model.Year = ds.Tables[0].Rows[0]["Year"].ToString();
                }
                if (ds.Tables[0].Rows[0]["Month"] != null && ds.Tables[0].Rows[0]["Month"].ToString() != "")
                {
                    model.Month = ds.Tables[0].Rows[0]["Month"].ToString();
                }
                if (ds.Tables[0].Rows[0]["GasClass"] != null && ds.Tables[0].Rows[0]["GasClass"].ToString() != "")
                {
                    model.GasClass = ds.Tables[0].Rows[0]["GasClass"].ToString();
                }
                if (ds.Tables[0].Rows[0]["PowerSupplyCode"] != null && ds.Tables[0].Rows[0]["PowerSupplyCode"].ToString() != "")
                {
                    model.PowerSupplyCode = ds.Tables[0].Rows[0]["PowerSupplyCode"].ToString();
                }
                if (ds.Tables[0].Rows[0]["PowerSupplyName"] != null && ds.Tables[0].Rows[0]["PowerSupplyName"].ToString() != "")
                {
                    model.PowerSupplyName = ds.Tables[0].Rows[0]["PowerSupplyName"].ToString();
                }
                if (ds.Tables[0].Rows[0]["ConvertStationCode"] != null && ds.Tables[0].Rows[0]["ConvertStationCode"].ToString() != "")
                {
                    model.ConvertStationCode = ds.Tables[0].Rows[0]["ConvertStationCode"].ToString();
                }
                if (ds.Tables[0].Rows[0]["ConvertStationName"] != null && ds.Tables[0].Rows[0]["ConvertStationName"].ToString() != "")
                {
                    model.ConvertStationName = ds.Tables[0].Rows[0]["ConvertStationName"].ToString();
                }
                if (ds.Tables[0].Rows[0]["AmountOutStorage"] != null && ds.Tables[0].Rows[0]["AmountOutStorage"].ToString() != "")
                {
                    model.AmountOutStorage = decimal.Parse(ds.Tables[0].Rows[0]["AmountOutStorage"].ToString());
                }
                if (ds.Tables[0].Rows[0]["CountCyliner"] != null && ds.Tables[0].Rows[0]["CountCyliner"].ToString() != "")
                {
                    model.CountCyliner = int.Parse(ds.Tables[0].Rows[0]["CountCyliner"].ToString());
                }
                if (ds.Tables[0].Rows[0]["Address"] != null && ds.Tables[0].Rows[0]["Address"].ToString() != "")
                {
                    model.Address = ds.Tables[0].Rows[0]["Address"].ToString();
                }
                if (ds.Tables[0].Rows[0]["Contacts"] != null && ds.Tables[0].Rows[0]["Contacts"].ToString() != "")
                {
                    model.Contacts = ds.Tables[0].Rows[0]["Contacts"].ToString();
                }
                if (ds.Tables[0].Rows[0]["OfficeTel"] != null && ds.Tables[0].Rows[0]["OfficeTel"].ToString() != "")
                {
                    model.OfficeTel = ds.Tables[0].Rows[0]["OfficeTel"].ToString();
                }
                if (ds.Tables[0].Rows[0]["PhoneNum"] != null && ds.Tables[0].Rows[0]["PhoneNum"].ToString() != "")
                {
                    model.PhoneNum = ds.Tables[0].Rows[0]["PhoneNum"].ToString();
                }
                if (ds.Tables[0].Rows[0]["Status"] != null && ds.Tables[0].Rows[0]["Status"].ToString() != "")
                {
                    model.Status = ds.Tables[0].Rows[0]["Status"].ToString();
                }
                if (ds.Tables[0].Rows[0]["UsingOID"] != null && ds.Tables[0].Rows[0]["UsingOID"].ToString() != "")
                {
                    model.UsingOID = ds.Tables[0].Rows[0]["UsingOID"].ToString();
                }
                if (ds.Tables[0].Rows[0]["UsingDate"] != null && ds.Tables[0].Rows[0]["UsingDate"].ToString() != "")
                {
                    model.UsingDate = DateTime.Parse(ds.Tables[0].Rows[0]["UsingDate"].ToString());
                }
                if (ds.Tables[0].Rows[0]["RegistantOID"] != null && ds.Tables[0].Rows[0]["RegistantOID"].ToString() != "")
                {
                    model.RegistantOID = ds.Tables[0].Rows[0]["RegistantOID"].ToString();
                }
                if (ds.Tables[0].Rows[0]["RegistantDate"] != null && ds.Tables[0].Rows[0]["RegistantDate"].ToString() != "")
                {
                    model.RegistantDate = DateTime.Parse(ds.Tables[0].Rows[0]["RegistantDate"].ToString());
                }
                if (ds.Tables[0].Rows[0]["CREATED_BY"] != null && ds.Tables[0].Rows[0]["CREATED_BY"].ToString() != "")
                {
                    model.CREATED_BY = ds.Tables[0].Rows[0]["CREATED_BY"].ToString();
                }
                if (ds.Tables[0].Rows[0]["CREATED_DATE"] != null && ds.Tables[0].Rows[0]["CREATED_DATE"].ToString() != "")
                {
                    model.CREATED_DATE = DateTime.Parse(ds.Tables[0].Rows[0]["CREATED_DATE"].ToString());
                }
                if (ds.Tables[0].Rows[0]["LAST_UPD_BY"] != null && ds.Tables[0].Rows[0]["LAST_UPD_BY"].ToString() != "")
                {
                    model.LAST_UPD_BY = ds.Tables[0].Rows[0]["LAST_UPD_BY"].ToString();
                }
                if (ds.Tables[0].Rows[0]["LAST_UPD_DATE"] != null && ds.Tables[0].Rows[0]["LAST_UPD_DATE"].ToString() != "")
                {
                    model.LAST_UPD_DATE = DateTime.Parse(ds.Tables[0].Rows[0]["LAST_UPD_DATE"].ToString());
                }
                if (ds.Tables[0].Rows[0]["BusinessCode"] != null && ds.Tables[0].Rows[0]["BusinessCode"].ToString() != "")
                {
                    model.BusinessCode = ds.Tables[0].Rows[0]["BusinessCode"].ToString();
                }
                if (ds.Tables[0].Rows[0]["CountDesc"] != null && ds.Tables[0].Rows[0]["CountDesc"].ToString() != "")
                {
                    model.CountDesc = ds.Tables[0].Rows[0]["CountDesc"].ToString();
                }

                return model;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select OID,Code,Year,Month,GasClass,PowerSupplyCode,PowerSupplyName,ConvertStationCode,ConvertStationName,AmountOutStorage,CountCyliner,Address,Contacts,OfficeTel,PhoneNum,Status,UsingOID,UsingDate,RegistantOID,RegistantDate,CREATED_BY,CREATED_DATE,LAST_UPD_BY,LAST_UPD_DATE,BusinessCode,CountDesc, ");
            strSql.Append(@"(SELECT COUNT(1) FROM DetailsGasOutStorage dgos 
WHERE dgos.TableGasOutStorage_Code=Code AND dgos.AmountGas=50)  as '50',");
            strSql.Append(@"(SELECT COUNT(1) FROM DetailsGasOutStorage dgos 
WHERE dgos.TableGasOutStorage_Code=Code AND dgos.AmountGas=25)  as '25'");
            strSql.Append(" FROM TableGasOutStorage ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" OID,Code,Year,Month,GasClass,PowerSupplyCode,PowerSupplyName,ConvertStationCode,ConvertStationName,AmountOutStorage,CountCyliner,Address,Contacts,OfficeTel,PhoneNum,Status,UsingOID,UsingDate,RegistantOID,RegistantDate,CREATED_BY,CREATED_DATE,LAST_UPD_BY,LAST_UPD_DATE,BusinessCode,CountDesc ");
            strSql.Append(" FROM TableGasOutStorage ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM TableGasOutStorage ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.OID desc");
            }
            strSql.Append(")AS Row, T.*  from TableGasOutStorage T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}",

(startIndex - 1) * endIndex + 1, startIndex * endIndex);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /*
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        {
            SqlParameter[] parameters = {
                    new SqlParameter("@tblName", SqlDbType.VarChar, 255),
                    new SqlParameter("@fldName", SqlDbType.VarChar, 255),
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@PageIndex", SqlDbType.Int),
                    new SqlParameter("@IsReCount", SqlDbType.Bit),
                    new SqlParameter("@OrderType", SqlDbType.Bit),
                    new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
                    };
            parameters[0].Value = "TableGasOutStorage";
            parameters[1].Value = "OID";
            parameters[2].Value = PageSize;
            parameters[3].Value = PageIndex;
            parameters[4].Value = 0;
            parameters[5].Value = 0;
            parameters[6].Value = strWhere;	
            return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
        }*/

        #endregion  Method
    }
}


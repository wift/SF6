﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Maticsoft.DBUtility;//Please add references
namespace EGMNGS.DAL
{
	/// <summary>
	/// 数据访问类:BookGasStorage
	/// </summary>
	public partial class BookGasStorage
	{
		public BookGasStorage()
		{}
		#region  Method

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
		return DbHelperSQL.GetMaxID("OID", "BookGasStorage"); 
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int OID)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from BookGasStorage");
			strSql.Append(" where OID=@OID");
			SqlParameter[] parameters = {
					new SqlParameter("@OID", SqlDbType.Int,4)
			};
			parameters[0].Value = OID;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(EGMNGS.Model.BookGasStorage model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into BookGasStorage(");
			strSql.Append("Year,Month,CountRecycleGas,CountWaitingGas,CountPassGas,CheckStatus,CheckOID,CheckDate,CREATED_BY,CREATED_DATE,LAST_UPD_BY,LAST_UPD_DATE)");
			strSql.Append(" values (");
			strSql.Append("@Year,@Month,@CountRecycleGas,@CountWaitingGas,@CountPassGas,@CheckStatus,@CheckOID,@CheckDate,@CREATED_BY,@CREATED_DATE,@LAST_UPD_BY,@LAST_UPD_DATE)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@Year", SqlDbType.VarChar,4),
					new SqlParameter("@Month", SqlDbType.VarChar,2),
					new SqlParameter("@CountRecycleGas", SqlDbType.Decimal,9),
					new SqlParameter("@CountWaitingGas", SqlDbType.Decimal,9),
					new SqlParameter("@CountPassGas", SqlDbType.Decimal,9),
					new SqlParameter("@CheckStatus", SqlDbType.VarChar,4),
					new SqlParameter("@CheckOID", SqlDbType.VarChar,50),
					new SqlParameter("@CheckDate", SqlDbType.Date,3),
					new SqlParameter("@CREATED_BY", SqlDbType.NVarChar,12),
					new SqlParameter("@CREATED_DATE", SqlDbType.Date,3),
					new SqlParameter("@LAST_UPD_BY", SqlDbType.NVarChar,12),
					new SqlParameter("@LAST_UPD_DATE", SqlDbType.Date,3)};
			parameters[0].Value = model.Year;
			parameters[1].Value = model.Month;
			parameters[2].Value = model.CountRecycleGas;
			parameters[3].Value = model.CountWaitingGas;
			parameters[4].Value = model.CountPassGas;
			parameters[5].Value = model.CheckStatus;
			parameters[6].Value = model.CheckOID;
			parameters[7].Value = model.CheckDate;
			parameters[8].Value = model.CREATED_BY;
			parameters[9].Value = model.CREATED_DATE;
			parameters[10].Value = model.LAST_UPD_BY;
			parameters[11].Value = model.LAST_UPD_DATE;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(EGMNGS.Model.BookGasStorage model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update BookGasStorage set ");
			strSql.Append("Year=@Year,");
			strSql.Append("Month=@Month,");
			strSql.Append("CountRecycleGas=@CountRecycleGas,");
			strSql.Append("CountWaitingGas=@CountWaitingGas,");
			strSql.Append("CountPassGas=@CountPassGas,");
			strSql.Append("CheckStatus=@CheckStatus,");
			strSql.Append("CheckOID=@CheckOID,");
			strSql.Append("CheckDate=@CheckDate,");
			strSql.Append("CREATED_BY=@CREATED_BY,");
			strSql.Append("CREATED_DATE=@CREATED_DATE,");
			strSql.Append("LAST_UPD_BY=@LAST_UPD_BY,");
			strSql.Append("LAST_UPD_DATE=@LAST_UPD_DATE");
			strSql.Append(" where OID=@OID");
			SqlParameter[] parameters = {
					new SqlParameter("@Year", SqlDbType.VarChar,4),
					new SqlParameter("@Month", SqlDbType.VarChar,2),
					new SqlParameter("@CountRecycleGas", SqlDbType.Decimal,9),
					new SqlParameter("@CountWaitingGas", SqlDbType.Decimal,9),
					new SqlParameter("@CountPassGas", SqlDbType.Decimal,9),
					new SqlParameter("@CheckStatus", SqlDbType.VarChar,4),
					new SqlParameter("@CheckOID", SqlDbType.VarChar,50),
					new SqlParameter("@CheckDate", SqlDbType.Date,3),
					new SqlParameter("@CREATED_BY", SqlDbType.NVarChar,12),
					new SqlParameter("@CREATED_DATE", SqlDbType.Date,3),
					new SqlParameter("@LAST_UPD_BY", SqlDbType.NVarChar,12),
					new SqlParameter("@LAST_UPD_DATE", SqlDbType.Date,3),
					new SqlParameter("@OID", SqlDbType.Int,4)};
			parameters[0].Value = model.Year;
			parameters[1].Value = model.Month;
			parameters[2].Value = model.CountRecycleGas;
			parameters[3].Value = model.CountWaitingGas;
			parameters[4].Value = model.CountPassGas;
			parameters[5].Value = model.CheckStatus;
			parameters[6].Value = model.CheckOID;
			parameters[7].Value = model.CheckDate;
			parameters[8].Value = model.CREATED_BY;
			parameters[9].Value = model.CREATED_DATE;
			parameters[10].Value = model.LAST_UPD_BY;
			parameters[11].Value = model.LAST_UPD_DATE;
			parameters[12].Value = model.OID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int OID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from BookGasStorage ");
			strSql.Append(" where OID=@OID");
			SqlParameter[] parameters = {
					new SqlParameter("@OID", SqlDbType.Int,4)
			};
			parameters[0].Value = OID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string OIDlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from BookGasStorage ");
			strSql.Append(" where OID in ("+OIDlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public EGMNGS.Model.BookGasStorage GetModel(int OID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 OID,Year,Month,CountRecycleGas,CountWaitingGas,CountPassGas,CheckStatus,CheckOID,CheckDate,CREATED_BY,CREATED_DATE,LAST_UPD_BY,LAST_UPD_DATE from BookGasStorage ");
			strSql.Append(" where OID=@OID");
			SqlParameter[] parameters = {
					new SqlParameter("@OID", SqlDbType.Int,4)
			};
			parameters[0].Value = OID;

			EGMNGS.Model.BookGasStorage model=new EGMNGS.Model.BookGasStorage();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				if(ds.Tables[0].Rows[0]["OID"]!=null && ds.Tables[0].Rows[0]["OID"].ToString()!="")
				{
					model.OID=int.Parse(ds.Tables[0].Rows[0]["OID"].ToString());
				}
				if(ds.Tables[0].Rows[0]["Year"]!=null && ds.Tables[0].Rows[0]["Year"].ToString()!="")
				{
					model.Year=ds.Tables[0].Rows[0]["Year"].ToString();
				}
				if(ds.Tables[0].Rows[0]["Month"]!=null && ds.Tables[0].Rows[0]["Month"].ToString()!="")
				{
					model.Month=ds.Tables[0].Rows[0]["Month"].ToString();
				}
				if(ds.Tables[0].Rows[0]["CountRecycleGas"]!=null && ds.Tables[0].Rows[0]["CountRecycleGas"].ToString()!="")
				{
					model.CountRecycleGas=decimal.Parse(ds.Tables[0].Rows[0]["CountRecycleGas"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CountWaitingGas"]!=null && ds.Tables[0].Rows[0]["CountWaitingGas"].ToString()!="")
				{
					model.CountWaitingGas=decimal.Parse(ds.Tables[0].Rows[0]["CountWaitingGas"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CountPassGas"]!=null && ds.Tables[0].Rows[0]["CountPassGas"].ToString()!="")
				{
					model.CountPassGas=decimal.Parse(ds.Tables[0].Rows[0]["CountPassGas"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CheckStatus"]!=null && ds.Tables[0].Rows[0]["CheckStatus"].ToString()!="")
				{
					model.CheckStatus=ds.Tables[0].Rows[0]["CheckStatus"].ToString();
				}
				if(ds.Tables[0].Rows[0]["CheckOID"]!=null && ds.Tables[0].Rows[0]["CheckOID"].ToString()!="")
				{
					model.CheckOID=ds.Tables[0].Rows[0]["CheckOID"].ToString();
				}
				if(ds.Tables[0].Rows[0]["CheckDate"]!=null && ds.Tables[0].Rows[0]["CheckDate"].ToString()!="")
				{
					model.CheckDate=DateTime.Parse(ds.Tables[0].Rows[0]["CheckDate"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CREATED_BY"]!=null && ds.Tables[0].Rows[0]["CREATED_BY"].ToString()!="")
				{
					model.CREATED_BY=ds.Tables[0].Rows[0]["CREATED_BY"].ToString();
				}
				if(ds.Tables[0].Rows[0]["CREATED_DATE"]!=null && ds.Tables[0].Rows[0]["CREATED_DATE"].ToString()!="")
				{
					model.CREATED_DATE=DateTime.Parse(ds.Tables[0].Rows[0]["CREATED_DATE"].ToString());
				}
				if(ds.Tables[0].Rows[0]["LAST_UPD_BY"]!=null && ds.Tables[0].Rows[0]["LAST_UPD_BY"].ToString()!="")
				{
					model.LAST_UPD_BY=ds.Tables[0].Rows[0]["LAST_UPD_BY"].ToString();
				}
				if(ds.Tables[0].Rows[0]["LAST_UPD_DATE"]!=null && ds.Tables[0].Rows[0]["LAST_UPD_DATE"].ToString()!="")
				{
					model.LAST_UPD_DATE=DateTime.Parse(ds.Tables[0].Rows[0]["LAST_UPD_DATE"].ToString());
				}
				return model;
			}
			else
			{
				return null;
			}
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select OID,Year,Month,CountRecycleGas,CountWaitingGas,CountPassGas,CheckStatus,CheckOID,CheckDate,CREATED_BY,CREATED_DATE,LAST_UPD_BY,LAST_UPD_DATE ");
			strSql.Append(" FROM BookGasStorage ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" OID,Year,Month,CountRecycleGas,CountWaitingGas,CountPassGas,CheckStatus,CheckOID,CheckDate,CREATED_BY,CREATED_DATE,LAST_UPD_BY,LAST_UPD_DATE ");
			strSql.Append(" FROM BookGasStorage ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM BookGasStorage ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.OID desc");
			}
			strSql.Append(")AS Row, T.*  from BookGasStorage T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", (startIndex - 1) * endIndex + 1, startIndex * endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "BookGasStorage";
			parameters[1].Value = "OID";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  Method
	}
}


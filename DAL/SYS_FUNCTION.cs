﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Maticsoft.DBUtility;//Please add references
namespace EGMNGS.DAL
{
	/// <summary>
	/// 数据访问类:SYS_FUNCTION
	/// </summary>
	public partial class SYS_FUNCTION
	{
		public SYS_FUNCTION()
		{}
		#region  Method

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(string FUNC_ID)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from SYS_FUNCTION");
			strSql.Append(" where FUNC_ID=@FUNC_ID ");
			SqlParameter[] parameters = {
					new SqlParameter("@FUNC_ID", SqlDbType.VarChar,4)			};
			parameters[0].Value = FUNC_ID;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(EGMNGS.Model.SYS_FUNCTION model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into SYS_FUNCTION(");
			strSql.Append("FUNC_ID,FUNC_DESC,PARENT_FUNC_ID,PAGE_SOURCE,CREATED_BY,CREATED_DATE,LAST_UPD_BY,LAST_UPD_DATE)");
			strSql.Append(" values (");
			strSql.Append("@FUNC_ID,@FUNC_DESC,@PARENT_FUNC_ID,@PAGE_SOURCE,@CREATED_BY,@CREATED_DATE,@LAST_UPD_BY,@LAST_UPD_DATE)");
			SqlParameter[] parameters = {
					new SqlParameter("@FUNC_ID", SqlDbType.VarChar,4),
					new SqlParameter("@FUNC_DESC", SqlDbType.NVarChar,60),
					new SqlParameter("@PARENT_FUNC_ID", SqlDbType.VarChar,4),
					new SqlParameter("@PAGE_SOURCE", SqlDbType.VarChar,50),
					new SqlParameter("@CREATED_BY", SqlDbType.VarChar,12),
					new SqlParameter("@CREATED_DATE", SqlDbType.DateTime),
					new SqlParameter("@LAST_UPD_BY", SqlDbType.VarChar,12),
					new SqlParameter("@LAST_UPD_DATE", SqlDbType.DateTime)};
			parameters[0].Value = model.FUNC_ID;
			parameters[1].Value = model.FUNC_DESC;
			parameters[2].Value = model.PARENT_FUNC_ID;
			parameters[3].Value = model.PAGE_SOURCE;
			parameters[4].Value = model.CREATED_BY;
			parameters[5].Value = model.CREATED_DATE;
			parameters[6].Value = model.LAST_UPD_BY;
			parameters[7].Value = model.LAST_UPD_DATE;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(EGMNGS.Model.SYS_FUNCTION model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update SYS_FUNCTION set ");
			strSql.Append("FUNC_DESC=@FUNC_DESC,");
			strSql.Append("PARENT_FUNC_ID=@PARENT_FUNC_ID,");
			strSql.Append("PAGE_SOURCE=@PAGE_SOURCE,");
			strSql.Append("CREATED_BY=@CREATED_BY,");
			strSql.Append("CREATED_DATE=@CREATED_DATE,");
			strSql.Append("LAST_UPD_BY=@LAST_UPD_BY,");
			strSql.Append("LAST_UPD_DATE=@LAST_UPD_DATE");
			strSql.Append(" where FUNC_ID=@FUNC_ID ");
			SqlParameter[] parameters = {
					new SqlParameter("@FUNC_DESC", SqlDbType.NVarChar,60),
					new SqlParameter("@PARENT_FUNC_ID", SqlDbType.VarChar,4),
					new SqlParameter("@PAGE_SOURCE", SqlDbType.VarChar,50),
					new SqlParameter("@CREATED_BY", SqlDbType.VarChar,12),
					new SqlParameter("@CREATED_DATE", SqlDbType.DateTime),
					new SqlParameter("@LAST_UPD_BY", SqlDbType.VarChar,12),
					new SqlParameter("@LAST_UPD_DATE", SqlDbType.DateTime),
					new SqlParameter("@FUNC_ID", SqlDbType.VarChar,4)};
			parameters[0].Value = model.FUNC_DESC;
			parameters[1].Value = model.PARENT_FUNC_ID;
			parameters[2].Value = model.PAGE_SOURCE;
			parameters[3].Value = model.CREATED_BY;
			parameters[4].Value = model.CREATED_DATE;
			parameters[5].Value = model.LAST_UPD_BY;
			parameters[6].Value = model.LAST_UPD_DATE;
			parameters[7].Value = model.FUNC_ID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(string FUNC_ID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from SYS_FUNCTION ");
			strSql.Append(" where FUNC_ID=@FUNC_ID ");
			SqlParameter[] parameters = {
					new SqlParameter("@FUNC_ID", SqlDbType.VarChar,4)			};
			parameters[0].Value = FUNC_ID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string FUNC_IDlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from SYS_FUNCTION ");
			strSql.Append(" where FUNC_ID in ("+FUNC_IDlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public EGMNGS.Model.SYS_FUNCTION GetModel(string FUNC_ID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 FUNC_ID,FUNC_DESC,PARENT_FUNC_ID,PAGE_SOURCE,CREATED_BY,CREATED_DATE,LAST_UPD_BY,LAST_UPD_DATE from SYS_FUNCTION ");
			strSql.Append(" where FUNC_ID=@FUNC_ID ");
			SqlParameter[] parameters = {
					new SqlParameter("@FUNC_ID", SqlDbType.VarChar,4)			};
			parameters[0].Value = FUNC_ID;

			EGMNGS.Model.SYS_FUNCTION model=new EGMNGS.Model.SYS_FUNCTION();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				if(ds.Tables[0].Rows[0]["FUNC_ID"]!=null && ds.Tables[0].Rows[0]["FUNC_ID"].ToString()!="")
				{
					model.FUNC_ID=ds.Tables[0].Rows[0]["FUNC_ID"].ToString();
				}
				if(ds.Tables[0].Rows[0]["FUNC_DESC"]!=null && ds.Tables[0].Rows[0]["FUNC_DESC"].ToString()!="")
				{
					model.FUNC_DESC=ds.Tables[0].Rows[0]["FUNC_DESC"].ToString();
				}
				if(ds.Tables[0].Rows[0]["PARENT_FUNC_ID"]!=null && ds.Tables[0].Rows[0]["PARENT_FUNC_ID"].ToString()!="")
				{
					model.PARENT_FUNC_ID=ds.Tables[0].Rows[0]["PARENT_FUNC_ID"].ToString();
				}
				if(ds.Tables[0].Rows[0]["PAGE_SOURCE"]!=null && ds.Tables[0].Rows[0]["PAGE_SOURCE"].ToString()!="")
				{
					model.PAGE_SOURCE=ds.Tables[0].Rows[0]["PAGE_SOURCE"].ToString();
				}
				if(ds.Tables[0].Rows[0]["CREATED_BY"]!=null && ds.Tables[0].Rows[0]["CREATED_BY"].ToString()!="")
				{
					model.CREATED_BY=ds.Tables[0].Rows[0]["CREATED_BY"].ToString();
				}
				if(ds.Tables[0].Rows[0]["CREATED_DATE"]!=null && ds.Tables[0].Rows[0]["CREATED_DATE"].ToString()!="")
				{
					model.CREATED_DATE=DateTime.Parse(ds.Tables[0].Rows[0]["CREATED_DATE"].ToString());
				}
				if(ds.Tables[0].Rows[0]["LAST_UPD_BY"]!=null && ds.Tables[0].Rows[0]["LAST_UPD_BY"].ToString()!="")
				{
					model.LAST_UPD_BY=ds.Tables[0].Rows[0]["LAST_UPD_BY"].ToString();
				}
				if(ds.Tables[0].Rows[0]["LAST_UPD_DATE"]!=null && ds.Tables[0].Rows[0]["LAST_UPD_DATE"].ToString()!="")
				{
					model.LAST_UPD_DATE=DateTime.Parse(ds.Tables[0].Rows[0]["LAST_UPD_DATE"].ToString());
				}
				return model;
			}
			else
			{
				return null;
			}
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select FUNC_ID,FUNC_DESC,PARENT_FUNC_ID,PAGE_SOURCE,CREATED_BY,CREATED_DATE,LAST_UPD_BY,LAST_UPD_DATE ");
			strSql.Append(" FROM SYS_FUNCTION ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" FUNC_ID,FUNC_DESC,PARENT_FUNC_ID,PAGE_SOURCE,CREATED_BY,CREATED_DATE,LAST_UPD_BY,LAST_UPD_DATE ");
			strSql.Append(" FROM SYS_FUNCTION ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM SYS_FUNCTION ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.FUNC_ID desc");
			}
			strSql.Append(")AS Row, T.*  from SYS_FUNCTION T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", (startIndex - 1) * endIndex + 1, startIndex * endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "SYS_FUNCTION";
			parameters[1].Value = "FUNC_ID";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  Method
	}
}


﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Maticsoft.DBUtility;
using System.Collections.Generic;
using System.Collections;//Please add references
namespace EGMNGS.DAL
{
    /// <summary>
    /// 数据访问类:CylinderInspectTable
    /// </summary>
    public partial class CylinderInspectTable
    {
        public CylinderInspectTable()
        { }
        #region  Method

        /// <summary>
        /// 得到最大ID
        /// </summary>
        public int GetMaxId()
        {
            return DbHelperSQL.GetMaxID("OID", "CylinderInspectTable");
        }

        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int OID)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) from CylinderInspectTable");
            strSql.Append(" where OID=@OID");
            SqlParameter[] parameters = {
					new SqlParameter("@OID", SqlDbType.Int,4)
			};
            parameters[0].Value = OID;

            return DbHelperSQL.Exists(strSql.ToString(), parameters);
        }


        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(EGMNGS.Model.CylinderInspectTable model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into CylinderInspectTable(");
            strSql.Append("CylinderCode,OriginalEffectiveDate,NewEffectiveDate,IsPass,Remarks,Status,SendCheckOID,SendCheckDate,RegistantsOID,RegistantsDate,CREATED_BY,CREATED_DATE,LAST_UPD_BY,LAST_UPD_DATE)");
            strSql.Append(" values (");
            strSql.Append("@CylinderCode,@OriginalEffectiveDate,@NewEffectiveDate,@IsPass,@Remarks,@Status,@SendCheckOID,@SendCheckDate,@RegistantsOID,@RegistantsDate,@CREATED_BY,@CREATED_DATE,@LAST_UPD_BY,@LAST_UPD_DATE)");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
					new SqlParameter("@CylinderCode", SqlDbType.VarChar,20),
					new SqlParameter("@OriginalEffectiveDate", SqlDbType.Date,3),
					new SqlParameter("@NewEffectiveDate", SqlDbType.Date,3),
					new SqlParameter("@IsPass", SqlDbType.VarChar,4),
					new SqlParameter("@Remarks", SqlDbType.NVarChar,500),
					new SqlParameter("@Status", SqlDbType.VarChar,4),
					new SqlParameter("@SendCheckOID", SqlDbType.VarChar,50),
					new SqlParameter("@SendCheckDate", SqlDbType.Date,3),
					new SqlParameter("@RegistantsOID", SqlDbType.VarChar,50),
					new SqlParameter("@RegistantsDate", SqlDbType.Date,3),
					new SqlParameter("@CREATED_BY", SqlDbType.NVarChar,12),
					new SqlParameter("@CREATED_DATE", SqlDbType.Date,3),
					new SqlParameter("@LAST_UPD_BY", SqlDbType.NVarChar,12),
					new SqlParameter("@LAST_UPD_DATE", SqlDbType.Date,3)};
            parameters[0].Value = model.CylinderCode;
            parameters[1].Value = model.OriginalEffectiveDate;
            parameters[2].Value = model.NewEffectiveDate;
            parameters[3].Value = model.IsPass;
            parameters[4].Value = model.Remarks;
            parameters[5].Value = model.Status;
            parameters[6].Value = model.SendCheckOID;
            parameters[7].Value = model.SendCheckDate;
            parameters[8].Value = model.RegistantsOID;
            parameters[9].Value = model.RegistantsDate;
            parameters[10].Value = model.CREATED_BY;
            parameters[11].Value = model.CREATED_DATE;
            parameters[12].Value = model.LAST_UPD_BY;
            parameters[13].Value = model.LAST_UPD_DATE;

            object obj = DbHelperSQL.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(EGMNGS.Model.CylinderInspectTable model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update CylinderInspectTable set ");
            strSql.Append("CylinderCode=@CylinderCode,");
            strSql.Append("OriginalEffectiveDate=@OriginalEffectiveDate,");
            strSql.Append("NewEffectiveDate=@NewEffectiveDate,");
            strSql.Append("IsPass=@IsPass,");
            strSql.Append("Remarks=@Remarks,");
            strSql.Append("Status=@Status,");
            strSql.Append("SendCheckOID=@SendCheckOID,");
            strSql.Append("SendCheckDate=@SendCheckDate,");
            strSql.Append("RegistantsOID=@RegistantsOID,");
            strSql.Append("RegistantsDate=@RegistantsDate,");
            strSql.Append("CREATED_BY=@CREATED_BY,");
            strSql.Append("CREATED_DATE=@CREATED_DATE,");
            strSql.Append("LAST_UPD_BY=@LAST_UPD_BY,");
            strSql.Append("LAST_UPD_DATE=@LAST_UPD_DATE");
            strSql.Append(" where OID=@OID");
            SqlParameter[] parameters = {
					new SqlParameter("@CylinderCode", SqlDbType.VarChar,20),
					new SqlParameter("@OriginalEffectiveDate", SqlDbType.Date,3),
					new SqlParameter("@NewEffectiveDate", SqlDbType.Date,3),
					new SqlParameter("@IsPass", SqlDbType.VarChar,4),
					new SqlParameter("@Remarks", SqlDbType.NVarChar,500),
					new SqlParameter("@Status", SqlDbType.VarChar,4),
					new SqlParameter("@SendCheckOID", SqlDbType.VarChar,50),
					new SqlParameter("@SendCheckDate", SqlDbType.Date,3),
					new SqlParameter("@RegistantsOID", SqlDbType.VarChar,50),
					new SqlParameter("@RegistantsDate", SqlDbType.Date,3),
					new SqlParameter("@CREATED_BY", SqlDbType.NVarChar,12),
					new SqlParameter("@CREATED_DATE", SqlDbType.Date,3),
					new SqlParameter("@LAST_UPD_BY", SqlDbType.NVarChar,12),
					new SqlParameter("@LAST_UPD_DATE", SqlDbType.Date,3),
					new SqlParameter("@OID", SqlDbType.Int,4)};
            parameters[0].Value = model.CylinderCode;
            parameters[1].Value = model.OriginalEffectiveDate;
            parameters[2].Value = model.NewEffectiveDate;
            parameters[3].Value = model.IsPass;
            parameters[4].Value = model.Remarks;
            parameters[5].Value = model.Status;
            parameters[6].Value = model.SendCheckOID;
            parameters[7].Value = model.SendCheckDate;
            parameters[8].Value = model.RegistantsOID;
            parameters[9].Value = model.RegistantsDate;
            parameters[10].Value = model.CREATED_BY;
            parameters[11].Value = model.CREATED_DATE;
            parameters[12].Value = model.LAST_UPD_BY;
            parameters[13].Value = model.LAST_UPD_DATE;
            parameters[14].Value = model.OID;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 送检
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool UpdateSJ(EGMNGS.Model.CylinderInspectTable model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update CylinderInspectTable set ");
            strSql.Append("CylinderCode=@CylinderCode,");
            strSql.Append("OriginalEffectiveDate=@OriginalEffectiveDate,");
            strSql.Append("NewEffectiveDate=@NewEffectiveDate,");
            strSql.Append("IsPass=@IsPass,");
            strSql.Append("Remarks=@Remarks,");
            strSql.Append("Status=@Status,");
            strSql.Append("SendCheckOID=@SendCheckOID,");
            strSql.Append("SendCheckDate=@SendCheckDate,");
            strSql.Append("RegistantsOID=@RegistantsOID,");
            strSql.Append("RegistantsDate=@RegistantsDate,");
            strSql.Append("CREATED_BY=@CREATED_BY,");
            strSql.Append("CREATED_DATE=@CREATED_DATE,");
            strSql.Append("LAST_UPD_BY=@LAST_UPD_BY,");
            strSql.Append("LAST_UPD_DATE=@LAST_UPD_DATE");
            strSql.Append(" where OID=@OID");
            SqlParameter[] parameters = {
					new SqlParameter("@CylinderCode", SqlDbType.VarChar,20),
					new SqlParameter("@OriginalEffectiveDate", SqlDbType.Date,3),
					new SqlParameter("@NewEffectiveDate", SqlDbType.Date,3),
					new SqlParameter("@IsPass", SqlDbType.VarChar,4),
					new SqlParameter("@Remarks", SqlDbType.NVarChar,500),
					new SqlParameter("@Status", SqlDbType.VarChar,4),
					new SqlParameter("@SendCheckOID", SqlDbType.VarChar,50),
					new SqlParameter("@SendCheckDate", SqlDbType.Date,3),
					new SqlParameter("@RegistantsOID", SqlDbType.VarChar,50),
					new SqlParameter("@RegistantsDate", SqlDbType.Date,3),
					new SqlParameter("@CREATED_BY", SqlDbType.NVarChar,12),
					new SqlParameter("@CREATED_DATE", SqlDbType.Date,3),
					new SqlParameter("@LAST_UPD_BY", SqlDbType.NVarChar,12),
					new SqlParameter("@LAST_UPD_DATE", SqlDbType.Date,3),
					new SqlParameter("@OID", SqlDbType.Int,4)};
            parameters[0].Value = model.CylinderCode;
            parameters[1].Value = model.OriginalEffectiveDate;
            parameters[2].Value = model.NewEffectiveDate;
            parameters[3].Value = model.IsPass;
            parameters[4].Value = model.Remarks;
            parameters[5].Value = model.Status;
            parameters[6].Value = model.SendCheckOID;
            parameters[7].Value = model.SendCheckDate;
            parameters[8].Value = model.RegistantsOID;
            parameters[9].Value = model.RegistantsDate;
            parameters[10].Value = model.CREATED_BY;
            parameters[11].Value = model.CREATED_DATE;
            parameters[12].Value = model.LAST_UPD_BY;
            parameters[13].Value = model.LAST_UPD_DATE;
            parameters[14].Value = model.OID;


            StringBuilder strSqlsj = new StringBuilder();
            strSqlsj.Append("update BookCylinderInfo set ");
            strSqlsj.Append("EffectiveDate=@EffectiveDate,");
            strSqlsj.Append("Status=@Status");
            strSqlsj.Append(" where CylinderCode=@CylinderCode");
            SqlParameter[] parameterssj = {
					new SqlParameter("@EffectiveDate", SqlDbType.Date,3),
					new SqlParameter("@Status", SqlDbType.VarChar,4),
                   new SqlParameter("@CylinderCode", SqlDbType.VarChar,20)};
            if (model.IsPass == "1")
            {
                parameterssj[0].Value = model.NewEffectiveDate;
                parameterssj[1].Value = "0";//未抽空清洗
                parameterssj[2].Value = model.CylinderCode;
            }
            else
            {
                parameterssj[0].Value = model.NewEffectiveDate;
                parameterssj[1].Value = "6";//已报废
                parameterssj[2].Value = model.CylinderCode;
            }

            Hashtable ht = new Hashtable();
            ht.Add(strSql.ToString(), parameters);
            ht.Add(strSqlsj.ToString(), parameterssj);
            try
            {
                DbHelperSQL.ExecuteSqlTran(ht);
                return true;
            }
            catch (Exception ex)
            {

                return false;
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool UpdateStatus(string cylinderCode, string status,string oid)
        {
            string strSql = string.Format("UPDATE dbo.CylinderInspectTable SET Status='{0}' WHERE OID='{1}'", status, oid);
            string strSql2 = string.Format("UPDATE dbo.BookCylinderInfo SET Status='{0}' WHERE CylinderCode='{1}'", status, cylinderCode);
            List<string> lsSQL = new List<string>();
            lsSQL.Add(strSql);
            lsSQL.Add(strSql2);
            int rows = DbHelperSQL.ExecuteSqlTran(lsSQL);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int OID)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from CylinderInspectTable ");
            strSql.Append(" where OID=@OID");
            SqlParameter[] parameters = {
					new SqlParameter("@OID", SqlDbType.Int,4)
			};
            parameters[0].Value = OID;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string OIDlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from CylinderInspectTable ");
            strSql.Append(" where OID in (" + OIDlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public EGMNGS.Model.CylinderInspectTable GetModel(int OID)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 OID,CylinderCode,OriginalEffectiveDate,NewEffectiveDate,IsPass,Remarks,Status,SendCheckOID,SendCheckDate,RegistantsOID,RegistantsDate,CREATED_BY,CREATED_DATE,LAST_UPD_BY,LAST_UPD_DATE from CylinderInspectTable ");
            strSql.Append(" where OID=@OID");
            SqlParameter[] parameters = {
					new SqlParameter("@OID", SqlDbType.Int,4)
			};
            parameters[0].Value = OID;

            EGMNGS.Model.CylinderInspectTable model = new EGMNGS.Model.CylinderInspectTable();
            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0]["OID"] != null && ds.Tables[0].Rows[0]["OID"].ToString() != "")
                {
                    model.OID = int.Parse(ds.Tables[0].Rows[0]["OID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["CylinderCode"] != null && ds.Tables[0].Rows[0]["CylinderCode"].ToString() != "")
                {
                    model.CylinderCode = ds.Tables[0].Rows[0]["CylinderCode"].ToString();
                }
                if (ds.Tables[0].Rows[0]["OriginalEffectiveDate"] != null && ds.Tables[0].Rows[0]["OriginalEffectiveDate"].ToString() != "")
                {
                    model.OriginalEffectiveDate = DateTime.Parse(ds.Tables[0].Rows[0]["OriginalEffectiveDate"].ToString());
                }
                if (ds.Tables[0].Rows[0]["NewEffectiveDate"] != null && ds.Tables[0].Rows[0]["NewEffectiveDate"].ToString() != "")
                {
                    model.NewEffectiveDate = DateTime.Parse(ds.Tables[0].Rows[0]["NewEffectiveDate"].ToString());
                }
                if (ds.Tables[0].Rows[0]["IsPass"] != null && ds.Tables[0].Rows[0]["IsPass"].ToString() != "")
                {
                    model.IsPass = ds.Tables[0].Rows[0]["IsPass"].ToString();
                }
                if (ds.Tables[0].Rows[0]["Remarks"] != null && ds.Tables[0].Rows[0]["Remarks"].ToString() != "")
                {
                    model.Remarks = ds.Tables[0].Rows[0]["Remarks"].ToString();
                }
                if (ds.Tables[0].Rows[0]["Status"] != null && ds.Tables[0].Rows[0]["Status"].ToString() != "")
                {
                    model.Status = ds.Tables[0].Rows[0]["Status"].ToString();
                }
                if (ds.Tables[0].Rows[0]["SendCheckOID"] != null && ds.Tables[0].Rows[0]["SendCheckOID"].ToString() != "")
                {
                    model.SendCheckOID = ds.Tables[0].Rows[0]["SendCheckOID"].ToString();
                }
                if (ds.Tables[0].Rows[0]["SendCheckDate"] != null && ds.Tables[0].Rows[0]["SendCheckDate"].ToString() != "")
                {
                    model.SendCheckDate = DateTime.Parse(ds.Tables[0].Rows[0]["SendCheckDate"].ToString());
                }
                if (ds.Tables[0].Rows[0]["RegistantsOID"] != null && ds.Tables[0].Rows[0]["RegistantsOID"].ToString() != "")
                {
                    model.RegistantsOID = ds.Tables[0].Rows[0]["RegistantsOID"].ToString();
                }
                if (ds.Tables[0].Rows[0]["RegistantsDate"] != null && ds.Tables[0].Rows[0]["RegistantsDate"].ToString() != "")
                {
                    model.RegistantsDate = DateTime.Parse(ds.Tables[0].Rows[0]["RegistantsDate"].ToString());
                }
                if (ds.Tables[0].Rows[0]["CREATED_BY"] != null && ds.Tables[0].Rows[0]["CREATED_BY"].ToString() != "")
                {
                    model.CREATED_BY = ds.Tables[0].Rows[0]["CREATED_BY"].ToString();
                }
                if (ds.Tables[0].Rows[0]["CREATED_DATE"] != null && ds.Tables[0].Rows[0]["CREATED_DATE"].ToString() != "")
                {
                    model.CREATED_DATE = DateTime.Parse(ds.Tables[0].Rows[0]["CREATED_DATE"].ToString());
                }
                if (ds.Tables[0].Rows[0]["LAST_UPD_BY"] != null && ds.Tables[0].Rows[0]["LAST_UPD_BY"].ToString() != "")
                {
                    model.LAST_UPD_BY = ds.Tables[0].Rows[0]["LAST_UPD_BY"].ToString();
                }
                if (ds.Tables[0].Rows[0]["LAST_UPD_DATE"] != null && ds.Tables[0].Rows[0]["LAST_UPD_DATE"].ToString() != "")
                {
                    model.LAST_UPD_DATE = DateTime.Parse(ds.Tables[0].Rows[0]["LAST_UPD_DATE"].ToString());
                }
                return model;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select c.OID,b.CylinderSealNo,b.CylinderCapacity,c.CylinderCode,c.OriginalEffectiveDate,c.NewEffectiveDate,c.IsPass,c.Remarks,c.Status,c.SendCheckOID,c.SendCheckDate,c.RegistantsOID,c.RegistantsDate,c.CREATED_BY,c.CREATED_DATE,c.LAST_UPD_BY,c.LAST_UPD_DATE ");
            strSql.Append(" FROM CylinderInspectTable c LEFT JOIN BookCylinderInfo b ON c.CylinderCode=b.CylinderCode ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" OID,CylinderCode,OriginalEffectiveDate,NewEffectiveDate,IsPass,Remarks,Status,SendCheckOID,SendCheckDate,RegistantsOID,RegistantsDate,CREATED_BY,CREATED_DATE,LAST_UPD_BY,LAST_UPD_DATE ");
            strSql.Append(" FROM CylinderInspectTable ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM CylinderInspectTable ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.OID desc");
            }
            strSql.Append(")AS Row, T.*  from CylinderInspectTable T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", (startIndex - 1) * endIndex + 1, startIndex * endIndex);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /*
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        {
            SqlParameter[] parameters = {
                    new SqlParameter("@tblName", SqlDbType.VarChar, 255),
                    new SqlParameter("@fldName", SqlDbType.VarChar, 255),
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@PageIndex", SqlDbType.Int),
                    new SqlParameter("@IsReCount", SqlDbType.Bit),
                    new SqlParameter("@OrderType", SqlDbType.Bit),
                    new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
                    };
            parameters[0].Value = "CylinderInspectTable";
            parameters[1].Value = "OID";
            parameters[2].Value = PageSize;
            parameters[3].Value = PageIndex;
            parameters[4].Value = 0;
            parameters[5].Value = 0;
            parameters[6].Value = strWhere;	
            return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
        }*/

        #endregion  Method
    }
}


﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Maticsoft.DBUtility;//Please add references
namespace EGMNGS.DAL
{
	/// <summary>
	/// 数据访问类:CylinderCarcaseCode
	/// </summary>
	public partial class CylinderCarcaseCode
	{
		public CylinderCarcaseCode()
		{}
		#region  BasicMethod



		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(EGMNGS.Model.CylinderCarcaseCode model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into CylinderCarcaseCode(");
			strSql.Append("CarcaseCode,PositionCode,PositionName,DepositPosition,DepositPositionName,DepositColumnCount,DepositRowCount,DepositDesc,GasType,GasTypeName,CylinderStatus,CylinderStatusName,CREATED_BY,CREATED_DATE,LAST_UPD_BY,LAST_UPD_DATE)");
			strSql.Append(" values (");
			strSql.Append("@CarcaseCode,@PositionCode,@PositionName,@DepositPosition,@DepositPositionName,@DepositColumnCount,@DepositRowCount,@DepositDesc,@GasType,@GasTypeName,@CylinderStatus,@CylinderStatusName,@CREATED_BY,@CREATED_DATE,@LAST_UPD_BY,@LAST_UPD_DATE)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@CarcaseCode", SqlDbType.VarChar,50),
					new SqlParameter("@PositionCode", SqlDbType.VarChar,50),
					new SqlParameter("@PositionName", SqlDbType.VarChar,50),
					new SqlParameter("@DepositPosition", SqlDbType.VarChar,4),
					new SqlParameter("@DepositPositionName", SqlDbType.VarChar,50),
					new SqlParameter("@DepositColumnCount", SqlDbType.Int,4),
					new SqlParameter("@DepositRowCount", SqlDbType.Int,4),
					new SqlParameter("@DepositDesc", SqlDbType.VarChar,200),
					new SqlParameter("@GasType", SqlDbType.VarChar,4),
					new SqlParameter("@GasTypeName", SqlDbType.VarChar,50),
					new SqlParameter("@CylinderStatus", SqlDbType.VarChar,4),
					new SqlParameter("@CylinderStatusName", SqlDbType.VarChar,50),
					new SqlParameter("@CREATED_BY", SqlDbType.NVarChar,12),
					new SqlParameter("@CREATED_DATE", SqlDbType.Date,3),
					new SqlParameter("@LAST_UPD_BY", SqlDbType.NVarChar,12),
					new SqlParameter("@LAST_UPD_DATE", SqlDbType.Date,3)};
			parameters[0].Value = model.CarcaseCode;
			parameters[1].Value = model.PositionCode;
			parameters[2].Value = model.PositionName;
			parameters[3].Value = model.DepositPosition;
			parameters[4].Value = model.DepositPositionName;
			parameters[5].Value = model.DepositColumnCount;
			parameters[6].Value = model.DepositRowCount;
			parameters[7].Value = model.DepositDesc;
			parameters[8].Value = model.GasType;
			parameters[9].Value = model.GasTypeName;
			parameters[10].Value = model.CylinderStatus;
			parameters[11].Value = model.CylinderStatusName;
			parameters[12].Value = model.CREATED_BY;
			parameters[13].Value = model.CREATED_DATE;
			parameters[14].Value = model.LAST_UPD_BY;
			parameters[15].Value = model.LAST_UPD_DATE;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(EGMNGS.Model.CylinderCarcaseCode model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update CylinderCarcaseCode set ");
			strSql.Append("CarcaseCode=@CarcaseCode,");
			strSql.Append("PositionCode=@PositionCode,");
			strSql.Append("PositionName=@PositionName,");
			strSql.Append("DepositPosition=@DepositPosition,");
			strSql.Append("DepositPositionName=@DepositPositionName,");
			strSql.Append("DepositColumnCount=@DepositColumnCount,");
			strSql.Append("DepositRowCount=@DepositRowCount,");
			strSql.Append("DepositDesc=@DepositDesc,");
			strSql.Append("GasType=@GasType,");
			strSql.Append("GasTypeName=@GasTypeName,");
			strSql.Append("CylinderStatus=@CylinderStatus,");
			strSql.Append("CylinderStatusName=@CylinderStatusName,");
			strSql.Append("CREATED_BY=@CREATED_BY,");
			strSql.Append("CREATED_DATE=@CREATED_DATE,");
			strSql.Append("LAST_UPD_BY=@LAST_UPD_BY,");
			strSql.Append("LAST_UPD_DATE=@LAST_UPD_DATE");
			strSql.Append(" where OID=@OID");
			SqlParameter[] parameters = {
					new SqlParameter("@CarcaseCode", SqlDbType.VarChar,50),
					new SqlParameter("@PositionCode", SqlDbType.VarChar,50),
					new SqlParameter("@PositionName", SqlDbType.VarChar,50),
					new SqlParameter("@DepositPosition", SqlDbType.VarChar,4),
					new SqlParameter("@DepositPositionName", SqlDbType.VarChar,50),
					new SqlParameter("@DepositColumnCount", SqlDbType.Int,4),
					new SqlParameter("@DepositRowCount", SqlDbType.Int,4),
					new SqlParameter("@DepositDesc", SqlDbType.VarChar,200),
					new SqlParameter("@GasType", SqlDbType.VarChar,4),
					new SqlParameter("@GasTypeName", SqlDbType.VarChar,50),
					new SqlParameter("@CylinderStatus", SqlDbType.VarChar,4),
					new SqlParameter("@CylinderStatusName", SqlDbType.VarChar,50),
					new SqlParameter("@CREATED_BY", SqlDbType.NVarChar,12),
					new SqlParameter("@CREATED_DATE", SqlDbType.Date,3),
					new SqlParameter("@LAST_UPD_BY", SqlDbType.NVarChar,12),
					new SqlParameter("@LAST_UPD_DATE", SqlDbType.Date,3),
					new SqlParameter("@OID", SqlDbType.Int,4)};
			parameters[0].Value = model.CarcaseCode;
			parameters[1].Value = model.PositionCode;
			parameters[2].Value = model.PositionName;
			parameters[3].Value = model.DepositPosition;
			parameters[4].Value = model.DepositPositionName;
			parameters[5].Value = model.DepositColumnCount;
			parameters[6].Value = model.DepositRowCount;
			parameters[7].Value = model.DepositDesc;
			parameters[8].Value = model.GasType;
			parameters[9].Value = model.GasTypeName;
			parameters[10].Value = model.CylinderStatus;
			parameters[11].Value = model.CylinderStatusName;
			parameters[12].Value = model.CREATED_BY;
			parameters[13].Value = model.CREATED_DATE;
			parameters[14].Value = model.LAST_UPD_BY;
			parameters[15].Value = model.LAST_UPD_DATE;
			parameters[16].Value = model.OID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int OID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from CylinderCarcaseCode ");
			strSql.Append(" where OID=@OID");
			SqlParameter[] parameters = {
					new SqlParameter("@OID", SqlDbType.Int,4)
			};
			parameters[0].Value = OID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string OIDlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from CylinderCarcaseCode ");
			strSql.Append(" where OID in ("+OIDlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public EGMNGS.Model.CylinderCarcaseCode GetModel(int OID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 OID,CarcaseCode,PositionCode,PositionName,DepositPosition,DepositPositionName,DepositColumnCount,DepositRowCount,DepositDesc,GasType,GasTypeName,CylinderStatus,CylinderStatusName,CREATED_BY,CREATED_DATE,LAST_UPD_BY,LAST_UPD_DATE from CylinderCarcaseCode ");
			strSql.Append(" where OID=@OID");
			SqlParameter[] parameters = {
					new SqlParameter("@OID", SqlDbType.Int,4)
			};
			parameters[0].Value = OID;

			EGMNGS.Model.CylinderCarcaseCode model=new EGMNGS.Model.CylinderCarcaseCode();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public EGMNGS.Model.CylinderCarcaseCode DataRowToModel(DataRow row)
		{
			EGMNGS.Model.CylinderCarcaseCode model=new EGMNGS.Model.CylinderCarcaseCode();
			if (row != null)
			{
				if(row["OID"]!=null && row["OID"].ToString()!="")
				{
					model.OID=int.Parse(row["OID"].ToString());
				}
				if(row["CarcaseCode"]!=null)
				{
					model.CarcaseCode=row["CarcaseCode"].ToString();
				}
				if(row["PositionCode"]!=null)
				{
					model.PositionCode=row["PositionCode"].ToString();
				}
				if(row["PositionName"]!=null)
				{
					model.PositionName=row["PositionName"].ToString();
				}
				if(row["DepositPosition"]!=null)
				{
					model.DepositPosition=row["DepositPosition"].ToString();
				}
				if(row["DepositPositionName"]!=null)
				{
					model.DepositPositionName=row["DepositPositionName"].ToString();
				}
				if(row["DepositColumnCount"]!=null && row["DepositColumnCount"].ToString()!="")
				{
					model.DepositColumnCount=int.Parse(row["DepositColumnCount"].ToString());
				}
				if(row["DepositRowCount"]!=null && row["DepositRowCount"].ToString()!="")
				{
					model.DepositRowCount=int.Parse(row["DepositRowCount"].ToString());
				}
				if(row["DepositDesc"]!=null)
				{
					model.DepositDesc=row["DepositDesc"].ToString();
				}
				if(row["GasType"]!=null)
				{
					model.GasType=row["GasType"].ToString();
				}
				if(row["GasTypeName"]!=null)
				{
					model.GasTypeName=row["GasTypeName"].ToString();
				}
				if(row["CylinderStatus"]!=null)
				{
					model.CylinderStatus=row["CylinderStatus"].ToString();
				}
				if(row["CylinderStatusName"]!=null)
				{
					model.CylinderStatusName=row["CylinderStatusName"].ToString();
				}
				if(row["CREATED_BY"]!=null)
				{
					model.CREATED_BY=row["CREATED_BY"].ToString();
				}
				if(row["CREATED_DATE"]!=null && row["CREATED_DATE"].ToString()!="")
				{
					model.CREATED_DATE=DateTime.Parse(row["CREATED_DATE"].ToString());
				}
				if(row["LAST_UPD_BY"]!=null)
				{
					model.LAST_UPD_BY=row["LAST_UPD_BY"].ToString();
				}
				if(row["LAST_UPD_DATE"]!=null && row["LAST_UPD_DATE"].ToString()!="")
				{
					model.LAST_UPD_DATE=DateTime.Parse(row["LAST_UPD_DATE"].ToString());
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select OID,CarcaseCode,PositionCode,PositionName,DepositPosition,DepositPositionName,DepositColumnCount,DepositRowCount,DepositDesc,GasType,GasTypeName,CylinderStatus,CylinderStatusName,CREATED_BY,CREATED_DATE,LAST_UPD_BY,LAST_UPD_DATE ");
			strSql.Append(" FROM CylinderCarcaseCode ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" OID,CarcaseCode,PositionCode,PositionName,DepositPosition,DepositPositionName,DepositColumnCount,DepositRowCount,DepositDesc,GasType,GasTypeName,CylinderStatus,CylinderStatusName,CREATED_BY,CREATED_DATE,LAST_UPD_BY,LAST_UPD_DATE ");
			strSql.Append(" FROM CylinderCarcaseCode ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM CylinderCarcaseCode ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby + " desc");
			}
			else
			{
				strSql.Append("order by T.OID desc");
			}
			strSql.Append(")AS Row, T.*  from CylinderCarcaseCode T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", (startIndex - 1) * endIndex + 1, startIndex * endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "CylinderCarcaseCode";
			parameters[1].Value = "OID";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}


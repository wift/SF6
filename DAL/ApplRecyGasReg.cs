﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Maticsoft.DBUtility;//Please add references
namespace EGMNGS.DAL
{
    /// <summary>
    /// 数据访问类:ApplRecyGasReg
    /// </summary>
    public partial class ApplRecyGasReg
    {
        public ApplRecyGasReg()
        { }
        #region  Method

        /// <summary>
        /// 得到最大ID
        /// </summary>
        public int GetMaxId()
        {
            return DbHelperSQL.GetMaxID("OID", "ApplRecyGasReg");
        }

        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int OID)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) from ApplRecyGasReg");
            strSql.Append(" where OID=@OID");
            SqlParameter[] parameters = {
					new SqlParameter("@OID", SqlDbType.Int,4)
			};
            parameters[0].Value = OID;

            return DbHelperSQL.Exists(strSql.ToString(), parameters);
        }


        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(EGMNGS.Model.ApplRecyGasReg model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into ApplRecyGasReg(");
            strSql.Append("RecyCode,Year,Month,PowerSupplyCode,PowerSupplyName,ConvertStationCode,ConvertStationName,AmountRecovery,CylinderCount,RecoveryDesc,ContactsName,OfficeTel,PhoneNum,Status,RegistrantOID,RegistrantDate,AuditorOID,AuditsDate,RecyclerOID,RecyDate,CREATED_BY,CREATED_DATE,LAST_UPD_BY,LAST_UPD_DATE)");
            strSql.Append(" values (");
            strSql.Append("@RecyCode,@Year,@Month,@PowerSupplyCode,@PowerSupplyName,@ConvertStationCode,@ConvertStationName,@AmountRecovery,@CylinderCount,@RecoveryDesc,@ContactsName,@OfficeTel,@PhoneNum,@Status,@RegistrantOID,@RegistrantDate,@AuditorOID,@AuditsDate,@RecyclerOID,@RecyDate,@CREATED_BY,@CREATED_DATE,@LAST_UPD_BY,@LAST_UPD_DATE)");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
					new SqlParameter("@RecyCode", SqlDbType.VarChar,20),
					new SqlParameter("@Year", SqlDbType.VarChar,4),
					new SqlParameter("@Month", SqlDbType.VarChar,2),
					new SqlParameter("@PowerSupplyCode", SqlDbType.VarChar,50),
					new SqlParameter("@PowerSupplyName", SqlDbType.VarChar,50),
					new SqlParameter("@ConvertStationCode", SqlDbType.VarChar,50),
					new SqlParameter("@ConvertStationName", SqlDbType.VarChar,50),
					new SqlParameter("@AmountRecovery", SqlDbType.Decimal,9),
					new SqlParameter("@CylinderCount", SqlDbType.Int,4),
					new SqlParameter("@RecoveryDesc", SqlDbType.NVarChar,500),
					new SqlParameter("@ContactsName", SqlDbType.VarChar,50),
					new SqlParameter("@OfficeTel", SqlDbType.VarChar,20),
					new SqlParameter("@PhoneNum", SqlDbType.VarChar,20),
					new SqlParameter("@Status", SqlDbType.VarChar,4),
					new SqlParameter("@RegistrantOID", SqlDbType.VarChar,50),
					new SqlParameter("@RegistrantDate", SqlDbType.Date,3),
					new SqlParameter("@AuditorOID", SqlDbType.VarChar,50),
					new SqlParameter("@AuditsDate", SqlDbType.Date,3),
					new SqlParameter("@RecyclerOID", SqlDbType.VarChar,50),
					new SqlParameter("@RecyDate", SqlDbType.Date,3),
					new SqlParameter("@CREATED_BY", SqlDbType.NVarChar,12),
					new SqlParameter("@CREATED_DATE", SqlDbType.Date,3),
					new SqlParameter("@LAST_UPD_BY", SqlDbType.NVarChar,12),
					new SqlParameter("@LAST_UPD_DATE", SqlDbType.Date,3)};
            parameters[0].Value = model.RecyCode;
            parameters[1].Value = model.Year;
            parameters[2].Value = model.Month;
            parameters[3].Value = model.PowerSupplyCode;
            parameters[4].Value = model.PowerSupplyName;
            parameters[5].Value = model.ConvertStationCode;
            parameters[6].Value = model.ConvertStationName;
            parameters[7].Value = model.AmountRecovery;
            parameters[8].Value = model.CylinderCount;
            parameters[9].Value = model.RecoveryDesc;
            parameters[10].Value = model.ContactsName;
            parameters[11].Value = model.OfficeTel;
            parameters[12].Value = model.PhoneNum;
            parameters[13].Value = model.Status;
            parameters[14].Value = model.RegistrantOID;
            parameters[15].Value = model.RegistrantDate;
            parameters[16].Value = model.AuditorOID;
            parameters[17].Value = model.AuditsDate;
            parameters[18].Value = model.RecyclerOID;
            parameters[19].Value = model.RecyDate;
            parameters[20].Value = model.CREATED_BY;
            parameters[21].Value = model.CREATED_DATE;
            parameters[22].Value = model.LAST_UPD_BY;
            parameters[23].Value = model.LAST_UPD_DATE;

            object obj = DbHelperSQL.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {
                EGMNGS.Common.ComServies.UpdCodeNum(model.RecyCode);
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(EGMNGS.Model.ApplRecyGasReg model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update ApplRecyGasReg set ");
            strSql.Append("RecyCode=@RecyCode,");
            strSql.Append("Year=@Year,");
            strSql.Append("Month=@Month,");
            strSql.Append("PowerSupplyCode=@PowerSupplyCode,");
            strSql.Append("PowerSupplyName=@PowerSupplyName,");
            strSql.Append("ConvertStationCode=@ConvertStationCode,");
            strSql.Append("ConvertStationName=@ConvertStationName,");
            strSql.Append("AmountRecovery=@AmountRecovery,");
            strSql.Append("CylinderCount=@CylinderCount,");
            strSql.Append("RecoveryDesc=@RecoveryDesc,");
            strSql.Append("ContactsName=@ContactsName,");
            strSql.Append("OfficeTel=@OfficeTel,");
            strSql.Append("PhoneNum=@PhoneNum,");
            strSql.Append("Status=@Status,");
            strSql.Append("RegistrantOID=@RegistrantOID,");
            strSql.Append("RegistrantDate=@RegistrantDate,");
            strSql.Append("AuditorOID=@AuditorOID,");
            strSql.Append("AuditsDate=@AuditsDate,");
            strSql.Append("RecyclerOID=@RecyclerOID,");
            strSql.Append("RecyDate=@RecyDate,");
            strSql.Append("CREATED_BY=@CREATED_BY,");
            strSql.Append("CREATED_DATE=@CREATED_DATE,");
            strSql.Append("LAST_UPD_BY=@LAST_UPD_BY,");
            strSql.Append("LAST_UPD_DATE=@LAST_UPD_DATE");
            strSql.Append(" where OID=@OID");
            SqlParameter[] parameters = {
					new SqlParameter("@RecyCode", SqlDbType.VarChar,20),
					new SqlParameter("@Year", SqlDbType.VarChar,4),
					new SqlParameter("@Month", SqlDbType.VarChar,2),
					new SqlParameter("@PowerSupplyCode", SqlDbType.VarChar,50),
					new SqlParameter("@PowerSupplyName", SqlDbType.VarChar,50),
					new SqlParameter("@ConvertStationCode", SqlDbType.VarChar,50),
					new SqlParameter("@ConvertStationName", SqlDbType.VarChar,50),
					new SqlParameter("@AmountRecovery", SqlDbType.Decimal,9),
					new SqlParameter("@CylinderCount", SqlDbType.Int,4),
					new SqlParameter("@RecoveryDesc", SqlDbType.NVarChar,500),
					new SqlParameter("@ContactsName", SqlDbType.VarChar,50),
					new SqlParameter("@OfficeTel", SqlDbType.VarChar,20),
					new SqlParameter("@PhoneNum", SqlDbType.VarChar,20),
					new SqlParameter("@Status", SqlDbType.VarChar,4),
					new SqlParameter("@RegistrantOID", SqlDbType.VarChar,50),
					new SqlParameter("@RegistrantDate", SqlDbType.Date,3),
					new SqlParameter("@AuditorOID", SqlDbType.VarChar,50),
					new SqlParameter("@AuditsDate", SqlDbType.Date,3),
					new SqlParameter("@RecyclerOID", SqlDbType.VarChar,50),
					new SqlParameter("@RecyDate", SqlDbType.Date,3),
					new SqlParameter("@CREATED_BY", SqlDbType.NVarChar,12),
					new SqlParameter("@CREATED_DATE", SqlDbType.Date,3),
					new SqlParameter("@LAST_UPD_BY", SqlDbType.NVarChar,12),
					new SqlParameter("@LAST_UPD_DATE", SqlDbType.Date,3),
					new SqlParameter("@OID", SqlDbType.Int,4)};
            parameters[0].Value = model.RecyCode;
            parameters[1].Value = model.Year;
            parameters[2].Value = model.Month;
            parameters[3].Value = model.PowerSupplyCode;
            parameters[4].Value = model.PowerSupplyName;
            parameters[5].Value = model.ConvertStationCode;
            parameters[6].Value = model.ConvertStationName;
            parameters[7].Value = model.AmountRecovery;
            parameters[8].Value = model.CylinderCount;
            parameters[9].Value = model.RecoveryDesc;
            parameters[10].Value = model.ContactsName;
            parameters[11].Value = model.OfficeTel;
            parameters[12].Value = model.PhoneNum;
            parameters[13].Value = model.Status;
            parameters[14].Value = model.RegistrantOID;
            parameters[15].Value = model.RegistrantDate;
            parameters[16].Value = model.AuditorOID;
            parameters[17].Value = model.AuditsDate;
            parameters[18].Value = model.RecyclerOID;
            parameters[19].Value = model.RecyDate;
            parameters[20].Value = model.CREATED_BY;
            parameters[21].Value = model.CREATED_DATE;
            parameters[22].Value = model.LAST_UPD_BY;
            parameters[23].Value = model.LAST_UPD_DATE;
            parameters[24].Value = model.OID;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int OID)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from ApplRecyGasReg ");
            strSql.Append(" where OID=@OID");
            SqlParameter[] parameters = {
					new SqlParameter("@OID", SqlDbType.Int,4)
			};
            parameters[0].Value = OID;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string OIDlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from ApplRecyGasReg ");
            strSql.Append(" where OID in (" + OIDlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public EGMNGS.Model.ApplRecyGasReg GetModel(int OID)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 OID,RecyCode,Year,Month,PowerSupplyCode,PowerSupplyName,ConvertStationCode,ConvertStationName,AmountRecovery,CylinderCount,RecoveryDesc,ContactsName,OfficeTel,PhoneNum,Status,RegistrantOID,RegistrantDate,AuditorOID,AuditsDate,RecyclerOID,RecyDate,CREATED_BY,CREATED_DATE,LAST_UPD_BY,LAST_UPD_DATE from ApplRecyGasReg ");
            strSql.Append(" where OID=@OID");
            SqlParameter[] parameters = {
					new SqlParameter("@OID", SqlDbType.Int,4)
			};
            parameters[0].Value = OID;

            EGMNGS.Model.ApplRecyGasReg model = new EGMNGS.Model.ApplRecyGasReg();
            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0]["OID"] != null && ds.Tables[0].Rows[0]["OID"].ToString() != "")
                {
                    model.OID = int.Parse(ds.Tables[0].Rows[0]["OID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["RecyCode"] != null && ds.Tables[0].Rows[0]["RecyCode"].ToString() != "")
                {
                    model.RecyCode = ds.Tables[0].Rows[0]["RecyCode"].ToString();
                }
                if (ds.Tables[0].Rows[0]["Year"] != null && ds.Tables[0].Rows[0]["Year"].ToString() != "")
                {
                    model.Year = ds.Tables[0].Rows[0]["Year"].ToString();
                }
                if (ds.Tables[0].Rows[0]["Month"] != null && ds.Tables[0].Rows[0]["Month"].ToString() != "")
                {
                    model.Month = ds.Tables[0].Rows[0]["Month"].ToString();
                }
                if (ds.Tables[0].Rows[0]["PowerSupplyCode"] != null && ds.Tables[0].Rows[0]["PowerSupplyCode"].ToString() != "")
                {
                    model.PowerSupplyCode = ds.Tables[0].Rows[0]["PowerSupplyCode"].ToString();
                }
                if (ds.Tables[0].Rows[0]["PowerSupplyName"] != null && ds.Tables[0].Rows[0]["PowerSupplyName"].ToString() != "")
                {
                    model.PowerSupplyName = ds.Tables[0].Rows[0]["PowerSupplyName"].ToString();
                }
                if (ds.Tables[0].Rows[0]["ConvertStationCode"] != null && ds.Tables[0].Rows[0]["ConvertStationCode"].ToString() != "")
                {
                    model.ConvertStationCode = ds.Tables[0].Rows[0]["ConvertStationCode"].ToString();
                }
                if (ds.Tables[0].Rows[0]["ConvertStationName"] != null && ds.Tables[0].Rows[0]["ConvertStationName"].ToString() != "")
                {
                    model.ConvertStationName = ds.Tables[0].Rows[0]["ConvertStationName"].ToString();
                }
                if (ds.Tables[0].Rows[0]["AmountRecovery"] != null && ds.Tables[0].Rows[0]["AmountRecovery"].ToString() != "")
                {
                    model.AmountRecovery = decimal.Parse(ds.Tables[0].Rows[0]["AmountRecovery"].ToString());
                }
                if (ds.Tables[0].Rows[0]["CylinderCount"] != null && ds.Tables[0].Rows[0]["CylinderCount"].ToString() != "")
                {
                    model.CylinderCount = int.Parse(ds.Tables[0].Rows[0]["CylinderCount"].ToString());
                }
                if (ds.Tables[0].Rows[0]["RecoveryDesc"] != null && ds.Tables[0].Rows[0]["RecoveryDesc"].ToString() != "")
                {
                    model.RecoveryDesc = ds.Tables[0].Rows[0]["RecoveryDesc"].ToString();
                }
                if (ds.Tables[0].Rows[0]["ContactsName"] != null && ds.Tables[0].Rows[0]["ContactsName"].ToString() != "")
                {
                    model.ContactsName = ds.Tables[0].Rows[0]["ContactsName"].ToString();
                }
                if (ds.Tables[0].Rows[0]["OfficeTel"] != null && ds.Tables[0].Rows[0]["OfficeTel"].ToString() != "")
                {
                    model.OfficeTel = ds.Tables[0].Rows[0]["OfficeTel"].ToString();
                }
                if (ds.Tables[0].Rows[0]["PhoneNum"] != null && ds.Tables[0].Rows[0]["PhoneNum"].ToString() != "")
                {
                    model.PhoneNum = ds.Tables[0].Rows[0]["PhoneNum"].ToString();
                }
                if (ds.Tables[0].Rows[0]["Status"] != null && ds.Tables[0].Rows[0]["Status"].ToString() != "")
                {
                    model.Status = ds.Tables[0].Rows[0]["Status"].ToString();
                }
                if (ds.Tables[0].Rows[0]["RegistrantOID"] != null && ds.Tables[0].Rows[0]["RegistrantOID"].ToString() != "")
                {
                    model.RegistrantOID = ds.Tables[0].Rows[0]["RegistrantOID"].ToString();
                }
                if (ds.Tables[0].Rows[0]["RegistrantDate"] != null && ds.Tables[0].Rows[0]["RegistrantDate"].ToString() != "")
                {
                    model.RegistrantDate = DateTime.Parse(ds.Tables[0].Rows[0]["RegistrantDate"].ToString());
                }
                if (ds.Tables[0].Rows[0]["AuditorOID"] != null && ds.Tables[0].Rows[0]["AuditorOID"].ToString() != "")
                {
                    model.AuditorOID = ds.Tables[0].Rows[0]["AuditorOID"].ToString();
                }
                if (ds.Tables[0].Rows[0]["AuditsDate"] != null && ds.Tables[0].Rows[0]["AuditsDate"].ToString() != "")
                {
                    model.AuditsDate = DateTime.Parse(ds.Tables[0].Rows[0]["AuditsDate"].ToString());
                }
                if (ds.Tables[0].Rows[0]["RecyclerOID"] != null && ds.Tables[0].Rows[0]["RecyclerOID"].ToString() != "")
                {
                    model.RecyclerOID = ds.Tables[0].Rows[0]["RecyclerOID"].ToString();
                }
                if (ds.Tables[0].Rows[0]["RecyDate"] != null && ds.Tables[0].Rows[0]["RecyDate"].ToString() != "")
                {
                    model.RecyDate = DateTime.Parse(ds.Tables[0].Rows[0]["RecyDate"].ToString());
                }
                if (ds.Tables[0].Rows[0]["CREATED_BY"] != null && ds.Tables[0].Rows[0]["CREATED_BY"].ToString() != "")
                {
                    model.CREATED_BY = ds.Tables[0].Rows[0]["CREATED_BY"].ToString();
                }
                if (ds.Tables[0].Rows[0]["CREATED_DATE"] != null && ds.Tables[0].Rows[0]["CREATED_DATE"].ToString() != "")
                {
                    model.CREATED_DATE = DateTime.Parse(ds.Tables[0].Rows[0]["CREATED_DATE"].ToString());
                }
                if (ds.Tables[0].Rows[0]["LAST_UPD_BY"] != null && ds.Tables[0].Rows[0]["LAST_UPD_BY"].ToString() != "")
                {
                    model.LAST_UPD_BY = ds.Tables[0].Rows[0]["LAST_UPD_BY"].ToString();
                }
                if (ds.Tables[0].Rows[0]["LAST_UPD_DATE"] != null && ds.Tables[0].Rows[0]["LAST_UPD_DATE"].ToString() != "")
                {
                    model.LAST_UPD_DATE = DateTime.Parse(ds.Tables[0].Rows[0]["LAST_UPD_DATE"].ToString());
                }
                return model;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select OID,RecyCode,Year,Month,PowerSupplyCode,PowerSupplyName,ConvertStationCode,ConvertStationName,AmountRecovery,CylinderCount,RecoveryDesc,ContactsName,OfficeTel,PhoneNum,Status,RegistrantOID,RegistrantDate,AuditorOID,AuditsDate,RecyclerOID,RecyDate,CREATED_BY,CREATED_DATE,LAST_UPD_BY,LAST_UPD_DATE ");
            strSql.Append(" FROM ApplRecyGasReg ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" OID,RecyCode,Year,Month,PowerSupplyCode,PowerSupplyName,ConvertStationCode,ConvertStationName,AmountRecovery,CylinderCount,RecoveryDesc,ContactsName,OfficeTel,PhoneNum,Status,RegistrantOID,RegistrantDate,AuditorOID,AuditsDate,RecyclerOID,RecyDate,CREATED_BY,CREATED_DATE,LAST_UPD_BY,LAST_UPD_DATE ");
            strSql.Append(" FROM ApplRecyGasReg ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM ApplRecyGasReg ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.OID desc");
            }
            strSql.Append(")AS Row, T.*  from ApplRecyGasReg T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", (startIndex - 1) * endIndex + 1, startIndex * endIndex);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /*
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        {
            SqlParameter[] parameters = {
                    new SqlParameter("@tblName", SqlDbType.VarChar, 255),
                    new SqlParameter("@fldName", SqlDbType.VarChar, 255),
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@PageIndex", SqlDbType.Int),
                    new SqlParameter("@IsReCount", SqlDbType.Bit),
                    new SqlParameter("@OrderType", SqlDbType.Bit),
                    new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
                    };
            parameters[0].Value = "ApplRecyGasReg";
            parameters[1].Value = "OID";
            parameters[2].Value = PageSize;
            parameters[3].Value = PageIndex;
            parameters[4].Value = 0;
            parameters[5].Value = 0;
            parameters[6].Value = strWhere;	
            return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
        }*/

        #endregion  Method
    }
}


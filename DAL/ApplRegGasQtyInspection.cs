﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Maticsoft.DBUtility;//Please add references
namespace EGMNGS.DAL
{
    /// <summary>
    /// 数据访问类:ApplRegGasQtyInspection
    /// </summary>
    public partial class ApplRegGasQtyInspection
    {
        public ApplRegGasQtyInspection()
        { }
        #region  Method

        /// <summary>
        /// 得到最大ID
        /// </summary>
        public int GetMaxId()
        {
            return DbHelperSQL.GetMaxID("OID", "ApplRegGasQtyInspection");
        }

        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int OID)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) from ApplRegGasQtyInspection");
            strSql.Append(" where OID=@OID");
            SqlParameter[] parameters = {
					new SqlParameter("@OID", SqlDbType.Int,4)
			};
            parameters[0].Value = OID;

            return DbHelperSQL.Exists(strSql.ToString(), parameters);
        }


        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(EGMNGS.Model.ApplRegGasQtyInspection model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into ApplRegGasQtyInspection(");
            strSql.Append("CheckCode,ReportCode,BusinessCode,GasCdoe,CylinderCode,GasSourse,C,RH,KPA,SPYID,LDYID,WSJID,QYJID,SDJID,FGGDJID,HWHYFXYID,SF6Index,SF6Result,AirQualityScoreIndex,AirQualityScoreResult,CF4Index,CF4Result,WaterQualityScoreIndex,WaterQualityScoreResult,WaterDewIndex,WaterDewResult,HFIndex,HFResult,KSJHFIndex,KSJHFResult,KWYSorceIndex,KWYSorceResult,Conclusion,IsPass,ApplOID,AppLDate,CheckOID,CheckDate,SignerOID,SignerDate,ApproverOID,ApproverDate,FlowStatus,IsPrint,MTID,CheckPJType,Monoxide,hydrothion,SulfurDioxide,C2F6,C3F8)");
            strSql.Append(" values (");
            strSql.Append("@CheckCode,@ReportCode,@BusinessCode,@GasCdoe,@CylinderCode,@GasSourse,@C,@RH,@KPA,@SPYID,@LDYID,@WSJID,@QYJID,@SDJID,@FGGDJID,@HWHYFXYID,@SF6Index,@SF6Result,@AirQualityScoreIndex,@AirQualityScoreResult,@CF4Index,@CF4Result,@WaterQualityScoreIndex,@WaterQualityScoreResult,@WaterDewIndex,@WaterDewResult,@HFIndex,@HFResult,@KSJHFIndex,@KSJHFResult,@KWYSorceIndex,@KWYSorceResult,@Conclusion,@IsPass,@ApplOID,@AppLDate,@CheckOID,@CheckDate,@SignerOID,@SignerDate,@ApproverOID,@ApproverDate,@FlowStatus,@IsPrint,@MTID,@CheckPJType,@Monoxide,@hydrothion,@SulfurDioxide,@C2F6,@C3F8)");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
					new SqlParameter("@CheckCode", SqlDbType.VarChar,20),
					new SqlParameter("@ReportCode", SqlDbType.VarChar,20),
					new SqlParameter("@BusinessCode", SqlDbType.VarChar,20),
					new SqlParameter("@GasCdoe", SqlDbType.VarChar,20),
					new SqlParameter("@CylinderCode", SqlDbType.VarChar,20),
					new SqlParameter("@GasSourse", SqlDbType.VarChar,4),
					new SqlParameter("@C", SqlDbType.Decimal,9),
					new SqlParameter("@RH", SqlDbType.Decimal,9),
					new SqlParameter("@KPA", SqlDbType.Decimal,9),
					new SqlParameter("@SPYID", SqlDbType.VarChar,20),
					new SqlParameter("@LDYID", SqlDbType.VarChar,20),
					new SqlParameter("@WSJID", SqlDbType.VarChar,20),
					new SqlParameter("@QYJID", SqlDbType.VarChar,20),
					new SqlParameter("@SDJID", SqlDbType.VarChar,20),
					new SqlParameter("@FGGDJID", SqlDbType.VarChar,20),
					new SqlParameter("@HWHYFXYID", SqlDbType.VarChar,20),
					new SqlParameter("@SF6Index", SqlDbType.VarChar,50),
					new SqlParameter("@SF6Result", SqlDbType.Decimal,9),
					new SqlParameter("@AirQualityScoreIndex", SqlDbType.VarChar,50),
					new SqlParameter("@AirQualityScoreResult", SqlDbType.Decimal,9),
					new SqlParameter("@CF4Index", SqlDbType.VarChar,50),
					new SqlParameter("@CF4Result", SqlDbType.Decimal,9),
					new SqlParameter("@WaterQualityScoreIndex", SqlDbType.VarChar,50),
					new SqlParameter("@WaterQualityScoreResult", SqlDbType.Decimal,9),
					new SqlParameter("@WaterDewIndex", SqlDbType.VarChar,50),
					new SqlParameter("@WaterDewResult", SqlDbType.Decimal,9),
					new SqlParameter("@HFIndex", SqlDbType.VarChar,50),
					new SqlParameter("@HFResult", SqlDbType.Decimal,9),
					new SqlParameter("@KSJHFIndex", SqlDbType.VarChar,50),
					new SqlParameter("@KSJHFResult", SqlDbType.Decimal,9),
					new SqlParameter("@KWYSorceIndex", SqlDbType.VarChar,50),
					new SqlParameter("@KWYSorceResult", SqlDbType.Decimal,9),
					new SqlParameter("@Conclusion", SqlDbType.NVarChar,500),
					new SqlParameter("@IsPass", SqlDbType.VarChar,4),
					new SqlParameter("@ApplOID", SqlDbType.VarChar,50),
					new SqlParameter("@AppLDate", SqlDbType.Date,3),
					new SqlParameter("@CheckOID", SqlDbType.VarChar,50),
					new SqlParameter("@CheckDate", SqlDbType.Date,3),
					new SqlParameter("@SignerOID", SqlDbType.VarChar,50),
					new SqlParameter("@SignerDate", SqlDbType.Date,3),
					new SqlParameter("@ApproverOID", SqlDbType.VarChar,50),
					new SqlParameter("@ApproverDate", SqlDbType.Date,3),
					new SqlParameter("@FlowStatus", SqlDbType.VarChar,4),
					new SqlParameter("@IsPrint", SqlDbType.Bit,1),
					new SqlParameter("@MTID", SqlDbType.VarChar,20),
                    new SqlParameter("@CheckPJType", SqlDbType.VarChar,4),
                    new SqlParameter("@Monoxide", SqlDbType.Decimal,9),
					new SqlParameter("@hydrothion", SqlDbType.Decimal,9),
                    new SqlParameter("@SulfurDioxide", SqlDbType.Decimal,9),
                    new SqlParameter("@C2F6", SqlDbType.Decimal,9),
                    new SqlParameter("@C3F8", SqlDbType.Decimal,9)
                                        };
            parameters[0].Value = model.CheckCode;
            parameters[1].Value = model.ReportCode;
            parameters[2].Value = model.BusinessCode;
            parameters[3].Value = model.GasCdoe;
            parameters[4].Value = model.CylinderCode;
            parameters[5].Value = model.GasSourse;
            parameters[6].Value = model.C;
            parameters[7].Value = model.RH;
            parameters[8].Value = model.KPA;
            parameters[9].Value = model.SPYID;
            parameters[10].Value = model.LDYID;
            parameters[11].Value = model.WSJID;
            parameters[12].Value = model.QYJID;
            parameters[13].Value = model.SDJID;
            parameters[14].Value = model.FGGDJID;
            parameters[15].Value = model.HWHYFXYID;
            parameters[16].Value = model.SF6Index;
            parameters[17].Value = model.SF6Result;
            parameters[18].Value = model.AirQualityScoreIndex;
            parameters[19].Value = model.AirQualityScoreResult;
            parameters[20].Value = model.CF4Index;
            parameters[21].Value = model.CF4Result;
            parameters[22].Value = model.WaterQualityScoreIndex;
            parameters[23].Value = model.WaterQualityScoreResult;
            parameters[24].Value = model.WaterDewIndex;
            parameters[25].Value = model.WaterDewResult;
            parameters[26].Value = model.HFIndex;
            parameters[27].Value = model.HFResult;
            parameters[28].Value = model.KSJHFIndex;
            parameters[29].Value = model.KSJHFResult;
            parameters[30].Value = model.KWYSorceIndex;
            parameters[31].Value = model.KWYSorceResult;
            parameters[32].Value = model.Conclusion;
            parameters[33].Value = model.IsPass;
            parameters[34].Value = model.ApplOID;
            parameters[35].Value = model.AppLDate;
            parameters[36].Value = model.CheckOID;
            parameters[37].Value = model.CheckDate;
            parameters[38].Value = model.SignerOID;
            parameters[39].Value = model.SignerDate;
            parameters[40].Value = model.ApproverOID;
            parameters[41].Value = model.ApproverDate;
            parameters[42].Value = model.FlowStatus;
            parameters[43].Value = model.IsPrint;
            parameters[44].Value = model.MTID;
            parameters[45].Value = model.CheckPJType;
            parameters[46].Value = model.Monoxide;
            parameters[47].Value = model.Hydrothion;
            parameters[48].Value = model.SulfurDioxide;
            parameters[49].Value = model.C2F6;
            parameters[50].Value = model.C3F8;
            object obj = DbHelperSQL.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {
                EGMNGS.Common.ComServies.UpdCodeNum(model.CheckCode);
                EGMNGS.Common.ComServies.UpdCodeReportNum(model.ReportCode.Substring(4, 1));
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(EGMNGS.Model.ApplRegGasQtyInspection model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update ApplRegGasQtyInspection set ");
            strSql.Append("CheckCode=@CheckCode,");
            strSql.Append("ReportCode=@ReportCode,");
            strSql.Append("BusinessCode=@BusinessCode,");
            strSql.Append("GasCdoe=@GasCdoe,");
            strSql.Append("CylinderCode=@CylinderCode,");
            strSql.Append("GasSourse=@GasSourse,");
            strSql.Append("C=@C,");
            strSql.Append("RH=@RH,");
            strSql.Append("KPA=@KPA,");
            strSql.Append("SPYID=@SPYID,");
            strSql.Append("LDYID=@LDYID,");
            strSql.Append("WSJID=@WSJID,");
            strSql.Append("QYJID=@QYJID,");
            strSql.Append("SDJID=@SDJID,");
            strSql.Append("FGGDJID=@FGGDJID,");
            strSql.Append("HWHYFXYID=@HWHYFXYID,");
            strSql.Append("SF6Index=@SF6Index,");
            strSql.Append("SF6Result=@SF6Result,");
            strSql.Append("AirQualityScoreIndex=@AirQualityScoreIndex,");
            strSql.Append("AirQualityScoreResult=@AirQualityScoreResult,");
            strSql.Append("CF4Index=@CF4Index,");
            strSql.Append("CF4Result=@CF4Result,");
            strSql.Append("WaterQualityScoreIndex=@WaterQualityScoreIndex,");
            strSql.Append("WaterQualityScoreResult=@WaterQualityScoreResult,");
            strSql.Append("WaterDewIndex=@WaterDewIndex,");
            strSql.Append("WaterDewResult=@WaterDewResult,");
            strSql.Append("HFIndex=@HFIndex,");
            strSql.Append("HFResult=@HFResult,");
            strSql.Append("KSJHFIndex=@KSJHFIndex,");
            strSql.Append("KSJHFResult=@KSJHFResult,");
            strSql.Append("KWYSorceIndex=@KWYSorceIndex,");
            strSql.Append("KWYSorceResult=@KWYSorceResult,");
            strSql.Append("Conclusion=@Conclusion,");
            strSql.Append("IsPass=@IsPass,");
            strSql.Append("ApplOID=@ApplOID,");
            strSql.Append("AppLDate=@AppLDate,");
            strSql.Append("CheckOID=@CheckOID,");
            strSql.Append("CheckDate=@CheckDate,");
            strSql.Append("SignerOID=@SignerOID,");
            strSql.Append("SignerDate=@SignerDate,");
            strSql.Append("ApproverOID=@ApproverOID,");
            strSql.Append("ApproverDate=@ApproverDate,");
            strSql.Append("FlowStatus=@FlowStatus,");
            strSql.Append("IsPrint=@IsPrint,");
            strSql.Append("MTID=@MTID,");
            strSql.Append("CheckPJType=@CheckPJType,");
            strSql.Append("Monoxide=@Monoxide,");
            strSql.Append("hydrothion=@hydrothion,");
            strSql.Append("SulfurDioxide=@SulfurDioxide,");
            strSql.Append("C2F6=@C2F6,");
            strSql.Append("C3F8=@C3F8");
            strSql.Append(" where OID=@OID");

            SqlParameter[] parameters = {
					new SqlParameter("@CheckCode", SqlDbType.VarChar,20),
					new SqlParameter("@ReportCode", SqlDbType.VarChar,20),
					new SqlParameter("@BusinessCode", SqlDbType.VarChar,20),
					new SqlParameter("@GasCdoe", SqlDbType.VarChar,20),
					new SqlParameter("@CylinderCode", SqlDbType.VarChar,20),
					new SqlParameter("@GasSourse", SqlDbType.VarChar,4),
					new SqlParameter("@C", SqlDbType.Decimal,9),
					new SqlParameter("@RH", SqlDbType.Decimal,9),
					new SqlParameter("@KPA", SqlDbType.Decimal,9),
					new SqlParameter("@SPYID", SqlDbType.VarChar,20),
					new SqlParameter("@LDYID", SqlDbType.VarChar,20),
					new SqlParameter("@WSJID", SqlDbType.VarChar,20),
					new SqlParameter("@QYJID", SqlDbType.VarChar,20),
					new SqlParameter("@SDJID", SqlDbType.VarChar,20),
					new SqlParameter("@FGGDJID", SqlDbType.VarChar,20),
					new SqlParameter("@HWHYFXYID", SqlDbType.VarChar,20),
					new SqlParameter("@SF6Index", SqlDbType.VarChar,50),
					new SqlParameter("@SF6Result", SqlDbType.Decimal,9),
					new SqlParameter("@AirQualityScoreIndex", SqlDbType.VarChar,50),
					new SqlParameter("@AirQualityScoreResult", SqlDbType.Decimal,9),
					new SqlParameter("@CF4Index", SqlDbType.VarChar,50),
					new SqlParameter("@CF4Result", SqlDbType.Decimal,9),
					new SqlParameter("@WaterQualityScoreIndex", SqlDbType.VarChar,50),
					new SqlParameter("@WaterQualityScoreResult", SqlDbType.Decimal,9),
					new SqlParameter("@WaterDewIndex", SqlDbType.VarChar,50),
					new SqlParameter("@WaterDewResult", SqlDbType.Decimal,9),
					new SqlParameter("@HFIndex", SqlDbType.VarChar,50),
					new SqlParameter("@HFResult", SqlDbType.Decimal,9),
					new SqlParameter("@KSJHFIndex", SqlDbType.VarChar,50),
					new SqlParameter("@KSJHFResult", SqlDbType.Decimal,9),
					new SqlParameter("@KWYSorceIndex", SqlDbType.VarChar,50),
					new SqlParameter("@KWYSorceResult", SqlDbType.Decimal,9),
					new SqlParameter("@Conclusion", SqlDbType.NVarChar,500),
					new SqlParameter("@IsPass", SqlDbType.VarChar,4),
					new SqlParameter("@ApplOID", SqlDbType.VarChar,50),
					new SqlParameter("@AppLDate", SqlDbType.Date,3),
					new SqlParameter("@CheckOID", SqlDbType.VarChar,50),
					new SqlParameter("@CheckDate", SqlDbType.Date,3),
					new SqlParameter("@SignerOID", SqlDbType.VarChar,50),
					new SqlParameter("@SignerDate", SqlDbType.Date,3),
					new SqlParameter("@ApproverOID", SqlDbType.VarChar,50),
					new SqlParameter("@ApproverDate", SqlDbType.Date,3),
					new SqlParameter("@FlowStatus", SqlDbType.VarChar,4),
					new SqlParameter("@IsPrint", SqlDbType.Bit,1),
					new SqlParameter("@MTID", SqlDbType.VarChar,20),
                    new SqlParameter("@CheckPJType", SqlDbType.VarChar,4),
					new SqlParameter("@OID", SqlDbType.Int,4),
                    new SqlParameter("@Monoxide", SqlDbType.Decimal,9),
					new SqlParameter("@hydrothion", SqlDbType.Decimal,9),
                    new SqlParameter("@SulfurDioxide", SqlDbType.Decimal,9),
                    new SqlParameter("@C2F6", SqlDbType.Decimal,9),
                    new SqlParameter("@C3F8", SqlDbType.Decimal,9)};
            parameters[0].Value = model.CheckCode;
            parameters[1].Value = model.ReportCode;
            parameters[2].Value = model.BusinessCode;
            parameters[3].Value = model.GasCdoe;
            parameters[4].Value = model.CylinderCode;
            parameters[5].Value = model.GasSourse;
            parameters[6].Value = model.C;
            parameters[7].Value = model.RH;
            parameters[8].Value = model.KPA;
            parameters[9].Value = model.SPYID;
            parameters[10].Value = model.LDYID;
            parameters[11].Value = model.WSJID;
            parameters[12].Value = model.QYJID;
            parameters[13].Value = model.SDJID;
            parameters[14].Value = model.FGGDJID;
            parameters[15].Value = model.HWHYFXYID;
            parameters[16].Value = model.SF6Index;
            parameters[17].Value = model.SF6Result;
            parameters[18].Value = model.AirQualityScoreIndex;
            parameters[19].Value = model.AirQualityScoreResult;
            parameters[20].Value = model.CF4Index;
            parameters[21].Value = model.CF4Result;
            parameters[22].Value = model.WaterQualityScoreIndex;
            parameters[23].Value = model.WaterQualityScoreResult;
            parameters[24].Value = model.WaterDewIndex;
            parameters[25].Value = model.WaterDewResult;
            parameters[26].Value = model.HFIndex;
            parameters[27].Value = model.HFResult;
            parameters[28].Value = model.KSJHFIndex;
            parameters[29].Value = model.KSJHFResult;
            parameters[30].Value = model.KWYSorceIndex;
            parameters[31].Value = model.KWYSorceResult;
            parameters[32].Value = model.Conclusion;
            parameters[33].Value = model.IsPass;
            parameters[34].Value = model.ApplOID;
            parameters[35].Value = model.AppLDate;
            parameters[36].Value = model.CheckOID;
            parameters[37].Value = model.CheckDate;
            parameters[38].Value = model.SignerOID;
            parameters[39].Value = model.SignerDate;
            parameters[40].Value = model.ApproverOID;
            parameters[41].Value = model.ApproverDate;
            parameters[42].Value = model.FlowStatus;
            parameters[43].Value = model.IsPrint;
            parameters[44].Value = model.MTID;
            parameters[45].Value = model.CheckPJType;
            parameters[46].Value = model.OID;
            parameters[47].Value = model.Monoxide;
            parameters[48].Value = model.Hydrothion;
            parameters[49].Value = model.SulfurDioxide;
            parameters[50].Value = model.C2F6;
            parameters[51].Value = model.C3F8;
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int OID)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from ApplRegGasQtyInspection ");
            strSql.Append(" where OID=@OID");
            SqlParameter[] parameters = {
					new SqlParameter("@OID", SqlDbType.Int,4)
			};
            parameters[0].Value = OID;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string OIDlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from ApplRegGasQtyInspection ");
            strSql.Append(" where OID in (" + OIDlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public EGMNGS.Model.ApplRegGasQtyInspection GetModel(int OID)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 * from ApplRegGasQtyInspection ");
            strSql.Append(" where OID=@OID");
            SqlParameter[] parameters = {
					new SqlParameter("@OID", SqlDbType.Int,4)
			};
            parameters[0].Value = OID;

            EGMNGS.Model.ApplRegGasQtyInspection model = new EGMNGS.Model.ApplRegGasQtyInspection();
            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0]["OID"] != null && ds.Tables[0].Rows[0]["OID"].ToString() != "")
                {
                    model.OID = int.Parse(ds.Tables[0].Rows[0]["OID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["CheckCode"] != null && ds.Tables[0].Rows[0]["CheckCode"].ToString() != "")
                {
                    model.CheckCode = ds.Tables[0].Rows[0]["CheckCode"].ToString();
                }
                if (ds.Tables[0].Rows[0]["ReportCode"] != null && ds.Tables[0].Rows[0]["ReportCode"].ToString() != "")
                {
                    model.ReportCode = ds.Tables[0].Rows[0]["ReportCode"].ToString();
                }
                if (ds.Tables[0].Rows[0]["BusinessCode"] != null && ds.Tables[0].Rows[0]["BusinessCode"].ToString() != "")
                {
                    model.BusinessCode = ds.Tables[0].Rows[0]["BusinessCode"].ToString();
                }
                if (ds.Tables[0].Rows[0]["GasCdoe"] != null && ds.Tables[0].Rows[0]["GasCdoe"].ToString() != "")
                {
                    model.GasCdoe = ds.Tables[0].Rows[0]["GasCdoe"].ToString();
                }
                if (ds.Tables[0].Rows[0]["CylinderCode"] != null && ds.Tables[0].Rows[0]["CylinderCode"].ToString() != "")
                {
                    model.CylinderCode = ds.Tables[0].Rows[0]["CylinderCode"].ToString();
                }
                if (ds.Tables[0].Rows[0]["GasSourse"] != null && ds.Tables[0].Rows[0]["GasSourse"].ToString() != "")
                {
                    model.GasSourse = ds.Tables[0].Rows[0]["GasSourse"].ToString();
                }
                if (ds.Tables[0].Rows[0]["C"] != null && ds.Tables[0].Rows[0]["C"].ToString() != "")
                {
                    model.C = decimal.Parse(ds.Tables[0].Rows[0]["C"].ToString());
                }
                if (ds.Tables[0].Rows[0]["RH"] != null && ds.Tables[0].Rows[0]["RH"].ToString() != "")
                {
                    model.RH = decimal.Parse(ds.Tables[0].Rows[0]["RH"].ToString());
                }
                if (ds.Tables[0].Rows[0]["KPA"] != null && ds.Tables[0].Rows[0]["KPA"].ToString() != "")
                {
                    model.KPA = decimal.Parse(ds.Tables[0].Rows[0]["KPA"].ToString());
                }
                if (ds.Tables[0].Rows[0]["SPYID"] != null && ds.Tables[0].Rows[0]["SPYID"].ToString() != "")
                {
                    model.SPYID = ds.Tables[0].Rows[0]["SPYID"].ToString();
                }
                if (ds.Tables[0].Rows[0]["LDYID"] != null && ds.Tables[0].Rows[0]["LDYID"].ToString() != "")
                {
                    model.LDYID = ds.Tables[0].Rows[0]["LDYID"].ToString();
                }
                if (ds.Tables[0].Rows[0]["WSJID"] != null && ds.Tables[0].Rows[0]["WSJID"].ToString() != "")
                {
                    model.WSJID = ds.Tables[0].Rows[0]["WSJID"].ToString();
                }
                if (ds.Tables[0].Rows[0]["QYJID"] != null && ds.Tables[0].Rows[0]["QYJID"].ToString() != "")
                {
                    model.QYJID = ds.Tables[0].Rows[0]["QYJID"].ToString();
                }
                if (ds.Tables[0].Rows[0]["SDJID"] != null && ds.Tables[0].Rows[0]["SDJID"].ToString() != "")
                {
                    model.SDJID = ds.Tables[0].Rows[0]["SDJID"].ToString();
                }
                if (ds.Tables[0].Rows[0]["FGGDJID"] != null && ds.Tables[0].Rows[0]["FGGDJID"].ToString() != "")
                {
                    model.FGGDJID = ds.Tables[0].Rows[0]["FGGDJID"].ToString();
                }
                if (ds.Tables[0].Rows[0]["HWHYFXYID"] != null && ds.Tables[0].Rows[0]["HWHYFXYID"].ToString() != "")
                {
                    model.HWHYFXYID = ds.Tables[0].Rows[0]["HWHYFXYID"].ToString();
                }
                if (ds.Tables[0].Rows[0]["SF6Index"] != null && ds.Tables[0].Rows[0]["SF6Index"].ToString() != "")
                {
                    model.SF6Index = ds.Tables[0].Rows[0]["SF6Index"].ToString();
                }
                if (ds.Tables[0].Rows[0]["SF6Result"] != null && ds.Tables[0].Rows[0]["SF6Result"].ToString() != "")
                {
                    model.SF6Result = decimal.Parse(ds.Tables[0].Rows[0]["SF6Result"].ToString());
                }
                if (ds.Tables[0].Rows[0]["AirQualityScoreIndex"] != null && ds.Tables[0].Rows[0]["AirQualityScoreIndex"].ToString() != "")
                {
                    model.AirQualityScoreIndex = ds.Tables[0].Rows[0]["AirQualityScoreIndex"].ToString();
                }
                if (ds.Tables[0].Rows[0]["AirQualityScoreResult"] != null && ds.Tables[0].Rows[0]["AirQualityScoreResult"].ToString() != "")
                {
                    model.AirQualityScoreResult = decimal.Parse(ds.Tables[0].Rows[0]["AirQualityScoreResult"].ToString());
                }
                if (ds.Tables[0].Rows[0]["CF4Index"] != null && ds.Tables[0].Rows[0]["CF4Index"].ToString() != "")
                {
                    model.CF4Index = ds.Tables[0].Rows[0]["CF4Index"].ToString();
                }
                if (ds.Tables[0].Rows[0]["CF4Result"] != null && ds.Tables[0].Rows[0]["CF4Result"].ToString() != "")
                {
                    model.CF4Result = decimal.Parse(ds.Tables[0].Rows[0]["CF4Result"].ToString());
                }
                if (ds.Tables[0].Rows[0]["WaterQualityScoreIndex"] != null && ds.Tables[0].Rows[0]["WaterQualityScoreIndex"].ToString() != "")
                {
                    model.WaterQualityScoreIndex = ds.Tables[0].Rows[0]["WaterQualityScoreIndex"].ToString();
                }
                if (ds.Tables[0].Rows[0]["WaterQualityScoreResult"] != null && ds.Tables[0].Rows[0]["WaterQualityScoreResult"].ToString() != "")
                {
                    model.WaterQualityScoreResult = decimal.Parse(ds.Tables[0].Rows[0]["WaterQualityScoreResult"].ToString());
                }
                if (ds.Tables[0].Rows[0]["WaterDewIndex"] != null && ds.Tables[0].Rows[0]["WaterDewIndex"].ToString() != "")
                {
                    model.WaterDewIndex = ds.Tables[0].Rows[0]["WaterDewIndex"].ToString();
                }
                if (ds.Tables[0].Rows[0]["WaterDewResult"] != null && ds.Tables[0].Rows[0]["WaterDewResult"].ToString() != "")
                {
                    model.WaterDewResult = decimal.Parse(ds.Tables[0].Rows[0]["WaterDewResult"].ToString());
                }
                if (ds.Tables[0].Rows[0]["HFIndex"] != null && ds.Tables[0].Rows[0]["HFIndex"].ToString() != "")
                {
                    model.HFIndex = ds.Tables[0].Rows[0]["HFIndex"].ToString();
                }
                if (ds.Tables[0].Rows[0]["HFResult"] != null && ds.Tables[0].Rows[0]["HFResult"].ToString() != "")
                {
                    model.HFResult = decimal.Parse(ds.Tables[0].Rows[0]["HFResult"].ToString());
                }
                if (ds.Tables[0].Rows[0]["KSJHFIndex"] != null && ds.Tables[0].Rows[0]["KSJHFIndex"].ToString() != "")
                {
                    model.KSJHFIndex = ds.Tables[0].Rows[0]["KSJHFIndex"].ToString();
                }
                if (ds.Tables[0].Rows[0]["KSJHFResult"] != null && ds.Tables[0].Rows[0]["KSJHFResult"].ToString() != "")
                {
                    model.KSJHFResult = decimal.Parse(ds.Tables[0].Rows[0]["KSJHFResult"].ToString());
                }
                if (ds.Tables[0].Rows[0]["KWYSorceIndex"] != null && ds.Tables[0].Rows[0]["KWYSorceIndex"].ToString() != "")
                {
                    model.KWYSorceIndex = ds.Tables[0].Rows[0]["KWYSorceIndex"].ToString();
                }
                if (ds.Tables[0].Rows[0]["KWYSorceResult"] != null && ds.Tables[0].Rows[0]["KWYSorceResult"].ToString() != "")
                {
                    model.KWYSorceResult = decimal.Parse(ds.Tables[0].Rows[0]["KWYSorceResult"].ToString());
                }
                if (ds.Tables[0].Rows[0]["Conclusion"] != null && ds.Tables[0].Rows[0]["Conclusion"].ToString() != "")
                {
                    model.Conclusion = ds.Tables[0].Rows[0]["Conclusion"].ToString();
                }
                if (ds.Tables[0].Rows[0]["IsPass"] != null && ds.Tables[0].Rows[0]["IsPass"].ToString() != "")
                {
                    model.IsPass = ds.Tables[0].Rows[0]["IsPass"].ToString();
                }
                if (ds.Tables[0].Rows[0]["ApplOID"] != null && ds.Tables[0].Rows[0]["ApplOID"].ToString() != "")
                {
                    model.ApplOID = ds.Tables[0].Rows[0]["ApplOID"].ToString();
                }
                if (ds.Tables[0].Rows[0]["AppLDate"] != null && ds.Tables[0].Rows[0]["AppLDate"].ToString() != "")
                {
                    model.AppLDate = DateTime.Parse(ds.Tables[0].Rows[0]["AppLDate"].ToString());
                }
                if (ds.Tables[0].Rows[0]["CheckOID"] != null && ds.Tables[0].Rows[0]["CheckOID"].ToString() != "")
                {
                    model.CheckOID = ds.Tables[0].Rows[0]["CheckOID"].ToString();
                }
                if (ds.Tables[0].Rows[0]["CheckDate"] != null && ds.Tables[0].Rows[0]["CheckDate"].ToString() != "")
                {
                    model.CheckDate = DateTime.Parse(ds.Tables[0].Rows[0]["CheckDate"].ToString());
                }
                if (ds.Tables[0].Rows[0]["SignerOID"] != null && ds.Tables[0].Rows[0]["SignerOID"].ToString() != "")
                {
                    model.SignerOID = ds.Tables[0].Rows[0]["SignerOID"].ToString();
                }
                if (ds.Tables[0].Rows[0]["SignerDate"] != null && ds.Tables[0].Rows[0]["SignerDate"].ToString() != "")
                {
                    model.SignerDate = DateTime.Parse(ds.Tables[0].Rows[0]["SignerDate"].ToString());
                }
                if (ds.Tables[0].Rows[0]["ApproverOID"] != null && ds.Tables[0].Rows[0]["ApproverOID"].ToString() != "")
                {
                    model.ApproverOID = ds.Tables[0].Rows[0]["ApproverOID"].ToString();
                }
                if (ds.Tables[0].Rows[0]["ApproverDate"] != null && ds.Tables[0].Rows[0]["ApproverDate"].ToString() != "")
                {
                    model.ApproverDate = DateTime.Parse(ds.Tables[0].Rows[0]["ApproverDate"].ToString());
                }
                if (ds.Tables[0].Rows[0]["FlowStatus"] != null && ds.Tables[0].Rows[0]["FlowStatus"].ToString() != "")
                {
                    model.FlowStatus = ds.Tables[0].Rows[0]["FlowStatus"].ToString();
                }
                if (ds.Tables[0].Rows[0]["IsPrint"] != null && ds.Tables[0].Rows[0]["IsPrint"].ToString() != "")
                {
                    if ((ds.Tables[0].Rows[0]["IsPrint"].ToString() == "1") || (ds.Tables[0].Rows[0]["IsPrint"].ToString().ToLower() == "true"))
                    {
                        model.IsPrint = true;
                    }
                    else
                    {
                        model.IsPrint = false;
                    }
                }
                if (ds.Tables[0].Rows[0]["MTID"] != null && ds.Tables[0].Rows[0]["MTID"].ToString() != "")
                {
                    model.MTID = ds.Tables[0].Rows[0]["MTID"].ToString();
                }

                if (ds.Tables[0].Rows[0]["CheckPJType"] != null && ds.Tables[0].Rows[0]["CheckPJType"].ToString() != "")
                {
                    model.CheckPJType = ds.Tables[0].Rows[0]["CheckPJType"].ToString();
                }
                if (ds.Tables[0].Rows[0]["Monoxide"] != null && ds.Tables[0].Rows[0]["Monoxide"].ToString() != "")
                {
                    model.Monoxide = decimal.Parse(ds.Tables[0].Rows[0]["Monoxide"].ToString());
                }
                if (ds.Tables[0].Rows[0]["hydrothion"] != null && ds.Tables[0].Rows[0]["hydrothion"].ToString() != "")
                {
                    model.Hydrothion = decimal.Parse(ds.Tables[0].Rows[0]["hydrothion"].ToString());
                }
                if (ds.Tables[0].Rows[0]["SulfurDioxide"] != null && ds.Tables[0].Rows[0]["SulfurDioxide"].ToString() != "")
                {
                    model.SulfurDioxide = decimal.Parse(ds.Tables[0].Rows[0]["SulfurDioxide"].ToString());
                }
                if (ds.Tables[0].Rows[0]["C3F8"] != null && ds.Tables[0].Rows[0]["C3F8"].ToString() != "")
                {
                    model.C3F8 = decimal.Parse(ds.Tables[0].Rows[0]["C3F8"].ToString());
                }
                if (ds.Tables[0].Rows[0]["C2F6"] != null && ds.Tables[0].Rows[0]["C2F6"].ToString() != "")
                {
                    model.C2F6 = decimal.Parse(ds.Tables[0].Rows[0]["C2F6"].ToString());
                }
                if (ds.Tables[0].Rows[0]["StandardVolume"] != null && ds.Tables[0].Rows[0]["StandardVolume"].ToString() != "")
                {
                    model.StandardVolume = decimal.Parse(ds.Tables[0].Rows[0]["StandardVolume"].ToString());
                }
                return model;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select OID,CheckCode,ReportCode,BusinessCode,GasCdoe,CylinderCode,GasSourse,C,RH,KPA,SPYID,LDYID,WSJID,QYJID,SDJID,FGGDJID,HWHYFXYID,SF6Index,SF6Result,AirQualityScoreIndex,AirQualityScoreResult,CF4Index,CF4Result,WaterQualityScoreIndex,WaterQualityScoreResult,WaterDewIndex,WaterDewResult,HFIndex,HFResult,KSJHFIndex,KSJHFResult,KWYSorceIndex,KWYSorceResult,Conclusion,IsPass,ApplOID,AppLDate,CheckOID,CheckDate,SignerOID,SignerDate,ApproverOID,ApproverDate,FlowStatus,IsPrint,MTID,CheckPJType,Monoxide,hydrothion,SulfurDioxide,C3F8,C2F6");
            strSql.Append(" FROM ApplRegGasQtyInspection ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" OID,CheckCode,ReportCode,BusinessCode,GasCdoe,CylinderCode,GasSourse,C,RH,KPA,SPYID,LDYID,WSJID,QYJID,SDJID,FGGDJID,HWHYFXYID,SF6Index,SF6Result,AirQualityScoreIndex,AirQualityScoreResult,CF4Index,CF4Result,WaterQualityScoreIndex,WaterQualityScoreResult,WaterDewIndex,WaterDewResult,HFIndex,HFResult,KSJHFIndex,KSJHFResult,KWYSorceIndex,KWYSorceResult,Conclusion,IsPass,ApplOID,AppLDate,CheckOID,CheckDate,SignerOID,SignerDate,ApproverOID,ApproverDate,FlowStatus,IsPrint,MTID,CheckPJType,Monoxide,hydrothion,SulfurDioxide,C3F8,C2F6 ");
            strSql.Append(" FROM ApplRegGasQtyInspection ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM ApplRegGasQtyInspection ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT *,(SELECT bci.CylinderSealNo FROM BookCylinderInfo bci WHERE bci.CylinderCode=TT.CylinderCode) as CylinderSealNo FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by case when  T." + orderby);
            }
            else
            {
                strSql.Append("order by T.OID desc");
            }
            strSql.Append(")AS Row, T.*  from ApplRegGasQtyInspection T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", (startIndex - 1) * endIndex + 1, startIndex * endIndex);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /*
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        {
            SqlParameter[] parameters = {
                    new SqlParameter("@tblName", SqlDbType.VarChar, 255),
                    new SqlParameter("@fldName", SqlDbType.VarChar, 255),
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@PageIndex", SqlDbType.Int),
                    new SqlParameter("@IsReCount", SqlDbType.Bit),
                    new SqlParameter("@OrderType", SqlDbType.Bit),
                    new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
                    };
            parameters[0].Value = "ApplRegGasQtyInspection";
            parameters[1].Value = "OID";
            parameters[2].Value = PageSize;
            parameters[3].Value = PageIndex;
            parameters[4].Value = 0;
            parameters[5].Value = 0;
            parameters[6].Value = strWhere;	
            return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
        }*/

        #endregion  Method
    }
}


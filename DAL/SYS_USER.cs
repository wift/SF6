﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Maticsoft.DBUtility;//Please add references
namespace EGMNGS.DAL
{
	/// <summary>
	/// 数据访问类:SYS_USER
	/// </summary>
	public partial class SYS_USER
	{
		public SYS_USER()
		{}
		#region  Method

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(string USER_ID)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from SYS_USER");
			strSql.Append(" where USER_ID=@USER_ID ");
			SqlParameter[] parameters = {
					new SqlParameter("@USER_ID", SqlDbType.VarChar,12)			};
			parameters[0].Value = USER_ID;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(EGMNGS.Model.SYS_USER model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into SYS_USER(");
			strSql.Append("USER_ID,USER_PWD,USER_NAME,EMAIL,POST,USER_STATUS,SUSPEND_REASON,SUSPEND_DATE,LAST_SIGNON,FAIL_COUNT,PWD_RENEWAL_DATE,Org_Code,CREATED_BY,CREATED_DATE,LAST_UPD_BY,LAST_UPD_DATE)");
			strSql.Append(" values (");
			strSql.Append("@USER_ID,@USER_PWD,@USER_NAME,@EMAIL,@POST,@USER_STATUS,@SUSPEND_REASON,@SUSPEND_DATE,@LAST_SIGNON,@FAIL_COUNT,@PWD_RENEWAL_DATE,@Org_Code,@CREATED_BY,@CREATED_DATE,@LAST_UPD_BY,@LAST_UPD_DATE)");
			SqlParameter[] parameters = {
					new SqlParameter("@USER_ID", SqlDbType.VarChar,12),
					new SqlParameter("@USER_PWD", SqlDbType.VarChar,10),
					new SqlParameter("@USER_NAME", SqlDbType.NVarChar,50),
					new SqlParameter("@EMAIL", SqlDbType.VarChar,70),
					new SqlParameter("@POST", SqlDbType.VarChar,20),
					new SqlParameter("@USER_STATUS", SqlDbType.VarChar,4),
					new SqlParameter("@SUSPEND_REASON", SqlDbType.VarChar,3),
					new SqlParameter("@SUSPEND_DATE", SqlDbType.DateTime),
					new SqlParameter("@LAST_SIGNON", SqlDbType.DateTime),
					new SqlParameter("@FAIL_COUNT", SqlDbType.TinyInt,1),
					new SqlParameter("@PWD_RENEWAL_DATE", SqlDbType.DateTime),
					new SqlParameter("@Org_Code", SqlDbType.VarChar,20),
					new SqlParameter("@CREATED_BY", SqlDbType.VarChar,12),
					new SqlParameter("@CREATED_DATE", SqlDbType.DateTime),
					new SqlParameter("@LAST_UPD_BY", SqlDbType.VarChar,12),
					new SqlParameter("@LAST_UPD_DATE", SqlDbType.DateTime)};
			parameters[0].Value = model.USER_ID;
			parameters[1].Value = model.USER_PWD;
			parameters[2].Value = model.USER_NAME;
			parameters[3].Value = model.EMAIL;
			parameters[4].Value = model.POST;
			parameters[5].Value = model.USER_STATUS;
			parameters[6].Value = model.SUSPEND_REASON;
			parameters[7].Value = model.SUSPEND_DATE;
			parameters[8].Value = model.LAST_SIGNON;
			parameters[9].Value = model.FAIL_COUNT;
			parameters[10].Value = model.PWD_RENEWAL_DATE;
			parameters[11].Value = model.Org_Code;
			parameters[12].Value = model.CREATED_BY;
			parameters[13].Value = model.CREATED_DATE;
			parameters[14].Value = model.LAST_UPD_BY;
			parameters[15].Value = model.LAST_UPD_DATE;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(EGMNGS.Model.SYS_USER model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update SYS_USER set ");
			strSql.Append("USER_PWD=@USER_PWD,");
			strSql.Append("USER_NAME=@USER_NAME,");
			strSql.Append("EMAIL=@EMAIL,");
			strSql.Append("POST=@POST,");
			strSql.Append("USER_STATUS=@USER_STATUS,");
			strSql.Append("SUSPEND_REASON=@SUSPEND_REASON,");
			strSql.Append("SUSPEND_DATE=@SUSPEND_DATE,");
			strSql.Append("LAST_SIGNON=@LAST_SIGNON,");
			strSql.Append("FAIL_COUNT=@FAIL_COUNT,");
			strSql.Append("PWD_RENEWAL_DATE=@PWD_RENEWAL_DATE,");
			strSql.Append("Org_Code=@Org_Code,");
			strSql.Append("CREATED_BY=@CREATED_BY,");
			strSql.Append("CREATED_DATE=@CREATED_DATE,");
			strSql.Append("LAST_UPD_BY=@LAST_UPD_BY,");
			strSql.Append("LAST_UPD_DATE=@LAST_UPD_DATE");
			strSql.Append(" where USER_ID=@USER_ID ");
			SqlParameter[] parameters = {
					new SqlParameter("@USER_PWD", SqlDbType.VarChar,10),
					new SqlParameter("@USER_NAME", SqlDbType.NVarChar,50),
					new SqlParameter("@EMAIL", SqlDbType.VarChar,70),
					new SqlParameter("@POST", SqlDbType.VarChar,20),
					new SqlParameter("@USER_STATUS", SqlDbType.VarChar,4),
					new SqlParameter("@SUSPEND_REASON", SqlDbType.VarChar,3),
					new SqlParameter("@SUSPEND_DATE", SqlDbType.DateTime),
					new SqlParameter("@LAST_SIGNON", SqlDbType.DateTime),
					new SqlParameter("@FAIL_COUNT", SqlDbType.TinyInt,1),
					new SqlParameter("@PWD_RENEWAL_DATE", SqlDbType.DateTime),
					new SqlParameter("@Org_Code", SqlDbType.VarChar,20),
					new SqlParameter("@CREATED_BY", SqlDbType.VarChar,12),
					new SqlParameter("@CREATED_DATE", SqlDbType.DateTime),
					new SqlParameter("@LAST_UPD_BY", SqlDbType.VarChar,12),
					new SqlParameter("@LAST_UPD_DATE", SqlDbType.DateTime),
					new SqlParameter("@USER_ID", SqlDbType.VarChar,12)};
			parameters[0].Value = model.USER_PWD;
			parameters[1].Value = model.USER_NAME;
			parameters[2].Value = model.EMAIL;
			parameters[3].Value = model.POST;
			parameters[4].Value = model.USER_STATUS;
			parameters[5].Value = model.SUSPEND_REASON;
			parameters[6].Value = model.SUSPEND_DATE;
			parameters[7].Value = model.LAST_SIGNON;
			parameters[8].Value = model.FAIL_COUNT;
			parameters[9].Value = model.PWD_RENEWAL_DATE;
			parameters[10].Value = model.Org_Code;
			parameters[11].Value = model.CREATED_BY;
			parameters[12].Value = model.CREATED_DATE;
			parameters[13].Value = model.LAST_UPD_BY;
			parameters[14].Value = model.LAST_UPD_DATE;
			parameters[15].Value = model.USER_ID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(string USER_ID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from SYS_USER ");
			strSql.Append(" where USER_ID=@USER_ID ");
			SqlParameter[] parameters = {
					new SqlParameter("@USER_ID", SqlDbType.VarChar,12)			};
			parameters[0].Value = USER_ID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string USER_IDlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from SYS_USER ");
			strSql.Append(" where USER_ID in ("+USER_IDlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public EGMNGS.Model.SYS_USER GetModel(string USER_ID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 USER_ID,USER_PWD,USER_NAME,EMAIL,POST,USER_STATUS,SUSPEND_REASON,SUSPEND_DATE,LAST_SIGNON,FAIL_COUNT,PWD_RENEWAL_DATE,Org_Code,CREATED_BY,CREATED_DATE,LAST_UPD_BY,LAST_UPD_DATE from SYS_USER ");
			strSql.Append(" where USER_ID=@USER_ID ");
			SqlParameter[] parameters = {
					new SqlParameter("@USER_ID", SqlDbType.VarChar,12)			};
			parameters[0].Value = USER_ID;

			EGMNGS.Model.SYS_USER model=new EGMNGS.Model.SYS_USER();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				if(ds.Tables[0].Rows[0]["USER_ID"]!=null && ds.Tables[0].Rows[0]["USER_ID"].ToString()!="")
				{
					model.USER_ID=ds.Tables[0].Rows[0]["USER_ID"].ToString();
				}
				if(ds.Tables[0].Rows[0]["USER_PWD"]!=null && ds.Tables[0].Rows[0]["USER_PWD"].ToString()!="")
				{
					model.USER_PWD=ds.Tables[0].Rows[0]["USER_PWD"].ToString();
				}
				if(ds.Tables[0].Rows[0]["USER_NAME"]!=null && ds.Tables[0].Rows[0]["USER_NAME"].ToString()!="")
				{
					model.USER_NAME=ds.Tables[0].Rows[0]["USER_NAME"].ToString();
				}
				if(ds.Tables[0].Rows[0]["EMAIL"]!=null && ds.Tables[0].Rows[0]["EMAIL"].ToString()!="")
				{
					model.EMAIL=ds.Tables[0].Rows[0]["EMAIL"].ToString();
				}
				if(ds.Tables[0].Rows[0]["POST"]!=null && ds.Tables[0].Rows[0]["POST"].ToString()!="")
				{
					model.POST=ds.Tables[0].Rows[0]["POST"].ToString();
				}
				if(ds.Tables[0].Rows[0]["USER_STATUS"]!=null && ds.Tables[0].Rows[0]["USER_STATUS"].ToString()!="")
				{
					model.USER_STATUS=ds.Tables[0].Rows[0]["USER_STATUS"].ToString();
				}
				if(ds.Tables[0].Rows[0]["SUSPEND_REASON"]!=null && ds.Tables[0].Rows[0]["SUSPEND_REASON"].ToString()!="")
				{
					model.SUSPEND_REASON=ds.Tables[0].Rows[0]["SUSPEND_REASON"].ToString();
				}
				if(ds.Tables[0].Rows[0]["SUSPEND_DATE"]!=null && ds.Tables[0].Rows[0]["SUSPEND_DATE"].ToString()!="")
				{
					model.SUSPEND_DATE=DateTime.Parse(ds.Tables[0].Rows[0]["SUSPEND_DATE"].ToString());
				}
				if(ds.Tables[0].Rows[0]["LAST_SIGNON"]!=null && ds.Tables[0].Rows[0]["LAST_SIGNON"].ToString()!="")
				{
					model.LAST_SIGNON=DateTime.Parse(ds.Tables[0].Rows[0]["LAST_SIGNON"].ToString());
				}
				if(ds.Tables[0].Rows[0]["FAIL_COUNT"]!=null && ds.Tables[0].Rows[0]["FAIL_COUNT"].ToString()!="")
				{
					model.FAIL_COUNT=int.Parse(ds.Tables[0].Rows[0]["FAIL_COUNT"].ToString());
				}
				if(ds.Tables[0].Rows[0]["PWD_RENEWAL_DATE"]!=null && ds.Tables[0].Rows[0]["PWD_RENEWAL_DATE"].ToString()!="")
				{
					model.PWD_RENEWAL_DATE=DateTime.Parse(ds.Tables[0].Rows[0]["PWD_RENEWAL_DATE"].ToString());
				}
				if(ds.Tables[0].Rows[0]["Org_Code"]!=null && ds.Tables[0].Rows[0]["Org_Code"].ToString()!="")
				{
					model.Org_Code=ds.Tables[0].Rows[0]["Org_Code"].ToString();
				}
				if(ds.Tables[0].Rows[0]["CREATED_BY"]!=null && ds.Tables[0].Rows[0]["CREATED_BY"].ToString()!="")
				{
					model.CREATED_BY=ds.Tables[0].Rows[0]["CREATED_BY"].ToString();
				}
				if(ds.Tables[0].Rows[0]["CREATED_DATE"]!=null && ds.Tables[0].Rows[0]["CREATED_DATE"].ToString()!="")
				{
					model.CREATED_DATE=DateTime.Parse(ds.Tables[0].Rows[0]["CREATED_DATE"].ToString());
				}
				if(ds.Tables[0].Rows[0]["LAST_UPD_BY"]!=null && ds.Tables[0].Rows[0]["LAST_UPD_BY"].ToString()!="")
				{
					model.LAST_UPD_BY=ds.Tables[0].Rows[0]["LAST_UPD_BY"].ToString();
				}
				if(ds.Tables[0].Rows[0]["LAST_UPD_DATE"]!=null && ds.Tables[0].Rows[0]["LAST_UPD_DATE"].ToString()!="")
				{
					model.LAST_UPD_DATE=DateTime.Parse(ds.Tables[0].Rows[0]["LAST_UPD_DATE"].ToString());
				}
				return model;
			}
			else
			{
				return null;
			}
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select USER_ID,USER_PWD,USER_NAME,EMAIL,POST,USER_STATUS,SUSPEND_REASON,SUSPEND_DATE,LAST_SIGNON,FAIL_COUNT,PWD_RENEWAL_DATE,Org_Code,CREATED_BY,CREATED_DATE,LAST_UPD_BY,LAST_UPD_DATE ");
			strSql.Append(" FROM SYS_USER ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" USER_ID,USER_PWD,USER_NAME,EMAIL,POST,USER_STATUS,SUSPEND_REASON,SUSPEND_DATE,LAST_SIGNON,FAIL_COUNT,PWD_RENEWAL_DATE,Org_Code,CREATED_BY,CREATED_DATE,LAST_UPD_BY,LAST_UPD_DATE ");
			strSql.Append(" FROM SYS_USER ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM SYS_USER ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.USER_ID desc");
			}
			strSql.Append(")AS Row, T.*  from SYS_USER T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", (startIndex - 1) * endIndex + 1, startIndex * endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "SYS_USER";
			parameters[1].Value = "USER_ID";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  Method
	}
}


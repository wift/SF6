using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Maticsoft.DBUtility;//请先添加引用
namespace EGMNGS.DAL
{
	/// <summary>
	/// 数据访问类ApplRegGasQtyInspectionValue。
	/// </summary>
	public class ApplRegGasQtyInspectionValue
	{
		public ApplRegGasQtyInspectionValue()
		{}
		#region  成员方法

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
		return DbHelperSQL.GetMaxID("OID", "ApplRegGasQtyInspectionValue"); 
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int OID)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from ApplRegGasQtyInspectionValue");
			strSql.Append(" where OID=@OID ");
			SqlParameter[] parameters = {
					new SqlParameter("@OID", SqlDbType.Int,4)};
			parameters[0].Value = OID;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
        public int Add(EGMNGS.Model.ApplRegGasQtyInspectionValue model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into ApplRegGasQtyInspectionValue(");
			strSql.Append("CheckCode,EnvironmentTemperature,EnvironmentHumidity,BarometricPressure,Pipe1,Pipe2,Pipe3,Pipe4,Pipe5,Pipe6,Pipe7,Pipe8)");
			strSql.Append(" values (");
			strSql.Append("@CheckCode,@EnvironmentTemperature,@EnvironmentHumidity,@BarometricPressure,@Pipe1,@Pipe2,@Pipe3,@Pipe4,@Pipe5,@Pipe6,@Pipe7,@Pipe8)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@CheckCode", SqlDbType.VarChar,20),
					new SqlParameter("@EnvironmentTemperature", SqlDbType.Decimal,9),
					new SqlParameter("@EnvironmentHumidity", SqlDbType.Decimal,9),
					new SqlParameter("@BarometricPressure", SqlDbType.Decimal,9),
					new SqlParameter("@Pipe1", SqlDbType.VarChar,20),
					new SqlParameter("@Pipe2", SqlDbType.VarChar,20),
					new SqlParameter("@Pipe3", SqlDbType.VarChar,20),
					new SqlParameter("@Pipe4", SqlDbType.VarChar,20),
					new SqlParameter("@Pipe5", SqlDbType.VarChar,20),
					new SqlParameter("@Pipe6", SqlDbType.VarChar,20),
					new SqlParameter("@Pipe7", SqlDbType.VarChar,20),
					new SqlParameter("@Pipe8", SqlDbType.VarChar,20)};
			parameters[0].Value = model.CheckCode;
			parameters[1].Value = model.EnvironmentTemperature;
			parameters[2].Value = model.EnvironmentHumidity;
			parameters[3].Value = model.BarometricPressure;
			parameters[4].Value = model.Pipe1;
			parameters[5].Value = model.Pipe2;
			parameters[6].Value = model.Pipe3;
			parameters[7].Value = model.Pipe4;
			parameters[8].Value = model.Pipe5;
			parameters[9].Value = model.Pipe6;
			parameters[10].Value = model.Pipe7;
			parameters[11].Value = model.Pipe8;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 1;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
        public void Update(EGMNGS.Model.ApplRegGasQtyInspectionValue model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update ApplRegGasQtyInspectionValue set ");
			strSql.Append("CheckCode=@CheckCode,");
			strSql.Append("EnvironmentTemperature=@EnvironmentTemperature,");
			strSql.Append("EnvironmentHumidity=@EnvironmentHumidity,");
			strSql.Append("BarometricPressure=@BarometricPressure,");
			strSql.Append("Pipe1=@Pipe1,");
			strSql.Append("Pipe2=@Pipe2,");
			strSql.Append("Pipe3=@Pipe3,");
			strSql.Append("Pipe4=@Pipe4,");
			strSql.Append("Pipe5=@Pipe5,");
			strSql.Append("Pipe6=@Pipe6,");
			strSql.Append("Pipe7=@Pipe7,");
			strSql.Append("Pipe8=@Pipe8");
			strSql.Append(" where OID=@OID ");
			SqlParameter[] parameters = {
					new SqlParameter("@OID", SqlDbType.Int,4),
					new SqlParameter("@CheckCode", SqlDbType.VarChar,20),
					new SqlParameter("@EnvironmentTemperature", SqlDbType.Decimal,9),
					new SqlParameter("@EnvironmentHumidity", SqlDbType.Decimal,9),
					new SqlParameter("@BarometricPressure", SqlDbType.Decimal,9),
					new SqlParameter("@Pipe1", SqlDbType.VarChar,20),
					new SqlParameter("@Pipe2", SqlDbType.VarChar,20),
					new SqlParameter("@Pipe3", SqlDbType.VarChar,20),
					new SqlParameter("@Pipe4", SqlDbType.VarChar,20),
					new SqlParameter("@Pipe5", SqlDbType.VarChar,20),
					new SqlParameter("@Pipe6", SqlDbType.VarChar,20),
					new SqlParameter("@Pipe7", SqlDbType.VarChar,20),
					new SqlParameter("@Pipe8", SqlDbType.VarChar,20)};
			parameters[0].Value = model.OID;
			parameters[1].Value = model.CheckCode;
			parameters[2].Value = model.EnvironmentTemperature;
			parameters[3].Value = model.EnvironmentHumidity;
			parameters[4].Value = model.BarometricPressure;
			parameters[5].Value = model.Pipe1;
			parameters[6].Value = model.Pipe2;
			parameters[7].Value = model.Pipe3;
			parameters[8].Value = model.Pipe4;
			parameters[9].Value = model.Pipe5;
			parameters[10].Value = model.Pipe6;
			parameters[11].Value = model.Pipe7;
			parameters[12].Value = model.Pipe8;

			DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public void Delete(int OID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from ApplRegGasQtyInspectionValue ");
			strSql.Append(" where OID=@OID ");
			SqlParameter[] parameters = {
					new SqlParameter("@OID", SqlDbType.Int,4)};
			parameters[0].Value = OID;

			DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public EGMNGS.Model.ApplRegGasQtyInspectionValue GetModel(int OID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 OID,CheckCode,EnvironmentTemperature,EnvironmentHumidity,BarometricPressure,Pipe1,Pipe2,Pipe3,Pipe4,Pipe5,Pipe6,Pipe7,Pipe8 from ApplRegGasQtyInspectionValue ");
			strSql.Append(" where OID=@OID ");
			SqlParameter[] parameters = {
					new SqlParameter("@OID", SqlDbType.Int,4)};
			parameters[0].Value = OID;

			EGMNGS.Model.ApplRegGasQtyInspectionValue model=new EGMNGS.Model.ApplRegGasQtyInspectionValue();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				if(ds.Tables[0].Rows[0]["OID"].ToString()!="")
				{
					model.OID=int.Parse(ds.Tables[0].Rows[0]["OID"].ToString());
				}
				model.CheckCode=ds.Tables[0].Rows[0]["CheckCode"].ToString();
				if(ds.Tables[0].Rows[0]["EnvironmentTemperature"].ToString()!="")
				{
					model.EnvironmentTemperature=decimal.Parse(ds.Tables[0].Rows[0]["EnvironmentTemperature"].ToString());
				}
				if(ds.Tables[0].Rows[0]["EnvironmentHumidity"].ToString()!="")
				{
					model.EnvironmentHumidity=decimal.Parse(ds.Tables[0].Rows[0]["EnvironmentHumidity"].ToString());
				}
				if(ds.Tables[0].Rows[0]["BarometricPressure"].ToString()!="")
				{
					model.BarometricPressure=decimal.Parse(ds.Tables[0].Rows[0]["BarometricPressure"].ToString());
				}
				model.Pipe1=ds.Tables[0].Rows[0]["Pipe1"].ToString();
				model.Pipe2=ds.Tables[0].Rows[0]["Pipe2"].ToString();
				model.Pipe3=ds.Tables[0].Rows[0]["Pipe3"].ToString();
				model.Pipe4=ds.Tables[0].Rows[0]["Pipe4"].ToString();
				model.Pipe5=ds.Tables[0].Rows[0]["Pipe5"].ToString();
				model.Pipe6=ds.Tables[0].Rows[0]["Pipe6"].ToString();
				model.Pipe7=ds.Tables[0].Rows[0]["Pipe7"].ToString();
				model.Pipe8=ds.Tables[0].Rows[0]["Pipe8"].ToString();
				return model;
			}
			else
			{
				return null;
			}
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select OID,CheckCode,EnvironmentTemperature,EnvironmentHumidity,BarometricPressure,Pipe1,Pipe2,Pipe3,Pipe4,Pipe5,Pipe6,Pipe7,Pipe8 ");
			strSql.Append(" FROM ApplRegGasQtyInspectionValue ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" OID,CheckCode,EnvironmentTemperature,EnvironmentHumidity,BarometricPressure,Pipe1,Pipe2,Pipe3,Pipe4,Pipe5,Pipe6,Pipe7,Pipe8 ");
			strSql.Append(" FROM ApplRegGasQtyInspectionValue ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "ApplRegGasQtyInspectionValue";
			parameters[1].Value = "ID";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  成员方法
	}
}


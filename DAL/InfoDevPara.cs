﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Maticsoft.DBUtility;
using EGMNGS.Common;//Please add references
namespace EGMNGS.DAL
{
    /// <summary>
    /// 数据访问类:InfoDevPara
    /// </summary>
    public partial class InfoDevPara
    {
        public InfoDevPara()
        { }
        #region  Method

        /// <summary>
        /// 得到最大ID
        /// </summary>
        public int GetMaxId()
        {
            return DbHelperSQL.GetMaxID("OID", "InfoDevPara");
        }

        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int OID)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) from InfoDevPara");
            strSql.Append(" where OID=@OID");
            SqlParameter[] parameters = {
					new SqlParameter("@OID", SqlDbType.Int,4)
			};
            parameters[0].Value = OID;

            return DbHelperSQL.Exists(strSql.ToString(), parameters);
        }


        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(EGMNGS.Model.InfoDevPara model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into InfoDevPara(");
            strSql.Append("DevCode,PowerSupplyCode,PowerSupplyName,ConvertStationCode,ConvertStationName,VoltageLevel,DevClass,RunCode,DevName,Manufacturer,DevType,DevNum,PurchaseDate,PutDate,ReturnDate,RatedQL,RatedQY,Status,RegistrantOID,RegistrantDate,CREATED_BY,CREATED_DATE,LAST_UPD_BY,LAST_UPD_DATE,KV,A,KA,IsOnlineCheck)");
            strSql.Append(" values (");
            strSql.Append("@DevCode,@PowerSupplyCode,@PowerSupplyName,@ConvertStationCode,@ConvertStationName,@VoltageLevel,@DevClass,@RunCode,@DevName,@Manufacturer,@DevType,@DevNum,@PurchaseDate,@PutDate,@ReturnDate,@RatedQL,@RatedQY,@Status,@RegistrantOID,@RegistrantDate,@CREATED_BY,@CREATED_DATE,@LAST_UPD_BY,@LAST_UPD_DATE,@KV,@A,@KA,@IsOnlineCheck)");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
					new SqlParameter("@DevCode", SqlDbType.VarChar,20),
					new SqlParameter("@PowerSupplyCode", SqlDbType.VarChar,50),
					new SqlParameter("@PowerSupplyName", SqlDbType.NVarChar,50),
					new SqlParameter("@ConvertStationCode", SqlDbType.VarChar,50),
					new SqlParameter("@ConvertStationName", SqlDbType.NVarChar,50),
					new SqlParameter("@VoltageLevel", SqlDbType.VarChar,4),
					new SqlParameter("@DevClass", SqlDbType.VarChar,4),
					new SqlParameter("@RunCode", SqlDbType.VarChar,20),
					new SqlParameter("@DevName", SqlDbType.NVarChar,50),
					new SqlParameter("@Manufacturer", SqlDbType.VarChar,50),
					new SqlParameter("@DevType", SqlDbType.VarChar,50),
					new SqlParameter("@DevNum", SqlDbType.VarChar,20),
					new SqlParameter("@PurchaseDate", SqlDbType.Date,3),
					new SqlParameter("@PutDate", SqlDbType.Date,3),
					new SqlParameter("@ReturnDate", SqlDbType.Date,3),
					new SqlParameter("@RatedQL", SqlDbType.Decimal,9),
					new SqlParameter("@RatedQY", SqlDbType.Decimal,9),
					new SqlParameter("@Status", SqlDbType.VarChar,4),
					new SqlParameter("@RegistrantOID", SqlDbType.VarChar,50),
					new SqlParameter("@RegistrantDate", SqlDbType.Date,3),
					new SqlParameter("@CREATED_BY", SqlDbType.NVarChar,12),
					new SqlParameter("@CREATED_DATE", SqlDbType.Date,3),
					new SqlParameter("@LAST_UPD_BY", SqlDbType.NVarChar,12),
					new SqlParameter("@LAST_UPD_DATE", SqlDbType.Date,3),
					new SqlParameter("@KV", SqlDbType.Int,4),
					new SqlParameter("@A", SqlDbType.Int,4),
					new SqlParameter("@KA", SqlDbType.Int,4),
            new SqlParameter("@IsOnlineCheck", SqlDbType.VarChar,10)};
            parameters[0].Value = model.DevCode;
            parameters[1].Value = model.PowerSupplyCode;
            parameters[2].Value = model.PowerSupplyName;
            parameters[3].Value = model.ConvertStationCode;
            parameters[4].Value = model.ConvertStationName;
            parameters[5].Value = model.VoltageLevel;
            parameters[6].Value = model.DevClass;
            parameters[7].Value = model.RunCode;
            parameters[8].Value = model.DevName;
            parameters[9].Value = model.Manufacturer;
            parameters[10].Value = model.DevType;
            parameters[11].Value = model.DevNum;
            parameters[12].Value = model.PurchaseDate;
            parameters[13].Value = model.PutDate;
            parameters[14].Value = model.ReturnDate;
            parameters[15].Value = model.RatedQL;
            parameters[16].Value = model.RatedQY;
            parameters[17].Value = model.Status;
            parameters[18].Value = model.RegistrantOID;
            parameters[19].Value = model.RegistrantDate;
            parameters[20].Value = model.CREATED_BY;
            parameters[21].Value = model.CREATED_DATE;
            parameters[22].Value = model.LAST_UPD_BY;
            parameters[23].Value = model.LAST_UPD_DATE;
            parameters[24].Value = model.KV;
            parameters[25].Value = model.A;
            parameters[26].Value = model.KA;
            parameters[27].Value = model.IsOnlineCheck;

            object obj = DbHelperSQL.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {
                ComServies.UpdCodeNum(model.DevCode);
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(EGMNGS.Model.InfoDevPara model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update InfoDevPara set ");
            strSql.Append("DevCode=@DevCode,");
            strSql.Append("PowerSupplyCode=@PowerSupplyCode,");
            strSql.Append("PowerSupplyName=@PowerSupplyName,");
            strSql.Append("ConvertStationCode=@ConvertStationCode,");
            strSql.Append("ConvertStationName=@ConvertStationName,");
            strSql.Append("VoltageLevel=@VoltageLevel,");
            strSql.Append("DevClass=@DevClass,");
            strSql.Append("RunCode=@RunCode,");
            strSql.Append("DevName=@DevName,");
            strSql.Append("Manufacturer=@Manufacturer,");
            strSql.Append("DevType=@DevType,");
            strSql.Append("DevNum=@DevNum,");
            strSql.Append("PurchaseDate=@PurchaseDate,");
            strSql.Append("PutDate=@PutDate,");
            strSql.Append("ReturnDate=@ReturnDate,");
            strSql.Append("RatedQL=@RatedQL,");
            strSql.Append("RatedQY=@RatedQY,");
            strSql.Append("Status=@Status,");
            strSql.Append("RegistrantOID=@RegistrantOID,");
            strSql.Append("RegistrantDate=@RegistrantDate,");
            strSql.Append("CREATED_BY=@CREATED_BY,");
            strSql.Append("CREATED_DATE=@CREATED_DATE,");
            strSql.Append("LAST_UPD_BY=@LAST_UPD_BY,");
            strSql.Append("LAST_UPD_DATE=@LAST_UPD_DATE,");
            strSql.Append("KV=@KV,");
            strSql.Append("A=@A,");
            strSql.Append("KA=@KA,");
            strSql.Append("IsOnlineCheck=@IsOnlineCheck");
            strSql.Append(" where OID=@OID");
          
            SqlParameter[] parameters = {
					new SqlParameter("@DevCode", SqlDbType.VarChar,20),
					new SqlParameter("@PowerSupplyCode", SqlDbType.VarChar,50),
					new SqlParameter("@PowerSupplyName", SqlDbType.NVarChar,50),
					new SqlParameter("@ConvertStationCode", SqlDbType.VarChar,50),
					new SqlParameter("@ConvertStationName", SqlDbType.NVarChar,50),
					new SqlParameter("@VoltageLevel", SqlDbType.VarChar,4),
					new SqlParameter("@DevClass", SqlDbType.VarChar,4),
					new SqlParameter("@RunCode", SqlDbType.VarChar,20),
					new SqlParameter("@DevName", SqlDbType.NVarChar,50),
					new SqlParameter("@Manufacturer", SqlDbType.VarChar,50),
					new SqlParameter("@DevType", SqlDbType.VarChar,50),
					new SqlParameter("@DevNum", SqlDbType.VarChar,20),
					new SqlParameter("@PurchaseDate", SqlDbType.Date,3),
					new SqlParameter("@PutDate", SqlDbType.Date,3),
					new SqlParameter("@ReturnDate", SqlDbType.Date,3),
					new SqlParameter("@RatedQL", SqlDbType.Decimal,9),
					new SqlParameter("@RatedQY", SqlDbType.Decimal,9),
					new SqlParameter("@Status", SqlDbType.VarChar,4),
					new SqlParameter("@RegistrantOID", SqlDbType.VarChar,50),
					new SqlParameter("@RegistrantDate", SqlDbType.Date,3),
					new SqlParameter("@CREATED_BY", SqlDbType.NVarChar,12),
					new SqlParameter("@CREATED_DATE", SqlDbType.Date,3),
					new SqlParameter("@LAST_UPD_BY", SqlDbType.NVarChar,12),
					new SqlParameter("@LAST_UPD_DATE", SqlDbType.Date,3),
					new SqlParameter("@KV", SqlDbType.Int,4),
					new SqlParameter("@A", SqlDbType.Int,4),
					new SqlParameter("@KA", SqlDbType.Int,4),
                    new SqlParameter("@OID", SqlDbType.Int,4),
                    new SqlParameter("@IsOnlineCheck", SqlDbType.VarChar,10)};
            parameters[0].Value = model.DevCode;
            parameters[1].Value = model.PowerSupplyCode;
            parameters[2].Value = model.PowerSupplyName;
            parameters[3].Value = model.ConvertStationCode;
            parameters[4].Value = model.ConvertStationName;
            parameters[5].Value = model.VoltageLevel;
            parameters[6].Value = model.DevClass;
            parameters[7].Value = model.RunCode;
            parameters[8].Value = model.DevName;
            parameters[9].Value = model.Manufacturer;
            parameters[10].Value = model.DevType;
            parameters[11].Value = model.DevNum;
            parameters[12].Value = model.PurchaseDate;
            parameters[13].Value = model.PutDate;
            parameters[14].Value = model.ReturnDate;
            parameters[15].Value = model.RatedQL;
            parameters[16].Value = model.RatedQY;
            parameters[17].Value = model.Status;
            parameters[18].Value = model.RegistrantOID;
            parameters[19].Value = model.RegistrantDate;
            parameters[20].Value = model.CREATED_BY;
            parameters[21].Value = model.CREATED_DATE;
            parameters[22].Value = model.LAST_UPD_BY;
            parameters[23].Value = model.LAST_UPD_DATE;
            parameters[24].Value = model.KV;
            parameters[25].Value = model.A;
            parameters[26].Value = model.KA;
            parameters[27].Value = model.OID;
            parameters[28].Value = model.IsOnlineCheck;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int OID)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from InfoDevPara ");
            strSql.Append(" where OID=@OID");
            SqlParameter[] parameters = {
					new SqlParameter("@OID", SqlDbType.Int,4)
			};
            parameters[0].Value = OID;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string OIDlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from InfoDevPara ");
            strSql.Append(" where OID in (" + OIDlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public EGMNGS.Model.InfoDevPara GetModel(int OID)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 * from InfoDevPara ");
            strSql.Append(" where OID=@OID");
            SqlParameter[] parameters = {
					new SqlParameter("@OID", SqlDbType.Int,4)
			};
            parameters[0].Value = OID;

            EGMNGS.Model.InfoDevPara model = new EGMNGS.Model.InfoDevPara();
            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0]["OID"] != null && ds.Tables[0].Rows[0]["OID"].ToString() != "")
                {
                    model.OID = int.Parse(ds.Tables[0].Rows[0]["OID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["DevCode"] != null && ds.Tables[0].Rows[0]["DevCode"].ToString() != "")
                {
                    model.DevCode = ds.Tables[0].Rows[0]["DevCode"].ToString();
                }
                if (ds.Tables[0].Rows[0]["PowerSupplyCode"] != null && ds.Tables[0].Rows[0]["PowerSupplyCode"].ToString() != "")
                {
                    model.PowerSupplyCode = ds.Tables[0].Rows[0]["PowerSupplyCode"].ToString();
                }
                if (ds.Tables[0].Rows[0]["PowerSupplyName"] != null && ds.Tables[0].Rows[0]["PowerSupplyName"].ToString() != "")
                {
                    model.PowerSupplyName = ds.Tables[0].Rows[0]["PowerSupplyName"].ToString();
                }
                if (ds.Tables[0].Rows[0]["ConvertStationCode"] != null && ds.Tables[0].Rows[0]["ConvertStationCode"].ToString() != "")
                {
                    model.ConvertStationCode = ds.Tables[0].Rows[0]["ConvertStationCode"].ToString();
                }
                if (ds.Tables[0].Rows[0]["ConvertStationName"] != null && ds.Tables[0].Rows[0]["ConvertStationName"].ToString() != "")
                {
                    model.ConvertStationName = ds.Tables[0].Rows[0]["ConvertStationName"].ToString();
                }
                if (ds.Tables[0].Rows[0]["VoltageLevel"] != null && ds.Tables[0].Rows[0]["VoltageLevel"].ToString() != "")
                {
                    model.VoltageLevel = ds.Tables[0].Rows[0]["VoltageLevel"].ToString();
                }
                if (ds.Tables[0].Rows[0]["DevClass"] != null && ds.Tables[0].Rows[0]["DevClass"].ToString() != "")
                {
                    model.DevClass = ds.Tables[0].Rows[0]["DevClass"].ToString();
                }
                if (ds.Tables[0].Rows[0]["RunCode"] != null && ds.Tables[0].Rows[0]["RunCode"].ToString() != "")
                {
                    model.RunCode = ds.Tables[0].Rows[0]["RunCode"].ToString();
                }
                if (ds.Tables[0].Rows[0]["DevName"] != null && ds.Tables[0].Rows[0]["DevName"].ToString() != "")
                {
                    model.DevName = ds.Tables[0].Rows[0]["DevName"].ToString();
                }
                if (ds.Tables[0].Rows[0]["Manufacturer"] != null && ds.Tables[0].Rows[0]["Manufacturer"].ToString() != "")
                {
                    model.Manufacturer = ds.Tables[0].Rows[0]["Manufacturer"].ToString();
                }
                if (ds.Tables[0].Rows[0]["DevType"] != null && ds.Tables[0].Rows[0]["DevType"].ToString() != "")
                {
                    model.DevType = ds.Tables[0].Rows[0]["DevType"].ToString();
                }
                if (ds.Tables[0].Rows[0]["DevNum"] != null && ds.Tables[0].Rows[0]["DevNum"].ToString() != "")
                {
                    model.DevNum = ds.Tables[0].Rows[0]["DevNum"].ToString();
                }
                if (ds.Tables[0].Rows[0]["PurchaseDate"] != null && ds.Tables[0].Rows[0]["PurchaseDate"].ToString() != "")
                {
                    model.PurchaseDate = DateTime.Parse(ds.Tables[0].Rows[0]["PurchaseDate"].ToString());
                }
                if (ds.Tables[0].Rows[0]["PutDate"] != null && ds.Tables[0].Rows[0]["PutDate"].ToString() != "")
                {
                    model.PutDate = DateTime.Parse(ds.Tables[0].Rows[0]["PutDate"].ToString());
                }
                if (ds.Tables[0].Rows[0]["ReturnDate"] != null && ds.Tables[0].Rows[0]["ReturnDate"].ToString() != "")
                {
                    model.ReturnDate = DateTime.Parse(ds.Tables[0].Rows[0]["ReturnDate"].ToString());
                }
                if (ds.Tables[0].Rows[0]["RatedQL"] != null && ds.Tables[0].Rows[0]["RatedQL"].ToString() != "")
                {
                    model.RatedQL = decimal.Parse(ds.Tables[0].Rows[0]["RatedQL"].ToString());
                }
                if (ds.Tables[0].Rows[0]["RatedQY"] != null && ds.Tables[0].Rows[0]["RatedQY"].ToString() != "")
                {
                    model.RatedQY = decimal.Parse(ds.Tables[0].Rows[0]["RatedQY"].ToString());
                }
                if (ds.Tables[0].Rows[0]["Status"] != null && ds.Tables[0].Rows[0]["Status"].ToString() != "")
                {
                    model.Status = ds.Tables[0].Rows[0]["Status"].ToString();
                }
                if (ds.Tables[0].Rows[0]["RegistrantOID"] != null && ds.Tables[0].Rows[0]["RegistrantOID"].ToString() != "")
                {
                    model.RegistrantOID = ds.Tables[0].Rows[0]["RegistrantOID"].ToString();
                }
                if (ds.Tables[0].Rows[0]["RegistrantDate"] != null && ds.Tables[0].Rows[0]["RegistrantDate"].ToString() != "")
                {
                    model.RegistrantDate = DateTime.Parse(ds.Tables[0].Rows[0]["RegistrantDate"].ToString());
                }
                if (ds.Tables[0].Rows[0]["CREATED_BY"] != null && ds.Tables[0].Rows[0]["CREATED_BY"].ToString() != "")
                {
                    model.CREATED_BY = ds.Tables[0].Rows[0]["CREATED_BY"].ToString();
                }
                if (ds.Tables[0].Rows[0]["CREATED_DATE"] != null && ds.Tables[0].Rows[0]["CREATED_DATE"].ToString() != "")
                {
                    model.CREATED_DATE = DateTime.Parse(ds.Tables[0].Rows[0]["CREATED_DATE"].ToString());
                }
                if (ds.Tables[0].Rows[0]["LAST_UPD_BY"] != null && ds.Tables[0].Rows[0]["LAST_UPD_BY"].ToString() != "")
                {
                    model.LAST_UPD_BY = ds.Tables[0].Rows[0]["LAST_UPD_BY"].ToString();
                }
                if (ds.Tables[0].Rows[0]["LAST_UPD_DATE"] != null && ds.Tables[0].Rows[0]["LAST_UPD_DATE"].ToString() != "")
                {
                    model.LAST_UPD_DATE = DateTime.Parse(ds.Tables[0].Rows[0]["LAST_UPD_DATE"].ToString());
                }
                if (ds.Tables[0].Rows[0]["KV"] != null && ds.Tables[0].Rows[0]["KV"].ToString() != "")
                {
                    model.KV = int.Parse(ds.Tables[0].Rows[0]["KV"].ToString());
                }
                if (ds.Tables[0].Rows[0]["A"] != null && ds.Tables[0].Rows[0]["A"].ToString() != "")
                {
                    model.A = int.Parse(ds.Tables[0].Rows[0]["A"].ToString());
                }
                if (ds.Tables[0].Rows[0]["KA"] != null && ds.Tables[0].Rows[0]["KA"].ToString() != "")
                {
                    model.KA = int.Parse(ds.Tables[0].Rows[0]["KA"].ToString());
                }

                if (ds.Tables[0].Rows[0]["IsOnlineCheck"] != null && ds.Tables[0].Rows[0]["IsOnlineCheck"].ToString() != "")
                {
                    model.IsOnlineCheck = ds.Tables[0].Rows[0]["IsOnlineCheck"].ToString();
                }
                return model;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select * ");
            strSql.Append(" FROM InfoDevPara ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" * ");
            strSql.Append(" FROM InfoDevPara ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM InfoDevPara ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.OID desc");
            }
            strSql.Append(")AS Row, T.*  from InfoDevPara T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", (startIndex - 1) * endIndex + 1, startIndex * endIndex);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /*
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        {
            SqlParameter[] parameters = {
                    new SqlParameter("@tblName", SqlDbType.VarChar, 255),
                    new SqlParameter("@fldName", SqlDbType.VarChar, 255),
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@PageIndex", SqlDbType.Int),
                    new SqlParameter("@IsReCount", SqlDbType.Bit),
                    new SqlParameter("@OrderType", SqlDbType.Bit),
                    new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
                    };
            parameters[0].Value = "InfoDevPara";
            parameters[1].Value = "OID";
            parameters[2].Value = PageSize;
            parameters[3].Value = PageIndex;
            parameters[4].Value = 0;
            parameters[5].Value = 0;
            parameters[6].Value = strWhere;	
            return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
        }*/

        #endregion  Method
    }
}


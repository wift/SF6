﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Maticsoft.DBUtility;//Please add references
namespace EGMNGS.DAL
{
	/// <summary>
	/// 数据访问类:SYS_USER_PWD
	/// </summary>
	public partial class SYS_USER_PWD
	{
		public SYS_USER_PWD()
		{}
		#region  Method

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(string USER_ID,DateTime EXPIRED_DATE)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from SYS_USER_PWD");
			strSql.Append(" where USER_ID=@USER_ID and EXPIRED_DATE=@EXPIRED_DATE ");
			SqlParameter[] parameters = {
					new SqlParameter("@USER_ID", SqlDbType.VarChar,12),
					new SqlParameter("@EXPIRED_DATE", SqlDbType.DateTime)			};
			parameters[0].Value = USER_ID;
			parameters[1].Value = EXPIRED_DATE;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(EGMNGS.Model.SYS_USER_PWD model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into SYS_USER_PWD(");
			strSql.Append("USER_ID,EXPIRED_DATE,USER_PWD)");
			strSql.Append(" values (");
			strSql.Append("@USER_ID,@EXPIRED_DATE,@USER_PWD)");
			SqlParameter[] parameters = {
					new SqlParameter("@USER_ID", SqlDbType.VarChar,12),
					new SqlParameter("@EXPIRED_DATE", SqlDbType.DateTime),
					new SqlParameter("@USER_PWD", SqlDbType.VarChar,10)};
			parameters[0].Value = model.USER_ID;
			parameters[1].Value = model.EXPIRED_DATE;
			parameters[2].Value = model.USER_PWD;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(EGMNGS.Model.SYS_USER_PWD model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update SYS_USER_PWD set ");
			strSql.Append("USER_PWD=@USER_PWD");
			strSql.Append(" where USER_ID=@USER_ID and EXPIRED_DATE=@EXPIRED_DATE ");
			SqlParameter[] parameters = {
					new SqlParameter("@USER_PWD", SqlDbType.VarChar,10),
					new SqlParameter("@USER_ID", SqlDbType.VarChar,12),
					new SqlParameter("@EXPIRED_DATE", SqlDbType.DateTime)};
			parameters[0].Value = model.USER_PWD;
			parameters[1].Value = model.USER_ID;
			parameters[2].Value = model.EXPIRED_DATE;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(string USER_ID,DateTime EXPIRED_DATE)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from SYS_USER_PWD ");
			strSql.Append(" where USER_ID=@USER_ID and EXPIRED_DATE=@EXPIRED_DATE ");
			SqlParameter[] parameters = {
					new SqlParameter("@USER_ID", SqlDbType.VarChar,12),
					new SqlParameter("@EXPIRED_DATE", SqlDbType.DateTime)			};
			parameters[0].Value = USER_ID;
			parameters[1].Value = EXPIRED_DATE;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public EGMNGS.Model.SYS_USER_PWD GetModel(string USER_ID,DateTime EXPIRED_DATE)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 USER_ID,EXPIRED_DATE,USER_PWD from SYS_USER_PWD ");
			strSql.Append(" where USER_ID=@USER_ID and EXPIRED_DATE=@EXPIRED_DATE ");
			SqlParameter[] parameters = {
					new SqlParameter("@USER_ID", SqlDbType.VarChar,12),
					new SqlParameter("@EXPIRED_DATE", SqlDbType.DateTime)			};
			parameters[0].Value = USER_ID;
			parameters[1].Value = EXPIRED_DATE;

			EGMNGS.Model.SYS_USER_PWD model=new EGMNGS.Model.SYS_USER_PWD();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				if(ds.Tables[0].Rows[0]["USER_ID"]!=null && ds.Tables[0].Rows[0]["USER_ID"].ToString()!="")
				{
					model.USER_ID=ds.Tables[0].Rows[0]["USER_ID"].ToString();
				}
				if(ds.Tables[0].Rows[0]["EXPIRED_DATE"]!=null && ds.Tables[0].Rows[0]["EXPIRED_DATE"].ToString()!="")
				{
					model.EXPIRED_DATE=DateTime.Parse(ds.Tables[0].Rows[0]["EXPIRED_DATE"].ToString());
				}
				if(ds.Tables[0].Rows[0]["USER_PWD"]!=null && ds.Tables[0].Rows[0]["USER_PWD"].ToString()!="")
				{
					model.USER_PWD=ds.Tables[0].Rows[0]["USER_PWD"].ToString();
				}
				return model;
			}
			else
			{
				return null;
			}
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select USER_ID,EXPIRED_DATE,USER_PWD ");
			strSql.Append(" FROM SYS_USER_PWD ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" USER_ID,EXPIRED_DATE,USER_PWD ");
			strSql.Append(" FROM SYS_USER_PWD ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM SYS_USER_PWD ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.EXPIRED_DATE desc");
			}
			strSql.Append(")AS Row, T.*  from SYS_USER_PWD T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", (startIndex - 1) * endIndex + 1, startIndex * endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "SYS_USER_PWD";
			parameters[1].Value = "EXPIRED_DATE";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  Method
	}
}


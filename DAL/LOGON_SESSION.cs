﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Maticsoft.DBUtility;//Please add references
namespace EGMNGS.DAL
{
	/// <summary>
	/// 数据访问类:LOGON_SESSION
	/// </summary>
	public partial class LOGON_SESSION
	{
		public LOGON_SESSION()
		{}
		#region  Method

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(DateTime LOGON_DATE,string USER_ID)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from LOGON_SESSION");
			strSql.Append(" where LOGON_DATE=@LOGON_DATE and USER_ID=@USER_ID ");
			SqlParameter[] parameters = {
					new SqlParameter("@LOGON_DATE", SqlDbType.DateTime),
					new SqlParameter("@USER_ID", SqlDbType.VarChar,12)			};
			parameters[0].Value = LOGON_DATE;
			parameters[1].Value = USER_ID;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(EGMNGS.Model.LOGON_SESSION model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into LOGON_SESSION(");
			strSql.Append("LOGON_DATE,USER_ID,LAST_ACTION_DATE,LOGOUT_DATE,FAIL_REASON,SESSION_TIMEOUT_DATE)");
			strSql.Append(" values (");
			strSql.Append("@LOGON_DATE,@USER_ID,@LAST_ACTION_DATE,@LOGOUT_DATE,@FAIL_REASON,@SESSION_TIMEOUT_DATE)");
			SqlParameter[] parameters = {
					new SqlParameter("@LOGON_DATE", SqlDbType.DateTime),
					new SqlParameter("@USER_ID", SqlDbType.VarChar,12),
					new SqlParameter("@LAST_ACTION_DATE", SqlDbType.DateTime),
					new SqlParameter("@LOGOUT_DATE", SqlDbType.DateTime),
					new SqlParameter("@FAIL_REASON", SqlDbType.VarChar,50),
					new SqlParameter("@SESSION_TIMEOUT_DATE", SqlDbType.DateTime)};
			parameters[0].Value = model.LOGON_DATE;
			parameters[1].Value = model.USER_ID;
			parameters[2].Value = model.LAST_ACTION_DATE;
			parameters[3].Value = model.LOGOUT_DATE;
			parameters[4].Value = model.FAIL_REASON;
			parameters[5].Value = model.SESSION_TIMEOUT_DATE;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(EGMNGS.Model.LOGON_SESSION model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update LOGON_SESSION set ");
			strSql.Append("LAST_ACTION_DATE=@LAST_ACTION_DATE,");
			strSql.Append("LOGOUT_DATE=@LOGOUT_DATE,");
			strSql.Append("FAIL_REASON=@FAIL_REASON,");
			strSql.Append("SESSION_TIMEOUT_DATE=@SESSION_TIMEOUT_DATE");
			strSql.Append(" where LOGON_DATE=@LOGON_DATE and USER_ID=@USER_ID ");
			SqlParameter[] parameters = {
					new SqlParameter("@LAST_ACTION_DATE", SqlDbType.DateTime),
					new SqlParameter("@LOGOUT_DATE", SqlDbType.DateTime),
					new SqlParameter("@FAIL_REASON", SqlDbType.VarChar,50),
					new SqlParameter("@SESSION_TIMEOUT_DATE", SqlDbType.DateTime),
					new SqlParameter("@LOGON_DATE", SqlDbType.DateTime),
					new SqlParameter("@USER_ID", SqlDbType.VarChar,12)};
			parameters[0].Value = model.LAST_ACTION_DATE;
			parameters[1].Value = model.LOGOUT_DATE;
			parameters[2].Value = model.FAIL_REASON;
			parameters[3].Value = model.SESSION_TIMEOUT_DATE;
			parameters[4].Value = model.LOGON_DATE;
			parameters[5].Value = model.USER_ID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(DateTime LOGON_DATE,string USER_ID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from LOGON_SESSION ");
			strSql.Append(" where LOGON_DATE=@LOGON_DATE and USER_ID=@USER_ID ");
			SqlParameter[] parameters = {
					new SqlParameter("@LOGON_DATE", SqlDbType.DateTime),
					new SqlParameter("@USER_ID", SqlDbType.VarChar,12)			};
			parameters[0].Value = LOGON_DATE;
			parameters[1].Value = USER_ID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public EGMNGS.Model.LOGON_SESSION GetModel(DateTime LOGON_DATE,string USER_ID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 LOGON_DATE,USER_ID,LAST_ACTION_DATE,LOGOUT_DATE,FAIL_REASON,SESSION_TIMEOUT_DATE from LOGON_SESSION ");
			strSql.Append(" where LOGON_DATE=@LOGON_DATE and USER_ID=@USER_ID ");
			SqlParameter[] parameters = {
					new SqlParameter("@LOGON_DATE", SqlDbType.DateTime),
					new SqlParameter("@USER_ID", SqlDbType.VarChar,12)			};
			parameters[0].Value = LOGON_DATE;
			parameters[1].Value = USER_ID;

			EGMNGS.Model.LOGON_SESSION model=new EGMNGS.Model.LOGON_SESSION();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				if(ds.Tables[0].Rows[0]["LOGON_DATE"]!=null && ds.Tables[0].Rows[0]["LOGON_DATE"].ToString()!="")
				{
					model.LOGON_DATE=DateTime.Parse(ds.Tables[0].Rows[0]["LOGON_DATE"].ToString());
				}
				if(ds.Tables[0].Rows[0]["USER_ID"]!=null && ds.Tables[0].Rows[0]["USER_ID"].ToString()!="")
				{
					model.USER_ID=ds.Tables[0].Rows[0]["USER_ID"].ToString();
				}
				if(ds.Tables[0].Rows[0]["LAST_ACTION_DATE"]!=null && ds.Tables[0].Rows[0]["LAST_ACTION_DATE"].ToString()!="")
				{
					model.LAST_ACTION_DATE=DateTime.Parse(ds.Tables[0].Rows[0]["LAST_ACTION_DATE"].ToString());
				}
				if(ds.Tables[0].Rows[0]["LOGOUT_DATE"]!=null && ds.Tables[0].Rows[0]["LOGOUT_DATE"].ToString()!="")
				{
					model.LOGOUT_DATE=DateTime.Parse(ds.Tables[0].Rows[0]["LOGOUT_DATE"].ToString());
				}
				if(ds.Tables[0].Rows[0]["FAIL_REASON"]!=null && ds.Tables[0].Rows[0]["FAIL_REASON"].ToString()!="")
				{
					model.FAIL_REASON=ds.Tables[0].Rows[0]["FAIL_REASON"].ToString();
				}
				if(ds.Tables[0].Rows[0]["SESSION_TIMEOUT_DATE"]!=null && ds.Tables[0].Rows[0]["SESSION_TIMEOUT_DATE"].ToString()!="")
				{
					model.SESSION_TIMEOUT_DATE=DateTime.Parse(ds.Tables[0].Rows[0]["SESSION_TIMEOUT_DATE"].ToString());
				}
				return model;
			}
			else
			{
				return null;
			}
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select LOGON_DATE,USER_ID,LAST_ACTION_DATE,LOGOUT_DATE,FAIL_REASON,SESSION_TIMEOUT_DATE ");
			strSql.Append(" FROM LOGON_SESSION ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" LOGON_DATE,USER_ID,LAST_ACTION_DATE,LOGOUT_DATE,FAIL_REASON,SESSION_TIMEOUT_DATE ");
			strSql.Append(" FROM LOGON_SESSION ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM LOGON_SESSION ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.USER_ID desc");
			}
			strSql.Append(")AS Row, T.*  from LOGON_SESSION T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", (startIndex - 1) * endIndex + 1, startIndex * endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "LOGON_SESSION";
			parameters[1].Value = "USER_ID";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  Method
	}
}


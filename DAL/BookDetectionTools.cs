﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Maticsoft.DBUtility;//Please add references
namespace EGMNGS.DAL
{
    /// <summary>
    /// 数据访问类:BookDetectionTools
    /// </summary>
    public partial class BookDetectionTools
    {
        public BookDetectionTools()
        { }
        #region  Method

        /// <summary>
        /// 得到最大ID
        /// </summary>
        public int GetMaxId()
        {
            return DbHelperSQL.GetMaxID("OID", "BookDetectionTools");
        }

        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int OID)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) from BookDetectionTools");
            strSql.Append(" where OID=@OID");
            SqlParameter[] parameters = {
					new SqlParameter("@OID", SqlDbType.Int,4)
			};
            parameters[0].Value = OID;

            return DbHelperSQL.Exists(strSql.ToString(), parameters);
        }


        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(EGMNGS.Model.BookDetectionTools model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into BookDetectionTools(");
            strSql.Append("ToolCode,ToolType,ToolName,SpecType,FactoryCode,EffectiveDate,Status,RegistantsOID,RegistantsDate,CREATED_BY,CREATED_DATE,LAST_UPD_BY,LAST_UPD_DATE)");
            strSql.Append(" values (");
            strSql.Append("@ToolCode,@ToolType,@ToolName,@SpecType,@FactoryCode,@EffectiveDate,@Status,@RegistantsOID,@RegistantsDate,@CREATED_BY,@CREATED_DATE,@LAST_UPD_BY,@LAST_UPD_DATE)");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
					new SqlParameter("@ToolCode", SqlDbType.VarChar,20),
					new SqlParameter("@ToolType", SqlDbType.VarChar,4),
					new SqlParameter("@ToolName", SqlDbType.VarChar,50),
					new SqlParameter("@SpecType", SqlDbType.VarChar,50),
					new SqlParameter("@FactoryCode", SqlDbType.VarChar,20),
					new SqlParameter("@EffectiveDate", SqlDbType.Date,3),
					new SqlParameter("@Status", SqlDbType.VarChar,4),
					new SqlParameter("@RegistantsOID", SqlDbType.VarChar,50),
					new SqlParameter("@RegistantsDate", SqlDbType.Date,3),
					new SqlParameter("@CREATED_BY", SqlDbType.NVarChar,12),
					new SqlParameter("@CREATED_DATE", SqlDbType.Date,3),
					new SqlParameter("@LAST_UPD_BY", SqlDbType.NVarChar,12),
					new SqlParameter("@LAST_UPD_DATE", SqlDbType.Date,3)};
            parameters[0].Value = model.ToolCode;
            parameters[1].Value = model.ToolType;
            parameters[2].Value = model.ToolName;
            parameters[3].Value = model.SpecType;
            parameters[4].Value = model.FactoryCode;
            parameters[5].Value = model.EffectiveDate;
            parameters[6].Value = model.Status;
            parameters[7].Value = model.RegistantsOID;
            parameters[8].Value = model.RegistantsDate;
            parameters[9].Value = model.CREATED_BY;
            parameters[10].Value = model.CREATED_DATE;
            parameters[11].Value = model.LAST_UPD_BY;
            parameters[12].Value = model.LAST_UPD_DATE;

            object obj = DbHelperSQL.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {
                EGMNGS.Common.ComServies.UpdCodeNum(model.ToolCode);
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(EGMNGS.Model.BookDetectionTools model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update BookDetectionTools set ");
            strSql.Append("ToolCode=@ToolCode,");
            strSql.Append("ToolType=@ToolType,");
            strSql.Append("ToolName=@ToolName,");
            strSql.Append("SpecType=@SpecType,");
            strSql.Append("FactoryCode=@FactoryCode,");
            strSql.Append("EffectiveDate=@EffectiveDate,");
            strSql.Append("Status=@Status,");
            strSql.Append("RegistantsOID=@RegistantsOID,");
            strSql.Append("RegistantsDate=@RegistantsDate,");
            strSql.Append("CREATED_BY=@CREATED_BY,");
            strSql.Append("CREATED_DATE=@CREATED_DATE,");
            strSql.Append("LAST_UPD_BY=@LAST_UPD_BY,");
            strSql.Append("LAST_UPD_DATE=@LAST_UPD_DATE");
            strSql.Append(" where OID=@OID");
            SqlParameter[] parameters = {
					new SqlParameter("@ToolCode", SqlDbType.VarChar,20),
					new SqlParameter("@ToolType", SqlDbType.VarChar,4),
					new SqlParameter("@ToolName", SqlDbType.VarChar,50),
					new SqlParameter("@SpecType", SqlDbType.VarChar,50),
					new SqlParameter("@FactoryCode", SqlDbType.VarChar,20),
					new SqlParameter("@EffectiveDate", SqlDbType.Date,3),
					new SqlParameter("@Status", SqlDbType.VarChar,4),
					new SqlParameter("@RegistantsOID", SqlDbType.VarChar,50),
					new SqlParameter("@RegistantsDate", SqlDbType.Date,3),
					new SqlParameter("@CREATED_BY", SqlDbType.NVarChar,12),
					new SqlParameter("@CREATED_DATE", SqlDbType.Date,3),
					new SqlParameter("@LAST_UPD_BY", SqlDbType.NVarChar,12),
					new SqlParameter("@LAST_UPD_DATE", SqlDbType.Date,3),
					new SqlParameter("@OID", SqlDbType.Int,4)};
            parameters[0].Value = model.ToolCode;
            parameters[1].Value = model.ToolType;
            parameters[2].Value = model.ToolName;
            parameters[3].Value = model.SpecType;
            parameters[4].Value = model.FactoryCode;
            parameters[5].Value = model.EffectiveDate;
            parameters[6].Value = model.Status;
            parameters[7].Value = model.RegistantsOID;
            parameters[8].Value = model.RegistantsDate;
            parameters[9].Value = model.CREATED_BY;
            parameters[10].Value = model.CREATED_DATE;
            parameters[11].Value = model.LAST_UPD_BY;
            parameters[12].Value = model.LAST_UPD_DATE;
            parameters[13].Value = model.OID;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int OID)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from BookDetectionTools ");
            strSql.Append(" where OID=@OID");
            SqlParameter[] parameters = {
					new SqlParameter("@OID", SqlDbType.Int,4)
			};
            parameters[0].Value = OID;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string OIDlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from BookDetectionTools ");
            strSql.Append(" where OID in (" + OIDlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public EGMNGS.Model.BookDetectionTools GetModel(int OID)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 OID,ToolCode,ToolType,ToolName,SpecType,FactoryCode,EffectiveDate,Status,RegistantsOID,RegistantsDate,CREATED_BY,CREATED_DATE,LAST_UPD_BY,LAST_UPD_DATE from BookDetectionTools ");
            strSql.Append(" where OID=@OID");
            SqlParameter[] parameters = {
					new SqlParameter("@OID", SqlDbType.Int,4)
			};
            parameters[0].Value = OID;

            EGMNGS.Model.BookDetectionTools model = new EGMNGS.Model.BookDetectionTools();
            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0]["OID"] != null && ds.Tables[0].Rows[0]["OID"].ToString() != "")
                {
                    model.OID = int.Parse(ds.Tables[0].Rows[0]["OID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["ToolCode"] != null && ds.Tables[0].Rows[0]["ToolCode"].ToString() != "")
                {
                    model.ToolCode = ds.Tables[0].Rows[0]["ToolCode"].ToString();
                }
                if (ds.Tables[0].Rows[0]["ToolType"] != null && ds.Tables[0].Rows[0]["ToolType"].ToString() != "")
                {
                    model.ToolType = ds.Tables[0].Rows[0]["ToolType"].ToString();
                }
                if (ds.Tables[0].Rows[0]["ToolName"] != null && ds.Tables[0].Rows[0]["ToolName"].ToString() != "")
                {
                    model.ToolName = ds.Tables[0].Rows[0]["ToolName"].ToString();
                }
                if (ds.Tables[0].Rows[0]["SpecType"] != null && ds.Tables[0].Rows[0]["SpecType"].ToString() != "")
                {
                    model.SpecType = ds.Tables[0].Rows[0]["SpecType"].ToString();
                }
                if (ds.Tables[0].Rows[0]["FactoryCode"] != null && ds.Tables[0].Rows[0]["FactoryCode"].ToString() != "")
                {
                    model.FactoryCode = ds.Tables[0].Rows[0]["FactoryCode"].ToString();
                }
                if (ds.Tables[0].Rows[0]["EffectiveDate"] != null && ds.Tables[0].Rows[0]["EffectiveDate"].ToString() != "")
                {
                    model.EffectiveDate = DateTime.Parse(ds.Tables[0].Rows[0]["EffectiveDate"].ToString());
                }
                if (ds.Tables[0].Rows[0]["Status"] != null && ds.Tables[0].Rows[0]["Status"].ToString() != "")
                {
                    model.Status = ds.Tables[0].Rows[0]["Status"].ToString();
                }
                if (ds.Tables[0].Rows[0]["RegistantsOID"] != null && ds.Tables[0].Rows[0]["RegistantsOID"].ToString() != "")
                {
                    model.RegistantsOID = ds.Tables[0].Rows[0]["RegistantsOID"].ToString();
                }
                if (ds.Tables[0].Rows[0]["RegistantsDate"] != null && ds.Tables[0].Rows[0]["RegistantsDate"].ToString() != "")
                {
                    model.RegistantsDate = DateTime.Parse(ds.Tables[0].Rows[0]["RegistantsDate"].ToString());
                }
                if (ds.Tables[0].Rows[0]["CREATED_BY"] != null && ds.Tables[0].Rows[0]["CREATED_BY"].ToString() != "")
                {
                    model.CREATED_BY = ds.Tables[0].Rows[0]["CREATED_BY"].ToString();
                }
                if (ds.Tables[0].Rows[0]["CREATED_DATE"] != null && ds.Tables[0].Rows[0]["CREATED_DATE"].ToString() != "")
                {
                    model.CREATED_DATE = DateTime.Parse(ds.Tables[0].Rows[0]["CREATED_DATE"].ToString());
                }
                if (ds.Tables[0].Rows[0]["LAST_UPD_BY"] != null && ds.Tables[0].Rows[0]["LAST_UPD_BY"].ToString() != "")
                {
                    model.LAST_UPD_BY = ds.Tables[0].Rows[0]["LAST_UPD_BY"].ToString();
                }
                if (ds.Tables[0].Rows[0]["LAST_UPD_DATE"] != null && ds.Tables[0].Rows[0]["LAST_UPD_DATE"].ToString() != "")
                {
                    model.LAST_UPD_DATE = DateTime.Parse(ds.Tables[0].Rows[0]["LAST_UPD_DATE"].ToString());
                }
                return model;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select OID,ToolCode,ToolType,ToolName,SpecType,FactoryCode,EffectiveDate,Status,RegistantsOID,RegistantsDate,CREATED_BY,CREATED_DATE,LAST_UPD_BY,LAST_UPD_DATE,SpecType+','+FactoryCode+','+CONVERT(VARCHAR(20),EffectiveDate,23) as sfe");
            strSql.Append(" FROM BookDetectionTools ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" OID,ToolCode,ToolType,ToolName,SpecType,FactoryCode,EffectiveDate,Status,RegistantsOID,RegistantsDate,CREATED_BY,CREATED_DATE,LAST_UPD_BY,LAST_UPD_DATE ");
            strSql.Append(" FROM BookDetectionTools ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM BookDetectionTools ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.OID desc");
            }
            strSql.Append(")AS Row, T.*  from BookDetectionTools T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", (startIndex - 1) * endIndex + 1, startIndex * endIndex);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /*
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        {
            SqlParameter[] parameters = {
                    new SqlParameter("@tblName", SqlDbType.VarChar, 255),
                    new SqlParameter("@fldName", SqlDbType.VarChar, 255),
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@PageIndex", SqlDbType.Int),
                    new SqlParameter("@IsReCount", SqlDbType.Bit),
                    new SqlParameter("@OrderType", SqlDbType.Bit),
                    new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
                    };
            parameters[0].Value = "BookDetectionTools";
            parameters[1].Value = "OID";
            parameters[2].Value = PageSize;
            parameters[3].Value = PageIndex;
            parameters[4].Value = 0;
            parameters[5].Value = 0;
            parameters[6].Value = strWhere;	
            return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
        }*/

        #endregion  Method
    }
}


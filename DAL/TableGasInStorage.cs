﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Maticsoft.DBUtility;//Please add references
namespace EGMNGS.DAL
{
	/// <summary>
	/// 数据访问类:TableGasInStorage
	/// </summary>
	public partial class TableGasInStorage
	{
		public TableGasInStorage()
		{}
		#region  Method

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
		return DbHelperSQL.GetMaxID("OID", "TableGasInStorage"); 
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int OID)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from TableGasInStorage");
			strSql.Append(" where OID=@OID");
			SqlParameter[] parameters = {
					new SqlParameter("@OID", SqlDbType.Int,4)
			};
			parameters[0].Value = OID;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(EGMNGS.Model.TableGasInStorage model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into TableGasInStorage(");
			strSql.Append("Code,Year,Month,GasType,BusinessCode,GasSourse,AmountInput,Status,registrantOID,RegistrantDate,CREATED_BY,CREATED_DATE,LAST_UPD_BY,LAST_UPD_DATE)");
			strSql.Append(" values (");
			strSql.Append("@Code,@Year,@Month,@GasType,@BusinessCode,@GasSourse,@AmountInput,@Status,@registrantOID,@RegistrantDate,@CREATED_BY,@CREATED_DATE,@LAST_UPD_BY,@LAST_UPD_DATE)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@Code", SqlDbType.VarChar,20),
					new SqlParameter("@Year", SqlDbType.VarChar,4),
					new SqlParameter("@Month", SqlDbType.VarChar,2),
					new SqlParameter("@GasType", SqlDbType.VarChar,4),
					new SqlParameter("@BusinessCode", SqlDbType.VarChar,20),
					new SqlParameter("@GasSourse", SqlDbType.VarChar,4),
					new SqlParameter("@AmountInput", SqlDbType.Decimal,9),
					new SqlParameter("@Status", SqlDbType.VarChar,4),
					new SqlParameter("@RegistrantOID", SqlDbType.VarChar,50),
					new SqlParameter("@RegistrantDate", SqlDbType.Date,3),
					new SqlParameter("@CREATED_BY", SqlDbType.NVarChar,12),
					new SqlParameter("@CREATED_DATE", SqlDbType.Date,3),
					new SqlParameter("@LAST_UPD_BY", SqlDbType.NVarChar,12),
					new SqlParameter("@LAST_UPD_DATE", SqlDbType.Date,3)};
			parameters[0].Value = model.Code;
			parameters[1].Value = model.Year;
			parameters[2].Value = model.Month;
			parameters[3].Value = model.GasType;
			parameters[4].Value = model.BusinessCode;
			parameters[5].Value = model.GasSourse;
			parameters[6].Value = model.AmountInput;
			parameters[7].Value = model.Status;
			parameters[8].Value = model.registrantOID;
			parameters[9].Value = model.RegistrantDate;
			parameters[10].Value = model.CREATED_BY;
			parameters[11].Value = model.CREATED_DATE;
			parameters[12].Value = model.LAST_UPD_BY;
			parameters[13].Value = model.LAST_UPD_DATE;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
                EGMNGS.Common.ComServies.UpdCodeNum(model.Code);
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(EGMNGS.Model.TableGasInStorage model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update TableGasInStorage set ");
			strSql.Append("Code=@Code,");
			strSql.Append("Year=@Year,");
			strSql.Append("Month=@Month,");
			strSql.Append("GasType=@GasType,");
			strSql.Append("BusinessCode=@BusinessCode,");
			strSql.Append("GasSourse=@GasSourse,");
			strSql.Append("AmountInput=@AmountInput,");
			strSql.Append("Status=@Status,");
			strSql.Append("registrantOID=@registrantOID,");
			strSql.Append("RegistrantDate=@RegistrantDate,");
			strSql.Append("CREATED_BY=@CREATED_BY,");
			strSql.Append("CREATED_DATE=@CREATED_DATE,");
			strSql.Append("LAST_UPD_BY=@LAST_UPD_BY,");
			strSql.Append("LAST_UPD_DATE=@LAST_UPD_DATE");
			strSql.Append(" where OID=@OID");
			SqlParameter[] parameters = {
					new SqlParameter("@Code", SqlDbType.VarChar,20),
					new SqlParameter("@Year", SqlDbType.VarChar,4),
					new SqlParameter("@Month", SqlDbType.VarChar,2),
					new SqlParameter("@GasType", SqlDbType.VarChar,4),
					new SqlParameter("@BusinessCode", SqlDbType.VarChar,20),
					new SqlParameter("@GasSourse", SqlDbType.VarChar,4),
					new SqlParameter("@AmountInput", SqlDbType.Decimal,9),
					new SqlParameter("@Status", SqlDbType.VarChar,4),
					new SqlParameter("@RegistrantOID", SqlDbType.VarChar,50),
					new SqlParameter("@RegistrantDate", SqlDbType.Date,3),
					new SqlParameter("@CREATED_BY", SqlDbType.NVarChar,12),
					new SqlParameter("@CREATED_DATE", SqlDbType.Date,3),
					new SqlParameter("@LAST_UPD_BY", SqlDbType.NVarChar,12),
					new SqlParameter("@LAST_UPD_DATE", SqlDbType.Date,3),
					new SqlParameter("@OID", SqlDbType.Int,4)};
			parameters[0].Value = model.Code;
			parameters[1].Value = model.Year;
			parameters[2].Value = model.Month;
			parameters[3].Value = model.GasType;
			parameters[4].Value = model.BusinessCode;
			parameters[5].Value = model.GasSourse;
			parameters[6].Value = model.AmountInput;
			parameters[7].Value = model.Status;
			parameters[8].Value = model.registrantOID;
			parameters[9].Value = model.RegistrantDate;
			parameters[10].Value = model.CREATED_BY;
			parameters[11].Value = model.CREATED_DATE;
			parameters[12].Value = model.LAST_UPD_BY;
			parameters[13].Value = model.LAST_UPD_DATE;
			parameters[14].Value = model.OID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int OID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from TableGasInStorage ");
			strSql.Append(" where OID=@OID");
			SqlParameter[] parameters = {
					new SqlParameter("@OID", SqlDbType.Int,4)
			};
			parameters[0].Value = OID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string OIDlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from TableGasInStorage ");
			strSql.Append(" where OID in ("+OIDlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public EGMNGS.Model.TableGasInStorage GetModel(int OID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 OID,Code,Year,Month,GasType,BusinessCode,GasSourse,AmountInput,Status,registrantOID,RegistrantDate,CREATED_BY,CREATED_DATE,LAST_UPD_BY,LAST_UPD_DATE from TableGasInStorage ");
			strSql.Append(" where OID=@OID");
			SqlParameter[] parameters = {
					new SqlParameter("@OID", SqlDbType.Int,4)
			};
			parameters[0].Value = OID;

			EGMNGS.Model.TableGasInStorage model=new EGMNGS.Model.TableGasInStorage();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				if(ds.Tables[0].Rows[0]["OID"]!=null && ds.Tables[0].Rows[0]["OID"].ToString()!="")
				{
					model.OID=int.Parse(ds.Tables[0].Rows[0]["OID"].ToString());
				}
				if(ds.Tables[0].Rows[0]["Code"]!=null && ds.Tables[0].Rows[0]["Code"].ToString()!="")
				{
					model.Code=ds.Tables[0].Rows[0]["Code"].ToString();
				}
				if(ds.Tables[0].Rows[0]["Year"]!=null && ds.Tables[0].Rows[0]["Year"].ToString()!="")
				{
					model.Year=ds.Tables[0].Rows[0]["Year"].ToString();
				}
				if(ds.Tables[0].Rows[0]["Month"]!=null && ds.Tables[0].Rows[0]["Month"].ToString()!="")
				{
					model.Month=ds.Tables[0].Rows[0]["Month"].ToString();
				}
				if(ds.Tables[0].Rows[0]["GasType"]!=null && ds.Tables[0].Rows[0]["GasType"].ToString()!="")
				{
					model.GasType=ds.Tables[0].Rows[0]["GasType"].ToString();
				}
				if(ds.Tables[0].Rows[0]["BusinessCode"]!=null && ds.Tables[0].Rows[0]["BusinessCode"].ToString()!="")
				{
					model.BusinessCode=ds.Tables[0].Rows[0]["BusinessCode"].ToString();
				}
				if(ds.Tables[0].Rows[0]["GasSourse"]!=null && ds.Tables[0].Rows[0]["GasSourse"].ToString()!="")
				{
					model.GasSourse=ds.Tables[0].Rows[0]["GasSourse"].ToString();
				}
				if(ds.Tables[0].Rows[0]["AmountInput"]!=null && ds.Tables[0].Rows[0]["AmountInput"].ToString()!="")
				{
					model.AmountInput=decimal.Parse(ds.Tables[0].Rows[0]["AmountInput"].ToString());
				}
				if(ds.Tables[0].Rows[0]["Status"]!=null && ds.Tables[0].Rows[0]["Status"].ToString()!="")
				{
					model.Status=ds.Tables[0].Rows[0]["Status"].ToString();
				}
				if(ds.Tables[0].Rows[0]["registrantOID"]!=null && ds.Tables[0].Rows[0]["registrantOID"].ToString()!="")
				{
					model.registrantOID=ds.Tables[0].Rows[0]["registrantOID"].ToString();
				}
				if(ds.Tables[0].Rows[0]["RegistrantDate"]!=null && ds.Tables[0].Rows[0]["RegistrantDate"].ToString()!="")
				{
					model.RegistrantDate=DateTime.Parse(ds.Tables[0].Rows[0]["RegistrantDate"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CREATED_BY"]!=null && ds.Tables[0].Rows[0]["CREATED_BY"].ToString()!="")
				{
					model.CREATED_BY=ds.Tables[0].Rows[0]["CREATED_BY"].ToString();
				}
				if(ds.Tables[0].Rows[0]["CREATED_DATE"]!=null && ds.Tables[0].Rows[0]["CREATED_DATE"].ToString()!="")
				{
					model.CREATED_DATE=DateTime.Parse(ds.Tables[0].Rows[0]["CREATED_DATE"].ToString());
				}
				if(ds.Tables[0].Rows[0]["LAST_UPD_BY"]!=null && ds.Tables[0].Rows[0]["LAST_UPD_BY"].ToString()!="")
				{
					model.LAST_UPD_BY=ds.Tables[0].Rows[0]["LAST_UPD_BY"].ToString();
				}
				if(ds.Tables[0].Rows[0]["LAST_UPD_DATE"]!=null && ds.Tables[0].Rows[0]["LAST_UPD_DATE"].ToString()!="")
				{
					model.LAST_UPD_DATE=DateTime.Parse(ds.Tables[0].Rows[0]["LAST_UPD_DATE"].ToString());
				}
				return model;
			}
			else
			{
				return null;
			}
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select OID,Code,Year,Month,GasType,BusinessCode,GasSourse,AmountInput,Status,registrantOID,RegistrantDate,CREATED_BY,CREATED_DATE,LAST_UPD_BY,LAST_UPD_DATE ");
			strSql.Append(" FROM TableGasInStorage ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" OID,Code,Year,Month,GasType,BusinessCode,GasSourse,AmountInput,Status,registrantOID,RegistrantDate,CREATED_BY,CREATED_DATE,LAST_UPD_BY,LAST_UPD_DATE ");
			strSql.Append(" FROM TableGasInStorage ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM TableGasInStorage ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.OID desc");
			}
			strSql.Append(")AS Row, T.*  from TableGasInStorage T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", (startIndex - 1) * endIndex + 1, startIndex * endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "TableGasInStorage";
			parameters[1].Value = "OID";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  Method
	}
}

